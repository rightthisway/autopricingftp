package com.autopricing.junit;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.rtw.autopricing.util.TicketUtil;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("classpath:*.xml")
public class TicketUtilTestCase {
		private TicketUtil ticketutil;
	
	@Test
	public void testComputeValidRowsByAlternateRow() throws Exception{
		boolean flag = true;
		List<String> strList =  TicketUtil.computeValidRowsByAlternateRow(new String[]{"a","k"});
		List<String> intList =  TicketUtil.computeValidRowsByAlternateRow(new String[]{"1","9"});
		for(String str: strList){
			if(!str.matches("[a-k]")){
				flag = false;
			}
		}
		
		for(String str: intList){
			if(!str.matches("[1-9]")){
				flag = false;
			}
		}
		
		Assert.isTrue(flag);
	}
	
	/*
	public static void main(String[] args) {
		String pal = "acb11bca";
		char[] temp = pal.toCharArray();
		int len = temp.length;
		int n =0;
		if(len%2==0){
			n=len/2;
		}else{
			n=(len+1)/2;
		}
		boolean flag = true;
		for(int i=0 ;i<n;i++){
			System.out.println(temp[i] + " :" + temp[(len-1)-(i)]);
			if(temp[i]!=temp[(len-1)-i]){
				flag = false;
				break;
			}
		}
		System.out.println(flag);
		
	}
	*/
}
