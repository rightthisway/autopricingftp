<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubFtpMenu" name="selectedSubFtpMenu"
	scope="request" />

<div id="adminSubMenu">
	
	<c:choose>
		<c:when test="${selectedSubFtpMenu == 'AutoMaticUpload'}">
			<b>Automatic File Upload</b>
		</c:when>
		<c:otherwise>
			<a href="GetAutomaticFileUpload">Automatic File Upload</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubFtpMenu == 'ManualFileUpload'}">
			<b>Manual File Upload</b>
		</c:when>
		<c:otherwise>
			<a href="GetManualFileUpload">Manual File Upload</a>
		</c:otherwise>
	</c:choose>	
	|
	<c:choose>
		<c:when test="${selectedSubFtpMenu == 'ManageAutoFileUpload'}">
			<b>Manage Automatic File Upload</b>
		</c:when>
		<c:otherwise>
			<a href="LoadAutomaticFileUploadInfo">Manage Automatic File Upload</a>
		</c:otherwise>
	</c:choose>

</div>
<tiles:insertAttribute name="subBody1" />