<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

	

$(function () {
	
	
	 $('#autoVenue').autocomplete("AutoCompleteVenue", {
			width: 650,
			max: 1000,
			minChars: 2,		
			dataType: "text",
			
			formatItem: function(row, i, max) {
				if(row[0]=='VENUE'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				} 
			}
		}).result(function (event,row,formatted){
			  
				$('#autoVenue').val('');
				$('#selectedValue').text(row[2]);
				 if(row[0]=='VENUE'){
					$('#selectedOption').text('Venue');
					$("#venueId").val(row[1]);
										
				}
		});
	 
	 $('#copyAllShippingMethod').click(function(){
			if($('#copyAllShippingMethod').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('shipping_method');
			}else{
				//$('.selectCheck').attr('checked', true);
			}
		});  
		$('#copyAllNearTermDisplayOption').click(function(){
			if($('#copyAllNearTermDisplayOption').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('near_term_display_option'); 
			}else{
				//$('.selectCheck').attr('checked', true);
			}
		});
		
		$('#copyShippingDays').click(function(){
			if($('#copyShippingDays').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('shipping_days'); 
			}else{
				$('.selectCheck').attr('checked', true);
			}
		});
		
		$('#copyAllMarkup').click(function(){
			if($('#copyAllMarkup').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('markup');
			}else{
				$('.selectCheck').attr('checked', true);
			}
		});
	
	 $("#isAllProduct").click(function() {
		if((document.getElementById("isAllProduct").checked)){
			$("#productId").each(function(){
				$("#productId option").attr("selected","selected");				
			});	
		} else {
			$("#productId").each(function(){
				$("#productId option").attr("selected",false);				
			});	
		}
		hideTableGrid();
	 }) ;
	 
	
	
	
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$( "#productId" ).change(function() {
		hideTableGrid();
		});
	
	
	
	
	/* $('#copyAll').click(function(){
		copyAll();
	}); */
});

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}

	function submitForms(action) {
		if(action=='save'){
			if(validateForm()){
				$("#action").val(action);
				$("#formId").submit();
			}
		 }else {
			$("#action").val(action);
			$("#formId").submit();
		} 
	}
	
	function validateForm(){
		var flag= true;
		var isMinimamOneRecord = false;
		
		$('.selectCheck:checkbox:checked').each(function () {
			
			isMinimamOneRecord = true;
			var minimamOneValue = false;
			var id,value;
			id = this.id.replace('checkbox','shipping_days');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Please select valid shipping days.');
				$("#"+id).focus();
				flag = false;
				return ;
			} 
			id = this.id.replace('checkbox','markup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
					alert('Please enter valid Markup.');
					$("#"+id).focus();
					flag = false;
					return ;
			}
			id = this.id.replace('checkbox','shipping_method');
			value = $.trim($("#"+id).val());
			if(value != '') {
				minimamOneValue = true;
			}
			id = this.id.replace('checkbox','near_term_display_option');
			value = $.trim($("#"+id).val());
			if(value != '') {
				minimamOneValue = true;
			}
			
			/*if(!minimamOneValue){
				alert('Please choose value for either shipping method or near term display option or shipping days or markup.');
				id = this.id.replace('checkbox','shipping_method');
				$("#"+id).focus();
				flag = false;
			}*/
		});
		
		/*if(!isMinimamOneRecord) {
			alert('Please Select Minimum one record for Update.');
			flag = false;
			//return ;
		}*/
		
		if(!flag){
			return flag;	
		}
		return true;
}
	function selectRow(id) {
		$("#checkbox_" + id ).attr('checked', true);
	}
	
	function hideTableGrid() {
		if($('#gridTable').length) {
			$('#gridTable').hide();
		}
	}
	
	function popupapVenueSettingsAudit(pId,vId){
		var url = "AutopricingVenuesettingsAudit?pId="+pId+"&vId="+vId;
		newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
		if (window.focus) {newwindow.focus()}
		return false;
		
	} 
	
</script>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> > Autopricing Venue Settings
</div>
<h1>Autopricing Venue Settings</h1>
<br/>
<hr/>
<form:form action="AutopricingVenuesettings" method="post" id="formId">
<input type="hidden" name="action" id="action"/>
<input type="hidden" name="uncheckedboxes" id="uncheckedboxes"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	
<table align="center">
<tr>
			<td colspan="3" align="center"><b>Product:</b>
				<input type="hidden" id="eventStr" name="eventStr" value = ${eventStr}/> 
				Check  All: <input type="checkbox" name="isAllProduct" <c:if test="${isAllProductSelected == 'on'}">checked</c:if> id="isAllProduct" /> 
				<br/>
				<select name="productId" id="productId" style="width: 300px" multiple size="5">
					<c:forEach var="product" items="${productList}">
					<c:set var="a2" value="false" />  
					  <c:forEach var="selected1" items="${selectedProductId}">
					  <c:if test="${product.id == selected1}">
					  <c:set var="a2" value="true" />
					  </c:if>
					  </c:forEach>
					    <option value="${product.id}" <c:if test="${a2 == 'true'}">selected</c:if>>${product.name}</option>
					  </c:forEach>
				</select> 
			<br />
			<br /> 
			</td>
			<td></td>
			<td colspan="3" align="center"></td>
			
		</tr>
<tr>
<td colspan="3" align="center">
<b>Select The Venue :</b>
      <input type="text" name="autoVenue" id="autoVenue"  onchange="hideTableGrid();">
      <input type="hidden" id="venueId" name="venueId" value="${venueId}">
</td> 
</tr> 
<tr>
			<td><b><span id="selectedOption" >${selectedOption}</span></b></td>
			<td><b><span id="selectedValue" >${selectedValue}</span></b></td>
			<td></td>
		</tr>  
<tr>
	<td colspan="3" align="center">
	<br />
		<input type="button" value="Get Data" onclick="submitForms('search');" class="medButton">
	</td>
</tr>
</table>
	<br />
	<c:if test="${not empty autopricingVenueSettings}" >
	<div id="gridTable" align="center" >
	<div style="float:center; ">
		<b>Update Existing Events : </b><input type="checkbox" name="update_existinge_events" value="Y" id="update_existinge_events"/>
		<br />
		<br />
	</div>
		<display:table list="${autopricingVenueSettings}" style="width:60%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="apVenueSettings"  requestURI="#" class="list">

		<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
			<input type="checkbox" name="checkbox_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" id="checkbox_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" 
			<c:if test ="${apVenueSettings.id !=null  && apVenueSettings.status !=null && apVenueSettings.status == 'ACTIVE'}"> checked="checked" </c:if> class="selectCheck" />
		</display:column>
		<display:column title="Product" sortable="true">${apVenueSettings.product.name}</display:column>
		<display:column title="Venue" sortable="true">${apVenueSettings.venue.building} ${apVenueSettings.venue.city} ${apVenueSettings.venue.state} ${apVenueSettings.venue.country}</display:column>
		<display:column title='Shipping Method<br/><input type="checkbox" name="copyAllShippingMethod" id="copyAllShippingMethod" >' >
				<select name="shipping_method_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" id="shipping_method_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" 
						class="shippingMethodText" onchange="selectRow('${apVenueSettings.product.id}_${apVenueSettings.venue.id}');" style="width:100px;" >
						<option value="">--- Select ---</option> 
					<option value="0" <c:if test ="${apVenueSettings.shippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${apVenueSettings.shippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${apVenueSettings.shippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${apVenueSettings.shippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${apVenueSettings.shippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${apVenueSettings.shippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${apVenueSettings.shippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${apVenueSettings.shippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${apVenueSettings.shippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${apVenueSettings.shippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
					
				</select>
			</display:column> 
			
			<display:column title='Near Term Display Option<br/><input type="checkbox" name="copyAllNearTermDisplayOption" id="copyAllNearTermDisplayOption" >' >
				<select name="near_term_display_option_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" id="near_term_display_option_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" 
						class="shippingMethodText" onchange="selectRow('${apVenueSettings.product.id}_${apVenueSettings.venue.id}');" style="width:100px;" >
					 
					<option value="0" <c:if test ="${apVenueSettings.nearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${apVenueSettings.nearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${apVenueSettings.nearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
			</display:column>
			<display:column title='Shipping Days<br/><input type="checkbox" name="copyShippingDays" id="copyShippingDays" >' >
				<select name="shipping_days_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" id="shipping_days_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" 
						class="shippingMethodText" onchange="selectRow('${apVenueSettings.product.id}_${apVenueSettings.venue.id}');" style="width:100px;" >
						<option value="">--- Select ---</option> 
						
						<option value="0" <c:if test ="${apVenueSettings.shippingDays eq '0'}" > selected </c:if> >0 Days</option>
					<option value="2" <c:if test ="${apVenueSettings.shippingDays eq '2'}" > selected </c:if> >2 Days</option>
					<option value="4" <c:if test ="${apVenueSettings.shippingDays eq '4'}" > selected </c:if> >4 Days</option>
				</select>
			</display:column>
			<display:column title='Markup<br/><input type="checkbox" name="copyAllMarkup" id="copyAllMarkup" >' >
				<input type="text" value="${apVenueSettings.markup}" name="markup_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" id="markup_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" 
					onchange="selectRow('${apVenueSettings.product.id}_${apVenueSettings.venue.id}');" size="5" class="markupText"/>
			</display:column>
			
			<%-- <display:column title="Updated By ">${apVenueSettings.lastUpdatedBy}</display:column>
			<display:column title="Updated Date ">
				<c:if test="${apVenueSettings.lastUpdatedDate ne null}">
					<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${apVenueSettings.lastUpdatedDate}" />
				</c:if>
			</display:column> --%>
						
        <display:column title="Action" >
			<input type="button" value="Audit" class="medButton" id="audit_${apVenueSettings.product.id}_${apVenueSettings.venue.id}" onClick="popupapVenueSettingsAudit('${apVenueSettings.product.id}','${apVenueSettings.venue.id}');"/>
		</display:column> 
		</display:table>
	<br />	
	<div style="float:right; padding-right:320px;">
		<input type="button" value="Save"    class="medButton" id="update1" name="update1" onclick="javascript:submitForms('save');"/> 
	</div>
	</div>	
	<br />
	
	
	</c:if>
	<c:if test="${empty autopricingVenueSettings}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>


</form:form>