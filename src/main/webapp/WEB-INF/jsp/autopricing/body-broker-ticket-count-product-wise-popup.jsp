
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%-- <jsp:include page="../decorators.jsp" /> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<div>

	<b>Broker : ${broker.name}</b>
	 
</div>

<div style="position:absolute;margin-top:22px;">
<c:if test="${not empty catList}" >
	<div id="auditTable" >
<h2> Category Ticket Count Details </h2>
		<display:table list="${catList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="obj"  requestURI="#" class="list">			
			<display:column title="Product Name">
			<c:choose>
				<c:when test="${obj[0] == 'MANUAL'}">
					<p>Manual Cat</p>		
				</c:when>
				<c:otherwise>
					<p>${map[obj[0]]}</p>
				</c:otherwise>
			</c:choose>
			</display:column>
			<display:column title="Internal Note">
				<c:choose>
					<c:when test="${obj[0] == 'MANUAL'}">
						<p>-</p>		
					</c:when>
					<c:otherwise>
						<p>${obj[0]}</p>
					</c:otherwise>
				</c:choose>		
			</display:column>
			
			<display:column  title ="Ticket Count">
			${obj[1]}
			</display:column>
		</display:table>
	</div>	
	</c:if>
	<c:if test="${catList eq null}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">No Category Tickets</span></h4> 
	</div>
		
	</c:if>
</div>
	<br/>
	
	
<hr>
<div style="margin-left:600px;">
<c:if test="${not empty ssList}" >
	<div id="auditTable" >
<h2> SS Account Ticket Count Details </h2>
		<display:table list="${ssList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="obj"  requestURI="#" class="list">			
			<display:column title="Product Name">
			<c:choose>
				<c:when test="${obj[0] == 'MANUAL'}">
					<p>SS Manual</p>		
				</c:when>
				<c:otherwise>
					<p>${map[obj[0]]}</p>
				</c:otherwise>
			</c:choose>
			</display:column>
			<display:column title="Internal Note">
			<c:choose>
				<c:when test="${obj[0] == 'MANUAL'}">
					<p>-</p>		
				</c:when>
				<c:otherwise>
					<p>${obj[0]}</p>
				</c:otherwise>
			</c:choose>	
			</display:column>
			
			<display:column  title ="Ticket Count">
			${obj[1]}
			</display:column>
		</display:table>
	</div>	
	</c:if>
	<c:if test="${ssList eq null}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">No SS Account Tickets</span></h4> 
	</div>
		
	</c:if>
</div>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>	
</body>
</html>