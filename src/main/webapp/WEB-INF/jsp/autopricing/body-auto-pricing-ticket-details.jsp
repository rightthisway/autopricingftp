<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<script type="text/javascript" src="../js/highChart/highcharts.js"></script>
<script type="text/javascript" src="../js/highChart/exporting.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
 


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Auto Pricing Ticket Details
</div>


<h1>Auto Pricing Ticket Details</h1>
<br/>
<form:form action="AutopricingTicketDetails" method="post" id="formId">
	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	
	<br/>
	<br/>
	
<table width="80%">
		
	<%-- 	<tr>
			<td><b>Filter: By Artist </b></td>
			<td>
				<select name="artistId" id="artistId" onchange="changeFilter();">
					<option value="">Select</option>
					<c:forEach var="artist" items="${artists}">
						<option value="${artist.id}" style="height:20px;" 
						<c:if test="${artist.id == selectedArtistId}">selected</c:if>>${artist.name}</option>
					</c:forEach>
				</select>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>  --%>
		
		
		 	<tr>		
		<td><b>Filter: By Artist </b></td>
		<td>	
		<input type="hidden" value = "${selectedArtistName}" name="hidselectedArtistName" id= "hidselectedArtistName">
		<input type="hidden" value ="${selectedArtistId}" name="artistId" id= "artistId">
		
		<span id="artistSelectSpanId">		
			<input type="text" id="txtartistId" name="txtartistId">
		</span>
		
		<span id="artistLabelSpanId">
		<span id="labelArtistName">${selectedArtistName}</span>			
		<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"  />
		</span>		
		
		
		</td>
		</tr> 
		
	<%-- 	
		<tr>
			<td><b>And By Venue </b></td>
			<td>
				<select name="venueId" id="venueId"  onchange="changeFilter();">
					<option value="">Select</option>
					<c:forEach var="venue" items="${venues}">
						<option value="${venue.id}" style="height:20px;" 
						<c:if test="${venue.id == selectedVenueId}">selected</c:if>>${venue.building}</option>
					</c:forEach>
				</select>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr> --%>
		
		
		<tr>		
		<td><b>And By Venue</b></td>
		<td>	
		<input type="hidden" value = "${selectedVenueName}" name="hidselectedVenueName" id= "hidselectedVenueName">
		<input type="hidden" value ="${selectedVenueId}" name="venueId" id= "venueId">
		
		<span id = "selectDropDownVenue">		
		<select name="dropDownvenueId" id="dropDownvenueId"  onchange="submitVenue(this);">
					<option value="">Select</option>
					<c:forEach var="venue" items="${venues}">
						<option value="${venue.id}" style="height:20px;" 
						<c:if test="${venue.id == selectedVenueId}">selected</c:if>>${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}</option>
					</c:forEach>
				</select>		
				
		<!-- <select name="dropDownvenueId" id="dropDownvenueId"  onchange="fetchEvents(this);">
					<option value="">Select</option>
		</select> -->
		
		</span>
		
		<span id="venueSelectSpanId">		
			<input type="text" id="txtvenueId" name="txtvenueId">
		</span>
		
		<span id="venueLabelSpanId">
		<span id="labelVenueName">${selectedVenueName}</span>			
		<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
		</span>		
		
		
		</td>
		</tr> 
		
			
		
		
		<tr>
			<td><b>Events: </b></td>
			<td>
				<select name="eventId" id="eventId"  >
					<option value="">Select</option>
					<c:forEach var="event" items="${events}">
						<option value="${event.id}" style="height:20px;" 
						<c:if test="${event.id == selectedEventId}">selected</c:if>>${event.name} 
						<fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								<c:if test="${event.venueId != null}"> - ${event.venue.building}</c:if>
						</option>
					</c:forEach>
				</select>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		
		
		<tr>
		
			<td><b>Section :</b></td>
			<td>
				<input type="text" id="section" name="section"  value="${param.section}" />
			</td>
		</tr>
		
		
		<tr>
		
			<td><b>Row :</b></td>
			<td>
				<input type="text" id="row" name="row"  value="${param.row}"  />
			</td>
		</tr>
		
		<tr>
		
			<td><b>Quantity : </b></td>
			<td>
				<input type="text" id="quantity" name="quantity" value="${param.quantity}"  />
			</td>
		</tr>
		<tr>
			<td colspan="6" align="center">
				<input type="button"  id="submitbutton"value="Get Ticket Details" onclick="submitForms();" class="medButton">
			</td>
		</tr>
	</table>
	
	<c:if test="${showgrid ==true}">
	<div id="container-tabs" style="margin-top: 5px">
	
	<ul>
			<li><a href="#fragment-above96Cats" id="tab-above96Cats"><span>Autocats96</span></a></li>
			<!-- <li><a href="#fragment-below96Cats" id="tab-below96Cats"><span>Below96Cats</span></a></li> -->
			<!-- <li><a href="#fragment-minicats" id="tab-minicattickets"><span>MiniCat</span></a></li>
			<li><a href="#fragment-vipminicats" id="tab-vipminicattickets"><span>VipMiniCat</span></a></li> 
			<li><a href="#fragment-lastrowminicats" id="tab-lastrowminicats"><span>LastRowMiniCat</span></a></li>  --> 
			<li><a href="#fragment-larrylastcats" id="tab-larrylastcats"><span>SS Account</span></a></li>
			<!-- <li><a href="#fragment-zonedLastrowminicats" id="tab-zonedlastrowcats"><span>Zoned LastRow Minicat</span></a></li> -->
			<!-- <li><a href="#fragment-zonesPricingcats" id="tab-zonespricingcats"><span>ZonesPricing</span></a></li>
			<li><a href="#fragment-tixcity-zonesPricingcats" id="tab-tixcity-zonespricingcats"><span>RTW ZonesPricing</span></a></li> -->
			<!-- <li><a href="#fragment-presaleAutocats" id="tab-presaleAutocats"><span>PreSale AutoCat</span></a></li> -->
			<li><a href="#fragment-zoneTicketsProcessorcats" id="tab-zoneTicketsProcessorcats"><span>RewardTheFan Listings</span></a></li>
			<!-- <li><a href="#fragment-manhattan-zonesPricingcats" id="tab-manhattan-zonespricingcats"><span>Manhattan ZonesPricing</span></a></li>
			<li><a href="#fragment-viplastrowminicats" id="tab-viplastrowminicats"><span>VipLastRowMiniCat</span></a></li>
			<li><a href="#fragment-presaleZoneTicketsCats" id="tab-presaleZoneTicketsCats"><span>Presale ZoneTickets</span></a></li>
			<li><a href="#fragment-sg-lastfiverow" id="tab-sg-lastfiverow-tickets"><span>SG LastFiveRow</span></a></li> -->
			<!-- <li><a href="#fragment-vipautocats" id="tab-vipautocats"><span>VipAutoCat</span></a></li>  -->  
			<!-- <li><a href="#fragment-lastfiverowminicats" id="tab-lastfiverowminicats"><span>LastFiveRowsMiniCat</span></a></li> -->
   	</ul>
   		
   		<%-- <div id="fragment-minicats">
   		<c:if test="${not empty categoryTickets}" >
		<div id="ebayInventoryTabel" >
		
		
		<display:table list="${categoryTickets}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="tgCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${tgCatTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${tgCatTicket.rowRange}</display:column>
			<display:column title="Last_Row" sortable="true">${tgCatTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${tgCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${tgCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${tgCatTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${tgCatTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${tgCatTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${tgCatTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${tgCatTicket.scoreBigPrice}</display:column>
			<display:column title="FanXchange Price" sortable="true">${tgCatTicket.fanxchangePrice}</display:column>
			<display:column title="Status" sortable="true">${tgCatTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${tgCatTicket.reason ne null}">${tgCatTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${tgCatTicket.id}"
				onClick="popupTGCatsTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${tgCatTicket.id}"
				onClick="popupTgcatTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.ticketId}', '${tgCatTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${tgCatTicket.baseTicketOneHistory ne null and tgCatTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${tgCatTicket.baseTicketOne}"
						onClick="popupTgcatTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.baseTicketOne}', '${tgCatTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${tgCatTicket.baseTicketTwoHistory ne null and tgCatTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${tgCatTicket.baseTicketTwo}"
						onClick="popupTgcatTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.baseTicketTwo}','${tgCatTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${tgCatTicket.baseTicketThreeHistory ne null and tgCatTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${tgCatTicket.baseTicketThree}"
						onClick="popupTgcatTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.baseTicketThree}','${tgCatTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty categoryTickets}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<%-- <div id="fragment-sg-lastfiverow">
   		<c:if test="${not empty sgLastFiveRowCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
		
		
		<display:table list="${sgLastFiveRowCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="sgLastFiveRowTix"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${sgLastFiveRowTix.section}</display:column>
			<display:column title="Last_Row" sortable="true">${sgLastFiveRowTix.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${sgLastFiveRowTix.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${sgLastFiveRowTix.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${sgLastFiveRowTix.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${sgLastFiveRowTix.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${sgLastFiveRowTix.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${sgLastFiveRowTix.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${sgLastFiveRowTix.scoreBigPrice}</display:column>
			<display:column title="FanXchange Price" sortable="true">${sgLastFiveRowTix.fanxchangePrice}</display:column>
			<display:column title="Status" sortable="true">${sgLastFiveRowTix.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${sgLastFiveRowTix.reason ne null}">${sgLastFiveRowTix.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${sgLastFiveRowTix.id}"
				onClick="popupSGLastFiveRowTicketInfo('${sgLastFiveRowTix.eventId}','${sgLastFiveRowTix.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${sgLastFiveRowTix.id}"
				onClick="popupSGLastFiveRowTMATTicketInfo('${sgLastFiveRowTix.eventId}','${sgLastFiveRowTix.ticketId}', '${sgLastFiveRowTix.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${sgLastFiveRowTix.baseTicketOneHistory ne null and sgLastFiveRowTix.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${sgLastFiveRowTix.baseTicketOne}"
						onClick="popupSGLastFiveRowTMATTicketInfo('${sgLastFiveRowTix.eventId}','${sgLastFiveRowTix.baseTicketOne}', '${sgLastFiveRowTix.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${sgLastFiveRowTix.baseTicketTwoHistory ne null and sgLastFiveRowTix.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${sgLastFiveRowTix.baseTicketTwo}"
						onClick="popupSGLastFiveRowTMATTicketInfo('${sgLastFiveRowTix.eventId}','${sgLastFiveRowTix.baseTicketTwo}','${sgLastFiveRowTix.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${sgLastFiveRowTix.baseTicketThreeHistory ne null and sgLastFiveRowTix.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${sgLastFiveRowTix.baseTicketThree}"
						onClick="popupSGLastFiveRowTMATTicketInfo('${sgLastFiveRowTix.eventId}','${sgLastFiveRowTix.baseTicketThree}','${sgLastFiveRowTix.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty sgLastFiveRowCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	
	
	
	
	<%-- <div id="fragment-lastrowminicats">
   		<c:if test="${not empty lastRowMiniCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${lastRowMiniCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="lastRowMiniTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${lastRowMiniTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${lastRowMiniTicket.rowRange}</display:column>
			<display:column title="Last_Row" sortable="true">${lastRowMiniTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${lastRowMiniTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${lastRowMiniTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${lastRowMiniTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${lastRowMiniTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${lastRowMiniTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${lastRowMiniTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${lastRowMiniTicket.scoreBigPrice}</display:column>
			<display:column title="FanXchange Price" sortable="true">${lastRowMiniTicket.fanxchangePrice}</display:column>
			<display:column title="Status" sortable="true">${lastRowMiniTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${lastRowMiniTicket.reason ne null}">${lastRowMiniTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${lastRowMiniTicket.id}"
				onClick="popupLastRowCatsTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${lastRowMiniTicket.id}"
				onClick="popupLastRowTMATTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.ticketId}','${lastRowMiniTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${lastRowMiniTicket.baseTicketOneHistory ne null and lastRowMiniTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${lastRowMiniTicket.baseTicketOne}"
						onClick="popupLastRowTMATTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.baseTicketOne}','${lastRowMiniTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${lastRowMiniTicket.baseTicketTwoHistory ne null and lastRowMiniTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${lastRowMiniTicket.baseTicketTwo}"
						onClick="popupLastRowTMATTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.baseTicketTwo}','${lastRowMiniTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${lastRowMiniTicket.baseTicketThreeHistory ne null and lastRowMiniTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${lastRowMiniTicket.baseTicketThree}"
						onClick="popupLastRowTMATTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.baseTicketThree}','${lastRowMiniTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty lastRowMiniCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	
	<!-- Larry Lat Display -->
	
	
	
	<div id="fragment-larrylastcats">
   		<c:if test="${not empty larryLastCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${larryLastCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="larryLastTicket"  requestURI="#" class="list">
			
			<display:column title="Internal Notes" sortable="true">${larryLastTicket.internalNotes}</display:column>
			<display:column title="Section" sortable="true">${larryLastTicket.section}</display:column>
			<display:column title="Last_Row" sortable="true">${larryLastTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${larryLastTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${larryLastTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${larryLastTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${larryLastTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${larryLastTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${larryLastTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${larryLastTicket.scoreBigPrice}</display:column>
			<display:column title="FanXchange Price" sortable="true">${larryLastTicket.fanxchangePrice}</display:column>
			<display:column title="Status" sortable="true">${larryLastTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${larryLastTicket.reason ne null}">${larryLastTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${larryLastTicket.id}"
				onClick="popupLarryLastCatsTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${larryLastTicket.id}"
				onClick="popupLarryLastTMATTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.ticketId}','${larryLastTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${larryLastTicket.baseTicketOneHistory ne null and larryLastTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${larryLastTicket.baseTicketOne}"
						onClick="popupLarryLastTMATTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.baseTicketOne}','${larryLastTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${larryLastTicket.baseTicketTwoHistory ne null and larryLastTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${larryLastTicket.baseTicketTwo}"
						onClick="popupLarryLastTMATTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.baseTicketTwo}','${larryLastTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${larryLastTicket.baseTicketThreeHistory ne null and larryLastTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${larryLastTicket.baseTicketThree}"
						onClick="popupLarryLastTMATTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.baseTicketThree}','${larryLastTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty larryLastCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	<!-- VipAutoCat -->
	
	<%-- <div id="fragment-vipautocats">
   		<c:if test="${not empty vipAutoCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${vipAutoCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="vipautoTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${vipautoTicket.section}</display:column>
			
			<display:column title="Last_Row" sortable="true">${vipautoTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${vipautoTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${vipautoTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${vipautoTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${vipautoTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${vipautoTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${vipautoTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${vipautoTicket.scoreBigPrice}</display:column>
			<display:column title="Status" sortable="true">${vipautoTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${vipautoTicket.id}"
				onClick="popupVipAutoCatsTicketInfo('${vipautoTicket.eventId}','${vipautoTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${vipautoTicket.id}"
				onClick="popupVipAutoTMATTicketInfo('${vipautoTicket.eventId}','${vipautoTicket.ticketId}','${vipautoTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${vipautoTicket.baseTicketOneHistory ne null and vipautoTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${vipautoTicket.baseTicketOne}"
						onClick="popupVipAutoTMATBaseTicket1Info('${vipautoTicket.eventId}','${vipautoTicket.baseTicketOne}','${vipautoTicket.id}');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${vipautoTicket.baseTicketTwoHistory ne null and vipautoTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${vipautoTicket.baseTicketTwo}"
						onClick="popupVipAutoTMATBaseTicket2Info('${vipautoTicket.eventId}','${vipautoTicket.baseTicketTwo}','${vipautoTicket.id}');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${vipautoTicket.baseTicketThreeHistory ne null and vipautoTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${vipautoTicket.baseTicketThree}"
						onClick="popupVipAutoTMATBaseTicket3Info('${vipautoTicket.eventId}','${vipautoTicket.baseTicketThree}','${vipautoTicket.id}');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty vipAutoCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<%-- <div id="fragment-lastfiverowminicats">
   		<c:if test="${not empty lastFiveRowMiniCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	<display:table list="${lastFiveRowMiniCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="lastfiverowTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${lastfiverowTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${lastfiverowTicket.rowRange}</display:column>
			<display:column title="Last Row" sortable="true">${lastfiverowTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${lastfiverowTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${lastfiverowTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${lastfiverowTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${lastfiverowTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${lastfiverowTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${lastfiverowTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${lastfiverowTicket.scoreBigPrice}</display:column>
			<display:column title="Status" sortable="true">${lastfiverowTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${lastfiverowTicket.id}"
				onClick="popupLastFiveRowCatsTicketInfo('${lastfiverowTicket.eventId}','${lastfiverowTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${lastfiverowTicket.id}"
				onClick="popupLastFiveRowTMATTicketInfo('${lastfiverowTicket.eventId}','${lastfiverowTicket.ticketId}','${lastfiverowTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${lastfiverowTicket.baseTicketOneHistory ne null and lastfiverowTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${lastfiverowTicket.baseTicketOne}"
						onClick="popupLastFiveRowTMATBaseTicket1Info('${lastfiverowTicket.eventId}','${lastfiverowTicket.baseTicketOne}','${lastfiverowTicket.id}');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${lastfiverowTicket.baseTicketTwoHistory ne null and lastfiverowTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${lastfiverowTicket.baseTicketTwo}"
						onClick="popupLastFiveRowTMATBaseTicket2Info('${lastfiverowTicket.eventId}','${lastfiverowTicket.baseTicketTwo}','${lastfiverowTicket.id}');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${lastfiverowTicket.baseTicketThreeHistory ne null and lastfiverowTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${lastfiverowTicket.baseTicketThree}"
						onClick="popupLastFiveRowTMATBaseTicket3Info('${lastfiverowTicket.eventId}','${lastfiverowTicket.baseTicketThree}','${lastfiverowTicket.id}');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
			</div>
	</c:if>
	<c:if test="${empty lastFiveRowMiniCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	
	<%-- <div id="fragment-vipminicats">
	<c:if test="${not empty vipCatsCategoryTicketsList}" >
	
		
		<display:table list="${vipCatsCategoryTicketsList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="vipCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${vipCatTicket.section}</display:column>
			<display:column title="Alternate Row" sortable="true">${vipCatTicket.rowRange}</display:column>
			<display:column title="Quantity" sortable="true">${vipCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${vipCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${vipCatTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${vipCatTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${vipCatTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${vipCatTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${vipCatTicket.scoreBigPrice}</display:column>
			<display:column title="FanXchange Price" sortable="true">${vipCatTicket.fanxchangePrice}</display:column>
			<display:column title="Status" sortable="true">${vipCatTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${vipCatTicket.reason ne null}">${vipCatTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${vipCatTicket.id}"
				onClick="popupVipCatsTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${vipCatTicket.id}"
				onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.ticketId}','${vipCatTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${vipCatTicket.baseTicketOneHistory ne null and vipCatTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${vipCatTicket.baseTicketOne}"
						onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.baseTicketOne}','${vipCatTicket.id}','baseTicketOne');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${vipCatTicket.baseTicketTwoHistory ne null and vipCatTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${vipCatTicket.baseTicketTwo}"
						onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.baseTicketTwo}','${vipCatTicket.id}','baseTicketTwo');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${vipCatTicket.baseTicketThreeHistory ne null and vipCatTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${vipCatTicket.baseTicketThree}"
						onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.baseTicketThree}','${vipCatTicket.id}','baseTicketThree');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</c:if>
	
	<c:if test="${empty vipCatsCategoryTicketsList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<%-- <div id="fragment-zonedLastrowminicats">
   		<c:if test="${not empty zonedLastRowMiniCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${zonedLastRowMiniCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="zonedLastRowMiniTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${zonedLastRowMiniTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${zonedLastRowMiniTicket.rowRange}</display:column>
			<display:column title="Last_Row" sortable="true">${zonedLastRowMiniTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${zonedLastRowMiniTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${zonedLastRowMiniTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${zonedLastRowMiniTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${zonedLastRowMiniTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${zonedLastRowMiniTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${zonedLastRowMiniTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${zonedLastRowMiniTicket.scoreBigPrice}</display:column>
			<display:column title="FanXchange Price" sortable="true">${zonedLastRowMiniTicket.fanxchangePrice}</display:column>
			<display:column title="Status" sortable="true">${zonedLastRowMiniTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${zonedLastRowMiniTicket.reason ne null}">${zonedLastRowMiniTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${zonedLastRowMiniTicket.id}"
				onClick="popupZonedLastRowCatsTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${zonedLastRowMiniTicket.id}"
				onClick="popupZonedLastRowTMATTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.ticketId}','${zonedLastRowMiniTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${zonedLastRowMiniTicket.baseTicketOneHistory ne null and zonedLastRowMiniTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${zonedLastRowMiniTicket.baseTicketOne}"
						onClick="popupZonedLastRowTMATTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.baseTicketOne}','${zonedLastRowMiniTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${zonedLastRowMiniTicket.baseTicketTwoHistory ne null and zonedLastRowMiniTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${zonedLastRowMiniTicket.baseTicketTwo}"
						onClick="popupZonedLastRowTMATTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.baseTicketTwo}','${zonedLastRowMiniTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${zonedLastRowMiniTicket.baseTicketThreeHistory ne null and zonedLastRowMiniTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${zonedLastRowMiniTicket.baseTicketThree}"
						onClick="popupZonedLastRowTMATTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.baseTicketThree}','${zonedLastRowMiniTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty zonedLastRowMiniCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<%-- <div id="fragment-zonesPricingcats">
   		<c:if test="${not empty zonesPricingCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${zonesPricingCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="zonesPricingTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${zonesPricingTicket.section}</display:column>
			<display:column title="Quantity" sortable="true">${zonesPricingTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${zonesPricingTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${zonesPricingTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${zonesPricingTicket.tnPrice}</display:column>
			<display:column title="FloorCap" sortable="true">${zonesPricingTicket.floorCap}</display:column>
			<display:column title="Vivid Price" sortable="true">${zonesPricingTicket.vividPrice}</display:column>
			 <display:column title="TickPick Price" sortable="true">${zonesPricingTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${zonesPricingTicket.scoreBigPrice}</display:column>
			<display:column title="Status" sortable="true">${zonesPricingTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${zonesPricingTicket.reason ne null}">${zonesPricingTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${zonesPricingTicket.id}"
				onClick="popupZonesPricingCatsTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${zonesPricingTicket.id}"
				onClick="popupZonesPricingTMATTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.ticketId}','${zonesPricingTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${zonesPricingTicket.baseTicketOneHistory ne null and zonesPricingTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${zonesPricingTicket.baseTicketOne}"
						onClick="popupZonesPricingTMATTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.baseTicketOne}','${zonesPricingTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${zonesPricingTicket.baseTicketTwoHistory ne null and zonesPricingTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${zonesPricingTicket.baseTicketTwo}"
						onClick="popupZonesPricingTMATTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.baseTicketTwo}','${zonesPricingTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${zonesPricingTicket.baseTicketThreeHistory ne null and zonesPricingTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${zonesPricingTicket.baseTicketThree}"
						onClick="popupZonesPricingTMATTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.baseTicketThree}','${zonesPricingTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
	<c:if test="${empty zonesPricingCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<%-- <div id="fragment-tixcity-zonesPricingcats">
   		<c:if test="${not empty zonesPricingCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${tixCityZonesPricingCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="tcZonesPricingTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${tcZonesPricingTicket.section}</display:column>
			<display:column title="Quantity" sortable="true">${tcZonesPricingTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${tcZonesPricingTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${tcZonesPricingTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${tcZonesPricingTicket.tnPrice}</display:column>
			<display:column title="FloorCap" sortable="true">${tcZonesPricingTicket.floorCap}</display:column>
			<display:column title="Vivid Price" sortable="true">${tcZonesPricingTicket.vividPrice}</display:column>
			 <display:column title="TickPick Price" sortable="true">${tcZonesPricingTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${tcZonesPricingTicket.scoreBigPrice}</display:column>
			<display:column title="Status" sortable="true">${tcZonesPricingTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${tcZonesPricingTicket.reason ne null}">${tcZonesPricingTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${tcZonesPricingTicket.id}"
				onClick="popupTixCityZonesPricingCatsTicketInfo('${tcZonesPricingTicket.eventId}','${tcZonesPricingTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${tcZonesPricingTicket.id}"
				onClick="popupTixCityZonesPricingTMATTicketInfo('${tcZonesPricingTicket.eventId}','${tcZonesPricingTicket.ticketId}','${tcZonesPricingTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${tcZonesPricingTicket.baseTicketOneHistory ne null and tcZonesPricingTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${tcZonesPricingTicket.baseTicketOne}"
						onClick="popupTixCityZonesPricingTMATTicketInfo('${tcZonesPricingTicket.eventId}','${tcZonesPricingTicket.baseTicketOne}','${tcZonesPricingTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${tcZonesPricingTicket.baseTicketTwoHistory ne null and tcZonesPricingTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${tcZonesPricingTicket.baseTicketTwo}"
						onClick="popupTixCityZonesPricingTMATTicketInfo('${tcZonesPricingTicket.eventId}','${tcZonesPricingTicket.baseTicketTwo}','${tcZonesPricingTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${tcZonesPricingTicket.baseTicketThreeHistory ne null and tcZonesPricingTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${tcZonesPricingTicket.baseTicketThree}"
						onClick="popupTixCityZonesPricingTMATTicketInfo('${tcZonesPricingTicket.eventId}','${tcZonesPricingTicket.baseTicketThree}','${tcZonesPricingTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
	<c:if test="${empty tcZonesPricingCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<div id="fragment-zoneTicketsProcessorcats">
   		<c:if test="${not empty zoneTicketsProcessorCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${zoneTicketsProcessorCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="zoneTicketsCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${zoneTicketsCatTicket.section}</display:column>
			<display:column title="Quantity" sortable="true">${zoneTicketsCatTicket.quantity}</display:column>
			<%-- <display:column title="Priority" sortable="true">${zoneTicketsCatTicket.priority}</display:column> --%>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${zoneTicketsCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${zoneTicketsCatTicket.actualPrice}</display:column>
			<display:column title="ZoneTicket Prices" sortable="true">${zoneTicketsCatTicket.zoneTicketPrice}</display:column>
			<display:column title="Tax Amount" sortable="true">${zoneTicketsCatTicket.taxAmount}</display:column>
			<display:column title="Tax Percentage" sortable="true">${zoneTicketsCatTicket.taxPercentage}</display:column>
			<%--<display:column title="Vivid Price" sortable="true">${zoneTicketsCatTicket.vividPrice}</display:column>
			 <display:column title="TickPick Price" sortable="true">${zoneTicketsCatTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${zoneTicketsCatTicket.scoreBigPrice}</display:column> --%>
			<display:column title="Status" sortable="true">${zoneTicketsCatTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${zoneTicketsCatTicket.reason ne null}">${zoneTicketsCatTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${zoneTicketsCatTicket.id}"
				onClick="popupPresaleZoneTicketsCatsTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${zoneTicketsCatTicket.id}"
				onClick="popupZoneTicketsTMATTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.ticketId}','${zoneTicketsCatTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${zoneTicketsCatTicket.baseTicketOneHistory ne null and zoneTicketsCatTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${zoneTicketsCatTicket.baseTicketOne}"
						onClick="popupZoneTicketsTMATTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.baseTicketOne}','${zoneTicketsCatTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${zoneTicketsCatTicket.baseTicketTwoHistory ne null and zoneTicketsCatTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${zoneTicketsCatTicket.baseTicketTwo}"
						onClick="popupZoneTicketsTMATTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.baseTicketTwo}','${zoneTicketsCatTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${zoneTicketsCatTicket.baseTicketThreeHistory ne null and zoneTicketsCatTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${zoneTicketsCatTicket.baseTicketThree}"
						onClick="popupZoneTicketsTMATTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.baseTicketThree}','${zoneTicketsCatTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
	<c:if test="${empty zoneTicketsProcessorCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	<%-- <div id="fragment-manhattan-zonesPricingcats">
   		<c:if test="${not empty manhattanZonesPricingCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${manhattanZonesPricingCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="manhattanZonesPricingTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${manhattanZonesPricingTicket.section}</display:column>
			<display:column title="Quantity" sortable="true">${manhattanZonesPricingTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${manhattanZonesPricingTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${manhattanZonesPricingTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${manhattanZonesPricingTicket.tnPrice}</display:column>
			<display:column title="FloorCap" sortable="true">${manhattanZonesPricingTicket.floorCap}</display:column>
			<display:column title="Vivid Price" sortable="true">${manhattanZonesPricingTicket.vividPrice}</display:column>
			 <display:column title="TickPick Price" sortable="true">${manhattanZonesPricingTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${manhattanZonesPricingTicket.scoreBigPrice}</display:column>
			<display:column title="Status" sortable="true">${manhattanZonesPricingTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${manhattanZonesPricingTicket.id}"
				onClick="popupManhattanZonesPricingCatsTicketInfo('${manhattanZonesPricingTicket.eventId}','${manhattanZonesPricingTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${manhattanZonesPricingTicket.id}"
				onClick="popupManhattanZonesPricingTMATTicketInfo('${manhattanZonesPricingTicket.eventId}','${manhattanZonesPricingTicket.ticketId}','${manhattanZonesPricingTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${manhattanZonesPricingTicket.baseTicketOneHistory ne null and manhattanZonesPricingTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${manhattanZonesPricingTicket.baseTicketOne}"
						onClick="popupManhattanZonesPricingTMATTicketInfo('${manhattanZonesPricingTicket.eventId}','${manhattanZonesPricingTicket.baseTicketOne}','${manhattanZonesPricingTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${manhattanZonesPricingTicket.baseTicketTwoHistory ne null and manhattanZonesPricingTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${manhattanZonesPricingTicket.baseTicketTwo}"
						onClick="popupManhattanZonesPricingTMATTicketInfo('${manhattanZonesPricingTicket.eventId}','${manhattanZonesPricingTicket.baseTicketTwo}','${manhattanZonesPricingTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${manhattanZonesPricingTicket.baseTicketThreeHistory ne null and manhattanZonesPricingTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${manhattanZonesPricingTicket.baseTicketThree}"
						onClick="popupManhattanZonesPricingTMATTicketInfo('${manhattanZonesPricingTicket.eventId}','${manhattanZonesPricingTicket.baseTicketThree}','${manhattanZonesPricingTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
	<c:if test="${empty manhattanZonesPricingCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<%-- <div id="fragment-viplastrowminicats">
   		<c:if test="${not empty vipLastRowMiniCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${vipLastRowMiniCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="vipLastRowMiniTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${vipLastRowMiniTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${vipLastRowMiniTicket.rowRange}</display:column>
			<display:column title="Last_Row" sortable="true">${vipLastRowMiniTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${vipLastRowMiniTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${vipLastRowMiniTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${vipLastRowMiniTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${vipLastRowMiniTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${vipLastRowMiniTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${vipLastRowMiniTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${vipLastRowMiniTicket.scoreBigPrice}</display:column>
			<display:column title="FanXchange Price" sortable="true">${vipLastRowMiniTicket.fanxchangePrice}</display:column>
			<display:column title="Status" sortable="true">${vipLastRowMiniTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${vipLastRowMiniTicket.id}"
				onClick="popupVipLastRowCatsTicketInfo('${vipLastRowMiniTicket.eventId}','${vipLastRowMiniTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${lastRowMiniTicket.id}"
				onClick="popupVipLastRowTMATTicketInfo('${vipLastRowMiniTicket.eventId}','${vipLastRowMiniTicket.ticketId}','${vipLastRowMiniTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${vipLastRowMiniTicket.baseTicketOneHistory ne null and vipLastRowMiniTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${vipLastRowMiniTicket.baseTicketOne}"
						onClick="popupVipLastRowTMATTicketInfo('${vipLastRowMiniTicket.eventId}','${vipLastRowMiniTicket.baseTicketOne}','${vipLastRowMiniTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${vipLastRowMiniTicket.baseTicketTwoHistory ne null and vipLastRowMiniTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${lastRowMiniTicket.baseTicketTwo}"
						onClick="popupVipLastRowTMATTicketInfo('${vipLastRowMiniTicket.eventId}','${vipLastRowMiniTicket.baseTicketTwo}','${vipLastRowMiniTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${vipLastRowMiniTicket.baseTicketThreeHistory ne null and vipLastRowMiniTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${lastRowMiniTicket.baseTicketThree}"
						onClick="popupVipLastRowTMATTicketInfo('${vipLastRowMiniTicket.eventId}','${vipLastRowMiniTicket.baseTicketThree}','${vipLastRowMiniTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty vipLastRowMiniCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<%-- <div id="fragment-presaleZoneTicketsCats">
   		<c:if test="${not empty presaleZoneTicketsCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${presaleZoneTicketsCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="zoneTicketsCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${zoneTicketsCatTicket.section}</display:column>
			<display:column title="Quantity" sortable="true">${zoneTicketsCatTicket.quantity}</display:column>
			<display:column title="Priority" sortable="true">${zoneTicketsCatTicket.priority}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${zoneTicketsCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${zoneTicketsCatTicket.actualPrice}</display:column>
			<display:column title="ZoneTicket Prices" sortable="true">${zoneTicketsCatTicket.zoneTicketPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${zoneTicketsCatTicket.vividPrice}</display:column>
			 <display:column title="TickPick Price" sortable="true">${zoneTicketsCatTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${zoneTicketsCatTicket.scoreBigPrice}</display:column>
			<display:column title="Status" sortable="true">${zoneTicketsCatTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${zoneTicketsCatTicket.reason ne null}">${zoneTicketsCatTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${zoneTicketsCatTicket.id}"
				onClick="popupPresaleZoneTicketsCatsTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${zoneTicketsCatTicket.id}"
				onClick="popupPresaleZoneTicketsTMATTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.ticketId}','${zoneTicketsCatTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${zoneTicketsCatTicket.baseTicketOneHistory ne null and zoneTicketsCatTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${zoneTicketsCatTicket.baseTicketOne}"
						onClick="popupPresaleZoneTicketsTMATTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.baseTicketOne}','${zoneTicketsCatTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${zoneTicketsCatTicket.baseTicketTwoHistory ne null and zoneTicketsCatTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${zoneTicketsCatTicket.baseTicketTwo}"
						onClick="popupPresaleZoneTicketsTMATTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.baseTicketTwo}','${zoneTicketsCatTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${zoneTicketsCatTicket.baseTicketThreeHistory ne null and zoneTicketsCatTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${zoneTicketsCatTicket.baseTicketThree}"
						onClick="popupPresaleZoneTicketsTMATTicketInfo('${zoneTicketsCatTicket.eventId}','${zoneTicketsCatTicket.baseTicketThree}','${zoneTicketsCatTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
	<c:if test="${empty presaleZoneTicketsCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<%-- <div id="fragment-presaleAutocats">
   		<c:if test="${not empty presaleAutoCatCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${presaleAutoCatCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="autoCatTicket"  requestURI="#" class="list">
			
			
			<display:column title="Section" sortable="true">${autoCatTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${autoCatTicket.rowRange}</display:column>
			<display:column title="Quantity" sortable="true">${autoCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${autoCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${autoCatTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${autoCatTicket.tnPrice}</display:column>
			<display:column title="Status" sortable="true">${autoCatTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${autoCatTicket.id}"
				onClick="popupPresaleAutoCatsTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${autoCatTicket.id}"
				onClick="popupPresaleAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.ticketId}','${autoCatTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${autoCatTicket.baseTicketOneHistory ne null and autoCatTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${autoCatTicket.baseTicketOne}"
						onClick="popupPresaleAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.baseTicketOne}','${autoCatTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${autoCatTicket.baseTicketTwoHistory ne null and autoCatTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${autoCatTicket.baseTicketTwo}"
						onClick="popupPresaleAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.baseTicketTwo}','${autoCatTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${autoCatTicket.baseTicketThreeHistory ne null and autoCatTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${autoCatTicket.baseTicketThree}"
						onClick="popupPresaleAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.baseTicketThree}','${autoCatTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
	<c:if test="${empty presaleAutoCatCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	
	<div id="fragment-above96Cats">
   		<c:if test="${not empty above96CatsCategoryTicketTickets}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${above96CatsCategoryTicketTickets}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="above96CatsTicket"  requestURI="#" class="list">
			
			<display:column title="Broker" sortable="true">${above96CatsTicket.brokerName}</display:column>
			<display:column title="Product Name" sortable="true">${above96CatsTicket.internalNotes}</display:column>
			<display:column title="Section" sortable="true">${above96CatsTicket.section}</display:column>
			<display:column title="Row" sortable="true">${above96CatsTicket.catRow}</display:column>
			<display:column title="Quantity" sortable="true">${above96CatsTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${above96CatsTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${above96CatsTicket.actualPrice}</display:column>
			<display:column title="Tn Prices" sortable="true">${above96CatsTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${above96CatsTicket.vividPrice}</display:column>
			 <%--<display:column title="TickPick Price" sortable="true">${zoneTicketsCatTicket.tickpickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${zoneTicketsCatTicket.scoreBigPrice}</display:column> --%>
			<display:column title="Status" sortable="true">${above96CatsTicket.status}</display:column>
			<display:column title="Reason" sortable="true">
				<c:if test="${above96CatsTicket.reason ne null}">${above96CatsTicket.reason}</c:if>
			</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${above96CatsTicket.id}"
				onClick="popupAutoCats96CatsTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${above96CatsTicket.id}"
				onClick="popupAutoCats96TMATTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.ticketId}','${above96CatsTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${above96CatsTicket.baseTicketOneHistory ne null and above96CatsTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${above96CatsTicket.baseTicketOne}"
						onClick="popupAutoCats96TMATTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.baseTicketOne}','${above96CatsTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${above96CatsTicket.baseTicketTwoHistory ne null and above96CatsTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${above96CatsTicket.baseTicketTwo}"
						onClick="popupAutoCats96TMATTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.baseTicketTwo}','${above96CatsTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${above96CatsTicket.baseTicketThreeHistory ne null and above96CatsTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${above96CatsTicket.baseTicketThree}"
						onClick="popupAutoCats96TMATTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.baseTicketThree}','${above96CatsTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
	<c:if test="${empty above96CatsCategoryTicketTickets}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	<%-- <div id="fragment-below96Cats">
   		<c:if test="${not empty below96CatsCategoryTicketTickets}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${below96CatsCategoryTicketTickets}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="above96CatsTicket"  requestURI="#" class="list">
			
			<display:column title="Broker" sortable="true">${above96CatsTicket.brokerName}</display:column>
			<display:column title="Product Name" sortable="true">${above96CatsTicket.internalNotes}</display:column>
			<display:column title="Section" sortable="true">${above96CatsTicket.section}</display:column>
			<display:column title="Row" sortable="true">${above96CatsTicket.catRow}</display:column>
			<display:column title="Quantity" sortable="true">${above96CatsTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${above96CatsTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${above96CatsTicket.actualPrice}</display:column>
			<display:column title="Tn Prices" sortable="true">${above96CatsTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${above96CatsTicket.vividPrice}</display:column>
			<display:column title="Status" sortable="true">${above96CatsTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${above96CatsTicket.id}"
				onClick="popupAutoCats96CatsTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${above96CatsTicket.id}"
				onClick="popupAutoCats96TMATTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.ticketId}','${above96CatsTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${above96CatsTicket.baseTicketOneHistory ne null and above96CatsTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${above96CatsTicket.baseTicketOne}"
						onClick="popupAutoCats96TMATTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.baseTicketOne}','${above96CatsTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${above96CatsTicket.baseTicketTwoHistory ne null and above96CatsTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${above96CatsTicket.baseTicketTwo}"
						onClick="popupAutoCats96TMATTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.baseTicketTwo}','${above96CatsTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${above96CatsTicket.baseTicketThreeHistory ne null and above96CatsTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${above96CatsTicket.baseTicketThree}"
						onClick="popupAutoCats96TMATTicketInfo('${above96CatsTicket.eventId}','${above96CatsTicket.baseTicketThree}','${above96CatsTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
	<c:if test="${empty below96CatsCategoryTicketTickets}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	<br/>
	<br/>
	</div>
</c:if>
	<input type="hidden" name="action" id="action"/>
</form:form>



<script type="text/javascript">

function fetchEvents(selObj) {
	var obj = eval(selObj);
	var text1 = obj.options[obj.selectedIndex].text;
	alert(text1);
	alert(selObj.value);	
	$("#formId").submit();		  
}

function submitVenue(selObj) {
	var obj = eval(selObj);
	$("#venueId").val(obj.value);
	var venueName = obj.options[obj.selectedIndex].text;
	$('#eventId').prop('selectedIndex',0);
	//$("#hidselectedVenueName").val(venueName);	
	$("#formId").submit();
	
}

//Added Lloyd
function callChangeArtist(){
	$('#artistLabelSpanId').hide();
	$('#artistSelectSpanId').show();	
	$('#txtartistId').val('');
	$('#artistId').val('');
	$('#hidselectedArtistName').val('');
	$("#hidselectedVenueName").val('');
	$('#venueId').val('');
	$('#eventId').prop('selectedIndex',0);
	$("#formId").submit();	
	
}

function callChangeVenue(){
	$('#venueLabelSpanId').hide();
	$('#venueSelectSpanId').show();	
	$('#txtvenueId').val('');
	$('#venueId').val('');
	$("#hidselectedVenueName").val('');
	$('#eventId').prop('selectedIndex',0);
	$("#formId").submit();
	
}

function getVenueBasedOnArtist(){	
	var artistId = $('#artistId').val();	
	var url = "AutoCompleteVenuesForArtistWithActiveEvents?q="+artistId;
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){			
			var jsonData = JSON.parse(res);			
			$('#dropDownvenueId').empty();			
			var rowText = "<option value="+">---Select---</option>"
			$('#dropDownvenueId').append(rowText);
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				rowText = "<option value="+data.id+ ">"+ data.building + ", " + data.city + ", " + data.state + ", " + data.country +   " </option>";
				$('#dropDownvenueId').append(rowText);
            }	
		}
	}); 
}


//Added Lloyd

$(document).ready(function () {	
		
	$( "#row" ).change(function() {
	  var section=$("#row").val();
	  $("#row").show();
	});
	
	
	$( "#section" ).change(function() {
		  var section=$("#section").val();
		  $("#section").show();
		});
	
	$( "#quantity" ).change(function() {
		  var section=$("#quantity").val();
		  $("#quantity").show();
		});
	
		
	
	 var prevSelectedArtistName = document.getElementById("hidselectedArtistName").value;	
	 if(null == prevSelectedArtistName || prevSelectedArtistName == ""){
		 $('#artistLabelSpanId').hide();
		 $('#artistSelectSpanId').show();
	 }
	 else{
		 $('#artistLabelSpanId').show();
		 $('#artistSelectSpanId').hide();		 
	 }	
	 
	 
	 var prevSelectedVenueName = document.getElementById("hidselectedVenueName").value;	
	 if(null == prevSelectedVenueName || prevSelectedVenueName == ""){		 
		// selectDropDownVenue venueSelectSpanId venueLabelSpanId
		 
		 $('#venueLabelSpanId').hide();
		 $('#selectDropDownVenue').hide();
		 $('#venueSelectSpanId').show();
	 }
	 else{
		 $('#venueLabelSpanId').show();
		 $('#venueSelectSpanId').hide();
		 $('#selectDropDownVenue').hide();		 
	 }
	
	 if(prevSelectedArtistName.length >0 && prevSelectedVenueName.length <=0 )	
	  {	
		
		     $('#venueLabelSpanId').hide();
			 $('#selectDropDownVenue').show();
			 $('#venueSelectSpanId').hide();
	  }
	 
	 
	 
	
	
	 $('#txtartistId').autocomplete("AutoCompleteArtistWithEvents", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" +  row[2]  ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){		  
			$('#artistId').val('');
			$('#selectedValue').text(row[2]);			
			if(row[0]=='ARTIST'){
				$('#selectedOption').text('Artist');				
				$("#txtartistId").val(row[2]);
				$("#hidselectedArtistName").val(row[2]);
				$("#labelArtistName").text(row[2]);
				$("#artistId").val(row[1]);	
				$('#artistSelectSpanId').hide();
				$('#artistLabelSpanId').show();
				$('#eventId').prop('selectedIndex',0);				
				$('#venueId').val('');
				$("#hidselectedVenueName").val('');
				$("#formId").submit();				
				
			} 
	}); 
	 
	 

	 $('#txtvenueId').autocomplete("AutoCompleteVenueWithEvents", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",		
		formatItem: function(row, i, max) {
			if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" +  row[2]  ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){		  
			$('#venueId').val('');
			$('#selectedValue').text(row[2]);			
			if(row[0]=='VENUE'){
				$('#selectedOption').text('Venue');				
				$("#txtvenueId").val(row[2]);
				$("#hidselectedVenueName").val(row[2]);
				$("#labelVenueName").text(row[2]);
				$("#venueId").val(row[1]);	
				$('#venueSelectSpanId').hide();
				$('#venueLabelSpanId').show();
				$('#eventId').prop('selectedIndex',0);
				$("#formId").submit();			
									
			} 
	});	
	
	
	
	});

$('#container-tabs').tabs({
    select: function(event, ui) {
      var url = ui.tab.href;
      var posAnchor = url.indexOf('#');
  	//anchor = url.substr(posAnchor, url.length - posAnchor);
  	anchor = "";   
  	
      if (url.indexOf('#fragment-chart') > 0) {
        $('#chartDiv').css('opacity', 1);
        $('#chartDiv').css('height', 400);
	  } else {
	    $('#chartDiv').css('opacity', 0);
        $('#chartDiv').css('height', 1);
	  }
    }
    });
    
if (location.href.indexOf('#fragment-chart') > 0) {
    $('#chartDiv').css('height', 400);
    $('#chartDiv').css('opacity', 1);
}  else {
    $('#chartDiv').css('opacity', 0);
    $('#chartDiv').css('height', 1);
}

function changeFilter() {
	
	$("#formId").submit();
}


function submitForms(){
	
	var eventId = $("#eventId").val(); 
	if(eventId == '') {
		alert('Please select valid event.');
		return false;
	}
	
	$("#formId").submit();
	
}
function popupTGCatsTicketInfo(eId,tId){
	var url = "CategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus();}
	return false;
	} 
function popupTgcatTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "TMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus();}
	return false;
	}
	
function popupSGLastFiveRowTicketInfo(eId,tId){
	var url = "SGLastFiveRowCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus();}
	return false;
	} 
function popupSGLastFiveRowTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "SGLastFiveRowTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus();}
	return false;
	}

function popupAutoCatsTicketInfo(eId,tId){
	var url = "AutoCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupAutoTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "AutoTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupVipCatsTicketInfo(eId,tId){
	var url = "VipCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupVipTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "VipTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupLastFiveRowCatsTicketInfo(eId,tId){
	var url = "LastFiveRowCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupLastFiveRowTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "LastFiveRowTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}


function popupLastRowCatsTicketInfo(eId,tId){
	var url = "LastRowCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupLastRowTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "LastRowTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupLarryLastCatsTicketInfo(eId,tId){
	var url = "LarryLastCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupLarryLastTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "LarryLastTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupVipAutoCatsTicketInfo(eId,tId){
	var url = "VipAutoCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupVipAutoTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "VipAutoTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	

function popupZonedLastRowCatsTicketInfo(eId,tId){
	var url = "ZonedLastrowMiniCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupZonedLastRowTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "ZonedLastrowMiniTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupZonesPricingCatsTicketInfo(eId,tId){
	var url = "ZonesPricingCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupZonesPricingTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "ZonesPricingTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupTixCityZonesPricingCatsTicketInfo(eId,tId){
	var url = "TixCityZonesPricingCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupTixCityZonesPricingTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "TixCityZonesPricingTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupZoneTicketsCatsTicketInfo(eId,tId){
	var url = "ZoneTicketsCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
	
function popupPresaleZoneTicketsCatsTicketInfo(eId,tId){
	var url = "PresaleZoneTicketsCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
	
function popupZoneTicketsTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "ZoneTicketsTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	
function popupPresaleZoneTicketsTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "PresaleZoneTicketsTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	
function popupPresaleAutoCatsTicketInfo(eId,tId){
	var url = "PresaleAutoCatsCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupPresaleAutoTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "PresaleAutoCatsTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}


function popupEbayCatsTicketInfo(eId,tId){
	var url = "EbayCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupEbayTMATTicketInfo(eId,ticketId){
	var url = "EbayTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupEbayTMATBaseTicket1Info(eId,ticketId){
	var url = "EbayTMATBaseTicket1PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupEbayTMATBaseTicket2Info(eId,ticketId){
	var url = "EbayTMATBaseTicket2PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	
function popupAutoCats96CatsTicketInfo(eId,tId){
	var url = "AutoCats96CatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	
function popupAutoCats96TMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "AutoCats96TMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	
function popupManhattanZonesPricingCatsTicketInfo(eId,tId){
	var url = "ManhattanZonesPricingCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	
function popupManhattanZonesPricingTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "ManhattanZonesPricingTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	
function popupVipLastRowCatsTicketInfo(eId,tId){
	var url = "VipLastRowCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupVipLastRowTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "VipLastRowTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
</script>		