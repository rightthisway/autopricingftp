<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">


</script>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> > Manage AutoPricing Brokers
</div>
<h1>Manage AutoPricing Brokers</h1>
<br/>
<hr/>
<form:form action="ManageAutoPricingBroker" method="post" id="formId">
<input type="hidden" name="action" id="action"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	<div id="gridTable" align="center" >

		<display:table list="${brokerList}" style="width:90%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="broker"  requestURI="#" class="list">

		<display:column title="Name" sortable="true">${broker.name}</display:column>
		<display:column title="EmailId" sortable="true">${broker.emailId}</display:column>
		<display:column title="SMTP Email Suffix" sortable="true">${broker.emailSuffix}</display:column>
		<display:column title="PhoneNo" sortable="true">${broker.phoneNo}</display:column>
		<display:column title="POS Broker Id" sortable="true">${broker.posBrokerId}</display:column>
		<display:column title="POS System User Id" sortable="true">${broker.systemUserId}</display:column>
		<display:column title="POS Buyer Broker Id" sortable="true">${broker.buyerBrokerId}</display:column>
		<display:column title="POS Client Broker Id" sortable="true">${broker.clientBrokerId}</display:column>
		<display:column title="POS Client Broker Employee Id" sortable="true">${broker.clientBrokerEmployeeId}</display:column>
		<display:column title="POS Minute Update Limit" sortable="true">${broker.minutePOSUpdateLimit}</display:column>
		<display:column title="Edit" >
			<a href="EditAutoPricingBroker?brokerId=${broker.id}">Edit</a>
		</display:column> 
		
		<%-- <display:column title="Action" >
			<input type="button" value="Edit" class="medButton" id="edit_${broker.id}" onClick="popupBrokerProductManageAudit('${broker.id}');"/>
		</display:column> --%>
		
		
		</display:table>
		<br />	
	</div>	
	<br />

</form:form>