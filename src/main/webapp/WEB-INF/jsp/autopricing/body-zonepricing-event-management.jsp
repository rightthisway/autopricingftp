<%@include file="/WEB-INF/jsp/taglibs.jsp"%>


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Manage AutoPricing
</div>
<h1>Manage AutoPricing</h1>
<br/>
<hr/>

<form:form action="${url}" method="post" id="formId">
<c:forEach items="${products}" var="product">
	<input type="checkbox" name="product-${product.id}" id="product-${product.id}"/> ${product.name}
</c:forEach>
<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br/>
	<br/>
	<table width="80%">
		<tr>
			<td><b>Select Broker:</b></td>
			<td>
				<select id="brokerId" name="brokerId">
					<option value="">---ALL---</option>
					<c:forEach items="${brokers}" var="brokerObj">
						<option value="${brokerObj.id }"
						<c:if test="${broker ne null and brokerObj.id eq broker}"> Selected </c:if>
						>${brokerObj.name}</option>
					</c:forEach>
				</select>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Select Child or GrandChild or Artist or Venue:</b></td>
			<td>
				<input type="hidden" id="artist" name="artist" value ="${artist}">
				<input type="hidden" id="venue" name="venue" value ="${venue}">
				<input type="hidden" id="grandChild" name="grandChild" value ="${grandChild}">
				<input type="hidden" id="brokerId" name="brokerId" value ="${brokerId}">
				<input type="text" id="autoArtistVenue" name="autoArtistVenue">
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b><span id="selectedOption" >${selectedOption}</span></b></td>
			<td><b><span id="selectedValue" >${selectedValue}</span></b></td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Events:</b></td>
			<td >
				<input type="hidden" id="eventStr" name="eventStr" value = ${eventStr}/> 
				<input type="checkbox" id="eventsCheckAll" name="eventsCheckAll" onclick ="selectAllEvents()" <c:if test ="${eventsCheckAll}"> checked="checked" </c:if>/> 
				<label for="eventsCheckAll">Select All</label>
				<br/>
				<select id='events' name="events"  multiple="multiple" size='6' style="width:480px;" >
					<c:forEach items="${events}" var="event">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${event.id}${temp}"/>
						<option 
							<c:if test="${event!=null && fn:contains(eventStr,temp2)}"> Selected </c:if>
							value="${event.id}"> ${event.name} <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								${event.venue.building}
						</option>
					</c:forEach> 
				</select>
			</td>
			<td></td>
			<td colspan="3" align="center"></td>
			
		</tr>
		<tr>
			<td colspan="6" align="center">
				<input type="button" value="Get Pricing" onclick="submitForms('search');" class="medButton">
			</td>
		</tr>
	</table>
	
	<c:if test="${not empty zonepricingExchangeEventList}" >
	<div id="ebayInventoryTabel" >
	
		
		<div style="float:right; padding-right:320px;">
			<input type="button" value="Update"  name="update1" id="update1" class="medButton" onclick="javascript:submitForms('update');"/> 
		</div>
		<div id="copyCheckBox" style="float:right;">
			<!--<input onclick="javascript:copyAllEventData();" type="checkbox" name="copyAllCheckbox" id="copyAllCheckbox" >Copy first row  to all remaining rows &nbsp;&nbsp;&nbsp; -->
		</div>
		<br/>
		<br/>
		<br/>
		<display:table list="${zonepricingExchangeEventList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="exchangeEvent"  requestURI="#" class="list">
			<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
				<input type="checkbox" name="checkbox_${exchangeEvent.event.id}" id="checkbox_${exchangeEvent.event.id}" <c:if test ="${exchangeEvent.lowerMarkup !=null  }"> checked="checked" </c:if> class="selectCheck" />
			</display:column>
			<display:column title="EvenName" sortable="true">${exchangeEvent.event.name}</display:column>
			<display:column title="Event Date And Time" sortable="true">

				<fmt:formatDate pattern="MM/dd/yyyy" value="${exchangeEvent.event.localDate}" />				
					<c:choose>
						<c:when test="${exchangeEvent.event.localTime == null}">
							TBD
						</c:when>
						<c:otherwise>
							<fmt:formatDate pattern="hh:mm aa" value="${exchangeEvent.event.localTime}" />
						</c:otherwise>
					</c:choose>
			</display:column>
			<display:column title="Venue" sortable="true">${exchangeEvent.event.venue.building},${exchangeEvent.event.venue.state},${exchangeEvent.event.venue.city},${exchangeEvent.event.venue.country}</display:column>
			<display:column title='PreSale<br/><input type="checkbox" name="copyAllSectionRange" id="copyAllSectionRange" >' >
				<input type="checkbox" name="allowSectionRange_${exchangeEvent.event.id}" id="allowSectionRange_${exchangeEvent.event.id}" 
				onchange="selectRow('${exchangeEvent.event.id}');" <c:if test ="${exchangeEvent.allowSectionRange !=null && exchangeEvent.allowSectionRange}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='Exposure<br/><input type="checkbox" name="copyAllAutoExposure" id="copyAllAutoExposure" >' >
				<select name="exposure_${exchangeEvent.event.id}" id="exposure_${exchangeEvent.event.id}" class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${exchangeEvent.exposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${exchangeEvent.exposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${exchangeEvent.exposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			
			<display:column title='Shipping Method<br/><input type="checkbox" name="copyAllShippingMethod" id="copyAllShippingMethod" >' >
				<select name="shipping_method_${exchangeEvent.event.id}" id="shipping_method_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- Select ---</option> 
					<option value="0" <c:if test ="${exchangeEvent.shippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${exchangeEvent.shippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${exchangeEvent.shippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${exchangeEvent.shippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${exchangeEvent.shippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${exchangeEvent.shippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${exchangeEvent.shippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${exchangeEvent.shippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${exchangeEvent.shippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${exchangeEvent.shippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
					
				</select>
			</display:column> 
			
			<display:column title='Near Term Display Option<br/><input type="checkbox" name="copyAllNearTermDisplayOption" id="copyAllNearTermDisplayOption" >' >
				<select name="near_term_display_option_${exchangeEvent.event.id}" id="near_term_display_option_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option> 
					<option value="0" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
			</display:column>
			
			<display:column title='Shipping Days<br/><input type="checkbox" name="copyShippingDays" id="copyShippingDays" >' >
				<select name="shipping_days_${exchangeEvent.event.id}" id="shipping_days_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- Select ---</option> 
						<option value="0" <c:if test ="${exchangeEvent.shippingDays eq '0'}" > selected </c:if> >0 Days</option> 
					<option value="2" <c:if test ="${exchangeEvent.shippingDays eq '2'}" > selected </c:if> >2 Days</option>
					<option value="4" <c:if test ="${exchangeEvent.shippingDays eq '4'}" > selected </c:if> >4 Days</option>
				</select>
			</display:column>
			
			<display:column title='RPT Factor<br/><input type="checkbox" name="copyAllRptFactor" id="copyAllRptFactor" >' >
				<input type="text" value="${exchangeEvent.rptFactor}" name="rptFactor_${exchangeEvent.event.id}" id="rptFactor_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Price Breakup<br/><input type="checkbox" name="copyAllPriceBreakup" id="copyAllPriceBreakup" >' >
				<input type="text" value="${exchangeEvent.priceBreakup}" name="priceBreakup_${exchangeEvent.event.id}" id="priceBreakup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Lower Markup<br/><input type="checkbox" name="copyAllLowerMarkup" id="copyAllLowerMarkup" >' >
				<input type="text" value="${exchangeEvent.lowerMarkup}" name="lowerMarkup_${exchangeEvent.event.id}" id="lowerMarkup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Upper Markup<br/><input type="checkbox" name="copyAllUpperMarkup" id="copyAllUpperMarkup" >' >
				<input type="text" value="${exchangeEvent.upperMarkup}" name="upperMarkup_${exchangeEvent.event.id}" id="upperMarkup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Lower Shipping Fees<br/><input type="checkbox" name="copyAllLowerShippingFees" id="copyAllLowerShippingFees" >' >
				<input type="text" value="${exchangeEvent.lowerShippingFees}" name="lowerShippingFees_${exchangeEvent.event.id}" id="lowerShippingFees_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Upper Shipping Fees<br/><input type="checkbox" name="copyAllUpperShippingFees" id="copyAllUpperShippingFees" >' >
				<input type="text" value="${exchangeEvent.upperShippingFees}" name="upperShippingFees_${exchangeEvent.event.id}" id="upperShippingFees_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='TicketNetwork Event<br/><input type="checkbox" name="copyAllTicketNetworkEvent" id="copyAllTicketNetworkEvent" >' >
				<input type="checkbox" name="isTicketNetwork_${exchangeEvent.event.id}" id="isTicketNetwork_${exchangeEvent.event.id}" <c:if test ="${exchangeEvent.isTicketNetwork!=null && exchangeEvent.isTicketNetwork}"> checked="checked" </c:if> />
			</display:column>
			<display:column title='VividSeats Event<br/><input type="checkbox" name="copyAllVividSeatsEvent" id="copyAllVividSeatsEvent" >' >
				<input type="checkbox" name="isVivid_${exchangeEvent.event.id}" id="isVivid_${exchangeEvent.event.id}" <c:if test ="${exchangeEvent.isVivid!=null && exchangeEvent.isVivid}"> checked="checked" </c:if> />
			</display:column>
			<display:column title='ScoreBig Event<br/><input type="checkbox" name="copyAllScoreBigEvent" id="copyAllScoreBigEvent" >' >
				<input type="checkbox" name="isScoreBig_${exchangeEvent.event.id}" id="isScoreBig_${exchangeEvent.event.id}" <c:if test ="${exchangeEvent.isScoreBig!=null && exchangeEvent.isScoreBig}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='Broker Event Status' >
				${exchangeEvent.event.brokerStatus}
			</display:column>
			
			<display:column title="Action" >
			 	<input type="button" value="Audit" class="medButton" id="audit_${exchangeEvent.event.id}}"
				onClick="popupUpdateTGCatsPricingAudit('${exchangeEvent.event.id}');"/>
			 	
			 </display:column>
		</display:table>
			
			<br/>
			<div style="float:right; padding-right:320px;">
				<input type="button" value="Update"    class="medButton" id="update2" name="update2"onclick="javascript:submitForms('update');"/> 
			</div>
			</div>
			
	</c:if>
	<c:if test="${empty larryLastExchangeEventList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
		<table id="listAudit"></table>
		<br/>
		<br/>
	</c:if>
	<input type="hidden" name="action" id="action"/>
	<br/>
	<br/>
	
</form:form>
<script type="text/javascript">
var otherFrequentEventsCount = 0;
if('${otherFrequentEventsCount}' != null && '${otherFrequentEventsCount}' != '') {
	otherFrequentEventsCount = '${otherFrequentEventsCount}';
}


$(document).ready(function(){
	var allEvents = '${eventsCheckAll}';
	var isUpdate = 'true';
	function selectCheckBox(){
		if(allEvents=='true'){
			$('#eventsCheckAll').attr("checked","checked");
		}
		allEvents='false';
	}
	selectCheckBox();
	$('#autoArtistVenue').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
			$('#autoArtistVenue').val('');
			$('#selectedValue').text(row[2]);
			if(row[0]=='ARTIST'){
				$('#selectedOption').text('Artist');
				$("#artist").val(row[1]);
				$("#grandChild").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('artistId',row[1]);
				
			} else if(row[0]=='VENUE'){
				$('#selectedOption').text('Venue');
				$("#venue").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#child").val('');
				getEvents('venueId',row[1]);
				
			} else if(row[0]=='CHILD'){
				$('#selectedOption').text('Child');
				$("#child").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#venue").val('');
				getEvents('childId',row[1]);
				
			} else {
				$('#selectedOption').text('GrandChild');
				$("#grandChild").val(row[1]);
				$("#artist").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('grandChildId',row[1]);
				
			} 
	});

	$("#events").change(function () {
		if(isUpdate!='true'){
			$('#ebayInventoryTabel').hide();
			$("#events option:selected").each(function () {
				$('#eventsCheckAll').removeAttr("checked");
			});
		}
		isUpdate='false'; 
	}).trigger('change');
	
	   
	
});
	
function getEvents(isArtist,id){
	
	var url = "";
	var brokerId = $("#brokerId").val();
	if(isArtist == 'artistId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?artistId="+id+"&brokerId="+brokerId;
	}else if(isArtist == 'venueId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?venueId="+id+"&brokerId="+brokerId;
	} else if(isArtist == 'childId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?childId="+id+"&brokerId="+brokerId;;
	} else{
		url = "GetEventsByGrandChildAndArtistAndVenue?grandChildId="+id+"&brokerId="+brokerId;
	}
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#events').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.name+" "+data.date+" "+data.time+" "+data.venue+"</option>"
				$('#events').append(rowText);
            }	
		}
	}); 
} 
function validateForm(){
		var flag= true;
		$('.selectCheck:checkbox:checked').each(function () {
			//var id = this.id;
			var tn = this.id.replace('checkbox','isTicketNetwork');
			var vivid = this.id.replace('checkbox','isVividSeats');
			var scorebig = this.id.replace('checkbox','isScoreBig');
			var stubhub = this.id.replace('checkbox','isStubhub');
			
			var isTN = false,isVividSeat = false,isScorebig=false;//Currently we are using Stubhub exchange only in LastFiveRowMC project 
			
			if($("#" + tn).attr('checked') ) {
				isTN = true;
				isMinimamOneExchange = true;
			}
			if($("#" + vivid).attr('checked') ) {
				isVividSeat = true;
				isMinimamOneExchange = true;
			} 
			if($("#" + scorebig).attr('checked') ) {
				isScorebig = true;
				isMinimamOneExchange = true;
			}
				
			/* if(!isMinimamOneExchange){
				alert('Please select minimam one exchanges for every event.');
				$("#"+tn).focus();
				flag = false;
				return ;	
			} */
			
			var id,value;
			
			
			id = this.id.replace('checkbox','exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Exposure can not be blank for TicketNetwork Exchange Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			
			id = this.id.replace('checkbox','rptFactor');
			value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid RPT Factor.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','priceBreakup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Price Breakup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerMarkup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperMarkup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
		});
		if(!flag){
			return flag;	
		}
		
		return true;
}
function submitForms(action){
	if(action=='update'){
		if(validateForm()){
			$("#action").val(action);
			$("#formId").submit();
		}
	}else{
		$("#action").val(action);
		$("#formId").submit();
	}
}

$(function () {
	
	$('#copyAllFrequentEvent').click(function(){
		if($('#copyAllFrequentEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isFrequentEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllSectionRange').click(function(){
		if($('#copyAllSectionRange').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('allowSectionRange');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllAutoExposure').click(function(){
		if($('#copyAllAutoExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('auto_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllMiniExposure').click(function(){
		if($('#copyAllMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('mini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllVipMiniExposure').click(function(){
		if($('#copyAllVipMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('vip_mini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	}); 
	
	$('#copyAllVipAutoExposure').click(function(){
		if($('#copyAllVipAutoExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('vip_auto_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	}); 
	
	$('#copyAllLastRowMiniExposure').click(function(){
		if($('#copyAllLastRowMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lastrow_mini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllLastFiveRowMiniExposure').click(function(){
		if($('#copyAllLastFiveRowMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lastfiverow_mini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllZonesPricingExposure').click(function(){
		if($('#copyAllZonesPricingExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('zones_pricing_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllZonedLastRowMiniExposure').click(function(){
		if($('#copyAllZonedLastRowMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('zoned_lastrowmini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllShippingMethod').click(function(){
		if($('#copyAllShippingMethod').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('shipping_method');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});  
	$('#copyAllNearTermDisplayOption').click(function(){
		if($('#copyAllNearTermDisplayOption').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('near_term_display_option'); 
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyShippingDays').click(function(){
		if($('#copyShippingDays').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('shipping_days'); 
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllLowerMarkup').click(function(){
		if($('#copyAllLowerMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lowerMarkup');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllUpperMarkup').click(function(){
		if($('#copyAllUpperMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('upperMarkup');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllLowerShippingFees').click(function(){
		if($('#copyAllLowerShippingFees').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lowerShippingFees');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllUpperShippingFees').click(function(){
		if($('#copyAllUpperShippingFees').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('upperShippingFees');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllRptFactor').click(function(){
		if($('#copyAllRptFactor').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('rptFactor');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllPriceBreakup').click(function(){
		if($('#copyAllPriceBreakup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('priceBreakup');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	
	$('#copyAllTicketNetworkEvent').click(function(){
		if($('#copyAllTicketNetworkEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isTicketNetwork');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllVividSeatsEvent').click(function(){
		if($('#copyAllVividSeatsEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isVivid');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllScoreBigEvent').click(function(){
		if($('#copyAllScoreBigEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isScoreBig');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllLarryMiniTNEvent').click(function(){
		if($('#copyAllLarryMiniTNEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isLarryMiniTNEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllStubhubEvent').click(function(){
		if($('#copyAllStubhubEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isStubhubEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllZonesPricingTNEvent').click(function(){
		if($('#copyAllZonesPricingTNEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isZonesPricingTNEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllZonedLastRowMiniTNEvent').click(function(){
		if($('#copyAllZonedLastRowMiniTNEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isZonedLastRowMiniTNEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAll').click(function(){
		copyAll();
	});
});

function copyAll(){
	if($('#copyAll').attr('checked')){
		$('.selectCheck').attr('checked', true);
		copyCheckBox('isFrequentEvent');
		copyCheckBox('allowSectionRange');
		//copyTextField('exposure');
		copyTextField('auto_exposure');
		copyTextField('mini_exposure');
		copyTextField('vip_mini_exposure');
		copyTextField('vip_auto_exposure');
		copyTextField('lastrow_mini_exposure');
		copyTextField('lastfiverow_mini_exposure');
		copyTextField('zones_pricing_exposure');
		copyTextField('zoned_lastrowmini_exposure');
		
		copyTextField('shipping_method');
		copyTextField('near_term_display_option'); 
		copyTextField('shipping_days');    
		
		copyTextField('lowerMarkup');
		copyTextField('upperMarkup');
		copyTextField('lowerShippingFees');
		copyTextField('upperShippingFees');
		copyTextField('rptFactor');
		copyTextField('priceBreakup');
		
		copyCheckBox('isTicketNetwork');
		copyCheckBox('isVivid');
		//copyCheckBox('isTickPickEvent');
		//copyCheckBox('isEbayEvent');
		
		copyCheckBox('isScoreBig');
		copyCheckBox('isLarryMiniTNEvent');
		copyCheckBox('isStubhubEvent');
		copyCheckBox('isZonesPricingTNEvent');
		copyCheckBox('isZonedLastRowMiniTNEvent');
		
	}else{
		$('.selectCheck').attr('checked', false);
	}
}
function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}
function selectRow(id) {
	$("#checkbox_" + id ).attr('checked', true);
}

function selectAllEvents(){
	if((document.getElementById("eventsCheckAll").checked)){
		$("#events").each(function(){
			$("#events option").attr("selected","selected"); 
		});

	}
	else{
			$("#events").each(function(){
			$("#events option").removeAttr("selected"); 
		});
	}
}

function popupUpdateTGCatsPricingAudit(eId){
	var url = "LarryLastExchangeEventAuditPopup?eventId="+eId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
} 
	
</script>