
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

$(function () {
	$(function() {
	    $( "#fromDate" ).datepicker({}).datepicker("setDate", new Date());
	  });
	
	$(function() {
	    $( "#toDate" ).datepicker({}).datepicker("setDate", new Date());
	  });
	
	 $("#isAllBroker").click(function() {
		if((document.getElementById("isAllBroker").checked)){
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected","selected");				
			});	
		} else {
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllProduct").click(function() {
		if((document.getElementById("isAllProduct").checked)){
			$("#productId").each(function(){
				$("#productId option").attr("selected","selected");				
			});	
		} else {
			$("#productId").each(function(){
				$("#productId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllExchange").click(function() {
		if((document.getElementById("isAllExchange").checked)){
			$("#exchangeId").each(function(){
				$("#exchangeId option").attr("selected","selected");				
			});	
		} else {
			$("#exchangeId").each(function(){
				$("#exchangeId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	$('#copyAllisEnabled').click(function(){
		if($('#copyAllisEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isEnabled');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllExcludeEventDays').click(function(){
		if($('#copyAllExcludeEventDays').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('excludeEventDays');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$( "#brokerId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#productId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#exchangeId" ).change(function() {
		$('#gridTable').hide();
		});
	
	
	/* $('#copyAll').click(function(){
		copyAll();
	}); */
});

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}

	function submitForms(action) {
		if(action=='update'){
			if(validateForm()){
				$("#action").val(action);
				$("#formId").submit();
			}
		}else {
			$("#action").val(action);
			$("#formId").submit();
		}
	}
	
	function validateForm(){
		var flag= true;
		var isMinimamOneRecord = false;
		
		$('.selectCheck:checkbox:checked').each(function () {
			
			isMinimamOneRecord = true;
			
			var id = this.id.replace('checkbox','excludeEventDays');
			var value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Exclude Event Days.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
		});
		
		if(!isMinimamOneRecord) {
			alert('Please Select Minimum one record for Update.');
			flag = false;
			//return ;
		}
		
		if(!flag){
			return flag;	
		}
		return true;
}
	function selectRow(id) {
		$("#checkbox_" + id ).attr('checked', true);
	}
	

	
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > AutoCat Project Audit
</div>
<h1>AutoCat Project Audit</h1>
<a href="DownloadAutocatProjectAudits">Download AutoPricingProductsAudits Report</a>
<br/>
<hr/>
<form:form action="AutocatProjectAudits" method="post" id="formId">
<input type="hidden" name="action" id="action"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	
<table align="center">
<tr>
	<td align="center"><b>Broker :</b> Check  All: <input type="checkbox" name="isAllBroker" <c:if test="${isAllBrokerSelected == 'on'}">checked</c:if> id="isAllBroker"/>
	</td>
	<td align="center"><b>Product :</b> Check  All: <input type="checkbox" name="isAllProduct" <c:if test="${isAllProductSelected == 'on'}">checked</c:if> id="isAllProduct" />
	</td>
</tr>
<tr><td>
<select name="brokerId" id="brokerId" style="width: 300px" multiple size="5" >
 <c:forEach var="broker" items="${brokerList}">
 <c:set var="a1" value="false" />
 <c:forEach var="selected" items="${selectedBrokerId}">
  <c:if test="${broker.id == selected}">
  <c:set var="a1" value="true" />
  </c:if>
  </c:forEach>
    <option value="${broker.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${broker.name}</option>
  </c:forEach>
</select> 
</td>
<td> 
<select name="productId" id="productId" style="width: 300px" multiple size="5">
<c:forEach var="product" items="${productList}">
<c:set var="a2" value="false" />  
  <c:forEach var="selected1" items="${selectedProductId}">
  <c:if test="${product.id == selected1}">
  <c:set var="a2" value="true" />
  </c:if>
  </c:forEach>
    <option value="${product.id}" <c:if test="${a2 == 'true'}">selected</c:if>>${product.name}</option>
  </c:forEach>

</select>  
</td>
</tr>
<tr>
	<td align="center"><b>From Date :</b><input type="text" name="fromDate" id="fromDate"  value="${fromDate}"/> ${fromDate} </td> 

	<td align="center"><b>Hour :</b>
		<select id ="fromHour" name = "fromHour">
			<option value=''>--- Select Hour -- </option>
			<c:forEach var="i" begin="0" end="23">
				<option value='${i}' <c:if test="${fromHour == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
	</td>
	<td align="center"><b>Minute :</b>
		<select id ="fromMinute" name = "fromMinute">
			<option value=''>--- Select Minute -- </option>
			<c:forEach var="i" begin="00" end="50">
				<option value='${i}' <c:if test="${fromMinute == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
	</td>
</tr>
<tr>
	<td align="center"><b> To Date  :</b><input type="text" name="toDate" id="toDate" value="${toDate}"/></td>

	<td align="center"><b>Hour  :</b>
		<select id ="toHour" name = "toHour">
			<option value=''>--- Select Hour -- </option>
			<c:forEach var="i" begin="00" end="23">
				<option value='${i}' <c:if test="${toHour == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
	</td>
	<td align="center"><b>Minute  :</b>
		<select id ="toMinute" name = "toMinute">
			<option value=''>--- Select Minute -- </option>
			<c:forEach var="i" begin="00" end="59">
				<option value='${i}' <c:if test="${toMinute == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
	</td>
</tr>
<tr>
	<td colspan="3" align="center">
	<br />
		<input type="button" value="Get Data" onclick="submitForms('search');" class="medButton">
	</td>
</tr>


</table>

	<br />
	<c:if test="${not empty autoCatsProjectAuditList}" >
	<div id="gridTable" align="center" >

		<display:table list="${autoCatsProjectAuditList}" style="width:60%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="audit"  requestURI="#" class="list">

		<display:column title="Broker" sortable="true">${audit.broker.name}</display:column>
		<display:column title="Product" sortable="true">${audit.product.name}</display:column>
		<display:column title="Count" sortable="true">${audit.count}</display:column>		
		<display:column title="Process" sortable="true">${audit.processType}</display:column>
		<display:column title="lastRunTime" sortable="true">${audit.lastRunTime}</display:column>	
     	</display:table>
	<br />
	</div>	
	<br />
	
	
	</c:if>
	<c:if test="${empty autoCatsProjectAuditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>


</form:form>