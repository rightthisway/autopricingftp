<%@include file="/WEB-INF/jsp/taglibs.jsp"%>


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Audit LarryLast Events
</div>
<h1>Audit LarryLast Events</h1>
<br/>
<hr/>
	
<br/>
<br/>
<display:table list="${miniExchangeEventList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="exchangeEvent"  requestURI="#" class="list">
	<display:column title="EvenName" sortable="true">${exchangeEvent.event.name}</display:column>
	<display:column title="Event Date And Time" sortable="true">
		<fmt:formatDate pattern="MM/dd/yyyy" value="${exchangeEvent.event.localDate}" />				
			<c:choose>
				<c:when test="${exchangeEvent.event.localTime == null}">
					TBD
				</c:when>
				<c:otherwise>
					<fmt:formatDate pattern="hh:mm aa" value="${exchangeEvent.event.localTime}" />
				</c:otherwise>
			</c:choose>
	</display:column>
	<display:column title="Venue" sortable="true">${exchangeEvent.event.venue.building},${exchangeEvent.event.venue.state},${exchangeEvent.event.venue.city},${exchangeEvent.event.venue.country}</display:column>
	<display:column title='PreSale'> 
		<c:choose>
			<c:when test="${exchangeEvent.allowSectionRange !=null && exchangeEvent.allowSectionRange}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>
	
	<display:column property="exposure" title="Exposure"/>
	<display:column title='Shipping Method' >
		<c:choose>
			<c:when test="${exchangeEvent.shippingMethod eq '0'}">Default Website Setting</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '1'}">E-Ticket</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '2'}" >Will Call</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '3'}" >Local Pickup Near Venue</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '4'}" >E-Ticket or Will Call</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '5'}" >Will Call or Local Pickup Near Venue</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '6'}" >E-Ticket or Local Pickup Near Venue</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '7'}" >E-Ticket or Will Call or Local Pickup Near Venue</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '8'}" >Paperless (Meet Seller at Venue)</c:when>
			<c:when test ="${exchangeEvent.shippingMethod eq '9'}" >Electronic Transfer</c:when>
		</c:choose>
	</display:column> 
	<display:column title='Near Term Display Option' >
		<c:choose>
			<c:when test="${exchangeEvent.nearTermDisplayOption eq '0'}">Default near-term display options</c:when>
			<c:when test ="${exchangeEvent.nearTermDisplayOption eq '1'}">Always show near-term shipping</c:when>
			<c:when test ="${exchangeEvent.nearTermDisplayOption eq '2'}" >Only show near-term shipping</c:when>
		</c:choose>
	</display:column>
	
	<display:column title='Shipping Days' >
			${exchangeEvent.shippingDays} Days
	</display:column>
	
	<display:column title='RPT Factor'>
		${exchangeEvent.rptFactor}%
	</display:column>
	<display:column title='Price Breakup' >
		$${exchangeEvent.priceBreakup} 
	</display:column>
	<display:column title='Lower Markup' >
		$${exchangeEvent.lowerMarkup}
	</display:column>
	<display:column title='Upper Markup' >
		$${exchangeEvent.upperMarkup}
	</display:column>
	
	<display:column title='Lower Shipping Fees' >
		$${exchangeEvent.lowerShippingFees}
	</display:column>
	
	<display:column title='Upper Shipping Fees' >
		$${exchangeEvent.upperShippingFees}
	</display:column>
	
	<display:column title='TicketNetwork Event' >
		<c:choose>
			<c:when test="${exchangeEvent.tnBrokerId!=null}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='VividSeats Event' >
		<c:choose>
			<c:when test="${exchangeEvent.vividBrokerId!=null}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='ScoreBig Event'>
		<c:choose>
			<c:when test="${exchangeEvent.scoreBigBrokerId!=null}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>
	
	<display:column title='Broker Event Status' >
		${exchangeEvent.event.brokerStatus}
	</display:column>
</display:table>
			
			
