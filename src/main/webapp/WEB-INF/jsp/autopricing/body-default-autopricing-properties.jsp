<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Default Autopricing Properties
</div>

<div style="clear:both"></div>

<h1>Default AutoPricing Properties</h1>
<script type="text/javascript">

	$(function () {
		
		$("#isAllParentCategory").click(function() {
			if((document.getElementById("isAllParentCategory").checked)){
				$("#parentCategoryId").each(function(){
					$("#parentCategoryId option").attr("selected","selected");				
				});	
			} else {
				$("#parentCategoryId").each(function(){
					$("#parentCategoryId option").attr("selected",false);				
				});	
			}
			$('#gridTable').hide();
			$('#applicableProductRow').hide();
		 }) ;	
		
		$("#isAllApplicableProduct").click(function() {
			if((document.getElementById("isAllApplicableProduct").checked)){
				$("#applicableProductId").each(function(){
					$("#applicableProductId option").attr("selected","selected");				
				});	
			} else {
				$("#applicableProductId").each(function(){
					$("#applicableProductId option").attr("selected",false);				
				});	
			}
		 }) ;	
		
		$('#copyAllExposure').click(function(){
			if($('#copyAllExposure').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('exposure');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllShippingMethod').click(function(){
			if($('#copyAllShippingMethod').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('shippingMethod');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});  
		$('#copyAllNearTermDisplayOption').click(function(){
			if($('#copyAllNearTermDisplayOption').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('nearTermDisplayOption'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllShippingDays').click(function(){
			if($('#copyAllShippingDays').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('shippingDays'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllLowerMarkup').click(function(){
			if($('#copyAllLowerMarkup').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('lowerMarkup');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllUpperMarkup').click(function(){
			if($('#copyAllUpperMarkup').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('upperMarkup');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllLowerShippingFees').click(function(){
			if($('#copyAllLowerShippingFees').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('lowerShippingFees');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllUpperShippingFees').click(function(){
			if($('#copyAllUpperShippingFees').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('upperShippingFees');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllRptFactor').click(function(){
			if($('#copyAllRptFactor').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('rptFactor');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllPriceBreakup').click(function(){
			if($('#copyAllPriceBreakup').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('priceBreakup');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllSectionCountTicket').click(function(){
			if($('#copyAllSectionCountTicket').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('sectionCountTicket');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllTicketNetworkBroker').click(function(){
			if($('#copyAllTicketNetworkBroker').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('ticketnetworkBroker'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllVividSeatsBroker').click(function(){
			if($('#copyAllVividSeatsBroker').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('vividseatBroker'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllScoreBigBroker').click(function(){
			if($('#copyAllScoreBigBroker').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('scorebigBroker'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllFanXchangeBroker').click(function(){
			if($('#copyAllFanXchangeBroker').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('fanxchangeBroker'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllTicketcityBroker').click(function(){
			if($('#copyAllTicketcityBroker').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('ticketcityBroker'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllSeatGeekBroker').click(function(){
			if($('#copyAllSeatGeekBroker').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('seatgeek'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllZoneTicketBroker').click(function(){
			if($('#copyAllZoneTicketBroker').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('zoneTicketBroker'); 
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		
		/* $('#copyAllTixcityEnabled').click(function(){
			if($('#copyAllTixcityEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('tixcityEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		}); */
		
		$('#copyAllRotEnabled').click(function(){
			if($('#copyAllRotEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('rotEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllVividEnabled').click(function(){
			if($('#copyAllVividEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('vividEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllRtwEnabled').click(function(){
			if($('#copyAllRtwEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('rtwEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllMinicatsEnabled').click(function(){
			if($('#copyAllMinicatsEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('minicatsEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllLastrowMinicatsEnabled').click(function(){
			if($('#copyAllLastrowMinicatsEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('lastrowMinicatsEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllVipMinicatsEnabled').click(function(){
			if($('#copyAllVipMinicatsEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('vipMinicatsEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllVipLastRowMinicatsEnabled').click(function(){
			if($('#copyAllVipLastRowMinicatsEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('vipLastRowMinicatsEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAllPresaleAutocatsEnabled').click(function(){
			if($('#copyAllPresaleAutocatsEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('presaleAutocatsEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllTaxPercentage').click(function(){
			if($('#copyAllTaxPercentage').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('taxPercentage');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllExcludeHoursBeforeEvent').click(function(){
			if($('#copyAllExcludeHoursBeforeEvent').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyTextField('excludeHoursBeforeEvent');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllLarryLastEnabled').click(function(){
			if($('#copyAllLarryLastEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('larryLastEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllZonedLastRowMinicatsEnabled').click(function(){
			if($('#copyAllZonedLastRowMinicatsEnabled').attr('checked')){
				$('.selectCheck').attr('checked', true);
				copyCheckBox('zonedLastrowMinicatsEnabled');
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$('#copyAll').click(function(){
			if($('#copyAll').attr('checked')){
				$('.selectCheck').attr('checked', true);
			}else{
				$('.selectCheck').attr('checked', false);
			}
		});
		
		$( "#productId" ).change(function() {
			$('#gridTable').hide();
			$('#applicableProductRow').hide();
		});
		$( "#parentCategoryId" ).change(function() {
			$('#gridTable').hide();
			$('#applicableProductRow').hide();
		});
	});

	function copyCheckBox(fieldName){
		var isFirst = true;
		var firstFieldValue = false;
		$(".selectCheck:checked").each(function() {
			var id = this.id.replace('checkbox',fieldName);
			if(isFirst){
				if($("#" + id).attr('checked')) {
					firstFieldValue = true;
				}
				isFirst= false;
			}else{
				$("#" + id ).attr('checked', firstFieldValue);
			}
		});
	}
	function copyTextField(fieldName){
		var isFirst = true;
		var firstFieldValue;
		$(".selectCheck:checked").each(function() {
			var id = this.id.replace('checkbox',fieldName);
			if(isFirst){
				firstFieldValue = $("#" + id ).val();
				isFirst= false;
			}else{
				$("#" + id ).val(firstFieldValue);
			}
		});
	}
	function selectRow(id) {
		$("#checkbox_" + id ).attr('checked', true);
	}
	
	function validateForm() {
		
		var productId =$('#productId').val();
		if(productId == '') {
			alert('select valid product');
			$("#"+productId).focus();
			flag = false;
			return ;
		}
		var decimalRegex = /^[0-9]{0,10}(\.[0-9]{0,3})?$/;
		var flag= true;
		var isMinimamOneRecord = false;
		$('.selectCheck:checkbox:checked').each(function () {
			
			isMinimamOneRecord = true;
			var id,value;
			
			id = this.id.replace('checkbox','exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Exposure can not be blank .');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			
			id = this.id.replace('checkbox','rptFactor');
			value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid RPT Factor.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','priceBreakup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Price Breakup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerMarkup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperMarkup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			if(productId == 9) {
				id = this.id.replace('checkbox','taxPercentage');
				value = $.trim($("#"+id).val());
				if(value =='' || !decimalRegex.test(value)){
					alert('Please enter valid Tax Percentage.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
				id = this.id.replace('checkbox','excludeHoursBeforeEvent');
				value = $.trim($("#"+id).val());
				if(value =='' || !(/^\d+$/.test(value))){
					alert('Please enter valid Exclude Hours Before Event.');
					$("#"+id).focus();
					flag = false;
					return false;
				}
			}
			id = this.id.replace('checkbox','sectionCountTicket');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Section count Tickets.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
		});
		if(!isMinimamOneRecord) {
			alert('Please Select Minimum one record for Update.');
			flag = false;
			//return ;
		}
		if(!flag){
			return flag;	
		}
		
		return true;
	}
function submitForms(action) {
	if(action=='update'){
		if(validateForm()){
			$("#action").val(action);
			var product = $('#productId :selected').text();
				var applicableProducts = []; 
				$('#applicableProductId :selected').each(function(i, selected){ 
					applicableProducts[i] = $(selected).text(); 
				});
		
				//console.log("Main product " +product+ "Applicable products " +applicableProducts);
				var confirmUpdate;
				if(applicableProducts.length > 0){
					 confirmUpdate = confirm("Are you sure you want to update the selected products " +"\n"+product+"\n"+applicableProducts.join("\n")+"?");
					 //console.log("Update status log :: " +confirmUpdate);
					 if(confirmUpdate == true){
						$("#formId").submit();	
				     }
				}else{
					 confirmUpdate = confirm("Are you sure you want to update the selected product " +"\n"+product+" ?");
					 //console.log("Update status log :: " +confirmUpdate);
					 if(confirmUpdate == true){
						$("#formId").submit();	
				     }
				}
		}
	}else {
		$("#action").val(action);
		$("#formId").submit();
	}
 }

function popupDefaultAutoPricingAudit(pId,pcId){
	var url = "DefaultAutoPricingPropertiesAudit?pId="+pId+"&pcId="+pcId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<form:form action="DefaultAutopricingProperties" method="post" id="formId">
<input type="hidden" name="action" id="action"/>
<c:if test="${not empty info}">
	<div  style="color:blue;"  >${info}</div>
</c:if> 
<br/>
<br/>
<table width="80%" align="center">
	<tr>
		<td><b>Product:</b></td>
		<td>
			<select id="productId" name="productId">
				<option value="">---Select---</option>
				<c:forEach items="${productList}" var="productObj">
				<option value="${productObj.id }"
				<c:if test="${selectedProductId ne null and productObj.id eq selectedProductId}"> Selected </c:if>
				>${productObj.name}</option>
			</c:forEach>
		</select>
	</td>
	</tr>
	<tr>
	<td><b>Parent Category:</b></td>
	<td>
		Check  All: <input type="checkbox" name="isAllParentCategory" <c:if test="${isAllParentCategorySelected == 'on'}">checked</c:if> id="isAllParentCategory" /><br />
	
		<select name="parentCategoryId" id="parentCategoryId" style="width: 300px" multiple size="5">
			<c:forEach var="parentCategory" items="${parentCategoryList}">
				<c:set var="a1" value="false" />  
				<c:forEach var="selected1" items="${selectedParentCategoryId}">
					<c:if test="${parentCategory.id == selected1}">
						<c:set var="a1" value="true" />
					</c:if>
				</c:forEach>
				<option value="${parentCategory.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${parentCategory.name}</option>	
			</c:forEach>
		</select>
	</td>
	</tr>
	<tr>
	<td colspan="3" align="center">
	<br />
		<input type="button" value="Get Data" onclick="submitForms('search');" class="medButton">
	<br />
	</td>
</tr>
	<c:if test="${selectedProductId ne null and selectedProductId ne 10 and selectedProductId ne 11 and selectedProductId ne 6 and selectedProductId ne 7}">
<c:if test="${not empty apDefaultPropertyList}" >
	<tr id="applicableProductRow">
	<td><b>Apply To:</b></td>
	<td>
		Check  All: <input type="checkbox" name="isAllApplicableProduct" <c:if test="${isAllProductSelected == 'on'}">checked</c:if> id="isAllApplicableProduct" /><br />
	
		<select name="applicableProductId" id="applicableProductId" style="width: 300px" multiple size="5">
			<c:forEach var="product" items="${productList}">
				<c:if test="${product.id ne selectedProductId}">
					<c:set var="a1" value="false" />  
					<c:forEach var="selected1" items="${selectedApplicableProductId}">
						<c:if test="${product.id == selected1}">
							<c:set var="a1" value="true" />
						</c:if>
					</c:forEach>
					<option value="${product.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${product.name}</option>	
				</c:if>
			</c:forEach>
		</select>
	</td>
	</tr>
</c:if>
</c:if>
</table>
<br />
		
	<c:if test="${not empty apDefaultPropertyList}" >

	<div id="gridTable" align="center" >

		<display:table list="${apDefaultPropertyList}" style="width:60%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="apDefaultProperty"  requestURI="#" class="list">

		<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
			<input type="checkbox" name="checkbox_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="checkbox_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
			<c:if test ="${apDefaultProperty.id !=null  }"> checked="checked" </c:if> class="selectCheck" />
		</display:column>
		<display:column title="Product">${apDefaultProperty.product.name}</display:column>
		<display:column title="Event Category">${apDefaultProperty.parentCategory.name}</display:column>
		<display:column title='Exposure<br/><input type="checkbox" name="copyAllExposure" id="copyAllExposure" >' >
			<select name="exposure_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
			id="exposure_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
			class="shippingMethodText" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
				<option value="">--- Select ---</option>
				<option value="1-OXP" <c:if test ="${apDefaultProperty.exposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
				<option value="2-OXP" <c:if test ="${apDefaultProperty.exposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
				<option value="3-OXP" <c:if test ="${apDefaultProperty.exposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
			</select>
		</display:column>
			
			<display:column title='Shipping Method<br/><input type="checkbox" name="copyAllShippingMethod" id="copyAllShippingMethod" >' >
				<select name="shippingMethod_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="shippingMethod_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						class="shippingMethodText" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
						<option value="">--- Select ---</option>
					<option value="0" <c:if test ="${apDefaultProperty.shippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${apDefaultProperty.shippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${apDefaultProperty.shippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${apDefaultProperty.shippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${apDefaultProperty.shippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${apDefaultProperty.shippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${apDefaultProperty.shippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${apDefaultProperty.shippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${apDefaultProperty.shippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${apDefaultProperty.shippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
					
				</select>
			</display:column> 
			
			<display:column title='Near Term Display Option<br/><input type="checkbox" name="copyAllNearTermDisplayOption" id="copyAllNearTermDisplayOption" >' >
				<select name="nearTermDisplayOption_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="nearTermDisplayOption_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						class="shippingMethodText" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
					<option value="0" <c:if test ="${apDefaultProperty.nearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${apDefaultProperty.nearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${apDefaultProperty.nearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
			</display:column>
			
			<display:column title='Shipping Days<br/><input type="checkbox" name="copyAllShippingDays" id="copyAllShippingDays" >' >
				<select name="shippingDays_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="shippingDays_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						class="shippingMethodText" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
					<option value="0" <c:if test ="${apDefaultProperty.shippingDays eq '0'}" > selected </c:if> >0 Days</option>
					<option value="2" <c:if test ="${apDefaultProperty.shippingDays eq '2'}" > selected </c:if> >2 Days</option>
					<option value="4" <c:if test ="${apDefaultProperty.shippingDays eq '4'}" > selected </c:if> >4 Days</option>
				</select>
			</display:column>
			
			<display:column title='RPT Factor<br/><input type="checkbox" name="copyAllRptFactor" id="copyAllRptFactor" >' >
				<input type="text" value="${apDefaultProperty.rptFactor}" name="rptFactor_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="rptFactor_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Price Breakup<br/><input type="checkbox" name="copyAllPriceBreakup" id="copyAllPriceBreakup" >' >
				<input type="text" value="${apDefaultProperty.priceBreakup}" name="priceBreakup_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="priceBreakup_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Lower Markup<br/><input type="checkbox" name="copyAllLowerMarkup" id="copyAllLowerMarkup" >' >
				<input type="text" value="${apDefaultProperty.lowerMarkup}" name="lowerMarkup_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="lowerMarkup_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Upper Markup<br/><input type="checkbox" name="copyAllUpperMarkup" id="copyAllUpperMarkup" >' >
				<input type="text" value="${apDefaultProperty.upperMarkup}" name="upperMarkup_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="upperMarkup_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Lower Shipping Fees<br/><input type="checkbox" name="copyAllLowerShippingFees" id="copyAllLowerShippingFees" >' >
				<input type="text" value="${apDefaultProperty.lowerShippingFees}" name="lowerShippingFees_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="lowerShippingFees_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Upper Shipping Fees<br/><input type="checkbox" name="copyAllUpperShippingFees" id="copyAllUpperShippingFees" >' >
				<input type="text" value="${apDefaultProperty.upperShippingFees}" name="upperShippingFees_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="upperShippingFees_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="5" class="markupText"/>
			</display:column>
			<c:if test="${selectedProductId ne null and selectedProductId eq 9}">
				<display:column title='Tax Percentage<br/><input type="checkbox" name="copyAllTaxPercentage" id="copyAllTaxPercentage" >' >
					<input type="text" value="${apDefaultProperty.taxPercentage}" name="taxPercentage_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="taxPercentage_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="5" class="markupText"/>
				</display:column>
				<display:column title='Exclude Hours Before Event<br/><input type="checkbox" name="copyAllExcludeHoursBeforeEvent" id="copyAllExcludeHoursBeforeEvent" >' >
					<input type="text" value="${apDefaultProperty.excludeHoursBeforeEvent}" name="excludeHoursBeforeEvent_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="excludeHoursBeforeEvent_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="3" class="markupText"/>
				</display:column>
			</c:if>
			<display:column title='Section Count Ticket<br/><input type="checkbox" name="copyAllSectionCountTicket" id="copyAllSectionCountTicket" >' >
				<input type="text" value="${apDefaultProperty.sectionCountTicket}" name="sectionCountTicket_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="sectionCountTicket_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" size="5" class="markupText"/>
			</display:column>
			
			<c:if test="${selectedProductId ne null and selectedProductId ne 10 and selectedProductId ne 11 and selectedProductId ne 6 and selectedProductId ne 7}">
				<display:column title='TicketNetwork Broker<br/><input type="checkbox" name="copyAllTicketNetworkBroker" id="copyAllTicketNetworkBroker" >' >
					<select name="ticketnetworkBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="ticketnetworkBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" class="ticketnetworkBroker" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq apDefaultProperty.ticketNetworkBrokerId}" > selected </c:if> >${broker.name}</option>
						</c:forEach>
					</select>
				</display:column>
			<display:column title='VividSeats Broker<br/><input type="checkbox" name="copyAllVividSeatsBroker" id="copyAllVividSeatsBroker" >' >
				<select name="vividseatBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="vividseatBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" class="vividBroker" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
					<option value="">--- No Broker ---</option>
					<c:forEach items="${brokers}" var="broker">
						<option value="${broker.id}" <c:if test ="${broker.id eq apDefaultProperty.vividBrokerId}" > selected </c:if> >${broker.name}</option>
					</c:forEach>
				</select>
			</display:column>
			
				<display:column title='ScoreBig Broker<br/><input type="checkbox" name="copyAllScoreBigBroker" id="copyAllScoreBigBroker" >' >
					<select name="scorebigBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="scorebigBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" class="scoreBigBroker" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq apDefaultProperty.scoreBigBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach>
					</select>
				</display:column>
				<display:column title='FanXchange Broker<br/><input type="checkbox" name="copyAllFanXchangeBroker" id="copyAllFanXchangeBroker" >' >
					<select name="fanxchangeBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="fanxchangeBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" class="fanxchangeBroker" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq apDefaultProperty.fanxchangeBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach>
					</select>
				</display:column>
				<display:column title='TicketCity Broker<br/><input type="checkbox" name="copyAllTicketcityBroker" id="copyAllTicketcityBroker" >' >
					<select name="ticketcityBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="ticketcityBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" class="ticketcityBroker" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq apDefaultProperty.ticketcityBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach>
					</select>
				</display:column>
				<display:column title='SeatGeek Broker<br/><input type="checkbox" name="copyAllSeatGeekBroker" id="copyAllSeatGeekBroker" >' >
					<select name="seatgeekBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="seatgeekBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" class="ticketcityBroker" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq apDefaultProperty.seatGeekBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach>
					</select>
				</display:column>
				<display:column title='RewardTheFan Broker<br/><input type="checkbox" name="copyAllZoneTicketBroker" id="copyAllZoneTicketBroker" >' >
					<select name="zoneTicketBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" id="zoneTicketBroker_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" class="zoneTicketBroker" onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<option value="1" <c:if test ="${1 eq apDefaultProperty.zoneTicketBrokerId}" > selected </c:if> >MZTix</option>
						<option value="5" <c:if test ="${5 eq apDefaultProperty.zoneTicketBrokerId}" > selected </c:if> >Right This Way</option>
						
						<%-- <c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq apDefaultProperty.zoneTicketBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach> --%>
					</select>
				</display:column>
			</c:if>
			
			<c:if test="${selectedProductId ne null and (selectedProductId eq 10 or selectedProductId eq 11 or selectedProductId eq 6 or selectedProductId eq 7)}">
			
			 <display:column title='RTW Enabled<br/><input type="checkbox" name="copyAllRtwEnabled" id="copyAllRtwEnabled" >' >
					<input type="checkbox" name="rtwEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
					id="rtwEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
					onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
					<c:if test ="${apDefaultProperty.rtwEnabled !=null && apDefaultProperty.rtwEnabled}"> checked="checked" </c:if> />
				</display:column> 
				
				<%-- <display:column title='TixCity Enabled<br/><input type="checkbox" name="copyAllTixcityEnabled" id="copyAllTixcityEnabled" >' >
					<input type="checkbox" name="tixcityEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
					id="tixcityEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
					onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
					<c:if test ="${apDefaultProperty.tixcityEnabled !=null && apDefaultProperty.tixcityEnabled}"> checked="checked" </c:if> />
				</display:column> --%>
				<%-- <display:column title='RTW-2 Enabled<br/><input type="checkbox" name="copyAllRotEnabled" id="copyAllRotEnabled" >' >
					<input type="checkbox" name="rotEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
					id="rotEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
					onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
					<c:if test ="${apDefaultProperty.rotEnabled !=null && apDefaultProperty.rotEnabled}"> checked="checked" </c:if> />
				</display:column> --%>
				
				<%-- <display:column title='VividSeat <br/><input type="checkbox" name="copyAllVividEnabled" id="copyAllVividEnabled" >' >
					<input type="checkbox" name="vividEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
					id="vividEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
					onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
					<c:if test ="${apDefaultProperty.vividSeatEnabled !=null && apDefaultProperty.vividSeatEnabled}"> checked="checked" </c:if> />
				</display:column> --%>
				
				<c:if test="${selectedProductId ne null and selectedProductId eq 6}">
					
					<display:column title='Zoned LastRow Minicats Enabled<br/><input type="checkbox" name="copyAllZonedLastRowMinicatsEnabled" id="copyAllZonedLastRowMinicatsEnabled" >' >
						<input type="checkbox" name="zonedLastrowMinicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						id="zonedLastrowMinicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
						<c:if test ="${apDefaultProperty.zonedLastrowMinicatsEnabled !=null && apDefaultProperty.zonedLastrowMinicatsEnabled}"> checked="checked" </c:if> />
					</display:column>
					
					<display:column title='LarryLast Enabled<br/><input type="checkbox" name="copyAllLarryLastEnabled" id="copyAllLarryLastEnabled" >' >
						<input type="checkbox" name="larryLastEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						id="larryLastEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
						<c:if test ="${apDefaultProperty.larryLastEnabled !=null && apDefaultProperty.larryLastEnabled}"> checked="checked" </c:if> />
					</display:column>
					
				</c:if>
				
				<c:if test="${selectedProductId ne null and selectedProductId eq 10}">
					
					<display:column title='Minicats Enabled<br/><input type="checkbox" name="copyAllMinicatsEnabled" id="copyAllMinicatsEnabled" >' >
						<input type="checkbox" name="minicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						id="minicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
						<c:if test ="${apDefaultProperty.minicatsEnabled !=null && apDefaultProperty.minicatsEnabled}"> checked="checked" </c:if> />
					</display:column>
					
					<display:column title='Lastrow Minicats Enabled<br/><input type="checkbox" name="copyAllLastrowMinicatsEnabled" id="copyAllLastrowMinicatsEnabled" >' >
						<input type="checkbox" name="lastrowMinicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						id="lastrowMinicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
						<c:if test ="${apDefaultProperty.lastrowMinicatsEnabled !=null && apDefaultProperty.lastrowMinicatsEnabled}"> checked="checked" </c:if> />
					</display:column>
					
					<display:column title='VipMinicats Enabled<br/><input type="checkbox" name="copyAllVipMinicatsEnabled" id="copyAllVipMinicatsEnabled" >' >
						<input type="checkbox" name="vipMinicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						id="vipMinicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
						<c:if test ="${apDefaultProperty.vipMinicatsEnabled !=null && apDefaultProperty.vipMinicatsEnabled}"> checked="checked" </c:if> />
					</display:column>
					
					<display:column title='VipLastRow Minicats Enabled<br/><input type="checkbox" name="copyAllVipLastRowMinicatsEnabled" id="copyAllVipLastRowMinicatsEnabled" >' >
						<input type="checkbox" name="vipLastRowMinicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						id="vipLastRowMinicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
						<c:if test ="${apDefaultProperty.vipLastRowMinicatsEnabled !=null && apDefaultProperty.vipLastRowMinicatsEnabled}"> checked="checked" </c:if> />
					</display:column>
				</c:if>
				
				<c:if test="${selectedProductId ne null and selectedProductId eq 11}">
					
					<display:column title='Minicats Enabled<br/><input type="checkbox" name="copyAllMinicatsEnabled" id="copyAllMinicatsEnabled" >' >
						<input type="checkbox" name="minicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						id="minicatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
						<c:if test ="${apDefaultProperty.minicatsEnabled !=null && apDefaultProperty.minicatsEnabled}"> checked="checked" </c:if> />
					</display:column>
					
					<display:column title='Presale Autocats Enabled<br/><input type="checkbox" name="copyAllPresaleAutocatsEnabled" id="copyAllPresaleAutocatsEnabled" >' >
						<input type="checkbox" name="presaleAutocatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						id="presaleAutocatsEnabled_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}" 
						onchange="selectRow('${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}');" 
						<c:if test ="${apDefaultProperty.presaleAutocatsEnabled !=null && apDefaultProperty.presaleAutocatsEnabled}"> checked="checked" </c:if> />
					</display:column>
				</c:if>
			</c:if>
			
			<display:column title="Action" >
			 	<input type="button" value="Audit"    class="medButton" id="audit_${apDefaultProperty.product.id}_${apDefaultProperty.parentCategory.id}"
				onClick="popupDefaultAutoPricingAudit('${apDefaultProperty.product.id}','${apDefaultProperty.parentCategory.id}');"/>
			 </display:column>
		
		</display:table>
	<br />	
	<div style="float:right; padding-right:320px;">
		<input type="button" value="Update"    class="medButton" id="update1" name="update1" onclick="javascript:submitForms('update');"/>
	</div>
	</div>	
	<br />
	
	</c:if>
	<c:if test="${empty apDefaultPropertyList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
</form:form>