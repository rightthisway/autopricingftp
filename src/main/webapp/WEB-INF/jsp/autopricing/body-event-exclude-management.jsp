<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

	

$(function () {
	
	
	 $('#autoVenue').autocomplete("AutoCompleteVenue", {
			width: 650,
			max: 1000,
			minChars: 2,		
			dataType: "text",
			
			formatItem: function(row, i, max) {
				if(row[0]=='VENUE'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				} 
			}
		}).result(function (event,row,formatted){
			  
				$('#autoVenue').val('');
				$('#selectedValue').text(row[2]);
				 if(row[0]=='VENUE'){
					$('#selectedOption').text('Venue');
					$("#venueId").val(row[1]);
										
				}
		});  	
	
	 $("#isAllBroker").click(function() {
		if((document.getElementById("isAllBroker").checked)){
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected","selected");				
			});	
		} else {
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllProduct").click(function() {
		if((document.getElementById("isAllProduct").checked)){
			$("#productId").each(function(){
				$("#productId option").attr("selected","selected");				
			});	
		} else {
			$("#productId").each(function(){
				$("#productId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	
	
	
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$( "#brokerId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#productId" ).change(function() {
		$('#gridTable').hide();
		});
	
	
	
	
	/* $('#copyAll').click(function(){
		copyAll();
	}); */
});

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}

	function submitForms(action) {
		if(action=='update'){
			if(validateForm()){
				$("#action").val(action);
				$("#formId").submit();
			}
		}else {
			var selected = [];
			$('#gridTable input[type=checkbox]').each(function() {
			   if(!$(this).prop('checked')){
			         selected.push($(this).attr('name'));
			   }
			});
			$('#uncheckedboxes').val(selected);
			
			for (var i = 0; i < selected.length; i++) {
			   console.log(selected[i]);
			    //Do something
			}
			$("#action").val(action);
			$("#formId").submit();
		}
	}
	
	function validateForm(){
		var flag= true;
		var isMinimamOneRecord = false;
		
		$('.selectCheck:checkbox:checked').each(function () {
			
			isMinimamOneRecord = true;
			
			var id = this.id.replace('checkbox','excludeEventDays');
			var value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Exclude Event Days.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
		});
		
		if(!isMinimamOneRecord) {
			alert('Please Select Minimum one record for Update.');
			flag = false;
			//return ;
		}
		
		if(!flag){
			return flag;	
		}
		return true;
}
	function selectRow(id) {
		$("#checkbox_" + id ).attr('checked', true);
	}
	
	function popupExcludeVenueAudit(bId,pId,vId){
		var url = "ExcludeVenuesAudit?bId="+bId+"&pId="+pId+"&vId="+vId;
		newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
		if (window.focus) {newwindow.focus()}
		return false;
		
	} 
	
</script>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> > Autopricing Settings
</div>
<h1>Autopricing Settings</h1>
<br/>
<hr/>
<form:form action="ExcludeEventsBasedOnVenues" method="post" id="formId">
<input type="hidden" name="action" id="action"/>
<input type="hidden" name="uncheckedboxes" id="uncheckedboxes"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	
<table align="center">
<tr>
<td align="center"><b>Broker :</b> Check  All: <input type="checkbox" name="isAllBroker" <c:if test="${isAllBrokerSelected == 'on'}">checked</c:if> id="isAllBroker"/>
</td>
<td align="center"><b>Product :</b> Check  All: <input type="checkbox" name="isAllProduct" <c:if test="${isAllProductSelected == 'on'}">checked</c:if> id="isAllProduct" />
</td>
</tr>
<tr><td>
<select name="brokerId" id="brokerId" style="width: 300px" multiple size="5" >
 <c:forEach var="broker" items="${brokerList}">
 <c:set var="a1" value="false" />
 <c:forEach var="selected" items="${selectedBrokerId}">
  <c:if test="${broker.id == selected}">
  <c:set var="a1" value="true" />
  </c:if>
  </c:forEach>
    <option value="${broker.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${broker.name}</option>
  </c:forEach>
</select> 
</td>
<td> 
<select name="productId" id="productId" style="width: 300px" multiple size="5">
<c:forEach var="product" items="${productList}">
<c:set var="a2" value="false" />  
  <c:forEach var="selected1" items="${selectedProductId}">
  <c:if test="${product.id == selected1}">
  <c:set var="a2" value="true" />
  </c:if>
  </c:forEach>
    <option value="${product.id}" <c:if test="${a2 == 'true'}">selected</c:if>>${product.name}</option>
  </c:forEach>
</select>  
</td>
</tr>
<tr>
   <td colspan="3">
   </td>
</tr>  
<tr>
   <td colspan="3">
   </td>
</tr> 
<tr>
   <td colspan="3">
   </td>
</tr>
<tr>
<td colspan="3" align="center">
<b>Select The Venue :</b>
      <input type="text" name="autoVenue" id="autoVenue"  >
      <input type="hidden" id="venueId" name="venueId" value="${venueId}">
</td> 
</tr> 
<tr>
			<td><b><span id="selectedOption" >${selectedOption}</span></b></td>
			<td><b><span id="selectedValue" >${selectedValue}</span></b></td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>  
<tr>
	<td colspan="3" align="center">
	<br />
		<input type="button" value="Get Data" onclick="submitForms('search');" class="medButton">
	</td>
</tr>
</table>
	<br />
	<c:if test="${not empty excludeVenuesList}" >
	<div id="gridTable" align="center" >

		<display:table list="${excludeVenuesList}" style="width:60%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="excludeVenue"  requestURI="#" class="list">

		<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
			<input type="checkbox" name="checkbox_${excludeVenue.broker.id}_${excludeVenue.product.id}_${excludeVenue.venue.id}" id="checkbox_${excludeVenue.broker.id}_${excludeVenue.product.id}_${excludeVenue.venue.id}" 
			<c:if test ="${excludeVenue.id !=null  }"> checked="checked" </c:if> class="selectCheck" />
		</display:column>
		<display:column title="Broker" sortable="true">${excludeVenue.broker.name}</display:column>
		<display:column title="Product" sortable="true">${excludeVenue.product.name}</display:column>
		<display:column title="Venue" sortable="true">${excludeVenue.venue.building} ${excludeVenue.venue.city} ${excludeVenue.venue.state} ${excludeVenue.venue.country}</display:column>
						
        <display:column title="Action" >
			<input type="button" value="Audit" class="medButton" id="audit_${excludeVenue.broker.id}_${excludeVenue.product.id}_${excludeVenue.venue.id}" onClick="popupExcludeVenueAudit('${excludeVenue.broker.id}','${excludeVenue.product.id}','${excludeVenue.venue.id}');"/>
		</display:column> 
		</display:table>
	<br />	
	<div style="float:right; padding-right:320px;">
		<input type="button" value="Save"    class="medButton" id="update1" name="update1" onclick="javascript:submitForms('save');"/> 
	</div>
	</div>	
	<br />
	
	
	</c:if>
	<c:if test="${empty excludeVenuesList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>


</form:form>