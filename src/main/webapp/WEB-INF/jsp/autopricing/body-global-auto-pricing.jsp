<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Manage Existings Global Events
</div>

<div style="clear:both"></div>

<h1>Manage Existings Global Events</h1>
<script type="text/javascript">
$(function () {
	
	 $("#isAllEventType").click(function() {
		if((document.getElementById("isAllEventType").checked)){
			$("#event_type").each(function(){
				$("#event_type option").attr("selected","selected");				
			});	
		} else {
			$("#event_type").each(function(){
				$("#event_type option").attr("selected",false);				
			});	
		}
		
	 }) ;
	 
	 $("#isAllProduct").click(function() {
		if((document.getElementById("isAllProduct").checked)){
			$("#productId").each(function(){
				$("#productId option").attr("selected","selected");				
			});	
		} else {
			$("#productId").each(function(){
				$("#productId option").attr("selected",false);				
			});	
		}
		
	 }) ;
});	 
function ebayExposureUpdate(){
	
	var e = document.getElementById("event_type").value;
	var productId = document.getElementById("productId").value;

	if(e == ''){
		alert('Please select minimam one event type.');
		return;
	}
	if(productId == ''){
		alert('Please select minimam one product.');
		return;
	}
	
	var isEdit = $('#shipping_days_ticketnetwork_edit').attr('checked');
	var value = $.trim($("#shipping_days_ticketnetwork").val());
	if(isEdit && value==''){
		alert('Shipping Days can not be blank.');
		$("#shipping_days_ticketnetwork").focus();
		return ;
	}
	var isEdit = $('#exposure_edit').attr('checked');
	var value = $.trim($("#exposure").val());
	if(isEdit && value==''){
		alert('Exposure can not be blank.');
		$("#exposure").focus();
		return ;
	}
	
	var isEdit = $('#is_auto_exposure_edit').attr('checked');
	var value = $.trim($("#auto_exposure").val());
	if(isEdit && value==''){
		alert('Auto Exposure can not be blank.');
		$("#auto_exposure").focus();
		return ;
	}
	isEdit = $('#is_mini_exposure_edit').attr('checked');
	value = $.trim($("#mini_exposure").val());
	if(isEdit && value==''){
		alert('Mini Exposure can not be blank.');
		$("#mini_exposure").focus();
		return ;
	}
	isEdit = $('#is_vip_mini_exposure_edit').attr('checked');
	value = $.trim($("#vip_mini_exposure").val());
	if(isEdit && value==''){
		alert('Vip Mini Exposure can not be blank.');
		$("#vip_mini_exposure").focus();
		return ;
	}
	isEdit = $('#is_vip_auto_exposure_edit').attr('checked');
	value = $.trim($("#vip_auto_exposure").val());
	if(isEdit && value==''){
		alert('Vip Auto Exposure can not be blank.');
		$("#vip_auto_exposure").focus();
		return ;
	}
	isEdit = $('#is_lastrow_mini_exposure_edit').attr('checked');
	value = $.trim($("#lastrow_mini_exposure").val());
	if(isEdit && value==''){
		alert('LastRow Mini Exposure can not be blank.');
		$("#lastrow_mini_exposure").focus();
		return ;
	}
	isEdit = $('#is_lastfiverow_mini_exposure_edit').attr('checked');
	value = $.trim($("#lastfiverow_mini_exposure").val());
	if(isEdit && value==''){
		alert('LastFiveRow Mini Exposure can not be blank.');
		$("#lastfiverow_mini_exposure").focus();
		return ;
	}
	
	isEdit = $('#is_zones_pricing_exposure_edit').attr('checked');
	value = $.trim($("#zones_pricing_exposure").val());
	if(isEdit && value==''){
		alert('Zones Pricing Exposure can not be blank.');
		$("#zones_pricing_exposure").focus();
		return ;
	}
	
	isEdit = $('#is_zoned_lastrowmini_exposure_edit').attr('checked');
	value = $.trim($("#zoned_lastrowmini_exposure").val());
	if(isEdit && value==''){
		alert('Zoned LastRowMini Exposure can not be blank.');
		$("#zoned_lastrowmini_exposure").focus();
		return ;
	}
	
	isEdit = $('#is_shipping_method_edit').attr('checked');
	value = $.trim($("#shipping_method").val());
	if(isEdit && value==''){
		alert('Shipping Method can not be blank.');
		$("#shipping_method").focus();
		return ;
	}
	
	isEdit = $('#is_near_term_display_option_edit').attr('checked');
	value = $.trim($("#near_term_display_option").val());
	if(isEdit && value==''){
		alert('Near Term Display Option can not be blank.');
		$("#near_term_display_option").focus();
		return ;
	}
	
	isEdit = $('#is_rpt_ticketnetwork_edit').attr('checked');
	value = $.trim($("#rpt_ticketnetwork").val());
	if(isEdit && (value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value)))){
		alert('Please enter valid RPT Factor.');
		$("#rpt_ticketnetwork").focus();
		return ;
	}
	isEdit = $('#is_price_breakup_ticketnetwork_edit').attr('checked');
	value = $.trim($("#price_breakup_ticketnetwork").val());
	if(isEdit && (value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value)))){
		alert('Please enter valid Price Breakup.');
		$("#price_breakup_ticketnetwork").focus();
		return ;
	}
	isEdit = $('#is_lower_markup_ticketnetwork_edit').attr('checked');
	value = $.trim($("#lower_markup_ticketnetwork").val());
	if(isEdit && (value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value)))){
		alert('Please enter valid Lower Markup.');
		$("#lower_markup_ticketnetwork").focus();
		return ;
	}
	isEdit = $('#is_upper_markup_ticketnetwork_edit').attr('checked');
	value = $.trim($("#upper_markup_ticketnetwork").val());
	if(isEdit && (value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value)))){
		alert('Please enter valid Upper Markup.');
		$("#upper_markup_ticketnetwork").focus();
		return ;
	}
	isEdit = $('#is_lower_shippingFees_ticketnetwork_edit').attr('checked');
	value = $.trim($("#lower_shippingFees_ticketnetwork").val());
	if(isEdit && (value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value)))){
		alert('Please enter valid Lower Shipping Fees.');
		$("#lower_shippingFees_ticketnetwork").focus();
		return ;
	}
	isEdit = $('#is_upper_shippingFees_ticketnetwork_edit').attr('checked');
	value = $.trim($("#upper_shippingFees_ticketnetwork").val());
	if(isEdit && (value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value)))){
		alert('Please enter valid Upper Shipping Fees.');
		$("#upper_shippingFees_ticketnetwork").focus();
		return ;
	}
	
	$("#action").val("updateExposure");
	$("#formId").submit();
} 

function isEditAllow(obj,fieldName) {
	
	if($(obj).attr('checked')){
		$("#"+fieldName).removeAttr("disabled");
	} else {
		$("#"+fieldName).val("");
		$("#"+fieldName).attr("disabled", "disabled"); 
	}
}

function popupDefaultAutoPricingAudit(){
	
	var url = "GlobalAutoPricingAuditPopup";
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
	 </script>
<form:form modelAttribute="exosure" action="GlobalAutopricing" method="post" id="formId">




<span  <c:if test="${store==true}">  style="color:blue;" </c:if> <c:if test="${store!=true}">class="successMessage" </c:if> >${exposureMsg}</span>
<br/>
<br/>
<div id="ebayInventoryTabel" >
<c:if test="${not empty sportinfo}">
		<div    style="color:blue;"  >${sportinfo}</div>
</c:if>
<br/>
<table align="center">
<tr align="center">
    <td align="center"><b>Products:</b> Check  All<input type="checkbox" name="isAllProduct" <c:if test="${isAllProductSelected == 'on'}">checked</c:if> id="isAllProduct"/></td><td></td>
    <td align="center"><b>Event Type:</b> Check  All <input type="checkbox" name="isAllEventType" <c:if test="${isAllProductSelected == 'on'}">checked</c:if> id="isAllEventType" /></td>
  </tr>
<tr align="center">
	<td align="center">
		<%-- <input type="checkbox" name="event_type" id="event_type" value="sports" <c:if test="${event_type!=null && fn:contains(event_type,sports)}"> selected </c:if> >Sports &nbsp;&nbsp;
		<input type="checkbox" name="event_type" id="event_type2" value="concerts" <c:if test="${event_type!=null && fn:contains(event_type,concerts)}">selected </c:if>>Concerts &nbsp;&nbsp;
		<input type="checkbox" name="event_type" id="event_type3" value="theater" <c:if test="${event_type!=null && fn:contains(event_type,theater)}">selected </c:if>>Theater &nbsp;&nbsp;
		<input type="checkbox" name="event_type" id="event_type4" value="other" <c:if test="${event_type!=null && fn:contains(event_type,other)}">selected </c:if>>Other &nbsp;&nbsp; --%>
		<select name="productId" id="productId" style="width: 300px" multiple size="5">
        <c:forEach var="product" items="${productList}"> 
              <option value="${product.id}">${product.name}</option>
        </c:forEach>
       </select>  
		
		</td><td></td>
		<td align="center">
		 <select name="event_type" multiple id="event_type">
		  <c:forEach var="tourCat" items="${tourCategory}">
           <option value="${tourCat.name}" >${tourCat.name}</option>
           </c:forEach>
         </select>
		 <%--  <option value="sports"  <c:if test="${event_type!=null && fn:contains(event_type,sports)}"> selected </c:if> >Sports &nbsp;&nbsp;
		  <option value="concerts" <c:if test="${event_type!=null && fn:contains(event_type,concerts)}">selected </c:if>>Concerts &nbsp;&nbsp;
		  <option value="theater"  value="theater" <c:if test="${event_type!=null && fn:contains(event_type,theater)}">selected </c:if>>Theater &nbsp;&nbsp;
		  <option value="other" value="other" <c:if test="${event_type!=null && fn:contains(event_type,other)}">selected </c:if>>Other &nbsp;&nbsp; --%>
		
	</td>
</tr>
</table>	
<br />
	
<table list=""  defaultsort="5" decorator="org.displaytag.decorator.TotalTableDecorator"   id="ebayInventory"  requestURI="#" class="list">
<tr>
   
 <!--  <th>AutoCats Exposure</th>
  <th>MiniCats Exposure</th>	
  <th>Vip MiniCats Exposure</th>		
  <th>Vip AutoCats Exposure</th>
  <th>LastRow MiniCats Exposure</th>
  <th>LastFiveRow MiniCats Exposure</th>
  <th>Zones Pricing Exposure</th>
  <th>Zoned LastRowMini Exposure</th> -->
  <th>Exposure</th>
  <th>Shipping Method</th>
  <th>Near Term Display Option</th>
  <th>RPT(%)</th>
  <th>Price BreakUp</th>
  <th>Lower Markup(%)</th>
  <th>Upper Markup(%)</th>
  <th>Lower Shipping Fees($)</th>
  <th>Upper Shipping Fees($)</th>
  <th>Shipping Days($)</th>
</tr>
<tr>
<td>
<select name="exposure" id="exposure" style="width:200px;" disabled="disabled">
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${autoExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${autoExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${autoExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
<input type="checkbox" name="exposure_edit" id="exposure_edit"  onchange="isEditAllow(this,'exposure');" /></td>

<td> <select name="shipping_method" id="shipping_method" style="width:200px;" disabled="disabled">
					<option value="" >--- Select ---</option>
					<option value="0" <c:if test ="${shippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${shippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${shippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${shippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${shippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${shippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${shippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${shippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${shippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${shippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
				</select>
<input type="checkbox" name="is_shipping_method_edit" id="is_shipping_method_edit" onchange="isEditAllow(this,'shipping_method');" /></td>

<td> <select name="near_term_display_option" id="near_term_display_option" style="width:200px;" disabled="disabled">
					<option value="" >--- Select ---</option>
					<option value="0" <c:if test ="${nearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${nearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${nearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
<input type="checkbox" name="is_near_term_display_option_edit" id="is_near_term_display_option_edit" onchange="isEditAllow(this,'near_term_display_option');" /></td>



  <td>
  	<input type="text"  id="rpt_ticketnetwork" size="5" name="rpt_ticketnetwork" value="${ticketnetworkrpt}" disabled="disabled"/>
  	<input type="checkbox" id="is_rpt_ticketnetwork_edit" name="is_rpt_ticketnetwork_edit" onchange="isEditAllow(this,'rpt_ticketnetwork');" />
  </td>
  <td>
  		<input type="text"  id="price_breakup_ticketnetwork" size="5" name="price_breakup_ticketnetwork" value="${ticketnetworkpricebreakup}" disabled="disabled"/>
  		<input type="checkbox" id="is_price_breakup_ticketnetwork_edit" name="is_price_breakup_ticketnetwork_edit" onchange="isEditAllow(this,'price_breakup_ticketnetwork');"  />
  	</td>
  <td>
  	<input type="text" id="lower_markup_ticketnetwork" size="5" name="lower_markup_ticketnetwork" value="${ticketnetworklowermarkup}" disabled="disabled"/>
  	<input type="checkbox" id="is_lower_markup_ticketnetwork_edit" name="is_lower_markup_ticketnetwork_edit" onchange="isEditAllow(this,'lower_markup_ticketnetwork');"   />
  </td>		
  <td>
  	<input type="text" id="upper_markup_ticketnetwork" size="5" name="upper_markup_ticketnetwork" value="${ticketnetworkuppermarkup}" disabled="disabled"/>
  	<input type="checkbox" id="is_upper_markup_ticketnetwork_edit" name="is_upper_markup_ticketnetwork_edit"  onchange="isEditAllow(this,'upper_markup_ticketnetwork');" />
  </td>
 <td>
 	<input type="text" id="lower_shippingFees_ticketnetwork" size="5" name="lower_shippingFees_ticketnetwork" value="${ticketnetworklowershipping}" disabled="disabled"/>
 	<input type="checkbox" id="is_lower_shippingFees_ticketnetwork_edit" name="is_lower_shippingFees_ticketnetwork_edit"  onchange="isEditAllow(this,'lower_shippingFees_ticketnetwork');" />
 	</td>
  <td>
  	<input type="text" id="upper_shippingFees_ticketnetwork" size="5" name="upper_shippingFees_ticketnetwork" value="${ticketnetworkuppershipping}" disabled="disabled"/>
  	<input type="checkbox" id="is_upper_shippingFees_ticketnetwork_edit" name="is_upper_shippingFees_ticketnetwork_edit"  onchange="isEditAllow(this,'upper_shippingFees_ticketnetwork');" />
  </td>	
  <td>
	<select name="shipping_days_ticketnetwork" id="shipping_days_ticketnetwork" disabled="disabled">
       <option value="">Select</option>
       <option value="0">0 Days</option>
        <option value="2">2 Days</option>
	    <option value="4">4 Days</option>
    </select>
    <input type="checkbox" id="shipping_days_ticketnetwork_edit" name="shipping_days_ticketnetwork_edit"  onchange="isEditAllow(this,'shipping_days_ticketnetwork');" />
</td>	
</tr>							
</table>							
<br/>
</div>
<table align ="center">
<tr>
<td align="center"><input type="button" value="Update"  class="medButton" id="update2" name="update2"onclick="javascript:ebayExposureUpdate();"/>
	&nbsp;&nbsp;<input type="button" value="Audit"  class="medButton" id="update2" name="update2"onclick="javascript:popupDefaultAutoPricingAudit();"/>
</td>
</tr>
</table>		
	<input type="hidden" name="action" id="action"/>
	<input type="hidden" name="parentCat" id="parentCat"/>
	<br/>
	<br/>
</form:form>