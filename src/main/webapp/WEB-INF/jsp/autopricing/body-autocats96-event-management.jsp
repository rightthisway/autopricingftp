
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>

<style media="screen" type="text/css">
#loading{
display:none;
margin-left:105px
}

</style>


<meta http-equiv="Content-type" content="text/plain; charset=UTF-8"/>
</head>
<div id="userSubMenu">
	<jsp:include page="autopricing-exchange-event-user-sub-menu.jsp"></jsp:include>
</div>

<c:set var="auditUrl" value="${auditUrl}"/>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > ${url}
</div>
<h1>Manage Autocats96 Exchange Events</h1>
<br/>


<form:form action="${url}" method="post" id="formId">

<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br/>
	<br/>
	<table width="80%">
		<%-- <tr>
			<td><b>Select Broker:</b></td>
			<td>
				<select id="brokerId" name="brokerId">
					<option value="">---ALL---</option>
					<c:forEach items="${brokers}" var="brokerObj">
						<option value="${brokerObj.id }"
						<c:if test="${broker ne null and brokerObj.id eq broker}"> Selected </c:if>
						>${brokerObj.name}</option>
					</c:forEach>
				</select>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr> --%>
		  <tr>
		  <td></td>
		  <td>
		      <div id="message" style="color: red;"><b>${message}</b></div>
		  </td>
		  <td></td>		
		  <td></td>
		<tr>
			<td><b>Select Child or GrandChild or Artist or Venue:</b></td>
			<td>
				<input type="hidden" id="artist" name="artist" value ="${artist}">
				<input type="hidden" id="venue" name="venue" value ="${venue}">
				<input type="hidden" id="grandChild" name="grandChild" value ="${grandChild}">
				<input type="hidden" id="brokerId" name="brokerId" value ="${brokerId}">
				<input type="text" id="autoArtistVenue" name="autoArtistVenue" onclick="clearArtist();" >
				
				<img id="loading" src="/autopricingweb/images/progress-img.gif" border="0" height="50" width="75">
				
			</td>
			<td>
			
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b><span id="selectedOption" >${selectedOption}</span></b></td>
			<td><b><span id="selectedValue" >${selectedValue}</span></b></td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Events:</b></td>
			<td >
				<input type="hidden" id="eventStr" name="eventStr" value = ${eventStr}/> 
				<input type="checkbox" id="eventsCheckAll" name="eventsCheckAll" onclick ="selectAllEvents()" <c:if test ="${eventsCheckAll}"> checked="checked" </c:if>/> 
				<label for="eventsCheckAll">Select All</label>
				<br/>
				<select id='events' name="events"  multiple="multiple" size='6' style="width:480px;" >
					<c:forEach items="${events}" var="event">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${event.id}${temp}"/>
						<option 
							<c:if test="${event!=null && fn:contains(eventStr,temp2)}"> Selected </c:if>
							value="${event.id}"> ${event.name} <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								${event.venue.building}
						</option>
					</c:forEach> 
				</select>
			</td>
			<td></td>
			<td colspan="3" align="center"></td>
			
		</tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>			
		
		<tr>
			<td colspan="6" align="center">
				<input type="button" value="Get Pricing" onclick="submitForms('search');" class="medButton">
			</td>
		</tr>
		<%-- <tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>	
		<tr>
		    <td>
			 <b> Apply the rules for :</b>
			</td>
			<td>		
		      <c:forEach items="${products}" var="product">
	             <input type="checkbox" name="product-${product.id}" value="${product.id}" id="product-${product.id}"/> ${product.name}
              </c:forEach>
			</td>
		</tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr> --%>
	</table>
	
	<c:if test="${not empty exchangeEventList}" >
	<div id="ebayInventoryTabel" >
	
		
		<div style="float:right; padding-right:320px; margin-right:688px">
			<input type="button" value="Update"  name="update1" id="update1" class="medButton" onclick="javascript:submitForms('update');"/> 
		</div>
		<div id="copyCheckBox" style="float:right;">
			<!--<input onclick="javascript:copyAllEventData();" type="checkbox" name="copyAllCheckbox" id="copyAllCheckbox" >Copy first row  to all remaining rows &nbsp;&nbsp;&nbsp; -->
		</div>
		<br/>
		<br/>
		<br/>
		<display:table list="${exchangeEventList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="exchangeEvent"  requestURI="#" class="list">
			<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
				<input type="checkbox" name="checkbox_${exchangeEvent.event.id}" id="checkbox_${exchangeEvent.event.id}" <c:if test ="${exchangeEvent.status !=null && exchangeEvent.status == 'ACTIVE'  }"> checked="checked" </c:if> class="selectCheck" />
			</display:column>
			<display:column title="EvenName" sortable="true">${exchangeEvent.event.name}</display:column>
			<display:column title="Event Date And Time" sortable="true">

				<fmt:formatDate pattern="MM/dd/yyyy" value="${exchangeEvent.event.localDate}" />				
					<c:choose>
						<c:when test="${exchangeEvent.event.localTime == null}">
							TBD
						</c:when>
						<c:otherwise>
							<fmt:formatDate pattern="hh:mm aa" value="${exchangeEvent.event.localTime}" />
						</c:otherwise>
					</c:choose>
			</display:column>
			<display:column title="Venue" sortable="true">${exchangeEvent.event.venue.building},${exchangeEvent.event.venue.state},${exchangeEvent.event.venue.city},${exchangeEvent.event.venue.country}</display:column>
			<%-- <display:column title='Presale Event' sortable="true">
				<c:choose>
						<c:when test="${exchangeEvent.event.presaleEvent}">Yes</c:when>
						<c:otherwise>No</c:otherwise>
				</c:choose>
			</display:column> --%>
			<display:column title='PreSale<br/><input type="checkbox" name="copyAllSectionRange" id="copyAllSectionRange" >' >
				<input type="checkbox" name="allowSectionRange_${exchangeEvent.event.id}" id="allowSectionRange_${exchangeEvent.event.id}" class="presaleSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" <c:if test ="${exchangeEvent.allowSectionRange !=null && exchangeEvent.allowSectionRange}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='Exposure<br/><input type="checkbox" name="copyAllExposure" id="copyAllExposure" >' >
				<select name="exposure_${exchangeEvent.event.id}" id="exposure_${exchangeEvent.event.id}" class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${exchangeEvent.exposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${exchangeEvent.exposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${exchangeEvent.exposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			
			 <display:column title='Shipping Method<br/><input type="checkbox" name="copyAllShippingMethod" id="copyAllShippingMethod" >' >
				<select name="shipping_method_${exchangeEvent.event.id}" id="shipping_method_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- Select ---</option> 
					<option value="0" <c:if test ="${exchangeEvent.shippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${exchangeEvent.shippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${exchangeEvent.shippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${exchangeEvent.shippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${exchangeEvent.shippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${exchangeEvent.shippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${exchangeEvent.shippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${exchangeEvent.shippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${exchangeEvent.shippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${exchangeEvent.shippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
					
				</select>
			</display:column> 
			
			<display:column title='Near Term Display Option<br/><input type="checkbox" name="copyAllNearTermDisplayOption" id="copyAllNearTermDisplayOption" >' >
				<select name="near_term_display_option_${exchangeEvent.event.id}" id="near_term_display_option_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option> 
					<option value="0" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
			</display:column>
			
			<display:column title='Shipping Days<br/><input type="checkbox" name="copyShippingDays" id="copyShippingDays" >' >
				<select name="shipping_days_${exchangeEvent.event.id}" id="shipping_days_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- Select ---</option> 
						<option value="0" <c:if test ="${exchangeEvent.shippingDays eq '0'}" > selected </c:if> >0 Days</option>
					<option value="2" <c:if test ="${exchangeEvent.shippingDays eq '2'}" > selected </c:if> >2 Days</option>
					<option value="4" <c:if test ="${exchangeEvent.shippingDays eq '4'}" > selected </c:if> >4 Days</option>
				</select>
			</display:column>
			
			<display:column title='RPT Factor<br/><input type="checkbox" name="copyAllRptFactor" id="copyAllRptFactor" >' >
				<input type="text" value="${exchangeEvent.rptFactor}" name="rptFactor_${exchangeEvent.event.id}" id="rptFactor_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Price Breakup<br/><input type="checkbox" name="copyAllPriceBreakup" id="copyAllPriceBreakup" >' >
				<input type="text" value="${exchangeEvent.priceBreakup}" name="priceBreakup_${exchangeEvent.event.id}" id="priceBreakup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Lower Markup<br/><input type="checkbox" name="copyAllLowerMarkup" id="copyAllLowerMarkup" >' >
				<input type="text" value="${exchangeEvent.lowerMarkup}" name="lowerMarkup_${exchangeEvent.event.id}" id="lowerMarkup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Upper Markup<br/><input type="checkbox" name="copyAllUpperMarkup" id="copyAllUpperMarkup" >' >
				<input type="text" value="${exchangeEvent.upperMarkup}" name="upperMarkup_${exchangeEvent.event.id}" id="upperMarkup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Lower Shipping Fees<br/><input type="checkbox" name="copyAllLowerShippingFees" id="copyAllLowerShippingFees" >' >
				<input type="text" value="${exchangeEvent.lowerShippingFees}" name="lowerShippingFees_${exchangeEvent.event.id}" id="lowerShippingFees_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Upper Shipping Fees<br/><input type="checkbox" name="copyAllUpperShippingFees" id="copyAllUpperShippingFees" >' >
				<input type="text" value="${exchangeEvent.upperShippingFees}" name="upperShippingFees_${exchangeEvent.event.id}" id="upperShippingFees_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<%-- <display:column title='TicketNetwork Event<br/><input type="checkbox" name="copyAllTicketNetworkEvent" id="copyAllTicketNetworkEvent" >' >
				<input type="checkbox" name="isTicketNetwork_${exchangeEvent.event.id}" id="isTicketNetwork_${exchangeEvent.event.id}" <c:if test ="${exchangeEvent.isTicketNetwork!=null && exchangeEvent.isTicketNetwork}"> checked="checked" </c:if> />
			</display:column> 
			<display:column title='TicketNetwork Event<br/><input type="checkbox" name="copyAllTicketNetworkEvent" id="copyAllTicketNetworkEvent" >' >
				<select name="ticketnetwork_broker_${exchangeEvent.event.id}" id="ticketnetwork_broker_${exchangeEvent.event.id}" class="ticketnetworkBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- No Broker ---</option>
					<c:forEach items="${brokers}" var="broker">
						<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.ticketNetworkBrokerId}" > selected </c:if> >${broker.name}</option>
					</c:forEach>
				</select>
			</display:column>
			
			<display:column title='ScoreBig Event<br/><input type="checkbox" name="copyAllScoreBigEvent" id="copyAllScoreBigEvent" >' >
				<select name="scorebig_broker_${exchangeEvent.event.id}" id="scorebig_broker_${exchangeEvent.event.id}" class="scoreBigBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- No Broker ---</option>
					<c:forEach items="${brokers}" var="broker">
						<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.scoreBigBrokerId}" > selected </c:if> >${broker.name}</option>	
					</c:forEach>
				</select>
			</display:column>
			<display:column title='VividSeats Event<br/><input type="checkbox" name="copyAllVividSeatsEvent" id="copyAllVividSeatsEvent" >' >
				<select name="vividseat_broker_${exchangeEvent.event.id}" id="vividseat_broker_${exchangeEvent.event.id}" class="vividBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- No Broker ---</option>
					<c:forEach items="${brokers}" var="broker">
						<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.vividBrokerId}" > selected </c:if> >${broker.name}</option>
					</c:forEach>
				</select>
			</display:column>
			
			--%>
			
			<%-- <display:column title='VividSeat <br/><input type="checkbox" name="copyAllVividEnabled" id="copyAllVividEnabled" >' >
				<input type="checkbox" name="vividEnabled_${exchangeEvent.event.id}" id="vividEnabled_${exchangeEvent.event.id}" class="vividSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.vividEnabled !=null && exchangeEvent.vividEnabled}"> checked="checked" </c:if> />
			</display:column> --%>
			
			 <display:column title='RTW Enabled<br/><input type="checkbox" name="copyAllRtwEnabled" id="copyAllRtwEnabled" >' >
				<input type="checkbox" name="rtwEnabled_${exchangeEvent.event.id}" id="rtwEnabled_${exchangeEvent.event.id}" class="rtwSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.rtwEnabled !=null && exchangeEvent.rtwEnabled}"> checked="checked" </c:if> />
			</display:column>
			
			<%-- <display:column title='TixCity Enabled<br/><input type="checkbox" name="copyAllTixcityEnabled" id="copyAllTixcityEnabled" >' >
				<input type="checkbox" name="tixcityEnabled_${exchangeEvent.event.id}" id="tixcityEnabled_${exchangeEvent.event.id}" class="tixcitySelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.tixcityEnabled !=null && exchangeEvent.tixcityEnabled}"> checked="checked" </c:if> />
			</display:column> --%>
			
			<%-- <display:column title='RTW-2 Enabled<br/><input type="checkbox" name="copyAllRotEnabled" id="copyAllRotEnabled" >' >
				<input type="checkbox" name="rotEnabled_${exchangeEvent.event.id}" id="rotEnabled_${exchangeEvent.event.id}" class="rotSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.rotEnabled !=null && exchangeEvent.rotEnabled}"> checked="checked" </c:if> />
			</display:column> --%>
			
			<%-- <display:column title='Minicats Enabled<br/><input type="checkbox" name="copyAllMinicatsEnabled" id="copyAllMinicatsEnabled" >' >
				<input type="checkbox" name="minicatsEnabled_${exchangeEvent.event.id}" id="minicatsEnabled_${exchangeEvent.event.id}" class="miniSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.minicatsEnabled !=null && exchangeEvent.minicatsEnabled}"> checked="checked" </c:if> />
			</display:column> --%>
			
			<display:column title='Lastrow Minicats Enabled<br/><input type="checkbox" name="copyAllLastrowMinicatsEnabled" id="copyAllLastrowMinicatsEnabled" >' >
				<input type="checkbox" name="lastrowMinicatsEnabled_${exchangeEvent.event.id}" id="lastrowMinicatsEnabled_${exchangeEvent.event.id}" class="lastrowSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.lastrowMinicatsEnabled !=null && exchangeEvent.lastrowMinicatsEnabled}"> checked="checked" </c:if> />
			</display:column>
			
			<%-- <display:column title='VipMinicats Enabled<br/><input type="checkbox" name="copyAllVipMinicatsEnabled" id="copyAllVipMinicatsEnabled" >' >
				<input type="checkbox" name="vipMinicatsEnabled_${exchangeEvent.event.id}" id="vipMinicatsEnabled_${exchangeEvent.event.id}" class="vipminiSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.vipMinicatsEnabled !=null && exchangeEvent.vipMinicatsEnabled}"> checked="checked" </c:if> />
			</display:column> --%>
			
			<display:column title='Vip Lastrow Minicats Enabled<br/><input type="checkbox" name="copyAllVipLastrowMinicatsEnabled" id="copyAllVipLastrowMinicatsEnabled" >' >
				<input type="checkbox" name="vipLastrowMinicatsEnabled_${exchangeEvent.event.id}" id="vipLastrowMinicatsEnabled_${exchangeEvent.event.id}" class="vipLastrowSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.vipLastrowMinicatsEnabled !=null && exchangeEvent.vipLastrowMinicatsEnabled}"> checked="checked" </c:if> />
			</display:column>
			
			<%-- <display:column title='Presale AutoCats Enabled<br/><input type="checkbox" name="copyAllPresaleAutocatsEnabled" id="copyAllPresaleAutocatsEnabled" >' >
				<input type="checkbox" name="presaleAutocatsEnabled_${exchangeEvent.event.id}" id="presaleAutocatsEnabled_${exchangeEvent.event.id}" class="presaleAutocatSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" 
				<c:if test ="${exchangeEvent.presaleAutocatsEnabled !=null && exchangeEvent.presaleAutocatsEnabled}"> checked="checked" </c:if> />
			</display:column> --%>
			
			<%-- <display:column title="Assigned Product">${exchangeEvent.assignedProduct}</display:column>
			<display:column title="Section Ticket Count">${exchangeEvent.sectionTicketCount}</display:column> --%>
			
			<%-- <display:column title='Broker Event Status' >
				${exchangeEvent.event.brokerStatus}
			</display:column> --%>
			<display:column title="Updated By ">${exchangeEvent.lastUpdatedBy}</display:column>
			<display:column title="Updated Date "><fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${exchangeEvent.lastUpdatedDate}" /></display:column>
			<display:column title="Action" >
			 	<input type="button" value="Audit" class="medButton" id="audit_${exchangeEvent.event.id}}"
				onClick="popupUpdateTGCatsPricingAudit('${exchangeEvent.event.id}');"/>
			 	
			 </display:column>
		</display:table>
			
			<br/>
			<div style="float:right; padding-right:320px; margin-right:688px">
				<input type="button" value="Update"    class="medButton" id="update2" name="update2"onclick="javascript:submitForms('update');"/> 
			</div>
			</div>
			
	</c:if>
	<c:if test="${empty exchangeEventList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
		<table id="listAudit"></table>
		<br/>
		<br/>
	</c:if>
	<input type="hidden" name="action" id="action"/>
	<br/>
	<br/>
	
</form:form>

<script type="text/javascript">
var otherFrequentEventsCount = 0;
if('${otherFrequentEventsCount}' != null && '${otherFrequentEventsCount}' != '') {
	otherFrequentEventsCount = '${otherFrequentEventsCount}';
}

function clearArtist(){
	$("#message").hide();
}

$(document).ready(function(){
	var allEvents = '${eventsCheckAll}';
	var isUpdate = 'true';
	function selectCheckBox(){
		if(allEvents=='true'){
			$('#eventsCheckAll').attr("checked","checked");
		}
		allEvents='false';
	}
	selectCheckBox();
	
	function showProgress(event, ui) {
       alert("called from auto " );
    }
	
	 $('#autoArtistVenue').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
		width: 650,		
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		  
			$('#autoArtistVenue').val('');
			$('#selectedValue').text(row[2]);
			if(row[0]=='ARTIST'){
				$('#selectedOption').text('Artist');
				$("#artist").val(row[1]);
				$("#grandChild").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('artistId',row[1],'ARTIST ' , row[2] );
				
			} else if(row[0]=='VENUE'){
				$('#selectedOption').text('Venue');
				$("#venue").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#child").val('');
				getEvents('venueId',row[1], 'VENUE ' , row[2]);
				
			} else if(row[0]=='CHILD'){
				$('#selectedOption').text('Child');
				$("#child").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#venue").val('');
				getEvents('childId',row[1], 'CHILD ' ,row[2]);
				
			} else {
				$('#selectedOption').text('GrandChild');
				$("#grandChild").val(row[1]);
				$("#artist").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('grandChildId',row[1], 'GRANDCHILD ', row[2]);
				
			} 
	}); 

	$("#events").change(function () {
		if(isUpdate!='true'){
			$('#ebayInventoryTabel').hide();
			$("#events option:selected").each(function () {
				$('#eventsCheckAll').removeAttr("checked");
			});
		}
		isUpdate='false'; 
	}).trigger('change');
	
	   
	
});
	
function getEvents(isArtist,id, typeString, selectedName){
	
	$("#loading").show();	
	var url = "";
	var brokerId = $("#brokerId").val();
	if(isArtist == 'artistId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?artistId="+id+"&brokerId="+brokerId;
	}else if(isArtist == 'venueId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?venueId="+id+"&brokerId="+brokerId;
	} else if(isArtist == 'childId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?childId="+id+"&brokerId="+brokerId;;
	} else{
		url = "GetEventsByGrandChildAndArtistAndVenue?grandChildId="+id+"&brokerId="+brokerId;
	}
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			if(jsonData.length ==0)
			 {
				alert(" There are no Events for selected " + typeString + "- "  + selectedName );
			 
			 }
			
			$('#events').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.name+" "+data.date+" "+data.time+" "+data.venue+" "+data.city+" "+data.state+"</option>"
				$('#events').append(rowText);
            }
			$("#loading").hide();
		}
	}); 
} 
function validateForm(){
	
	var weekendFlag = false;
	var today = new Date();
	if(today.getDay() == 6 || today.getDay() == 0) {// 6-saturday,0-sunday
		weekendFlag = true;
	}
		var flag= true;
		$('.selectCheck:checkbox:checked').each(function () {
			
			var id,value;
			id = this.id.replace('checkbox','exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Exposure can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','shipping_days');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Shipping days can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','shipping_method');
			value = $.trim($("#"+id).val());
			/* if(value==''){
				alert('Shipping Method can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			} */
			id = this.id.replace('checkbox','near_term_display_option');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Nearterm Display Option can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','rptFactor');
			value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid RPT Factor.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','priceBreakup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Price Breakup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerMarkup');
			var lowerMarkup = $.trim($("#"+id).val());
			if(lowerMarkup =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(lowerMarkup))){
				alert('Please enter valid Lower Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperMarkup');
			var upperMarkup = $.trim($("#"+id).val());
			if(upperMarkup =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(upperMarkup))){
				alert('Please enter valid Upper Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			if(weekendFlag && (lowerMarkup<=0 || upperMarkup <=0)) {
				alert('Markup Can not be updated to 0% on saturday and sunday.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			
		});
		if(!flag){
			return flag;	
		}
		
		return true;
}
function submitForms(action){
	if(action=='update'){
		if(validateForm()){
			$("#action").val(action);
			$("#formId").submit();
		}
	}else{
		  var event=$("#events").val()
		  if(event!=null){
		    $("#action").val(action);
		    $("#formId").submit();
		  }else{
			  alert('Please select the events.');
		  }
	}

}

$(function () {
	
	$('#copyAllFrequentEvent').click(function(){
		if($('#copyAllFrequentEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isFrequentEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllSectionRange').click(function(){
		if($('#copyAllSectionRange').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.presaleSelect').attr('checked', true);
		//	copyCheckBox('allowSectionRange');
		}else{
			 $('.selectCheck').attr('checked', true); 
			$('.presaleSelect').attr('checked', false);
		}
	});
	$('#copyAllExposure').click(function(){
		if($('#copyAllExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('exposure');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllShippingMethod').click(function(){
		if($('#copyAllShippingMethod').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('shipping_method');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});  
	$('#copyAllNearTermDisplayOption').click(function(){
		if($('#copyAllNearTermDisplayOption').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('near_term_display_option'); 
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyShippingDays').click(function(){
		if($('#copyShippingDays').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('shipping_days'); 
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllLowerMarkup').click(function(){
		if($('#copyAllLowerMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lowerMarkup');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllUpperMarkup').click(function(){
		if($('#copyAllUpperMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('upperMarkup');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllLowerShippingFees').click(function(){
		if($('#copyAllLowerShippingFees').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lowerShippingFees');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllUpperShippingFees').click(function(){
		if($('#copyAllUpperShippingFees').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('upperShippingFees');
		}else{
			$('.selectCheck').attr('checked',true);
		}
	});
	$('#copyAllRptFactor').click(function(){
		if($('#copyAllRptFactor').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('rptFactor');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllPriceBreakup').click(function(){
		if($('#copyAllPriceBreakup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('priceBreakup');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	
	$('#copyAllTicketNetworkEvent').click(function(){
		if($('#copyAllTicketNetworkEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('ticketnetwork_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllVividSeatsEvent').click(function(){
		if($('#copyAllVividSeatsEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('vividseat_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllScoreBigEvent').click(function(){
		if($('#copyAllScoreBigEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('scorebig_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllVividEnabled').click(function(){
		if($('#copyAllVividEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.vividSelect').attr('checked', true);
		//	copyCheckBox('vividEnabled');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.vividSelect').attr('checked', false);
		}
	});
	
	$('#copyAllRotEnabled').click(function(){
		if($('#copyAllRotEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.rotSelect').attr('checked', true);
		//	copyCheckBox('rotEnabled');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.rotSelect').attr('checked', false);
		}
	});
	
	/* $('#copyAllTixcityEnabled').click(function(){
		if($('#copyAllTixcityEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.tixcitySelect').attr('checked', true);
		//	copyCheckBox('tixcityEnabled');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.tixcitySelect').attr('checked', false);
		}
	}); */
	
	$('#copyAllRtwEnabled').click(function(){
		if($('#copyAllRtwEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.rtwSelect').attr('checked', true);
		//	copyCheckBox('rtwEnabled');
		}else{
		    $('.selectCheck').attr('checked', true);
			$('.rtwSelect').attr('checked', false);
		}
	});
	
	$('#copyAllMinicatsEnabled').click(function(){
		if($('#copyAllMinicatsEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.miniSelect').attr('checked', true);
		//	copyCheckBox('minicatsEnabled');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.miniSelect').attr('checked', false);
		}
	});
	
	$('#copyAllLastrowMinicatsEnabled').click(function(){
		if($('#copyAllLastrowMinicatsEnabled').attr('checked')){
		//	$('.selectCheck').attr('checked', true);
			$('.lastrowSelect').attr('checked', true);
		//	copyCheckBox('lastrowMinicatsEnabled');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.lastrowSelect').attr('checked', false);
		}
	});
	
	$('#copyAllVipLastrowMinicatsEnabled').click(function(){
		if($('#copyAllVipLastrowMinicatsEnabled').attr('checked')){
		//	$('.selectCheck').attr('checked', true);
			$('.vipLastrowSelect').attr('checked', true);
		//	copyCheckBox('vipLastrowMinicatsEnabled');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.vipLastrowSelect').attr('checked', false);
		}
	});
	
	$('#copyAllVipMinicatsEnabled').click(function(){
		if($('#copyAllVipMinicatsEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.vipminiSelect').attr('checked', true);
		//	copyCheckBox('vipMinicatsEnabled');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.vipminiSelect').attr('checked', false);
		}
	});
	
	$('#copyAllPresaleAutocatsEnabled').click(function(){
		if($('#copyAllPresaleAutocatsEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.presaleAutocat').attr('checked', true);
		//	copyCheckBox('presaleAutocatsEnabled');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.presaleAutocat').attr('checked', false);
		}
	});
	
	
	$('#copyAll').click(function(){
		copyAll();
	});
	
});

function copyAll(){
	if($('#copyAll').attr('checked')){
		$('.selectCheck').attr('checked', true);
		copyCheckBox('discount_zone');
		copyCheckBox('allowSectionRange');
		//copyCheckBox('minicatsEnabled');
		copyCheckBox('lastrowMinicatsEnabled');
		copyCheckBox('vipLastrowMinicatsEnabled');
		//copyCheckBox('vipMinicatsEnabled');
		//copyCheckBox('presaleAutocatsEnabled');
		//copyCheckBox('vividEnabled');
		//copyCheckBox('rotEnabled');
		//copyCheckBox('tixcityEnabled');
		copyCheckBox('rtwEnabled');
		
		copyTextField('exposure');
		
		copyTextField('shipping_method');
		copyTextField('near_term_display_option'); 
		copyTextField('shipping_days');    
		
		copyTextField('lowerMarkup');
		copyTextField('upperMarkup');
		copyTextField('lowerShippingFees');
		copyTextField('upperShippingFees');
		copyTextField('rptFactor');
		copyTextField('priceBreakup');
		copyTextField('ticketnetwork_broker');
		copyTextField('vividseat_broker');
		
		copyTextField('scorebig_broker');
		
		
	}else{
		$('.selectCheck').attr('checked', false);
	}
}
function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}
function selectRow(id) {
	$("#checkbox_" + id ).attr('checked', true);
}

function selectAllEvents(){
	if((document.getElementById("eventsCheckAll").checked)){
		$("#events").each(function(){
			$("#events option").attr("selected","selected"); 
		});

	}
	else{
			$("#events").each(function(){
			$("#events option").removeAttr("selected"); 
		});
	}
}

function popupUpdateTGCatsPricingAudit(eId){
	var url = "${auditUrl}?eventId="+eId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
} 
	
</script>