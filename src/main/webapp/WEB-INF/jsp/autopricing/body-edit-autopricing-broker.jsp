<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

	function validateForm(){
		var flag= true;
			
			var value;
			
			value = $.trim($("#emailId").val());
			if(value==''){
				alert('Please enter valid EmailId.');
				$("#emailId").focus();
				flag = false;
				return false;
			}
			value = $.trim($("#emailSuffix").val());
			if(value==''){
				alert('Please enter valid Email Suffix.');
				$("#emailSuffix").focus();
				flag = false;
				return false;
			}
			
			/*value = $.trim($("#phoneNo").val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid PhoneNo.');
				$("#phoneNo").focus();
				flag = false;
				return false;
			}*/
			value = $.trim($("#posBrokerId").val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid POS Broker Id.');
				$("#posBrokerId").focus();
				flag = false;
				return false;
			}
			value = $.trim($("#systemUserId").val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid System User Id.');
				$("#systemUserId").focus();
				flag = false;
				return false;
			}
			value = $.trim($("#buyerBrokerId").val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid POS Buyer Broker Id.');
				$("#buyerBrokerId").focus();
				flag = false;
				return false;
			}
			value = $.trim($("#clientBrokerId").val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid POS Client Broker Id.');
				$("#clientBrokerId").focus();
				flag = false;
				return false;
			}
			value = $.trim($("#clientBrokerEmployeeId").val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid POS Client Broker Employee Id.');
				$("#clientBrokerEmployeeId").focus();
				flag = false;
				return false;
			}
			value = $.trim($("#minutePOSUpdateLimit").val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid POS Minute Update Limit.');
				$("#minutePOSUpdateLimit").focus();
				flag = false;
				return false;
			}
			
		if(!flag){
			return flag;	
		}
		
		return true;
	}
	function submitForms(action){
	if(action=='update'){
		if(validateForm()){
			$("#action").val(action);
			$("#formId").submit();
		}
	}else{
		$("#action").val(action);
		$("#formId").submit();
	}
}
</script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > <a href="ManageAutoPricingBroker">Manage AutoPricing Brokers</a> > Edit AutoPricing Broker
</div>
<h1>Edit AutoPricing Brokers</h1>
<br/>
<hr/>
<form:form action="EditAutoPricingBroker" method="post" id="formId">
<input type="hidden" name="action" id="action"/>
<input type="hidden" name="brokerId" value="${broker.id}" />

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	<table align="center">
		<tr>
			<td><b>Name</b></td>
			<td>${broker.name}</td>
		</tr>
		<tr>
			<td><b>Email Id</b></td>
			<td><input type="text" id="emailId" name="emailId" value="<c:out value='${broker.emailId}' />" style="width: 220px;" /></td>
		</tr>
		<tr>
			<td><b>SMTP Email suffix</b></td>
			<td><input type="text" id="emailSuffix" name="emailSuffix" value="<c:out value='${broker.emailSuffix}' />" style="width: 220px;" />&nbsp;&nbsp;<b>e.g( @example.com)</b></td>
		</tr>
		<tr>
			<td><b>Phone No</b></td>
			<td><input type="text" id="phoneNo" name="phoneNo" value="<c:out value='${broker.phoneNo}' />" style="width: 220px;" /></td>
		</tr>
		<tr>
			<td><b>POS Broker Id</b></td>
			<td><input type="text" id="posBrokerId" name="posBrokerId" value="<c:out value='${broker.posBrokerId}' />" style="width: 220px;" /></td>
		</tr>
		<tr>
			<td><b>POS System User Id</b></td>
			<td><input type="text" id="systemUserId" name="systemUserId" value="<c:out value='${broker.systemUserId}' />" style="width: 220px;" /></td>
		</tr>
		<tr>
			<td><b>POS Buyer Broker Id</b></td>
			<td><input type="text" id="buyerBrokerId" name="buyerBrokerId" value="<c:out value='${broker.buyerBrokerId}' />" style="width: 220px;" /></td>
		</tr>
		<tr>
			<td><b>POS Client Broker Id</b></td>
			<td><input type="text" id="clientBrokerId" name="clientBrokerId" value="<c:out value='${broker.clientBrokerId}' />" style="width: 220px;" /></td>
		</tr>
		<tr>
			<td><b>POS Client Broker Employee Id</b></td>
			<td><input type="text" id="clientBrokerEmployeeId" name="clientBrokerEmployeeId" value="<c:out value='${broker.clientBrokerEmployeeId}' />" style="width: 220px;" /></td>
		</tr>
		<tr>
			<td><b>POS Minute Update Limit</b></td>
			<td><input type="text" id="minutePOSUpdateLimit" name="minutePOSUpdateLimit" value="<c:out value='${broker.minutePOSUpdateLimit}' />" style="width: 220px;" /></td>
		</tr>
		
		<tr>
			<td colspan="2" align="center">
			<br />
				<input type="button" value="Update" onclick="submitForms('update');" class="medButton">
			</td>
		</tr>
</table>	
	<br />

</form:form>