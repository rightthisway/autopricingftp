
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%-- <jsp:include page="../decorators.jsp" /> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<div>
	<b>Product : ${product.name}</b>
	<br />
	<b>Venue : ${venue.building}</b>
	<br/>
	 
</div> 
<br />
<br/>
<display:table list="${auditList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="audit"  requestURI="#" class="list">
	<display:column  title ="Action">${audit.action}</display:column>										
	<display:column title="Updated By ">${audit.lastUpdatedBy}</display:column>
	<display:column title="Updated Date "><fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.lastUpdatedDate}" /></display:column>
	 <display:column title='Shipping Method' >
		<c:choose>
			<c:when test="${audit.shippingMethod == null}">
				-
			</c:when>
			<c:otherwise>
				<c:if test ="${audit.shippingMethod eq '0'}" >Default Website Setting</c:if>
				<c:if test ="${audit.shippingMethod eq '1'}" >E-Ticket</c:if>
				<c:if test ="${audit.shippingMethod eq '2'}" >Will Call</c:if>
				<c:if test ="${audit.shippingMethod eq '3'}" >Local Pickup Near Venue</c:if>
				<c:if test ="${audit.shippingMethod eq '4'}" >E-Ticket or Will Call</c:if>
				<c:if test ="${audit.shippingMethod eq '5'}" >Will Call or Local Pickup Near Venue</c:if>
				<c:if test ="${audit.shippingMethod eq '6'}" >E-Ticket or Local Pickup Near Venue</c:if>
				<c:if test ="${audit.shippingMethod eq '7'}" >E-Ticket or Will Call or Local Pickup Near Venue</c:if>
				<c:if test ="${audit.shippingMethod eq '8'}" >Paperless (Meet Seller at Venue)</c:if>
				<c:if test ="${audit.shippingMethod eq '9'}" >Electronic Transfer</c:if>
			</c:otherwise>
		</c:choose>
	</display:column>  
	<display:column title='Near Term Display Option' >
		<c:choose>
			<c:when test="${audit.nearTermDisplayOption == null}">
				-
			</c:when>
			<c:otherwise>
				<c:if  test="${audit.nearTermDisplayOption eq '0'}">Default near-term display options</c:if>
				<c:if  test ="${audit.nearTermDisplayOption eq '1'}">Always show near-term shipping</c:if>
				<c:if  test ="${audit.nearTermDisplayOption eq '2'}" >Only show near-term shipping</c:if>
			</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='Shipping Days' >
		<c:choose>
			<c:when test="${audit.shippingDays !=null}">
				${audit.shippingDays} Days
			</c:when>
			<c:otherwise>
				-
			</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='Markup' >
			<c:choose>
			<c:when test="${audit.markup !=null}">
				${audit.markup}
			</c:when>
			<c:otherwise>
				-
			</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='Existing Event Updated' >
			<c:choose>
			<c:when test="${audit.existingEventUpdate !=null && audit.existingEventUpdate}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>
	
</display:table>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>
			
			
