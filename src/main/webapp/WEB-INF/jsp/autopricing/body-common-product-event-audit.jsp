
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%-- <jsp:include page="../decorators.jsp" /> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<br/>
<br/>
<display:table list="${exchangeEventList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="exchangeEvent"  requestURI="#" class="list">
	<display:column title="Updated By ">${exchangeEvent.lastUpdatedBy}</display:column>
	<display:column title="Updated Date "><fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${exchangeEvent.lastUpdatedDate}" /></display:column>
	<display:column title="EvenName" sortable="true">${exchangeEvent.event.name}</display:column>
	<display:column title="Event Date And Time" sortable="true">
		<fmt:formatDate pattern="MM/dd/yyyy" value="${exchangeEvent.event.localDate}" />				
			<c:choose>
				<c:when test="${exchangeEvent.event.localTime == null}">
					TBD
				</c:when>
				<c:otherwise>
					<fmt:formatDate pattern="hh:mm aa" value="${exchangeEvent.event.localTime}" />
				</c:otherwise>
			</c:choose>
	</display:column>
	<display:column title="Venue" sortable="true">${exchangeEvent.event.venue.building},${exchangeEvent.event.venue.state},${exchangeEvent.event.venue.city},${exchangeEvent.event.venue.country}</display:column>
	<display:column title='Allow Section Range'> 
		<c:choose>
			<c:when test="${exchangeEvent.allowSectionRange !=null && exchangeEvent.allowSectionRange}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>
	
	<display:column property="exposure" title="Exposure"/>
	 <display:column title='Shipping Method' >
		<c:choose>
			<c:when test="${exchangeEvent.shippingMethod == null}">
				-
			</c:when>
			<c:otherwise>
				<c:if test ="${exchangeEvent.shippingMethod eq '0'}" >Default Website Setting</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '1'}" >E-Ticket</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '2'}" >Will Call</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '3'}" >Local Pickup Near Venue</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '4'}" >E-Ticket or Will Call</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '5'}" >Will Call or Local Pickup Near Venue</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '6'}" >E-Ticket or Local Pickup Near Venue</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '7'}" >E-Ticket or Will Call or Local Pickup Near Venue</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '8'}" >Paperless (Meet Seller at Venue)</c:if>
				<c:if test ="${exchangeEvent.shippingMethod eq '9'}" >Electronic Transfer</c:if>
			</c:otherwise>
		</c:choose>
	</display:column>  
	<display:column title='Near Term Display Option' >
		<c:choose>
			<c:when test="${exchangeEvent.nearTermDisplayOption eq '0'}">Default near-term display options</c:when>
			<c:when test ="${exchangeEvent.nearTermDisplayOption eq '1'}">Always show near-term shipping</c:when>
			<c:when test ="${exchangeEvent.nearTermDisplayOption eq '2'}" >Only show near-term shipping</c:when>
		</c:choose>
	</display:column>
	
	<display:column title='Shipping Days' >
			${exchangeEvent.shippingDays} Days
	</display:column>
	
	<display:column title='RPT Factor'>
		${exchangeEvent.rptFactor}%
	</display:column>
	<display:column title='Price Breakup' >
		$${exchangeEvent.priceBreakup} 
	</display:column>
	<display:column title='Lower Markup' >
		$${exchangeEvent.lowerMarkup}
	</display:column>
	<display:column title='Upper Markup' >
		$${exchangeEvent.upperMarkup}
	</display:column>
	
	<display:column title='Lower Shipping Fees' >
		$${exchangeEvent.lowerShippingFees}
	</display:column>
	
	<display:column title='Upper Shipping Fees' >
		$${exchangeEvent.upperShippingFees}
	</display:column>
	
	<c:if test="${not empty product and product.id eq 9}">
		<display:column title='Tax Percentage' >
			${exchangeEvent.taxPercentage}
		</display:column>
		<display:column title='Exclude Hours Before Event' >
			${exchangeEvent.excludeHoursBeforeEvent}
		</display:column>
	</c:if>
	
	<c:if test="${empty product or (product.id ne 3 and product.id ne 4 and product.id ne 5 and product.id ne 9)}">
	<display:column title='TicketNetwork Event' >
		<c:choose>
			<c:when test="${exchangeEvent.ticketNetworkBroker!=null}">
				${exchangeEvent.ticketNetworkBroker.name}
			</c:when>
			<c:otherwise>
				-
			</c:otherwise>
		</c:choose>
	</display:column>
	</c:if>
	<c:if test="${empty product or (product.id ne 9)}">
		<display:column title='VividSeats Event' >
			<c:choose>
				<c:when test="${exchangeEvent.vividBroker!=null}">
					${exchangeEvent.vividBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title='ScoreBig Event'>
			<c:choose>
				<c:when test="${exchangeEvent.scoreBigBroker!=null}">
					${exchangeEvent.scoreBigBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
		
		<display:column title='FanXchange Event'>
			<c:choose>
				<c:when test="${exchangeEvent.fanxchangeBroker!=null}">
					${exchangeEvent.fanxchangeBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
		
		<display:column title='TicketCity Event'>
			<c:choose>
				<c:when test="${exchangeEvent.ticketcityBroker!=null}">
					${exchangeEvent.ticketcityBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
		
			<display:column title='SeatGeek Event'>
			<c:choose>
				<c:when test="${exchangeEvent.seatGeekBroker!=null}">
					${exchangeEvent.seatGeekBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
	</c:if>
	
	<c:if test="${not empty product and (product.id eq 9)}">
		<display:column title='ZoneTicket Broker'>
			<c:choose>
				<c:when test="${exchangeEvent.zoneTicketBroker!=null}">
					${exchangeEvent.zoneTicketBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
	</c:if>
	<display:column title='Discount Zone'> 
		<c:choose>
			<c:when test="${exchangeEvent.discountZone !=null && exchangeEvent.discountZone}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='Zone Discount'>
		<c:choose>
			<c:when test="${exchangeEvent.zoneDiscountPerc!=null}">
				${exchangeEvent.zoneDiscountPerc}
			</c:when>
			<c:otherwise>
				-
			</c:otherwise>
		</c:choose>
	</display:column>
</display:table>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>
			
			
