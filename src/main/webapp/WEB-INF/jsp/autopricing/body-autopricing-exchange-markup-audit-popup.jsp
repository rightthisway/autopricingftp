
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%-- <jsp:include page="../decorators.jsp" /> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<c:if test="${not empty auditList}" >
	<div id="auditTable" >

		<display:table list="${auditList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="audit"  requestURI="#" class="list">

			<display:column  title ="Action">${audit.action}</display:column>
			<display:column title="User">${audit.lastUpdatedBy}</display:column>
			<display:column title="Last Updated Date">
				<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.lastUpdated}" />					
			</display:column>
			<display:column title="Exchange Name">${audit.exchangeName}</display:column>
			<display:column title="Additional Markup">
				<c:choose>
					<c:when test="${audit.additionalMarkup == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.additionalMarkup}
					</c:otherwise>
				</c:choose>
			</display:column>
				
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty auditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>