<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> > Broker Connection Status
</div>
<display:table id="induxStatus"  requestURI="ManageBrokersConnections" class="list" list="${induxConnectionStatusList}">
	<display:column title="Broker Id" property="brokerId" /> 
	<display:column title="Broker Name" property="brokerName" /> 
	<display:column title="Broker AutoCats Ticket Count">
		${induxStatus.catTixCount} &nbsp <input type="button" value="Details"    class="medButton" id="catTicketInfo_${tgCatTicket.id}"
				onClick="showBrokerTicketsCountProductWise('${induxStatus.brokerId}');"/>
	</display:column> 
	<display:column title="Broker Connection Status" property="connectionStatus" /> 
	<display:column title="Broker Error" property="error" /> 
</display:table>

<script type="text/javascript">
	function showBrokerTicketsCountProductWise(brokerId){
		var url = "BrokerTicketsCountProductWisePopup?brokerId="+brokerId;
		newwindow = window.open(url,'name','height=500,width=1200,scrollbars=yes');
		if(window.focus) {newwindow.focus();}
		return false;
	}
</script>