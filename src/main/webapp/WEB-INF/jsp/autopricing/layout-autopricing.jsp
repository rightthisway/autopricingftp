<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="userSubMenu">
	
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Event Management'}">
			<b>Event Management</b>
		</c:when>
		<c:otherwise>
			<a href="ManageAutoCats96ExchangeEvent">Event Management</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Exclude Event Zones'}">
			<b>Exclude Event Zones</b>
		</c:when>
		<c:otherwise>
			<a href="ExcludeEventZones">Exclude Event Zones</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Exclude VenueCategory Zones'}">
			<b>Exclude VenueCategory Zones</b>
		</c:when>
		<c:otherwise>
			<a href="ExcludeVenueCategoryZones">Exclude VenueCategory Zones</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Log Files'}">
			<b>LogFiles</b>
		</c:when>
		<c:otherwise>
			<a href="AutopricingEditLogFiles">Log Files</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'AutoPricing Ticket Details'}">
			<b>AutoPricing Tickets Details</b>
		</c:when>
		<c:otherwise>
			<a href="AutopricingTicketDetails">AutoPricing Ticket Details</a>
		</c:otherwise>
	</c:choose>
	
	</div>
	
	
<tiles:insertAttribute name="subBody" />