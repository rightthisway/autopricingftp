<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
<meta http-equiv="Content-type" content="text/plain; charset=UTF-8"/>
<style media="screen" type="text/css">
#loading{
display:none;
margin-left:105px
}

</style>
</head>
<div id="userSubMenu"> 
	<jsp:include page="autopricing-exchange-event-user-sub-menu.jsp"></jsp:include>
</div>

<c:set var="auditUrl" value="${auditUrl}"/>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > ${url}
</div>
<h1>Manage Reward The Fan Listings Exchange Event</h1>
<br/>


<form:form action="${url}" method="post" id="formId">

<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br/>
	<br/>
	<table width="80%">
		<%-- <tr>
			<td><b>Select Broker:</b></td>
			<td>
				<select id="brokerId" name="brokerId">
					<option value="">---ALL---</option>
					<c:forEach items="${brokers}" var="brokerObj">
						<option value="${brokerObj.id }"
						<c:if test="${broker ne null and brokerObj.id eq broker}"> Selected </c:if>
						>${brokerObj.name}</option>
					</c:forEach>
				</select>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr> --%>
		  <tr>
		  <td></td>
		  <td>
		      <div id="message" style="color: red;"><b>${message}</b></div>
		  </td>
		  <td></td>		
		  <td></td>
		<tr>
			<td><b>Select Child or GrandChild or Artist or Venue:</b></td>
			<td>
				<input type="hidden" id="artist" name="artist" value ="${artist}">
				<input type="hidden" id="venue" name="venue" value ="${venue}">
				<input type="hidden" id="grandChild" name="grandChild" value ="${grandChild}">
				<input type="hidden" id="brokerId" name="brokerId" value ="${brokerId}">
				<input type="text" id="autoArtistVenue" name="autoArtistVenue" onclick="clearArtist();" >
				<img id="loading" src="/autopricingweb/images/progress-img.gif" border="0" height="50" width="75">
			</td>
			<td>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b><span id="selectedOption" >${selectedOption}</span></b></td>
			<td><b><span id="selectedValue" >${selectedValue}</span></b></td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Events:</b></td>
			<td >
				<input type="hidden" id="eventStr" name="eventStr" value = ${eventStr}/> 
				<input type="checkbox" id="eventsCheckAll" name="eventsCheckAll" onclick ="selectAllEvents()" <c:if test ="${eventsCheckAll}"> checked="checked" </c:if>/> 
				<label for="eventsCheckAll">Select All</label>
				<br/>
				<select id='events' name="events"  multiple="multiple" size='6' style="width:480px;" >
					<c:forEach items="${events}" var="event">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${event.id}${temp}"/>
						<option 
							<c:if test="${event!=null && fn:contains(eventStr,temp2)}"> Selected </c:if>
							value="${event.id}"> ${event.name} <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								${event.venue.building}
						</option>
					</c:forEach> 
				</select>
			</td>
			<td></td>
			<td colspan="3" align="center"></td>
			
		</tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>			
		
		<tr>
			<td colspan="6" align="center">
				<input type="button" value="Get Pricing" onclick="submitForms('search');" class="medButton">
			</td>
		</tr>
		<tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>	
		<tr>
		    <td>
			 <b> Apply the rules for :</b>
			</td>
			<td>		
		      <c:forEach items="${products}" var="product">
	             <input type="checkbox" name="product-${product.id}" value="${product.id}" id="product-${product.id}"/> ${product.name}
              </c:forEach>
			</td>
		</tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>
	</table>
	
	<c:if test="${not empty exchangeEventList}" >
	<div id="ebayInventoryTabel" >
	
		
		<div style="float:right; padding-right:320px; margin-right:688px">
			<input type="button" value="Update"  name="update1" id="update1" class="medButton" onclick="javascript:submitForms('update');"/> 
		</div>
		<div id="copyCheckBox" style="float:right;">
			<!--<input onclick="javascript:copyAllEventData();" type="checkbox" name="copyAllCheckbox" id="copyAllCheckbox" >Copy first row  to all remaining rows &nbsp;&nbsp;&nbsp; -->
		</div>
		<br/>
		<br/>
		<br/>
		<display:table list="${exchangeEventList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="exchangeEvent"  requestURI="#" class="list">
			<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
				<input type="checkbox" name="checkbox_${exchangeEvent.event.id}" id="checkbox_${exchangeEvent.event.id}" <c:if test ="${exchangeEvent.status !=null && exchangeEvent.status == 'ACTIVE'  }"> checked="checked" </c:if> class="selectCheck" />
			</display:column>
			<display:column title="EvenName" sortable="true">${exchangeEvent.event.name}</display:column>
			<display:column title="Event Date And Time" sortable="true">

				<fmt:formatDate pattern="MM/dd/yyyy" value="${exchangeEvent.event.localDate}" />				
					<c:choose>
						<c:when test="${exchangeEvent.event.localTime == null}">
							TBD
						</c:when>
						<c:otherwise>
							<fmt:formatDate pattern="hh:mm aa" value="${exchangeEvent.event.localTime}" />
						</c:otherwise>
					</c:choose>
			</display:column>
			<display:column title="Venue" sortable="true">${exchangeEvent.event.venue.building},${exchangeEvent.event.venue.state},${exchangeEvent.event.venue.city},${exchangeEvent.event.venue.country}</display:column>
			<%-- <display:column title='Presale Event' sortable="true">
				<c:choose>
						<c:when test="${exchangeEvent.event.presaleEvent}">Yes</c:when>
						<c:otherwise>No</c:otherwise>
				</c:choose>
			</display:column> --%>
			<display:column title='Allow Section Range<br/><input type="checkbox" name="copyAllSectionRange" id="copyAllSectionRange" >' >
				<input type="checkbox" name="allowSectionRange_${exchangeEvent.event.id}" id="allowSectionRange_${exchangeEvent.event.id}" class="presaleSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" <c:if test ="${exchangeEvent.allowSectionRange !=null && exchangeEvent.allowSectionRange}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='Exposure<br/><input type="checkbox" name="copyAllExposure" id="copyAllExposure" >' >
				<select name="exposure_${exchangeEvent.event.id}" id="exposure_${exchangeEvent.event.id}" class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${exchangeEvent.exposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${exchangeEvent.exposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${exchangeEvent.exposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			
			<display:column title='Shipping Method<br/><input type="checkbox" name="copyAllShippingMethod" id="copyAllShippingMethod" >' >
				<select name="shipping_method_${exchangeEvent.event.id}" id="shipping_method_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- Select ---</option> 
					<option value="0" <c:if test ="${exchangeEvent.shippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${exchangeEvent.shippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${exchangeEvent.shippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${exchangeEvent.shippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${exchangeEvent.shippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${exchangeEvent.shippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${exchangeEvent.shippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${exchangeEvent.shippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${exchangeEvent.shippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${exchangeEvent.shippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
					
				</select>
			</display:column> 
			
			<display:column title='Near Term Display Option<br/><input type="checkbox" name="copyAllNearTermDisplayOption" id="copyAllNearTermDisplayOption" >' >
				<select name="near_term_display_option_${exchangeEvent.event.id}" id="near_term_display_option_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option> 
					<option value="0" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${exchangeEvent.nearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
			</display:column>
			
			<display:column title='Shipping Days<br/><input type="checkbox" name="copyShippingDays" id="copyShippingDays" >' >
				<select name="shipping_days_${exchangeEvent.event.id}" id="shipping_days_${exchangeEvent.event.id}" 
						class="shippingMethodText" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- Select ---</option> 
						<option value="0" <c:if test ="${exchangeEvent.shippingDays eq '0'}" > selected </c:if> >0 Days</option>
					<option value="2" <c:if test ="${exchangeEvent.shippingDays eq '2'}" > selected </c:if> >2 Days</option>
					<option value="4" <c:if test ="${exchangeEvent.shippingDays eq '4'}" > selected </c:if> >4 Days</option>
				</select>
			</display:column>
			
			<display:column title='RPT Factor<br/><input type="checkbox" name="copyAllRptFactor" id="copyAllRptFactor" >' >
				<input type="text" value="${exchangeEvent.rptFactor}" name="rptFactor_${exchangeEvent.event.id}" id="rptFactor_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Price Breakup<br/><input type="checkbox" name="copyAllPriceBreakup" id="copyAllPriceBreakup" >' >
				<input type="text" value="${exchangeEvent.priceBreakup}" name="priceBreakup_${exchangeEvent.event.id}" id="priceBreakup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Lower Markup<br/><input type="checkbox" name="copyAllLowerMarkup" id="copyAllLowerMarkup" >' >
				<input type="text" value="${exchangeEvent.lowerMarkup}" name="lowerMarkup_${exchangeEvent.event.id}" id="lowerMarkup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Upper Markup<br/><input type="checkbox" name="copyAllUpperMarkup" id="copyAllUpperMarkup" >' >
				<input type="text" value="${exchangeEvent.upperMarkup}" name="upperMarkup_${exchangeEvent.event.id}" id="upperMarkup_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Lower Shipping Fees<br/><input type="checkbox" name="copyAllLowerShippingFees" id="copyAllLowerShippingFees" >' >
				<input type="text" value="${exchangeEvent.lowerShippingFees}" name="lowerShippingFees_${exchangeEvent.event.id}" id="lowerShippingFees_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Upper Shipping Fees<br/><input type="checkbox" name="copyAllUpperShippingFees" id="copyAllUpperShippingFees" >' >
				<input type="text" value="${exchangeEvent.upperShippingFees}" name="upperShippingFees_${exchangeEvent.event.id}" id="upperShippingFees_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<c:if test="${not empty product and product.id eq 9}">
				<display:column title='Tax Percentage<br/><input type="checkbox" name="copyAllTaxPercentage" id="copyAllTaxPercentage" >' >
					<input type="text" value="${exchangeEvent.taxPercentage}" name="taxPercentage_${exchangeEvent.event.id}" id="taxPercentage_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
				</display:column>
				<display:column title='Exclude Hours Before Event<br/><input type="checkbox" name="copyAllExcludeHoursBeforeEvent" id="copyAllExcludeHoursBeforeEvent" >' >
					<input type="text" value="${exchangeEvent.excludeHoursBeforeEvent}" name="excludeHoursBeforeEvent_${exchangeEvent.event.id}" id="excludeHoursBeforeEvent_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="3" class="markupText"/>
				</display:column>
			</c:if>
			
			<%-- <display:column title='TicketNetwork Event<br/><input type="checkbox" name="copyAllTicketNetworkEvent" id="copyAllTicketNetworkEvent" >' >
				<input type="checkbox" name="isTicketNetwork_${exchangeEvent.event.id}" id="isTicketNetwork_${exchangeEvent.event.id}" <c:if test ="${exchangeEvent.isTicketNetwork!=null && exchangeEvent.isTicketNetwork}"> checked="checked" </c:if> />
			</display:column> --%>
			
			<c:if test="${empty product or (product.id ne 3 and product.id ne 4 and product.id ne 5 and product.id ne 9)}">
				<display:column title='TicketNetwork Event<br/><input type="checkbox" name="copyAllTicketNetworkEvent" id="copyAllTicketNetworkEvent" >' >
					<select name="ticketnetwork_broker_${exchangeEvent.event.id}" id="ticketnetwork_broker_${exchangeEvent.event.id}" class="ticketnetworkBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.ticketNetworkBrokerId}" > selected </c:if> >${broker.name}</option>
						</c:forEach>
					</select>
				</display:column>
			</c:if>
			
			<c:if test="${empty product or (product.id ne 9)}">
				<display:column title='VividSeats Event<br/><input type="checkbox" name="copyAllVividSeatsEvent" id="copyAllVividSeatsEvent" >' >
					<select name="vividseat_broker_${exchangeEvent.event.id}" id="vividseat_broker_${exchangeEvent.event.id}" class="vividBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.vividBrokerId}" > selected </c:if> >${broker.name}</option>
						</c:forEach>
					</select>
				</display:column>
				<display:column title='ScoreBig Event<br/><input type="checkbox" name="copyAllScoreBigEvent" id="copyAllScoreBigEvent" >' >
					<select name="scorebig_broker_${exchangeEvent.event.id}" id="scorebig_broker_${exchangeEvent.event.id}" class="scoreBigBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.scoreBigBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach>
					</select>
				
				</display:column>
				<display:column title='FanXchange Event<br/><input type="checkbox" name="copyAllFanxchangeEvent" id="copyAllFanxchangeEvent" >' >
					<select name="fanxchange_broker_${exchangeEvent.event.id}" id="fanxchange_broker_${exchangeEvent.event.id}" class="fanxchangeBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.fanxchangeBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach>
					</select>
				
				</display:column>
				<display:column title='TicketCity Event<br/><input type="checkbox" name="copyAllTicketcityEvent" id="copyAllTicketcityEvent" >' >
					<select name="ticketcity_broker_${exchangeEvent.event.id}" id="ticketcity_broker_${exchangeEvent.event.id}" class="ticketcityBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.ticketcityBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach>
					</select>
				
				</display:column>
				
				<display:column title='SeatGeek Event<br/><input type="checkbox" name="copyAllSeatGeekEvent" id="copyAllSeatGeekEvent" >' >
					<select name="seatgeek_broker_${exchangeEvent.event.id}" id="seatgeek_broker_${exchangeEvent.event.id}" class="seatgeekBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.seatGeekBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach>
					</select>
				</display:column>
			</c:if>		
			<c:if test="${not empty product and (product.id eq 9)}">
				<display:column title='RewardTheFan Event<br/><input type="checkbox" name="copyAllZoneTicketsEvent" id="copyAllZoneTicketsEvent" >' >
					<select name="zoneTicket_broker_${exchangeEvent.event.id}" id="zoneTicket_broker_${exchangeEvent.event.id}" class="zoneTicketBroker" onchange="selectRow('${exchangeEvent.event.id}');" style="width:100px;" >
						<option value="">--- No Broker ---</option>
						<option value="1" <c:if test ="${1 eq exchangeEvent.zoneTicketBrokerId}" > selected </c:if> >MZTix</option>
						<%-- <c:forEach items="${brokers}" var="broker">
							<option value="${broker.id}" <c:if test ="${broker.id eq exchangeEvent.zoneTicketBrokerId}" > selected </c:if> >${broker.name}</option>	
						</c:forEach> --%>
					</select>
				</display:column>
			</c:if>
			
			<display:column title='Discount Zone<br/><input type="checkbox" name="copyAllDiscountZone" id="copyAllDiscountZone" >' >
				<input type="checkbox" name="discount_zone_${exchangeEvent.event.id}" id="discount_zone_${exchangeEvent.event.id}" class="discountSelect"
				onchange="selectRow('${exchangeEvent.event.id}');" <c:if test ="${exchangeEvent.discountZone !=null && exchangeEvent.discountZone}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='Zone Discount<br/><input type="checkbox" name="copyAllZoneDiscount" id="copyAllZoneDiscount" >' >
				<input type="text" value="${exchangeEvent.zoneDiscountPerc}" name="zoneDiscountPerc_${exchangeEvent.event.id}" 
				id="zoneDiscountPerc_${exchangeEvent.event.id}" onchange="selectRow('${exchangeEvent.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<%-- <display:column title='Broker Event Status' >
				${exchangeEvent.event.brokerStatus}
			</display:column> --%>
			<display:column title="Updated By ">${exchangeEvent.lastUpdatedBy}</display:column>
			<display:column title="Updated Date "><fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${exchangeEvent.lastUpdatedDate}" /></display:column>
			<display:column title="Action" >
			 	<input type="button" value="Audit" class="medButton" id="audit_${exchangeEvent.event.id}}"
				onClick="popupUpdateTGCatsPricingAudit('${exchangeEvent.event.id}');"/>
			 	
			 </display:column>
		</display:table>
			
			<br/>
			<div style="float:right; padding-right:320px; margin-right:688px">
				<input type="button" value="Update"    class="medButton" id="update2" name="update2"onclick="javascript:submitForms('update');"/> 
			</div>
			</div>
			
	</c:if>
	<c:if test="${empty exchangeEventList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
		<table id="listAudit"></table>
		<br/>
		<br/>
	</c:if>
	<input type="hidden" name="action" id="action"/>
	<br/>
	<br/>
	
</form:form>

<script type="text/javascript">
var otherFrequentEventsCount = 0;
if('${otherFrequentEventsCount}' != null && '${otherFrequentEventsCount}' != '') {
	otherFrequentEventsCount = '${otherFrequentEventsCount}';
}

function clearArtist(){
	$("#message").hide();
}

$(document).ready(function(){
	var allEvents = '${eventsCheckAll}';
	var isUpdate = 'true';
	function selectCheckBox(){
		if(allEvents=='true'){
			$('#eventsCheckAll').attr("checked","checked");
		}
		allEvents='false';
	}
	selectCheckBox();
	
	
	 $('#autoArtistVenue').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		  
			$('#autoArtistVenue').val('');
			$('#selectedValue').text(row[2]);
			if(row[0]=='ARTIST'){
				$('#selectedOption').text('Artist');
				$("#artist").val(row[1]);
				$("#grandChild").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('artistId',row[1], 'ARTIST ' , row[2] );
				
			} else if(row[0]=='VENUE'){
				$('#selectedOption').text('Venue');
				$("#venue").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#child").val('');
				getEvents('venueId',row[1], 'VENUE ' , row[2]);
				
			} else if(row[0]=='CHILD'){
				$('#selectedOption').text('Child');
				$("#child").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#venue").val('');
				getEvents('childId',row[1] , 'CHILD ' , row[2]);
				
			} else {
				$('#selectedOption').text('GrandChild');
				$("#grandChild").val(row[1]);
				$("#artist").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('grandChildId',row[1] , 'GRANDCHILD ' , row[2]);
				
			} 
	}); 

	$("#events").change(function () {
		if(isUpdate!='true'){
			$('#ebayInventoryTabel').hide();
			$("#events option:selected").each(function () {
				$('#eventsCheckAll').removeAttr("checked");
			});
		}
		isUpdate='false'; 
	}).trigger('change');
	
	   
	
});
	
function getEvents(isArtist,id , typeString, selectedName ){
	$("#loading").show();
	var url = "";
	var brokerId = $("#brokerId").val();
	if(isArtist == 'artistId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?artistId="+id+"&brokerId="+brokerId;
	}else if(isArtist == 'venueId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?venueId="+id+"&brokerId="+brokerId;
	} else if(isArtist == 'childId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?childId="+id+"&brokerId="+brokerId;;
	} else{
		url = "GetEventsByGrandChildAndArtistAndVenue?grandChildId="+id+"&brokerId="+brokerId;
	}
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			if(jsonData.length== 0)
			 {
				alert(" There are no Events for selected " + typeString + "- "  + selectedName );			 
			 }
			
			$('#events').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.name+" "+data.date+" "+data.time+" "+data.venue+" "+data.city+" "+data.state+"</option>"
				$('#events').append(rowText);
            }
			$("#loading").hide();
		}
	}); 
} 
function validateForm(){
		var productId = '${product.id}';
		var flag= true;
		$('.selectCheck:checkbox:checked').each(function () {
			
			var id,value;
			id = this.id.replace('checkbox','exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Exposure can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','shipping_days');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Shipping days can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','shipping_method');
			value = $.trim($("#"+id).val());
			/* if(value==''){
				alert('Shipping Method can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			} */
			id = this.id.replace('checkbox','near_term_display_option');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Nearterm Display Option can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','rptFactor');
			value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid RPT Factor.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','priceBreakup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Price Breakup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerMarkup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperMarkup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			if(productId == 9) {
				id = this.id.replace('checkbox','taxPercentage');
				value = $.trim($("#"+id).val());
				if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
					alert('Please enter valid Tax Percentage.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
				id = this.id.replace('checkbox','excludeHoursBeforeEvent');
				value = $.trim($("#"+id).val());
				if(value =='' || !(/^\d+$/.test(value))){
					alert('Please enter valid Exclude hours Before Event.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
		});
		if(!flag){
			return flag;	
		}
		
		return true;
}
function submitForms(action){
	if(action=='update'){
		if(validateForm()){
			$("#action").val(action);
			$("#formId").submit();
		}
	}else{
		  var event=$("#events").val()
		  if(event!=null){
		    $("#action").val(action);
		    $("#formId").submit();
		  }else{
			  alert('Please select the events.');
		  }
	}

}

$(function () {
	
	$('#copyAllFrequentEvent').click(function(){
		if($('#copyAllFrequentEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isFrequentEvent');
		}else{
		//	$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllSectionRange').click(function(){
		if($('#copyAllSectionRange').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.presaleSelect').attr('checked', true);
		//	copyCheckBox('allowSectionRange');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.presaleSelect').attr('checked', false);
		}
	});
	$('#copyAllExposure').click(function(){
		if($('#copyAllExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('exposure');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllShippingMethod').click(function(){
		if($('#copyAllShippingMethod').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('shipping_method');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});  
	$('#copyAllNearTermDisplayOption').click(function(){
		if($('#copyAllNearTermDisplayOption').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('near_term_display_option'); 
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyShippingDays').click(function(){
		if($('#copyShippingDays').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('shipping_days'); 
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllLowerMarkup').click(function(){
		if($('#copyAllLowerMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lowerMarkup');
		}else{
			$('.selectCheck').attr('checked',tru);
		}
	});
	$('#copyAllUpperMarkup').click(function(){
		if($('#copyAllUpperMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('upperMarkup');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllLowerShippingFees').click(function(){
		if($('#copyAllLowerShippingFees').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lowerShippingFees');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllUpperShippingFees').click(function(){
		if($('#copyAllUpperShippingFees').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('upperShippingFees');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllTaxPercentage').click(function(){
		if($('#copyAllTaxPercentage').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('taxPercentage');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllExcludeHoursBeforeEvent').click(function(){
		if($('#copyAllExcludeHoursBeforeEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('excludeHoursBeforeEvent');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllRptFactor').click(function(){
		if($('#copyAllRptFactor').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('rptFactor');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllPriceBreakup').click(function(){
		if($('#copyAllPriceBreakup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('priceBreakup');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllZoneDiscount').click(function(){
		if($('#copyAllZoneDiscount').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('zoneDiscountPerc');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	
	$('#copyAllTicketNetworkEvent').click(function(){
		if($('#copyAllTicketNetworkEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('ticketnetwork_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllVividSeatsEvent').click(function(){
		if($('#copyAllVividSeatsEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('vividseat_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllScoreBigEvent').click(function(){
		if($('#copyAllScoreBigEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('scorebig_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllFanxchangeEvent').click(function(){
		if($('#copyAllFanxchangeEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('fanxchange_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllTicketcityEvent').click(function(){
		if($('#copyAllTicketcityEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('ticketcity_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllSeatGeekEvent').click(function(){
		if($('#copyAllSeatGeekEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('seatgeek_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllZoneTicketsEvent').click(function(){
		if($('#copyAllZoneTicketsEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('zoneTicket_broker');
		}else{
			$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllDiscountZone').click(function(){
		if($('#copyAllDiscountZone').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.discountSelect').attr('checked', true);
			//copyCheckBox('discount_zone');
		}else{
			$('.selectCheck').attr('checked', true);
			$('.discountSelect').attr('checked', false);
		}
	});
	
	$('#copyAll').click(function(){
		copyAll();
	});
	
});

function copyAll(){
	if($('#copyAll').attr('checked')){
		$('.selectCheck').attr('checked', true);
		copyCheckBox('discount_zone');
		copyCheckBox('allowSectionRange');
		copyTextField('exposure');
		
		copyTextField('shipping_method');
		copyTextField('near_term_display_option'); 
		copyTextField('shipping_days');    
		
		copyTextField('lowerMarkup');
		copyTextField('upperMarkup');
		copyTextField('lowerShippingFees');
		copyTextField('upperShippingFees');
		copyTextField('rptFactor');
		copyTextField('priceBreakup');
		copyTextField('ticketnetwork_broker');
		copyTextField('vividseat_broker');
		//copyCheckBox('isTicketNetwork');
		//copyCheckBox('isVivid');
		//copyCheckBox('isTickPickEvent');
		//copyCheckBox('isEbayEvent');
		
		copyTextField('scorebig_broker');
		copyTextField('fanxchange_broker');
		copyTextField('ticketcity_broker');
		copyTextField('zoneTicket_broker');
		copyTextField('taxPercentage');
		copyTextField('excludeHoursBeforeEvent');
		
	}else{
		$('.selectCheck').attr('checked', false);
	}
}
function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}
function selectRow(id) {
	$("#checkbox_" + id ).attr('checked', true);
}

function selectAllEvents(){
	if((document.getElementById("eventsCheckAll").checked)){
		$("#events").each(function(){
			$("#events option").attr("selected","selected"); 
		});

	}
	else{
			$("#events").each(function(){
			$("#events option").removeAttr("selected"); 
		});
	}
}

function popupUpdateTGCatsPricingAudit(eId){
	var url = "${auditUrl}?eventId="+eId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
} 
	
</script>