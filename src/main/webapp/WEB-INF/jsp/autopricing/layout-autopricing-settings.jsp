<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubSettingsMenu" name="selectedSubSettingsMenu" scope="request" />

<div id="adminSubMenu">
	
	
	
	<c:choose>
		<c:when  test="${selectedSubSettingsMenu == 'Default Autopricing Properties'}">
			<b>Default Autopricing Properties</b>
		</c:when>
		<c:otherwise>
			<a href="DefaultAutopricingProperties">Default Autopricing Properties</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when  test="${selectedSubSettingsMenu == 'Global Autopricing'}">
			<b>Manage Existings Global Events</b>
		</c:when>
		<c:otherwise>
			<a href="GlobalAutopricing">Manage Existings Global Events</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when  test="${selectedSubSettingsMenu == 'Exchange Markup'}">
			<b>Exchange Markup</b>
		</c:when>
		<c:otherwise>
			<a href="EditAutoPricingExchangeMarkup">Exchange Markup</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when  test="${selectedSubSettingsMenu == 'Manual AutoPricing File Generation'}">
			<b>Manual AutoPricing File Generation</b>
		</c:when>
		<c:otherwise>
			<a href="ManualAutoPricingFileGeneration">Manual AutoPricing File Generation</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when  test="${selectedSubSettingsMenu == 'Autopricing Settings'}">
			<b>Autopricing Settings</b>
		</c:when>
		<c:otherwise>
			<a href="AutoPricingSettings">Autopricing Settings</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when  test="${selectedSubSettingsMenu == 'Exclude Venues'}">
			<b>Exclude Venues</b>
		</c:when>
		<c:otherwise>
			<a href="ExcludeEventsBasedOnVenues">Exclude Venues</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubSettingsMenu == 'Scheduler Management'}">
			<b>Scheduler Management</b>
		</c:when>
		<c:otherwise>
			<a href="SchedulerManagement">Scheduler Management</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubSettingsMenu == 'Broker Connection'}">
			<b>Broker Connection Status</b>
		</c:when>
		<c:otherwise>
			<a href="ManageBrokersConnections">Broker Connection Status</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubSettingsMenu == 'Autopricing Venue Settings'}">
			<b>Autopricing Venue Settings</b>
		</c:when>
		<c:otherwise>
			<a href="AutopricingVenuesettings">Autopricing Venue Settings</a>
		</c:otherwise>
	</c:choose>
	
	
</div>
<tiles:insertAttribute name="subBody1" /> 	