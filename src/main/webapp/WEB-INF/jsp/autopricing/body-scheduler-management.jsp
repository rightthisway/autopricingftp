<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> > Scheduler Management
</div>
<display:table id="product"  requestURI="ManageScheduler" class="list" list="${products}">
	<display:column title="Name" property="name" /> 
	<display:column title ="LastRunTime">
	    <fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${product.lastRunTime}" />
	</display:column>
	<display:column title ="NextRunTime">
			<c:choose>
				<c:when test ="${product.stopped=='true'}">
					Stopped..
				</c:when>
				<c:when test ="${product.status=='Pause'}">
					<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${product.nextRunTime}" />
				</c:when>
				<c:otherwise>${product.status}..</c:otherwise>
		</c:choose>

	</display:column>
	<display:column >
		<c:choose>
			<c:when test ="${true==product.stopped}"><a href ="javaScript:toggleStatusProcess(${product.id},'false');"> <b>START</b></a></c:when>
			<c:otherwise><a href ="javaScript:toggleStatusProcess(${product.id},'true');"> <b>STOP</b> </a></c:otherwise>
		</c:choose>
		
	</display:column>
</display:table>
<script type="text/javascript">
	function toggleStatusProcess(productId,isStopped){
		window.location = "ToggleProduct?productId=" + productId +"&isStopped=" + isStopped ; 
	}
</script>