
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<div>
<br/>
	 <b>Product : ${defaultApProperties.product.name}</b>
	<br/>
	<b>Parent Category : ${defaultApProperties.parentCategory.name}</b>
	<br/>
	<br/>
	
	 
</div>
<c:if test="${not empty auditList}" >
	<div id="auditTable" >

		<display:table list="${auditList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"  id="audit"  requestURI="#" class="list">

			<display:column  title ="Action">	
				${audit.action}
			</display:column>										
			
			<display:column title="Date">
				<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.lastUpdated}" />					
			</display:column>
			<display:column title="User">${audit.lastUpdatedBy}</display:column> 
			
			<display:column title="Exposure">
				<c:choose>
					<c:when test="${audit.exposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.exposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Shipping Method">
				<c:choose>
					<c:when test="${audit.shippingMethod == null}">
						-
					</c:when>
					<c:otherwise>
						<c:if test ="${audit.shippingMethod eq '0'}" >Default Website Setting</c:if>
						<c:if test ="${audit.shippingMethod eq '1'}" >E-Ticket</c:if>
						<c:if test ="${audit.shippingMethod eq '2'}" >Will Call</c:if>
						<c:if test ="${audit.shippingMethod eq '3'}" >Local Pickup Near Venue</c:if>
						<c:if test ="${audit.shippingMethod eq '4'}" >E-Ticket or Will Call</c:if>
						<c:if test ="${audit.shippingMethod eq '5'}" >Will Call or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.shippingMethod eq '6'}" >E-Ticket or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.shippingMethod eq '7'}" >E-Ticket or Will Call or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.shippingMethod eq '8'}" >Paperless (Meet Seller at Venue)</c:if>
						<c:if test ="${audit.shippingMethod eq '9'}" >Electronic Transfer</c:if>
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Near Term Display option">
				<c:choose>
					<c:when test="${audit.nearTermDisplayOption == null}">
						-
					</c:when>
					<c:otherwise>
						<c:if test ="${audit.nearTermDisplayOption eq '0'}" >Default near-term display options</c:if>
						<c:if test ="${audit.nearTermDisplayOption eq '1'}" >Always show near-term shipping</c:if>
						<c:if test ="${audit.nearTermDisplayOption eq '2'}" >Only show near-term shipping</c:if>
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Shipping Days">
				<c:choose>
					<c:when test="${audit.shippingDays == null}">
						-
					</c:when>
					<c:otherwise>
						<c:if test ="${audit.shippingDays eq '0'}" >0 Days</c:if>
						<c:if test ="${audit.shippingDays eq '2'}" >2 Days</c:if>
						<c:if test ="${audit.shippingDays eq '4'}" >4 Days</c:if>
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="RPT Factor">
				<c:choose>
					<c:when test="${audit.rptFactor == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.rptFactor}
					</c:otherwise>
				</c:choose>
				
			</display:column>
			
			<display:column title="Price Breakup">
				<c:choose>
					<c:when test="${audit.priceBreakup == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.priceBreakup}
					</c:otherwise>
				</c:choose>
				
			</display:column>
			
		 <display:column title="Lower Markup" sortable="true" headerClass="sortable"> 
				<c:choose>
						<c:when test="${audit.lowerMarkup == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.lowerMarkup}
						</c:otherwise>
					</c:choose>
			</display:column>	
			<display:column title="Upper Markup">
				<c:choose>
					<c:when test="${audit.upperMarkup == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.upperMarkup}
					</c:otherwise>
				</c:choose>
				
			</display:column>
			<display:column title="Lower Shipping Fees">
				<c:choose>
					<c:when test="${audit.lowerShippingFees == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.lowerShippingFees}
					</c:otherwise>
				</c:choose>
			</display:column>	
			<display:column title="Upper Shipping Fees">
				<c:choose>
					<c:when test="${audit.upperShippingFees == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.upperShippingFees}
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<c:if test="${defaultApProperties.product.id eq 9}">
				<display:column title="Tax Percentage">
					<c:choose>
						<c:when test="${audit.taxPercentage == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.taxPercentage}
						</c:otherwise>
					</c:choose>
				</display:column>
				<display:column title="Exclude Hours Before Event">
					<c:choose>
						<c:when test="${audit.excludeHoursBeforeEvent == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.excludeHoursBeforeEvent}
						</c:otherwise>
					</c:choose>
				</display:column>
			</c:if>
			
			<display:column title="Section Count Ticket">
				<c:choose>
					<c:when test="${audit.sectionCountTicket == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.sectionCountTicket}
					</c:otherwise>
				</c:choose>
			</display:column>
	
	<c:if test="${defaultApProperties.product.id ne null and defaultApProperties.product.id ne 10 and defaultApProperties.product.id ne 11 
					and defaultApProperties.product.id ne 6 and defaultApProperties.product.id ne 7}">		
		<display:column title='TicketNetwork Broker' >
			<c:choose>
				<c:when test="${audit.ticketNetworkBroker!=null}">
					${audit.ticketNetworkBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
	<display:column title='VividSeats Broker' >
		<c:choose>
			<c:when test="${audit.vividBroker!=null}">
				${audit.vividBroker.name}
			</c:when>
			<c:otherwise>
				-
			</c:otherwise>
		</c:choose>
	</display:column>
		<display:column title='ScoreBig Broker'>
			<c:choose>
				<c:when test="${audit.scoreBigBroker!=null}">
					${audit.scoreBigBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title='FanXchange Broker'>
			<c:choose>
				<c:when test="${audit.fanxchangeBroker!=null}">
					${audit.fanxchangeBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
		
			<display:column title='TicketCity Broker'>
			<c:choose>
				<c:when test="${audit.ticketcityBroker!=null}">
					${audit.ticketcityBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
		
		<display:column title='SeatGeek Broker'>
			<c:choose>
				<c:when test="${audit.seatGeekBroker!=null}">
					${audit.seatGeekBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
		
		<display:column title='ZoneTicket Broker'>
			<c:choose>
				<c:when test="${audit.zoneTicketBroker!=null}">
					${audit.zoneTicketBroker.name}
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose>
		</display:column>
	</c:if>
	
	<c:if test="${defaultApProperties.product.id ne null and (defaultApProperties.product.id eq 10 or defaultApProperties.product.id eq 11
					or defaultApProperties.product.id eq 6 or defaultApProperties.product.id eq 7)}">
	
	 <display:column title='RTW Enabled'> 
			<c:choose>
				<c:when test="${audit.rtwEnabled !=null && audit.rtwEnabled}">
					Yes
				</c:when>
				<c:otherwise>
					No
				</c:otherwise>
			</c:choose>
		</display:column>
		
		<%-- <display:column title='ROT Enabled'> 
			<c:choose>
				<c:when test="${audit.rotEnabled !=null && audit.rotEnabled}">
					Yes
				</c:when>
				<c:otherwise>
					No
				</c:otherwise>
			</c:choose>
		</display:column> --%>
		
		<display:column title='TixCity Enabled'> 
			<c:choose>
				<c:when test="${audit.tixcityEnabled !=null && audit.tixcityEnabled}">
					Yes
				</c:when>
				<c:otherwise>
					No
				</c:otherwise>
			</c:choose>
		</display:column>
		
		<%-- <display:column title='VividSeat'> 
			<c:choose>
				<c:when test="${audit.vividSeatEnabled !=null && audit.vividSeatEnabled}">
					Yes
				</c:when>
				<c:otherwise>
					No
				</c:otherwise>
			</c:choose>
		</display:column> --%>
		
		<c:if test="${defaultApProperties.product.id ne null and defaultApProperties.product.id eq 6}">
			
			<display:column title='Zoned LastRow Minicats Enabled'> 
				<c:choose>
					<c:when test="${audit.zonedLastrowMinicatsEnabled !=null && audit.zonedLastrowMinicatsEnabled}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title='LarryLast Enabled'> 
				<c:choose>
					<c:when test="${audit.larryLastEnabled !=null && audit.larryLastEnabled}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
		</c:if>
		
		<c:if test="${defaultApProperties.product.id ne null and defaultApProperties.product.id eq 10}">
			
			<display:column title='Minicats Enabled'> 
				<c:choose>
					<c:when test="${audit.minicatsEnabled !=null && audit.minicatsEnabled}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title='Lastrow Minicats Enabled'> 
				<c:choose>
					<c:when test="${audit.lastrowMinicatsEnabled !=null && audit.lastrowMinicatsEnabled}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			
			<display:column title='VipMinicats Enabled'> 
				<c:choose>
					<c:when test="${audit.vipMinicatsEnabled !=null && audit.vipMinicatsEnabled}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title='VipLastRow Minicats Enabled'> 
				<c:choose>
					<c:when test="${audit.vipLastRowMinicatsEnabled !=null && audit.vipLastRowMinicatsEnabled}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
		</c:if>
		<c:if test="${defaultApProperties.product.id ne null and defaultApProperties.product.id eq 11}">
			
			<display:column title='Minicats Enabled'> 
				<c:choose>
					<c:when test="${audit.minicatsEnabled !=null && audit.minicatsEnabled}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title='Presale Autocats Enabled'> 
				<c:choose>
					<c:when test="${audit.presaleAutocatsEnabled !=null && audit.presaleAutocatsEnabled}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
		</c:if>
	
	</c:if>
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty auditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit for this selection.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>