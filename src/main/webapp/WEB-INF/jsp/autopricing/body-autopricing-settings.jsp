<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

	

$(function () {
	
	 $("#isAllBroker").click(function() {
		if((document.getElementById("isAllBroker").checked)){
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected","selected");				
			});	
		} else {
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllProduct").click(function() {
		if((document.getElementById("isAllProduct").checked)){
			$("#productId").each(function(){
				$("#productId option").attr("selected","selected");				
			});	
		} else {
			$("#productId").each(function(){
				$("#productId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllExchange").click(function() {
		if((document.getElementById("isAllExchange").checked)){
			$("#exchangeId").each(function(){
				$("#exchangeId option").attr("selected","selected");				
			});	
		} else {
			$("#exchangeId").each(function(){
				$("#exchangeId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	$('#copyAllisEnabled').click(function(){
		if($('#copyAllisEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isEnabled');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllExcludeEventDays').click(function(){
		if($('#copyAllExcludeEventDays').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('excludeEventDays');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$( "#brokerId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#productId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#exchangeId" ).change(function() {
		$('#gridTable').hide();
		});
	
	
	/* $('#copyAll').click(function(){
		copyAll();
	}); */
});

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}

	function submitForms(action) {
		if(action=='update'){
			if(validateForm()){
				$("#action").val(action);
				$("#formId").submit();
			}
		}else {
			$("#action").val(action);
			$("#formId").submit();
		}
	}
	
	function validateForm(){
		var flag= true;
		var isMinimamOneRecord = false;
		
		$('.selectCheck:checkbox:checked').each(function () {
			
			isMinimamOneRecord = true;
			
			var id = this.id.replace('checkbox','excludeEventDays');
			var value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Exclude Event Days.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
		});
		
		if(!isMinimamOneRecord) {
			alert('Please Select Minimum one record for Update.');
			flag = false;
			//return ;
		}
		
		if(!flag){
			return flag;	
		}
		return true;
}
	function selectRow(id) {
		$("#checkbox_" + id ).attr('checked', true);
	}
	
	function popupBrokerProductManageAudit(bId,pId,eId){
		var url = "AutoPricingSettingsAudit?bId="+bId+"&pId="+pId+"&eId="+eId;
		newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
		if (window.focus) {newwindow.focus()}
		return false;
		
	} 
	
</script>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> > Autopricing Settings
</div>
<h1>Autopricing Settings</h1>
<br/>
<hr/>
<form:form action="AutoPricingSettings" method="post" id="formId">
<input type="hidden" name="action" id="action"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	
<table align="center">
<tr>
<td align="center"><b>Broker :</b> Check  All: <input type="checkbox" name="isAllBroker" <c:if test="${isAllBrokerSelected == 'on'}">checked</c:if> id="isAllBroker"/>
</td>
<td align="center"><b>Product :</b> Check  All: <input type="checkbox" name="isAllProduct" <c:if test="${isAllProductSelected == 'on'}">checked</c:if> id="isAllProduct" />
</td>
<td align="center"><b>Exchange :</b> Check  All: <input type="checkbox" name="isAllExchange" <c:if test="${isAllExchangeSelected == 'on'}">checked</c:if> id="isAllExchange"/>
</td>
</tr>
<tr><td>
<select name="brokerId" id="brokerId" style="width: 300px" multiple size="5" >
 <c:forEach var="broker" items="${brokerList}">
 <c:set var="a1" value="false" />
 <c:forEach var="selected" items="${selectedBrokerId}">
  <c:if test="${broker.id == selected}">
  <c:set var="a1" value="true" />
  </c:if>
  </c:forEach>
    <option value="${broker.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${broker.name}</option>
  </c:forEach>
</select> 
</td>
<td> 
<select name="productId" id="productId" style="width: 300px" multiple size="5">
<c:forEach var="product" items="${productList}">
<c:set var="a2" value="false" />  
  <c:forEach var="selected1" items="${selectedProductId}">
  <c:if test="${product.id == selected1}">
  <c:set var="a2" value="true" />
  </c:if>
  </c:forEach>
    <option value="${product.id}" <c:if test="${a2 == 'true'}">selected</c:if>>${product.name}</option>
  </c:forEach>

</select>  
</td>
<td> 
<select name="exchangeId" id="exchangeId" style="width: 300px" multiple size="5">
<c:forEach var="exchange" items="${exchangeList}">
<c:set var="a3" value="false" />  
  <c:forEach var="selected2" items="${selectedExchangeId}">
  <c:if test="${exchange.id == selected2}">
  <c:set var="a3" value="true" />
  </c:if>
  </c:forEach>
    <option value="${exchange.id}" <c:if test="${a3 == 'true'}">selected</c:if>>${exchange.name}</option>
  </c:forEach>
  
</select>  
</td>
</tr>
<tr>
	<td colspan="3" align="center">
	<br />
		<input type="button" value="Get Data" onclick="submitForms('search');" class="medButton">
	</td>
</tr>
</table>
	<br />
	<c:if test="${not empty autopriicngSettingsList}" >
	<div id="gridTable" align="center" >

		<display:table list="${autopriicngSettingsList}" style="width:60%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="apSettings"  requestURI="#" class="list">

		<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
			<input type="checkbox" name="checkbox_${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}" id="checkbox_${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}" 
			<c:if test ="${apSettings.id !=null  }"> checked="checked" </c:if> class="selectCheck" />
		</display:column>
		<display:column title="Broker" sortable="true">${apSettings.broker.name}</display:column>
		<display:column title="Product" sortable="true">${apSettings.product.name}</display:column>
		<display:column title="Exchange" sortable="true">${apSettings.exchange.name}</display:column>
		
		<display:column title='Exclude Events in Days<br/><input type="checkbox" name="copyAllExcludeEventDays" id="copyAllExcludeEventDays" >' >
				<input type="text" name="excludeEventDays_${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}" id="excludeEventDays_${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}" value="${apSettings.excludeEventDays}" size="2" class="markupText"
				 onclick="selectRow('${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}');" />
		</display:column>
			
		<display:column title='Enable<br/><input type="checkbox" name="copyAllisEnabled" id="copyAllisEnabled" >' >
			<input type="checkbox" name="isEnabled_${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}" id="isEnabled_${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}" <c:if test ="${apSettings.isEnabled !=null && apSettings.isEnabled}"> checked="checked" </c:if> 
			 onclick="selectRow('${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}');" />
		</display:column>
		
		<%-- <display:column title="Action" >
			<input type="button" value="Audit" class="medButton" id="audit_${apSettings.broker.id}_${apSettings.product.id}_${apSettings.exchange.id}" onClick="popupBrokerProductManageAudit('${apSettings.broker.id}','${apSettings.product.id}','${apSettings.exchange.id}');"/>
		</display:column> --%>
		</display:table>
	<br />	
	<div style="float:right; padding-right:320px;">
		<input type="button" value="Update"    class="medButton" id="update1" name="update1" onclick="javascript:submitForms('update');"/> 
	</div>
	</div>	
	<br />
	
	
	</c:if>
	<c:if test="${empty autopriicngSettingsList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>


</form:form>