<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

	function submitForms(action){
		$("#action").val(action);
		$("#formId").submit();
	}


</script>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> > Manual AutoPricing File Generation
</div>
<h1>Manual AutoPricing File Generation</h1>
<br/>
<hr/>
<form:form action="ManualAutoPricingFileGeneration" method="post" id="formId">
<input type="hidden" name="action" id="action"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	<br />
	
	<div id="gridTable" align="center" >
	<span>
		<!-- <a href="ManualAutoPricingFileGeneration?action=update">Manual AutoPricing File Generation</a> -->
		<input type="button" value="Generate"  name="Generate" id="Generate" class="medButton" onclick="javascript:submitForms('update');"/>
	</span>		
	</div>	
</form:form>