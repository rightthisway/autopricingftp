<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Additional Exchange Markup
</div>


<div style="clear:both"></div>

<h1>Additional Exchange Markup</h1>
<script type="text/javascript">


$(function () {
	
	$('#copyAllAdditionalMarkup').click(function(){
		if($('#copyAllAdditionalMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('additionalMarkup');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
});

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}
function submitForms(action) {
	if(action=='update'){
		if(validateForm()){
			$("#action").val(action);
			$("#formId").submit();
		}
	}else {
		$("#action").val(action);
		$("#formId").submit();
	}
}

function validateForm(){
	var flag= true;
	var isMinimamOneRecord = false;
	
	$('.selectCheck:checkbox:checked').each(function () {
		
		isMinimamOneRecord = true;
		
		var id = this.id.replace('checkbox','additionalMarkup');
		var value = $.trim($("#"+id).val());
		if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
			alert('Please enter valid Additional Markup.');
			$("#"+id).focus();
			flag = false;
			return ;
		}
	});
	
	if(!isMinimamOneRecord) {
		alert('Please Select Minimum one Exchange for Update.');
		flag = false;
		//return ;
	}
	
	if(!flag){
		return flag;	
	}
	return true;
}
function selectRow(id) {
	$("#checkbox_" + id ).attr('checked', true);
}
function popupAutopricingExchangeMarkup(exchangeId){
	var url = "AutoPricingExchangeMarkupAudit?exchangeId="+exchangeId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}

	 </script>
<form:form modelAttribute="exosure" action="EditAutoPricingExchangeMarkup" method="post" id="formId">
<input type="hidden" name="action" id="action"/>
<div id="ebayInventoryTabel" >
	    <table>
			<tr align="center">
				<td colspan="3" class="successMessage" id="success" ><font face="Tahoma" size="4" color="blue">${sportinfo}</font></td>
        	</tr>
		</table>	
		<br/>
</div>
	<div id="gridTable" align="left" >

		<display:table list="${autopricingExchangeList}" style="width:50%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="apExchange"  requestURI="#" class="list">

		<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
			<input type="checkbox" name="checkbox_${apExchange.id}" id="checkbox_${apExchange.id}" 
			<c:if test ="${apExchange.id !=null  }"> checked="checked" </c:if> class="selectCheck" />
		</display:column>
		<display:column title="Broker" sortable="true">${apExchange.name}</display:column>
		
		<display:column title='Additional Markup<br/><input type="checkbox" name="copyAllAdditionalMarkup" id="copyAllAdditionalMarkup" >' >
				<input type="text" name="additionalMarkup_${apExchange.id}" id="additionalMarkup_${apExchange.id}" value="${apExchange.additionalMarkup}" size="6" class="markupText"
				 onclick="selectRow('${apExchange.id}');" />
		</display:column>
			
		<display:column title="Action" >
			<input type="button" value="Audit" class="medButton" id="audit_${apExchange.id}" onClick="popupAutopricingExchangeMarkup('${apExchange.id}');"/>
		</display:column>
		</display:table>
	<br />	
	<div style="float:right; padding-right:620px;">
		<input type="button" value="Update"    class="medButton" id="update1" name="update1" onclick="javascript:submitForms('update');"/> 
	</div>
	</div>
	<br/>
	<br/>
</form:form>