<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%@ page isELIgnored="false" %>
<script type="text/javascript">

	

$(function () {
	
	$( "#brokerId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#productId" ).change(function() {
		$('#gridTable').hide();
		});
	$( "#venueId" ).change(function() {
		$('#gridTable').hide();
		getVenueCategories();
	});
	
	$( "#venueCategoryId" ).change(function() {
		$('#gridTable').hide();
	});
	
	 $("#isAllApplicableProduct").click(function() {
		if((document.getElementById("isAllApplicableProduct").checked)){
			$("#applicableProductId").each(function(){
				$("#applicableProductId option").attr("selected","selected");				
			});	
		} else {
			$("#applicableProductId").each(function(){
				$("#applicableProductId option").attr("selected",false);				
			});	
		}
	 }) ;
	 $("#isAllApplicableBroker").click(function() {
			if((document.getElementById("isAllApplicableBroker").checked)){
				$("#applicableBrokerId").each(function(){
					$("#applicableBrokerId option").attr("selected","selected");				
				});	
			} else {
				$("#applicableBrokerId").each(function(){
					$("#applicableBrokerId option").attr("selected",false);				
				});	
			}
		 }) ;
	 
	 $('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
});

function getVenueCategories(autovenueId){
	
	//var venueId = $('#venueId').val();
	var venueId = autovenueId;
	var url = "GetVenueCategoriesByVenue?venueId="+venueId;

	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#VenueCategorys').children().remove();
			$('#venueCategoryId').empty();
			
			var rowText = "<option value="+">---Select---</option>"
			$('#venueCategoryId').append(rowText);
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				rowText = "<option value="+data.id+ ">"+ data.categoryGroup+"</option>"
				$('#venueCategoryId').append(rowText);
            }	
		}
	}); 
} 

	function submitForms(action) {
		if(validateForm(action)){
			$("#action").val(action);
			$("#formId").submit();
		}
	}
	
	function validateForm(action){
		
		var flag= true;
		var value = $('#productId').val();
		if(value == null || value== ''){
			alert('Please select product.');
			$("#venueCategoryId").focus();
			flag = false;
			return false;
		}
		value = $('#brokerId').val();
		if(value == null || value== ''){
			alert('Please select broker.');
			$("#brokerId").focus();
			flag = false;
			return false; 
		}
		value = $('#venueId').val();
		if(value == null || value== ''){
			alert('Please select venue.');
			//$("#venueId").focus();
			flag = false;
			return false;
		}
		value = $('#venueCategoryId').val();
		if(value == null || value== ''){
			alert('Please select venueCategory.');
			$("#venueCategoryId").focus();
			flag = false;
			return false;
		}
		
		return true;
	}

	function popupExcludeVenueCategoryZonesAudit(pId,bId,vcId,zone){
		var url = "ExcludeVenueCategoryZonesAudit?pId="+pId+"&bId="+bId+"&vcId="+vcId+"&zone="+zone;
		newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
		if (window.focus) {newwindow.focus()}
		return false;
		
	} 
	
	
	function callChangeVenue(){
	$('#venueLabelSpanId').hide();
	$('#venueSelectSpanId').show();
	$('#venueCategoryId').empty().append('--Select--');
	$('#txtvenueId').val('');
	$('#venueId').val('');
	$('#gridTable').hide();
	
	}
	
$(document).ready(function(){	
	 var prevSelectedVenueId = document.getElementById("hidselectedVenueName").value;	
	 if(null == prevSelectedVenueId || prevSelectedVenueId == ""){
	 	callChangeVenue();
	 }
	 else{
		 $('#venueLabelSpanId').show();
		 $('#venueSelectSpanId').hide();
		 
	 }

	
	
	
	 $('#txtvenueId').autocomplete("AutoCompleteVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",		
		formatItem: function(row, i, max) {
			if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" +  row[2]  ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){		  
			$('#venueId').val('');
			$('#selectedValue').text(row[2]);			
			if(row[0]=='VENUE'){
				$('#selectedOption').text('Venue');				
				$("#txtvenueId").val(row[2]);
				$("#hidselectedVenueName").val(row[2]);
				$("#labelVenueName").text(row[2]);
				$("#venueId").val(row[1]);	
				$('#venueSelectSpanId').hide();
				$('#venueLabelSpanId').show();
				getVenueCategories(row[1]);					
			} 
	}); 


});

	
	
	
	
	
</script>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> > Exclude VenueCategory Zones
</div>
<h1>Exclude VenueCategory Zones</h1>
<br/>
<hr/>
<form:form action="ExcludeVenueCategoryZones" method="post" id="formId">
<input type="hidden" name="action" id="action"/>
<input type="hidden" name="uncheckedboxes" id="uncheckedboxes"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	
<table align="center">
	<tr>
		<td><b>Product:</b></td>
		<td>
			<select id="productId" name="productId">
				<option value="">---Select---</option>
				<c:forEach items="${productList}" var="productObj">
				<option value="${productObj.id }"
				<c:if test="${selectedProductId ne null and productObj.id eq selectedProductId}"> Selected </c:if>
				>${productObj.name}</option>
			</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td><b>Broker:</b></td>
		<td>
			<select id="brokerId" name="brokerId">
				<option value="">---Select---</option>
				<c:forEach items="${brokerList}" var="brokerObj">
				<option value="${brokerObj.id }"
				<c:if test="${selectedBrokerId ne null and brokerObj.id eq selectedBrokerId}"> Selected </c:if>
				>${brokerObj.name}</option>
			</c:forEach>
			</select>
		</td>
	</tr>
	
		
		
      	<tr>		
		<td><b>Venue</b></td>
		<td>	
		<input type="hidden" value = "${selectedVenueName}" name="hidselectedVenueName" id= "hidselectedVenueName">
		<input type="hidden" value ="${selectedVenueId}" name="venueId" id= "venueId">
		
		<span id="venueSelectSpanId">		
			<input type="text" id="txtvenueId" name="txtvenueId">
		</span>
		
		<span id="venueLabelSpanId">
		<span id="labelVenueName">${selectedVenueName}</span>			
		<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
		</span>		
		
		
		</td>
		</tr> 
	<tr>
		<td><b>VenueCategory:</b></td>
		<td>
			<select id="venueCategoryId" name="venueCategoryId">
				<option value="">---Select---</option>
				<c:forEach items="${venueCategoryList}" var="venueCategoryObj">
				<option value="${venueCategoryObj.id }"
				<c:if test="${selectedVenueCategoryId ne null and venueCategoryObj.id eq selectedVenueCategoryId}"> Selected </c:if>
				>${venueCategoryObj.categoryGroup}</option>
			</c:forEach>
			</select>
		</td>
	</tr>
<tr>
	<td colspan="4" align="center">
	<br />
		<input type="button" value="Get Data" onclick="submitForms('search');" class="medButton">
	</td>
</tr>
</table>
	<br />
	<c:if test="${not empty excludeVenueCategoryZones}" >
	
	<div id="gridTable" align="center" >
		<table align="center">
			<tr>
				<td><b>Apply To:</b></td>
				<td>
					Check  All: <input type="checkbox" name="isAllApplicableProduct" <c:if test="${isAllApplicableProductSelected == 'on'}">checked</c:if> id="isAllApplicableProduct" /><br />
				
					<select name="applicableProductId" id="applicableProductId" style="width: 300px" multiple size="5">
						<c:forEach var="product" items="${productList}">
						<c:if test="${selectedProductId eq null or product.id ne selectedProductId }" >
							<c:set var="a1" value="false" />  
								<c:forEach var="selected1" items="${selectedApplicableProductId}">
									<c:if test="${product.id == selected1}">
										<c:set var="a1" value="true" />
									</c:if>
								</c:forEach>
								<option value="${product.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${product.name}</option>
							</c:if>	
						</c:forEach>
					</select>
				</td>
				<td>
					Check  All: <input type="checkbox" name="isAllApplicableBroker" <c:if test="${isAllApplicableBrokerSelected == 'on'}">checked</c:if> id="isAllApplicableBroker" /><br />
				
					<select name="applicableBrokerId" id="applicableBrokerId" style="width: 300px" multiple size="5">
						<c:forEach var="broker" items="${brokerList}">
							<c:if test="${selectedBrokerId eq null or broker.id ne selectedBrokerId }" >
								<c:set var="a1" value="false" />  
								<c:forEach var="selected1" items="${selectedApplicableBrokerId}">
									<c:if test="${broker.id == selected1}">
										<c:set var="a1" value="true" />
									</c:if>
								</c:forEach>
								<option value="${broker.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${broker.name}</option>
							</c:if>	
						</c:forEach>
					</select>
				</td>
			</tr>
		</table>
		<div style="float:right; padding-right:320px;">
			<br />
			<input type="button" value="Update"    class="medButton" id="update1" name="update1" onclick="javascript:submitForms('update');"/>
			<br /> 
		</div>

		<display:table list="${excludeVenueCategoryZones}" style="width:60%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="excludeVenueCategoryZone"  requestURI="#" class="list">

		<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
			<input type="checkbox" name="checkbox_${excludeVenueCategoryZone.zone}" id="checkbox_${excludeVenueCategoryZone.zone}" 
			<c:if test ="${excludeVenueCategoryZone.id !=null  }"> checked="checked" </c:if> class="selectCheck" />
		</display:column>
		<display:column title="Venue" sortable="true" group="1">${excludeVenueCategoryZone.venueCategory.venue.building} ${excludeVenueCategoryZone.venueCategory.venue.city} ${excludeVenueCategoryZone.venueCategory.venue.state} ${excludeVenueCategoryZone.venueCategory.venue.country}</display:column>
		<display:column title="Venue Category" sortable="true" group="1">${excludeVenueCategoryZone.venueCategory.categoryGroup}</display:column>
		<display:column title="Zones" sortable="true">${excludeVenueCategoryZone.zone}</display:column>
		
						
        <display:column title="Action" >
			<input type="button" value="Audit" class="medButton" id="audit_${excludeVenueCategoryZone.zone}" onClick="popupExcludeVenueCategoryZonesAudit('${excludeVenueCategoryZone.product.id}','${excludeVenueCategoryZone.broker.id}','${excludeVenueCategoryZone.venueCategory.id}','${excludeVenueCategoryZone.zone}');"/>
		</display:column> 
		</display:table>
	<br />	
	<div style="float:right; padding-right:320px;">
		<input type="button" value="Update"    class="medButton" id="update2" name="update2" onclick="javascript:submitForms('update');"/> 
	</div>
	</div>	
	<br />
	
	
	</c:if>
	<c:if test="${empty excludeVenueCategoryZones}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>

</form:form>