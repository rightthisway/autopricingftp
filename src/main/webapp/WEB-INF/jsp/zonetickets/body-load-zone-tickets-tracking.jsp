
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

$(function () {
	$(function() {
	    $( "#fromDate" ).datepicker({});
	  });
	
	$(function() {
	    $( "#toDate" ).datepicker({});
	  });
	
	 $("#isAllBroker").click(function() {
		if((document.getElementById("isAllBroker").checked)){
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected","selected");				
			});	
		} else {
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllProduct").click(function() {
		if((document.getElementById("isAllProduct").checked)){
			$("#productId").each(function(){
				$("#productId option").attr("selected","selected");				
			});	
		} else {
			$("#productId").each(function(){
				$("#productId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllExchange").click(function() {
		if((document.getElementById("isAllExchange").checked)){
			$("#exchangeId").each(function(){
				$("#exchangeId option").attr("selected","selected");				
			});	
		} else {
			$("#exchangeId").each(function(){
				$("#exchangeId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	$('#copyAllisEnabled').click(function(){
		if($('#copyAllisEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isEnabled');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllExcludeEventDays').click(function(){
		if($('#copyAllExcludeEventDays').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('excludeEventDays');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$( "#brokerId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#productId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#exchangeId" ).change(function() {
		$('#gridTable').hide();
		});
	
	
	/* $('#copyAll').click(function(){
		copyAll();
	}); */
});

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}

function submitForms(action) {
	var actionType =  $("#actionType").val();
	var flag= true;
	if(actionType == 'SELECT'){
		alert('Please Select Action Type.');
		flag = false;
	}
	if(flag){
		$("#action").val(action);
		$("#formId").submit();
	}
}
	
	function validateForm(){
		var flag= true;
		var isMinimamOneRecord = false;
		
		$('.selectCheck:checkbox:checked').each(function () {
			
			isMinimamOneRecord = true;
			
			var id = this.id.replace('checkbox','excludeEventDays');
			var value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Exclude Event Days.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
		});
		
		if(!isMinimamOneRecord) {
			alert('Please Select Minimum one record for Update.');
			flag = false;
			//return ;
		}
		
		if(!flag){
			return flag;	
		}
		return true;
}
	function selectRow(id) {
		$("#checkbox_" + id ).attr('checked', true);
	}
	

	
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > RewardTheFan Tracking
</div>
<h1>RewardTheFan Tracking</h1>
<br/>
<hr/>
<form:form action="ZoneTicketsTracking" method="post" id="formId">
<input type="hidden" name="action" id="action"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	
<table align="center" >

<tr>
	<td ><b>From Date :</b></td>
	
	<td>
	<input type="text" name="fromDate" id="fromDate"  value="${fromDate}"/>  
	
	<b>Hour :</b>
		<select id ="fromHour" name = "fromHour">
			<option value=''>--- Select Hour -- </option>
			<c:forEach var="i" begin="0" end="23">
				<option value='${i}' <c:if test="${fromHour == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
		
	<b>Minute :</b>
		<select id ="fromMinute" name = "fromMinute">
			<option value=''>--- Select Minute -- </option>
			<c:forEach var="i" begin="00" end="50">
				<option value='${i}' <c:if test="${fromMinute == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>	
	
	</td> 
	
</tr>
<tr>
	<td ><b> To Date  :</b></td>
	
	<td><input type="text" name="toDate" id="toDate" value="${toDate}"/>
	
	<b>Hour  :</b>
		<select id ="toHour" name = "toHour">
			<option value=''>--- Select Hour -- </option>
			<c:forEach var="i" begin="00" end="23">
				<option value='${i}' <c:if test="${toHour == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
	
	<b>Minute  :</b>
		<select id ="toMinute" name = "toMinute">
			<option value=''>--- Select Minute -- </option>
			<c:forEach var="i" begin="00" end="59">
				<option value='${i}' <c:if test="${toMinute == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
	</td>

</tr>

<tr>
	<td ><b> Action Type  :</b></td>
	
	<td>
		<select id ="actionType" name ="actionType">
			<option value='SELECT' <c:if test="${selectedActionType eq 'SELECT'}">selected</c:if> >--- SELECT -- </option>
			<option value='ALL' <c:if test="${selectedActionType eq 'ALL' }">selected</c:if> >ALL</option>
			<c:forEach items="${actionList}" var="action">
				<option value='${action}' 
				<c:if test="${ action == selectedActionType}">
				selected</c:if>>${action}</option>
			</c:forEach>
		</select>
	</td>
</tr>

<tr>
	<td ><b> Web Config Id  :</b></td>
	
	<td>
		<select id ="webConfigId" name ="webConfigId">
			<option value='ALL' <c:if test="${selectedWebConfigId eq 'ALL' }">selected</c:if> >ALL</option>
			<c:forEach items="${webConfigList}" var="webConfig">
				<option value='${webConfig.configId}' 
				<c:if test="${webConfig.configId == selectedWebConfigId}">
				selected</c:if>>${webConfig.configId}</option>
			</c:forEach>
		</select>
	</td>
</tr>

<tr>
	<td align="center" colspan="2">
	<br />
		<input type="button" value="Get Data" onclick="submitForms('search');" class="medButton">
	</td>
</tr>


</table>
	<br />
	<c:if test="${not empty zoneTicketsTrackings}" >
	<div id="gridTable" align="center" >

		<display:table list="${zoneTicketsTrackings}" style="width:100%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   
		id="audit"  requestURI="#" class="list">
			<display:column title="Hit Date" sortable="true"><fmt:formatDate type="both" value="${audit.hittingDate}" /></display:column>
			<display:column title="WebConfigId" >${audit.webConfigId}</display:column>		
			<display:column title="IP Address" sortable="true">${audit.customerIpAddress}</display:column>
			<display:column title="ActionType" sortable="true">${audit.actionType}</display:column>
			<display:column title="MethodDescription" >${audit.methodDescription}</display:column>
			<display:column title="AuthenticatedHit" >${audit.authenticatedHit}</display:column>
			<display:column title="ActionResult" >${audit.actionResult}</display:column>
     	</display:table>
	<br />
	</div>	
	<br />
	
	
	</c:if>
	<c:if test="${empty zoneTicketsTrackings}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>


</form:form>