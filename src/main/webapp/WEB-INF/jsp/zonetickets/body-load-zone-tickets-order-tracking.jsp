
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

$(function () {
	$(function() {
	    $( "#fromDate" ).datepicker({});
	  });
	
	$(function() {
	    $( "#toDate" ).datepicker({});
	  });
	
	 $("#isAllBroker").click(function() {
		if((document.getElementById("isAllBroker").checked)){
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected","selected");				
			});	
		} else {
			$("#brokerId").each(function(){
				$("#brokerId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllProduct").click(function() {
		if((document.getElementById("isAllProduct").checked)){
			$("#productId").each(function(){
				$("#productId option").attr("selected","selected");				
			});	
		} else {
			$("#productId").each(function(){
				$("#productId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	 $("#isAllExchange").click(function() {
		if((document.getElementById("isAllExchange").checked)){
			$("#exchangeId").each(function(){
				$("#exchangeId option").attr("selected","selected");				
			});	
		} else {
			$("#exchangeId").each(function(){
				$("#exchangeId option").attr("selected",false);				
			});	
		}
		$('#gridTable').hide();
	 }) ;
	 
	$('#copyAllisEnabled').click(function(){
		if($('#copyAllisEnabled').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isEnabled');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllExcludeEventDays').click(function(){
		if($('#copyAllExcludeEventDays').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('excludeEventDays');
		}else{
			//$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$( "#brokerId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#productId" ).change(function() {
		$('#gridTable').hide();
		});
	
	$( "#exchangeId" ).change(function() {
		$('#gridTable').hide();
		});
	
	
	/* $('#copyAll').click(function(){
		copyAll();
	}); */
});

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}

	function submitForms(action) {
		var actionType =  $("#actionType").val();
		var flag= true;
		if(actionType == 'SELECT'){
			alert('Please Select Action Type.');
			flag = false;
		}
		if(flag){
			$("#action").val(action);
			$("#formId").submit();
		}
	}
	
	function validateForm(){
		var flag= true;
		var isMinimamOneRecord = false;
		
		
		
		if(!isMinimamOneRecord) {
			alert('Please Select Minimum one record for Update.');
			flag = false;
			//return ;
		}
		
		if(!flag){
			return flag;	
		}
		return true;
}
	function selectRow(id) {
		$("#checkbox_" + id ).attr('checked', true);
	}
	
	function showGpsSearchOption(value){
		
		if(value == 'GENERALISEDSEARCH'){
			$("#gpsSearchRowId").show();
		}else{
			$("#gpsSearchRowId").hide();
		}
		
	}
	

	
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > RewardTheFan Order Tracking
</div>
<h1>RewardTheFan Order Tracking</h1>
<br/>
<hr/>
<form:form action="ZoneTicketsOrderTracking" method="post" id="formId">
<input type="hidden" name="action" id="action"/>

	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br />
	<br />
	
<table align="center" >

<tr>
	<td ><b>From Date :</b></td>
	
	<td>
	<input type="text" name="fromDate" id="fromDate"  value="${fromDate}"/>  
	
	<b>Hour :</b>
		<select id ="fromHour" name = "fromHour">
			<option value=''>--- Select Hour -- </option>
			<c:forEach var="i" begin="0" end="23">
				<option value='${i}' <c:if test="${fromHour == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
		
	<b>Minute :</b>
		<select id ="fromMinute" name = "fromMinute">
			<option value=''>--- Select Minute -- </option>
			<c:forEach var="i" begin="00" end="50">
				<option value='${i}' <c:if test="${fromMinute == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>	
	
	</td> 
	
</tr>
<tr>
	<td ><b> To Date  :</b></td>
	
	<td><input type="text" name="toDate" id="toDate" value="${toDate}"/>
	
	<b>Hour  :</b>
		<select id ="toHour" name = "toHour">
			<option value=''>--- Select Hour -- </option>
			<c:forEach var="i" begin="00" end="23">
				<option value='${i}' <c:if test="${toHour == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
	
	<b>Minute  :</b>
		<select id ="toMinute" name = "toMinute">
			<option value=''>--- Select Minute -- </option>
			<c:forEach var="i" begin="00" end="59">
				<option value='${i}' <c:if test="${toMinute == i}">selected</c:if>>${i}</option>
			</c:forEach>
		</select>
	</td>

</tr>


<tr>
	<td ><b> Action Type  :</b></td>
	
	<td><select id ="actionType" name ="actionType" onchange="showGpsSearchOption(this.value);">
			<option value='SELECT' <c:if test="${selectedActionType eq 'SELECT'}">selected</c:if> >--- SELECT -- </option>
			<c:forEach items="${actionList}" var="action">
				<option value='${action}' <c:if test="${action == selectedActionType}">
				selected</c:if>>${action}</option>
			</c:forEach>
		</select>
	</td>
</tr>

<tr>
	<td ><b> Web Config Id  :</b></td>
	
	<td><select id ="webConfigId" name ="webConfigId" onchange="submitForms('search');">
			<option value='ALL' <c:if test="${selectedWebConfigId eq 'ALL' }">selected</c:if> >ALL</option>
			<c:forEach items="${webConfigList}" var="webConfig">
				<option value='${webConfig.configId}' 
				<c:if test="${webConfig.configId == selectedWebConfigId}">
				selected</c:if>>${webConfig.configId}</option>
			</c:forEach>
		</select>
	</td>
</tr>

<tr id="gpsSearchRowId">
	<td><b> GPS Search  :</b></td>
	<td><select id ="gpsSearch" name ="gpsSearch" onchange="submitForms('search');">
			<option value='ALL' <c:if test="${selectedGpsSearchOption eq 'ALL' }">selected</c:if> >ALL</option>
			<option value='Yes' <c:if test="${selectedGpsSearchOption eq 'Yes' }">selected</c:if> >Yes</option>
			<option value='No' <c:if test="${selectedGpsSearchOption eq 'No' }">selected</c:if> >No</option>
		</select>
	</td>
</tr>

<tr>
	<td  align="center" colspan="2">
	<br />
		<input type="button" value="Get Data" onclick="submitForms('search');" class="medButton">
	</td>
</tr>


</table>
	<br />
	<c:if test="${not empty showGrid and null ne showGrid and showGrid eq 'Event'}">
		<c:if test="${not empty zoneTicketsTrackings}" >
		<div id="gridTable" align="center" >
	
			<display:table list="${zoneTicketsTrackings}" style="width:100%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   
			id="audit"  requestURI="#" class="list">
				<display:column title="Hit Date" sortable="true"><fmt:formatDate type="both" value="${audit.hittingDate}" /></display:column>
				<display:column title="WebConfigId" >${audit.webConfigId}</display:column>		
				<display:column title="IP Address" sortable="true">${audit.customerIpAddress}</display:column>
				<display:column title="AuthenticatedHit" >${audit.authenticatedHit}</display:column>
				<display:column title="GPS Search" >${audit.eventGpsSearch}</display:column>
				<display:column title="Search Value" >${audit.eventSearchValue}</display:column>
				
	     	</display:table>
		<br />
		</div>	
		<br />
		
		
		</c:if>
		<c:if test="${empty zoneTicketsTrackings}" >
		<div align='center'>
			 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
		</div>
			
			<br/>
			<br/>		
		</c:if>
	</c:if>
	
	
	<c:if test="${not empty showGrid and null ne showGrid and showGrid eq 'Order'}">
		<c:if test="${not empty zoneTicketsTrackings}" >
		<div id="gridTable" align="center" >
	
			<display:table list="${zoneTicketsTrackings}" style="width:100%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   
			id="audit"  requestURI="#" class="list">
				<display:column title="Hit Date" sortable="true"><fmt:formatDate type="both" value="${audit.hittingDate}" /></display:column>
				<display:column title="WebConfigId" >${audit.webConfigId}</display:column>		
				<display:column title="IP Address" sortable="true">${audit.customerIpAddress}</display:column>
				<display:column title="Order No" >${audit.userOrder.id}</display:column>
				<display:column title="Order Date" sortable="true">${audit.userOrder.wsOrderDate}</display:column>
				<display:column title="Order Total" >${audit.userOrder.orderTotal}</display:column>
				<display:column title="Zone" >${audit.userOrder.ticketGroup.zone}</display:column>
				<display:column title="Qty" >${audit.userOrder.ticketGroup.quantity}</display:column>
				<display:column title="Unit Price" >${audit.userOrder.ticketGroup.unitPrice}</display:column>
				<display:column title="Event Details" >${audit.userOrder.zoneEvent.eventName},
				${audit.userOrder.zoneEvent.eventDate} ${audit.userOrder.zoneEvent.eventTime}</display:column>
				<display:column title="Venue Details" >${audit.userOrder.zoneEvent.venueName},
				${audit.userOrder.zoneEvent.venueCity},${audit.userOrder.zoneEvent.venueState},${audit.userOrder.zoneEvent.venueCountry}</display:column>
				<display:column title="Customer Name" >${audit.userOrder.customer.firstName} ${audit.userOrder.customer.lastName}</display:column>
				<display:column title="Customer Email" >${audit.userOrder.customer.email}</display:column>
	     	</display:table>
		<br />
		</div>	
		<br />
		
		
		</c:if>
		<c:if test="${empty zoneTicketsTrackings}" >
		<div align='center'>
			 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
		</div>
			
			<br/>
			<br/>		
		</c:if>
	</c:if>
	
	<c:if test="${not empty showGrid and null ne showGrid and showGrid eq 'Ticket'}">
		<c:if test="${not empty zoneTicketsTrackings}" >
		<div id="gridTable" align="center" >
	
			<display:table list="${zoneTicketsTrackings}" style="width:100%;"  decorator="org.displaytag.decorator.TotalTableDecorator"   
			id="audit"  requestURI="#" class="list">
				<display:column title="Hit Date" sortable="true"><fmt:formatDate type="both" value="${audit.hittingDate}" /></display:column>
				<display:column title="WebConfigId" >${audit.webConfigId}</display:column>		
				<display:column title="IP Address" sortable="true">${audit.customerIpAddress}</display:column>
				<display:column title="Event Id" >${audit.eventId}</display:column>
				<display:column title="Event Details" >
					<c:choose>
						<c:when test="${null ne audit.zoneEvent.eventName and audit.zoneEvent.eventName ne '' }">
							${audit.zoneEvent.eventName},
						${audit.zoneEvent.eventDate} ${audit.zoneEvent.eventTime}
						
						</c:when>
						<c:otherwise>
							-
						</c:otherwise>
					</c:choose>
				</display:column>
				
				<display:column title="Venue Details" >
					<c:choose>
						<c:when test="${null ne audit.zoneEvent.eventName and audit.zoneEvent.eventName  ne '' }">
							${audit.zoneEvent.venueName},
							${audit.zoneEvent.venueCity},${audit.zoneEvent.venueState},${audit.zoneEvent.venueCountry}
						</c:when>
						<c:otherwise>
							-
						</c:otherwise>
					</c:choose>
				
				</display:column>
	     	</display:table>
		<br />
		</div>	
		<br />
		
		
		</c:if>
		<c:if test="${empty zoneTicketsTrackings}" >
		<div align='center'>
			 <h4><span  id="emptyMsg">There isn't any details.</span></h4> 
		</div>
			
			<br/>
			<br/>		
		</c:if>
	</c:if>

</form:form>
<script type="text/javascript">
showGpsSearchOption('${selectedActionType}')
</script>
