
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%-- <jsp:include page="../decorators.jsp" /> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<br/>
<br/>
<br/>
 <b>Event : ${ztEvent.eventName}</b>
	<br/>
	<b>Event Date : <fmt:formatDate pattern="MM/dd/yyyy" value="${ztEvent.eventDate}" />				
					<c:choose>
					<c:when test="${ztEvent.eventTime == null}">
								 TBD
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm aa" value="${ztEvent.eventTime}" />
					</c:otherwise>
					</c:choose>
	</b>
	<br/>
	 <b>Venue : ${ztEvent.venueName}</b>
	<br/>
	<br/>
<c:if test="${not empty audits}" >
		<display:table list="${audits}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="audit"  requestURI="#" class="list">
		<display:column title="Updated Date "><fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.lastupdatedDate}" /></display:column>
		<display:column title="Action" >${audit.action}</display:column>
		<display:column title="Updated By ">${audit.lastupdatedBy}</display:column>
		<display:column title="Zone" >${audit.zone}</display:column>
		<display:column title="Additional Markup" >${audit.additionalMarkup}</display:column>
	</display:table>
</c:if>
<c:if test="${empty audits}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit for this Zone.</span></h4> 
	</div>
	<br/>
	<br/>		
</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>
			
			
