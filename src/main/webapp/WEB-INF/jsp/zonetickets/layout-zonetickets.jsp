<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="userSubMenu">
	
	<c:choose>
		<c:when  test="${selectedSubMenu == 'RewardTheFan Listings Additional Markup'}">
			<b>RewardTheFan Listings Additional Markup</b>
		</c:when>
		<c:otherwise>
			<a href="ManageZoneTicketsAdditionalMarkup">RewardTheFan Listings Additional Markup</a>
		</c:otherwise>
	</c:choose>
	</div>
	
	
<tiles:insertAttribute name="subBody" />