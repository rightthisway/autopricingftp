<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
<meta http-equiv="Content-type" content="text/plain; charset=UTF-8"/>
</head>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > ${url}
</div>
<h1>RewardTheFan Listings Additional Markup</h1>
<br/>


<form:form action="ManageZoneTicketsAdditionalMarkup" method="post" id="formId">

<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
</c:if>
<c:if test="${not empty message}"> 
	<div id="message" style="color: red;"><b>${message}</b></div>
</c:if>
	<br/>
	<br/>
	<table width="80%">
		<tr>
			<td><b>Select Child or GrandChild or Artist or Venue:</b></td>
			<td>
				<input type="hidden" id="artist" name="artist" value ="${artist}">
				<input type="hidden" id="venue" name="venue" value ="${venue}">
				<input type="hidden" id="grandChild" name="grandChild" value ="${grandChild}">
				<input type="hidden" id="brokerId" name="brokerId" value ="${brokerId}">
				<input type="text" id="autoArtistVenue" name="autoArtistVenue" onclick="clearArtist();" >
			</td>
			<td>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b><span id="selectedOption" >${selectedOption}</span></b></td>
			<td><b><span id="selectedValue" >${selectedValue}</span></b></td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Events:</b></td>
			<td >
				<input type="hidden" id="eventStr" name="eventStr" value = ${eventStr}/> 
				<input type="checkbox" id="eventsCheckAll" name="eventsCheckAll" onclick ="selectAllEvents()" <c:if test ="${eventsCheckAll}"> checked="checked" </c:if>/> 
				<label for="eventsCheckAll">Select All</label>
				<br/>
				<select id='events' name="events"  multiple="multiple" size='6' style="width:480px;" >
					<c:forEach items="${events}" var="event">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${event.id}${temp}"/>
						<option 
							<c:if test="${event!=null && fn:contains(eventStr,temp2)}"> Selected </c:if>
							value="${event.id}"> ${event.name} <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								${event.venue.building}
						</option>
					</c:forEach> 
				</select>
			</td>
			<td></td>
			<td colspan="3" align="center"></td>
			
		</tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>			
		
		<tr>
			<td colspan="6" align="center">
				<input type="button" value="Get Pricing" onclick="submitForms('search');" class="medButton">
			</td>
		</tr>
	</table>
	
	<c:if test="${not empty ztAdditionalMarkups}" >
	<div id="ebayInventoryTabel" >
	
		<!-- <div id="copyCheckBox" style="float:center;">
			<input onclick="javascript:copyAllEventData();" type="checkbox" name="copyAllCheckbox" id="copyAllCheckbox" ><b>Copy  first row  to all remaining rows</b> &nbsp;&nbsp;&nbsp;
		</div>
		<div style="float:right; padding-right:320px;">
			
		<input type="button" value="Update All"    class="medButton" id="update3" name="update3" 
		onclick="javascript:ebayExposureUpdate('A');"/> &nbsp;&nbsp;&nbsp;
		
		<input type="button" value="Update"   class="medButton" id="update2" name="update2"onclick="javascript:ebayExposureUpdate('S');"/> 
		</div> -->
		
		<div style="float:right; padding-right:320px;">
			<input type="button" value="Update"  name="update1" id="update1" class="medButton" onclick="javascript:submitForms('update');"/> 
		</div>
		<br/>
		<br/>
		
		<display:table list="${ztAdditionalMarkups}"  decorator="org.displaytag.decorator.TotalTableDecorator"  
		 id="ztAdditionalMarkup"  requestURI="#" class="list">

			<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
				<input type="checkbox" name="checkbox_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}" id="checkbox_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}" <c:if test ="${ztAdditionalMarkup.id !=null}"> checked="checked" </c:if> class="selectCheck" />
			</display:column>
			
			<%-- <display:column  title ="<input type='checkbox' id='copyAll' name='copyAll'>">  
				 <input type="checkbox" value="${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}" 
				 name="select" class="ebayCheckboxes"/>	
				<input type="hidden" id="event_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}" value="${ztAdditionalMarkup.ztEvent.id}"/>
				<input type="hidden" id="zone_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}" 
				name="zone_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}" value="${ztAdditionalMarkup.zone}"/>
				<input type="hidden" id="venue_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}" value="${ztAdditionalMarkup.zone}"/>
			</display:column> --%>
				
			<display:column title="Venue" sortable="true" group="1"> ${ztAdditionalMarkup.ztEvent.venueName}</display:column>
			<display:column title="Event" sortable="true"  group="1">${ztAdditionalMarkup.ztEvent.eventName} &nbsp;
				<fmt:formatDate pattern="MM/dd/yyyy" value="${ztAdditionalMarkup.ztEvent.eventDate}" />		
				<c:choose>
					<c:when test="${ztAdditionalMarkup.ztEvent.eventTime == null}">
						TBD
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm aa" value="${ztAdditionalMarkup.ztEvent.eventTime}" />
					</c:otherwise>
				</c:choose>
				&nbsp; -  &nbsp;&nbsp; <fmt:formatDate pattern="EEEE" value="${ztAdditionalMarkup.ztEvent.eventDate}" />	
			</display:column>
			
			<display:column title="Zones"  >${ztAdditionalMarkup.zone}</display:column>
				
			<display:column title="<input type='checkbox' name='copyAllAdditionalMarkup' id='copyAllAdditionalMarkup' > <br/>Additional Markup" >
				<input type="text"  id="additionalMarkup_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}" 
				size="5" name="additionalMarkup_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}"
				value="${ztAdditionalMarkup.additionalMarkup}"
				 onchange="selectRow('${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}');" >
			</display:column>
			
			<display:column title="Action" >
			 	<input type="button" value="Audit"    class="medButton" id="audit_${ztAdditionalMarkup.ztEvent.id}-${ztAdditionalMarkup.zone}"
				onClick="popupAudit('${ztAdditionalMarkup.ztEvent.id}','${ztAdditionalMarkup.zone}');"/>
			 	
			 	<%-- <input type="hidden" id="eventName_${ztAdditionalMarkup.ztEvent.id}_${ztAdditionalMarkup.zone}" 
			 	 value="${ztAdditionalMarkup.ztEvent.eventName}" />
			 	 
			 	<input type="hidden" id="venueName_${ztAdditionalMarkup.ztEvent.id}_${ztAdditionalMarkup.zone}"
			 	 value="${ztAdditionalMarkup.ztEvent.venueName}" /> --%>
			 </display:column>
					
		</display:table>
			
			
			
			<br/>
			<div style="float:right; padding-right:320px;">
			<input type="button" value="Update"  name="update2" id="update2" class="medButton" onclick="javascript:submitForms('update');"/> 
		</div>
			</div>
			
	</c:if>
	<c:if test="${empty ztAdditionalMarkups}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
		<table id="listAudit"></table>
		<br/>
		<br/>
	</c:if>
	<input type="hidden" name="action" id="action"/>
	<br/>
	<br/>
	
</form:form>

<script type="text/javascript">

function clearArtist(){
	$("#message").hide();
}

$(document).ready(function(){
	var allEvents = '${eventsCheckAll}';
	var isUpdate = 'true';
	function selectCheckBox(){
		if(allEvents=='true'){
			$('#eventsCheckAll').attr("checked","checked");
		}
		allEvents='false';
	}
	selectCheckBox();
	
	
	 $('#autoArtistVenue').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		  
			$('#autoArtistVenue').val('');
			$('#selectedValue').text(row[2]);
			if(row[0]=='ARTIST'){
				$('#selectedOption').text('Artist');
				$("#artist").val(row[1]);
				$("#grandChild").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('artistId',row[1]);
				
			} else if(row[0]=='VENUE'){
				$('#selectedOption').text('Venue');
				$("#venue").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#child").val('');
				getEvents('venueId',row[1]);
				
			} else if(row[0]=='CHILD'){
				$('#selectedOption').text('Child');
				$("#child").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#venue").val('');
				getEvents('childId',row[1]);
				
			} else {
				$('#selectedOption').text('GrandChild');
				$("#grandChild").val(row[1]);
				$("#artist").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('grandChildId',row[1]);
				
			} 
	}); 

	$("#events").change(function () {
		if(isUpdate!='true'){
			$('#ebayInventoryTabel').hide();
			$("#events option:selected").each(function () {
				$('#eventsCheckAll').removeAttr("checked");
			});
		}
		isUpdate='false'; 
	}).trigger('change');
	
	   
	
});
	
function getEvents(isArtist,id){
	
	var url = "";
	var brokerId = $("#brokerId").val();
	if(isArtist == 'artistId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?artistId="+id+"&brokerId="+brokerId;
	}else if(isArtist == 'venueId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?venueId="+id+"&brokerId="+brokerId;
	} else if(isArtist == 'childId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?childId="+id+"&brokerId="+brokerId;;
	} else{
		url = "GetEventsByGrandChildAndArtistAndVenue?grandChildId="+id+"&brokerId="+brokerId;
	}
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#events').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.name+" "+data.date+" "+data.time+" "+data.venue+" "+data.city+" "+data.state+"</option>"
				$('#events').append(rowText);
            }	
		}
	}); 
} 
function validateForm(){
		var flag= true;
		$('.selectCheck:checkbox:checked').each(function () {
			
			var id,value;
			id = this.id.replace('checkbox','additionalMarkup');
			value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid additional markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
		});
		if(!flag){
			return flag;	
		}
		
		return true;
}
function submitForms(action){
	if(action=='update'){
		if(validateForm()){
			$("#action").val(action);
			$("#formId").submit();
		}
	}else{
		  var event=$("#events").val()
		  if(event!=null){
		    $("#action").val(action);
		    $("#formId").submit();
		  }else{
			  alert('Please select the events.');
		  }
	}

}

$(function () {
	
	$('#copyAllAdditionalMarkup').click(function(){
		if($('#copyAllAdditionalMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('additionalMarkup');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
});

function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}
function selectRow(id) {
	$("#checkbox_" + id ).attr('checked', true);
}

function selectAllEvents(){
	if((document.getElementById("eventsCheckAll").checked)){
		$("#events").each(function(){
			$("#events option").attr("selected","selected"); 
		});

	}
	else{
			$("#events").each(function(){
			$("#events option").removeAttr("selected"); 
		});
	}
}

function popupAudit(eId,zone){
	var url = "ZoneTicketsAdditionalMarkupAuditPopup?eventId="+eId+"&zone="+zone;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
} 
	
</script>