<%@page import="org.springframework.security.context.SecurityContextHolder"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%-- <%
String username = (String)pageContext.getAttribute("username");
%> --%>
<%
String username = SecurityContextHolder.getContext().getAuthentication().getName();
%>

<div id="header">

<div font-size:35px;font-weight: bold;">
<img src="<%=request.getContextPath()%>/images/autopricing-1.png" border="0" height="100" width="150"/>
</div>
<div style="clear:both">&nbsp;&nbsp;&nbsp;&nbsp;</div>
<div style="clear:both">&nbsp;&nbsp;&nbsp;&nbsp;</div>
 <c:if test="${username != 'anonymousUser'}">
	      <div style="text-align: right; margin-right: 5px">
	      	<div style="height: 40px">
		        <img src="../images/ico-user.gif" align="absbottom" /><a href="MyAccount"><%=username%></a> | 
               <!--  <a href="Bookmarks"><img src="../images/ico-star-on.gif" align="absbottom" />Starred</a> | -->
		        <a href="j_acegi_logout"><img src="../images/ico-delete.gif" align="absbottom" /> Log out</a>
		        
		        <%-- <authz:authorize ifAnyGranted="PRIV_CRAWLER">  --%>       
			    	<%-- <div id="crawlerDiv" style="color: #0000ee"><span id="crawlerSpan">${global.numNodes} <img src="../images/ico-screen-blue.gif" align="absbottom"/></span></div> --%>
			   <%--  </authz:authorize> --%>
  			    <!-- <div style="margin-top:3px">
				  <img src="../images/ico-clock.gif" align="absbottom"/>
				  <span id="headerTime"></span><br/>
			      <span id="headerDate" style="font-weight: bold"></span>
			    </div> -->
		    </div>
	      </div>
      </c:if>
      
      <div>
      
      	<div style="float:left;width:10px">&nbsp;</div>
      	
      	 	<a id="tab_AutoPricing" class="tabHeaderOff" href="AutoPricing" style="font-size: medium; ">
	      		AutoPricing
	      	</a>
	      
	    <!--   <a id="tab_data" class="tabHeaderOff" href="Data" style="font-size: medium; ">
	      		Data
	      	</a> -->
	      	<!-- <a id="tab_fileUpload" class="tabHeaderOff" href="GetAutomaticFileUpload" style="font-size: medium;">
	      		File Upload
	      	</a> -->
	    
	      	
	       	<a id="tab_AuditInfo" class="tabHeaderOff" href="AutocatProjectAudits" style="font-size: medium; ">
	      		Audit
	      	  </a> 
        <security:authorize ifAnyGranted='ROLE_SUPER_ADMIN'>

			<a id="tab_ZoneTickets" class="tabHeaderOff" href="ManageZoneTicketsAdditionalMarkup" style="font-size: medium; ">
	      		RewardTheFan Listings
	      	  </a> 
	        <a id="tab_admin" class="tabHeaderAdminOff" href="ManageAutoPricingBroker" style="font-size: medium; ">
	      		Admin
	      	</a>
       </security:authorize>
     
	      
	      <!-- <a id="tab_settings" class="tabHeaderOff" href="LocationSettings" style="font-size: medium; ">
	      	Settings
	      </a> -->
	      

        <div style="clear: both"></div>
      </div>
</div>


<script type="text/javascript">
	$('#tab_${selectedTab}').attr('class', 'tabHeaderOn');
	
	
  <c:choose>
	<c:when test="${fn:contains(selectedTab, 'admin')}">
		$('#tab_${selectedTab}').attr('class', 'tabHeaderAdminOn');
	</c:when>
  </c:choose>
	
 
/*<c:choose>
	<c:when test="${fn:contains(selectedTab, 'fileUpload')}">
		$('#tab_${selectedTab}').attr('class', 'tabHeaderOn');
	</c:when>
	<c:when test="${fn:contains(selectedTab, 'fileUpload')}">
		$('#tab_${selectedTab}').attr('class', 'tabHeaderOn');
	</c:when>
    <c:when test="${fn:contains(selectedTab, 'fileUpload')}">
      $('#tab_${selectedTab}').attr('class', 'tabHeaderOn');
    </c:when>
    <c:when test="${fn:contains(selectedTab, 'ftpAuditInfo')}">
    $('#tab_${selectedTab}').attr('class', 'tabHeaderOn');
  </c:when>
  <c:when test="${fn:contains(selectedTab, 'settings')}">
  $('#tab_${selectedTab}').attr('class', 'tabHeaderOn');
</c:when>
    <c:otherwise>
      $('#tab_${selectedTab}').attr('class', 'tabHeaderOn');
    </c:otherwise>
  </c:choose>
*/
  </script>