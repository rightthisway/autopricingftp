	<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
	<body>
		<display:table list="${brokers}" id="broker" class ="list">
			<display:column title="Broker" >${broker.broker.name}</display:column>
			<display:column title="DBConnection"><c:choose><c:when test="${broker.isDBConnection}"> OK </c:when> <c:otherwise> Not Connected </c:otherwise></c:choose></display:column>
			<display:column title="Product" >${broker.product.name}</display:column>
			<display:column title="LastRunTime" >${broker.lastRunTime}</display:column>
			<display:column title="Process" >${broker.processType}</display:column>
			<display:column title="ChangeCount" >${broker.count}</display:column>
		</display:table>
		
	</body>