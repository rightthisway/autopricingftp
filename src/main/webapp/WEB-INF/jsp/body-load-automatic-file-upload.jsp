<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(function() {
			$( "#uploadDate" ).datepicker({ minDate: 0});
			
		});
		
		
	</script>
</head>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
		  <a href="..">File Upload</a> 
		  &gt; Manage FTP Accounts
		</div>

<h1>Manage FTP Accounts</h2>

<div align="center">
	
	
	<display:table class="list" name="${ftpUserAccounts}" id="ftpAccount" requestURI="LoadAutomaticFileUploadInfo">
    
    <display:column title="ID" sortable="true" property="accountNo" />
    <display:column title="User Name" sortable="true" property="userName" />
    <display:column title="Host" sortable="true" property="hostName" />
    <%-- <display:column title="File Source" sortable="true" property="fileSource" /> --%>
    
    <display:column title="Frequency" sortable="false">
    <c:choose>
      <c:when test="${ftpAccount.frequency gt 60}">
            ${ftpAccount.frequency/60} Hours
      </c:when>
      <c:otherwise>
            ${ftpAccount.frequency} Minutes
      </c:otherwise>
    
     </c:choose>
    </display:column>
    
    <display:column title="Next Upload Time" sortable="false">
    	<fmt:formatDate type="both" value="${ftpAccount.nextUploadTime}" />
    </display:column>
    
    <display:column>
    	<a href="GetEditFtpUserAccount?action=edit&id=${ftpAccount.accountNo}"><img src="../images/ico-edit.gif" align="absbottom" /> Edit</a>
    </display:column>
    
</display:table>

</div>

