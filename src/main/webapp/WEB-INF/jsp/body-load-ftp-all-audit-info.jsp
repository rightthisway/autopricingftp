<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		function callClearMethod(action){
			$( "#action" ).val(action);
			$( "#formId" ).submit();
		}
		
		function changeUserName(userName) {
			document.location.href = "LoadAllFTPAuditInfo?userName=" + userName;
			
		}
		
	</script>
</head>

<div id="breadCrumbPath" class="breadCrumbPathUser">
		  <a href="..">Audit</a> 
		  &gt; Successful FTP Audit
		</div>

<h1>All FTP Audit</h2>


<div align="center">

	
	
	<div>
		<table>
			<tr>
				<td><b>User Name :</b> </td>
				<td>
					<select name="userName" name="userName" onchange="changeUserName(this.value);">
						<option value="All" <c:if test="${selectedUserName eq 'All'}">selected</c:if> >All</option>
						<c:forEach items="${ftpUserAccounts}" var="userAccount">
							<option value="${userAccount.userName}" <c:if test="${selectedUserName eq userAccount.userName}">selected</c:if>  >${userAccount.userName}</option>
						</c:forEach>
					 	
				 	</select>
				 </td>
			</tr>
		
		</table>
	
	</div>
	<div>&nbsp;&nbsp;&nbsp;&nbsp;</div>
	<div>&nbsp;&nbsp;&nbsp;&nbsp;</div>
	
	<display:table class="list" name="${ftpFileUploadAudits}" id="ftpAudit" requestURI="LoadFTPAuditInfo">
		<display:column title="Upload Type" sortable="true" property="uploadType" />
		<display:column title="FTP Type" sortable="true" property="ftpType" />
	    <display:column title="Host" sortable="true" property="hostName" />
	    <display:column title="Port" sortable="true" property="port" />
	    <display:column title="Destination Sub Directory on Host" sortable="true" property="destination" />
	    <display:column title="User Name" sortable="true" property="userName" />
	    <display:column title="File Name" sortable="true" property="fileSource" />
	    <display:column title="Last Upload Time" sortable="false">
	    	<fmt:formatDate type="both" value="${ftpAudit.lastUploadTime}" />
	    </display:column>
	   	<display:column title="File Size" sortable="true"  >
	   		${ftpAudit.fileSize} KB
	   	</display:column>
	   	<display:column title="Status" sortable="true" property="status" />
	   	
	</display:table>

</div>

