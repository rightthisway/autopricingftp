<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		function callSaveMethod(action){
			alert("action--->"+action)
			$( "#action" ).val(action);
			$( "#formId" ).submit();
		}
	
	</script>
</head>

<div id="breadCrumbPath" class="breadCrumbPathUser">
		  <a href="..">Audit</a> 
		  &gt; Successful FTP Audit
		</div>

<h1>Successful FTP Audit</h2>


<div align="center">

<form method="POST" action="LocationSettings" id='formId' enctype="multipart/form-data" >
<input type="hidden" id="ftpLocationsRowCount" name="ftpLocationsRowCount" value="${ftpLocationListSize}">
<input type="hidden" name="action" id="action"  >
<table border="1" width="75%">

	<tr>
	
		<td>
			<table border="1" width="75%" id="tableID" align="center">
			<tr>
				<td style="font: bold; color: black;" align="center"><b>Action</b></td>
				<td style="font: bold; color:navy;" align="center"><b>Location</b></td>
			</tr>
				<c:choose>
					<c:when test="${ftpLocationList ne null && ftpLocationList ne ''}">
						<c:forEach items="${ftpLocationList}" var="ftpLocationObj"
							varStatus="indexCount">
							<tr id="rowID" align="center">
								<td>
									<input type="hidden"  name="rowHiddenId_${indexCount.index}" id="rowHiddenId_${indexCount.index}" value='${indexCount.index}' /> 
									<input type="hidden" name="locationId_${indexCount.index}" id="locationId_${indexCount.index}" value='${ftpLocationObj.id}' /> 
									<input type="checkbox" name="deleteRow_${indexCount.index}" id="deleteRow_${indexCount.index}" />
								</td>
	
								<td class="formInputs" align="center"><input type="text" name="ftpLocation_${indexCount.index}"
									id="ftpLocation_${indexCount.index}" value="${ftpLocationObj.location}"/>
									
									<label class="error" for="ftpLocation" id="ftpLocation_error_${indexCount.index}"><br>Please enter Location</label>
								</td>
								
							</tr>
						</c:forEach>
					</c:when>
				</c:choose>
			</table>
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 <input  id="getIpButton" type="button" onclick="addNewRow();" class="medButton" value="Add Location"/>
			<input id="getIpButton" type="button" onclick="removeConfirm('tableID',k,'deleteRow_');"  class="medButton" value="Remove Location"/>
		
		</td>
	
	</tr>
	<tr>
		<td align="center"  ><input id="saveButton" type="button" onclick="callSaveMethod('save');" class="medButton" value="Save"/></td>
	</tr>



</table>
</form>
	
	
</div>


<script>
	var k='${ftpLocationListSize}';
	document.getElementById("ftpLocationsRowCount").value=k; 
	function addNewRow(){
		var ipAddress = null;
		
		var oldRow =k-1;
		if(null != document.getElementById("rowHiddenId_"+oldRow)){
			ipAddress =document.getElementById("ftpLocation_"+oldRow).value;
		}
		
		if(null != ipAddress && ipAddress != ""){
			
		$('#ftpLocation_error_'+oldRow).hide();
		
		$("#rowID").clone().find("input,label").each(function() {       

			currentId=this.id;
	        var splitedTag = currentId.split('_');
	        
	        if(this.tagName == 'LABEL'){
	        	 $(this).attr('id',splitedTag[0] +"_"+splitedTag[1] +"_"+k);
	             $(this).attr('name',splitedTag[0] +"_"+splitedTag[1] +"_"+k);
	             currentId=this.id;	
	        }else{
	        	 $(this).attr('id',splitedTag[0] +"_"+k);
	             $(this).attr('name',splitedTag[0] +"_"+k);
	             currentId=this.id;	
	        }
	       
	               

	        if(this.tagName == 'INPUT') {
	            if(($(this).attr('name')) == ('rowHiddenId_'+k)) {
	          		$(this).attr('value',k);
	            }else {
	         	   $(this).attr('value','');
	            } 
	         }		       
	  }).end().appendTo("#tableID");  
	   k++;    
	   document.getElementById("ftpLocationsRowCount").value=k;
		}else{
			$("#ftpLocation_error_"+oldRow).show();
			$("#ftpLocation").focus();
		}
	 }
	 
	function removeConfirm(tableID,rowCount,columnName){

		var checkCount=0;
		for(var i=0;i<rowCount;i++){
			if(null !=document.getElementById(columnName+i)){
				if(document.getElementById(columnName+i).checked) {
					checkCount+=1;
				}
			}
		}
		
		if(checkCount > 0){
			var conf=confirm("Do you want to remove selected Location");
		    if(conf) { 
		    	removeRows(tableID,rowCount,columnName);
		    }else {
		        return false;
			 }
		} else {
			alert("Please check the checkbox to remove Location");
		}
	 }
	 
	function removeRows(tableID,rowCount,columnName) {
		
		var removeCount=0;

		for(var i=0;i<rowCount;i++) {
			 if(document.getElementById(columnName+i) !=null){
				if(document.getElementById(columnName+i).checked) {

					var table= document.getElementById(tableID);
					var size=table.rows.length;
					
					if(size > 2) {
						var elementObj = document.getElementById(columnName+i);
						var rowObj = elementObj.parentNode.parentNode;
						if(rowObj != null) {	
							 table.deleteRow((rowObj.rowIndex));						 
						}
					} else {
						alert("Atleast one Location should be present");
						document.getElementById(columnName+i).checked=false;
					}
			    }
		    }
		 }
		
		if(removeCount > 0){
			alert("FTP Locations Removed Sucessfully");
		}
	 } 
		
	</script>

