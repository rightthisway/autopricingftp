<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<tiles:useAttribute id="selectedTab" name="selectedTab" scope="request" />
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=1000" >
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="0">
		<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	</head>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    	
    	<%String title = (String)session.getAttribute("title"); %>
    	
   		<c:if test="${title!=null}" >
   		<title>${title}</title>
   		</c:if>
   		<c:if test="${title==null}" >
        <title><tiles:getAsString name="title"/></title>
        </c:if>
        
        <style type="text/css">
            @import url(../css/common.css);
		    @import url(../css/jquery/jquery-all.css);
		</style>
		
       	<link rel="stylesheet" href="../css/jquery-ui-1.8.18.custom.css">
		<script src="../js/jquery-1.7.1.min.js"></script>
		<script src="../js/jquery-ui-1.8.18.custom.min.js"></script>
		<script src="../js/jquery-validate.js"></script>
		 <script type='text/javascript' src='../js/scripts.js'></script> 
		  
		<script type='text/javascript' src='../js/date.js'></script>
		<script type='text/javascript' src='../js/utils.js'></script>

		<script type="text/javascript" src="../js/FABridge.js"></script>
		<script type="text/javascript" src="../js/swfobject.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery-autocomplete.js"></script>
	
     </head>    	
    <body style="background-color: white;">
	<table width="100%" style="border-collapse:collapse;" height="100%" border="0">
			<tr height="20" >
				<td >
				<%-- <tiles:useAttribute id="selectedSubTab" name="selectedSubTab" scope="request" /> --%>
					<tiles:insertAttribute name="header" />
				</td>
			</tr >
			
			<tr height="625" valign="top" >
				<td>
					<tiles:insertAttribute name="body" />
				</td>
			</tr>
			<tr height="20" >
				<td >
					<tiles:insertAttribute name="footer" />
				</td>
			</tr>
		</table>
		
		<!-- <img src="../images/bar-feedback.gif" style="position:fixed;bottom:30px;right:0px;cursor:pointer" onclick="showFeedbackDialog();" title="Send a feedback" /> -->
		 
	</body>
	<head>
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="0">
		<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	</head>
</html>
	