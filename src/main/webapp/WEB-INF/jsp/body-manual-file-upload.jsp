<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(function() {
			$( "#uploadDate" ).datepicker({ minDate: 0});
			
		});
		
		function showFileName() {
			   myFileList = document.getElementById("file");
	            // loop through files property, using length to get number of files chosen
	              $("#display").html('');
	            for (var i = 0,j = 1; i < myFileList.files.length; i++,j++) {
	                // display them in the div
	                document.getElementById("display").innerHTML += "<br/>"+ j+")" + myFileList.files[i].name ;
	            }
			}
		
		function calleSaveMethod(action){
			var flag= true;

			if (!document.getElementById('ftpType1').checked && 
					!document.getElementById('ftpType2').checked) {
				alert('Please select FTP Type .');
				$("#ftpType1").focus();
				flag = false;
				return ;
			}
			
			var value = $.trim($("#ftpHost").val());
			if( value =='' ){
				alert('Please enter FTP Host.');
				$("#ftpHost").focus();
				flag = false;
				return ;
			}  
			
			value = $.trim($("#ftpUserName").val());
			if( value =='' ){
				alert('Please enter FTP Account User Name.');
				$("#ftpUserName").focus();
				flag = false;
				return ;
			}
			
			value = $.trim($("#ftpPassword").val());
			if( value =='' ){
				alert('Please enter FTP Account Password.');
				$("#ftpPassword").focus();
				flag = false;
				return ;
			}
			
			value = $.trim($("#file").val());
			if( value =='' ){
				alert('Please Choose proper file.');
				$("#file").focus();
				flag = false;
				return ;
			}
			
			if(flag){
				
				$( "#action" ).val(action);
				$( "#formId" ).submit();
			}
			
		}
		
		function callClearMethod(action){
			$( "#action" ).val(action);
			$( "#formId" ).submit();
		}
		
	</script>
</head>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
		  <a href="..">File Upload</a> 
		  &gt; Manual File Uploader 
		</div>

<h1>Manual File Uploader</h2>
	

<div align="center">
	<form method="POST" action="GetManualFileUpload" id='formId' enctype="multipart/form-data" >
	<input type="hidden" name="action" id="action"  >
		<%-- <div style="color: green; font-size: medium;" ><b>${message} </b></div> --%>
		
			<table>
	      <tr>
	         <td> 
	                 <c:forEach var="message" items="${messageList}"> 
	                     <div style="color: green; font-size: medium;" ><b style="margin-left: 218px;">${message}</b></div></br>
	                 </c:forEach>
	         </td>
	      </tr>
	      <tr>
	         <td> 
	                 <c:forEach var="message" items="${failureMessageList}"> 
	                     <div style="color: red; font-size: medium;" ><b style="margin-left: 218px;">${message}</b></div></br>
	                 </c:forEach>
	         </td>
	      </tr>
	    </table>
	    <br />
		<table>
		
			<tr>
				<td colspan="2">
					<b>FTP Type :</b>
				</td>
				<td colspan="2">
					<input type="radio" id="ftpType1" name="ftpType" value="FTP" <c:if test="${ftpType eq null or ftpType eq 'FTP'}">checked</c:if> /> FTP
					<input type="radio" id="ftpType2" name="ftpType" value="SFTP" <c:if test="${ftpType eq 'SFTP'}">checked</c:if>/> SFTP
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Host :</b>
				</td>
				<td colspan="2">
					<input type="text" name="ftpHost" id="ftpHost" style="width: 220px;" value="${ftpHost}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<b>Port :</b>
				</td>
				<td colspan="2">
					<input type="text" name="ftpPort" id="ftpPort" style="width: 220px;" value="${ftpPort}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
						
			<tr>
				<td colspan="2">
					<b>Destination Sub Directory on Host :</b>
				</td>
				<td colspan="2">
					<input type="text" name="subDirectoryHost" id="subDirectoryHost" style="width: 220px;" value="${subDirectoryHost}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>User Name :</b>
				</td>
				<td colspan="2">
					<input type="text" name="ftpUserName" id="ftpUserName" class="group" style="width: 220px;" value="${ftpUserName}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Password :</b>
				</td>
				<td colspan="2">
					<input type="password" name="ftpPassword" id="ftpPassword" class="group" style="width: 220px;" value="${ftpPassword}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Choose File :</b>
				</td>
				
				<td colspan="2">
					<input name="file" type="file" id="file" multiple="multiple" onchange="showFileName();"/>						
				</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			   <td colspan="2"> <div id="display" style="color: green;"></div></td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4" align="center">
					<input type="button" value="Upload"  class="medButton" onclick="calleSaveMethod('save');">
					<input type="button" value="Clear"  class="medButton" onclick="callClearMethod('clear');">
				</td>
			</tr>
				
		</table>
					<%-- <b style="margin-left: 32px;">Host :</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
					<input type="text" name="ftpHost" id="ftpHost" value='${ftpHost}'  style="width: 220px;">
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Port :</b>&nbsp;&nbsp;
				
					<input type="text" name="ftpPort" id="ftpPort" value='${ftpPort}'  style="width: 220px;"></br></br>
			     
			         <b style="margin-left: -132px;">Destination Sub Directory on Host :</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
					<input type="text" name="subDirectoryHost" id="subDirectoryHost" value='${subDirectoryHost}' style="width: 220px;">
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>User Name :</b>&nbsp;&nbsp;
				
					<input type="text" name="ftpUserName" id="ftpUserName" value='${ftpUserName}'  style="width: 220px;"></br></br>
		
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Password :</b>&nbsp;&nbsp;
			
					<input type="password" name="ftpPassword" id="ftpPassword" value='${ftpPassword}'  style="width: 220px;">
					
			        </br></br></br>
                                                                         	
					<b  style="border-right-width: 0px; margin-left: 155px;">Choose File :</b>
				
					<input name="file" type="file" id="file" multiple onchange="showFileName();"/> 
					<table>
					   <tr>
					     <td> <div id="display" style="color: green;"></div></br></td>
					   </tr>
					</table>
			        
				    </br></br>
	
					<input class="medButton" type="button" onclick="calleSaveMethod('save');" value="Upload" style="margin-left: 19px;"> &nbsp;&nbsp;&nbsp;&nbsp;
				
					<input type="button" value="Clear"  class="medButton" onclick="callClearMethod('clear');"> --%>
						
	</form>
	</br>
	   
</div>



