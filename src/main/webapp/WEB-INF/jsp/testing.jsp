<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(function() {
			$( "#startDate" ).datepicker({ minDate: 0});
			$( "#endDate" ).datepicker({ minDate: 0});
			$.validator.addMethod("require_from_group", function(value) {
				var flag=false;
				$('.group').each(function(){
					if ($(this).val()!='') {
						flag=true;
						return flag;
					}
				});
				return flag;
			},"Enter atleast one field apart from config id.");

			
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			var validator = $("#event").validate({
				rules: {
				configId:"required",
				id:"require_from_group"
				},
				messages:{
					configId:"Config id is required."
					
				}
			});
		});
	</script>
</head>
Click <a href="../ws/"> here </a> for complete list of operations.
<div>
	<form action="GetEventList.xml" id="event" target="_blank">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			<tr>
				<td>
					Event Id:
				</td>
				<td>
					<input type="text" name="id" id="id" class="group">
				</td>
			</tr>
			<tr>
				<td>
					Event Name:
				</td>
				<td>
					<input type="text" name="name" id="name" class="group">
				</td>
			</tr>
			<tr>
				<td>
					Start Date:
				</td>
				<td>
				 	<input type="text" name="startDate" id="startDate" class="group">
				</td>
			</tr>
			<tr>
				<td>
					End Date:
				</td>
				<td>
					<input type="text" name="endDate" id="endDate" class="group">
				</td>
			</tr>
			<tr>
				<td>
					Tour Id:
				</td>
				<td>
					<input type="text" name="tourId" id="tourId" class="group">
				</td>
			</tr>
			<tr>
				<td>
					Venue Id:
				</td>
				<td>
					<input type="text" name="venueId" id="venueId" class="group">
				</td>
			</tr>
			<tr>
				<td>
					Location Name:
				</td>
				<td>
					<input type="text" name="location" id="location" class="group">
				</td>
			</tr>
			<tr>
				<td>
					City:
				</td>
				<td>
				 	<input type="text" name="city" id="coty" class="group">
				</td>
			</tr>
			<tr>
				<td>
					State:
				</td>
				<td>
					<input type="text" name="state" id="state" class="group"> (Use Two letter abbrevation)*
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">
XML: http://zoneswebservices.com/ws/GetEventList.xml?configId=xxxx&id=&name=Baltimore&startDate=&endDate=&tourId=&venueId=&location=&city=&state=<br/>
JSON: http://zoneswebservices.com/ws/GetEventList.json?configId=xxxx&id=&name=Baltimore&startDate=&endDate=&tourId=&venueId=&location=&city=&state=<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>
Output:<br/>
<div style="background-color:#CCC">
&lt;list&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;event&gt; <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;42762&lt;/id&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;name&gt;Wicked New York&lt;/name&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventDate&gt;April 20, 2012&lt;/eventDate&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventTime&gt;08:00 PM&lt;/eventTime&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;mapPath&gt;http://zonesmaps.com/1167_WICKED.gif&lt;/mapPath&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venue&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;210&lt;/id&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;location&gt;Gershwin Theatre&lt;/location&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;city&gt;New York&lt;/city&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;state&gt;NY&lt;/state&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;country&gt;US&lt;/country&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/venue&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ACTIVE&lt;/status&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;tour&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;235&lt;/id&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;name&gt;Wicked&lt;/name&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ACTIVE&lt;/status&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/tour&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/event&gt;<br/>
&lt;list&gt;<br/>
</div>

