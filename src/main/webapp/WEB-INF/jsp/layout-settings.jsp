<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu"
	scope="request" />

<div id="adminSubMenu">
	<c:choose>
		<c:when test="${selectedSubMenu == 'FtpLocationSettings'}">
			<b>FTP Location Settings</b>
		</c:when>
		<c:otherwise>
			<a href="LocationSettings">FTP Location Settings</a>
		</c:otherwise>
	</c:choose>
	
</div>
<tiles:insertAttribute name="subBody" />