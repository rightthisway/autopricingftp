<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu"
	scope="request" />

<div id="userSubMenu">
    <c:choose>
		<c:when test="${selectedSubMenu == 'AutocatProjectAudits'}">
			<b>AutoPricingProductAudit</b>
		</c:when>
		<c:otherwise>
			<a href="AutocatProjectAudits">AutoPricingProductAudit</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'ZoneTicketsTracking'}">
			<b>RewardTheFan Tracking</b>
		</c:when>
		<c:otherwise>
			<a href="ZoneTicketsTracking">RewardTheFan Tracking</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'ZoneTicketsOrderTracking'}">
			<b>RewardTheFan Functional Tracking</b>
		</c:when>
		<c:otherwise>
			<a href="ZoneTicketsOrderTracking">RewardTheFan Functional Tracking</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'FtpUploadAudit'}">
			<b>Successful FTP Audits</b>
		</c:when>
		<c:otherwise>
			<a href="LoadFTPAuditInfo">Successful FTP Audits</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'AllFtpUploadAudit'}">
			<b>All FTP Audits</b>
		</c:when>
		<c:otherwise>
			<a href="LoadAllFTPAuditInfo">All FTP Audits</a>
		</c:otherwise>
	</c:choose>
</div>
<tiles:insertAttribute name="subBody" />