<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
     <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
	<script>
	
	var addFile=false;
		$(function() {
			$( "#uploadDate" ).datepicker({ minDate: 0});
			
		});
		
		$( document ).ready(function() {
			$( "#changeFilePathRowID1" ).hide();
			$( "#chooseFile" ).hide();
		});
		
		function removeFile(action){
										    	
								  $("#remove").dialog({
								      resizable: false,
								      height:140,
								      modal: true,
								      buttons: {
								        "Delete File": function() {
								          $( this ).dialog( "close" );
								          $( "#action" ).val(action);
										  $( "#formId" ).submit();
								        },
								        Cancel: function() {
								          $( this ).dialog( "close" );
								        }
								      }
								    });
												
		}
		    function showFileNameChangeFile() {
				   myFileList = document.getElementById("changeFile");
				   $("#display").html('');
		            // loop through files property, using length to get number of files chosen
		            for (var i = 0,j = 1; i < myFileList.files.length; i++,j++) {
		                // display them in the div
		                document.getElementById("display").innerHTML += "<br/>"+ j+")" + myFileList.files[i].name ;
		            }
				}
			function showFileName() {
				   myFileList = document.getElementById("file");
		            // loop through files property, using length to get number of files chosen
		            for (var i = 0,j = 1; i < myFileList.files.length; i++,j++) {
		                // display them in the div
		                document.getElementById("display").innerHTML += "<br/>"+ j+")" + myFileList.files[i].name ;
		            }
				}	
		
		function calleSaveMethod(action){
			var flag= true;
			var value = $.trim($("#ftpHost").val());
			
			if( value =='' ){
				alert('Please enter FTP Host.');
				$("#ftpHost").focus();
				flag = false;
				return ;
			}  
			
			value = $.trim($("#ftpUserName").val());
			if( value =='' ){
				alert('Please enter FTP Account User Name.');
				$("#ftpUserName").focus();
				flag = false;
				return ;
			}
			
			value = $.trim($("#ftpPassword").val());
			if( value =='' ){
				alert('Please enter FTP Account Password.');
				$("#ftpPassword").focus();
				flag = false;
				return ;
			}
			
			value = $.trim($("#uploadDate").val());
			if( value =='' ){
				alert('Please enter Upload Date.');
				$("#uploadDate").focus();
				flag = false;
				return ;
			}
			
			value = $.trim($("#filePath").val());
			if( value =='' ){
				alert('Please Choose proper file path.');
				$("#filePath").focus();
				flag = false;
				return ;
			}
			
			value = $.trim($("#file").val());
			if( value =='' ){
				alert('Please Choose proper file.');
				$("#file").focus();
				flag = false;
				return ;
			}
			
			if(flag){
				
				$( "#action" ).val(action);
				$( "#formId" ).submit();
			}
			
		}
		
		function callClearMethod(action){
			$( "#action" ).val(action);
			$( "#formId" ).submit();
		}
		
		function callUpdateMethod(action){
			var flag= true;
			var value = $.trim($("#ftpHost").val());
			
			var uploadDateHrs=$.trim($("#uploadDateHrs").val());
			var uploadDateMins=$.trim($("#uploadDateMins").val());
			var uploadDate=$.trim($("#uploadDate").val());
			var newUploadDate =new Date(uploadDate);
			newUploadDate.setHours(uploadDateHrs);
			newUploadDate.setMinutes(uploadDateMins);
			if(newUploadDate <= new Date()){
				alert('Upload Date should be greater than current date and time');
				$("#uploadDate").focus();
				flag = false;
				return ;
			}
			
			if( value =='' ){
				alert('Please enter FTP Host.');
				$("#ftpHost").focus();
				flag = false;
				return ;
			}  
			
			value = $.trim($("#ftpUserName").val());
			if( value =='' ){
				alert('Please enter FTP Account User Name.');
				$("#ftpUserName").focus();
				flag = false;
				return ;
			}
			
			value = $.trim($("#ftpPassword").val());
			if( value =='' ){
				alert('Please enter FTP Account Password.');
				$("#ftpPassword").focus();
				flag = false;
				return ;
			}
			
			value = $.trim($("#uploadDate").val());
			if( value =='' ){
				alert('Please enter Upload Date.');
				$("#uploadDate").focus();
				flag = false;
				return ;
			}
			
			
			if(addFile==true){
				
				value = $.trim($("#changeFilePath").val());
				if( value =='' ){
					alert('Please Choose valid file path.');
					$("#changeFilePath").focus();
					flag = false;
					return ;
				}
				
				value = $.trim($("#changeFile").val());
				if( value =='' ){
					alert('Please Choose proper file.');
					$("#changeFile").focus();
					flag = false;
					return ;
				}
			}
			
			if(flag){
				
				$( "#action" ).val(action);
				$( "#formId" ).submit();
			}
			
		}
		
		function callDeleteMethod(action){
			$( "#action" ).val(action);
			$( "#formId" ).submit();
		}
		
		/* function callChangeFile(){
			
			if(document.getElementById("changeFileOption").checked){
				$( "#changeFilePathRowID2" ).show();
				$( "#changeFilePathRowID1" ).show();
				$( "#changeFile" ).show();
				$( ".sourceFile" ).hide();
			}else{
				$( "#changeFilePathRowID2" ).hide();
				$( "#changeFilePathRowID1" ).hide();
				$( "#changeFile" ).hide();
				$( ".sourceFile" ).show();
			}
		} */
		
function callChangeFile(){
			     addFile=true;
				$( "#changeFilePathRowID1" ).show();
				$( "#chooseFile" ).show();
				$("#addFiles").val('addFiles');
				
		} 
		
	</script>
</head>


<div id="breadCrumbPath" class="breadCrumbPathAdmin">
		  <a href="..">File Upload</a> 
		  &gt; Automatic File Uploader 
		</div>

<h1>Automatic File Uploader</h2>
	

<div align="center">
	<form method="POST" action="GetAutomaticFileUpload" id='formId' enctype="multipart/form-data" >
	<input type="hidden" name="addFilesAction" id="addFiles"/>
	<input type="hidden" name="action" id="action"  >
	<input type="hidden" name="id" id="id" value="${ftpUserAccount.accountNo}" >
		<div style="color: green; font-size: medium;" ><b>${message} </b> </div>
		<div style="color: red; font-size: medium;" ><b>${redmessage} </b> </div>
		<div> &nbsp;&nbsp;&nbsp;&nbsp;</div>
	
		<table>
			<tr>
				<td colspan="2">
					<b>FTP Type :</b>
				</td>
				<td colspan="2">
					<input type="radio" id="ftpType1" name="ftpType" value="FTP" <c:if test="${ftpUserAccount.ftpType eq null or ftpUserAccount.ftpType eq 'FTP'}">checked</c:if> /> FTP
					<input type="radio" id="ftpType2" name="ftpType" value="SFTP" <c:if test="${ftpUserAccount.ftpType eq 'SFTP'}">checked</c:if>/> SFTP
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Host :</b>
				</td>
				<td colspan="2">
					<input type="text" name="ftpHost" id="ftpHost" style="width: 220px;" value="${ftpUserAccount.hostName}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Port :</b>
				</td>
				<td colspan="2">
					<input type="text" name="ftpPort" id="ftpPort" style="width: 220px;" value="${ftpUserAccount.portNumber}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Destination Sub Directory on Host :</b>
				</td>
				<td colspan="2">
					<input type="text" name="subDirectoryHost" id="subDirectoryHost" style="width: 220px;" value="${ftpUserAccount.destination}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>User Name :</b>
				</td>
				<td colspan="2">
					<input type="text" name="ftpUserName" id="ftpUserName" class="group" style="width: 220px;" value="${ftpUserAccount.userName}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Password :</b>
				</td>
				<td colspan="2">
					<input type="password" name="ftpPassword" id="ftpPassword" class="group" style="width: 220px;" value="${ftpUserAccount.password}">
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr> 
				<td colspan="2">
					<b>Upload Date :</b>
				</td>
				<td colspan="2">
				 	<input type="text" name="uploadDate" id="uploadDate" class="group" value="${ftpUserAccount.formatedDate}">
				 	
				 	<select name="uploadDateHrs" name="uploadDateHrs" id="uploadDateHrs" >
					 	<c:forEach var="hour" begin="0" end="23" step="1">
					 		<option value="${hour}" <c:if test="${ftpUserAccount.hour eq hour}">selected</c:if> >
					 			<fmt:formatNumber pattern="00" value="${hour}" />
					 		</option>
					 	</c:forEach>
				 	</select>
				 	<select name="uploadDateMins" name="uploadDateMins" id="uploadDateMins">
					 	<c:forEach var="minute" begin="0" end="59" step="1">
					 		<option value="${minute}" <c:if test="${ftpUserAccount.minute eq minute}">selected</c:if> >
					 			<fmt:formatNumber pattern="00" value="${minute}" />
					 		</option>
					 	</c:forEach>
				 	</select>
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Frequency :</b> 
				</td>
				<td colspan="2">
					<select name="uploadFrequency" name="uploadFrequency">
						<option value="10" <c:if test="${ftpUserAccount.frequency eq 10}">selected</c:if>  >10 Minutes</option>
					 	<option value="15" <c:if test="${ftpUserAccount.frequency eq 15}">selected</c:if>  >15 Minutes</option>
					 	<option value="30" <c:if test="${ftpUserAccount.frequency eq 30}">selected</c:if>  >30 Minutes</option>
					 	<option value="45" <c:if test="${ftpUserAccount.frequency eq 45}">selected</c:if>  >45 Minutes</option>
					 	<option value="60" <c:if test="${ftpUserAccount.frequency eq 60}">selected</c:if>  >1 Hour</option>
					 	<option value="120" <c:if test="${ftpUserAccount.frequency eq 120}">selected</c:if>  >2 Hour</option>
					 	<option value="180" <c:if test="${ftpUserAccount.frequency eq 180}">selected</c:if>  >3 Hour</option>
					 	<option value="240" <c:if test="${ftpUserAccount.frequency eq 240}">selected</c:if>  >4 Hour</option>
					 	<option value="300" <c:if test="${ftpUserAccount.frequency eq 300}">selected</c:if>  >5 Hour</option>
					 	<option value="360" <c:if test="${ftpUserAccount.frequency eq 360}">selected</c:if>  >6 Hour</option>
					 	<option value="420" <c:if test="${ftpUserAccount.frequency eq 420}">selected</c:if>  >7 Hour</option>
					 	<option value="480" <c:if test="${ftpUserAccount.frequency eq 480}">selected</c:if>  >8 Hour</option>
					 	<option value="540" <c:if test="${ftpUserAccount.frequency eq 540}">selected</c:if>  >9 Hour</option>
					 	<option value="600" <c:if test="${ftpUserAccount.frequency eq 600}">selected</c:if>  >10 Hour</option>
					 	<option value="660" <c:if test="${ftpUserAccount.frequency eq 660}">selected</c:if>  >11 Hour</option>
					 	<option value="720" <c:if test="${ftpUserAccount.frequency eq 720}">selected</c:if>  >12 Hour</option>
				 	</select>
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			
			
			<%-- <c:choose>
			
			<c:when test="${null ne ftpUserAccount.accountNo and ftpUserAccount.accountNo ne 0}">
			
				<tr id="changeFilePathRowID1"  style="display: none;">
					<td colspan="2">
						<b>File Path :</b>
						
					</td>
					
					<td colspan="2">
						<input type="text"  name="changeFilePath" id="changeFilePath" style="width: 220px;" value="${ftpUserAccount.filePath}" /> 
						<span style="color: green;">Example: &nbsp;&nbsp; <b>C:\\Files\\ </b></span>
					</td>
				</tr>
				<!-- <tr id="changeFilePathRowID2"  style="display: none;">
					<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr> -->
				
				<tr>
					<td colspan="2"></br>
						<b>Source :</b>
					</td>
					
					<td colspan="2">
					    <c:forEach var="userFileInfo" items="${ftpUserAccount.ftpUserFileInformation}">
					       <input name="sourceFile" type="text" class="sourceFile" style="width: 220px;" value="${userFileInfo.fileSource}" readonly="readonly" /></br> 
					    </c:forEach>
												
						<input type="file" name="changeFile" id="changeFile"  multiple style="display: none; " onchange="showFileNameChangeFile();" /> </br></br>
					Yes :<input type="checkbox" id="changeFileOption" name="changeFileOption" value="Y" 
						 onclick ="callChangeFile();" <c:if test="${ftpUserAccount.changeOption eq 'Y'}">checked</c:if>> Do you want to change the source?
						 
					</td>
				</tr>
				<tr>
				    <td colspan="2">
					&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				    <td colspan="2">
				      <div id="display1" style="color: green;"></div></br>
				   </td>
				</tr>
				
			</c:when>
			<c:otherwise>
				 --%>
				 <tr>
				     <td colspan="2">
				        <b>Selected Files :</b>
				     </td>
				    <td colspan="">
					&nbsp;&nbsp;&nbsp;&nbsp;
					</td>			  			    				   
				</tr>
				<c:forEach var="userFileInfo" items="${ftpUserAccount.ftpUserFileInformation}"> 
				<tr>
				    <td colspan="2">
					   &nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				    <td colspan="2">
						<label  id="filePath" style="width: 220px;"><b>${userFileInfo.fileSource}</b></label> 
						<input type="hidden" name="${userFileInfo.fileName}" value="${userFileInfo.fileName}" />
						<input type="hidden"  name="${userFileInfo.filePath}" value="${userFileInfo.filePath}" />
					<!-- 	<span style="color: green;">Example: &nbsp;&nbsp; <b>C:\\Files\\ </b></span> -->
					</td>
					<td colspan="2">
						<input type="button"  value="Remove"  class="medButton" onclick="removeFile('remove');">						
					</td>
					<td colspan="2">
						<div id="remove" title="Conferm Delete?">
                       </div>						
					</td>
							
				</tr>
				</c:forEach>
				
				<tr id="changeFilePathRowID1"  style="display: none;">
					<td colspan="2">
						<b>File Path :</b>
						
					</td>
					
					<td colspan="2">
						<input type="text"  name="changeFilePath" id="changeFilePath" style="width: 220px;"/> 
						<span style="color: green;">Example: &nbsp;&nbsp; <b>C:\\Files\\ </b></span>
					</td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr id="chooseFile"  style="display: none;">
					<td colspan="2">
						<b>Choose File :</b>
					</td>
					
					<td colspan="2">
						<input name="changeFile" type="file" id="changeFile"  multiple="multiple" onchange="showFileNameChangeFile();"/>						
					</td>
				</tr>
				<tr>
				    <td colspan="2">
					&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				    <td colspan="2">
				      <div id="display" style="color: green;"></div></br>
				   </td>
				</tr>
			<%-- </c:otherwise>			
			</c:choose> --%>
			
			
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			
			<tr>
						<td colspan="4">
							<input type="button" value="Update"  class="medButton" onclick="callUpdateMethod('update');">
							<input type="button" value="Delete"  class="medButton" onclick="callDeleteMethod('delete');">
							<input type="button" value="Clear"  class="medButton" onclick="callClearMethod('clear');">
						    <input type="button" value="Add Files"  class="medButton"  onclick="callChangeFile();">										
						</td>
					
			</tr>
		</table>			
	</form>
</div>

