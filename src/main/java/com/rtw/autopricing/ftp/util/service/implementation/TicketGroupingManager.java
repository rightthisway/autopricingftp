package com.rtw.autopricing.ftp.util.service.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
//import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

//import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.autopricing.ftp.data.EbayInventory;
import com.rtw.autopricing.ftp.data.Ticket;



public class TicketGroupingManager implements com.rtw.autopricing.ftp.util.service.TicketGroupingManager{
	private static final Logger LOG = LoggerFactory.getLogger(TicketGroupingManager.class);
	private static String categoryScheme = "DEFAULT";
	private static HashMap<Integer, String> catschemeMap = new HashMap<Integer, String>();
	private static Double fanAddition;
	private static final int REINIT_TIMER_INTERVAL = 30 * 60 * 1000;
	private static final int EXPOSOR = 2;
	private static final int RPT_FACTOR = 20;
	//private ZonesSetting zonesSetting;
	public TicketGroupingManager(){
		reinitProperties();
	}
	
	/*public Boolean initializeTicketGroupingForEvent(Integer eventId){
		Event event = DAORegistry.getEventDAO().get(eventId);
		LOG.info("in initializeTicketGroupingForEvent(), grouping requested for the event :"+eventId);
		if(event == null){
			return Boolean.FALSE;
		}
		Tour tour = event.getTour();
		if(tour == null){
			return Boolean.FALSE;
		}
		
		Collection<Site> sites = TMATDAORegistry.getSiteDAO().getAll();
		LOG.info("got the sites with size :"+sites.size());
		for(Site site : sites){
			TourPriceAdjustment tourPriceAdjustment = TMATDAORegistry.getTourPriceAdjustmentDAO().getPriceAdjustmentsForTourAndEvent(tour.getId(), site.getId());
			if(tourPriceAdjustment == null){
				tourPriceAdjustment = new TourPriceAdjustment();
				tourPriceAdjustment.setPercentAdjustment(-15.0D);
				tourPriceAdjustment.setSiteId(site.getId());
				tourPriceAdjustment.setTourId(tour.getId());
				TMATDAORegistry.getTourPriceAdjustmentDAO().saveOrUpdate(tourPriceAdjustment);
			}
			EventPriceAdjustment eventPriceAdjustment = TMATDAORegistry.getEventPriceAdjustmentDAO().get(eventId, site.getId());
			LOG.info("setting event price adjustment");
			if(eventPriceAdjustment == null){
				eventPriceAdjustment = new EventPriceAdjustment();
				eventPriceAdjustment.setEventId(eventId);
				eventPriceAdjustment.setPercentAdjustment(-15.0D);
				eventPriceAdjustment.setSiteId(site.getId());
				TMATDAORegistry.getEventPriceAdjustmentDAO().saveOrUpdate(eventPriceAdjustment);
			}
		}
		
		//Commented by Chirag - It will be managed by ZonesAdmin now.
		   
//		EbayInventoryTour ebayInventoryTour = ZonesDAORegistry.getEbayInventoryTourDAO().get(tour.getTmatTourId());
//		LOG.info("Saving EbayInventoryTour");
//		if(ebayInventoryTour == null){
//			ebayInventoryTour = new EbayInventoryTour(tour.getId(),"");
//			ZonesDAORegistry.getEbayInventoryTourDAO().saveOrUpdate(ebayInventoryTour);
//		}
//		LOG.info("Saving Ebay Inventory");
//		EbayInventoryEvent ebayInventoryEvent = ZonesDAORegistry.getEbayInventoryEventDAO().get(event.getTmatEventId());
//		if(ebayInventoryEvent == null){
//			ebayInventoryEvent = new EbayInventoryEvent(eventId, "system", new Date());
//			ZonesDAORegistry.getEbayInventoryEventDAO().saveOrUpdate(ebayInventoryEvent);
//		}
		
		return Boolean.TRUE;
	}*/
	/*public Collection<EbayInventoryGroup> groupTicketsForEvent(EbayInventoryEvent event,String sessionId,Collection<Ticket> nonAOTickets) {
		// TODO Auto-generated method stub
		try{
			Collection<EbayInventoryGroup> newGroups = computeEbayInventoryFromTickets(event,sessionId,nonAOTickets);
			return newGroups;
		}catch (Exception e) {
			System.out.println("1." + e.fillInStackTrace());
			return null;
		}
	}

	
	private Collection<EbayInventoryGroup> computeEbayInventoryFromTickets(EbayInventoryEvent event,String sessionId,Collection<Ticket> nonAOTickets) {
		Double netProfitFilter = 0.00;
		LOG.info("In computeEbayInventoryFromTickets() method, grouping for event :"+event.getEvent().getId()); 
		try {
//			netProfitFilter = Double.valueOf(ZonesDAORegistry.getPropertyDAO().get("ebay.filter.netProfit").getValue());
		
			return computeEbayInventoryFromTickets(event, netProfitFilter,sessionId,nonAOTickets);
		} catch (Exception e) {
			System.out.println("2." + e.fillInStackTrace());
//			e.printStackTrace();
			return null;
		}
		
	}
	
	private Collection<EbayInventoryGroup> computeEbayInventoryFromTickets (
			EbayInventoryEvent event, Double netProfitFilter,String sessionId,Collection<Ticket> nonAOTickets) {
		
		try{
			
			String catschemeforevent = event.getEvent().getVenueCategory().getCategoryGroup();
			

			Collection<EbayInventoryGroup> result = new ArrayList<EbayInventoryGroup>();
			Map<String,Collection<EbayInventoryGroup>> aoResult = new HashMap<String,Collection<EbayInventoryGroup>>();
			Map<String,Collection<EbayInventoryGroup>> nonAOResult = new HashMap<String,Collection<EbayInventoryGroup>>();
//			Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getEvent().getTour().getId(), catschemeforevent);	
//			Collection<Category> categoryList = DAORegistry.getCategoryDAO().getAllCategoryByEventId(event.getEvent().getId());
			Collection<Category> categoryList = null;
			Collection<CategoryMapping> categoryMappingList = null;
			VenueCategory venueCategory = null;
			if(event.getEvent().getVenueCategory()==null){
				venueCategory = event.getEvent().getVenueCategory();
			}else{
				venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getEvent().getVenueId(), catschemeforevent);
			}
			categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
			categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
			
			Map<String, Category> catSymbolMap=new HashMap<String, Category>();
			Map<Integer, Category> catMap=new HashMap<Integer, Category>();
			Map<Integer, List<CategoryMapping>> catMappingMap=new HashMap<Integer, List<CategoryMapping>>();
			for(Category cat:categoryList){
				catSymbolMap.put(cat.getSymbol().toUpperCase(), cat);
				catMap.put(cat.getId(), cat);
			}
			for(CategoryMapping mapping:categoryMappingList){
				List<CategoryMapping> list = catMappingMap.get(mapping.getCategoryId());
				if(list==null){
					list = new ArrayList<CategoryMapping>();
				}
				list.add(mapping);
				catMappingMap.put(mapping.getCategoryId(), list);
			}
			
			if(nonAOTickets!=null && !nonAOTickets.isEmpty()){
				Map<Integer,Collection<Ticket>> tixMap = getTicketQauntityMap(nonAOTickets);
				for(Integer key:tixMap.keySet()){
					Collection<Ticket> subset = tixMap.get(key);
					Collection<EbayInventoryGroup> groups = computeEbayInventoryFromTickets(subset, netProfitFilter, key, event,catSymbolMap,null,catschemeforevent,catMap,catMappingMap);
					if(groups!=null && !groups.isEmpty()){
						for(EbayInventoryGroup group : groups){
							String groupKey= key + "-" + group.getCategory().toUpperCase();
							Collection<EbayInventoryGroup> gps=nonAOResult.get(groupKey);
							if(gps==null){
								gps= new ArrayList<EbayInventoryGroup>(); 
							}
							gps.add(group);
							if(groupKey.equals("2-B1_CLUB")){
								System.out.println(gps.iterator().next().getEbayInventory1().getOnlinePrice());
							}
							nonAOResult.put(groupKey,gps);
								
						}
					}
				}
			}
			if(aoResult!=null && !aoResult.isEmpty()){
				int size=0;
				for(String key: aoResult.keySet() ){
					Collection<EbayInventoryGroup> aoGroups = aoResult.get(key);
					Collection<EbayInventoryGroup> nonAOGroups = nonAOResult.remove(key);
					//ZonesExposureProperty exposureProperty =  zonesExposureMap.get(event.getEvent().getId() + "-" + key.split("-")[1]);
					if(aoGroups!=null && !aoGroups.isEmpty() ){//&& !exposureProperty.getExposure().getShortDescription().contains("-OXP")
						if(nonAOGroups!=null && !nonAOGroups.isEmpty()){
							size=aoGroups.size()>nonAOGroups.size()?aoGroups.size():nonAOGroups.size();	
						}else{
							size=aoGroups.size();
						}
						for(EbayInventoryGroup aoGroup:aoGroups){
							size--;
							result.add(aoGroup);
						}
						if(size>0){
							Iterator<EbayInventoryGroup> iterator = nonAOGroups.iterator();
							for(int i=0;i<size;i++){
								if(iterator.hasNext()){
									result.add(iterator.next());
								}else{
									break;
								}
							}
						}
					}else{
						if(nonAOGroups!=null){
							result.addAll(nonAOGroups);
						}
					}
					
				}
				if(nonAOResult!=null && !nonAOResult.isEmpty()){
					for(String key: nonAOResult.keySet() ){
						Collection<EbayInventoryGroup> nonAOGroups = nonAOResult.get(key);
						for(EbayInventoryGroup nonAOGroup:nonAOGroups){
							result.add(nonAOGroup);
						}
						
					}
				}
				
			}else if(nonAOResult!=null && !nonAOResult.isEmpty()){
				for(Collection<EbayInventoryGroup> groups:nonAOResult.values()){
					result.addAll(groups);
				}
			}
			return result;
		}catch (Exception e) {
			System.out.println("3." +e.fillInStackTrace());
			return null;
		}
	}
	
	
	public Collection<EbayInventoryGroup> computeEbayInventoryFromTickets (
			Collection<Ticket> tickets, Double netProfitFilter, Integer quantity, EbayInventoryEvent ebayInventoryEvent,Map<String,Category> catSymbolMap,
			Map<String, Integer> lockedTicketsMap,String catSchemeToUse,Map<Integer,Category> catMap,Map<Integer,List<CategoryMapping>> catMappingMap) {
		try{
			if (tickets == null) {
				return new ArrayList<EbayInventoryGroup>();
			}
			Date startDate = new Date();

			// eventId-categoryId-remaining quantity => numOccurences
			Map<String, Integer> catNumTicketsMap = new HashMap<String, Integer>();

//			Collection<Ticket> tmpFilteredTickets = new ArrayList<Ticket>();
			Map<String, Set<Ticket>> cheapestTicketsMap = new HashMap<String, Set<Ticket>>();
			String catschemeforevent = catschemeMap.get(ebayInventoryEvent.getEvent().getId());
			LOG.info("Got cat scheme with for loop  :"+catschemeforevent);
			String catSchemeToUse = catschemeforevent;
			boolean flag=false;
			Event event =null;
			for (Ticket ticket : tickets) {
				if(event == null){
					event = ticket.getEvent();
				}
				if(ticket.getTicketType().equals(TicketType.AUCTION)) {
					continue;
				}
				if(!ticket.getSeller().equals("AOZones") && ticket.getCategoryId(event.getVenueCategory(),catSchemeToUse,catMap,catMappingMap) == null) {
					continue;
				}
				int soldQuantity=0;
				if(ticket.getSoldQuantity()!=null){
					soldQuantity=ticket.getSoldQuantity();
				}
				String key="";
				if(ticket.getSeller().equals("AOZones")){
					key = ticket.getEventId() + "-" + ticket.getSection().toUpperCase();
					flag=true;
				}else{
					Category cat = ticket.getCategory();
					if(cat==null){
						continue;
					}
					key = ticket.getEventId() + "-" + cat.getSymbol();
				}

				if(catNumTicketsMap.get(key) == null) {
					catNumTicketsMap.put(key, 1);
				}else {
					catNumTicketsMap.put(key, catNumTicketsMap.get(key) + 1);
				}
//				tmpFilteredTickets.add(ticket);
				Set<Ticket> cheapestTickets = cheapestTicketsMap.get(key);
				LOG.info("cheap ticket parsing");
				if (cheapestTickets == null) {
					cheapestTickets = new TreeSet<Ticket>(new Comparator<Ticket>() {
						public int compare(Ticket ticket1, Ticket ticket2) {
							int cmp = ticket1.getCurrentPrice().compareTo(
									ticket2.getCurrentPrice());
							if (cmp < 0) {
								return -1;
							}

							if (cmp > 0) {
								return 1;
							}
							
							// if same price get the one with less quantity first
							int cmp2 = ticket1.getRemainingQuantity().compareTo(
									ticket2.getRemainingQuantity());
							if (cmp2 < 0) {
								return -1;
							}

							if (cmp2 > 0) {
								return 1;
							}

							return ticket1.getId().compareTo(ticket2.getId());
						}	

					});
					cheapestTicketsMap.put(key, cheapestTickets);
				}
				cheapestTickets.add(ticket);
			}
//			System.out.println("All tickets are processed..");
			// second pass to remove groups with less than exposure level ticket entries
//			String catschemeforevent = catschemeMap.get(ebayInventoryEvent.getEvent().getId());
			//LOG.info("Got cat scheme tmpFilteredTickets  :"+catschemeforevent);
			if(!flag){
				String keys[]=catNumTicketsMap.keySet().toArray(new String[catNumTicketsMap.keySet().size()]);
				for(String key: keys){
					
//					String key = ticket.getEventId() + "-"	+ ticket.getCategoryId(catSchemeToUse);
					//ZonesExposureProperty exposureProperty = exposurePropertyMap.get(key);
					int numEntries = catNumTicketsMap.get(key);
					

				}

			}
//			System.out.println("All keys are processed..");
			List<EbayInventoryGroup> result = new ArrayList<EbayInventoryGroup>();
			for (String key : cheapestTicketsMap.keySet()) {
				Set<Ticket> ticketSet = cheapestTicketsMap.get(key);
//				Iterator<Ticket> iterator = ticketSet.iterator();
				Collection<EbayInventory> inventories = new ArrayList<EbayInventory>();
//				int count = 0;
				Ticket ticket = null;
				Category category = null;
				//ZonesExposureProperty exposureProperty = exposurePropertyMap.get(key);
				if(!flag){
					ticket = getSafeCheapestTicket(ticketSet,netProfitFilter,quantity,inventories);
					if(ticket == null){
						continue;
					}
					ticket.getCategoryId(event.getVenueCategory(),catSchemeToUse,catMap,catMappingMap);
					category = ticket.getCategory();
				}else{
					if(ticketSet.iterator().hasNext()){
						ticket= ticketSet.iterator().next();
						category = catSymbolMap.get(ticket.getSection());
						inventories.add(new EbayInventory(ticket, 1));
						ticket.setCategory(category);
					}else{
						continue;
					}
				}
				
				if(category != null){
					EbayInventoryGroup ebayInventoryGroup = new EbayInventoryGroup( ebayInventoryEvent, category.getGroupName(),quantity);
					ebayInventoryGroup.setEbayInventories(inventories);
					ebayInventoryGroup.computeInternalValues();
					result.add(ebayInventoryGroup);
				}
			}
			LOG.info("computeEbayInventoryFromTickets: "
					+ (new Date().getTime() - startDate.getTime() + " ms"));
			return result;
		}catch (Exception e) {
			System.out.println("4" + e.fillInStackTrace());
			return null;
		}
		
	}*/
	
	
	private Ticket getSafeCheapestTicket(Set<Ticket> sortedTickets,Double netProfitFilter, Integer quantity,
			Collection<EbayInventory> inventories){
	
		Iterator<Ticket> iterator = sortedTickets.iterator();
		Iterator<Ticket> currentTicketIterator = sortedTickets.iterator();
		Iterator<Ticket> nextTicketIterator = sortedTickets.iterator();
			
		if(!iterator.hasNext()){
			return null;
		}
		Ticket ticket1 = null;
		LOG.info("in calculatePricePt method");
	
		// if exposure is more than 1 then
		// check if the profit for the first backup (xp2)
		// is positive
		LOG.info("Added the inventories comparing with exposure");
		Ticket currentTicket=null;
		Ticket nextTicket=null;
		if(iterator.hasNext()){
			currentTicket= iterator.next();
			nextTicketIterator.next();
			currentTicketIterator.next();
			if(nextTicketIterator.hasNext()){
				nextTicket=nextTicketIterator.next();
				currentTicketIterator.next();
			}else{
				return null;
			}
			
		}else{
			return null;
		}
		int count=0;
		
		for(Ticket ticket=currentTicket;;ticket=currentTicketIterator.next()){
			try{
				Map<Integer, Ticket> map = new HashMap<Integer, Ticket>();
				count++;
				ticket1=ticket;
				boolean flag=false;
				if(1==EXPOSOR){
					if(ticket1!=null){
						inventories.add(new EbayInventory(ticket1, 1));
					}
					return ticket1;
				}else{
					for(int i=EXPOSOR ;i>1  && iterator != null;i--) {
						if(ticket.getCurrentPrice()*(1+EXPOSOR)>=(nextTicket.getCurrentPrice())){
							if(1 + EXPOSOR-i==1){
								map.put(1 + EXPOSOR-i, ticket);
							}
							map.put(2 + EXPOSOR-i, nextTicket);
							if(2<=EXPOSOR){
								break;
							}
							if(nextTicketIterator.hasNext() ){
								nextTicket=nextTicketIterator.next();
							}else{
								flag=true;
								break;
							}
						}else{
							if(iterator.hasNext()){
								ticket=nextTicket;
								nextTicket= iterator.next();
							}else{
								flag=true;
								break;
							}
						}
					}
					if(flag){
						if(currentTicketIterator.hasNext()){
							nextTicket=currentTicketIterator.next();
							int counter=0;
							nextTicketIterator = sortedTickets.iterator();
							while(count>counter){
								counter++;
								nextTicketIterator.next();
							}
							count=0;
						}else{
							return null;
						}
					}else{
						for(Integer key:map.keySet()){
							inventories.add(new EbayInventory(map.get(key), key));
						}
						break;
					}
				}
			}catch (Exception e) {
				System.out.println(quantity + ":" +  ":" +  e.fillInStackTrace());
			}
			if(!currentTicketIterator.hasNext()){
				break;
			}
		}
		return ticket1;
	}	
	

	public void afterPropertiesSet() throws Exception {
		reinitProperties();
	}
	
	public static void reinitProperties() {
		
		/*Timer timer = new Timer();
		timer.scheduleAtFixedRate(
			new TimerTask(){
				public void run(){
					try {
						Long startTime = System.currentTimeMillis();
						System.out.println("Start Time of reinit properties:"+startTime);
						Collection<Event> activeEvents = DAORegistry.getEventDAO().getAllActiveEvents();	
						
						categoryScheme =DAORegistry.getPropertyDAO().get("ebay.category.scheme").getValue();
						fanAddition = Double.valueOf(DAORegistry.getPropertyDAO().get("ebay.eimp.fan.addition").getValue());
						catschemeMap.clear();
						for(Event event: activeEvents){
							Collection<String> categories = DAORegistry.getCategoryDAO().getAllCategoryGroupsByEvent(event.getId());
							if(categories != null && categories.size() != 0){
								catschemeMap.put(event.getId(), categories.iterator().next());
							}
						}
						System.out.println("ENd time reinit properties=" + (System.currentTimeMillis() - startTime) / 1000);
					} catch (Exception e) {
						categoryScheme = "DEFAULT";
					}
				}
			},new Date(), REINIT_TIMER_INTERVAL);
		*/
	}

	
	
	public Map<Integer, Collection<Ticket>> getTicketQauntityMap(Collection<Ticket> tickets){
		Map<Integer,Collection<Ticket>> result = new HashMap<Integer, Collection<Ticket>>();
		for(Ticket ticket: tickets) {
			if(0<ticket.getRemainingQuantity()){
				for(Integer res:getPartition(ticket.getRemainingQuantity())){
					Collection<Ticket> tixs= result.get(res);
					if(tixs==null){
						tixs= new ArrayList<Ticket>();
					}
					tixs.add(ticket);
					result.put(res,tixs);
				}
			}
		}
		return result;
	}
	/**
	 * Tell if a quantity can be a partition of a bigger quantity.
	 * For instance a ticket of quantity 5 can be subdivided in 1, 2, 3, 5
	 * @param quantity
	 * @param totalQuantity
	 * @return
	 */
	public static Integer[] getPartition(int quantity) {
		
		if(quantity == 1){
			return new Integer[]{1};
		}else if(quantity == 2){
			return new Integer[]{2};
		}else if(quantity == 3) {
			return new Integer[]{1,3};
		}else if(quantity == 4) {
			return new Integer[]{2,4};
		}else if(quantity == 5) {
			return new Integer[]{1,2,3,5};
		}else if(quantity == 6) {
			return new Integer[]{1,2,4,6};
		}else if(quantity == 7) {
			return new Integer[]{1,2,3,4,5,7};
		}else if(quantity == 8) {
			return new Integer[]{1,2,3,4,5,6,8};
		}else if(quantity == 9) {
			return new Integer[]{1,2,3,4,5,6,7,9};
		}else if(quantity == 10) {
			return new Integer[]{1,2,3,4,5,6,7,8,10};
		}else if(quantity == 11) {
			return new Integer[]{1,2,3,4,5,6,7,8,9,11};
		}else{
			return new Integer[]{1,2,3,4,5,6,7,8,9,10,12};
		}
	}
	
}
