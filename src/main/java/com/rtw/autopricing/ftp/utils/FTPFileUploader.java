package com.rtw.autopricing.ftp.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.FtpUserAccount;
import com.rtw.autopricing.ftp.upload.FileUploaderUtil;

public class FTPFileUploader extends QuartzJobBean implements StatefulJob{

	public static final int DEFAULT_TIMEOUT = 30000;
	public static Date lastRunTime = new Date();

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		
		System.out.println("FTP UPLODER: Uploader Job Started....."+new Date());
		
		callFtpUploder();
		
		System.out.println("FTP UPLODER: Uploader Job Finished....."+new Date());
		
	}
	
	public void callFtpUploder(){
		
		List<FtpUserAccount> userAccounts = DAORegistry.getFtpUserAccountDAO().getAllActiveuserAccount();
		Date currentDate = new Date();
		//DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		for (FtpUserAccount ftpUserAccount : userAccounts) {
			
				System.out.println("FTP UPLODER: HOST"+ftpUserAccount.getHostName()+" ,PORT: "+ftpUserAccount.getPortNumber()+" ,DEST: "+ftpUserAccount.getDestination()+" ,USER NAME: "+ftpUserAccount.getUserName()+" ,PASSWORD: "+ftpUserAccount.getPassword());
				
				Date nextuploadDate =ftpUserAccount.getNextUploadTime() ;
				
				System.out.println("FTP UPLODER : LAST RUN TIME------>"+lastRunTime);
				System.out.println("FTP UPLODER : NEXT UPLOAD TIME------>"+nextuploadDate);
				System.out.println("FTP UPLODER : CURRENT TIME------>"+currentDate);
				
				boolean isToBeSchedule = istoBeScheduled(currentDate, nextuploadDate);
				
				if(isToBeSchedule){
					
					System.out.println("FTP UPLODER : FILES ARE READY TO UPLOAD - BEGINS"+new Date());
					
					try{
						FileUploaderUtil fileUploaderUtil = new FileUploaderUtil(ftpUserAccount.getHostName(), Integer.parseInt(ftpUserAccount.getPortNumber().trim()), 
								ftpUserAccount.getUserName(), ftpUserAccount.getPassword(),ftpUserAccount.getDestination(),ftpUserAccount.getFtpUserFileInformation());
						
						boolean isSucces = true;
						if(ftpUserAccount.getFtpType().equals(FTPType.SFTP)) {
							isSucces = fileUploaderUtil.uploadSFTPFile();
						} else { 
							isSucces = fileUploaderUtil.uploadFile();
						}
						System.out.println("FTP UPLODER : IS SUCCESS-> "+isSucces);
						System.out.println("FTP UPLODER :FILES ARE READY TO UPLOAD - ENDS : "+new Date());
						
						
					}catch(Exception e){
						e.printStackTrace();
					}
					ftpUserAccount.setLastUploadTime(new Date());
					Calendar calendar = new GregorianCalendar();
					calendar.setTime(nextuploadDate);
					calendar.add(Calendar.MINUTE, ftpUserAccount.getFrequency());
					ftpUserAccount.setNextUploadTime(calendar.getTime());
					DAORegistry.getFtpUserAccountDAO().saveOrUpdate(ftpUserAccount);
				}
		}
		lastRunTime = currentDate;
	}
	

	public boolean istoBeScheduled(Date currentDate,Date nextuploadDate){
		
		if(currentDate.compareTo(nextuploadDate) >=0 
				){//&& nextuploadDate.compareTo(lastRunTime) > 0
			return true;
		}
		return false;
	}

	
	public static void main(String[] args)  throws Exception{
		
		DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String nextuploadDateStr = "2015-10-08 09:59:00.0";
		String lastRunTimeStr = "2015-10-08 09:54:00.0";
		
		Date curentDate= new Date();
		Date nextuploadDate= dbDateTimeFormat.parse(nextuploadDateStr);
		lastRunTime = dbDateTimeFormat.parse(lastRunTimeStr);
		System.out.println("curentDate-->"+curentDate);
		System.out.println("nextuploadDate-->"+nextuploadDate);
		System.out.println("lastRunTime-->"+lastRunTime);
		
		 /*the value 0 if the argument Date is equal to this Date; a value less than 0 
		if this Date is before the Date argument; and a value greater than 0 
		if this Date is after the Date argument. */

		
		System.out.println("---------------------------------------");
		
		if(curentDate.compareTo(nextuploadDate) >=0 
				&& nextuploadDate.compareTo(lastRunTime) >= 0){
			
			System.out.println("Proceed for JOBS");
		}
		
		
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(nextuploadDate);
		calendar.add(Calendar.MINUTE, 15);
		
		System.out.println("Next Upload Time : "+calendar.getTime());
		
	}
	
	
	
}
