package com.rtw.autopricing.ftp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;


public class WebserviceUtil {
	public static String SEAT_GEEK_BASE_URL = "https://sellerdirect-api.seatgeek.com/rightthisway/";
	public static String SEAT_GEEK_APPLICATION_KEY = "application_key=<application_key>";
	public static String SEAT_GEEK_TOKEN = "m61Tv5IebugzqukzhkEu";
	
	
	public static String getObject(Map<String, String> dataMap,String url) throws Exception{
		try{
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(url);

			// add header
			//post.setHeader("x-sign", Constants.X_SIGN_HEADER);
			//post.setHeader("x-token", Constants.X_TOKEN_HEADER);
			//post.setHeader("Accept","application/json");
			dataMap.put("productType", "REWARDTHEFAN");
			dataMap.put("configId", "RewardTheFan");
			
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			
			for(Map.Entry<String, String> entry : dataMap.entrySet()){
				urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			
			post.setEntity(new UrlEncodedFormEntity(urlParameters));

			HttpResponse response = client.execute(post);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			return result;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String deleteTicket(Integer ticketId) throws Exception {
		try{
			
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			//System.out.println(url);
			String url="https://sellerdirect-api.seatgeek.com/rightthisway/listings?token=m61Tv5IebugzqukzhkEu&listing_id=14245083";
			HttpDelete delete = new HttpDelete(url);
			delete.addHeader("Content-Type", "application/json");
			System.out.println(url);
			HttpResponse response = httpClient.execute(delete);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			String message = null;
			//{"meta":{"status":200}}
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			}
			System.out.println("message : "+message);
			return result;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getOrdersBystatus() throws Exception {
		try{
			
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			String url="https://sellerdirect-api.seatgeek.com/rightthisway/orders?token=m61Tv5IebugzqukzhkEu&status=created&page_number=1&per_page=100";
			HttpGet get = new HttpGet(url);
			get.addHeader("Content-Type", "application/json");
			System.out.println(url);
			HttpResponse response = httpClient.execute(get);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			String message = null;
			//{"meta":{"status":200}}
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("meta")){
				message = jsonObject.getJSONArray("orders").toString();
			}
			System.out.println("message : "+message);
			return result;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String updatingOrderStatus(String orderId,String status) throws Exception {
		try{
			
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			
			String url="https://sellerdirect-api.seatgeek.com/rightthisway/order?token=m61Tv5IebugzqukzhkEu&status="+status+"&order_id=1";
			
			HttpPatch patch = new HttpPatch(url);
			//delete.addHeader("Accept", "application/vnd.ticketnetwork.posapi+json; version=1.0");
			patch.addHeader("Content-Type", "application/json");
			System.out.println(url);
			HttpResponse response = httpClient.execute(patch);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			
			String message = null;
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			}
			System.out.println(result+" : "+message);
			return result;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getOrderByOrderId(String orderId) throws Exception {
		try{
			
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			
			String url="https://sellerdirect-api.seatgeek.com/rightthisway/order?token=m61Tv5IebugzqukzhkEu&order_id=1";
			
			HttpGet get = new HttpGet(url);
			//delete.addHeader("Accept", "application/vnd.ticketnetwork.posapi+json; version=1.0");
			get.addHeader("Content-Type", "application/json");
			System.out.println(url);
			HttpResponse response = httpClient.execute(get);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			
			String message = null;
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			}
			System.out.println(result+" : "+message);
			return result;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String uploadTickets(String orderId) throws Exception {
		try{
			
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			
			String url="https://sellerdirect-api.seatgeek.com/rightthisway/order?token=m61Tv5IebugzqukzhkEu&order_id=1";
			
			HttpPut httpPut = new HttpPut(url);
			//delete.addHeader("Accept", "application/vnd.ticketnetwork.posapi+json; version=1.0");
			httpPut.addHeader("Host","sellerdirect-api.seatgeek.com");
			httpPut.addHeader("Content-Type", "application/x-www-form-urlencoded");
			
			File file = new File("C:\\Users\\tselvan.AO09\\Desktop\\10161_0.pdf");
			String pdfBase64Format = "";
			pdfBase64Format = new String(Base64.encodeBase64(FileUtils.readFileToByteArray((file))));
			
			String str = "{\"order_id\":\""+1+"\"," +
					"\"files\":[" +
					"{\"file\":\""+pdfBase64Format+"\"}" +
					"]" +
					"} ";
			StringEntity params =new StringEntity(str);
			//httpPut.addHeader("content-type", "application/json");
			httpPut.setEntity(params);

			System.out.println(url);
			System.out.println(str);
			HttpResponse response = httpClient.execute(httpPut);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			String message = null;
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			}
			System.out.println("message : "+message);
			return result;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) throws Exception {
		//deleteTicket(1);
		//updatingOrderStatus("1", "confirmed");
		//getOrderByOrderId("1");
		//getOrdersBystatus();
		uploadTickets("1");
	}
	
	public static String doBase64Encryption(String originalText){
	  byte[] encodedBytes = Base64.encodeBase64(originalText.getBytes());
	  String encryptedText = new String(encodedBytes);
	  return encryptedText;
	}
	
}
