package com.rtw.autopricing.ftp.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Error")
public class Error {
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
