package com.rtw.autopricing.ftp.utils.mail;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailManager {
	
	private final static String SMTP_HOST = "172.31.255.11";
	 private final static int SMTP_PORT = 25;
	 private final static String TRANSPORT_PROTOCOL = "smtp";
	 private final static String SMTP_SECURE_CONNECTION ="no";
	 private final static boolean SMTP_AUTH = false;
	 private final static String USERNAME = null; 
	 private final static String PASSWORD = null;
	 private final static String FROM_NAME = "TN POS API Call";
	 private final static String FROM_UPDATE_NAME = "";
	 private final static String EMAIL_FROM = "noreply@rightthisway.com";
	 private final static String RECIPIENTS = "kulaganathan@rightthisway.com,araut@rightthisway.com,cshah@righthisway.com";
	
	public static void main(String[] args) throws Exception {
		
		
		 FileOutputStream fop = null;
		 File file= new File("\\\\ADMIT1\\Shared\\MIS\\Developmentteam\\ulaganathan\\Not updated event List.csv");
		 
		 /*MailManager.sendMail(SMTP_HOST, SMTP_PORT, TRANSPORT_PROTOCOL,SMTP_SECURE_CONNECTION, SMTP_AUTH, USERNAME, PASSWORD, FROM_NAME, 
					EMAIL_FROM, RECIPIENTS, null, null, "Not Updated Events List in TN POS API Call.", "",file);*/
	}
	
	
	public static void  sendMail(String smtpHost, int smtpPort, String transportProtocol,
			  String smtpSecureConnection,
			  Boolean smtpAuth, String username, String password,
			  String fromName,
			  String emailFrom,
			  final String toAddress,
			  final String ccAddress,
			  final String bccAddress,
			  final String subject, final String body,String[] files) throws Exception {
			 
			 Properties props = new Properties();
			 props.put("mail.smtp.host", smtpHost);
			 props.put("mail.smtp.port", smtpPort);
			 props.put("mail.debug", "false");
			 props.put("mail.transport.protocol", "smtp");
			 
			 if (smtpSecureConnection.equalsIgnoreCase("tls")) {
			  props.put("mail.smtp.starttls.enable", "true");
			 }
			 
			 props.put("mail.smtp.connectiontimeout", 30000L);
			 props.put("mail.smtp.timeout", 30000L);
			 
			 props.put("mail.smtp.socketFactory.port", smtpPort);
			 if (!smtpSecureConnection.equalsIgnoreCase("no")) {
			  props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			 }
			 props.put("mail.smtp.socketFactory.fallback", "false");
			 props.put("mail.smtp.quitwait", "false");   
			   
			 Session session;
			 
			 if (smtpAuth) {
			  props.put("mail.smtp.auth", "true");
			  Authenticator auth = new MailAuthenticator(username, password);
			  session = Session.getInstance(props, auth);
			 } else {
			  props.put("mail.smtp.auth", "false");
			  Authenticator auth = new MailAuthenticator(username, null);
			  session = Session.getInstance(props, auth);
			 }

			 MimeMessage message = new MimeMessage(session);

			 String fullFromAddress = null;
			 if (fromName == null) {
			  fullFromAddress = emailFrom;   
			 } else {
			  fullFromAddress = "\"" + fromName + "\" <" + emailFrom + ">";   
			 }   

			 Address fromAddr = new InternetAddress(fullFromAddress);

			 message.setSubject(subject);
			 message.setFrom(fromAddr);
			 message.setReplyTo(new Address[] {new InternetAddress(fullFromAddress)});

			 if (toAddress != null) {
			  message.setRecipients(Message.RecipientType.TO, getAddresses(toAddress)); 
			 }
			 if (ccAddress != null) {
			  message.addRecipients(Message.RecipientType.CC, getAddresses(ccAddress)); 
			 }
			
			 if (bccAddress != null) {
			  message.addRecipients(Message.RecipientType.BCC, getAddresses(bccAddress));
			 }

			 message.setSubject(subject);
			 message.setText(body);
			 
			// Create the message body part
	         BodyPart messageBodyPart = new MimeBodyPart();
	 
	         // Fill the message
	         messageBodyPart.setText(body);
	         
	         Multipart multipart = new MimeMultipart("mixed");
	         multipart.addBodyPart(messageBodyPart);
	         
	         if(null != files && files.length >0){
	        	 for (String str : files) {
		             MimeBodyPart messageBodyPart1 = new MimeBodyPart();
		             DataSource source = new FileDataSource(str);
		             messageBodyPart1.setDataHandler(new DataHandler(source));
		             messageBodyPart1.setFileName(source.getName());
		             multipart.addBodyPart(messageBodyPart1);
		         }
	         }
	        
	         message.setContent(multipart);
			 message.setSentDate(new Date());
			 Transport transport = session.getTransport("smtp");
			 transport.connect(smtpHost, smtpPort, username, password);
			 transport.sendMessage(message, message.getAllRecipients());
			}

	
	private static Address[] getAddresses(String addr) throws Exception {
		addr = addr.trim();
		if (addr == null || addr.isEmpty()) {
			return null;
		}
		String[] recipients = addr.split(",");
		Address[] addrs = new Address[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			// replace admitone.com by admitone.net
			addrs[i] = new InternetAddress(recipients[i].toLowerCase().replace(
					"admitone.com", "admitone.net"));
		}
		return addrs;
	}
}
