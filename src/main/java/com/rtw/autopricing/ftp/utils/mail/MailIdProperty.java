package com.rtw.autopricing.ftp.utils.mail;



public class MailIdProperty {
	private String tmatmanagers;
	private String tnposMailerList;
	private String ecommerce;
	private String bccList;
	private String aoDev;
	private String posNoEventAlertMailFrom;
	private String posNoEventAlertMailTo;
	
	private String autopricingfolderpath;
	private String eventsNotinTMATCsvFolderPath;
	
	private String zoneticketsOrderFullFillMailTo;
	
	public String getAutopricingfolderpath() {
		return autopricingfolderpath;
	}
	public void setAutopricingfolderpath(String autopricingfolderpath) {
		this.autopricingfolderpath = autopricingfolderpath;
	}
	private String externalFileServerUrl;
	
	public String getBccList() {
		return bccList;
	}
	public void setBccList(String bccList) {
		this.bccList = bccList;
	}
	public String getTmatmanagers() {
		return tmatmanagers;
	}
	public void setTmatmanagers(String tmatmanagers) {
		this.tmatmanagers = tmatmanagers;
	}
	public String getTnposMailerList() {
		return tnposMailerList;
	}
	public void setTnposMailerList(String tnposMailerList) {
		this.tnposMailerList = tnposMailerList;
	}
	public String getEcommerce() {
		return ecommerce;
	}
	public void setEcommerce(String ecommerce) {
		this.ecommerce = ecommerce;
	}
	public String getAoDev() {
		return aoDev;
	}
	public void setAoDev(String aoDev) {
		this.aoDev = aoDev;
	}
	public String getExternalFileServerUrl() {
		return externalFileServerUrl;
	}
	public void setExternalFileServerUrl(String externalFileServerUrl) {
		this.externalFileServerUrl = externalFileServerUrl;
	}
	public String getPosNoEventAlertMailFrom() {
		return posNoEventAlertMailFrom;
	}
	public void setPosNoEventAlertMailFrom(String posNoEventAlertMailFrom) {
		this.posNoEventAlertMailFrom = posNoEventAlertMailFrom;
	}
	public String getPosNoEventAlertMailTo() {
		return posNoEventAlertMailTo;
	}
	public void setPosNoEventAlertMailTo(String posNoEventAlertMailTo) {
		this.posNoEventAlertMailTo = posNoEventAlertMailTo;
	}
	public String getEventsNotinTMATCsvFolderPath() {
		return eventsNotinTMATCsvFolderPath;
	}
	public void setEventsNotinTMATCsvFolderPath(String eventsNotinTMATCsvFolderPath) {
		this.eventsNotinTMATCsvFolderPath = eventsNotinTMATCsvFolderPath;
	}
	public String getZoneticketsOrderFullFillMailTo() {
		return zoneticketsOrderFullFillMailTo;
	}
	public void setZoneticketsOrderFullFillMailTo(
			String zoneticketsOrderFullFillMailTo) {
		this.zoneticketsOrderFullFillMailTo = zoneticketsOrderFullFillMailTo;
	}
	
}

