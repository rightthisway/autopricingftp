package com.rtw.autopricing.ftp.utils.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;

public class FTPMailGenerator {
	
	 private static Logger log = LoggerFactory.getLogger(FTPMailGenerator.class);
	 private final static String SMTP_HOST = DAORegistry.getPropertyDAO().get("smtp.host").getValue();
	 private final static int SMTP_PORT = Integer.parseInt(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
	 private final static String TRANSPORT_PROTOCOL = "smtp";
	 private final static String SMTP_SECURE_CONNECTION ="no";
	 private final static boolean SMTP_AUTH = Boolean.valueOf(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
	 private final static String USERNAME = DAORegistry.getPropertyDAO().get("smtp.username").getValue(); 
	 private final static String PASSWORD = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
	 private final static String FROM_NAME = "";
	 private final static String EMAIL_FROM = DAORegistry.getPropertyDAO().get("smtp.reply.from").getValue();
	 
	/* private final static String SMTP_HOST = "192.168.0.108";
	 private final static int SMTP_PORT = 25;
	 private final static String TRANSPORT_PROTOCOL = "smtp";
	 private final static String SMTP_SECURE_CONNECTION ="no";
	 private final static boolean SMTP_AUTH = false;
	 private final static String USERNAME = "no-reply@tg.com"; 
	 private final static String PASSWORD = "tickets#1441";
	 private final static String FROM_NAME = "";
	 private final static String EMAIL_FROM = "no-reply@tg.com";*/
	
	
	 
	 
	 public static void sendMail(String toAddress,String ccAddress,String bccAddress,String subject,String body) throws Exception{
			 
		    
		 try{
					 
			 MailManager.sendMail(SMTP_HOST, SMTP_PORT, TRANSPORT_PROTOCOL,SMTP_SECURE_CONNECTION, SMTP_AUTH, USERNAME, PASSWORD, FROM_NAME, 
						EMAIL_FROM, toAddress, ccAddress, bccAddress,
						subject, body,null);
				 
				 System.out.println("Mail sent sucessfully...");
				 log.info("MAIL SENT SUCESSFULLY........."); 
		 }catch (Exception e) {
			 e.printStackTrace();
			// TODO: handle exception
		}
			 
	 }
	 
	
}