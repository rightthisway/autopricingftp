package com.rtw.autopricing.ftp.utils;

import org.springframework.beans.factory.InitializingBean;


public class ZonesProperty implements InitializingBean {
	private String toaddressList;
	private String ccAddressList;
	
	public void afterPropertiesSet() throws Exception {
	}

	public String getToaddressList() {
		return toaddressList;
	}

	public void setToaddressList(String toaddressList) {
		this.toaddressList = toaddressList;
	}

	public String getCcAddressList() {
		return ccAddressList;
	}

	public void setCcAddressList(String ccAddressList) {
		this.ccAddressList = ccAddressList;
	}
	

	
}
