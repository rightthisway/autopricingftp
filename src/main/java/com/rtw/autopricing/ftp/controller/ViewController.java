package com.rtw.autopricing.ftp.controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.User;
import com.rtw.autopricing.util.BrokerStatus;
import com.rtw.autopricing.util.connection.Connections;


@Controller
@RequestMapping({"/Login","/","/MyAccount","/AutoCatStatus"})
public class ViewController {
	
	@RequestMapping("/Login")
	public String getLogin(HttpServletRequest request,Model model,HttpServletResponse response){
		return "page-login";
	}
	
	@RequestMapping("/")
	public String getHome(HttpServletRequest request,Model model,HttpServletResponse response){
		return "redirect:AutoPricing";
	}
	
	@RequestMapping("/AutoCatStatus")
	public ModelAndView loadAutoCatStatus(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("page-autocat-status");
		
		
		Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
		for(Broker broker:brokers){
			brokerMap.put(broker.getId(), broker);
		}
		
		Collection<AutopricingProduct> products = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductForMaangeScheduler();
		Map<Integer, AutopricingProduct> productMap = new HashMap<Integer, AutopricingProduct>();
		for(AutopricingProduct product:products){
			productMap.put(product.getId(), product);
		}
		
		Collection<BrokerStatus> brokerStatusList = DAORegistry.getQueryManagerDAO().getAllBrokersStatus();
		Map<String,BrokerStatus> brokerStatusMap = new HashMap<String, BrokerStatus>(); 
		mav.addObject("brokers", brokerStatusMap.values());
		for(BrokerStatus brokerStatus:brokerStatusList){
			BrokerStatus mappedBrokerStatus = brokerStatusMap.get(brokerStatus.getBrokerId() + ":" + brokerStatus.getProductId());
			if(mappedBrokerStatus==null){
				mappedBrokerStatus = brokerStatus;
				Broker broker = brokerMap.get(brokerStatus.getBrokerId());
				AutopricingProduct product = productMap.get(brokerStatus.getProductId());
				if(broker==null || product==null){
					continue;
				}
				mappedBrokerStatus.setBroker(broker);
				Connection connection = Connections.getInduxConnection(broker.getPosBrokerId());
				mappedBrokerStatus.setIsDBConnection(connection!=null && !connection.isClosed());
				mappedBrokerStatus.setProduct(product);
			}else{
				mappedBrokerStatus.setCount(brokerStatus.getCount() + mappedBrokerStatus.getCount());
			}
			brokerStatusMap.put(brokerStatus.getBrokerId() + ":" + brokerStatus.getProductId(), mappedBrokerStatus);
			
		}
		return mav;
	}		
	@RequestMapping("/MyAccount")
	public ModelAndView loadMyAccountPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	//	String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		ModelAndView mav = new ModelAndView("page-my-account");

		if (request.getParameter("updateInfo") != null) {
			user.setEmail(request.getParameter("email"));
			user.setFirstName(request.getParameter("firstName"));
			user.setLastName(request.getParameter("lastName"));
			user.setPhone(request.getParameter("phone"));
			DAORegistry.getUserDAO().update(user);
			mav.addObject("info", "User information updated.");
		} else if (request.getParameter("updatePassword") != null) {
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");
			if (!password.equals(password2)) {
				mav.addObject("error", "Passwords don't match.");
			} else {
				user.setAndEncryptPassword(password);			
				DAORegistry.getUserDAO().update(user);
				mav.addObject("info", "Password updated.");
			}
		} else if (request.getParameter("updateEmailSettings") != null) {
			Boolean emailCrawlErrorCreatorEnabled = "on".equals(request.getParameter("emailCrawlErrorCreatorEnabled"));
			Boolean emailCrawlErrorLastUpdaterEnabled = "on".equals(request.getParameter("emailCrawlErrorLastUpdaterEnabled"));
			user.setEmailCrawlErrorCreatorEnabled(emailCrawlErrorCreatorEnabled);
			user.setEmailCrawlErrorLastUpdaterEnabled(emailCrawlErrorLastUpdaterEnabled);
			DAORegistry.getUserDAO().update(user);
			mav.addObject("info", "Email settings updated.");
		}
		mav.addObject("user", user);
		return mav;
	}	
	

}
