package com.rtw.autopricing.ftp.controller;

//import javax.servlet.http.HttpServletResponse;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Artist;
import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEvent;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEventAudit;
import com.rtw.autopricing.ftp.data.AutoCats96TnCategoryTicket;
import com.rtw.autopricing.ftp.data.AutoCatsCategoryTicket;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingExchangeAudit;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.AutopricingSettingsAudit;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettingsAudit;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.BrokerAudit;
import com.rtw.autopricing.ftp.data.BrokerInduxConnection;
import com.rtw.autopricing.ftp.data.DateTimePop;
import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;
import com.rtw.autopricing.ftp.data.DefaultAutoPricingPropertiesAudit;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeEventZonesAudit;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZones;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZonesAudit;
import com.rtw.autopricing.ftp.data.ExcludeVenues;
import com.rtw.autopricing.ftp.data.ExcludeVenuesAudit;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.HistoricalTicket;
import com.rtw.autopricing.ftp.data.LarryLastCategoryTicket;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEventAudit;
import com.rtw.autopricing.ftp.data.LastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ManhattanZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ParentCategory;
import com.rtw.autopricing.ftp.data.PresaleAutoCatCategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEventAudit;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsCategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEventAudit;
import com.rtw.autopricing.ftp.data.SGLastFiveRowCategoryTicket;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEvent;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEventAudit;
import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.TixCityZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.data.Venue;
import com.rtw.autopricing.ftp.data.VenueCategory;
import com.rtw.autopricing.ftp.data.VipAutoCategoryTicket;
import com.rtw.autopricing.ftp.data.VipCatsCategoryTicket;
import com.rtw.autopricing.ftp.data.VipLastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZonedLastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.ZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.enums.EventStatus;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.LarryLastTnCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.VWLastRowMiniCatsCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.VWTGCatsCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.VWVipCatsCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.ZonePricingTnCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.ZonedLastRowTnCategoryTicket;
import com.rtw.autopricing.ftp.utils.mail.MailIdProperty;
import com.rtw.autopricing.util.AutoCats96Scheduler;
import com.rtw.autopricing.util.AutoExchangeEventLoader;
import com.rtw.autopricing.util.AutoPricingFileGenerator;
import com.rtw.autopricing.util.BrokerUtils;
import com.rtw.autopricing.util.CategoryTicketDownloader;
import com.rtw.autopricing.util.LarryLastScheduler;
import com.rtw.autopricing.util.LastRowMiniScheduler;
import com.rtw.autopricing.util.ManhattanZonePricingScheduler;
import com.rtw.autopricing.util.MiniScheduler;
import com.rtw.autopricing.util.PresaleAutocatScheduler;
import com.rtw.autopricing.util.PresaleZoneTicketsScheduler;
import com.rtw.autopricing.util.SGLastFiveRowScheduler;
import com.rtw.autopricing.util.TNDRealTixScheduler;
import com.rtw.autopricing.util.TNDZonesScheduler;
import com.rtw.autopricing.util.TicketNetworkSSAccountScheduler;
import com.rtw.autopricing.util.TixCityZonePricingScheduler;
import com.rtw.autopricing.util.VipLastRowMiniScheduler;
import com.rtw.autopricing.util.VipMiniScheduler;
import com.rtw.autopricing.util.ZoneLastRowMiniScheduler;
import com.rtw.autopricing.util.ZonePricingScheduler;
import com.rtw.autopricing.util.ZoneTicketsProcessosScheduler;
import com.rtw.autopricing.util.connection.Connections;



@Controller
@RequestMapping({"/DownloadManualCatVividCategoryTickets","/DownloadTnAutocatAbove96CategoryTickets","/AutopricingVenuesettings","/AutopricingVenuesettingsAudit","/ExcludeVenuesAudit","/ExcludeEventsBasedOnVenues","/AutoPricingSettings",
	"/AutocatProjectAudits","/DownloadAutocatProjectAudits","/PresaleAutoCatsCatPriceHistoryPopup","/ZoneTicketsTMATTicketPriceHistoryPopup","/ZonesPricingTMATTicketPriceHistoryPopup",
	"/ExcludeEventZones","/ExcludeEventZonesAudit","/ExcludeVenueCategoryZones","/ExcludeVenueCategoryZonesAudit",
	"/ZoneTicketsCatPriceHistoryPopup","/ZonesPricingCatPriceHistoryPopup","/ZonedLastrowMiniTMATTicketPriceHistoryPopup","/ZonedLastrowMiniCatPriceHistoryPopup","/VipAutoTMATTicketPriceHistoryPopup",
	"/VipAutoCategoryPriceHistoryPopup","/LarryLastTMATTicketPriceHistoryPopup","/LarryLastCategoryPriceHistoryPopup","/LastRowTMATTicketPriceHistoryPopup","/LastRowCategoryPriceHistoryPopup",
	"/VipTMATTicketPriceHistoryPopup","/VipCategoryPriceHistoryPopup","/AutoTMATTicketPriceHistoryPopup","/PresaleAutoCatsTMATTicketPriceHistoryPopup","/CategoryPriceHistoryPopup","/AutopricingTicketDetails",
	"/DownloadLastRowMiniscoreBigCategoryTickets","/DownloadVividTGCategoryTickets","/DownloadScoreBigTGCatsCategoryTickets","/DownloadZonesPricingTnCategoryTickets","/DownloadLarryLastTnCategoryTickets",
	"/DownloadZonedLastRowTnCategoryTickets","/DownloadLastRowMinivividCategoryTickets","/DownloadTNLastRowMiniCategoryTickets","/DownloadVipCatsCategoryTickets","/AutopricingEditLogFiles",
	"/DownloadTGCatsCategoryTickets","/EditAutoPricingExchangeMarkup","/AutoPricingExchangeMarkupAudit","/AutoPricingSettingsAudit","/DefaultAutopricingProperties","/DefaultAutoPricingPropertiesAudit",
	"/GlobalAutopricing","/GlobalAutoPricingAuditPopup","/TixCityZonesPricingTMATTicketPriceHistoryPopup","/TixCityZonesPricingCatPriceHistoryPopup","/DownloadTixCityZonesPricingTnCategoryTickets",
	"/SchedulerManagement","/TMATTicketPriceHistoryPopup","/ToggleProduct","/DownloadScoreBigPresaleAutoCatsCategoryTickets","/DownloadVividPresaleAutoCategoryTickets",
	"/DownloadPresaleAutoCatsCategoryTickets","/EditAutoPricingBroker","/ManageAutoPricingBroker","/ManualAutoPricingFileGeneration","/DownloadFanXchangeTGCatsCategoryTickets","/DownloadTicketcityTGCatsCategoryTickets",
	"/DownloadLastRowMiniFanXchangeCategoryTickets","/DownloadTicketcityVipCatsCategoryTickets","/DownloadLastRowMiniTicketcityCategoryTickets","/ManageBrokersConnections",
	"/DownloadZoneTicketsProcessorCategoryTickets","/AutoCats96TMATTicketPriceHistoryPopup","/AutoCats96CatPriceHistoryPopup","/DownloadManhattanZonesPricingTnCategoryTickets",
	"/DownloadTNVipLastRowMiniCategoryTickets","/ManhattanZonesPricingTMATTicketPriceHistoryPopup","/ManhattanZonesPricingCatPriceHistoryPopup","/VipLastRowCategoryPriceHistoryPopup","/VipLastRowTMATTicketPriceHistoryPopup",
	"/BrokerTicketsCountProductWisePopup","/PresaleZoneTicketsCatPriceHistoryPopup","/PresaleZoneTicketsTMATTicketPriceHistoryPopup","/SGLastFiveRowTMATTicketPriceHistoryPopup","/SGLastFiveRowCategoryPriceHistoryPopup",
	"/DownloadTnSgLastFiveRowTickets","/ResetBrokerConnection"})
public class AutopricingController {

	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	
	@RequestMapping(value="/ToggleProduct")
	public ModelAndView stopLarryLastScheduler(HttpServletRequest request,Model model,HttpServletResponse response){
		String isStoppedStr = request.getParameter("isStopped");
		String productId = request.getParameter("productId");
		Boolean isStopped = false;
		if(isStoppedStr!=null){
			isStopped = Boolean.parseBoolean(isStoppedStr);
		}
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().get(Integer.parseInt(productId));
		autopricingProduct.setStopped(isStopped);
		DAORegistry.getAutopricingProductDAO().update(autopricingProduct);
		if(autopricingProduct.getName().equalsIgnoreCase("SS Account")){
			LarryLastScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("minicats")){
			MiniScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("vip minicats")){
			VipMiniScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("LastRow MiniCats")){
			LastRowMiniScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("Zoned LastRow MiniCats")){
			ZoneLastRowMiniScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("ZonesPricing")){
			ZonePricingScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("RTWZonesPricing")){
			TixCityZonePricingScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("MHZonePricing")){
			ManhattanZonePricingScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("RewardTheFan Listings")){
			ZoneTicketsProcessosScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("PresaleZoneTicketsProcessor")){
			PresaleZoneTicketsScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("PresaleAutoCat")){
			PresaleAutocatScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("AutoCats96")){
			AutoCats96Scheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("SGLastFiveRow")){
			SGLastFiveRowScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("TNDRealTix")){
			TNDRealTixScheduler.setStopped(isStopped);
		}else if(autopricingProduct.getName().equalsIgnoreCase("TNDZones")){
			TNDZonesScheduler.setStopped(isStopped);
		}
		return new ModelAndView("redirect:SchedulerManagement");
	}
	
	@RequestMapping(value="/SchedulerManagement")
	public ModelAndView manageScheduler(HttpServletRequest request,Model model,HttpServletResponse response) throws Exception{
		ModelAndView mav = new ModelAndView("page-scheduler-management");
		Collection<AutopricingProduct> products =  DAORegistry.getAutopricingProductDAO().getAllAutopricingProductForMaangeScheduler();
		
		for(AutopricingProduct product:products){
			if(product.getName().equalsIgnoreCase("SS Account")){
				if(LarryLastScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(LarryLastScheduler.getNextRunTime());
			}
			else if(product.getName().equalsIgnoreCase("minicats")){
				if(MiniScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(MiniScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("vip minicats")){
				if(VipMiniScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(VipMiniScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("LastRow MiniCats")){
				if(LastRowMiniScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(LastRowMiniScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("Zoned LastRow MiniCats")){
				if(ZoneLastRowMiniScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(ZoneLastRowMiniScheduler.getNextRunTime());
			} else if(product.getName().equalsIgnoreCase("ZonesPricing")){
				if(ZonePricingScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(ZonePricingScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("RTWZonesPricing")){
				if(TixCityZonePricingScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(TixCityZonePricingScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("MHZonePricing")){
				if(ManhattanZonePricingScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(ManhattanZonePricingScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("RewardTheFan Listings")){
				if(ZoneTicketsProcessosScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(ZoneTicketsProcessosScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("PresaleZoneTicketsProcessor")){
				if(PresaleZoneTicketsScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(PresaleZoneTicketsScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("PresaleAutoCat")){
				if(PresaleAutocatScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(PresaleAutocatScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("AutoCats96")){
				if(AutoCats96Scheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(AutoCats96Scheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("TNSpecial")){
				if(TicketNetworkSSAccountScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(TicketNetworkSSAccountScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("VIPlastrowcats")){
				if(VipLastRowMiniScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(VipLastRowMiniScheduler.getNextRunTime());
			}else if(product.getName().equalsIgnoreCase("SGLastFiveRow")){
				if(SGLastFiveRowScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(SGLastFiveRowScheduler.getNextRunTime());
			} else if(product.getName().equalsIgnoreCase("TNDRealTix")){
				if(TNDRealTixScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(TNDRealTixScheduler.getNextRunTime());
			} else if(product.getName().equalsIgnoreCase("TNDZones")){
				if(TNDZonesScheduler.isRunning()){
					product.setStatus("Running");
				}else{
					product.setStatus("Pause");
				}
				product.setNextRunTime(TNDZonesScheduler.getNextRunTime());
			}
			
		}
		mav.addObject("products", products);
		return mav;
	}
	
	
	@RequestMapping(value="/ManageBrokersConnections")
	public ModelAndView manageBrokerConnections(HttpServletRequest request,Model model,HttpServletResponse response) throws Exception{
		ModelAndView mav = new ModelAndView("page-broker-connection");
		List<BrokerInduxConnection> brokerInduxConnections = new ArrayList<BrokerInduxConnection>();
		
		Map<Integer, Broker> brokersMap = BrokerUtils.getAllAutoPricingBrokers();
		BrokerInduxConnection brokerInduxConnection = null;
		Integer tixCount = 0;
		for (Integer brokerId : brokersMap.keySet()) {
			try{
			/*if(brokerId != 2 && brokerId != 3  && brokerId != 4  && brokerId != 5 && brokerId != 10){
				continue;
			}*/
			Broker broker =brokersMap.get(brokerId);
			tixCount = 0;
			brokerInduxConnection = new BrokerInduxConnection();
			brokerInduxConnection.setBrokerId(brokerId);
			brokerInduxConnection.setBrokerName(broker.getName());
			
			try{
				tixCount = InduxDAORegistry.getPosCategoryTicketGroupDAO().getAllActiveInduxTicketsCountinPOS(broker);
				brokerInduxConnection.setConnectionStatus("Success");
				brokerInduxConnection.setError("-");
			}catch(Exception e){
				e.printStackTrace();
				brokerInduxConnection.setConnectionStatus("Failure");
				brokerInduxConnection.setError(""+e.fillInStackTrace());
			}
			
			}catch(Exception e){
				e.printStackTrace();
				brokerInduxConnection.setConnectionStatus("Failure : NOT POS ISSUE");
				brokerInduxConnection.setError(""+e.fillInStackTrace());
			}
			brokerInduxConnection.setCatTixCount(tixCount);
			brokerInduxConnections.add(brokerInduxConnection);
		}
		mav.addObject("induxConnectionStatusList", brokerInduxConnections);
		return mav;
	}
	
	@RequestMapping(value="/AutoPricingSettings")
	public ModelAndView getAutopricingSettingDetails(HttpServletRequest request,Model model,HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("page-autopricing-settings");
		try {
			
			Collection<Broker> brokerList = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokersWithMZTixBroker();
			Collection<AutopricingProduct> autopricingProductList = DAORegistry.getAutopricingProductDAO().getAllActiveProducts();
			Collection<AutopricingExchange> autopricingExchangeList = DAORegistry.getAutopricingExchangeDAO().getAllActiveExchanges();
			
			String[] selectedBrokerIdStr = request.getParameterValues("brokerId");
			String[] selectedProductIdStr = request.getParameterValues("productId");
			String[] selectedExchangeIdStr = request.getParameterValues("exchangeId");
			String isAllBroker = request.getParameter("isAllBroker");
			String isAllProduct = request.getParameter("isAllProduct");
			String isAllExchange = request.getParameter("isAllExchange");
			
			List<Integer> selectedBrokerId = new ArrayList<Integer>();
			List<Integer> selectedProductId = new ArrayList<Integer>();
			List<Integer> selectedExchangeId = new ArrayList<Integer>();
			
			if (selectedBrokerIdStr != null && selectedBrokerIdStr.length != 0) {
				for (String brokerIdStr : selectedBrokerIdStr) {
					if (brokerIdStr.isEmpty()) {
						continue;
					}
					selectedBrokerId.add(Integer.parseInt(brokerIdStr));
				}
			}
			if (selectedProductIdStr != null && selectedProductIdStr.length != 0) {
				for (String productIdStr : selectedProductIdStr) {
					if (productIdStr.isEmpty()) {
						continue;
					}
					selectedProductId.add(Integer.parseInt(productIdStr));
				}
			}
			if (selectedExchangeIdStr != null && selectedExchangeIdStr.length != 0) {
				for (String exchangeIdStr : selectedExchangeIdStr) {
					if (exchangeIdStr.isEmpty()) {
						continue;
					}
					selectedExchangeId.add(Integer.parseInt(exchangeIdStr));
				}
			}
			
			String action = request.getParameter("action");
			String info = "";
			
			if(action != null) {
				if (action.equals("update")) {
					
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
			
					Collection<AutopricingSettings> autopriicngSettingsFromDB = DAORegistry.getAutopricingSettingsDAO().getAll();
					Map<String,AutopricingSettings> autopriicngSettingsMap = new HashMap<String, AutopricingSettings>();
					for (AutopricingSettings autopriicngSettings : autopriicngSettingsFromDB) {
						String key = autopriicngSettings.getBrokerId()+"_"+autopriicngSettings.getProductId()+"_"+autopriicngSettings.getExchangeId();
							autopriicngSettingsMap.put(key,autopriicngSettings);
					}
					
					List<AutopricingSettings> autopricingSettingsList = new ArrayList<AutopricingSettings>();
					List<AutopricingSettingsAudit> auditList = new ArrayList<AutopricingSettingsAudit>();
					
					List<Integer> updatedProductIds = new ArrayList<Integer>();
					Set<String> params = request.getParameterMap().keySet();
					for (String param : params) {
						if (param.contains("checkbox_")) {
							String temp[] = param.split("_");
							Integer brokerId = Integer.parseInt(temp[1].trim());
							Integer productId = Integer.parseInt(temp[2].trim());
							Integer exchangeId = Integer.parseInt(temp[3].trim());
							
							String key = brokerId+"_"+productId+"_"+exchangeId;
							
							String auditAction = "UPDATE";
							AutopricingSettings autopriicngSettings = autopriicngSettingsMap.get(key);
							if(autopriicngSettings == null) {
								autopriicngSettings = new AutopricingSettings();
								auditAction = "CREATE";
							} 
							
							String isEnabledStr = request.getParameter("isEnabled_" + key);
							String excludeEventDaysStr = request.getParameter("excludeEventDays_" + key);
							
							Boolean isEnabled = ((isEnabledStr == null || isEnabledStr.isEmpty()) ? false : true); 
							
							Integer excludeEventDays = 0;
							if(excludeEventDaysStr != null && !excludeEventDaysStr.isEmpty()) {
								excludeEventDays = Integer.parseInt(excludeEventDaysStr.trim());
							}
							
							if(!updatedProductIds.contains(productId)) {
								if(autopriicngSettings.getId() != null && ((isEnabled && !autopriicngSettings.getIsEnabled()) ||
										!(autopriicngSettings.getExcludeEventDays().equals(excludeEventDays)))) {
									AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(productId);
									product.setLastRunTime(null);
									
									if(product.getName().equalsIgnoreCase("Minicats")) {
										MiniScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("SS Account")) {
										LarryLastScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("Zoned LastRow MiniCats")) {
										ZoneLastRowMiniScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("VIP MiniCats")) {
										VipMiniScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("LastRow MiniCats")) {
										LastRowMiniScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("ZonesPricing")) {
										ZonePricingScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("RTWZonesPricing")) {
										TixCityZonePricingScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("MHZonePricing")) {
										ManhattanZonePricingScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("RewardTheFan Listings")) {
										ZoneTicketsProcessosScheduler.setLastUpdateTime(null);
									}else if(product.getName().equalsIgnoreCase("PresaleZoneTicketsProcessor")) {
										PresaleZoneTicketsScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("PresaleAutoCat")) {
										PresaleAutocatScheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("AutoCats96")){
										AutoCats96Scheduler.setLastUpdateTime(null);
									} else if(product.getName().equalsIgnoreCase("SGLastFiveRow")){
										SGLastFiveRowScheduler.setLastUpdateTime(null);
									}
									
									DAORegistry.getAutopricingProductDAO().update(product);
									updatedProductIds.add(productId);
								}
							}
							
							autopriicngSettings.setBrokerId(brokerId);
							autopriicngSettings.setProductId(productId);
							autopriicngSettings.setExchangeId(exchangeId);
							autopriicngSettings.setIsEnabled(isEnabled);
							autopriicngSettings.setExcludeEventDays(excludeEventDays);
							
							autopriicngSettings.setLastUpdated(now);
							autopriicngSettings.setLastUpdatedBy(username);
							
							autopricingSettingsList.add(autopriicngSettings);
							
							AutopricingSettingsAudit audit = new AutopricingSettingsAudit(autopriicngSettings);
							audit.setAction(auditAction);
							auditList.add(audit);
						}
					}
					DAORegistry.getAutopricingSettingsDAO().saveOrUpdateAll(autopricingSettingsList);
					
					if(!auditList.isEmpty()) {
						DAORegistry.getAutopricingSettingsAuditDAO().saveOrUpdateAll(auditList);
					}
					
					info = "Autopricing Settings Updated successfully.";
					action = "search";
				}
				
				if(action.equals("search")) {
					
					Collection<AutopricingSettings> autopriicngSettingsFromDB = DAORegistry.getAutopricingSettingsDAO().getAll();
					Map<String,AutopricingSettings> autopriicngSettingsMap = new HashMap<String, AutopricingSettings>();
					for (AutopricingSettings autopriicngSettings : autopriicngSettingsFromDB) {
						String key = autopriicngSettings.getBrokerId()+"_"+autopriicngSettings.getProductId()+"_"+autopriicngSettings.getExchangeId();
							autopriicngSettingsMap.put(key,autopriicngSettings);
					}
					
					List<AutopricingSettings> autopriicngSettingsList = new ArrayList<AutopricingSettings>();
					
					for (Broker broker : brokerList) {
						if(!selectedBrokerId.contains(broker.getId())) {
							continue;
						}
						for (AutopricingProduct autopricingProduct : autopricingProductList) {
							if(!selectedProductId.contains(autopricingProduct.getId())) {
								continue;
							}
							for (AutopricingExchange autopricingExchange : autopricingExchangeList) {
								if(!selectedExchangeId.contains(autopricingExchange.getId())) {
									continue;
								}
								
								String key = broker.getId()+"_"+autopricingProduct.getId()+"_"+autopricingExchange.getId();
								AutopricingSettings autopriicngSettings = autopriicngSettingsMap.get(key);
								
								if(autopriicngSettings == null) {
									autopriicngSettings = new AutopricingSettings();
									autopriicngSettings.setBrokerId(broker.getId());
									autopriicngSettings.setProductId(autopricingProduct.getId());
									autopriicngSettings.setExchangeId(autopricingExchange.getId());
								}
								
								autopriicngSettingsList.add(autopriicngSettings);
							}
						}
					}
					mav.addObject("autopriicngSettingsList", autopriicngSettingsList);
				}
			}
			
			
			mav.addObject("isAllBrokerSelected", isAllBroker);
			mav.addObject("isAllProductSelected", isAllProduct);
			mav.addObject("isAllExchangeSelected", isAllExchange);
			mav.addObject("selectedBrokerId", selectedBrokerIdStr);
			mav.addObject("selectedProductId", selectedProductIdStr);
			mav.addObject("selectedExchangeId", selectedExchangeIdStr);
			mav.addObject("brokerList", brokerList);
			mav.addObject("productList", autopricingProductList);
			mav.addObject("exchangeList", autopricingExchangeList);
			mav.addObject("info", info);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	@RequestMapping(value="/AutoPricingSettingsAudit")
	public ModelAndView loadAutopricingSettingsAudit(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView(
				"page-autopricing-settings-audit-popup");
		Integer brokerId = Integer.parseInt(request.getParameter("bId").trim());
		Integer productId = Integer.parseInt(request.getParameter("pId").trim());
		Integer exchangeId = Integer.parseInt(request.getParameter("eId").trim());
		
		List<AutopricingSettingsAudit> auditList = DAORegistry.getAutopricingSettingsAuditDAO().getAutopricingSettingsAuditList(brokerId, productId, exchangeId);
		AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(brokerId, productId, exchangeId);
		mav.addObject("auditList", auditList);
		mav.addObject("apSettings", autopricingSettings);
		
		return mav;
	}
	
	@RequestMapping(value="/DefaultAutopricingProperties")
	public ModelAndView getDefaultAutopricingProperties(HttpServletRequest request,Model model,HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("page-default-autopricing-properties");
		try {
			
			Collection<AutopricingProduct> autopricingProductList = DAORegistry.getAutopricingProductDAO().getAllActiveProducts();
			Collection<ParentCategory> parentCategoryList = DAORegistry.getParentCategoryDAO().getAll();
			Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			model.addAttribute("brokers", brokers);
			Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
			for(Broker broker:brokers){
				brokerMap.put(broker.getId(), broker);
			}
			
			String selectedProductIdStr = request.getParameter("productId");
			String[] selectedApplicableProductIdStr = request.getParameterValues("applicableProductId");
			String[] selectedParentCategoryIdStr = request.getParameterValues("parentCategoryId");
		
			Integer selectedProductId = null;
			if(selectedProductIdStr != null && !selectedProductIdStr.isEmpty()) {
				selectedProductId = Integer.parseInt(selectedProductIdStr.trim());
			}
			List<Integer> selectedApplicableProductId = new ArrayList<Integer>();
			if (selectedApplicableProductIdStr != null && selectedApplicableProductIdStr.length != 0) {
				for (String applicableProductIdStr : selectedApplicableProductIdStr) {
					if (applicableProductIdStr.isEmpty()) {
						continue;
					}
					selectedApplicableProductId.add(Integer.parseInt(applicableProductIdStr));
				}
			}
			List<Integer> selectedParentCategoryId = new ArrayList<Integer>();
			if (selectedParentCategoryIdStr != null && selectedParentCategoryIdStr.length != 0) {
				for (String parentCategoryIdStr : selectedParentCategoryIdStr) {
					if (parentCategoryIdStr.isEmpty()) {
						continue;
					}
					selectedParentCategoryId.add(Integer.parseInt(parentCategoryIdStr));
				}
			}
			
			String action = request.getParameter("action");
			String info = "";
			
			if(action != null) {
				if (action.equals("update")) {
					
					List<Integer> updateProductIds = new ArrayList<Integer>();
					updateProductIds.add(selectedProductId);
					updateProductIds.addAll(selectedApplicableProductId);
					
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
			
					Collection<DefaultAutoPricingProperties> defaultAutoPricingsFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getAll();
					Map<String,DefaultAutoPricingProperties> defaultAutoPricingsMap = new HashMap<String, DefaultAutoPricingProperties>();
					for (DefaultAutoPricingProperties defaultAutopriicngSettings : defaultAutoPricingsFromDB) {
						String key = defaultAutopriicngSettings.getProductId()+"_"+defaultAutopriicngSettings.getParentCategoryId();
						defaultAutoPricingsMap.put(key,defaultAutopriicngSettings);
					}
					
					List<DefaultAutoPricingProperties> defaultAutopricingSettingsList = new ArrayList<DefaultAutoPricingProperties>();
					
					List<DefaultAutoPricingPropertiesAudit> auditList = new ArrayList<DefaultAutoPricingPropertiesAudit>();
					Set<String> params = request.getParameterMap().keySet();
					for (String param : params) {
						if (param.contains("checkbox_")) {
							String temp[] = param.split("_");
							Integer productId = Integer.parseInt(temp[1].trim());
							Integer parentCategoryId = Integer.parseInt(temp[2].trim());
							
							String key = productId+"_"+parentCategoryId;
							
							String exposure = request.getParameter("exposure_" + key);
							String rptFactorStr = request.getParameter("rptFactor_" + key);
							String priceBreakupStr = request.getParameter("priceBreakup_" + key);
							String lowerMarkupStr = request.getParameter("lowerMarkup_" + key);
							String upperMarkupStr = request.getParameter("upperMarkup_" + key);
							String lowerShippingFeesStr = request.getParameter("lowerShippingFees_"+ key);
							String upperShippingFeesStr = request.getParameter("upperShippingFees_"+ key);
							String shippingMethodStr = request.getParameter("shippingMethod_" + key);
							String nearTermDisplayOptionStr = request.getParameter("nearTermDisplayOption_"+ key);
							String shippingDaysStr = request.getParameter("shippingDays_" + key);
							String sectionCountTicketStr = request.getParameter("sectionCountTicket_" + key);
							String tnBrokerIdStr = request.getParameter("ticketnetworkBroker_" + key);
							String vividBrokerIdStr = request.getParameter("vividseatBroker_" + key);
							String scorebigBrokerIdStr = request.getParameter("scorebigBroker_" + key);
							String fanxchangeBrokerIdStr = request.getParameter("fanxchangeBroker_" + key);
							String ticketcityBrokerIdStr = request.getParameter("ticketcityBroker_" + key);
							String seatgeekBrokerIdStr = request.getParameter("seatgeekBroker_" + key);
							String zoneTicketBrokerIdStr = request.getParameter("zoneTicketBroker_" + key);
							String taxPercentageStr = request.getParameter("taxPercentage_" + key);
							String excludeHoursBeforeEventStr = request.getParameter("excludeHoursBeforeEvent_" + key);
							
							String vividEnabledStr = request.getParameter("vividEnabled_"+key);
							boolean  vividEnabled = ((vividEnabledStr==null || vividEnabledStr== "" || !vividEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String rotEnabledStr = request.getParameter("rotEnabled_"+key);
							boolean  rotEnabled = ((rotEnabledStr==null || rotEnabledStr== "" || !rotEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String tixcityEnabledStr = request.getParameter("tixcityEnabled_"+key);
							boolean  tixcityEnabled = ((tixcityEnabledStr==null || tixcityEnabledStr== "" || !tixcityEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String rtwEnabledStr = request.getParameter("rtwEnabled_"+key);
							boolean  rtwEnabled = ((rtwEnabledStr==null || rtwEnabledStr== "" || !rtwEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String minicatsEnabledStr = request.getParameter("minicatsEnabled_"+key);
							boolean  minicatsEnabled = ((minicatsEnabledStr==null || minicatsEnabledStr== "" || !minicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String lastrowMinicatsEnabledStr = request.getParameter("lastrowMinicatsEnabled_"+key);
							boolean  lastrowMinicatsEnabled = ((lastrowMinicatsEnabledStr==null || lastrowMinicatsEnabledStr== "" || !lastrowMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String vipMinicatsEnabledStr = request.getParameter("vipMinicatsEnabled_"+key);
							boolean  vipMinicatsEnabled = ((vipMinicatsEnabledStr==null || vipMinicatsEnabledStr== "" || !vipMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String presaleAutocatsEnabledStr = request.getParameter("presaleAutocatsEnabled_"+key);
							boolean  presaleAutocatsEnabled = ((presaleAutocatsEnabledStr==null || presaleAutocatsEnabledStr== "" || !presaleAutocatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String vipLastrowMinicatsEnabledStr = request.getParameter("vipLastrowMinicatsEnabled_"+key);
							boolean  vipLastrowMinicatsEnabled = ((vipLastrowMinicatsEnabledStr==null || vipLastrowMinicatsEnabledStr== "" || !vipLastrowMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String zonedLastrowMinicatsEnabledStr = request.getParameter("zonedLastrowMinicatsEnabled_"+key);
							boolean  zonedLastrowMinicatsEnabled = ((zonedLastrowMinicatsEnabledStr==null || zonedLastrowMinicatsEnabledStr== "" || !zonedLastrowMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							
							String larryLastEnabledStr = request.getParameter("larryLastEnabled_"+key);
							boolean  larryLastEnabled = ((larryLastEnabledStr==null || larryLastEnabledStr== "" || !larryLastEnabledStr.equalsIgnoreCase("on") )?false:true);
							

							Integer shippingMethod = null, nearTermDisplayOption = null,shippingDays=null,sectionCountTicket = null;
							Double rptFactor = null, priceBreakup = null;
							Double lowerMarkup = null, upperMarkup = null, lowerShippingFees = null, upperShippingFees = null,taxPercentage = 0.0;
							Integer tnBrokerId = null,vividBrokerId = null,scorebigBrokerId = null,fanxchangeBrokerId = null,ticketcityBrokerId=null,seatgeekBrokerId=null,zoneTicketBrokerId = null;
							Integer excludeHoursBeforeEvent=null;
		
							if (rptFactorStr != null && !rptFactorStr.isEmpty()) {
								rptFactor = Double.parseDouble(rptFactorStr);
							}
							if (priceBreakupStr != null && !priceBreakupStr.isEmpty()) {
								priceBreakup = Double.parseDouble(priceBreakupStr);
							}
							if (lowerMarkupStr != null && !lowerMarkupStr.isEmpty()) {
								lowerMarkup = Double.parseDouble(lowerMarkupStr);
							}
							if (upperMarkupStr != null && !upperMarkupStr.isEmpty()) {
								upperMarkup = Double.parseDouble(upperMarkupStr);
							}
							if (lowerShippingFeesStr != null && !lowerShippingFeesStr.isEmpty()) {
								lowerShippingFees = Double.parseDouble(lowerShippingFeesStr);
							}
							if (upperShippingFeesStr != null && !upperShippingFeesStr.isEmpty()) {
								upperShippingFees = Double.parseDouble(upperShippingFeesStr);
							}
							if (taxPercentageStr != null && !taxPercentageStr.isEmpty()) {
								taxPercentage = Double.parseDouble(taxPercentageStr);
							}
							if (nearTermDisplayOptionStr != null && !nearTermDisplayOptionStr.isEmpty()) {
								nearTermDisplayOption = Integer.parseInt(nearTermDisplayOptionStr);
							}
							if (shippingMethodStr != null && !shippingMethodStr.isEmpty()) {
								shippingMethod = Integer.parseInt(shippingMethodStr);
							}
							if (shippingDaysStr != null && !shippingDaysStr.isEmpty()) {
								shippingDays = Integer.parseInt(shippingDaysStr);
							}
							if (sectionCountTicketStr != null && !sectionCountTicketStr.isEmpty()) {
								sectionCountTicket = Integer.parseInt(sectionCountTicketStr);
							}
							if (tnBrokerIdStr != null && !tnBrokerIdStr.isEmpty()) {
								tnBrokerId = Integer.parseInt(tnBrokerIdStr);
							}
							if (vividBrokerIdStr != null && !vividBrokerIdStr.isEmpty()) {
								vividBrokerId = Integer.parseInt(vividBrokerIdStr);
							}
							if (scorebigBrokerIdStr != null && !scorebigBrokerIdStr.isEmpty()) {
								scorebigBrokerId = Integer.parseInt(scorebigBrokerIdStr);
							}
							if (fanxchangeBrokerIdStr != null && !fanxchangeBrokerIdStr.isEmpty()) {
								fanxchangeBrokerId = Integer.parseInt(fanxchangeBrokerIdStr);
							}
							if (ticketcityBrokerIdStr != null && !ticketcityBrokerIdStr.isEmpty()) {
								ticketcityBrokerId = Integer.parseInt(ticketcityBrokerIdStr);
							}
							if (seatgeekBrokerIdStr != null && !seatgeekBrokerIdStr.isEmpty()) {
								seatgeekBrokerId = Integer.parseInt(seatgeekBrokerIdStr);
							}
							
							if (zoneTicketBrokerIdStr != null && !zoneTicketBrokerIdStr.isEmpty()) {
								zoneTicketBrokerId = Integer.parseInt(zoneTicketBrokerIdStr);
							}
							
							if (selectedProductId.equals(9)) {
								excludeHoursBeforeEvent = Integer.parseInt(excludeHoursBeforeEventStr);
							}
							
							//Iterating all update product id's
							for (Integer applicalbeProductId : updateProductIds) {
								String tempKey = applicalbeProductId+"_"+parentCategoryId;
								
								String auditAction = "UPDATE";
								DefaultAutoPricingProperties defaultAutoPricing = defaultAutoPricingsMap.get(tempKey);
								if(defaultAutoPricing == null) {
									defaultAutoPricing = new DefaultAutoPricingProperties();
									auditAction = "CREATE";
								} 
								defaultAutoPricing.setProductId(applicalbeProductId);
								defaultAutoPricing.setParentCategoryId(parentCategoryId);
								defaultAutoPricing.setExposure(exposure);
								defaultAutoPricing.setRptFactor(rptFactor);
								defaultAutoPricing.setPriceBreakup(priceBreakup);
								defaultAutoPricing.setLowerMarkup(lowerMarkup);
								defaultAutoPricing.setUpperMarkup(upperMarkup);
								defaultAutoPricing.setLowerShippingFees(lowerShippingFees);
								defaultAutoPricing.setUpperShippingFees(upperShippingFees);
								defaultAutoPricing.setShippingMethod(shippingMethod);
								defaultAutoPricing.setNearTermDisplayOption(nearTermDisplayOption);
								defaultAutoPricing.setShippingDays(shippingDays);
								defaultAutoPricing.setSectionCountTicket(sectionCountTicket);
								defaultAutoPricing.setTicketNetworkBrokerId(tnBrokerId);
								defaultAutoPricing.setVividBrokerId(vividBrokerId);
								defaultAutoPricing.setScoreBigBrokerId(scorebigBrokerId);
								defaultAutoPricing.setFanxchangeBrokerId(fanxchangeBrokerId);
								defaultAutoPricing.setTicketcityBrokerId(ticketcityBrokerId);
								defaultAutoPricing.setSeatGeekBrokerId(seatgeekBrokerId);
								defaultAutoPricing.setZoneTicketBrokerId(zoneTicketBrokerId);
								defaultAutoPricing.setVividSeatEnabled(vividEnabled);
								defaultAutoPricing.setRotEnabled(rotEnabled);
								defaultAutoPricing.setTixcityEnabled(tixcityEnabled);
								defaultAutoPricing.setRtwEnabled(rtwEnabled);
								defaultAutoPricing.setMinicatsEnabled(minicatsEnabled);
								defaultAutoPricing.setLastrowMinicatsEnabled(lastrowMinicatsEnabled);
								defaultAutoPricing.setVipMinicatsEnabled(vipMinicatsEnabled);
								defaultAutoPricing.setPresaleAutocatsEnabled(presaleAutocatsEnabled);
								defaultAutoPricing.setTaxPercentage(taxPercentage);
								defaultAutoPricing.setVipLastRowMinicatsEnabled(vipLastrowMinicatsEnabled);
								defaultAutoPricing.setZonedLastrowMinicatsEnabled(zonedLastrowMinicatsEnabled);
								defaultAutoPricing.setLarryLastEnabled(larryLastEnabled);
								
								if (applicalbeProductId.equals(9)) {
									defaultAutoPricing.setExcludeHoursBeforeEvent(excludeHoursBeforeEvent);
								}
								
								defaultAutoPricing.setLastUpdated(now);
								defaultAutoPricing.setLastUpdatedBy(username);
								
								defaultAutopricingSettingsList.add(defaultAutoPricing);
								
								DefaultAutoPricingPropertiesAudit audit = new DefaultAutoPricingPropertiesAudit(defaultAutoPricing);
								audit.setAction(auditAction);
								auditList.add(audit);
							}
						}
					}
					
					DAORegistry.getDefaultAutoPricingPropertiesDAO().saveOrUpdateAll(defaultAutopricingSettingsList);
					
					if(!auditList.isEmpty()) {
						DAORegistry.getDefaultAutoPricingPropertiesAuditDAO().saveOrUpdateAll(auditList);
					}
					
					info = "Default Autopricing Properties Updated successfully.";
					action = "search";
				}
				
				if(action.equals("search")) {
					
					Collection<DefaultAutoPricingProperties> defaultAutopricingPropertiesFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getAll();
					Map<String,DefaultAutoPricingProperties> autopriicngSettingsMap = new HashMap<String, DefaultAutoPricingProperties>();
					for (DefaultAutoPricingProperties defaultAutopricing : defaultAutopricingPropertiesFromDB) {
						String key = defaultAutopricing.getProductId()+"_"+defaultAutopricing.getParentCategoryId();
							autopriicngSettingsMap.put(key,defaultAutopricing);
					}
					
					List<DefaultAutoPricingProperties> defaultAutopricingList = new ArrayList<DefaultAutoPricingProperties>();
					
					if(selectedProductId != null) {
						for (ParentCategory parentCategory : parentCategoryList) {
							if(!selectedParentCategoryId.contains(parentCategory.getId())) {
								continue;
							}
							String key = selectedProductId+"_"+parentCategory.getId();
							DefaultAutoPricingProperties defaultAutopricingProperties = autopriicngSettingsMap.get(key);
							
							if(defaultAutopricingProperties == null) {
								defaultAutopricingProperties = new DefaultAutoPricingProperties();
								defaultAutopricingProperties.setProductId(selectedProductId);
								defaultAutopricingProperties.setParentCategoryId(parentCategory.getId());
							}
							
							defaultAutopricingList.add(defaultAutopricingProperties);
						}	
					}
					mav.addObject("apDefaultPropertyList", defaultAutopricingList);
				}
			}
			
			mav.addObject("selectedProductId", selectedProductId);
			mav.addObject("selectedApplicableProductId", selectedApplicableProductIdStr);
			mav.addObject("selectedParentCategoryId", selectedParentCategoryIdStr);
			mav.addObject("productList", autopricingProductList);
			mav.addObject("parentCategoryList", parentCategoryList);
			mav.addObject("info", info);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping(value="/DefaultAutoPricingPropertiesAudit")
	public ModelAndView loadDefaultAutopricingPropertiesAudit(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView(
				"page-default-autopricing-properties-audit-popup");
		Integer productId = Integer.parseInt(request.getParameter("pId").trim());
		Integer parentCategoryId = Integer.parseInt(request.getParameter("pcId").trim());
		
		List<DefaultAutoPricingPropertiesAudit> auditList = DAORegistry.getDefaultAutoPricingPropertiesAuditDAO().getDefaultAutoPricingPropertiesAudit(productId, parentCategoryId);
		DefaultAutoPricingProperties defaultAutopricingProperties = DAORegistry.getDefaultAutoPricingPropertiesDAO().getDefaultAutoPricingProperties(productId, parentCategoryId);
		mav.addObject("auditList", auditList);
		mav.addObject("defaultApProperties", defaultAutopricingProperties);
		
		return mav;
	}
	
	@RequestMapping(value="/GlobalAutopricing")
	public ModelAndView loadGlobalAutoPricing(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String sportinfo = "";
		ModelAndView mav = new ModelAndView("page-global-auto-pricing");
		Collection<AutopricingProduct> autopricingProductList = DAORegistry.getAutopricingProductDAO().getAllActiveProducts();
		Collection<ParentCategory> tourCategory= DAORegistry.getParentCategoryDAO().getAllTourCategoryOrderByName();
		//Collection<TourCategory> tourCategory= DAORegistry.getTourCategoryDAO().getAllTourCategoryOrderByName();
		
		String[] selectedProductIdStr = request.getParameterValues("productId");
	
		if ("updateExposure".equalsIgnoreCase(request.getParameter("action"))) {
		    String exposure = request
					.getParameter("exposure");
			
			String shippingMethodStr = request.getParameter("shipping_method");
			
			String allowSectionRange=request.getParameter("allowSectionRange_ticketnetwork");
			String nearTermDisplayOptionStr = request
					.getParameter("near_term_display_option");
			String RptTicketnetwork = request
					.getParameter("rpt_ticketnetwork");
			String priceBreakupTicketnetwork = request
					.getParameter("price_breakup_ticketnetwork");
			String upperMarkpuTicketnetwork = request
					.getParameter("upper_markup_ticketnetwork");
			String lowerMarkpuTicketnetwork = request
					.getParameter("lower_markup_ticketnetwork");
			String upperShippingfeesTicketnetwork = request
					.getParameter("upper_shippingFees_ticketnetwork");
			String lowerShippingfeesTicketnetwork = request
					.getParameter("lower_shippingFees_ticketnetwork");
            String shippingDays=request.getParameter("shipping_days_ticketnetwork");
			String[] eventTypeArr = request.getParameterValues("event_type");
			String eventType = "", eventTypeParam = "";
            String products="";
			
			if (null != eventTypeArr && eventTypeArr.length > 0) {
				for (String string : eventTypeArr) {
					eventType = eventType + "," + string;
					eventTypeParam = eventTypeParam + ",'" + string + "'";
				}
				eventType = eventType.substring(1);
				eventTypeParam = eventTypeParam.substring(1);
			}

			try {
				String userName = SecurityContextHolder.getContext()
						.getAuthentication().getName();
				GlobalAutoPricingAudit audit = new GlobalAutoPricingAudit();
				audit.setCreatedDate(new Date());
				audit.setUserName(userName);
				audit.setAction("Global Update");
				audit.setEventTypes(eventType);

				Boolean  isMiniUpdate = false, isVipMiniUpdate = false, isVipAutoUpdate = false, isLastRowMiniUpdate = false, 
				isLastFiveRowMiniUpdate = false,isPresaleAutocatUpdate=false,isLarryLastUpdate=false,isZonesPricingUpdate=false,isTixCityZonesPricingUpdate=false,
						isManhattanZonesPricingUpdate=false,isZonedLastRowMiniUpdate=false,isZoneTicketsProcessorUpdate=false,isAutoCats96Update=false,isTNSpecialUpdate=false,
						isVipLastRowMiniUpdate = false,isPresaleZoneTicketsUpdate=false,isSGLastFiveRowUpdate=false;

				for (String string : selectedProductIdStr) {
					if(string.equals("3")){
						isMiniUpdate=true;
						products+="Minicats ";
					} else if(string.equals("4")){
						isVipMiniUpdate=true;
						products+="VipMinicats ";
					} else if(string.equals("5")){
						isLastRowMiniUpdate = true;
						products+="LastRowMinicats ";
					} else if(string.equals("6")){
						isLarryLastUpdate=true;
						products+="SS Account ";
					} else if(string.equals("7")){
						isZonedLastRowMiniUpdate=true;
						products+="ZonedLastRowMiniCats ";
					} else if(string.equals("8")){
						isZonesPricingUpdate=true;
						products+="ZonesPricing ";
					} else if(string.equals("9")){
						isZoneTicketsProcessorUpdate=true;
						products+="RewardTheFan Listings ";
					} else if(string.equals("11")){
                        isPresaleAutocatUpdate=true;
						products+="PresaleAutocat ";
					} else if(string.equals("10")){
						isAutoCats96Update=true;
						products+="AutoCats96 ";
					} else if(string.equals("13")){
						isTNSpecialUpdate=true;
						products+="TNSpecial ";
					} else if(string.equals("14")){
						isTixCityZonesPricingUpdate=true;
						products+="RTWZonesPricing ";
					}else if(string.equals("15")){
						isManhattanZonesPricingUpdate=true;
						products+="MHZonePricing ";
					}else if(string.equals("16")){
						isVipLastRowMiniUpdate=true;
						products+="VIPlastrowcats ";
					}else if(string.equals("17")){
						isPresaleZoneTicketsUpdate=true;
						products+="PresaleZoneTicketsProcessor ";
					} else if(string.equals("19")){
						isSGLastFiveRowUpdate=true;
						products+="SGLastFiveRow ";
					} 
				}
				if(products!=null){
				products=products.replaceAll(" ", ",");
				products=products.substring(0, products.length()-1);
				audit.setProducts(products);
				}
				
				if (exposure!= null) {
					audit.setExposure(exposure);		
				}
				
				if(allowSectionRange!=null){					
                  if(allowSectionRange.equals("on")){
                	audit.setAllowSectionRange(true);
                  }else{
                	audit.setAllowSectionRange(false);
                 }
				}
				if (shippingMethodStr != null
						&& shippingMethodStr.trim().length() > 0) {
					audit.setShippingMethod(Integer.valueOf(shippingMethodStr));
					
				}

				if (nearTermDisplayOptionStr != null
						&& nearTermDisplayOptionStr.trim().length() > 0) {
					audit.setNearTermDisplayOption(Integer
							.valueOf(nearTermDisplayOptionStr));
					
				}

				if (RptTicketnetwork != null) {
					audit.setRptFactor(Double.parseDouble(RptTicketnetwork));
										
				}

				if (priceBreakupTicketnetwork != null) {
					audit.setPriceBreakup(Double
							.parseDouble(priceBreakupTicketnetwork));
						
				}

				if (upperMarkpuTicketnetwork != null) {
					audit.setUpperMarkup(Double
							.parseDouble(upperMarkpuTicketnetwork));
				
				}

				if (lowerMarkpuTicketnetwork != null) {
					audit.setLowerMarkup(Double
							.parseDouble(lowerMarkpuTicketnetwork));
					
				}

				if (upperShippingfeesTicketnetwork != null) {
					audit.setUpperShippingFees(Double
							.parseDouble(upperShippingfeesTicketnetwork));

					
				}

				if (lowerShippingfeesTicketnetwork != null) {
					audit.setLowerShippingFees(Double
							.parseDouble(lowerShippingfeesTicketnetwork));
				
				}
               if(shippingDays!=null){
            	   audit.setShippingDays(Integer.parseInt(shippingDays));
               }
				
				if (isMiniUpdate) {
					DAORegistry.getMiniExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
					
				}

				
				if (isVipMiniUpdate) {
					DAORegistry.getVipMiniExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				if (isLastRowMiniUpdate) {
					DAORegistry.getLastRowMiniExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				/*if (isLastFiveRowMiniUpdate) {
					DAORegistry.getLastFiveRowMCStubhubExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}*/
				
				if (isLarryLastUpdate) {
					DAORegistry.getLarryLastExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
			
				if (isZonesPricingUpdate) {
					DAORegistry.getZonePricingExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isTixCityZonesPricingUpdate) {
					DAORegistry.getTixCityZonePricingExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isManhattanZonesPricingUpdate) {
					DAORegistry.getManhattanZonePricingExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isZonedLastRowMiniUpdate) {
					DAORegistry.getZoneLastRowMiniExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}

				if (isZoneTicketsProcessorUpdate) {
					DAORegistry.getZoneTicketsProcessorExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isPresaleZoneTicketsUpdate) {
					DAORegistry.getPresaleZoneTicketsExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				if (isPresaleAutocatUpdate) {
					DAORegistry.getPresaleAutoCatExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				if (isAutoCats96Update) {
					DAORegistry.getAutoCats96ExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				if (isTNSpecialUpdate) {
					DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				if (isVipLastRowMiniUpdate) {
					DAORegistry.getVipLastRowMiniExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				if (isSGLastFiveRowUpdate) {
					DAORegistry.getSgLastFiveRowExchangeEventDAO().updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				if (isMiniUpdate || isVipMiniUpdate
						|| isVipAutoUpdate || isLastRowMiniUpdate
						|| isLastFiveRowMiniUpdate || isLarryLastUpdate || isZonesPricingUpdate || isPresaleAutocatUpdate || isZonedLastRowMiniUpdate
						|| isZoneTicketsProcessorUpdate || isAutoCats96Update || isTNSpecialUpdate || isTixCityZonesPricingUpdate
						|| isManhattanZonesPricingUpdate || isVipLastRowMiniUpdate || isSGLastFiveRowUpdate) {
					DAORegistry.getGlobalAutoPricingAuditDAO().saveOrUpdate(audit);

					sportinfo = "Updated successfully";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mav.addObject("sportinfo", sportinfo);
		mav.addObject("selectedProductId", selectedProductIdStr);
		mav.addObject("productList", autopricingProductList);
		mav.addObject("tourCategory",tourCategory);
		return mav;
	}
	@RequestMapping(value="/GlobalAutoPricingAuditPopup")
	public ModelAndView loadGlobalAutoPricingAudit(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-global-auto-pricing-audit-popup");

		try {
			List<GlobalAutoPricingAudit> list = DAORegistry
					.getGlobalAutoPricingAuditDAO()
					.getAllAuditsOrderByCreatedDate();
			mav.addObject("auditList", list);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	@RequestMapping("/EditAutoPricingExchangeMarkup")	
	public ModelAndView loadExchangeMarkup(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String sportinfo = "";
		ModelAndView mav = new ModelAndView("page-autopricing-exchange-markup");
		
		if ("update".equalsIgnoreCase(request.getParameter("action"))) {
			
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			Date now = new Date();
	
			Collection<AutopricingExchange> exchangeListFromDB = DAORegistry.getAutopricingExchangeDAO().getAllActiveExchanges();
			Map<Integer,AutopricingExchange> autopriicngSettingsMap = new HashMap<Integer, AutopricingExchange>();
			for (AutopricingExchange autopricingExchange : exchangeListFromDB) {
					autopriicngSettingsMap.put(autopricingExchange.getId(),autopricingExchange);
			}
			
			List<AutopricingExchange> autopricingExchangeList = new ArrayList<AutopricingExchange>();
			
			List<AutopricingExchangeAudit> auditList = new ArrayList<AutopricingExchangeAudit>();
			Set<String> params = request.getParameterMap().keySet();
			for (String param : params) {
				if (param.contains("checkbox_")) {
					String temp[] = param.split("_");
					Integer exchangeId = Integer.parseInt(temp[1].trim());
					
					String auditAction = "UPDATE";
					AutopricingExchange autopricingExchange = autopriicngSettingsMap.get(exchangeId);
					autopricingExchange.setAdditionalMarkup(Double.parseDouble(request.getParameter("additionalMarkup_" + exchangeId)));
					
					autopricingExchangeList.add(autopricingExchange);
					
					AutopricingExchangeAudit audit = new AutopricingExchangeAudit(autopricingExchange);
					audit.setAction(auditAction);
					audit.setLastUpdated(now);
					audit.setLastUpdatedBy(username);
					auditList.add(audit);
				}
			}
			DAORegistry.getAutopricingExchangeDAO().updateAll(autopricingExchangeList);
			
			if(!auditList.isEmpty()) {
				DAORegistry.getAutopricingExchangeAuditDAO().saveOrUpdateAll(auditList);
			}
			
			sportinfo = "Autopricing Exchange Markup Updated successfully.";
		}
		Collection<AutopricingExchange> autopricingExchangeList = DAORegistry.getAutopricingExchangeDAO().getAllActiveExchanges();
		
		
		mav.addObject("autopricingExchangeList", autopricingExchangeList);
		mav.addObject("sportinfo", sportinfo);
		return mav;
	}
	@RequestMapping(value="/AutoPricingExchangeMarkupAudit")
	public ModelAndView loadExchangeMarkupAudit(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	
	ModelAndView mav = new ModelAndView("page-autopricing-exchange-markup-audit-popup");
	Integer exchangeId = Integer.parseInt(request.getParameter("exchangeId"));
	List<AutopricingExchangeAudit> list = DAORegistry.getAutopricingExchangeAuditDAO().getAutopricingExchangeAuditByExchangeId(exchangeId);
	
	mav.addObject("auditList", list);
	return mav;
}
@RequestMapping(value="/AutopricingEditLogFiles")
public ModelAndView loadLogFiles(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		String sportinfo = "";
		String brokerId=request.getParameter("brokerId");
		
		ModelAndView mav = new ModelAndView("page-admin-log-files");
		mav.addObject("brokers", DAORegistry.getBrokerDAO().getAllActiveBrokers());
		if ("auto_download".equalsIgnoreCase(request.getParameter("action"))) {
			String exchange = request.getParameter("auto_exchange");
			String link = request.getParameter("auto_link");
			
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadTndCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadTndCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTndCategoryTickets"));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadTndCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadTndCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTndCategoryEvents"));
				}
				if (exchange.equals("tn") && link.equals("DownloadTndErrors")) {
					System.out
							.println("Inside tn exchange and DownloadTndErrors");
					return new ModelAndView(new RedirectView(
							"DownloadTndErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndConcertsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTndConcertsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividConcertsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndConcertsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTndConcertsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividConcertsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndSportsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTndSportsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividSportsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndSportsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTndSportsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividSportsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndTheaterCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTndTheaterCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividTheaterCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndTheaterCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTndTheaterCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividTheaterCategoryEvents"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTndCategoryTickets")) {
					System.out
							.println("Inside tickPick exchange and DownloadCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickAutoCatsCategoryTickets"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTndCategoryEvents")) {
					System.out
							.println("Inside tickPick exchange and DownloadCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickAutoCatsCategoryEvents"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTndCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigAutoCatsCategoryTickets"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTndCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigAutoCatsCategoryEvents"));
				}
				if (exchange.equals("exchange1")
						&& link.equals("DownloadTndErrors")) {
					System.out
							.println("Inside exchange1 and DownloadTndErrors");
				}
			}

		} else if ("mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("mini_exchange");
			String link = request.getParameter("mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()){
				 sportinfo = "Please select Broker !";
			    }
			 else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			   } else {
				if (exchange.equals("tn")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadTGCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTGCatsCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadTGCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTGCatsCategoryEvents"));
				}
				if (exchange.equals("tn") && link.equals("DownloadTGErrors")) {
					System.out
							.println("Inside tn exchange and DownloadTGErrors");
					return new ModelAndView(new RedirectView(
							"DownloadTGCatsErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGConcertsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGConcertsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGConcertsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGConcertsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGConcertsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGConcertsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGSportsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGSportsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGSportsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGSportsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGSportsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGSportsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGTheaterCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGTheaterCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGTheaterCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGTheaterCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGTheaterCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGTheaterCategoryEvents"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside tickPick exchange and DownloadTGCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickTGCatsCategoryTickets"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTGCategoryEvents")) {
					System.out
							.println("Inside tickPick exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickTGCatsCategoryEvents"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadTGCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigTGCatsCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTGCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigTGCatsCategoryEvents"));
				}
				/*if (exchange.equals("fanxchange")
						&& link.equals("DownloadTGCategoryTickets")) {
					
					if(brokerId.isEmpty() || !brokerId.equals("4")) {
						 sportinfo = "Please select Valid Broker !";
					} else {
						return new ModelAndView(new RedirectView(
								"DownloadFanXchangeTGCatsCategoryTickets?brokerId="+brokerId));
					}
					
				}*/
				if (exchange.equals("ticketcity")
						&& link.equals("DownloadTGCategoryTickets")) {
					
					if(brokerId.isEmpty() || !brokerId.equals("4")) {
						 sportinfo = "Please select Valid Broker !";
					} else {
						return new ModelAndView(new RedirectView(
								"DownloadTicketcityTGCatsCategoryTickets?brokerId="+brokerId));
					}
					
				}
				if (exchange.equals("exchange1")
						&& link.equals("DownloadTGErrors")) {
					System.out.println("Inside exchange1 and DownloadTGErrors");
				}
			}
		} else if ("vip_mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("vip_mini_exchange");
			String link = request.getParameter("vip_mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			    }
			     else if (exchange.isEmpty() || link.isEmpty()) {
					sportinfo = "Please select exchange and file !";
				   } else {
					if (exchange.equals("tn")
							&& link.equals("DownloadVipCategoryTickets")) {
						System.out
								.println("Inside tn exchange and DownloadVipCategoryTickets");
						return new ModelAndView(new RedirectView(
								"DownloadVipCatsCategoryTickets?brokerId="+brokerId));
					}
				if (exchange.equals("tn")
						&& link.equals("DownloadVipCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadVipCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVipCatsCategoryEvents"));
				}
				if (exchange.equals("tn") && link.equals("DownloadVipErrors")) {
					System.out
							.println("Inside tn exchange and DownloadVipErrors");
					return new ModelAndView(new RedirectView(
							"DownloadVipCatsErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadVipCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadVividVipCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividVipCatsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadVipCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadVipCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividVipCatsCategoryEvents"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadVipCategoryTickets")) {
					System.out
							.println("Inside tickPick exchange and DownloadVipCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickVipCatsCategoryTickets"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadVipCategoryEvents")) {
					System.out
							.println("Inside tickPick exchange and DownloadVipCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickVipCatsCategoryEvents"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadVipCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadVipCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigVipCatsCategoryTickets"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadVipCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadVipCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigVipCatsCategoryEvents"));
				}
				if (exchange.equals("ticketcity")
						&& link.equals("DownloadVipCategoryTickets")) {
					
					if(brokerId.isEmpty() || !brokerId.equals("4")) {
						 sportinfo = "Please select Valid Broker !";
					} else {
						return new ModelAndView(new RedirectView(
								"DownloadTicketcityVipCatsCategoryTickets?brokerId="+brokerId));
					}
					
				}
			}
		} else if ("vip_auto_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("vip_auto_exchange");
			String link = request.getParameter("vip_auto_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadVipAutoCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadVipAutoCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVipAutoCatsCategoryTickets"));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadVipAutoCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadVipAutoCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVipAutoCatsCategoryEvents"));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadVipAutoErrors")) {
					System.out
							.println("Inside tn exchange and DownloadVipAutoErrors");
					return new ModelAndView(new RedirectView(
							"DownloadVipAutoCatsErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadVipAutoCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadVividVipCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividVipAutoCatsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadVipAutoCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadVipAutoCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividVipAutoCatsCategoryEvents"));
				}
				/*
				 * if(exchange.equals("tickPick") &&
				 * link.equals("DownloadVipAutoCategoryTickets")) {
				 * System.out.println(
				 * "Inside tickPick exchange and DownloadVipAutoCatsCategoryTickets"
				 * ); return new ModelAndView(new
				 * RedirectView("DownloadTickPickVipAutoCatsCategoryTickets"));
				 * } if(exchange.equals("tickPick") &&
				 * link.equals("DownloadVipAutoCategoryEvents")) {
				 * System.out.println
				 * ("Inside tickPick exchange and DownloadVipAutoCategoryEvents"
				 * ); return new ModelAndView(new
				 * RedirectView("DownloadTickPickVipAutoCatsCategoryEvents")); }
				 */
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadVipAutoCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadVipAutoCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigVipAutoCatsCategoryTickets"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadVipAutoCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadVipAutoCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigVipAutoCatsCategoryEvents"));
				}
			}
		} else if ("lastrow_mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("lastrow_mini_exchange");
			String link = request.getParameter("lastrow_mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			  else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			     } else {
				if (exchange.equals("tn")
						&& link.equals("DownloadLastRowMiniCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadLastRowMiniCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTNLastRowMiniCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadLastRowMiniCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadLastRowMiniCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTNLastRowMiniCategoryEvents"));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadLastRowMiniErrors")) {
					System.out
							.println("Inside tn exchange and DownloadLastRowMiniErrors");
					return new ModelAndView(new RedirectView(
							"DownloadLastRowMiniErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadLastRowMiniCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadLastRowMiniCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadLastRowMinivividCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadLastRowMiniCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadLastRowMiniErrors");
					return new ModelAndView(new RedirectView(
							"DownloadLastRowMiniscoreBigCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("fanxchange")
						&& link.equals("DownloadLastRowMiniCategoryTickets")) {
					
					if(brokerId.isEmpty() || !brokerId.equals("4")) {
						 sportinfo = "Please select Valid Broker !";
					} else {
						return new ModelAndView(new RedirectView(
								"DownloadLastRowMiniFanXchangeCategoryTickets?brokerId="+brokerId));
					}
				}
				if (exchange.equals("ticketcity")
						&& link.equals("DownloadLastRowMiniCategoryTickets")) {
					
					if(brokerId.isEmpty() || !brokerId.equals("4")) {
						 sportinfo = "Please select Valid Broker !";
					} else {
						return new ModelAndView(new RedirectView(
								"DownloadLastRowMiniTicketcityCategoryTickets?brokerId="+brokerId));
					}
					
				}
			}
		}else if ("vip_lastrow_mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("vip_lastrow_mini_exchange");
			String link = request.getParameter("vip_lastrow_mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			  else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			     } else {
				if (exchange.equals("tn")
						&& link.equals("DownloadVipLastRowMiniCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadVipLastRowMiniCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTNVipLastRowMiniCategoryTickets?brokerId="+brokerId));
				}
				
			}
		} else if ("lastfiverow_mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("lastfiverow_mini_exchange");
			String link = request.getParameter("lastfiverow_mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("stubhub")
						&& link.equals("DownloadLastFiveRowMiniCatTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadStubhubLastFiveRowMiniCatTickets"));
				}
				if (exchange.equals("stubhub")
						&& link.equals("DownloadLastFiveRowMiniCatEvents")) {
					return new ModelAndView(new RedirectView(
							"DownloadStubhubLastFiveRowMiniCatEvents"));
				}

			}
		}else if("zones_pric_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("zones_pric_exchange");
			String link = request.getParameter("zones_pric_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadZonesPricingCategoryTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadZonesPricingTnCategoryTickets?brokerId="+brokerId));
				}
			
			}	
			
		} else if("tixcity_zones_pric_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("tixcity_zones_pric_exchange");
			String link = request.getParameter("tixcity_zones_pric_link");
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadTixCityZonesPricingCategoryTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadTixCityZonesPricingTnCategoryTickets?brokerId="+brokerId));
				}
			
			}	
			
		}else if("manhattan_zones_pric_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("manhattan_zones_pric_exchange");
			String link = request.getParameter("manhattan_zones_pric_link");
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadManhattanZonesPricingCategoryTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadManhattanZonesPricingTnCategoryTickets?brokerId="+brokerId));
				}
			
			}	
			
		}else if("zoned_lastrow_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("zoned_last_exchange");
			String link = request.getParameter("zoned_last_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadZonedLastRowCategoryTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadZonedLastRowTnCategoryTickets?brokerId="+brokerId));
				}
			
			}	
			
		}else if("larry_last_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("Larry_last_exchange");
			String link = request.getParameter("Larry_last_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadLarryLastCategoryTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadLarryLastTnCategoryTickets?brokerId="+brokerId));
				}
			
			}	
			
		}else if ("Presale_auto_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("Presale_auto_exchange");
			String link = request.getParameter("Presale_auto_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()){
				 sportinfo = "Please select Broker !";
			    }
			 else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			   } else {
				if (exchange.equals("tn")
						&& link.equals("DownloadPresaleAutoCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadPresaleAutoCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadPresaleAutoCatsCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadPresaleAutoCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadPresaleAutoCatsCategoryEvents"));
				}
				if (exchange.equals("tn") && link.equals("DownloadTGErrors")) {
					System.out
							.println("Inside tn exchange and DownloadTGErrors");
					return new ModelAndView(new RedirectView(
							"DownloadTGCatsErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadPresaleAutoCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadPresaleAutoCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividPresaleAutoCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGConcertsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGConcertsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGConcertsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGConcertsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGConcertsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGConcertsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGSportsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGSportsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGSportsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGSportsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGSportsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGSportsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGTheaterCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGTheaterCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGTheaterCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGTheaterCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGTheaterCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGTheaterCategoryEvents"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside tickPick exchange and DownloadTGCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickTGCatsCategoryTickets"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTGCategoryEvents")) {
					System.out
							.println("Inside tickPick exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickTGCatsCategoryEvents"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadPresaleAutoCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadPresaleAutoCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigPresaleAutoCatsCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTGCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigTGCatsCategoryEvents"));
				}
				if (exchange.equals("exchange1")
						&& link.equals("DownloadTGErrors")) {
					System.out.println("Inside exchange1 and DownloadTGErrors");
				}
			  }
		}else if("zone_tickets_processor_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("zone_tickets_processor_exchange");
			String parentCategory = request.getParameter("zone_tickets_processor_link");
			if(brokerId.isEmpty() || !brokerId.equals("1")) {
				 sportinfo = "Please select Valid Broker !";
			  }
			else if (exchange.isEmpty() || parentCategory.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")) {
					return new ModelAndView(new RedirectView(
							"DownloadZoneTicketsProcessorCategoryTickets?brokerId="+brokerId+"&parentCategory="+parentCategory));
				}
			
			}	
			
		}else if ("autocat_above96_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("autocat_above96_exchange");
			String link = request.getParameter("autocat_above96_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()){
				 sportinfo = "Please select Broker !";
			    }
			 else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			   } else {
				     if (exchange.equals("tn")	&& link.equals("DownloadAutocatAbove96CategoryTickets")) {
					System.out.println("Inside tn exchange and DownloadAutocatAbove96CategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTnAutocatAbove96CategoryTickets?brokerId="+brokerId));
				}								
			  }	
		
		} else if ("sg_lastfiverow_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("sg_lastfiverow_exchange");
			String link = request.getParameter("sg_lastfiverow_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadSgLastFiveRowTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadTnSgLastFiveRowTickets?brokerId="+brokerId));
				}
				/*if (exchange.equals("tn")
						&& link.equals("DownloadSgLastFiveRowEvents")) {
					return new ModelAndView(new RedirectView(
							"DownloadTnSgLastFiveRowEvents"));
				}*/

			}
		} else if("manualcats_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("manualcats_exchange");
			String link = request.getParameter("manualcats_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()){
				 sportinfo = "Please select Broker !";
			    }
			 else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			   } else {
				     if (exchange.equals("vivid")	&& link.equals("DownloadManualCatVividCategoryTickets")) {
					System.out.println("Inside tn exchange and DownloadManualCatVividCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadManualCatVividCategoryTickets?brokerId="+brokerId));
				}								
			  }	
		}
		
		mav.addObject("sportinfo", sportinfo);
		return mav;
	}
@RequestMapping(value="/DownloadTnAutocatAbove96CategoryTickets")
public void downloadTnAutocatAbove96CategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	 String brokerId=request.getParameter("brokerId");
	 
	if(brokerId.equals("3")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=ROTAutocatAbove96TNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("5")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTWAutocatAbove96TNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID.toString())){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=TixcityAutocats96TNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("10")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTW-2AutocatAbove96TNCategoryTickets." + today + ".csv");
	 }
	

	try {
	
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Qauntity,Section,Row,Price,InternalNote\n"
				.getBytes());     
	    							   
	    List<AutoCats96TnCategoryTicket> tixs =DAORegistry.getQueryManagerDAO().getTnAutoCatsAbove96CategoryTicket(brokerId);
				

		for (AutoCats96TnCategoryTicket cat : tixs) {
			
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenue() + ",";
			outString += cat.getQuantity() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRow() + ",";
			outString += cat.getPrice() + ",";
			outString += cat.getInternalNote() + "\n";
			out.write(outString.getBytes());
		}
	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}

@RequestMapping(value="/DownloadTnSgLastFiveRowTickets")
public void downloadTnSgLastFiveRowTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	 String brokerId=request.getParameter("brokerId");
	 
	if(brokerId.equals("5")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTW_SGLastFiveRowTNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("10")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTW-2_SGLastFiveRowTNCategoryTickets." + today + ".csv");
	 }
	

	try {
	
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Qauntity,Section,Row,Price,InternalNote\n"
				.getBytes());     
	    							   
	    List<AutoCats96TnCategoryTicket> tixs =DAORegistry.getQueryManagerDAO().getTnSgLastFiveRowCategoryTicket(brokerId);
				

		for (AutoCats96TnCategoryTicket cat : tixs) {
			
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenue() + ",";
			outString += cat.getQuantity() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRow() + ",";
			outString += cat.getPrice() + ",";
			outString += cat.getInternalNote() + "\n";
			out.write(outString.getBytes());
		}
	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}

@RequestMapping(value="/DownloadZonesPricingTnCategoryTickets")
public void downloadZonesPricingTnCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	 String brokerId=request.getParameter("brokerId");
	 
	if(brokerId.equals("3")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=ROTZonesPricingTNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("5")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTWZonesPricingTNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("10")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTW-2ZonesPricingTNCategoryTickets." + today + ".csv");
	 }
	
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
	
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Qauntity,Section,Row,Price\n"
				.getBytes());     
	    							   
	    List<ZonePricingTnCategoryTicket> tixs =DAORegistry.getQueryManagerDAO().getTnZonesCategoryTickets(brokerId);
				

		for (ZonePricingTnCategoryTicket cat : tixs) {
			
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenue() + ",";
			outString += cat.getQuantity() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRow() + ",";
			outString += cat.getPrice() + "\n";
			out.write(outString.getBytes());
		}
	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}
@RequestMapping(value="/DownloadTixCityZonesPricingTnCategoryTickets")
public void downloadTixCityZonesPricingTnCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	 String brokerId=request.getParameter("brokerId");
	 
	 if(brokerId.equals("3")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=ROT_RTWZonesPricingTNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("5")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTW_RTWZonesPricingTNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("10")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTW-2_RTWZonesPricingTNCategoryTickets." + today + ".csv");
	 } /*else  if(brokerId.equals("7")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTWNew_RTWZonesPricingTNCategoryTickets." + today + ".csv");
	 }*/
	
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
	
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Qauntity,Section,Row,Price\n"
				.getBytes());     
	    							   
	    List<ZonePricingTnCategoryTicket> tixs =DAORegistry.getQueryManagerDAO().getTnTixCityZonesCategoryTickets(brokerId);
				

		for (ZonePricingTnCategoryTicket cat : tixs) {
			
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenue() + ",";
			outString += cat.getQuantity() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRow() + ",";
			outString += cat.getPrice() + "\n";
			out.write(outString.getBytes());
		}
	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}

@RequestMapping(value="/DownloadManhattanZonesPricingTnCategoryTickets")
public void downloadManhattanZonesPricingTnCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	 String brokerId=request.getParameter("brokerId");
	 
	 if(brokerId.equals("3")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=ROT_ManhattanZonesPricingTNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("4")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=Manhattan_ManhattanZonesPricingTNCategoryTickets." + today + ".csv");
	 } else  if(brokerId.equals("10")){
		 response.setHeader("Content-Type", "application/vnd.ms-excel");
		 response.setHeader("Content-disposition",
					"attachment; filename=RTW-2_ManhattanZonesPricingTNCategoryTickets." + today + ".csv");
	 }
	
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
	
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Qauntity,Section,Row,Price\n"
				.getBytes());     
	    							   
	    List<ZonePricingTnCategoryTicket> mans =DAORegistry.getQueryManagerDAO().getTnManhattanZonesCategoryTickets(brokerId);
				

		for (ZonePricingTnCategoryTicket cat : mans) {
			
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenue() + ",";
			outString += cat.getQuantity() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRow() + ",";
			outString += cat.getPrice() + "\n";
			out.write(outString.getBytes());
		}
	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}


@RequestMapping(value="/DownloadZoneTicketsProcessorCategoryTickets")
public void downloadZoneTicketsProcessorCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	 String brokerId=request.getParameter("brokerId");
	 String parentCategory = request.getParameter("parentCategory");
	 
	String brokerDesc = "";
		 
	 /*if(brokerId.equals("3")){
		 brokerDesc = "ROT";
	 } else  if(brokerId.equals("5")){
		 brokerDesc = "RTW";
	 }*/
	 
	 response.setHeader("Content-Type", "application/vnd.ms-excel");
	 response.setHeader("Content-disposition","attachment; filename="+parentCategory+"ZonesTicketsTNCategoryTickets." + today + ".csv");
		
	try {
	
		String header = "EventName,EventDate,EventTime,Venue,Section,Qauntity,Priority,Price,SectionRange,RowRange,TicketDeliveryType\n";
		StringBuilder fileString = new StringBuilder(header);
		
		ServletOutputStream out = response.getOutputStream();
		ZipOutputStream zos = null;
		int rowCount = 0,fileCount=1;
	    boolean isZipFile = false;
		 
		List<CategoryTicketDownloader> tixs = DAORegistry.getQueryManagerDAO().getZoneTicketProcessorCategoryTickets(parentCategory);

		if(tixs != null) {
	    	if(tixs.size() > 1048570) {
	    		isZipFile = true;
	    		response.setContentType("Content-type: text/zip");
	    		response.setHeader("Content-Disposition","attachment; filename="+parentCategory+"ZonesTicketsTNCategoryTickets." + today + ".zip");
	    		zos = new ZipOutputStream(new BufferedOutputStream(out));
	    	}
			for (CategoryTicketDownloader cat : tixs) {
				rowCount++;
				fileString.append(cat.getEventName().replaceAll(",", "-") + ",");
				fileString.append(cat.getEventDate() + ",");
				fileString.append(cat.getEventTime() + ",");
				fileString.append(cat.getVenueName().replaceAll(",", "-") + ",");
				fileString.append(cat.getSection() + ",");
				fileString.append(cat.getQuantity() + ",");
				fileString.append(cat.getPriority() + ",");
				fileString.append(cat.getZoneTicketPrice() + ",");
				fileString.append(cat.getSectionRange().replaceAll(",", "-") + ",");
				fileString.append(cat.getRowRange().replaceAll(",", "-") + ",");
				fileString.append(cat.getTicketDeliveryType().replaceAll(",", "-") + ",");
				
				fileString.append("\n");

				if(isZipFile) {
					if(rowCount > 1048570) {
						ZipEntry entry = new ZipEntry(parentCategory+"ZonesTicketsTNCategoryTickets"+fileCount+"." + today + ".csv");
						zos.putNextEntry(entry);
						 zos.write(fileString.toString().getBytes());
						 zos.closeEntry();
						 
						 fileCount++;
						 rowCount =0;
						 fileString = new StringBuilder(header);
					}
				}
			}
	    }
	    if(isZipFile) {
			if(rowCount > 0) {
				ZipEntry entry = new ZipEntry(parentCategory+"ZonesTicketsTNCategoryTickets"+fileCount+"." + today + ".csv");
				zos.putNextEntry(entry);
				 zos.write(fileString.toString().getBytes());
				 zos.closeEntry();
				 
				 fileCount++;
				 rowCount =0;
				 fileString = new StringBuilder(header);
			}
			zos.close();
			
		} else {
			out.write(fileString.toString().getBytes());
		}
	    
	    
	} catch (Exception e) {
		e.printStackTrace();
		throw e;
	}
}

@RequestMapping(value="/DownloadZonedLastRowTnCategoryTickets")
public void downloadZonedLastRowTnCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("3")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ROTZonedLastRowTNCategoryTickets." + today + ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWZonedLastRowTNCategoryTickets." + today + ".csv");
	} else if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID.toString())){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=TixcityZonedLastRowTNCategoryTickets." + today + ".csv");
	} else if(brokerId.equals("10")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTW-2_ZonedLastRowTNCategoryTickets." + today + ".csv");
	}
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
	
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
				.getBytes());
		
	   		    
	    List<ZonedLastRowTnCategoryTicket> tixs = DAORegistry.getQueryManagerDAO().getTnZonedLastRowCategoryTickets(brokerId);
				

		for (ZonedLastRowTnCategoryTicket cat : tixs) {
			
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenue() + ",";
			outString += cat.getQuantity() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRow() + ".,";
			outString += cat.getPrice() + "\n";
          
			out.write(outString.getBytes());
		}
	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}	
	
@RequestMapping(value="/DownloadLarryLastTnCategoryTickets")	
public void downloadLarryLastTnCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("3")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ROTLarryLastTNCategoryTickets." + today + ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWLarryLastTNCategoryTickets." + today + ".csv");
	}else if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID.toString())){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=TixcityLarryLastTNCategoryTickets." + today + ".csv");
	} else if(brokerId.equals("10")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTW-2_LarryLastTNCategoryTickets." + today + ".csv");
	}
	
	
	
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
	
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Quantity,Section,Row,Price\n"
				.getBytes());
		
		
	    List<LarryLastTnCategoryTicket> tixs =DAORegistry.getQueryManagerDAO().getTnLarryLastCategoryTickets(brokerId);
				

		for (LarryLastTnCategoryTicket cat : tixs) {
			
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenue() + ",";
			outString += cat.getQuantity() + ",";
			//outString += cat.getZone() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRow() + ".,";
			outString += cat.getPrice() + "\n";
            
			
			out.write(outString.getBytes());
		}
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}	


@RequestMapping(value="/DownloadPresaleAutoCatsCategoryTickets")
public void downloadPresaleAutoCatsCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("3")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ROTPresaleAutoCatsTNCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWPresaleAutoCatsTNCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("10")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTW-2_PresaleAutoCatsTNCategoryTickets." + today
						+ ".csv");
	} 
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
		    OutputStream out = response.getOutputStream();
		    out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Seat,Price,TMATTicketGroupId\n"
				.getBytes());

      
		    List<VWTGCatsCategoryTicket> tixs=null;
		 		
			System.out.println("BrokerId:"+brokerId);
			tixs=DAORegistry.getQueryManagerDAO().getTnPresaleAutoCategoryTickets(brokerId);
			
			String dateInString=null;
			String newDate=null;     
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				  
				  			
		for (VWTGCatsCategoryTicket cat : tixs) {
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenueName().replaceAll(",", "-") + ",";
			outString += cat.getQuantity() + ",";
			// outString += cat.getZone() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRowRange() + ".,";
			outString += cat.getSeat() + ",";
			outString += cat.getTnPrice() + ",";
			outString += cat.getTmatTicketGroupId() + "\n";
			/*outString += cat.getExpectedArrivalDate() + ",";
			outString += shippingMap.get(cat.getShippingMethodId()) + ",";
			outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
					+ "\n";*/
			out.write(outString.getBytes());
		}

	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}
@RequestMapping(value="/DownloadTGCatsCategoryTickets")
public void downloadTGCatsCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("3")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ROTMiniCatsTNCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWMiniCatsTNCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("10")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTW-2_MiniCatsTNCategoryTickets." + today
						+ ".csv");
	}
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
		    OutputStream out = response.getOutputStream();
		    out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
				.getBytes());

      
		    List<VWTGCatsCategoryTicket> tixs=null;
		 		
			System.out.println("BrokerId:"+brokerId);
			tixs=DAORegistry.getQueryManagerDAO().getTnTgCategoryTickets(brokerId);
			
			String dateInString=null;
			String newDate=null;     
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				  
				  			
		for (VWTGCatsCategoryTicket cat : tixs) {
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenueName().replaceAll(",", "-") + ",";
			outString += cat.getQuantity() + ",";
			// outString += cat.getZone() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRowRange() + ".,";
			outString += cat.getTnPrice() + "\n";
			/*outString += cat.getExpectedArrivalDate() + ",";
			outString += shippingMap.get(cat.getShippingMethodId()) + ",";
			outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
					+ "\n";*/
			out.write(outString.getBytes());
		}

	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}


@RequestMapping(value="/DownloadVividPresaleAutoCategoryTickets")
public void downloadVividPresaleAutoCatsCategoryTickets(HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	

	
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	String brokerId=request.getParameter("brokerId");
	if(brokerId.equals("1")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MzTixPresaleAutoCatsVividCategoryTickets." + today
						+ ".csv");
		}
	if(brokerId.equals("3")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ROTPresaleAutoCatsVividCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWPresaleAutoCatsVividCategoryTickets." + today
						+ ".csv");
	}
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
		    OutputStream out = response.getOutputStream();
		    out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price,TmatTicketGroupId\n"
				.getBytes());

      
		    List<VWTGCatsCategoryTicket> tixs=null;
		 		
			System.out.println("BrokerId:"+brokerId);
			try{
			tixs=DAORegistry.getQueryManagerDAO().getVividPresaleAutoCategoryTickets(brokerId);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			String dateInString=null;
			String newDate=null;     
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				  
		if(tixs!=null){		  			
		for (VWTGCatsCategoryTicket cat : tixs) {
			String outString = "";
			 outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenueName().replaceAll(",", "-") + ",";
			outString += cat.getQuantity() + ",";
			// outString += cat.getZone() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getRowRange() + ".,";
			outString += cat.getTnPrice() + ",";
			outString += cat.getTmatTicketGroupId() + "\n";
			/*outString += cat.getExpectedArrivalDate() + ",";
			outString += shippingMap.get(cat.getShippingMethodId()) + ",";
			outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
					+ "\n";*/
			out.write(outString.getBytes());
		}
	  }
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	}	
	
}
@RequestMapping(value="/DownloadManualCatVividCategoryTickets")
public void downloadManualCatVividCategoryTickets(HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	
	String brokerId=request.getParameter("brokerId");
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
    String today = dateFormat.format(new Date());
    response.setHeader("Content-Type", "application/vnd.ms-excel");
                                                                  
    BufferedReader br = null;
    StringBuilder sb = new StringBuilder();
    
    if(brokerId.equals("3")){
    	response.setHeader("Content-disposition",
    			"attachment; filename=ROTManualCatsVividCategoryTickets."
    					+ today + ".csv");
    } else {
    	response.setHeader("Content-disposition",
    			"attachment; filename=RTWManualCatsVividCategoryTickets."
    					+ today + ".csv");
    
    }
    

try {
	OutputStream out = response.getOutputStream();
	
	if(brokerId.equals("3")){
		File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTvividseatsmanualcats.csv");
		br = new BufferedReader(
				new FileReader(file));			
	}/* else if(brokerId.equals("5"))	 {
		File file=new File(mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWvividseatsmanualcats.csv");
		br = new BufferedReader(
				new FileReader(file));	
	}*/
	String sCurrentLine;
	if(br!=null){
	while ((sCurrentLine = br.readLine()) != null) {
		sb.append(sCurrentLine + "\n");
	}
}
	out.write(sb.toString().getBytes());
} catch (IOException e) {
	System.out.println(e.fillInStackTrace());
} finally {
	try {
		if(br!=null){
	    	br.close();
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
		
	
}


@RequestMapping(value="/DownloadVividTGCategoryTickets")
public void downloadVividTGCatsCategoryTickets(HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	
	String brokerId=request.getParameter("brokerId");
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
    String today = dateFormat.format(new Date());
    response.setHeader("Content-Type", "application/vnd.ms-excel");
                                                                  
    BufferedReader br = null;
    StringBuilder sb = new StringBuilder();
    
    if(brokerId.equals("3")){
    	response.setHeader("Content-disposition",
    			"attachment; filename=ROTMiniCatsVividCategoryTickets."
    					+ today + ".csv");
    } else {
    	response.setHeader("Content-disposition",
    			"attachment; filename=RTWMiniCatsVividCategoryTickets."
    					+ today + ".csv");
    
    }
    

try {
	OutputStream out = response.getOutputStream();
	
	if(brokerId.equals("3")){
		File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTvividseatsminicats.csv");
		br = new BufferedReader(
				new FileReader(file));			
	} else if(brokerId.equals("5"))	 {
		File file=new File(mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWvividseatsminicats.csv");
		br = new BufferedReader(
				new FileReader(file));	
	}
	String sCurrentLine;
	while ((sCurrentLine = br.readLine()) != null) {
		sb.append(sCurrentLine + "\n");
	}
	out.write(sb.toString().getBytes());
} catch (IOException e) {
	System.out.println(e.fillInStackTrace());
} finally {
	try {
	    	br.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
		
	
}

@RequestMapping(value="/DownloadScoreBigPresaleAutoCatsCategoryTickets")
public void downloadScoreBigCatsScoreBigCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	String brokerId=request.getParameter("brokerId");
	if(brokerId.equals("1")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MzTixPresaleAutoCatsScoreBigCategoryTickets." + today
						+ ".csv");
		}
	if(brokerId.equals("3")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ROTPresaleAutoCatsScoreBigCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWPresaleAutoCatsScoreBigCategoryTickets." + today
						+ ".csv");
	}
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");
	try {
	    OutputStream out = response.getOutputStream();
	    out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price,TmatTicketGroupId\n"
			.getBytes());

  
	    List<VWTGCatsCategoryTicket> tixs=null;
	 		
		System.out.println("BrokerId:"+brokerId);
		try{
	//	tixs=DAORegistry.getPresaleAutoCatCategoryTicketDAO().getScoreBigPresaleAutoCategoryTickets(brokerId);
			tixs=DAORegistry.getQueryManagerDAO().getScoreBigPresaleAutoCategoryTickets(brokerId);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String dateInString=null;
		String newDate=null;     
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			  
	if( tixs!=null){		  			
	for (VWTGCatsCategoryTicket cat : tixs) {
		String outString = "";
		outString = cat.getEventName().replaceAll(",", "-") + ",";
		outString += cat.getEventDate() + ",";
		outString += cat.getEventTime() + ",";
		outString += cat.getVenueName().replaceAll(",", "-") + ",";
		outString += cat.getQuantity() + ",";
		// outString += cat.getZone() + ",";
		outString += cat.getSection() + ".,";
		outString += cat.getRowRange() + ".,";
//		outString += cat.getSeat() + ",";
		outString += cat.getTnPrice() + ",";
		outString += cat.getTmatTicketGroupId()+"." + "\n";
		/*outString += cat.getExpectedArrivalDate() + ",";
		outString += shippingMap.get(cat.getShippingMethodId()) + ",";
		outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
				+ "\n";*/
		out.write(outString.getBytes());
	}
}
} catch (Exception e) {
	System.out.println(e.fillInStackTrace());
}	

	
}
@RequestMapping(value="/DownloadScoreBigTGCatsCategoryTickets")
public void downloadTGCatsScoreBigCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	response.setHeader("Content-Type", "application/vnd.ms-excel");
	
	BufferedReader br = null;
	StringBuilder sb = new StringBuilder();
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("3")){
		response.setHeader("Content-disposition",
				"attachment; filename=ROTMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}else if(brokerId.equals("5")){
		response.setHeader("Content-disposition",
				"attachment; filename=RTWMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}else if(brokerId.equals("4")){
		response.setHeader("Content-disposition",
				"attachment; filename=ManhattanMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}

	try {
		OutputStream out = response.getOutputStream();
		if(brokerId.equals("3")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("5")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("4")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"Manhattan\\scorebig\\Manhattanscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

@RequestMapping(value="/DownloadFanXchangeTGCatsCategoryTickets")
public void downloadTGCatsFanXchangeCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	response.setHeader("Content-Type", "application/vnd.ms-excel");
	
	BufferedReader br = null;
	StringBuilder sb = new StringBuilder();
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("4")){
		response.setHeader("Content-disposition",
				"attachment; filename=ManhattanMiniCatsTicketcityCategoryTickets." + today
						+ ".csv");
	}/*else if(brokerId.equals("3")){
		response.setHeader("Content-disposition",
				"attachment; filename=ROTMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}else if(brokerId.equals("5")){
		response.setHeader("Content-disposition",
				"attachment; filename=RTWMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}*/

	try {
		OutputStream out = response.getOutputStream();
		if(brokerId.equals("4")){//Manhattan
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"Manhattan\\fanxchange\\Manhattanfanxchangeminicats.csv");
			br = new BufferedReader(
					new FileReader(file));
		}/*else if(brokerId.equals("3")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("5")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}*/
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

@RequestMapping(value="/DownloadTicketcityTGCatsCategoryTickets")
public void downloadTGCatsTicketcityCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	response.setHeader("Content-Type", "application/vnd.ms-excel");
	
	BufferedReader br = null;
	StringBuilder sb = new StringBuilder();
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("4")){
		response.setHeader("Content-disposition",
				"attachment; filename=ManhattanMiniCatsTicketcityCategoryTickets." + today
						+ ".csv");
	}/*else if(brokerId.equals("3")){
		response.setHeader("Content-disposition",
				"attachment; filename=ROTMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}else if(brokerId.equals("5")){
		response.setHeader("Content-disposition",
				"attachment; filename=RTWMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}*/

	try {
		OutputStream out = response.getOutputStream();
		if(brokerId.equals("4")){//Manhattan
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"Manhattan\\ticketcity\\Manhattanticketcityminicats.csv");
			br = new BufferedReader(
					new FileReader(file));
		}/*else if(brokerId.equals("3")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("5")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}*/
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
@RequestMapping(value="/DownloadTicketcityVipCatsCategoryTickets")
public void downloadVipCatsTicketcityCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	response.setHeader("Content-Type", "application/vnd.ms-excel");
	
	BufferedReader br = null;
	StringBuilder sb = new StringBuilder();
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("4")){
		response.setHeader("Content-disposition",
				"attachment; filename=ManhattanVipMiniCatsTicketcityCategoryTickets." + today
						+ ".csv");
	}/*else if(brokerId.equals("3")){
		response.setHeader("Content-disposition",
				"attachment; filename=ROTMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}else if(brokerId.equals("5")){
		response.setHeader("Content-disposition",
				"attachment; filename=RTWMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}*/

	try {
		OutputStream out = response.getOutputStream();
		if(brokerId.equals("4")){//Manhattan
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"Manhattan\\ticketcity\\Manhattanticketcityvipminicats.csv");
			br = new BufferedReader(
					new FileReader(file));
		}/*else if(brokerId.equals("3")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("5")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}*/
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

@RequestMapping(value="/DownloadLastRowMinivividCategoryTickets")
public void downloadLastRowMinivividCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	
	System.out.println("In DownloadLastRowMinivividCategoryTickets");
	
	String brokerId=request.getParameter("brokerId");
	
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	response.setHeader("Content-Type", "application/vnd.ms-excel");
	
	if(brokerId.equals("3")){
		response.setHeader("Content-disposition",
				"attachment; filename=ROTLastRowMiniCatsvividCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-disposition",
				"attachment; filename=RTWLastRowMiniCatsvividCategoryTickets." + today
						+ ".csv");
	}
	
	BufferedReader br = null;
	StringBuilder sb = new StringBuilder();

	try {
		OutputStream out = response.getOutputStream();
		if(brokerId.equals("3")){
			System.out.println("in rot vividfile read");
			File file=new File(
					mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTvividseatslarrytheater.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("5")){
			System.out.println("in RTW vividfile read");
			File file=new File(
					mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWvividseatslastrowmiinicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}
		
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
@RequestMapping(value="/DownloadLastRowMiniscoreBigCategoryTickets")
public void downloadLastRowMiniscoreBigCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	
	System.out.println("In  downloadLastRowMiniscoreBigCategoryTickets");
    String brokerId=request.getParameter("brokerId");
	
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	response.setHeader("Content-Type", "application/vnd.ms-excel");
	
	if(brokerId.equals("3")){
		response.setHeader("Content-disposition",
				"attachment; filename=ROTLastRowMiniCatsscoreBigCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-disposition",
				"attachment; filename=RTWLastRowMiniCatsscoreBigCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("4")){
		response.setHeader("Content-disposition",
				"attachment; filename=ManhattanLastRowMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");
	}
	
	BufferedReader br = null;
	StringBuilder sb = new StringBuilder();

	try {
		OutputStream out = response.getOutputStream();
		if(brokerId.equals("3")){
			System.out.println("in rot scoribig file read");
			File file=new File(
					mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigcats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("5")){
			System.out.println("in RTW scoribig file read");
			File file=new File(
					mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWscorebigcats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("4")){
			System.out.println("in RTW scoribig file read");
			File file=new File(
					mailIdProperty.getAutopricingfolderpath()+"Manhattan\\scorebig\\Manhattanscorebiglastrowminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

@RequestMapping(value="/DownloadLastRowMiniFanXchangeCategoryTickets")
public void downloadLastRowMiniFanXchangeCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	
    String brokerId=request.getParameter("brokerId");
	
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	response.setHeader("Content-Type", "application/vnd.ms-excel");
	
	if(brokerId.equals("4")){
		response.setHeader("Content-disposition",
				"attachment; filename=ManhattanLastRowMiniCatsFanXchangeCategoryTickets." + today
						+ ".csv");
	}/*else if(brokerId.equals("3")){
		response.setHeader("Content-disposition",
				"attachment; filename=ROTLastRowMiniCatsscoreBigCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("5")){
		response.setHeader("Content-disposition",
				"attachment; filename=RTWLastRowMiniCatsscoreBigCategoryTickets." + today
						+ ".csv");
	}*/
	
	BufferedReader br = null;
	StringBuilder sb = new StringBuilder();

	try {
		OutputStream out = response.getOutputStream();
		if(brokerId.equals("4")){//Manhattan
			File file=new File(
					mailIdProperty.getAutopricingfolderpath()+"Manhattan\\fanxchange\\Manhattanfanxchangelastrowminicats.csv");
			br = new BufferedReader(
					new FileReader(file));
		}/*else if(brokerId.equals("3")){
			System.out.println("in rot scoribig file read");
			File file=new File(
					mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigcats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("5")){
			System.out.println("in RTW scoribig file read");
			File file=new File(
					mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWscorebigcats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}*/
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
@RequestMapping(value="/DownloadLastRowMiniTicketcityCategoryTickets")
public void downloadLastrowMiniCatsTicketcityCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws JSONException {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	response.setHeader("Content-Type", "application/vnd.ms-excel");
	
	BufferedReader br = null;
	StringBuilder sb = new StringBuilder();
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("4")){
		response.setHeader("Content-disposition",
				"attachment; filename=ManhattanLastrowMiniCatsTicketcityCategoryTickets." + today
						+ ".csv");
	}/*else if(brokerId.equals("3")){
		response.setHeader("Content-disposition",
				"attachment; filename=ROTMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}else if(brokerId.equals("5")){
		response.setHeader("Content-disposition",
				"attachment; filename=RTWMiniCatsScoreBigCategoryTickets." + today
						+ ".csv");	
	}*/

	try {
		OutputStream out = response.getOutputStream();
		if(brokerId.equals("4")){//Manhattan
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"Manhattan\\ticketcity\\Manhattanticketcitylastrowminicats.csv");
			br = new BufferedReader(
					new FileReader(file));
		}/*else if(brokerId.equals("3")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		} else if(brokerId.equals("5")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"RTW\\RTWscorebigminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}*/
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
@RequestMapping(value="/DownloadVipCatsCategoryTickets")
public void downloadVipCatsCategoryTickets(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("3")) {
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ROTVipMiniCatsTNCategoryTickets." + today
						+ ".csv");
	} else if(brokerId.equals("5")) {
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWVipMiniCatsTNCategoryTickets." + today
						+ ".csv");
	}
	
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");

	try {
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
				.getBytes());
		List<VWVipCatsCategoryTicket> tixs=null;
		
			tixs=DAORegistry.getQueryManagerDAO().getVwVipCatsCategoryTicketDAO(brokerId);
		
		/*Collection<VWVipCatsCategoryTicket> tixs = DAORegistry
				.getVwVipCatsCategoryTicketDAO()
				.getAllActiveTnVipCatsCategoryTickets();*/
          
			String dateInString=null;
			String newDate=null;     
								
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
						
		for (VWVipCatsCategoryTicket cat : tixs) {
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenueName().replaceAll(",", "-") + ",";
			outString += cat.getQuantity() + ",";
			// outString += cat.getZone() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getAlternateRow() + ".,";
			outString += cat.getTnPrice() + "\n";
			
			/*outString += cat.getExpectedArrivalDate() + ",";
			outString += shippingMap.get(cat.getShippingMethodId()) + ",";
			outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
					+ "\n";*/
			out.write(outString.getBytes());
		}

	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}


@RequestMapping(value="/DownloadTNLastRowMiniCategoryTickets")
public void downloadLastRowMiniCatsCategoryTickets(
		HttpServletRequest request, HttpServletResponse response,
		ModelMap map) throws Exception {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	String brokerId=request.getParameter("brokerId");
	
	if(brokerId.equals("3")) {
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ROTLastRowMiniCatsTNCategoryTickets."
						+ today + ".csv");
	} else if(brokerId.equals("5")) {
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWLastRowMiniCatsTNCategoryTickets."
						+ today + ".csv");
	}
	
	Map<Integer, String> shippingMap = new HashMap<Integer, String>();
	shippingMap.put(0, "Default Website");
	shippingMap.put(1, "E-Ticket");
	shippingMap.put(2, "Will Call");
	shippingMap.put(3, "Local Pickup Near Venue");
	shippingMap.put(4, "E-Ticket or Will Call");
	shippingMap.put(5, "Will Call or Local Pickup Near Venue");
	shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
	shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
	shippingMap.put(8, "Paperless(Meet Seller at Venue)");
	shippingMap.put(9, "Electronic Transfer");

	Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
	nearTermMap.put(0, "Default near term options");
	nearTermMap.put(1, "Always show term shipping");
	nearTermMap.put(2, "Only show term shipping");
     
	
	try {
		OutputStream out = response.getOutputStream();
		out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
				.getBytes());

		List<VWLastRowMiniCatsCategoryTicket>  tixs=DAORegistry.getQueryManagerDAO().getTnLastRowMiniCatsCatsCategoryTickets(brokerId);
		
	/*	Collection<VWLastRowMiniCatsCategoryTicket> tixs = DAORegistry
				.getVwLastRowMiniCatsCategoryTicketDAO()
				.getAllActiveTnLastRowMiniCatsCatsCategoryTickets();*/

		//DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		//DateFormat tf = new SimpleDateFormat("hh:mm aa");
		// DateFormat dateTimeformat = new
		// SimpleDateFormat("MM/dd/yyyy HH:mm");

	    String dateInString=null;
	    String newDate=null;     
	    					
	    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	    
		for (VWLastRowMiniCatsCategoryTicket cat : tixs) {
			String outString = "";
			outString = cat.getEventName().replaceAll(",", "-") + ",";
			outString += cat.getEventDate() + ",";
			outString += cat.getEventTime() + ",";
			outString += cat.getVenueName().replaceAll(",", "-") + ",";
			outString += cat.getQuantity() + ",";
			// outString += cat.getZone() + ",";
			outString += cat.getSection() + ".,";
			outString += cat.getLastRow() + ".,";
			outString += cat.getTnPrice() + "\n";
			/*outString += cat.getExpectedArrivalDate() + ",";
			outString += shippingMap.get(cat.getShippingMethodId()) + ",";
			outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
					+ "\n";*/
			out.write(outString.getBytes());
		}

	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
		throw e;
	}
}
@RequestMapping(value="/AutopricingTicketDetails")
public ModelAndView loadAutoPricingTicketDetails(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

	ModelAndView mav = new ModelAndView("page-auto-pricing-ticket-details");
	try {
		Collection<Event> events = null;
		String application = request.getParameter("application");
		String artistIdString = request.getParameter("artistId");
		String venueIdString = request.getParameter("venueId");
		String eventIdString = request.getParameter("eventId");
		String section = request.getParameter("section");
		String row = request.getParameter("row");
		String quantity = request.getParameter("quantity");
		String showgrid="false";
		String selectedArtistName = request.getParameter("hidselectedArtistName");
		String selectedVenueName = request.getParameter("hidselectedVenueName");	
	
		Integer eventId;
		
		/*if(eventIdString!=null){
			Event event=DAORegistry.getEventDAO().get(Integer.parseInt(eventIdString));
		}*/
		
		//Tamil : select artist with minimum one valid events only 
		//List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtistsWithEvents();
		List<Artist> artists = new ArrayList<Artist>();
		//List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllArtistsBySqlQuery();
		
		//List<Venue> venues = (List<Venue>) DAORegistry.getVenueDAO().getAll();
		List<Venue> venues = new ArrayList<Venue>();
	
		/*Collections.sort(artists, new Comparator<Artist>() {
			public int compare(Artist t1, Artist t2) {
				AlphanumericSorting ans = new AlphanumericSorting();
				return ans.compare(t1.getName(), t2.getName());
			}
		});*/
	
		mav.addObject("artists", artists);
		mav.addObject("venues", venues);
		mav.addObject("selectedApplication", application);
		mav.addObject("selectedArtistName",selectedArtistName);
		mav.addObject("selectedVenueName",selectedVenueName);
		Integer artistId = null,venueId = null;
	
		if (null != artistIdString && !artistIdString.isEmpty()) {
			artistId = Integer.valueOf(artistIdString.trim());
			mav.addObject("selectedArtistId",artistId);
		    venues = (List<Venue>) DAORegistry.getVenueDAO().getVenuesForArtistWithActiveEvents(String.valueOf(artistId));
		    mav.addObject("venues", venues);
		}
		if (null != venueIdString && !venueIdString.isEmpty()) {
			venueId = Integer.valueOf(venueIdString.trim());
			mav.addObject("selectedVenueId",venueId);
		}
		if (null != artistId && null != venueId) {
			events = DAORegistry.getEventDAO().getAllEventsByArtistAndVenueAndStatus(artistId,venueId,EventStatus.ACTIVE);
		} else if(artistId != null) {
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		} else if(venueId != null) {
			events = DAORegistry.getEventDAO().getAllEventsByVenue(venueId);
		}
		mav.addObject("events", events);
	
		if (eventIdString == null || eventIdString.isEmpty()) {
			return mav;
		}
	
		
	
		if (eventIdString != null && eventIdString.trim().length() > 0) {
			showgrid="true";
			eventId = Integer.valueOf(eventIdString);
			mav.addObject("selectedEventId", eventId);			
			
			/*List<MiniCategoryTicket> categoryTicketsList = new ArrayList<MiniCategoryTicket>();
			categoryTicketsList = DAORegistry.getMiniCategoryTicketDAO().getAllTgCatsCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("categoryTickets", categoryTicketsList);*/
			
			/*List<AutoCatsCategoryTicket> autoCatsCategoryTicketsList = new ArrayList<AutoCatsCategoryTicket>();
			autoCatsCategoryTicketsList = DAORegistry.getAutoCatsCategoryTicketDAO().getAllAutoCatsCategoryTicketsByAll(eventId, section, row,quantity);
			mav.addObject("autoCatsCategoryTicketsList",autoCatsCategoryTicketsList);*/
			
			/*List<VipCatsCategoryTicket> vipCatsCategoryTicketsList = new ArrayList<VipCatsCategoryTicket>();
			try{
			vipCatsCategoryTicketsList = DAORegistry.getVipCatsCategoryTicketDAO().getAllVipCatsCategoryTicketsByAll(eventId,section, row,quantity);
			}catch(Exception e){
				e.printStackTrace();
			}
			mav.addObject("vipCatsCategoryTicketsList",vipCatsCategoryTicketsList);*/
			
			/*List<LastRowMiniCategoryTicket> lastRowMiniCategoryTicketList = new ArrayList<LastRowMiniCategoryTicket>();
			lastRowMiniCategoryTicketList = DAORegistry.getLastRowMiniCategoryTicketDAO().getAllLastRowMiniCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("lastRowMiniCategoryTicketList", lastRowMiniCategoryTicketList);*/
			
			List<LarryLastCategoryTicket> larryLastCategoryTicketList = new ArrayList<LarryLastCategoryTicket>();
			larryLastCategoryTicketList = DAORegistry.getLarryLastCategoryTicketDAO().getAllLarryLastCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("larryLastCategoryTicketList", larryLastCategoryTicketList);
			
						
			/*List<ZonesPricingCategoryTicket> zonesPricingCategoryTicketList = new ArrayList<ZonesPricingCategoryTicket>();
			zonesPricingCategoryTicketList = DAORegistry.getZonesPricingCategoryTicketDAO().getAllZonePricingCategoryTicketsByAll(eventId, section, row, quantity);
			mav.addObject("zonesPricingCategoryTicketList", zonesPricingCategoryTicketList);
			    
			List<TixCityZonesPricingCategoryTicket> tixcityZonesPricingCategoryTicketList = new ArrayList<TixCityZonesPricingCategoryTicket>();
			tixcityZonesPricingCategoryTicketList = DAORegistry.getTixCityZonesPricingCategoryTicketDAO().getAllTixCityZonePricingCategoryTicketsByAll(eventId, section, row, quantity);
			mav.addObject("tixCityZonesPricingCategoryTicketList", tixcityZonesPricingCategoryTicketList);*/
			
			/*List<ZonedLastRowMiniCategoryTicket> zonedLastRowMiniCategoryTicketList = new ArrayList<ZonedLastRowMiniCategoryTicket>();
			zonedLastRowMiniCategoryTicketList = DAORegistry.getZonedLastRowMiniCategoryTicketDAO().getAllZonedLastRowMiniCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("zonedLastRowMiniCategoryTicketList", zonedLastRowMiniCategoryTicketList);*/
			
			List<ZoneTicketsProcessorCategoryTicket> zoneticketsProcesorTickets = new ArrayList<ZoneTicketsProcessorCategoryTicket>();
			zoneticketsProcesorTickets = DAORegistry.getZoneTicketsProcessorCategoryTicketDAO().getAllTgCatsCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("zoneTicketsProcessorCategoryTicketList", zoneticketsProcesorTickets);
			
			/*List<PresaleZoneTicketsCategoryTicket> presaleZoneTickets = new ArrayList<PresaleZoneTicketsCategoryTicket>();
			presaleZoneTickets = DAORegistry.getPresaleZoneTicketsCategoryTicketDAO().getAllTgCatsCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("presaleZoneTicketsCategoryTicketList", presaleZoneTickets);*/
			
			/*List<PresaleAutoCatCategoryTicket> presaleAutoCatTickets = new ArrayList<PresaleAutoCatCategoryTicket>();
			presaleAutoCatTickets = DAORegistry.getPresaleAutoCatCategoryTicketDAO().getAllPresaleAutoCatsCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("presaleAutoCatCategoryTicketList", presaleAutoCatTickets);*/
			
			List<AutoCats96CategoryTicket> above96CatsCategoryTicketTickets = new ArrayList<AutoCats96CategoryTicket>();
			above96CatsCategoryTicketTickets = DAORegistry.getAutoCats96CategoryTicketDAO().getAllAutoCats96CatsCategoryTicketsByAll(eventId,"Above96Cats",section, row,quantity);
			mav.addObject("above96CatsCategoryTicketTickets", above96CatsCategoryTicketTickets);
			
			/*List<ManhattanZonesPricingCategoryTicket> manhattanZonesPricingCategoryTicketList = new ArrayList<ManhattanZonesPricingCategoryTicket>();
			manhattanZonesPricingCategoryTicketList = DAORegistry.getManhattanZonesPricingCategoryTicketDAO().getAllManhattanZonePricingCategoryTicketsByAll(eventId, section, row, quantity);
			mav.addObject("manhattanZonesPricingCategoryTicketList", manhattanZonesPricingCategoryTicketList);
			
			List<VipLastRowMiniCategoryTicket> vipLastRowMiniCategoryTicketList = new ArrayList<VipLastRowMiniCategoryTicket>();
			vipLastRowMiniCategoryTicketList = DAORegistry.getVipLastRowMiniCategoryTicketDAO().getAllVipLastRowMiniCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("vipLastRowMiniCategoryTicketList", vipLastRowMiniCategoryTicketList);*/
			
			/*List<SGLastFiveRowCategoryTicket> sgLastFiveRowCategoryTicketList = new ArrayList<SGLastFiveRowCategoryTicket>();
			sgLastFiveRowCategoryTicketList = DAORegistry.getSgLastFiveRowCategoryTicketDAO().getAllSGLastFiveRowCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("sgLastFiveRowCategoryTicketList", sgLastFiveRowCategoryTicketList);*/
			
			/*List<AutoCats96CategoryTicket> below96CatsCategoryTicketTickets = new ArrayList<AutoCats96CategoryTicket>();
			below96CatsCategoryTicketTickets = DAORegistry.getAutoCats96CategoryTicketDAO().getAllAutoCats96CatsCategoryTicketsByAll(eventId,"Below96Cats",section, row,quantity);
			mav.addObject("below96CatsCategoryTicketTickets", below96CatsCategoryTicketTickets);*/
			
			/*List<LastFiveRowMiniCategoryTicket> lastFiveRowMiniCategoryTicketList = new ArrayList<LastFiveRowMiniCategoryTicket>();
			lastFiveRowMiniCategoryTicketList = DAORegistry.getLastFiveRowMiniCategoryTicketDAO().getAllLastFiveRowMiniCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("lastFiveRowMiniCategoryTicketList", lastFiveRowMiniCategoryTicketList);
			*/
			
			/*List<VipAutoCategoryTicket> vipAutoCategoryTicketList = new ArrayList<VipAutoCategoryTicket>();
			vipAutoCategoryTicketList = DAORegistry.getVipAutoCategoryTicketDAO().getAllVipAutoCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("vipAutoCategoryTicketList", vipAutoCategoryTicketList);
			*/
		}
	
		mav.addObject("section", section);
		mav.addObject("row",row);
		mav.addObject("quantity", quantity);
		mav.addObject("showgrid",showgrid);
	}catch (Exception e) {
		e.printStackTrace();
	}
	return mav;
}
@RequestMapping(value="/CategoryPriceHistoryPopup")
public ModelAndView loadCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	MiniCategoryTicket categoryTicket = DAORegistry.getMiniCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<MiniCategoryTicket> ticketPriceList = new ArrayList<MiniCategoryTicket>();
	
	if(categoryTicket != null) {
		
		String pricehistory = categoryTicket.getPriceHistory();
		MiniCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String price = str[3];
				ticketPriceObj = new MiniCategoryTicket();
				
//				Not sure what is the use of this.. : CS
				ticketPriceObj.setPopDate(date);
				ticketPriceObj.setPopPrice(price);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new MiniCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}

@RequestMapping(value="/SGLastFiveRowCategoryPriceHistoryPopup")
public ModelAndView SGLastFiveRowCategoryPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-sg-lastfiverow-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	SGLastFiveRowCategoryTicket categoryTicket = DAORegistry.getSgLastFiveRowCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<SGLastFiveRowCategoryTicket> ticketPriceList = new ArrayList<SGLastFiveRowCategoryTicket>();
	
	if(categoryTicket != null) {
		
		String pricehistory = categoryTicket.getPriceHistory();
		SGLastFiveRowCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String price = str[3];
				ticketPriceObj = new SGLastFiveRowCategoryTicket();
				
//				Not sure what is the use of this.. : CS
				ticketPriceObj.setPopDate(date);
				ticketPriceObj.setPopPrice(price);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new MiniCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}

@RequestMapping(value="/SGLastFiveRowTMATTicketPriceHistoryPopup")
public ModelAndView SGLastFiveRowTMATTicketPriceHistory(HttpServletRequest request,
  HttpServletResponse response) throws ServletException, IOException {
 ModelAndView mav = new ModelAndView("page-sg-lastfiverow-tmat-ticket-price-history-popup");
 
 String eventId = request.getParameter("eId");
 //String ticketId = request.getParameter("ticketId");
 String tgcatId = request.getParameter("tgid");
 String ticketType = request.getParameter("ticketType");
 Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
 
 
 
 SGLastFiveRowCategoryTicket tgcatTicket = DAORegistry.getSgLastFiveRowCategoryTicketDAO().get(Integer.parseInt(tgcatId));
 List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
 DateTimePop ticketTicObj = null;
 String ticStr[]=null;
 
 if(tgcatTicket != null) {
  String temphistoryStr = null;
  if(ticketType.equalsIgnoreCase("ticketId")) {
   temphistoryStr = tgcatTicket.getTicketIdHistory();
   
  } else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
   
   if(tgcatTicket.getBaseTicketOneHistory() != null) {
    temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
   }
   
  } else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
   if(tgcatTicket.getBaseTicketTwoHistory() != null) {
    temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
   }
  } else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
   if(tgcatTicket.getBaseTicketThreeHistory() != null) {
    temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
   }
  }
  
  if(temphistoryStr != null) {
      String array[] = temphistoryStr.split(",");

      for (String string : array) {
       ticStr = string.split("/");
       String ate=ticStr[0];
       String pur = ticStr[2];
       Integer ticId = Integer.parseInt(ticStr[1]);
       
       ticketTicObj = new DateTimePop();
       ticketTicObj.settPopTicDate(ate);
    ticketTicObj.settPopTicPur(pur);
    
       if(ticId.equals(0)) {
        tickethistoryList.add(ticketTicObj);
        continue;
       }
    Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
    if(ticketInfo == null) {
     HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
     if(historicalTicket != null) {
      ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
      ticketTicObj.settPopTicSection(historicalTicket.getSection());
      ticketTicObj.settPopTicRow(historicalTicket.getRow());
      ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
      ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
      ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
      ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
      ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
      ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
     }
     tickethistoryList.add(ticketTicObj);
     
    } else {
     
     ticketTicObj.settPopTicDate(ate);
     ticketTicObj.settPopTicPur(pur);
     ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
     ticketTicObj.settPopTicSection(ticketInfo.getSection());
     ticketTicObj.settPopTicRow(ticketInfo.getRow());
     ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
     ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
     ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
     ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
     ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
     ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
     tickethistoryList.add(ticketTicObj);
    }
      }
     }
 }
 
 mav.addObject("tmatTicketList",tickethistoryList);
 //mav.addObject("CategoryTicketlist", CategoryTicketlist);
 //mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
 mav.addObject("event", event);
 mav.addObject("ticketType", ticketType);
 
 return mav;
}

@RequestMapping(value="/PresaleAutoCatsCatPriceHistoryPopup")
public ModelAndView loadPresaleAutoCatsCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-presale-auto-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	PresaleAutoCatCategoryTicket categoryTicket = DAORegistry.getPresaleAutoCatCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<PresaleAutoCatCategoryTicket> ticketPriceList = new ArrayList<PresaleAutoCatCategoryTicket>();
	
	if(categoryTicket != null) {
		
		String pricehistory = categoryTicket.getPriceHistory();
		PresaleAutoCatCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String price = str[3];
				ticketPriceObj = new PresaleAutoCatCategoryTicket();
				
//				Not sure what is the use of this.. : CS
				ticketPriceObj.setPopDate(date);
				ticketPriceObj.setPopPrice(price);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new PresaleAutoCatCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}

@RequestMapping(value="/TMATTicketPriceHistoryPopup")
public ModelAndView loadTMATTicketPriceHistory(HttpServletRequest request,
  HttpServletResponse response) throws ServletException, IOException {
 ModelAndView mav = new ModelAndView("page-tmat-ticket-price-history-popup");
 
 String eventId = request.getParameter("eId");
 //String ticketId = request.getParameter("ticketId");
 String tgcatId = request.getParameter("tgid");
 String ticketType = request.getParameter("ticketType");
 Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
 
 
 
 MiniCategoryTicket tgcatTicket = DAORegistry.getMiniCategoryTicketDAO().get(Integer.parseInt(tgcatId));
 List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
 DateTimePop ticketTicObj = null;
 String ticStr[]=null;
 
 if(tgcatTicket != null) {
  String temphistoryStr = null;
  if(ticketType.equalsIgnoreCase("ticketId")) {
   temphistoryStr = tgcatTicket.getTicketIdHistory();
   
  } else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
   
   if(tgcatTicket.getBaseTicketOneHistory() != null) {
    temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
   }
   
  } else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
   if(tgcatTicket.getBaseTicketTwoHistory() != null) {
    temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
   }
  } else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
   if(tgcatTicket.getBaseTicketThreeHistory() != null) {
    temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
   }
  }
  
  if(temphistoryStr != null) {
      String array[] = temphistoryStr.split(",");

      for (String string : array) {
       ticStr = string.split("/");
       String ate=ticStr[0];
       String pur = ticStr[2];
       Integer ticId = Integer.parseInt(ticStr[1]);
       
       ticketTicObj = new DateTimePop();
       ticketTicObj.settPopTicDate(ate);
    ticketTicObj.settPopTicPur(pur);
    
       if(ticId.equals(0)) {
        tickethistoryList.add(ticketTicObj);
        continue;
       }
    Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
    if(ticketInfo == null) {
     HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
     if(historicalTicket != null) {
      ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
      ticketTicObj.settPopTicSection(historicalTicket.getSection());
      ticketTicObj.settPopTicRow(historicalTicket.getRow());
      ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
      ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
      ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
      ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
      ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
      ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
     }
     tickethistoryList.add(ticketTicObj);
     
    } else {
     
     ticketTicObj.settPopTicDate(ate);
     ticketTicObj.settPopTicPur(pur);
     ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
     ticketTicObj.settPopTicSection(ticketInfo.getSection());
     ticketTicObj.settPopTicRow(ticketInfo.getRow());
     ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
     ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
     ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
     ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
     ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
     ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
     tickethistoryList.add(ticketTicObj);
    }
      }
     }
 }
 
 mav.addObject("tmatTicketList",tickethistoryList);
 //mav.addObject("CategoryTicketlist", CategoryTicketlist);
 //mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
 mav.addObject("event", event);
 mav.addObject("ticketType", ticketType);
 
 return mav;
}


@RequestMapping(value="/PresaleAutoCatsTMATTicketPriceHistoryPopup")
public ModelAndView loadPresaleAutoCatsTMATTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-presale-auto-tmat-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	PresaleAutoCatCategoryTicket presaleautoCatTicket = DAORegistry.getPresaleAutoCatCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(presaleautoCatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = presaleautoCatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(presaleautoCatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = presaleautoCatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(presaleautoCatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = presaleautoCatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(presaleautoCatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = presaleautoCatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/AutoTMATTicketPriceHistoryPopup")
public ModelAndView loadAutoTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-auto-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	AutoCatsCategoryTicket categoryTicket = DAORegistry.getAutoCatsCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(categoryTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = categoryTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(categoryTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = categoryTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(categoryTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = categoryTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(categoryTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = categoryTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/VipCategoryPriceHistoryPopup")
public ModelAndView loadVipCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-vip-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	VipCatsCategoryTicket categoryTicket = DAORegistry.getVipCatsCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<VipCatsCategoryTicket> ticketPriceList = new ArrayList<VipCatsCategoryTicket>();
	
	if(categoryTicket != null) {
	
		String pricehistory = categoryTicket.getPriceHistory();
		VipCatsCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			System.out.println("pricehistory :" + pricehistory);
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String rice = str[3];
				System.out.println("Date  : " + ate);
				System.out.println("Price :" + rice);
				ticketPriceObj = new VipCatsCategoryTicket();
				ticketPriceObj.setPopDate(ate);
				ticketPriceObj.setPopPrice(rice);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new VipCatsCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,
					(categoryTicket.getLastUpdated().toString()).indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket.getActualPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}
@RequestMapping(value="/VipTMATTicketPriceHistoryPopup")
public ModelAndView loadVipTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-vip-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	VipCatsCategoryTicket tgcatTicket = DAORegistry.getVipCatsCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
/*public ModelAndView loadLastfiverowCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-lastfiverow-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	LastFiveRowMiniCategoryTicket categoryTicket = DAORegistry.getLastFiveRowMiniCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<LastFiveRowMiniCategoryTicket> ticketPriceList = new ArrayList<LastFiveRowMiniCategoryTicket>();

	if(categoryTicket != null) {
	
		String pricehistory = categoryTicket.getPriceHistory();
		LastFiveRowMiniCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String rice = str[3];
				ticketPriceObj = new LastFiveRowMiniCategoryTicket();
				ticketPriceObj.setPopDate(ate);
				ticketPriceObj.setPopPrice(rice);
				ticketPriceList.add(ticketPriceObj);
			}
		} else {
			ticketPriceObj = new LastFiveRowMiniCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket
					.getLastUpdated().toString()).substring(0,
					(categoryTicket.getLastUpdated().toString())
							.indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket
					.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}


public ModelAndView loadLastfiverowTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-lastfiverow-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	LastFiveRowMiniCategoryTicket tgcatTicket = DAORegistry.getLastFiveRowMiniCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}*/

@RequestMapping(value="/LastRowCategoryPriceHistoryPopup")
public ModelAndView loadLastRowCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView(
			"page-lastrow-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	System.out.println("eventId :" + eventId);
	String tmatId = request.getParameter("tId");
	System.out.println("tmatid :" + tmatId);
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	LastRowMiniCategoryTicket categoryTicket = DAORegistry.getLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<LastRowMiniCategoryTicket> ticketPriceList = new ArrayList<LastRowMiniCategoryTicket>();
	
	if(categoryTicket != null) {
		String pricehistory = categoryTicket.getPriceHistory();
		LastRowMiniCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String rice = str[3];
				ticketPriceObj = new LastRowMiniCategoryTicket();
				ticketPriceObj.setPopDate(ate);
				ticketPriceObj.setPopPrice(rice);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new LastRowMiniCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket
					.getLastUpdated().toString()).substring(0,
					(categoryTicket.getLastUpdated().toString())
							.indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket
					. getActualPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}
@RequestMapping(value="/LastRowTMATTicketPriceHistoryPopup")
public ModelAndView loadLastRowTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-lastrow-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	LastRowMiniCategoryTicket tgcatTicket = DAORegistry.getLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
			    Ticket ticketInfo=null;
			    try{
				 ticketInfo = DAORegistry.getTicketDAO().get(ticId);
			    }catch(Exception e){
			    	e.printStackTrace();
			    }
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/LarryLastCategoryPriceHistoryPopup")
public ModelAndView loadLarryLastCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-larrylast-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	LarryLastCategoryTicket categoryTicket = DAORegistry.getLarryLastCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<LarryLastCategoryTicket> ticketPriceList = new ArrayList<LarryLastCategoryTicket>();
	
	if(categoryTicket != null) {
		
		String pricehistory = categoryTicket.getPriceHistory();
		LarryLastCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String rice = str[3];
				ticketPriceObj = new LarryLastCategoryTicket();
				ticketPriceObj.setPopDate(ate);
				ticketPriceObj.setPopPrice(rice);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new LarryLastCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket
					.getLastUpdated().toString()).substring(0,
					(categoryTicket.getLastUpdated().toString())
							.indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket
					. getActualPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}

@RequestMapping(value="/LarryLastTMATTicketPriceHistoryPopup")
public ModelAndView loadLarryLastPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-larrylast-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	LarryLastCategoryTicket tgcatTicket = DAORegistry.getLarryLastCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/VipAutoCategoryPriceHistoryPopup")
public ModelAndView loadVipAutoCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-vipauto-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	VipAutoCategoryTicket categoryTicket = DAORegistry.getVipAutoCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<VipAutoCategoryTicket> ticketPriceList = new ArrayList<VipAutoCategoryTicket>();

	if(categoryTicket != null) {
		String pricehistory = categoryTicket.getPriceHistory();
		VipAutoCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String rice = str[3];
				System.out.println("Date  : " + ate);
				System.out.println("Price :" + rice);
				ticketPriceObj = new VipAutoCategoryTicket();
				ticketPriceObj.setPopDate(ate);
				ticketPriceObj.setPopPrice(rice);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new VipAutoCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket
					.getLastUpdated().toString()).substring(0,
					(categoryTicket.getLastUpdated().toString())
							.indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket
					.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}

@RequestMapping(value="/VipAutoTMATTicketPriceHistoryPopup")
public ModelAndView loadVipAutoTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-vipauto-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	VipAutoCategoryTicket tgcatTicket = DAORegistry.getVipAutoCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/ZonedLastrowMiniCatPriceHistoryPopup")
public ModelAndView loadZonedLastrowCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView(
			"page-zonedlastrow-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	ZonedLastRowMiniCategoryTicket categoryTicket = DAORegistry.getZonedLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<ZonedLastRowMiniCategoryTicket> ticketPriceList = new ArrayList<ZonedLastRowMiniCategoryTicket>();
	
	if(categoryTicket != null) {
		
		String pricehistory = categoryTicket.getPriceHistory();
		ZonedLastRowMiniCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String price = str[3];
				ticketPriceObj = new ZonedLastRowMiniCategoryTicket();
				ticketPriceObj.setPopDate(date);
				ticketPriceObj.setPopPrice(price);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new MiniCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}

@RequestMapping(value="/ZonedLastrowMiniTMATTicketPriceHistoryPopup")
public ModelAndView loadZonedLastrowTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-zonedlastrow-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	ZonedLastRowMiniCategoryTicket tgcatTicket = DAORegistry.getZonedLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/ZonesPricingCatPriceHistoryPopup")
public ModelAndView loadZonesPricingCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView(
			"page-zonespricing-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	ZonesPricingCategoryTicket categoryTicket = DAORegistry.getZonesPricingCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<ZonesPricingCategoryTicket> ticketPriceList = new ArrayList<ZonesPricingCategoryTicket>();
	
	if(categoryTicket != null) {
		
		String pricehistory = categoryTicket.getPriceHistory();
		ZonesPricingCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String price = str[3];
				ticketPriceObj = new ZonesPricingCategoryTicket();
				ticketPriceObj.setPopDate(date);
				ticketPriceObj.setPopPrice(price);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new MiniCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}
@RequestMapping(value="/TixCityZonesPricingCatPriceHistoryPopup")
public ModelAndView loadTixCityZonesPricingCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView(
			"page-tixcity-zonespricing-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	TixCityZonesPricingCategoryTicket categoryTicket = DAORegistry.getTixCityZonesPricingCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<TixCityZonesPricingCategoryTicket> ticketPriceList = new ArrayList<TixCityZonesPricingCategoryTicket>();
	
	if(categoryTicket != null) {
		
		String pricehistory = categoryTicket.getPriceHistory();
		TixCityZonesPricingCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String price = str[3];
				ticketPriceObj = new TixCityZonesPricingCategoryTicket();
				ticketPriceObj.setPopDate(date);
				ticketPriceObj.setPopPrice(price);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new MiniCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}
@RequestMapping(value="/TixCityZonesPricingTMATTicketPriceHistoryPopup")
public ModelAndView loadTixCityZonesPricingTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-tixcity-zonespricing-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	TixCityZonesPricingCategoryTicket tgcatTicket = DAORegistry.getTixCityZonesPricingCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/ZoneTicketsCatPriceHistoryPopup")
public ModelAndView loadZoneTicketsCategoryTicketPriceHistory(
		HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	ModelAndView mav = new ModelAndView(
			"page-zonetickets-category-ticket-price-history-popup");
	String eventId = request.getParameter("eId");
	String tmatId = request.getParameter("tId");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	ZoneTicketsProcessorCategoryTicket categoryTicket = DAORegistry.getZoneTicketsProcessorCategoryTicketDAO().get(Integer.parseInt(tmatId));
	List<ZoneTicketsProcessorCategoryTicket> ticketPriceList = new ArrayList<ZoneTicketsProcessorCategoryTicket>();
	
	if(categoryTicket != null) {
		
		String pricehistory = categoryTicket.getPriceHistory();
		ZoneTicketsProcessorCategoryTicket ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			String array[] = pricehistory.split(",");

			for (String string : array) {
				str = string.split("-");
				String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
				String price = str[3];
				ticketPriceObj = new ZoneTicketsProcessorCategoryTicket();
				ticketPriceObj.setPopDate(date);
				ticketPriceObj.setPopPrice(price);
				ticketPriceList.add(ticketPriceObj);
			}
		} /*else {
			ticketPriceObj = new MiniCategoryTicket();
			ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
			ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}*/
	}
	mav.addObject("ticketPriceList", ticketPriceList);
	mav.addObject("categoryTicket", categoryTicket);
	mav.addObject("event", event);
	return mav;
}	
@RequestMapping(value="/ZonesPricingTMATTicketPriceHistoryPopup")
public ModelAndView loadZonesPricingTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-zonespricing-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	ZonesPricingCategoryTicket tgcatTicket = DAORegistry.getZonesPricingCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/ZoneTicketsTMATTicketPriceHistoryPopup")
public ModelAndView loadZoneTicketsTicketPriceHistory(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-zonetickets-ticket-price-history-popup");
	
	String eventId = request.getParameter("eId");
	//String ticketId = request.getParameter("ticketId");
	String tgcatId = request.getParameter("tgid");
	String ticketType = request.getParameter("ticketType");
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	
	
	
	ZoneTicketsProcessorCategoryTicket tgcatTicket = DAORegistry.getZoneTicketsProcessorCategoryTicketDAO().get(Integer.parseInt(tgcatId));
	List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
	DateTimePop ticketTicObj = null;
	String ticStr[]=null;
	
	if(tgcatTicket != null) {
		String temphistoryStr = null;
		if(ticketType.equalsIgnoreCase("ticketId")) {
			temphistoryStr = tgcatTicket.getTicketIdHistory();
			
		} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
			
			if(tgcatTicket.getBaseTicketOneHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
			}
			
		} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
			if(tgcatTicket.getBaseTicketTwoHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
			}
		} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
			if(tgcatTicket.getBaseTicketThreeHistory() != null) {
				temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
			}
		}
		
		if(temphistoryStr != null) {
		    String array[] = temphistoryStr.split(",");

		    for (String string : array) {
			    ticStr = string.split("/");
			    String ate=ticStr[0];
			    String pur = ticStr[2];
			    Integer ticId = Integer.parseInt(ticStr[1]);
			    
			    ticketTicObj = new DateTimePop();
		    	ticketTicObj.settPopTicDate(ate);
				ticketTicObj.settPopTicPur(pur);
				
			    if(ticId.equals(0)) {
			    	tickethistoryList.add(ticketTicObj);
			    	continue;
			    }
				Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				if(ticketInfo == null) {
					HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
					if(historicalTicket != null) {
						ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(historicalTicket.getSection());
						ticketTicObj.settPopTicRow(historicalTicket.getRow());
						ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
						ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
						ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
					}
					tickethistoryList.add(ticketTicObj);
					
				} else {
					
					ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
					ticketTicObj.settPopTicSection(ticketInfo.getSection());
					ticketTicObj.settPopTicRow(ticketInfo.getRow());
					ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
					ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
					ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
					ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
					ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
					ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
					tickethistoryList.add(ticketTicObj);
				}
		    }
	    }
	}
	
	mav.addObject("tmatTicketList",tickethistoryList);
	//mav.addObject("CategoryTicketlist", CategoryTicketlist);
	//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
	mav.addObject("event", event);
	mav.addObject("ticketType", ticketType);
	
	return mav;
}
@RequestMapping(value="/AutocatProjectAudits")
public  ModelAndView autoPricingProductsAudit(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws JSONException, ParseException{
	      ModelAndView mav=new ModelAndView("page-load-autocat-project-audits");  
	try {
		
		Collection<Broker> brokerList = DAORegistry.getBrokerDAO().getAllActiveBrokers();
		Collection<AutopricingProduct> autopricingProductList = DAORegistry.getAutopricingProductDAO().getAllActiveProducts();
		
		String[] selectedBrokerIdStr = request.getParameterValues("brokerId");
		String[] selectedProductIdStr = request.getParameterValues("productId");
		String isAllBroker = request.getParameter("isAllBroker");
		String isAllProduct = request.getParameter("isAllProduct");
		
		String fromDateString = request.getParameter("fromDate");
		String toDateString = request.getParameter("toDate");
		
		String fromHour = request.getParameter("fromHour");
		String toHour = request.getParameter("toHour");
		
		String fromMinute = request.getParameter("fromMinute");
		String toMinute = request.getParameter("toMinute");
		
		Date fromDate = null;
		Date toDate = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy h:m");
		if(fromDateString!= null){
			if(fromHour!=null && !fromHour.isEmpty()){
				if(fromMinute!=null  && !fromMinute.isEmpty()){
					fromDate = df.parse(fromDateString +  " " + fromHour + ":" + fromMinute);
				}else{
					fromDate = df.parse(fromDateString +  " " + fromHour + ":00");
				}
			}else{
				fromDate = df.parse(fromDateString + " 00:00");
			}
		}
		
		
		if(toDateString!= null){
			if(toHour!=null  && !toHour.isEmpty()){
				if(toMinute!=null  && !toMinute.isEmpty()){
					toDate = df.parse(toDateString +  " " + toHour + ":" + toMinute);
				}else{
					toDate = df.parse(toDateString +  " " + toHour + ":00");
				}
			}else{
				toDate = df.parse(toDateString + " 23:59");
			}
		}
		
		List<Integer> selectedBrokerId = new ArrayList<Integer>();
		List<Integer> selectedProductId = new ArrayList<Integer>();
		
		if (selectedBrokerIdStr != null && selectedBrokerIdStr.length != 0) {
			for (String brokerIdStr : selectedBrokerIdStr) {
				if (brokerIdStr.isEmpty()) {
					continue;
				}
				selectedBrokerId.add(Integer.parseInt(brokerIdStr));
			}
		}
		if (selectedProductIdStr != null && selectedProductIdStr.length != 0) {
			for (String productIdStr : selectedProductIdStr) {
				if (productIdStr.isEmpty()) {
					continue;
				}
				selectedProductId.add(Integer.parseInt(productIdStr));
			}
		}
		List<AutoCatsProjectAudit> autoCatsProjectAuditList = new ArrayList<AutoCatsProjectAudit>();
		for(Integer productId:selectedProductId){
			for(Integer brokerId:selectedBrokerId){
				Collection<AutoCatsProjectAudit> autoCatsProjectAuditFromDB = DAORegistry.getAutoCatsProjectAuditDAO().getAllAutoCatsProjectAuditsByProductBrokerAndDateRange(productId, brokerId, fromDate, toDate);
				if(autoCatsProjectAuditFromDB!=null && !autoCatsProjectAuditFromDB.isEmpty()){
					autoCatsProjectAuditList.addAll(autoCatsProjectAuditFromDB);			
				}
			}
		}
		mav.addObject("autoCatsProjectAuditList",autoCatsProjectAuditList);
			
		
		mav.addObject("isAllBrokerSelected", isAllBroker);
		mav.addObject("isAllProductSelected", isAllProduct);
		mav.addObject("selectedBrokerId", selectedBrokerIdStr);
		mav.addObject("selectedProductId", selectedProductIdStr);
		mav.addObject("brokerList", brokerList);
		mav.addObject("productList", autopricingProductList);
		mav.addObject("fromDate", fromDateString);
		mav.addObject("toDate", toDateString);
		mav.addObject("fromHour", fromHour);
		mav.addObject("toHour", toHour);
		mav.addObject("fromMinute", fromMinute);
		mav.addObject("toMinute", toMinute);
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	return mav;
}
@RequestMapping(value="/DownloadAutocatProjectAudits")
public void downloadAutoCatsProjectAudit(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws JSONException, ParseException {
	DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	String today = dateFormat.format(new Date());
	
	
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoCatsProjectAudit." + today + ".csv");
	

	try {
	
		OutputStream out = response.getOutputStream();
		out.write("Id,ProductId,BrokerId,Count,LastRunTime\n"
				.getBytes());
		
		
	    List<AutoCatsProjectAudit> audits =DAORegistry.getAutoCatsProjectAuditDAO().getAllAutoCatsProjectAudits();

		for (AutoCatsProjectAudit audit : audits) {
			
			String outString = "";
			outString = audit.getId() + ",";
			outString += audit.getProduct().getName() + ",";
			outString += audit.getBroker().getName() + ",";
			outString += audit.getCount() + ",";
			outString += audit.getLastRunTime() + "\n";
			
			out.write(outString.getBytes());
		}
	} catch (Exception e) {
		System.out.println(e.fillInStackTrace());
	}
}
	@RequestMapping(value="/ManageAutoPricingBroker")
	public ModelAndView getManageAutoPricingBroker(HttpServletRequest request,Model model,HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("page-manage-autopricing-broker");
		try {
			Collection<Broker> brokerList = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			mav.addObject("brokerList", brokerList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	@RequestMapping(value="/ManualAutoPricingFileGeneration")
	public ModelAndView geManualAutopricingFileGeneration(HttpServletRequest request,Model model,HttpServletResponse response){
		ModelAndView mav = new ModelAndView("page-manual-autopricing-file-generation");

		String action = request.getParameter("action");
		String info = "";
		if(action != null && action.equals("update")) {
			AutoPricingFileGenerator generator = new AutoPricingFileGenerator();
			generator.generateAutoPricingFiles();
			
			info = "AutoPricing Files are generated successfully.";
		}
		mav.addObject("info",info);
		
		return mav;
	}
	@RequestMapping(value="/EditAutoPricingBroker")
	public ModelAndView getEditAutopricingBroker(HttpServletRequest request,Model model,HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("page-edit-autopricing-broker");
		try {
			String brokerId = request.getParameter("brokerId");
			Broker broker = DAORegistry.getBrokerDAO().get(Integer.parseInt(brokerId));
			
			String info = "";
			String action = request.getParameter("action");
			if(action != null && action.equals("update")) {
				String username = SecurityContextHolder.getContext().getAuthentication().getName();
				
				broker.setEmailId(request.getParameter("emailId"));
				broker.setEmailSuffix(request.getParameter("emailSuffix"));
				broker.setPhoneNo(request.getParameter("phoneNo"));
				broker.setPosBrokerId(Integer.parseInt(request.getParameter("posBrokerId")));
				broker.setSystemUserId(Integer.parseInt(request.getParameter("systemUserId")));
				broker.setBuyerBrokerId(Integer.parseInt(request.getParameter("buyerBrokerId")));
				broker.setClientBrokerId(Integer.parseInt(request.getParameter("clientBrokerId")));
				broker.setClientBrokerEmployeeId(Integer.parseInt(request.getParameter("clientBrokerEmployeeId")));
				broker.setMinutePOSUpdateLimit(Integer.parseInt(request.getParameter("minutePOSUpdateLimit")));
				
				DAORegistry.getBrokerDAO().update(broker);
				BrokerUtils.removeBrokerById(broker.getId());//Removing broker from cache. 
				
				BrokerAudit audit = new BrokerAudit(broker);
				audit.setLastUpdatedBy(username);
				audit.setAction("Autopricing Broker Update");
				audit.setLastUpdated(new Date());
				DAORegistry.getBrokerAuditDAO().saveOrUpdate(audit);
				
				
				
				info = "Autopricing Broker Updated Successfully.";
			}
			
			mav.addObject("info", info);
			mav.addObject("broker", broker);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}



	@RequestMapping(value="/AutoCats96CatPriceHistoryPopup")
	public ModelAndView loadAutoCats96CategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-autocats96-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		AutoCats96CategoryTicket categoryTicket = DAORegistry.getAutoCats96CategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<AutoCats96CategoryTicket> ticketPriceList = new ArrayList<AutoCats96CategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			AutoCats96CategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");

				for (String string : array) {
					str = string.split("-");
					String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String price = str[3];
					ticketPriceObj = new AutoCats96CategoryTicket();
					ticketPriceObj.setPopDate(date);
					ticketPriceObj.setPopPrice(price);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new MiniCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}	

	@RequestMapping(value="/AutoCats96TMATTicketPriceHistoryPopup")
	public ModelAndView loadAutoCats96TicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-autocats96-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		AutoCats96CategoryTicket tgcatTicket = DAORegistry.getAutoCats96CategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	
	
	@RequestMapping(value="/ExcludeEventsBasedOnVenues")	
	public ModelAndView getExcludeEventsBasedOnVenues(HttpServletRequest request,Model model,HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("page-event-exclude-management");
		try {
			
			Collection<Broker> brokerList = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokersWithMZTixBroker();
			Collection<AutopricingProduct> autopricingProductList = DAORegistry.getAutopricingProductDAO().getAllActiveProducts();
			
			String[] selectedBrokerIdStr = request.getParameterValues("brokerId");
			String[] selectedProductIdStr = request.getParameterValues("productId");
			String venueId=request.getParameter("venueId");
			String isAllBroker = request.getParameter("isAllBroker");
			String isAllProduct = request.getParameter("isAllProduct");
			String selectedValue="";
			String selectedOption="";
			List<Integer> selectedBrokerId = new ArrayList<Integer>();
			List<Integer> selectedProductId = new ArrayList<Integer>();
			
			if(venueId!=null && venueId!=""){
				Venue venue=DAORegistry.getVenueDAO().get(Integer.parseInt(venueId));
				selectedOption="Venue";
				selectedValue= venue.getBuilding()+" "+venue.getCity()+" "+venue.getState()+" "+venue.getCountry();
			}
			if (selectedBrokerIdStr != null && selectedBrokerIdStr.length != 0) {
				for (String brokerIdStr : selectedBrokerIdStr) {
					if (brokerIdStr.isEmpty()) {
						continue;
					}
					selectedBrokerId.add(Integer.parseInt(brokerIdStr));
				}
			}
			if (selectedProductIdStr != null && selectedProductIdStr.length != 0) {
				for (String productIdStr : selectedProductIdStr) {
					if (productIdStr.isEmpty()) {
						continue;
					}
					selectedProductId.add(Integer.parseInt(productIdStr));
				}
			}
			String action = request.getParameter("action");
			String info = "";
			
			List<ExcludeVenuesAudit> excludeVenuesAuditList = new ArrayList<ExcludeVenuesAudit>();
			if(action != null) {
	            if (action.equals("save")) {
	            	List<ExcludeVenues> excludeVenuesList = new ArrayList<ExcludeVenues>();
	            	List<ExcludeVenues> excludeVenuesDeleteList = new ArrayList<ExcludeVenues>();
	            	Collection<ExcludeVenues> excludeVenuesFromDB=DAORegistry.getExcludeVenuesDAO().getAll();
					Map<String,ExcludeVenues> excludeVenuesMapFromDB = new HashMap<String,ExcludeVenues>();
					for (ExcludeVenues excludeVenues : excludeVenuesFromDB) {
						String key =  excludeVenues.getBrokerId()+"_"+ excludeVenues.getProductId()+"_"+ excludeVenues.getVenueId();
						excludeVenuesMapFromDB.put(key,excludeVenues);
					}
					
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
					Set<String> params=request.getParameterMap().keySet();
				  for (String param : params) {
					  if(param.contains("checkbox_")){
						  String temp[] = param.split("_");
							Integer brokerId = Integer.parseInt(temp[1].trim());
							Integer productId = Integer.parseInt(temp[2].trim());
							Integer venueIds = Integer.parseInt(temp[3].trim());
							String key=brokerId+"_"+productId+"_"+venueIds;
							ExcludeVenues excludeVenue=excludeVenuesMapFromDB.get(key);
						    if(excludeVenue==null){
							   excludeVenue=new ExcludeVenues();
							   excludeVenue.setVenueId(venueIds);
							   excludeVenue.setBrokerId(brokerId);
							   excludeVenue.setProductId(productId);
							   excludeVenue.setLastUpdatedBy(username);
							   excludeVenue.setLastUpdatedDate(now);
						     }
							
						    					    
							ExcludeVenuesAudit excludeVenuesAudit = new ExcludeVenuesAudit(excludeVenue);
							excludeVenuesAudit.setLastUpdatedBy(username);
							excludeVenuesAudit.setLastUpdatedDate(now);
							excludeVenuesAudit.setAction("Created");
							
							excludeVenuesList.add(excludeVenue);
							excludeVenuesAuditList.add(excludeVenuesAudit);
						}			  
				}	
				  updateExchangeEventTables(excludeVenuesList);
				  
				 /* for (Broker broker : brokerList) {
						if(!selectedBrokerId.contains(broker.getId())) {
							continue;
						}
						for (AutopricingProduct autopricingProduct : autopricingProductList) {
							if(!selectedProductId.contains(autopricingProduct.getId())) {
								continue;
							}
												
								String key = broker.getId()+"_"+autopricingProduct.getId()+"_"+venueId;
								ExcludeVenues excludeVenues = excludeVenuesMapFromDB.get(key);
								     if(excludeVenues!=null){							
								         excludeVenuesDeleteList.add(excludeVenues);
								     }
						    	
						}
					}*/
				  
				//Delete Logic
				    String uncheckbox=request.getParameter("uncheckedboxes");
				    String unchecked[]=uncheckbox.split(",");
				    for (String param : unchecked) {
						  if(param.contains("checkbox_")){
							  String temp[] = param.split("_");
								Integer brokerId = Integer.parseInt(temp[1].trim());
								Integer productId = Integer.parseInt(temp[2].trim());
								Integer venueIds = Integer.parseInt(temp[3].trim());
								
								ExcludeVenues deleteExcludeVenue=DAORegistry.getExcludeVenuesDAO().getExcludeVenueByProductIdAndBrokerIdAndVenueId(productId,brokerId,venueIds);
							 if(deleteExcludeVenue !=null){	
								ExcludeVenuesAudit excludeVenuesAudit = new ExcludeVenuesAudit(deleteExcludeVenue);
						    	excludeVenuesAudit.setLastUpdatedBy(username);
								excludeVenuesAudit.setLastUpdatedDate(now);
								excludeVenuesAudit.setAction("Deleted");						
								excludeVenuesAuditList.add(excludeVenuesAudit);
								excludeVenuesDeleteList.add(deleteExcludeVenue);
							 }
						  }
				    }
				    
				 	//Delete Logic
				   /* Collection<ExcludeVenues> exVenuesFromDb=DAORegistry.getExcludeVenuesDAO().getExcludeVenuesByVenueId(Integer.valueOf(venueId));
				    Map<String,ExcludeVenues> exVenueMapFromDb = new HashMap<String,ExcludeVenues>();
				    for (ExcludeVenues excludeVenues : exVenuesFromDb) {
						String key =  excludeVenues.getBrokerId()+"_"+ excludeVenues.getProductId()+"_"+ excludeVenues.getVenueId();
						exVenueMapFromDb.put(key,excludeVenues);
					}
				    for (ExcludeVenues excludeVenues : excludeVenuesList) {
				    	String key =  excludeVenues.getBrokerId()+"_"+ excludeVenues.getProductId()+"_"+ excludeVenues.getVenueId();
				    	exVenueMapFromDb.remove(key);
					}
				    Set<String> keySet=exVenueMapFromDb.keySet();
				    for (String key : keySet) {
				    	ExcludeVenues excludeVenues=exVenueMapFromDb.get(key);
				    	excludeVenuesDeleteList.add(excludeVenues);
				    	ExcludeVenuesAudit excludeVenuesAudit = new ExcludeVenuesAudit(excludeVenues);
				    	excludeVenuesAudit.setLastUpdatedBy(username);
						excludeVenuesAudit.setLastUpdatedDate(now);
						excludeVenuesAudit.setAction("Deleted");						
						excludeVenuesAuditList.add(excludeVenuesAudit);
				    	 
					}*/
				  
				    
				    DAORegistry.getExcludeVenuesDAO().deleteAll(excludeVenuesDeleteList);
				    DAORegistry.getExcludeVenuesDAO().saveOrUpdateAll(excludeVenuesList);
				    DAORegistry.getExcludeVenuesAuditDAO().saveAll(excludeVenuesAuditList);
				    
				    			    			    			    			    
				    
					info = "Excluded venue saved successfully.";
					action = "search";							
		}
				if(action.equals("search")&& venueId!="") {
					List<ExcludeVenues> excludeVenuesList = new ArrayList<ExcludeVenues>();
					Collection<ExcludeVenues> excludeVenuesFromDB=DAORegistry.getExcludeVenuesDAO().getAll();
					Map<String,ExcludeVenues> excludeVenuesMap = new HashMap<String,ExcludeVenues>();
					for (ExcludeVenues excludeVenues : excludeVenuesFromDB) {
						String key =  excludeVenues.getBrokerId()+"_"+ excludeVenues.getProductId()+"_"+ excludeVenues.getVenueId();
						excludeVenuesMap.put(key,excludeVenues);
					}
					
					
					
					for (Broker broker : brokerList) {
						if(!selectedBrokerId.contains(broker.getId())) {
							continue;
						}
						for (AutopricingProduct autopricingProduct : autopricingProductList) {
							if(!selectedProductId.contains(autopricingProduct.getId())) {
								continue;
							}
												
								String key = broker.getId()+"_"+autopricingProduct.getId()+"_"+venueId;
								ExcludeVenues excludeVenues = excludeVenuesMap.get(key);
								
								if( excludeVenues  == null) {
									 excludeVenues  = new  ExcludeVenues ();
									 excludeVenues.setBrokerId(broker.getId());
									 excludeVenues.setProductId(autopricingProduct.getId());
									 excludeVenues.setVenueId(Integer.valueOf(venueId));
								}
								
								excludeVenuesList.add(excludeVenues);
						    	
						}
					}
					mav.addObject("excludeVenuesList", excludeVenuesList);
				}
				
			}
			mav.addObject("isAllBrokerSelected", isAllBroker);
			mav.addObject("isAllProductSelected", isAllProduct);
			mav.addObject("selectedBrokerId", selectedBrokerIdStr);
			mav.addObject("selectedProductId", selectedProductIdStr);
			mav.addObject("brokerList", brokerList);
			mav.addObject("productList", autopricingProductList);
			mav.addObject("selectedValue", selectedValue);
			mav.addObject("selectedOption", selectedOption);
			mav.addObject("venueId",venueId);
			mav.addObject("info", info);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping(value="/ExcludeEventZones")	
	public ModelAndView getExcludeEventZones(HttpServletRequest request,Model model,HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("page-exclude-event-zones");
		try {
			
			Collection<Broker> brokerList = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokersWithMZTixBroker();
			Collection<AutopricingProduct> autopricingProductList = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductForVenueSettings();
			//List<Venue> venues = (List<Venue>) DAORegistry.getVenueDAO().getAll();
			List<Venue> venues = new ArrayList<Venue>();
			
			
			String selectedBrokerIdStr = request.getParameter("brokerId");
			String selectedProductIdStr = request.getParameter("productId");
			String selectedVenueId = request.getParameter("venueId");
			String selectedVenueCategoryId = request.getParameter("venueCategoryId");
			String[] eventStr = request.getParameterValues("events");
			String selectedVenueName = request.getParameter("hidselectedVenueName");
			String selectedEvent = "";
			String eventsCheckAll = request.getParameter("eventsCheckAll");
			boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
			
			
			Integer brokerId = null;
			if(selectedBrokerIdStr != null && !selectedBrokerIdStr.equals("")) {
				brokerId = Integer.parseInt(selectedBrokerIdStr);
			}
			Integer productId = null;
			if(selectedProductIdStr != null && selectedProductIdStr.trim().length() > 0) {
				productId = Integer.parseInt(selectedProductIdStr);
			}
			
			if(selectedVenueId != null && selectedVenueId!=""){
				//Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(selectedVenueId));
				List<VenueCategory> venueCategoryList = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(Integer.parseInt(selectedVenueId));
				
				mav.addObject("venueCategoryList", venueCategoryList);
			}
			if(selectedVenueCategoryId!=null && selectedVenueCategoryId!=""){
				List<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByVenueCategoryId(Integer.parseInt(selectedVenueCategoryId));
				mav.addObject("events", events);
			}
			
			if(eventStr!=null){
				for (String eventId : eventStr) {
					try{
						selectedEvent += eventId + ",";
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			String[] selectedApplicableBrokerIdStr = request.getParameterValues("applicableBrokerId");
			String[] selectedApplicableProductIdStr = request.getParameterValues("applicableProductId");
			String isAllApplicableBroker = request.getParameter("isAllApplicableBroker");
			String isAllApplicableProduct = request.getParameter("isAllApplicableProduct");
			
			
			mav.addObject("selectedVenueCategoryId", selectedVenueCategoryId);
			mav.addObject("selectedProductId", selectedProductIdStr);
			mav.addObject("selectedBrokerId", selectedBrokerIdStr);
			mav.addObject("venueList", venues);
			mav.addObject("selectedVenueId", selectedVenueId);
			model.addAttribute("eventsCheckAll", isAllEvent);
			model.addAttribute("eventStr", selectedEvent);
			mav.addObject("isAllApplicableBrokerSelected", isAllApplicableBroker);
			mav.addObject("isAllApplicableProductSelected", isAllApplicableProduct);
			mav.addObject("selectedApplicableBrokerId", selectedApplicableBrokerIdStr);
			mav.addObject("selectedApplicableProductId", selectedApplicableProductIdStr);
			mav.addObject("brokerList", brokerList);
			mav.addObject("productList", autopricingProductList);
			mav.addObject("selectedVenueName", selectedVenueName);		
			
			String action = request.getParameter("action");
			String info = "";
			
			if(action != null) {
	            if (action.equals("update")) {
	            	
	            	List<Integer> eligibleBrokerIds = new ArrayList<Integer>();
	    			List<Integer> eligibleProductIds = new ArrayList<Integer>();
	    			eligibleBrokerIds.add(brokerId);
	    			eligibleProductIds.add(productId);
	    			
	    			if (selectedApplicableBrokerIdStr != null && selectedApplicableBrokerIdStr.length != 0) {
	    				for (String brokerIdStr : selectedApplicableBrokerIdStr) {
	    					if (brokerIdStr.isEmpty() || eligibleBrokerIds.contains(Integer.parseInt(brokerIdStr))) {
	    						continue;
	    					}
	    					eligibleBrokerIds.add(Integer.parseInt(brokerIdStr));
	    				}
	    			}
	    			if (selectedApplicableProductIdStr != null && selectedApplicableProductIdStr.length != 0) {
	    				for (String productIdStr : selectedApplicableProductIdStr) {
	    					if (productIdStr.isEmpty() || eligibleProductIds.contains(Integer.parseInt(productIdStr))) {
	    						continue;
	    					}
	    					eligibleProductIds.add(Integer.parseInt(productIdStr));
	    				}
	    			}
	    			
	            	List<ExcludeEventZonesAudit> excludeEventZonesAuditList = new ArrayList<ExcludeEventZonesAudit>();
	            	List<ExcludeEventZones> excludeEventZonesList = new ArrayList<ExcludeEventZones>();
					Map<String,ExcludeEventZones> dbExcludeEventZonesMap = new HashMap<String,ExcludeEventZones>();

					for (String eventIdStr : eventStr) {
						Collection<ExcludeEventZones> dbExcludeEventZones = DAORegistry.getExcludeEventZonesDAO().getAllExcludeEventZonesByEventId(Integer.parseInt(eventIdStr));
						for(ExcludeEventZones excludeEventZone : dbExcludeEventZones) {
							
							if((eligibleProductIds.contains(excludeEventZone.getProductId()) &&
									eligibleBrokerIds.contains(excludeEventZone.getBrokerId()))) {
								dbExcludeEventZonesMap.put(excludeEventZone.getProductId()+"_"+
										excludeEventZone.getBrokerId()+"_"+excludeEventZone.getEventId()+"_"+excludeEventZone.getZone().replaceAll("\\s+", " "), excludeEventZone);	
							}
							
						}
					}
					
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
					Set<String> params=request.getParameterMap().keySet();
				  for (String param : params) {
					  if(param.contains("checkbox_")) {
						  String temp[] = param.split("_");
						  Integer eventId = Integer.parseInt(temp[1].trim());
						  String zone = temp[2].trim();

						  for (Integer eligibleProductId : eligibleProductIds) {
							  for (Integer eligibleBrokerId : eligibleBrokerIds) {
								  String key = eligibleProductId+"_"+eligibleBrokerId+"_"+eventId+"_"+zone;
								  
								  String auditAction = "Updated";
								  ExcludeEventZones excludeEvent = dbExcludeEventZonesMap.remove(key);
								  if(excludeEvent == null) {
									  excludeEvent = new ExcludeEventZones();
									  auditAction = "Created";
								  } else {
									  if(!excludeEvent.getStatus().equals("ACTIVE")) {
										  auditAction = "Created";
									  }
								  }
								  
								  excludeEvent.setBrokerId(eligibleBrokerId);
								  excludeEvent.setProductId(eligibleProductId);
								  excludeEvent.setEventId(eventId);
								  excludeEvent.setZone(zone);
								  excludeEvent.setStatus("ACTIVE");
								  excludeEvent.setLastUpdatedBy(username);
								  excludeEvent.setLastUpdatedDate(now);
									
								  ExcludeEventZonesAudit excludeEventZonesAudit = new ExcludeEventZonesAudit(excludeEvent);
								  excludeEventZonesAudit.setAction(auditAction);
								  excludeEventZonesAuditList.add(excludeEventZonesAudit);
									
								  excludeEventZonesList.add(excludeEvent);				
							  }
						  }
					  }			  
				  }	
				  
				  List<ExcludeEventZones> tobeDeleteList = new ArrayList<ExcludeEventZones>();
				  for (ExcludeEventZones excludeEventZone : dbExcludeEventZonesMap.values()) {
					
					  if(!excludeEventZone.getStatus().equals("ACTIVE")) {
						  continue;
					  }
					  excludeEventZone.setStatus("DELETED");
					  excludeEventZone.setLastUpdatedBy(username);
					  excludeEventZone.setLastUpdatedDate(now);
						
					  ExcludeEventZonesAudit excludeEventZonesAudit = new ExcludeEventZonesAudit(excludeEventZone);
					  excludeEventZonesAudit.setAction("Deleted");
					  excludeEventZonesAuditList.add(excludeEventZonesAudit);
					  
					  tobeDeleteList.add(excludeEventZone);
				  }
				  
				  //DAORegistry.getExcludeEventZonesDAO().deleteAll(tobeDeleteList);
				  DAORegistry.getExcludeEventZonesDAO().updateAll(tobeDeleteList);
				  DAORegistry.getExcludeEventZonesDAO().saveOrUpdateAll(excludeEventZonesList);
				  DAORegistry.getExcludeEventZonesAuditDAO().saveAll(excludeEventZonesAuditList);
				    
				  info = "ExcludeEvent Zones saved successfully.";
				  action = "search";							
	            }
			
				if(action.equals("search")) {
					if(eventStr!=null) {
						List<String> categoryList = DAORegistry.getCategoryDAO().getAllCategoryByVenueCategoryId(Integer.parseInt(selectedVenueCategoryId));
						
						Map<String, ExcludeEventZones> dbExcludeEventZonesMap = new HashMap<String, ExcludeEventZones>();
						List<ExcludeEventZones> excludeEventZones = new ArrayList<ExcludeEventZones>();
						for (String eventId : eventStr) {
							try {
								selectedEvent += eventId + ",";
									
								if(categoryList!=null) {
									Collection<ExcludeEventZones> dbExcludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdAndBrokerIdAndEventId(productId, brokerId, Integer.parseInt(eventId));
									for(ExcludeEventZones excludeEventZone : dbExcludeEventZones) {
										dbExcludeEventZonesMap.put(excludeEventZone.getProductId()+"_"+excludeEventZone.getBrokerId()+"_"+
												excludeEventZone.getEventId()+"_"+excludeEventZone.getZone().replaceAll("\\s+", " "), excludeEventZone);
									}
									for (String category : categoryList) {
										String key = productId+"_"+brokerId+"_"+eventId+"_"+category.replaceAll("\\s+", " ");
										ExcludeEventZones excludeEventZone = dbExcludeEventZonesMap.get(key);
										if(excludeEventZone == null) {
											excludeEventZone = new ExcludeEventZones();
										}
										excludeEventZone.setZone(category.replaceAll("\\s+", " "));
										excludeEventZone.setEventId(Integer.parseInt(eventId));
										excludeEventZone.setProductId(Integer.parseInt(selectedProductIdStr));
										excludeEventZone.setBrokerId(Integer.parseInt(selectedBrokerIdStr));
										
										excludeEventZones.add(excludeEventZone);
									}		
								 }
								
							} catch(Exception e){
								e.printStackTrace();
							}
						}
						model.addAttribute("excludeEventZones", excludeEventZones);
					}
						
				}
			}
			
			mav.addObject("info", info);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping(value="/ExcludeVenueCategoryZones")	
	public ModelAndView getExcludeVenueCategoryZones(HttpServletRequest request,Model model,HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("page-exclude-venue-category-zones");
		try {
			
			Collection<Broker> brokerList = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokersWithMZTixBroker();
			Collection<AutopricingProduct> autopricingProductList = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductForVenueSettings();
			//List<Venue> venues = (List<Venue>) DAORegistry.getVenueDAO().getAll();
			List<Venue> venues = new ArrayList<Venue>();
			
			
			String selectedBrokerIdStr = request.getParameter("brokerId");
			String selectedProductIdStr = request.getParameter("productId");
			String selectedVenueId = request.getParameter("venueId");
			String selectedVenueCategoryId = request.getParameter("venueCategoryId");
			String selectedVenueName = request.getParameter("hidselectedVenueName");
			
			
			Integer venueCategoryId = null;
			if(selectedVenueCategoryId != null && selectedVenueCategoryId.trim().length() > 0) {
				venueCategoryId = Integer.parseInt(selectedVenueCategoryId);
			}
			
			Integer brokerId = null;
			if(selectedBrokerIdStr != null && !selectedBrokerIdStr.equals("")) {
				brokerId = Integer.parseInt(selectedBrokerIdStr);
			}
			Integer productId = null;
			if(selectedProductIdStr != null && selectedProductIdStr.trim().length() > 0) {
				productId = Integer.parseInt(selectedProductIdStr);
			}
			
			if(selectedVenueId != null && selectedVenueId!=""){
				//Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(selectedVenueId));
				List<VenueCategory> venueCategoryList = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(Integer.parseInt(selectedVenueId));
				
				mav.addObject("venueCategoryList", venueCategoryList);
			}
			
			String[] selectedApplicableBrokerIdStr = request.getParameterValues("applicableBrokerId");
			String[] selectedApplicableProductIdStr = request.getParameterValues("applicableProductId");
			String isAllApplicableBroker = request.getParameter("isAllApplicableBroker");
			String isAllApplicableProduct = request.getParameter("isAllApplicableProduct");
			
			
			mav.addObject("selectedVenueCategoryId", selectedVenueCategoryId);
			mav.addObject("selectedProductId", selectedProductIdStr);
			mav.addObject("selectedBrokerId", selectedBrokerIdStr);
			mav.addObject("venueList", venues);
			mav.addObject("selectedVenueId", selectedVenueId);
			mav.addObject("isAllApplicableBrokerSelected", isAllApplicableBroker);
			mav.addObject("isAllApplicableProductSelected", isAllApplicableProduct);
			mav.addObject("selectedApplicableBrokerId", selectedApplicableBrokerIdStr);
			mav.addObject("selectedApplicableProductId", selectedApplicableProductIdStr);
			mav.addObject("brokerList", brokerList);
			mav.addObject("productList", autopricingProductList);
			mav.addObject("selectedVenueName",selectedVenueName);
			
			
			
			String action = request.getParameter("action");
			String info = "";
			
			if(action != null) {
	            if (action.equals("update")) {
	            	
	            	List<Integer> eligibleBrokerIds = new ArrayList<Integer>();
	    			List<Integer> eligibleProductIds = new ArrayList<Integer>();
	    			eligibleBrokerIds.add(brokerId);
	    			eligibleProductIds.add(productId);
	    			
	    			if (selectedApplicableBrokerIdStr != null && selectedApplicableBrokerIdStr.length != 0) {
	    				for (String brokerIdStr : selectedApplicableBrokerIdStr) {
	    					if (brokerIdStr.isEmpty() || eligibleBrokerIds.contains(Integer.parseInt(brokerIdStr))) {
	    						continue;
	    					}
	    					eligibleBrokerIds.add(Integer.parseInt(brokerIdStr));
	    				}
	    			}
	    			if (selectedApplicableProductIdStr != null && selectedApplicableProductIdStr.length != 0) {
	    				for (String productIdStr : selectedApplicableProductIdStr) {
	    					if (productIdStr.isEmpty() || eligibleProductIds.contains(Integer.parseInt(productIdStr))) {
	    						continue;
	    					}
	    					eligibleProductIds.add(Integer.parseInt(productIdStr));
	    				}
	    			}
	    			
	    			
	            	List<ExcludeVenueCategoryZonesAudit> excludeVenueCategoryZonesAuditList = new ArrayList<ExcludeVenueCategoryZonesAudit>();
	            	List<ExcludeVenueCategoryZones> excludeVenueCategoryZonesList = new ArrayList<ExcludeVenueCategoryZones>();
					Map<String,ExcludeVenueCategoryZones> dbExcludeVenueCategoryZonesMap = new HashMap<String,ExcludeVenueCategoryZones>();

					Collection<ExcludeVenueCategoryZones> dbExcludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getAllExcludeVenueCategoryZonesByVenueCategoryId(venueCategoryId);
					for(ExcludeVenueCategoryZones excludeVenueCategoryZone : dbExcludeVenueCategoryZones) {
						
						if((eligibleProductIds.contains(excludeVenueCategoryZone.getProductId()) &&
								eligibleBrokerIds.contains(excludeVenueCategoryZone.getBrokerId()))) {
							dbExcludeVenueCategoryZonesMap.put(excludeVenueCategoryZone.getProductId()+"_"+
									excludeVenueCategoryZone.getBrokerId()+"_"+excludeVenueCategoryZone.getVenueCategoryId()+"_"+excludeVenueCategoryZone.getZone().replaceAll("\\s+", " "), excludeVenueCategoryZone);	
						}
						
					}
					
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
					Set<String> params=request.getParameterMap().keySet();
				  for (String param : params) {
					  if(param.contains("checkbox_")) {
						  String temp[] = param.split("_");
						  //Integer venueCategoryId = Integer.parseInt(temp[1].trim());
						  String zone = temp[1].trim();

						  for (Integer eligibleProductId : eligibleProductIds) {
							  for (Integer eligibleBrokerId : eligibleBrokerIds) {
								  String key = eligibleProductId+"_"+eligibleBrokerId+"_"+venueCategoryId+"_"+zone;
								  
								  String auditAction = "Updated";
								  ExcludeVenueCategoryZones excludeVenueCategory = dbExcludeVenueCategoryZonesMap.remove(key);
								  if(excludeVenueCategory == null) {
									  excludeVenueCategory = new ExcludeVenueCategoryZones();
									  auditAction = "Created";
								  } else {
									  if(!excludeVenueCategory.getStatus().equals("ACTIVE")) {
										  auditAction = "Created";
									  }
								  }
								  excludeVenueCategory.setBrokerId(eligibleBrokerId);
								  excludeVenueCategory.setProductId(eligibleProductId);
								  excludeVenueCategory.setVenueCategoryId(venueCategoryId);
								  excludeVenueCategory.setZone(zone);
								  excludeVenueCategory.setStatus("ACTIVE");
								  excludeVenueCategory.setLastUpdatedBy(username);
								  excludeVenueCategory.setLastUpdatedDate(now);
									
								  ExcludeVenueCategoryZonesAudit excludeVenueCategoryZonesAudit = new ExcludeVenueCategoryZonesAudit(excludeVenueCategory);
								  excludeVenueCategoryZonesAudit.setAction(auditAction);
								  excludeVenueCategoryZonesAuditList.add(excludeVenueCategoryZonesAudit);
									
								  excludeVenueCategoryZonesList.add(excludeVenueCategory);				
							  }
						  }
					  }			  
				  }	
				  
				  List<ExcludeVenueCategoryZones> tobeDeleteList = new ArrayList<ExcludeVenueCategoryZones>();
				  for (ExcludeVenueCategoryZones excludeVenueCategoryZone : dbExcludeVenueCategoryZonesMap.values()) {
					
					  if(!excludeVenueCategoryZone.getStatus().equals("ACTIVE")) {
						  continue;
					  }
					  excludeVenueCategoryZone.setStatus("DELETED");
					  excludeVenueCategoryZone.setLastUpdatedBy(username);
					  excludeVenueCategoryZone.setLastUpdatedDate(now);
						
					  ExcludeVenueCategoryZonesAudit excludeVenueCategoryZonesAudit = new ExcludeVenueCategoryZonesAudit(excludeVenueCategoryZone);
					  excludeVenueCategoryZonesAudit.setAction("Deleted");
					  excludeVenueCategoryZonesAuditList.add(excludeVenueCategoryZonesAudit);
					  
					  tobeDeleteList.add(excludeVenueCategoryZone);
				  }
				  
				  //DAORegistry.getExcludeVenueCategoryZonesDAO().deleteAll(tobeDeleteList);
				  DAORegistry.getExcludeVenueCategoryZonesDAO().updateAll(tobeDeleteList);
				  DAORegistry.getExcludeVenueCategoryZonesDAO().saveOrUpdateAll(excludeVenueCategoryZonesList);
				  DAORegistry.getExcludeVenueCategoryZonesAuditDAO().saveAll(excludeVenueCategoryZonesAuditList);
				    
				  info = "ExcludeVenueCategory Zones saved successfully.";
				  action = "search";							
	            }
			
				if(action.equals("search")) {
					if(venueCategoryId != null) {
						List<String> categoryList = DAORegistry.getCategoryDAO().getAllCategoryByVenueCategoryId(venueCategoryId);
						
						Map<String, ExcludeVenueCategoryZones> dbExcludeVenueCategoryZonesMap = new HashMap<String, ExcludeVenueCategoryZones>();
						List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = new ArrayList<ExcludeVenueCategoryZones>();
							
						try {
									
							if(categoryList!=null) {
								Collection<ExcludeVenueCategoryZones> dbExcludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdAndBrokerIdAndVenueCategoryId(productId, brokerId, venueCategoryId);
								for(ExcludeVenueCategoryZones excludeVenueCategoryZone : dbExcludeVenueCategoryZones) {
									dbExcludeVenueCategoryZonesMap.put(excludeVenueCategoryZone.getProductId()+"_"+excludeVenueCategoryZone.getBrokerId()+"_"+
											excludeVenueCategoryZone.getVenueCategoryId()+"_"+excludeVenueCategoryZone.getZone().replaceAll("\\s+", " "), excludeVenueCategoryZone);
								}
								for (String category : categoryList) {
									String key = productId+"_"+brokerId+"_"+venueCategoryId+"_"+category.replaceAll("\\s+", " ");
									ExcludeVenueCategoryZones excludeVenueCategoryZone = dbExcludeVenueCategoryZonesMap.get(key);
									if(excludeVenueCategoryZone == null) {
										excludeVenueCategoryZone = new ExcludeVenueCategoryZones();
									}
									excludeVenueCategoryZone.setZone(category.replaceAll("\\s+", " "));
									excludeVenueCategoryZone.setVenueCategoryId(venueCategoryId);
									excludeVenueCategoryZone.setProductId(Integer.parseInt(selectedProductIdStr));
									excludeVenueCategoryZone.setBrokerId(Integer.parseInt(selectedBrokerIdStr));
									
									excludeVenueCategoryZones.add(excludeVenueCategoryZone);
								}		
							 }
							
						} catch(Exception e){
							e.printStackTrace();
						}
						model.addAttribute("excludeVenueCategoryZones", excludeVenueCategoryZones);
					}
						
				}
			}
			
			mav.addObject("info", info);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping(value="/ExcludeEventZonesAudit")
	public String getExcludeEventZonesAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response) throws Exception {
		Integer productId = Integer.parseInt(request.getParameter("pId"));
		Integer brokerId = Integer.parseInt(request.getParameter("bId"));
		Integer eventId = Integer.parseInt(request.getParameter("eId"));
		String zone = request.getParameter("zone");
		
		Event event = DAORegistry.getEventDAO().get(eventId);
		List<ExcludeEventZonesAudit> excludeEventZonesAudits = DAORegistry.getExcludeEventZonesAuditDAO().getExcludeEventZonesAuditByProductIdAndBrokerIdAndEventId(productId, brokerId, eventId, zone);
		model.addAttribute("audits", excludeEventZonesAudits);
		model.addAttribute("event", event);
		model.addAttribute("zone", zone);

		return "page-exclude-event-zones-audit-popup";
	}
	
	@RequestMapping(value="/ExcludeVenueCategoryZonesAudit")
	public String getExcludeVenueCategoryZonesAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response) throws Exception {
		Integer productId = Integer.parseInt(request.getParameter("pId"));
		Integer brokerId = Integer.parseInt(request.getParameter("bId"));
		Integer venueCategoryId = Integer.parseInt(request.getParameter("vcId"));
		String zone = request.getParameter("zone");
		
		VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().get(venueCategoryId);
		List<ExcludeVenueCategoryZonesAudit> excludeVenueCategoryZonesAudits = DAORegistry.getExcludeVenueCategoryZonesAuditDAO().getExcludeVenueCategoryZonesAuditByProductIdAndBrokerIdAndVenueCategoryIdAndZone(productId, brokerId, venueCategoryId, zone);
		model.addAttribute("audits", excludeVenueCategoryZonesAudits);
		model.addAttribute("venueCategory", venueCategory);
		model.addAttribute("zone", zone);
		return "page-exclude-venue-category-zones-audit-popup";
	}

	void updateExchangeEventTables(List<ExcludeVenues> excludeVenuesList) throws Exception {
		
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		for (ExcludeVenues excludeVenues : excludeVenuesList) {
		    if(excludeVenues.getProductId().equals(3)){//Minicats
		    	List<MiniExchangeEventAudit> exchangeEventAuditList= new ArrayList<MiniExchangeEventAudit>();
		    	List<MiniExchangeEvent> exchangeEventUpdateList = new ArrayList<MiniExchangeEvent>();
		    	List<MiniExchangeEvent> exchangeEventList=DAORegistry.getMiniExchangeEventDAO()
		    	.getMiniExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (MiniExchangeEvent miniExchangeEvent : exchangeEventList) {
		    		MiniExchangeEventAudit  miniExchangeEventAudit=new MiniExchangeEventAudit(miniExchangeEvent);
					exchangeEventAuditList.add(miniExchangeEventAudit);
					
					if(miniExchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(miniExchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  miniExchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(miniExchangeEvent.getScoreBigBrokerId()!=null){
					  if(miniExchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  miniExchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(miniExchangeEvent.getVividBrokerId()!=null){
					  if(miniExchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  miniExchangeEvent.setVividBrokerId(null);
					  }
					}
					if(miniExchangeEvent.getFanxchangeBrokerId()!=null){
					  if(miniExchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  miniExchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(miniExchangeEvent.getTicketcityBrokerId()!=null){
						  if(miniExchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  miniExchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(miniExchangeEvent.getSeatGeekBrokerId()!=null){
						  if(miniExchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  miniExchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					miniExchangeEvent.setLastUpdatedBy(userName);
					miniExchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(miniExchangeEvent);
				}
		    	DAORegistry.getMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getMiniExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    	
		    }else if(excludeVenues.getProductId().equals(4)){//VipMinicats
		    	List<VipMiniExchangeEventAudit> exchangeEventAuditList= new ArrayList<VipMiniExchangeEventAudit>();
		    	List<VipMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<VipMiniExchangeEvent>();
		    	List<VipMiniExchangeEvent> exchangeEventList=DAORegistry.getVipMiniExchangeEventDAO()
		    	.getVipMiniExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (VipMiniExchangeEvent exchangeEvent : exchangeEventList) {
		    		VipMiniExchangeEventAudit  exchangeEventAudit=new VipMiniExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
						  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setFanxchangeBrokerId(null);
						  }
						}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId()!=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getVipMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getVipMiniExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    	
		    }else if(excludeVenues.getProductId().equals(5)){//Last Row
		    	List<LastRowMiniExchangeEventAudit> exchangeEventAuditList= new ArrayList<LastRowMiniExchangeEventAudit>();
		    	List<LastRowMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<LastRowMiniExchangeEvent>();
		    	List<LastRowMiniExchangeEvent> exchangeEventList=DAORegistry.getLastRowMiniExchangeEventDAO()
		    	.getLastRowMiniExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (LastRowMiniExchangeEvent exchangeEvent : exchangeEventList) {
		    		LastRowMiniExchangeEventAudit  exchangeEventAudit=new LastRowMiniExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId() !=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getLastRowMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    }else if(excludeVenues.getProductId().equals(6)){//Last Row
		    	List<LarryLastExchangeEventAudit> exchangeEventAuditList= new ArrayList<LarryLastExchangeEventAudit>();
		    	List<LarryLastExchangeEvent> exchangeEventUpdateList = new ArrayList<LarryLastExchangeEvent>();
		    	List<LarryLastExchangeEvent> exchangeEventList=DAORegistry.getLarryLastExchangeEventDAO()
		    	.getLastRowMiniExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (LarryLastExchangeEvent exchangeEvent : exchangeEventList) {
		    		LarryLastExchangeEventAudit  exchangeEventAudit=new LarryLastExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(excludeVenues.getBrokerId().equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//Tixcity
						exchangeEvent.setTixcityEnabled(false);
					}
					if(excludeVenues.getBrokerId().equals(5)){//RTW
						exchangeEvent.setRtwEnabled(false);
					}
					if(excludeVenues.getBrokerId().equals(10)){//RTW-2
						exchangeEvent.setRotEnabled(false);
					}
					/*if(excludeVenues.getBrokerId().equals(3)){
						if(exchangeEvent.getRotEnabled().equals(true)){
							  exchangeEvent.setRotEnabled(false);
						}
			    	}else if(excludeVenues.getBrokerId().equals(5)){
						if(exchangeEvent.getRtwEnabled().equals(true)){
							  exchangeEvent.setRtwEnabled(false);
						}
			    	}*/
					
					/*if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}*/
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getLarryLastExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    	
		    }else if(excludeVenues.getProductId().equals(7)){//Zoned Last Row
		    	List<ZoneLastRowMiniExchangeEventAudit> exchangeEventAuditList= new ArrayList<ZoneLastRowMiniExchangeEventAudit>();
		    	List<ZoneLastRowMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<ZoneLastRowMiniExchangeEvent>();
		    	List<ZoneLastRowMiniExchangeEvent> exchangeEventList=DAORegistry.getZoneLastRowMiniExchangeEventDAO()
		    	.getZoneLastRowMiniExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (ZoneLastRowMiniExchangeEvent exchangeEvent : exchangeEventList) {
		    		ZoneLastRowMiniExchangeEventAudit  exchangeEventAudit=new ZoneLastRowMiniExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(excludeVenues.getBrokerId().equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//Tixcity
						exchangeEvent.setTixcityEnabled(false);
					}
					if(excludeVenues.getBrokerId().equals(5)){//RTW
						exchangeEvent.setRtwEnabled(false);
					}
					if(excludeVenues.getBrokerId().equals(10)){//RTW-2
						exchangeEvent.setRotEnabled(false);
					}
					
					/*if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId() !=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}*/
					
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getZoneLastRowMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    	
		    }else if(excludeVenues.getProductId().equals(8)){//Zones Pricing 
		    	List<ZonePricingExchangeEventAudit> exchangeEventAuditList= new ArrayList<ZonePricingExchangeEventAudit>();
		    	List<ZonePricingExchangeEvent> exchangeEventUpdateList = new ArrayList<ZonePricingExchangeEvent>();
		    	List<ZonePricingExchangeEvent> exchangeEventList=DAORegistry.getZonePricingExchangeEventDAO()
		    	.getZonePricingExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (ZonePricingExchangeEvent exchangeEvent : exchangeEventList) {
		    		ZonePricingExchangeEventAudit  exchangeEventAudit=new ZonePricingExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId() !=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getZonePricingExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getZonePricingExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    }else if(excludeVenues.getProductId().equals(9)){//Zones Ticket Processor 
		    	List<ZoneTicketsProcessorExchangeEventAudit> exchangeEventAuditList= new ArrayList<ZoneTicketsProcessorExchangeEventAudit>();
		    	List<ZoneTicketsProcessorExchangeEvent> exchangeEventUpdateList = new ArrayList<ZoneTicketsProcessorExchangeEvent>();
		    	List<ZoneTicketsProcessorExchangeEvent> exchangeEventList=DAORegistry.getZoneTicketsProcessorExchangeEventDAO()
		    	.getZoneTicketsProcessorExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (ZoneTicketsProcessorExchangeEvent exchangeEvent : exchangeEventList) {
		    		ZoneTicketsProcessorExchangeEventAudit  exchangeEventAudit=new ZoneTicketsProcessorExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId() !=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}
					if(exchangeEvent.getZoneTicketBrokerId()!=null){
						 if(exchangeEvent.getZoneTicketBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setZoneTicketBrokerId(null);
						  }
					}
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getZoneTicketsProcessorExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getZoneTicketsProcessorExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    	
		    }else if(excludeVenues.getProductId().equals(10)){//AutoCat96 
		    	List<AutoCats96ExchangeEventAudit> exchangeEventAuditList= new ArrayList<AutoCats96ExchangeEventAudit>();
		    	List<AutoCats96ExchangeEvent> exchangeEventUpdateList = new ArrayList<AutoCats96ExchangeEvent>();
		    	List<AutoCats96ExchangeEvent> exchangeEventList=DAORegistry.getAutoCats96ExchangeEventDAO()
		    	.getAutoCats96ExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		      if(exchangeEventList !=null){	
		    	for (AutoCats96ExchangeEvent exchangeEvent : exchangeEventList) {
		    		AutoCats96ExchangeEventAudit  exchangeEventAudit=new AutoCats96ExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(excludeVenues.getBrokerId().equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//Tixcity
						exchangeEvent.setTixcityEnabled(false);
					}
					if(excludeVenues.getBrokerId().equals(5)){//RTW
						exchangeEvent.setRtwEnabled(false);
					}
					if(excludeVenues.getBrokerId().equals(10)){//RTW-2
						exchangeEvent.setRotEnabled(false);
					}
					
			    	/*if(excludeVenues.getBrokerId().equals(3)){
						if(exchangeEvent.getRotEnabled().equals(true)){
							  exchangeEvent.setRotEnabled(false);
						}
			    	}else if(excludeVenues.getBrokerId().equals(5)){
						if(exchangeEvent.getRtwEnabled().equals(true)){
							  exchangeEvent.setRtwEnabled(false);
						}
			    	}*/
					
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
				
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getAutoCats96ExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getAutoCats96ExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		      }	
		    	
		    }else if(excludeVenues.getProductId().equals(11)){//Presale Autocat 
		    	List<PresaleAutoCatExchangeEventAudit> exchangeEventAuditList= new ArrayList<PresaleAutoCatExchangeEventAudit>();
		    	List<PresaleAutoCatExchangeEvent> exchangeEventUpdateList = new ArrayList<PresaleAutoCatExchangeEvent>();
		    	List<PresaleAutoCatExchangeEvent> exchangeEventList=DAORegistry.getPresaleAutoCatExchangeEventDAO()
		    	.getPresaleAutoCatExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	if(exchangeEventList!=null && !exchangeEventList.isEmpty()){
			    	for (PresaleAutoCatExchangeEvent exchangeEvent : exchangeEventList) {
			    		PresaleAutoCatExchangeEventAudit  exchangeEventAudit=new PresaleAutoCatExchangeEventAudit(exchangeEvent);
						exchangeEventAuditList.add(exchangeEventAudit);
						
			    		if(excludeVenues.getBrokerId().equals(3)){
							if(exchangeEvent.getRotEnabled().equals(true)){
								  exchangeEvent.setRotEnabled(false);
							}
				    	}else if(excludeVenues.getBrokerId().equals(5)){
							if(exchangeEvent.getRtwEnabled().equals(true)){
								  exchangeEvent.setRtwEnabled(false);
							}
				    	}
						exchangeEvent.setLastUpdatedBy(userName);
						exchangeEvent.setLastUpdatedDate(new Date());
						exchangeEventUpdateList.add(exchangeEvent);
						
					}
		    	}
		    	DAORegistry.getPresaleAutoCatExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    	
		    }else if(excludeVenues.getProductId().equals(14)){//RTW Zones Pricing 
		    	List<TixCityZonePricingExchangeEventAudit> exchangeEventAuditList= new ArrayList<TixCityZonePricingExchangeEventAudit>();
		    	List<TixCityZonePricingExchangeEvent> exchangeEventUpdateList = new ArrayList<TixCityZonePricingExchangeEvent>();
		    	List<TixCityZonePricingExchangeEvent> exchangeEventList=DAORegistry.getTixCityZonePricingExchangeEventDAO()
		    	.getTixCityZonePricingExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (TixCityZonePricingExchangeEvent exchangeEvent : exchangeEventList) {
		    		TixCityZonePricingExchangeEventAudit  exchangeEventAudit=new TixCityZonePricingExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId() !=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getTixCityZonePricingExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getTixCityZonePricingExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    }else if(excludeVenues.getProductId().equals(15)){//Manhattan Zones Pricing 
		    	List<ManhattanZonePricingExchangeEventAudit> exchangeEventAuditList= new ArrayList<ManhattanZonePricingExchangeEventAudit>();
		    	List<ManhattanZonePricingExchangeEvent> exchangeEventUpdateList = new ArrayList<ManhattanZonePricingExchangeEvent>();
		    	List<ManhattanZonePricingExchangeEvent> exchangeEventList=DAORegistry.getManhattanZonePricingExchangeEventDAO()
		    	.getManhattanZonePricingExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (ManhattanZonePricingExchangeEvent exchangeEvent : exchangeEventList) {
		    		ManhattanZonePricingExchangeEventAudit  exchangeEventAudit=new ManhattanZonePricingExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId() !=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getManhattanZonePricingExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getManhattanZonePricingExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    }else if(excludeVenues.getProductId().equals(16)){// VIP Last Row
		    	List<VipLastRowMiniExchangeEventAudit> exchangeEventAuditList= new ArrayList<VipLastRowMiniExchangeEventAudit>();
		    	List<VipLastRowMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<VipLastRowMiniExchangeEvent>();
		    	List<VipLastRowMiniExchangeEvent> exchangeEventList=DAORegistry.getVipLastRowMiniExchangeEventDAO()
		    	.getVipLastRowMiniExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (VipLastRowMiniExchangeEvent exchangeEvent : exchangeEventList) {
		    		VipLastRowMiniExchangeEventAudit  exchangeEventAudit=new VipLastRowMiniExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId() !=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getVipLastRowMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getVipLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    } else if(excludeVenues.getProductId().equals(17)){// PresaleZoneTicketsProcessor
		    	List<PresaleZoneTicketsExchangeEventAudit> exchangeEventAuditList= new ArrayList<PresaleZoneTicketsExchangeEventAudit>();
		    	List<PresaleZoneTicketsExchangeEvent> exchangeEventUpdateList = new ArrayList<PresaleZoneTicketsExchangeEvent>();
		    	List<PresaleZoneTicketsExchangeEvent> exchangeEventList=DAORegistry.getPresaleZoneTicketsExchangeEventDAO()
		    	.getPresaleZoneTicketsExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (PresaleZoneTicketsExchangeEvent exchangeEvent : exchangeEventList) {
		    		PresaleZoneTicketsExchangeEventAudit  exchangeEventAudit=new PresaleZoneTicketsExchangeEventAudit(exchangeEvent);
					exchangeEventAuditList.add(exchangeEventAudit);
					
					if(exchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(exchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(exchangeEvent.getScoreBigBrokerId()!=null){
					  if(exchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(exchangeEvent.getFanxchangeBrokerId()!=null){
					  if(exchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(exchangeEvent.getTicketcityBrokerId()!=null){
						  if(exchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(exchangeEvent.getSeatGeekBrokerId() !=null){
						  if(exchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					if(exchangeEvent.getVividBrokerId()!=null){
					  if(exchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  exchangeEvent.setVividBrokerId(null);
					  }
					}
					if(exchangeEvent.getZoneTicketBrokerId()!=null){
						 if(exchangeEvent.getZoneTicketBrokerId().equals(excludeVenues.getBrokerId())){					  
							  exchangeEvent.setZoneTicketBrokerId(null);
						  }
					}
					exchangeEvent.setLastUpdatedBy(userName);
					exchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(exchangeEvent);
				}
		    	DAORegistry.getPresaleZoneTicketsExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getPresaleZoneTicketsExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    } else if(excludeVenues.getProductId().equals(19)){//SGLastFiveRow
		    	List<SGLastFiveRowExchangeEventAudit> exchangeEventAuditList= new ArrayList<SGLastFiveRowExchangeEventAudit>();
		    	List<SGLastFiveRowExchangeEvent> exchangeEventUpdateList = new ArrayList<SGLastFiveRowExchangeEvent>();
		    	List<SGLastFiveRowExchangeEvent> exchangeEventList=DAORegistry.getSgLastFiveRowExchangeEventDAO()
		    	.getSGLastFiveRowExchangeEventByVenueAndExchangeBrokerId(excludeVenues.getVenueId(),excludeVenues.getBrokerId());
		    	
		    	for (SGLastFiveRowExchangeEvent miniExchangeEvent : exchangeEventList) {
		    		SGLastFiveRowExchangeEventAudit  miniExchangeEventAudit=new SGLastFiveRowExchangeEventAudit(miniExchangeEvent);
					exchangeEventAuditList.add(miniExchangeEventAudit);
					
					if(miniExchangeEvent.getTicketNetworkBrokerId()!=null){
					  if(miniExchangeEvent.getTicketNetworkBrokerId().equals(excludeVenues.getBrokerId())){					  
						  miniExchangeEvent.setTicketNetworkBrokerId(null);
					  }
					}
					if(miniExchangeEvent.getScoreBigBrokerId()!=null){
					  if(miniExchangeEvent.getScoreBigBrokerId().equals(excludeVenues.getBrokerId())){					  
						  miniExchangeEvent.setScoreBigBrokerId(null);
					  }
					}
					if(miniExchangeEvent.getVividBrokerId()!=null){
					  if(miniExchangeEvent.getVividBrokerId().equals(excludeVenues.getBrokerId())){					  
						  miniExchangeEvent.setVividBrokerId(null);
					  }
					}
					if(miniExchangeEvent.getFanxchangeBrokerId()!=null){
					  if(miniExchangeEvent.getFanxchangeBrokerId().equals(excludeVenues.getBrokerId())){					  
						  miniExchangeEvent.setFanxchangeBrokerId(null);
					  }
					}
					if(miniExchangeEvent.getTicketcityBrokerId()!=null){
						  if(miniExchangeEvent.getTicketcityBrokerId().equals(excludeVenues.getBrokerId())){					  
							  miniExchangeEvent.setTicketcityBrokerId(null);
						  }
						}
					if(miniExchangeEvent.getSeatGeekBrokerId()!=null){
						  if(miniExchangeEvent.getSeatGeekBrokerId().equals(excludeVenues.getBrokerId())){					  
							  miniExchangeEvent.setSeatGeekBrokerId(null);
						  }
						}
					miniExchangeEvent.setLastUpdatedBy(userName);
					miniExchangeEvent.setLastUpdatedDate(new Date());
					exchangeEventUpdateList.add(miniExchangeEvent);
				}
		    	DAORegistry.getSgLastFiveRowExchangeEventDAO().updateAll(exchangeEventUpdateList);
		    	DAORegistry.getSgLastFiveRowExchangeEventAuditDAO().saveAll(exchangeEventAuditList);
		    	
		    }
		}	
	}
	@RequestMapping(value="/ExcludeVenuesAudit")
	public ModelAndView loadExcludeVenuesAudit(
				HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			ModelAndView mav = new ModelAndView(
					"page-exclude-venues-audit-popup");
			Integer brokerId = Integer.parseInt(request.getParameter("bId").trim());
			Integer productId = Integer.parseInt(request.getParameter("pId").trim());
			Integer venueId = Integer.parseInt(request.getParameter("vId").trim());
			
			try{
			     List<ExcludeVenuesAudit> auditList = DAORegistry.getExcludeVenuesAuditDAO().getExcludeVenuesAuditList(brokerId, productId, venueId);
			     ExcludeVenuesAudit excludeVenuesAudit=DAORegistry.getExcludeVenuesAuditDAO().getExcludeVenuesAudit(brokerId, productId, venueId);
			     mav.addObject("auditList", auditList);
			     mav.addObject("excludeVenuesAudit", excludeVenuesAudit);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return mav;
		}	
	
	@RequestMapping(value="/AutopricingVenuesettings")	
	public ModelAndView geAutopricingVenuesettings(HttpServletRequest request,Model model,HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("page-autopricing-venue-settings");
		try {
			Collection<AutopricingProduct> autopricingProductList = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductForVenueSettings();
			
			String[] selectedProductIdStr = request.getParameterValues("productId");
			String venueId=request.getParameter("venueId");
			String isAllProduct = request.getParameter("isAllProduct");
			String selectedValue="";
			String selectedOption="";
			List<Integer> selectedProductId = new ArrayList<Integer>();
			
			if(venueId!=null && venueId!=""){
				Venue venue=DAORegistry.getVenueDAO().get(Integer.parseInt(venueId));
				selectedOption="Venue";
				selectedValue= venue.getBuilding()+" "+venue.getCity()+" "+venue.getState()+" "+venue.getCountry();
			}
			if (selectedProductIdStr != null && selectedProductIdStr.length != 0) {
				for (String productIdStr : selectedProductIdStr) {
					if (productIdStr.isEmpty()) {
						continue;
					}
					selectedProductId.add(Integer.parseInt(productIdStr));
				}
			}
			
			if(venueId!=null && venueId!=""){
				Venue venue=DAORegistry.getVenueDAO().get(Integer.parseInt(venueId));
				selectedOption="Venue";
				selectedValue= venue.getBuilding()+" "+venue.getCity()+" "+venue.getState()+" "+venue.getCountry();
			}
			String action = request.getParameter("action");
			String info = "";
			
			if(action != null) {
	            if (action.equals("save")) {
	            	List<AutopricingVenueSettings> autopricingVenueSettingsList = new ArrayList<AutopricingVenueSettings>();
	            	List<AutopricingVenueSettingsAudit> audits = new ArrayList<AutopricingVenueSettingsAudit>();

	            	Collection<AutopricingVenueSettings> autopricingVenueSettingsFromDB = DAORegistry.getAutopricingVenueSettingsDAO().getAutopricingVenueSettingsByVenueId(Integer.parseInt(venueId));
					Map<String,AutopricingVenueSettings> autopricingVenueSettingsFromDBMap = new HashMap<String,AutopricingVenueSettings>();
					for (AutopricingVenueSettings apVenueSettings : autopricingVenueSettingsFromDB) {
						if(!selectedProductId.contains(apVenueSettings.getProductId())) {
							continue;
						}
						String key =  apVenueSettings.getProductId()+"_"+apVenueSettings.getVenueId();
						autopricingVenueSettingsFromDBMap.put(key,apVenueSettings);
					}
					
					Boolean isUpdateExistingEvents = false;
					String isUpdateExistingEventStr = request.getParameter("update_existinge_events");
					if(isUpdateExistingEventStr!=null && isUpdateExistingEventStr.equals("Y")){
						isUpdateExistingEvents = true;
					}
					
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
					Set<String> params=request.getParameterMap().keySet();
					for (String param : params) {
						if(param.contains("checkbox_")){
							String temp[] = param.split("_");
							Integer productId = Integer.parseInt(temp[1].trim());
							Integer venueIds = Integer.parseInt(temp[2].trim());
							String key=productId+"_"+venueIds;
							String auditAction;
							AutopricingVenueSettings autopricingVenueSettings = autopricingVenueSettingsFromDBMap.remove(key);
							if(autopricingVenueSettings == null) {
								autopricingVenueSettings = new AutopricingVenueSettings();
								auditAction = "CREATED";
						    	
							} else {
								auditAction = "UPDATED";
							}
							autopricingVenueSettings.setVenueId(venueIds);
							autopricingVenueSettings.setProductId(productId);
							autopricingVenueSettings.setLastUpdatedBy(username);
							autopricingVenueSettings.setLastUpdatedDate(now);
							autopricingVenueSettings.setStatus("ACTIVE");
							
							if(request.getParameter("shipping_method_"+key) != null && request.getParameter("shipping_method_"+key)!=""){
								autopricingVenueSettings.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+key)));
							}else{
								autopricingVenueSettings.setShippingMethod(null);
							}
							if(request.getParameter("near_term_display_option_"+key) != null && request.getParameter("near_term_display_option_"+key)!=""){
								autopricingVenueSettings.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+key)));
							}else{
								autopricingVenueSettings.setNearTermDisplayOption(null);
							}
							if(request.getParameter("shipping_days_"+key) != null && request.getParameter("shipping_days_"+key)!=""){
								autopricingVenueSettings.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+key)));
							}else{
								autopricingVenueSettings.setShippingDays(null);
							}
							//if markup is 0 then consider it as null for to read markup from events.
							Double markup = 0.0;
							String markupStr = request.getParameter("markup_"+key);
							if(markupStr != null && markupStr!=""){
								markup = Double.parseDouble(markupStr);
								//if(markup<=0) {
									//autopricingVenueSettings.setMarkup(null);
								//} else {
									autopricingVenueSettings.setMarkup(markup);
								//}
							}else {
								autopricingVenueSettings.setMarkup(null);
							}
							
							AutopricingVenueSettingsAudit audit = new AutopricingVenueSettingsAudit(autopricingVenueSettings);
							audit.setAction(auditAction);
							audit.setExistingEventUpdate(isUpdateExistingEvents);
							audits.add(audit);
							
							autopricingVenueSettingsList.add(autopricingVenueSettings);
						}			  
					}
					//Update changed records value for existing events
					if(isUpdateExistingEvents) {
						updateAutopriicngVenueSettingsToExistingEvents(autopricingVenueSettingsList);
					}
					DAORegistry.getAutopricingVenueSettingsDAO().saveOrUpdateAll(autopricingVenueSettingsList);
					
					List<AutopricingVenueSettings> tobeDeletedList = new ArrayList<AutopricingVenueSettings>();
					for (AutopricingVenueSettings autopricingVenueSettings : autopricingVenueSettingsFromDBMap.values()) {
						autopricingVenueSettings.setShippingMethod(null);
						autopricingVenueSettings.setNearTermDisplayOption(null);
						autopricingVenueSettings.setMarkup(null);
						autopricingVenueSettings.setShippingDays(null);
						autopricingVenueSettings.setLastUpdatedBy(username);
						autopricingVenueSettings.setLastUpdatedDate(now);
						autopricingVenueSettings.setStatus("DELETED");
						tobeDeletedList.add(autopricingVenueSettings);
						
						AutopricingVenueSettingsAudit audit = new AutopricingVenueSettingsAudit(autopricingVenueSettings);
						audit.setAction("DELETED");
						audit.setExistingEventUpdate(false);
						audits.add(audit);
					}
					
					//Update deleted records value for existing events
					if(isUpdateExistingEvents) {
						//updateAutopriicngVenueSettingsToExistingEvents(tobeDeletedList);
					}
					DAORegistry.getAutopricingVenueSettingsDAO().deleteAll(tobeDeletedList);
					DAORegistry.getAutopricingVenueSettingsAuditDAO().saveOrUpdateAll(audits);
					
					
				  
					info = "Autopricing venue settings saved successfully.";
					action = "search";							
	            }
				if(action.equals("search")&& venueId!="") {
					List<AutopricingVenueSettings> autopricingVenueSettings = new ArrayList<AutopricingVenueSettings>();

					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueId));
					Collection<AutopricingVenueSettings> autopricingVenueSettingsFromDB = DAORegistry.getAutopricingVenueSettingsDAO().getAutopricingVenueSettingsByVenueId(Integer.parseInt(venueId));
					Map<String,AutopricingVenueSettings> autopricingVenueSettingsFromDBMap = new HashMap<String,AutopricingVenueSettings>();
					for (AutopricingVenueSettings apVenueSettings : autopricingVenueSettingsFromDB) {
						if(!selectedProductId.contains(apVenueSettings.getProductId())) {
							continue;
						}
						String key =  apVenueSettings.getProductId()+"_"+apVenueSettings.getVenueId();
						autopricingVenueSettingsFromDBMap.put(key,apVenueSettings);
					}
					
					for (AutopricingProduct autopricingProduct : autopricingProductList) {
						if(!selectedProductId.contains(autopricingProduct.getId())) {
							continue;
						}
						String key = autopricingProduct.getId()+"_"+venueId;
						AutopricingVenueSettings apVenueSettings = autopricingVenueSettingsFromDBMap.get(key);
								
						if(apVenueSettings  == null) {
							apVenueSettings  = new  AutopricingVenueSettings ();
							apVenueSettings.setProductId(autopricingProduct.getId());
							apVenueSettings.setVenueId(Integer.valueOf(venueId));
							apVenueSettings.setProduct(autopricingProduct);
							apVenueSettings.setVenue(venue);
						}
								
						autopricingVenueSettings.add(apVenueSettings);
					}
					mav.addObject("autopricingVenueSettings", autopricingVenueSettings);
				}
				
			}
			mav.addObject("productList", autopricingProductList);
			mav.addObject("isAllProductSelected", isAllProduct);
			mav.addObject("selectedProductId", selectedProductIdStr);
			mav.addObject("selectedValue", selectedValue);
			mav.addObject("selectedOption", selectedOption);
			mav.addObject("venueId",venueId);
			mav.addObject("info", info);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	void updateAutopriicngVenueSettingsToExistingEvents(List<AutopricingVenueSettings> autopricngVenueSettings) throws Exception {
		
		for (AutopricingVenueSettings autopricngVenueSetting : autopricngVenueSettings) {
			
			if(autopricngVenueSetting.getProductId().equals(3)) { //Minicats
				DAORegistry.getMiniExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(4)) {//VIP MiniCats
				DAORegistry.getVipMiniExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(5)) {//LastRow MiniCats
				DAORegistry.getLastRowMiniExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(6)) {//LarryLast
				DAORegistry.getLarryLastExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(7)) {//Zoned LastRow MiniCats
				DAORegistry.getZoneLastRowMiniExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(8)) {//ZonesPricing
				DAORegistry.getZonePricingExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(10)) {//AutoCats96
				DAORegistry.getAutoCats96ExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
			
			} else if(autopricngVenueSetting.getProductId().equals(11)) {//PresaleAutoCat
				DAORegistry.getPresaleAutoCatExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(14)) {//RTWZonesPricing
				DAORegistry.getTixCityZonePricingExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(15)) {//MHZonePricing
				DAORegistry.getManhattanZonePricingExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(16)) {//VIPlastrowcats
				DAORegistry.getVipLastRowMiniExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(19)) {//SGLastFiveRow
				DAORegistry.getSgLastFiveRowExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			} else if(autopricngVenueSetting.getProductId().equals(9)) {//RewardTheFan Listings
				DAORegistry.getZoneTicketsProcessorExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			}
			/*else if(autopricngVenueSetting.getProductId().equals(17)) {//Presale Zone Tickets
				DAORegistry.getPresaleZoneTicketsExchangeEventDAO().updateAutopricingVenueSettings(autopricngVenueSetting);
				
			}*/
		}
	}
	
	@RequestMapping(value="/AutopricingVenuesettingsAudit")
	public ModelAndView loadAutopricingVenuesettingsAudit(
				HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			ModelAndView mav = new ModelAndView(
					"page-autopricing-venue-settings-audit-popup");
			
			try{
				Integer productId = Integer.parseInt(request.getParameter("pId").trim());
				Integer venueId = Integer.parseInt(request.getParameter("vId").trim());
				
				Venue venue = DAORegistry.getVenueDAO().get(venueId);
				AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().get(productId);
			     List<AutopricingVenueSettingsAudit> auditList = DAORegistry.getAutopricingVenueSettingsAuditDAO().getAutopricingVenueSettingsAuditList(productId, venueId);
			     mav.addObject("auditList", auditList);
			     mav.addObject("venue", venue);
			     mav.addObject("product", autopricingProduct);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return mav;
		}
	
	@RequestMapping(value="/DownloadTNVipLastRowMiniCategoryTickets")
	public void downloadVipLastRowMiniCatsCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		String brokerId=request.getParameter("brokerId");
		
		if(brokerId.equals("3")) {
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=ROTVipLastRowMiniCatsTNCategoryTickets."
							+ today + ".csv");
		} else if(brokerId.equals("5")) {
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityVipLastRowMiniCatsTNCategoryTickets."
							+ today + ".csv");
		}
		
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");
	     
		
		try {
			OutputStream out = response.getOutputStream();
			out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
					.getBytes());

			List<VWLastRowMiniCatsCategoryTicket>  tixs=DAORegistry.getQueryManagerDAO().getTnVipLastRowMiniCatsCatsCategoryTickets(brokerId);
			
		/*	Collection<VWLastRowMiniCatsCategoryTicket> tixs = DAORegistry
					.getVwLastRowMiniCatsCategoryTicketDAO()
					.getAllActiveTnLastRowMiniCatsCatsCategoryTickets();*/

			//DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			//DateFormat tf = new SimpleDateFormat("hh:mm aa");
			// DateFormat dateTimeformat = new
			// SimpleDateFormat("MM/dd/yyyy HH:mm");

		    String dateInString=null;
		    String newDate=null;     
		    					
		    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		    
			for (VWLastRowMiniCatsCategoryTicket cat : tixs) {
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenueName().replaceAll(",", "-") + ",";
				outString += cat.getQuantity() + ",";
				// outString += cat.getZone() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getLastRow() + ".,";
				outString += cat.getTnPrice() + "\n";
				/*outString += cat.getExpectedArrivalDate() + ",";
				outString += shippingMap.get(cat.getShippingMethodId()) + ",";
				outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
						+ "\n";*/
				out.write(outString.getBytes());
			}

		} catch (Exception e) {
			System.out.println(e.fillInStackTrace());
			throw e;
		}
	}
	
	@RequestMapping(value="/ManhattanZonesPricingCatPriceHistoryPopup")
	public ModelAndView loadManhattanZonesPricingCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-manhattan-zonespricing-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		ManhattanZonesPricingCategoryTicket categoryTicket = DAORegistry.getManhattanZonesPricingCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<ManhattanZonesPricingCategoryTicket> ticketPriceList = new ArrayList<ManhattanZonesPricingCategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			ManhattanZonesPricingCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");

				for (String string : array) {
					str = string.split("-");
					String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String price = str[3];
					ticketPriceObj = new ManhattanZonesPricingCategoryTicket();
					ticketPriceObj.setPopDate(date);
					ticketPriceObj.setPopPrice(price);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new MiniCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}

	@RequestMapping(value="/ManhattanZonesPricingTMATTicketPriceHistoryPopup")
	public ModelAndView loadManhattanZonesPricingTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-manhattan-zonespricing-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		ManhattanZonesPricingCategoryTicket tgcatTicket = DAORegistry.getManhattanZonesPricingCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}

	@RequestMapping(value="/VipLastRowCategoryPriceHistoryPopup")
	public ModelAndView loadVipLastRowCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-vip-lastrow-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		System.out.println("eventId :" + eventId);
		String tmatId = request.getParameter("tId");
		System.out.println("tmatid :" + tmatId);
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		VipLastRowMiniCategoryTicket categoryTicket = DAORegistry.getVipLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<VipLastRowMiniCategoryTicket> ticketPriceList = new ArrayList<VipLastRowMiniCategoryTicket>();
		
		if(categoryTicket != null) {
			String pricehistory = categoryTicket.getPriceHistory();
			VipLastRowMiniCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");

				for (String string : array) {
					str = string.split("-");
					String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String rice = str[3];
					ticketPriceObj = new VipLastRowMiniCategoryTicket();
					ticketPriceObj.setPopDate(ate);
					ticketPriceObj.setPopPrice(rice);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new LastRowMiniCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket
						.getLastUpdated().toString()).substring(0,
						(categoryTicket.getLastUpdated().toString())
								.indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket
						. getActualPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}
	@RequestMapping(value="/VipLastRowTMATTicketPriceHistoryPopup")
	public ModelAndView loadVipLastRowTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-vip-lastrow-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		VipLastRowMiniCategoryTicket tgcatTicket = DAORegistry.getVipLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
				    Ticket ticketInfo=null;
				    try{
					 ticketInfo = DAORegistry.getTicketDAO().get(ticId);
				    }catch(Exception e){
				    	e.printStackTrace();
				    }
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}

	@RequestMapping("/BrokerTicketsCountProductWisePopup")
	public ModelAndView brokerTicketsCountProductWisePopup(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, Exception {
		ModelAndView mav = new ModelAndView("page-broker-ticket-count-product-wise-popup");
		String brokerId = request.getParameter("brokerId");
		if(brokerId != null && !brokerId.equals("")){
			String catInternalNotesCombined = BrokerUtils.getActiveAndUniqueCategoryInternalNotes();
			String ssAccountInternalNotesCombined = BrokerUtils.getActiveAndUniqueSSAccountInternalNotes();
			int catCount = 0;
			int ssCount = 0;
			Broker broker = DAORegistry.getBrokerDAO().get(Integer.parseInt(brokerId));
			List<Object[]> catList = InduxDAORegistry.getPosCategoryTicketGroupDAO().getBrokerCategoryTicketCountProductWise(broker.getPosBrokerId(), catInternalNotesCombined);
			List<Object[]> ssList = InduxDAORegistry.getPosCategoryTicketGroupDAO().getBrokerSSAccountTicketCountProductWise(broker, ssAccountInternalNotesCombined);
			for(Object[] obj : catList){
				if(obj[1] != null){
					catCount += Integer.parseInt(""+obj[1]);
				}else{
					obj[1] = 0;
				}
			}
			
			for(Object[] obj : ssList){
				if(obj[1] != null){
					ssCount += Integer.parseInt(""+obj[1]);
				}else{
					obj[1] = 0;
				}
			}
			Collection<AutopricingProduct> products = DAORegistry.getAutopricingProductDAO().getAllActiveAutopricingProducts();
			Map<String, String> map = new HashMap<String, String>();
			for(AutopricingProduct product : products){
				String arr[] = product.getInternalNotes().split(",");
				if(arr.length > 0) {
					for (String intNotes : arr) {
						map.put(intNotes, product.getName());					
					}
				} else {
					map.put(product.getInternalNotes(), product.getName());
				}
			}
			catList.add(new Object[]{"TOTAL", catCount});
			ssList.add(new Object[]{"TOTAL", ssCount});
			
			mav.addObject("catList", catList);
			mav.addObject("ssList", ssList);
			mav.addObject("broker", broker);
			mav.addObject("map", map);
		}
		
		return mav;
	}
	
	@RequestMapping(value="/PresaleZoneTicketsCatPriceHistoryPopup")
	public ModelAndView loadPresaleZoneTicketsCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-zonetickets-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		PresaleZoneTicketsCategoryTicket categoryTicket = DAORegistry.getPresaleZoneTicketsCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<PresaleZoneTicketsCategoryTicket> ticketPriceList = new ArrayList<PresaleZoneTicketsCategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			PresaleZoneTicketsCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");

				for (String string : array) {
					str = string.split("-");
					String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String price = str[3];
					ticketPriceObj = new PresaleZoneTicketsCategoryTicket();
					ticketPriceObj.setPopDate(date);
					ticketPriceObj.setPopPrice(price);
					ticketPriceList.add(ticketPriceObj);
				}
			} 
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}
	
	
	@RequestMapping(value="/PresaleZoneTicketsTMATTicketPriceHistoryPopup")
	public ModelAndView loadPresaleZoneTicketsTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-zonetickets-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		PresaleZoneTicketsCategoryTicket tgcatTicket = DAORegistry.getPresaleZoneTicketsCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		return mav;
	}

	@RequestMapping(value="/ResetBrokerConnection")
	public void getCrawlsDetailsForEvent(HttpServletRequest request,
			HttpServletResponse response)throws ServletException, IOException {
		try {
			Date startDate = new Date();
			Connections.getTixcityInduxConnection(true);
			Boolean flag = InduxDAORegistry.getPosEventDAO().checkPOSConnection(AutoExchangeEventLoader.POS_TIXCITY_BROKER_ID);
			
			String str = "TIXCITY Connection recreated : "+flag+" : "+(new Date().getTime()-startDate.getTime())+ new Date();
			System.out.println(str);
			
			PrintWriter writer =  response.getWriter();
			writer.write(str);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
