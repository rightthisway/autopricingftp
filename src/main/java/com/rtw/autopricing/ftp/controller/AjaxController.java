package com.rtw.autopricing.ftp.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.hibernate.JDBCException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Artist;
import com.rtw.autopricing.ftp.data.ChildCategory;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.GrandChildCategory;
import com.rtw.autopricing.ftp.data.Venue;
import com.rtw.autopricing.ftp.data.VenueCategory;


@Controller
@RequestMapping({"/AutoCompleteVenue","/LarryLastEventController","/AutoCompleteGrandChildAndArtistAndVenue","/GetEventsByGrandChildAndArtistAndVenue",
	"/GetEventsByVenueCategory","/GetVenueCategoriesByVenue","/AutoCompleteArtistWithEvents","/AutoCompleteVenueWithEvents","AutoCompleteVenuesForArtistWithActiveEvents"})
public class AjaxController {
	
	@RequestMapping("/GetEventsByGrandChildAndArtistAndVenue")
		public void getEventsByGrandChildAndArtistAndVenue(
				HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException, JSONException {

			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String grandChildId = request.getParameter("grandChildId");
			String childId = request.getParameter("childId");
			String brokerId = request.getParameter("brokerId");
			JSONArray jsonArray = new JSONArray();
			try {
				
			
			Collection<Event> events = null;
			if (brokerId != null && !brokerId.isEmpty()) {
				if (artistId != null) {
					try{
						events = DAORegistry.getEventDAO().getAllActiveEventsByArtistIdAndBrokerId(Integer.parseInt(artistId), Integer.parseInt(brokerId));
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				if (venueId != null) {
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueId), Integer.parseInt(brokerId));
				}
				if (childId != null) {
					events = DAORegistry.getEventDAO().getAllEventsByChildCategory(Integer.parseInt(childId), Integer.parseInt(brokerId));
				}
				if (grandChildId != null) {
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildId),Integer.parseInt(brokerId));
				}
			} else {
				if (artistId != null) {
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistId));
				}
				if (venueId != null) {
					events = DAORegistry.getEventDAO().getAllEventsByVenue(
							Integer.parseInt(venueId));
				}
				if (childId != null) {
					events = DAORegistry.getEventDAO().getAllEventsByChildCategory(
							Integer.parseInt(childId));
				}
				if (grandChildId != null) {
					events = DAORegistry.getEventDAO()
							.getAllEventsByGrandChildCategoryOrderdByEventDate(
									Integer.parseInt(grandChildId));
				}
			}

			if (events != null) {
				for (Event event : events) {

					/*
					 * if(event.getAdmitoneId() == null ||
					 * event.getVenueCategoryId() == null) { continue; }
					 */

					JSONObject jObject = new JSONObject();
					jObject.put("id", event.getId());
					jObject.put("name", event.getName());

					Format formatter = new SimpleDateFormat("MM/dd/yyyy");
					Date date = event.getLocalDate();
					String eventDate = "TBD";
					if (date != null) {
						eventDate = formatter.format(date);
					}

					formatter = new SimpleDateFormat("hh:mm aa");
					Time eventTime = event.getLocalTime();
					String time = "TBD";
					if (eventTime != null) {
						time = formatter.format(eventTime);
					}
					jObject.put("date", eventDate);
					jObject.put("time", time);
					jObject.put("venue", event.getVenue().getBuilding());
					jObject.put("city", event.getVenue().getCity());
					jObject.put("state", event.getVenue().getState());
//					System.out.println("Event Venue Is:" + event.getVenue().getBuilding());
					// jObject.put("venue", event.venue.getcountry());

					jsonArray.put(jObject);

				}
			}
			} catch (Exception e) {
				e.printStackTrace();
			}
			IOUtils.write(jsonArray.toString().getBytes(),
					response.getOutputStream());

	}
	@RequestMapping("/GetEventsByVenueCategory")
	public void getEventsByVenueCategory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, JSONException {

		String venueCategoryId = request.getParameter("venueCategoryId");
		JSONArray jsonArray = new JSONArray();
		try {
			Collection<Event> events = null;

			if (venueCategoryId != null && venueCategoryId.trim().length() > 0) {
				events = DAORegistry.getEventDAO().getAllActiveEventsByVenueCategoryId(Integer.parseInt(venueCategoryId));
			}
			if (events != null) {
				for (Event event : events) {

					/*
					 * if(event.getAdmitoneId() == null ||
					 * event.getVenueCategoryId() == null) { continue; }
					 */
	
					JSONObject jObject = new JSONObject();
					jObject.put("id", event.getId());
					jObject.put("name", event.getName());
	
					Format formatter = new SimpleDateFormat("MM/dd/yyyy");
					Date date = event.getLocalDate();
					String eventDate = "TBD";
					if (date != null) {
						eventDate = formatter.format(date);
					}
	
					formatter = new SimpleDateFormat("hh:mm aa");
					Time eventTime = event.getLocalTime();
					String time = "TBD";
					if (eventTime != null) {
						time = formatter.format(eventTime);
					}
					jObject.put("date", eventDate);
					jObject.put("time", time);
					jObject.put("venue", event.getVenue().getBuilding());
					jObject.put("city", event.getVenue().getCity());
					jObject.put("state", event.getVenue().getState());
	//				System.out.println("Event Venue Is:" + event.getVenue().getBuilding());
					// jObject.put("venue", event.venue.getcountry());
	
					jsonArray.put(jObject);
	
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		IOUtils.write(jsonArray.toString().getBytes(),
				response.getOutputStream());

	}
	@RequestMapping("/GetVenueCategoriesByVenue")
	public void getVenueCategoriesByVenue(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, JSONException {

		String venueId = request.getParameter("venueId");
		JSONArray jsonArray = new JSONArray();
		try {
			Collection<VenueCategory> venueCategories = null;

			if (venueId != null && venueId.trim().length() > 0) {
				venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(Integer.parseInt(venueId));
			}
			if (venueCategories != null) {
				for (VenueCategory venueCategory : venueCategories) {

					JSONObject jObject = new JSONObject();
					jObject.put("id", venueCategory.getId());
					jObject.put("categoryGroup", venueCategory.getCategoryGroup());
	
					jsonArray.put(jObject);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		IOUtils.write(jsonArray.toString().getBytes(),
				response.getOutputStream());

	}
@RequestMapping("/AutoCompleteVenue")
 public void getAutoCompleteVenues(HttpServletRequest request, HttpServletResponse response) throws IOException{
	      String param = request.getParameter("q");
	  	  String strResponse ="";
	  	  PrintWriter writer =  response.getWriter();
	      Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(
	            				param);
	      if (venues != null) {
				for (Venue venue : venues) {
					strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding()+" "+venue.getCity()+" "+venue.getState()+" "+venue.getCountry() + "\n");
				}
			}
	      writer.write(strResponse);               
 }
	@RequestMapping("/AutoCompleteGrandChildAndArtistAndVenue")
	public void getAutoCompleteGrandChildAndArtistAndVenue(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");
//		String brokerId = request.getParameter("brokerId");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
		Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(
				param);
		Collection<GrandChildCategory> grandChildCategories = DAORegistry
				.getGrandChildCategoryDAO().getGrandChildCategoriesByName(
						param);
		Collection<ChildCategory> childCategories = DAORegistry
				.getChildCategoryDAO().getChildTourCategoriesByName(param);

		if (artists == null && venues == null && grandChildCategories == null) {
			return;
		}
		if (artists != null) {
			for (Artist artist : artists) {
				strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
			}
		}
		if (venues != null) {
			for (Venue venue : venues) {
				strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding() + "\n");
			}
		}
		if (grandChildCategories != null) {
			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
				strResponse += ("GRANDCHILD" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
			}
		}

		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}
		
		writer.write(strResponse);

	}
	
	@RequestMapping("/AutoCompleteArtistWithEvents")
	 public void getAutoCompleteArtistWithEvents(HttpServletRequest request, HttpServletResponse response) throws IOException{
		      String param = request.getParameter("q");
		  	  String strResponse ="";
		  	  PrintWriter writer =  response.getWriter();		  
		  	  Collection<Artist> artists = 	(List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtistsWithEvents(param);
		  	
		      if (artists != null) {
					for (Artist artist : artists) {						 
						strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
					}
				}
		      writer.write(strResponse);               
	 }
	
	@RequestMapping("/AutoCompleteVenueWithEvents")
	 public void getAutoCompleteVenueWithEvents(HttpServletRequest request, HttpServletResponse response) throws IOException{
		     
		      String param = request.getParameter("q");
		  	  String strResponse ="";
		  	 
		  	  PrintWriter writer =  response.getWriter();
		  	 try{
		  	  Collection<Venue> venues = 	(List<Venue>) DAORegistry.getVenueDAO().getVenuesWithActiveEvents(param);
		  	
		      if (venues != null) {
					for (Venue venue : venues) {						  
						  strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding()+" "+venue.getCity()+" "+venue.getState()+" "+venue.getCountry() + "\n");
					}
				}
		  	  }
		  	   catch(Exception ex){		  		
		  		 ex.printStackTrace();
		  	  }
		      writer.write(strResponse);               
	 }
	
	
	@RequestMapping("/AutoCompleteVenuesForArtistWithActiveEvents")
	 public void getAutoCompleteVenuesForArtistWithActiveEvents(HttpServletRequest request, HttpServletResponse response) throws IOException{
		     
		      String param = request.getParameter("q");
		      JSONArray jsonArray = new JSONArray();		  	 
		  	try{
		  	  Collection<Venue> venues = 	(List<Venue>) DAORegistry.getVenueDAO().getVenuesForArtistWithActiveEvents(param);
		  	
		      if (venues != null) {
					for (Venue venue : venues) {
						  JSONObject jObject = new JSONObject();
						  jObject.put("id", venue.getId());
						  jObject.put("building", venue.getBuilding());
						  jObject.put("city", venue.getCity());
						  jObject.put("state", venue.getState());
						  jObject.put("country", venue.getCountry());			
						  jsonArray.put(jObject);					  
						
					}
				}
		  	 
		  	} catch (Exception e) {
				e.printStackTrace();
			}
			IOUtils.write(jsonArray.toString().getBytes(),
					response.getOutputStream());
		      
		      
	 }
	
	
	
}
