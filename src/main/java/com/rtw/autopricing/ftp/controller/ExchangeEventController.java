package com.rtw.autopricing.ftp.controller;

//import javax.servlet.http.HttpServletResponse;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Artist;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEvent;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEventAudit;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.GrandChildCategory;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEventAudit;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEventAudit;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEventAudit;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEvent;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEventAudit;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEvent;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEventAudit;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.Venue;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEventAudit;
import com.rtw.autopricing.ftp.zonetickets.dao.implementaion.ZoneTicketDAORegistry;
import com.rtw.autopricing.util.BrokerUtils;
import com.rtw.autopricing.util.RemoveZoneEvent;
import com.rtw.autopricing.util.StringUtil;


@Controller
@RequestMapping({"/ManagePresaleAutoCatExchangeEvent","/ManageMiniExchangeEvent","/LastRowMiniExchangeEventAuditPopUp","/ManageLastRowMiniExchangeEvent","/ManageLarryLastExchangeEvent","GetEventsByGrandChildAndArtistAndVenue",
	"/ManageTNSpecialExchangeEvent","/TNSpecialExchangeEventAuditPopup","/LarryLastExchangeEventAuditPopup","/ManageVipMiniExchangeEvent","/VipMiniExchangeEventAuditPopup","/ZoneLastRowMiniExchangeEventAuditPopup",
	"/GetMiniExchangeEventAuditPopup","/GetPresaleAutoCatExchangeEventAuditPopup","/ManageZoneLastRowMiniExchangeEvent","/ManageZonePricingExchangeEvent","/ManageTixCityZonePricingExchangeEvent","/ZonePricingExchangeEventAuditPopup","/TixCityZonePricingExchangeEventAuditPopup","/ManageZoneTicketsProcessorExchangeEvent",
	"/ZoneTicketsProcessorExchangeEventAuditPopup","/ManageAutoCats96ExchangeEvent","/GetAutoCats96ExchangeEventAuditPopup","/ManageManhattanZonePricingExchangeEvent","/ManhattanZonePricingExchangeEventAuditPopup",
	"/ManageVipLastRowMiniExchangeEvent","/VipLastRowMiniExchangeEventAuditPopUp","/ManagePresaleZoneTicketsExchangeEvent","/PresaleZoneTicketsExchangeEventAuditPopup",
	"/ManageZoneLastRowExchangeEvent","/ZoneLastRowExchangeEventAuditPopup","/ManageSGLastFiveRowExchangeEvent","/GetSGLastFiveRowExchangeEventAuditPopup"})
public class ExchangeEventController {

	private static String zoneWSUrl;
	private static String zoneWSConfigId;
	
	@RequestMapping(value="/ManagePresaleAutoCatExchangeEvent")
	public String loadManagePresaleAutoCatExchangeEventPage(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
	//	ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		model.addAttribute("brokers", brokers);
		Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
		for(Broker broker:brokers){
			brokerMap.put(broker.getId(), broker);
		}
//		ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		String action = request.getParameter("action");
		Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(3);
		model.addAttribute("url", "ManagePresaleAutoCatExchangeEvent");
		model.addAttribute("auditUrl", "GetPresaleAutoCatExchangeEventAuditPopup");
		model.addAttribute("products", autopricingProducts);
		if(action==""){
			model.addAttribute("message", "Please select valid artist ");
		}
		String isAutoCats96catChangeFlagStr = request.getParameter("product-10");
		boolean  isAutoCats96catChangeFlag = ((isAutoCats96catChangeFlagStr !=null && !isAutoCats96catChangeFlagStr.equalsIgnoreCase("on") )?false:true);
		model.addAttribute("isAutocat96", isAutoCats96catChangeFlag); 
		   
		if(action!=null && action!=""){
			Collection<Event> events = null;
			Collection<PresaleAutoCatExchangeEvent> dbPresaleExchangeEventList= null;
			String artistStr = request.getParameter("artist");
			String[] eventStr = request.getParameterValues("events");
			String temp[] = eventStr;
			String venueStr = request.getParameter("venue");
			String grandChildStr = request.getParameter("grandChild");
//			String childStr = request.getParameter("child");
			String selectedEvent = "";
			String eventsCheckAll = request.getParameter("eventsCheckAll");
			boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			Integer selectedId = null;

			   Collection<AutoCats96ExchangeEvent> dbAutoCats96ExchangeEventList= null;
			   
			if(artistStr!=null && !artistStr.isEmpty()){
				Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
				dbPresaleExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByArtistId(Integer.valueOf(artistStr));
				dbAutoCats96ExchangeEventList  = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByArtistId(Integer.valueOf(artistStr));
				selectedOption="Artist";
				selectedValue= artist.getName();
				selectedId = artist.getId();
			}else if(venueStr!=null && !venueStr.isEmpty()){
				Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
				dbPresaleExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByVenueId(Integer.valueOf(venueStr));
				dbAutoCats96ExchangeEventList  = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByVenueId(Integer.valueOf(venueStr));
				selectedOption="Venue";
				selectedValue= venue.getBuilding();
				events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
				selectedId = venue.getId();
			}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
				GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
				dbPresaleExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
				dbAutoCats96ExchangeEventList  = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
				selectedOption="GrandChildCategory";
				selectedValue= grandChildCategory.getName();
				events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
				selectedId = grandChildCategory.getId();
			}
			
			String brokerIdStr = request.getParameter("brokerId");

			model.addAttribute("broker", brokerIdStr);
			
		   Map<Integer,PresaleAutoCatExchangeEvent> dbPresaleExchangeEventMap = new HashMap<Integer, PresaleAutoCatExchangeEvent>();
		   Map<Integer,PresaleAutoCatExchangeEvent> dbPresaleExchangeEventUpdateMap = new HashMap<Integer, PresaleAutoCatExchangeEvent>();
		   
		   Map<Integer,AutoCats96ExchangeEvent> dbAutoCats96ExchangeEventMap = new HashMap<Integer, AutoCats96ExchangeEvent>();
		   Map<Integer,AutoCats96ExchangeEvent> dbAutoCats96ExchangeEventUpdateMap = new HashMap<Integer, AutoCats96ExchangeEvent>();
		   
		   
			List<PresaleAutoCatExchangeEvent> exchangeEventList = new ArrayList<PresaleAutoCatExchangeEvent>();
			List<PresaleAutoCatExchangeEvent> exchangeEventUpdateList = new ArrayList<PresaleAutoCatExchangeEvent>();
			for(PresaleAutoCatExchangeEvent presaleExchangeEvent:dbPresaleExchangeEventList){
				dbPresaleExchangeEventMap.put(presaleExchangeEvent.getEvent().getId(), presaleExchangeEvent);
				dbPresaleExchangeEventUpdateMap.put(presaleExchangeEvent.getEvent().getId(), presaleExchangeEvent);
			}
			
			for(AutoCats96ExchangeEvent autocats96ExchangeEvent:dbAutoCats96ExchangeEventList){
				dbAutoCats96ExchangeEventMap.put(autocats96ExchangeEvent.getEvent().getId(), autocats96ExchangeEvent);
				dbAutoCats96ExchangeEventUpdateMap.put(autocats96ExchangeEvent.getEvent().getId(), autocats96ExchangeEvent);
			}
			
			List<PresaleAutoCatExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<PresaleAutoCatExchangeEventAudit>();
			List<Integer> productIdList = new ArrayList<Integer>();
			List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
			if(action.equalsIgnoreCase("update")){
//				List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
				String userName = SecurityContextHolder.getContext().getAuthentication().getName();
				List<AutoCats96ExchangeEvent> autoCats96ExchangeEventUpdateList = new ArrayList<AutoCats96ExchangeEvent>();
				List<AutoCats96ExchangeEventAudit> autoCats96exchangeEventAuditUpdateList = new ArrayList<AutoCats96ExchangeEventAudit>();
				Map<String, String[]> requestParams = request.getParameterMap();
				Date now = new Date();
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					if(key.contains("product")){
						productIdList.add(Integer.parseInt(key.replace("product-","")));
					}
					if(key.contains("checkbox_")){
						Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
						temp = StringUtil.removeFromArray(temp,eventId.toString());
						PresaleAutoCatExchangeEvent dbPresaleExchangeEvent = dbPresaleExchangeEventUpdateMap.remove(eventId);
						toBeUpdaetEventIdList.add(eventId);
						if(dbPresaleExchangeEvent!=null){
							PresaleAutoCatExchangeEventAudit audit = new PresaleAutoCatExchangeEventAudit(dbPresaleExchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							AutoCats96ExchangeEvent dbAutoCats96ExchangeEvent = dbAutoCats96ExchangeEventUpdateMap.remove(eventId);
							AutoCats96ExchangeEventAudit autoCats96ExchangeEventAudit = null;
							
							String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
							boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
							
							dbPresaleExchangeEvent.setAllowSectionRange(isAllowSectionRange);
							dbPresaleExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
							//dbMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
							dbPresaleExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
							dbPresaleExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
							dbPresaleExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
							dbPresaleExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbPresaleExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbPresaleExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
							dbPresaleExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
							dbPresaleExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
							dbPresaleExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
							dbPresaleExchangeEvent.setStatus("ACTIVE");
							
							if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
								dbPresaleExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
							}else{
								dbPresaleExchangeEvent.setShippingMethod(null);
							}
							
							String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
							boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setDiscountZone(isDiscountZone);
							
							if(isAutoCats96catChangeFlag && dbAutoCats96ExchangeEvent!=null ){
								if(autoCats96ExchangeEventAudit==null){
									autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(dbAutoCats96ExchangeEvent);
								}
								dbAutoCats96ExchangeEvent.setStatus("ACTIVE");
							}
							
							String vividSeatEnabledStr = request.getParameter("vividEnabled_"+eventId);
							boolean  vividSeatEnabled = ((vividSeatEnabledStr==null || !vividSeatEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setVividEnabled(vividSeatEnabled);
							
							if(isAutoCats96catChangeFlag && dbAutoCats96ExchangeEvent!=null && vividSeatEnabled!=dbAutoCats96ExchangeEvent.getVividEnabled()){
								if(autoCats96ExchangeEventAudit==null){
									autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(dbAutoCats96ExchangeEvent);
								}
								dbAutoCats96ExchangeEvent.setVividEnabled(vividSeatEnabled);
							}
							
							String rotEnabledStr = request.getParameter("rotEnabled_"+eventId);
							boolean  rotEnabled = ((rotEnabledStr==null || !rotEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setRotEnabled(rotEnabled);
							
							if(isAutoCats96catChangeFlag && dbAutoCats96ExchangeEvent!=null && rotEnabled!=dbAutoCats96ExchangeEvent.getRotEnabled()){
								if(autoCats96ExchangeEventAudit==null){
								 autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(dbAutoCats96ExchangeEvent);
								}
								 dbAutoCats96ExchangeEvent.setRotEnabled(rotEnabled);
							}
							
							String tixcityEnabledStr = request.getParameter("tixcityEnabled_"+eventId);
							boolean  tixcityEnabled = ((tixcityEnabledStr==null || !tixcityEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setTixcityEnabled(tixcityEnabled);
							
							if(isAutoCats96catChangeFlag && dbAutoCats96ExchangeEvent!=null && tixcityEnabled!=dbAutoCats96ExchangeEvent.getTixcityEnabled()){
								if(autoCats96ExchangeEventAudit==null){
								 autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(dbAutoCats96ExchangeEvent);
								}
								 dbAutoCats96ExchangeEvent.setTixcityEnabled(tixcityEnabled);
							}
							
							String rtwEnabledStr = request.getParameter("rtwEnabled_"+eventId);
							boolean  rtwEnabled = ((rtwEnabledStr==null || !rtwEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setRtwEnabled(rtwEnabled);
							
							if(isAutoCats96catChangeFlag && dbAutoCats96ExchangeEvent!=null && rtwEnabled!=dbAutoCats96ExchangeEvent.getRtwEnabled()){
								if(autoCats96ExchangeEventAudit==null){
								 autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(dbAutoCats96ExchangeEvent);
								}
								 dbAutoCats96ExchangeEvent.setRtwEnabled(rtwEnabled);
							}
							
							String minicatsEnabledStr = request.getParameter("minicatsEnabled_"+eventId);
							boolean  minicatsEnabled = ((minicatsEnabledStr==null || !minicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setMinicatsEnabled(minicatsEnabled);
							
							String lastrowMinicatsEnabledStr = request.getParameter("lastrowMinicatsEnabled_"+eventId);
							boolean  lastrowMinicatsEnabled = ((lastrowMinicatsEnabledStr==null || !lastrowMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setLastrowMinicatsEnabled(lastrowMinicatsEnabled);
							
							String vipMinicatsEnabledStr = request.getParameter("vipMinicatsEnabled_"+eventId);
							boolean  vipMinicatsEnabled = ((vipMinicatsEnabledStr==null || !vipMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setVipMinicatsEnabled(vipMinicatsEnabled);
							
							String presaleAutocatsEnabledStr = request.getParameter("presaleAutocatsEnabled_"+eventId);
							boolean  presaleAutocatsEnabled = ((presaleAutocatsEnabledStr==null || !presaleAutocatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleExchangeEvent.setPresaleAutocatsEnabled(presaleAutocatsEnabled);
							
							dbPresaleExchangeEvent.setLastUpdatedDate(now);
							dbPresaleExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
									.getAuthentication().getName());  // Need to change after login functionality
							exchangeEventUpdateList.add(dbPresaleExchangeEvent);
//							exchangeEvents.add(dbPresaleExchangeEvent);
							
							if(isAutoCats96catChangeFlag) {
								if(dbAutoCats96ExchangeEvent!=null){
									dbAutoCats96ExchangeEvent.setLastUpdatedBy(userName);
									dbAutoCats96ExchangeEvent.setLastUpdatedDate(now);
									autoCats96ExchangeEventUpdateList.add(dbAutoCats96ExchangeEvent);
								}
								if(autoCats96ExchangeEventAudit!=null){
									autoCats96exchangeEventAuditUpdateList.add(autoCats96ExchangeEventAudit);
								}
							}
						
						}
						
					}
				}
				if(temp!=null){
					for (String eventId : temp) {
						try{
							PresaleAutoCatExchangeEvent exchangeEvent = dbPresaleExchangeEventUpdateMap.remove(Integer.parseInt(eventId));
							if(exchangeEvent != null) {
								PresaleAutoCatExchangeEventAudit audit = new PresaleAutoCatExchangeEventAudit(exchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								
								exchangeEvent.setStatus("DELETED");
								exchangeEvent.setLastUpdatedDate(now);
								exchangeEvent.setLastUpdatedBy(userName);
								exchangeEventUpdateList.add(exchangeEvent);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						try{
							if(isAutoCats96catChangeFlag) {
								AutoCats96ExchangeEvent exchangeEvent = dbAutoCats96ExchangeEventUpdateMap.remove(Integer.parseInt(eventId));
								if(exchangeEvent != null && !exchangeEvent.getStatus().equalsIgnoreCase("DELETED")) {
									AutoCats96ExchangeEventAudit audit = new AutoCats96ExchangeEventAudit(exchangeEvent);
									autoCats96exchangeEventAuditUpdateList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(userName);
									autoCats96ExchangeEventUpdateList.add(exchangeEvent);
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
				DAORegistry.getPresaleAutoCatExchangeEventDAO().updateAll(exchangeEventUpdateList);
				
				DAORegistry.getAutoCats96ExchangeEventAuditDAO().saveAll(autoCats96exchangeEventAuditUpdateList);
				DAORegistry.getAutoCats96ExchangeEventDAO().updateAll(autoCats96ExchangeEventUpdateList);
				
				/*if(!productIdList.isEmpty()){
					for(Integer id : productIdList){
						updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
					}
					
				}*/
			}
			if(eventStr!=null){
				for (String eventId : eventStr) {
					try{
						selectedEvent += eventId + ",";
						PresaleAutoCatExchangeEvent tempPresaleExchangeEvent = dbPresaleExchangeEventMap.get(Integer.parseInt(eventId));
						 if(tempPresaleExchangeEvent!=null){
							 exchangeEventList.add(tempPresaleExchangeEvent);
						 }
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			model.addAttribute("eventsCheckAll", isAllEvent);
			model.addAttribute("events", events);
			model.addAttribute("eventStr", selectedEvent);
			model.addAttribute("artist", artistStr);
			model.addAttribute("venue", venueStr);
			model.addAttribute("grandChild", grandChildStr);
			model.addAttribute("selectedValue", selectedValue);
			model.addAttribute("selectedOption", selectedOption);
			model.addAttribute("exchangeEventList",exchangeEventList);
		}
		return "page-presale-autocats-event-management";
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
}
	
	/*@RequestMapping(value="/ManagePresaleAutoCatExchangeEvent")
	public String loadManagePresaleAutoCatExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
	
		try{
			
		ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		model.addAttribute("brokers", brokers);
		Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
		for(Broker broker:brokers){
			brokerMap.put(broker.getId(), broker);
		}
//		ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		String action = request.getParameter("action");
		Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(11);
		model.addAttribute("url", "ManagePresaleAutoCatExchangeEvent");
		model.addAttribute("auditUrl", "GetPresaleAutoCatExchangeEventAuditPopup");
		model.addAttribute("products", autopricingProducts);
		if(action==""){
			model.addAttribute("message", "Please select valid artist ");
		}
		if(action!=null && action!=""){
			Collection<Event> events = null;
			Collection<PresaleAutoCatExchangeEvent> dbPresaleAutoCatExchangeEventList= null;
			String artistStr = request.getParameter("artist");
			String[] eventStr = request.getParameterValues("events");
			String temp[] = eventStr;
			String venueStr = request.getParameter("venue");
			String grandChildStr = request.getParameter("grandChild");
//			String childStr = request.getParameter("child");
			String selectedEvent = "";
			String eventsCheckAll = request.getParameter("eventsCheckAll");
			boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			Integer selectedId = null;
			if(artistStr!=null && !artistStr.isEmpty()){
				Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
				dbPresaleAutoCatExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByArtistId(Integer.valueOf(artistStr));
				selectedOption="Artist";
				selectedValue= artist.getName();
				selectedId = artist.getId();
			}else if(venueStr!=null && !venueStr.isEmpty()){
				Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
				dbPresaleAutoCatExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByVenueId(Integer.valueOf(venueStr));
				selectedOption="Venue";
				selectedValue= venue.getBuilding();
				events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
				selectedId = venue.getId();
			}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
				GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
				dbPresaleAutoCatExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
				selectedOption="GrandChildCategory";                                   
				selectedValue= grandChildCategory.getName();
				events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
				selectedId = grandChildCategory.getId();
			}
			
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = null;
			
			if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
				brokerId = Integer.parseInt(brokerIdStr);
			}
			model.addAttribute("broker", brokerIdStr);
		   
			Map<Integer,PresaleAutoCatExchangeEvent> dbPresaleAutoCatExchangeEventMap = new HashMap<Integer, PresaleAutoCatExchangeEvent>();
			Map<Integer,PresaleAutoCatExchangeEvent> dbPresaleAutoCatExchangeEventUpdateMap = new HashMap<Integer, PresaleAutoCatExchangeEvent>();
			List<PresaleAutoCatExchangeEvent> exchangeEventList = new ArrayList<PresaleAutoCatExchangeEvent>();
			List<PresaleAutoCatExchangeEvent> exchangeEventUpdateList = new ArrayList<PresaleAutoCatExchangeEvent>();
			for(PresaleAutoCatExchangeEvent exchangeEvent:dbPresaleAutoCatExchangeEventList){
				dbPresaleAutoCatExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				dbPresaleAutoCatExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			
			List<PresaleAutoCatExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<PresaleAutoCatExchangeEventAudit>();
			List<Integer> productIdList = new ArrayList<Integer>();
			List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
			if(action.equalsIgnoreCase("update")){
				List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
				Map<String, String[]> requestParams = request.getParameterMap();
				Date now = new Date();
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					if(key.contains("product")){
						productIdList.add(Integer.parseInt(key.replace("product-","")));
					}
					if(key.contains("checkbox_")){
						Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
						temp = StringUtil.removeFromArray(temp,eventId.toString());
						PresaleAutoCatExchangeEvent dbPresaleAutoCatExchangeEvent = dbPresaleAutoCatExchangeEventUpdateMap.remove(eventId);
						toBeUpdaetEventIdList.add(eventId);
						if(dbPresaleAutoCatExchangeEvent!=null){
							PresaleAutoCatExchangeEventAudit audit = new PresaleAutoCatExchangeEventAudit(dbPresaleAutoCatExchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
							boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
							
							dbPresaleAutoCatExchangeEvent.setAllowSectionRange(isAllowSectionRange);
							dbPresaleAutoCatExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
							dbPresaleAutoCatExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
							dbPresaleAutoCatExchangeEvent.setStatus("ACTIVE");
							if(!isAllowSectionRange){
								dbPresaleAutoCatExchangeEvent.setStatus("DELETED");															
							}
							
							String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
							boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
							dbPresaleAutoCatExchangeEvent.setDiscountZone(isDiscountZone);
							
							if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
								dbPresaleAutoCatExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								//dbMiniExchangeEvent.setTicketNetworkBroker(brokerMap.get(dbMiniExchangeEvent.getTicketNetworkBrokerId()));
							}else{
								dbPresaleAutoCatExchangeEvent.setTicketNetworkBrokerId(null);
								//dbMiniExchangeEvent.setTicketNetworkBroker(null);
							}
							if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
								dbPresaleAutoCatExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
							}else{
								dbPresaleAutoCatExchangeEvent.setVividBrokerId(null);
							}
							if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
								dbPresaleAutoCatExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
							}else{
								dbPresaleAutoCatExchangeEvent.setScoreBigBrokerId(null);
							}
							if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
								dbPresaleAutoCatExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
							}else{
								dbPresaleAutoCatExchangeEvent.setZoneTicketBrokerId(null);
							}
							if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
								dbPresaleAutoCatExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
							}else{
								dbPresaleAutoCatExchangeEvent.setFanxchangeBrokerId(null);
							}
							
							String ticketNetworkStr = request.getParameter("ticketnetwork_broker_"+eventId);
							boolean  isTicketNetwork = ((ticketNetworkStr==null || !ticketNetworkStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setIsTicketNetwork(isTicketNetwork);
							
							String vividSeatStr = request.getParameter("vividseat_broker_"+eventId);
							boolean  isVividSeat = ((vividSeatStr==null || !vividSeatStr.equalsIgnoreCase("on") )?false:true);
							
							
							String scoreBigStr = request.getParameter("scorebig_broker_"+eventId);
							boolean  isScoreBigStr = ((scoreBigStr==null || !scoreBigStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setIsScoreBig(isScoreBigStr);
							dbPresaleAutoCatExchangeEvent.setLastUpdatedDate(now);
							dbPresaleAutoCatExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
									.getAuthentication().getName());  // Need to change after login functionality
							exchangeEventUpdateList.add(dbPresaleAutoCatExchangeEvent);
							exchangeEvents.add(dbPresaleAutoCatExchangeEvent);
						}
						
					}
				}
				if(temp!=null){
					for (String eventId : temp) {
						try{
							PresaleAutoCatExchangeEvent exchangeEvent = dbPresaleAutoCatExchangeEventUpdateMap.remove(Integer.parseInt(eventId));
							if(exchangeEvent != null) {
								PresaleAutoCatExchangeEventAudit audit = new PresaleAutoCatExchangeEventAudit(exchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								
								exchangeEvent.setStatus("DELETED");
								exchangeEvent.setLastUpdatedDate(now);
								exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
								exchangeEventUpdateList.add(exchangeEvent);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
				DAORegistry.getPresaleAutoCatExchangeEventDAO().updateAll(exchangeEventUpdateList);
				if(!productIdList.isEmpty()){
					for(Integer id : productIdList){
						updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
					}
					
				}
				
			}
			if(eventStr!=null){
				for (String eventId : eventStr) {
					try{
						selectedEvent += eventId + ",";
						PresaleAutoCatExchangeEvent tempLarryExchangeEvent = dbPresaleAutoCatExchangeEventMap.get(Integer.parseInt(eventId));
						 if(tempLarryExchangeEvent!=null){
							 exchangeEventList.add(tempLarryExchangeEvent);
						 }
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			model.addAttribute("eventsCheckAll", isAllEvent);
			model.addAttribute("events", events);
			model.addAttribute("eventStr", selectedEvent);
			model.addAttribute("artist", artistStr);
			model.addAttribute("venue", venueStr);
			model.addAttribute("grandChild", grandChildStr);
			model.addAttribute("selectedValue", selectedValue);
			model.addAttribute("selectedOption", selectedOption);
			model.addAttribute("exchangeEventList",exchangeEventList);
		}
		return "page-common-product-event-management";
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
	}*/
	@RequestMapping(value="/ManageMiniExchangeEvent")
	public String loadManageMiniExchangeEventPage(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
	//	ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		model.addAttribute("brokers", brokers);
		Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
		for(Broker broker:brokers){
			brokerMap.put(broker.getId(), broker);
		}
//		ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		String action = request.getParameter("action");
		AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(3);
		model.addAttribute("product", product);
		Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(3);
		model.addAttribute("url", "ManageMiniExchangeEvent");
		model.addAttribute("auditUrl", "GetMiniExchangeEventAuditPopup");
		model.addAttribute("products", autopricingProducts);
		if(action==""){
			model.addAttribute("message", "Please select valid artist ");
		}
		if(action!=null && action!=""){
			Collection<Event> events = null;
			Collection<MiniExchangeEvent> dbMiniExchangeEventList= null;
			String artistStr = request.getParameter("artist");
			String[] eventStr = request.getParameterValues("events");
			String temp[] = eventStr;
			String venueStr = request.getParameter("venue");
			String grandChildStr = request.getParameter("grandChild");
//			String childStr = request.getParameter("child");
			String selectedEvent = "";
			String eventsCheckAll = request.getParameter("eventsCheckAll");
			boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			Integer selectedId = null;
			if(artistStr!=null && !artistStr.isEmpty()){
				Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
				dbMiniExchangeEventList  = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsByArtistId(Integer.valueOf(artistStr));
				selectedOption="Artist";
				selectedValue= artist.getName();
				selectedId = artist.getId();
			}else if(venueStr!=null && !venueStr.isEmpty()){
				Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
				dbMiniExchangeEventList  = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsByVenueId(Integer.valueOf(venueStr));
				selectedOption="Venue";
				selectedValue= venue.getBuilding();
				events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
				selectedId = venue.getId();
			}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
				GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
				dbMiniExchangeEventList  = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
				selectedOption="GrandChildCategory";
				selectedValue= grandChildCategory.getName();
				events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
				selectedId = grandChildCategory.getId();
			}
			
			String brokerIdStr = request.getParameter("brokerId");
			/*Integer brokerId = null;
			
			if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
				brokerId = Integer.parseInt(brokerIdStr);
			}*/
			model.addAttribute("broker", brokerIdStr);
		   
			Map<Integer,MiniExchangeEvent> dbMiniExchangeEventMap = new HashMap<Integer, MiniExchangeEvent>();
			Map<Integer,MiniExchangeEvent> dbMiniExchangeEventUpdateMap = new HashMap<Integer, MiniExchangeEvent>();
			List<MiniExchangeEvent> exchangeEventList = new ArrayList<MiniExchangeEvent>();
			List<MiniExchangeEvent> exchangeEventUpdateList = new ArrayList<MiniExchangeEvent>();
			for(MiniExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				dbMiniExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			
			List<MiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<MiniExchangeEventAudit>();
			List<Integer> productIdList = new ArrayList<Integer>();
			List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
			if(action.equalsIgnoreCase("update")){
				List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
				Map<String, String[]> requestParams = request.getParameterMap();
				Date now = new Date();
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					if(key.contains("product")){
						productIdList.add(Integer.parseInt(key.replace("product-","")));
					}
					if(key.contains("checkbox_")){
						Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
						temp = StringUtil.removeFromArray(temp,eventId.toString());
						MiniExchangeEvent dbMiniExchangeEvent = dbMiniExchangeEventUpdateMap.remove(eventId);
						toBeUpdaetEventIdList.add(eventId);
						if(dbMiniExchangeEvent!=null){
							MiniExchangeEventAudit audit = new MiniExchangeEventAudit(dbMiniExchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
							boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
							
							dbMiniExchangeEvent.setAllowSectionRange(isAllowSectionRange);
							dbMiniExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
							/*dbMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));*/
							dbMiniExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
							dbMiniExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
							dbMiniExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
							dbMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbMiniExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
							dbMiniExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
							dbMiniExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
							dbMiniExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
							dbMiniExchangeEvent.setStatus("ACTIVE");
							
							if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
								dbMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
							}else{
								dbMiniExchangeEvent.setShippingMethod(null);
							}
							
							String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
							boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setDiscountZone(isDiscountZone);
							
							if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								//dbMiniExchangeEvent.setTicketNetworkBroker(brokerMap.get(dbMiniExchangeEvent.getTicketNetworkBrokerId()));
							}else{
								dbMiniExchangeEvent.setTicketNetworkBrokerId(null);
								//dbMiniExchangeEvent.setTicketNetworkBroker(null);
							}
							if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setVividBrokerId(null);
							}
							if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setScoreBigBrokerId(null);
							}
							if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setFanxchangeBrokerId(null);
							}
							if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setTicketcityBrokerId(null);
							}
							if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setTicketcityBrokerId(null);
							}
							if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setZoneTicketBrokerId(null);
							}
							
							/*String ticketNetworkStr = request.getParameter("ticketnetwork_broker_"+eventId);
							boolean  isTicketNetwork = ((ticketNetworkStr==null || !ticketNetworkStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setIsTicketNetwork(isTicketNetwork);
							
							String vividSeatStr = request.getParameter("vividseat_broker_"+eventId);
							boolean  isVividSeat = ((vividSeatStr==null || !vividSeatStr.equalsIgnoreCase("on") )?false:true);
							
							
							String scoreBigStr = request.getParameter("scorebig_broker_"+eventId);
							boolean  isScoreBigStr = ((scoreBigStr==null || !scoreBigStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setIsScoreBig(isScoreBigStr);*/
							dbMiniExchangeEvent.setLastUpdatedDate(now);
							dbMiniExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
									.getAuthentication().getName());  // Need to change after login functionality
							exchangeEventUpdateList.add(dbMiniExchangeEvent);
							exchangeEvents.add(dbMiniExchangeEvent);
						}
						
					}
				}
				if(temp!=null){
					for (String eventId : temp) {
						try{
							MiniExchangeEvent exchangeEvent = dbMiniExchangeEventUpdateMap.remove(Integer.parseInt(eventId));
							if(exchangeEvent != null) {
								MiniExchangeEventAudit audit = new MiniExchangeEventAudit(exchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								
								exchangeEvent.setStatus("DELETED");
								exchangeEvent.setLastUpdatedDate(now);
								exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
								exchangeEventUpdateList.add(exchangeEvent);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				DAORegistry.getMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
				DAORegistry.getMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
				if(!productIdList.isEmpty()){
					for(Integer id : productIdList){
						updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
					}
					
				}
				
			}
			if(eventStr!=null){
				for (String eventId : eventStr) {
					try{
						selectedEvent += eventId + ",";
						MiniExchangeEvent tempLarryExchangeEvent = dbMiniExchangeEventMap.get(Integer.parseInt(eventId));
						 if(tempLarryExchangeEvent!=null){
							 exchangeEventList.add(tempLarryExchangeEvent);
						 }
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			model.addAttribute("eventsCheckAll", isAllEvent);
			model.addAttribute("events", events);
			model.addAttribute("eventStr", selectedEvent);
			model.addAttribute("artist", artistStr);
			model.addAttribute("venue", venueStr);
			model.addAttribute("grandChild", grandChildStr);
			model.addAttribute("selectedValue", selectedValue);
			model.addAttribute("selectedOption", selectedOption);
			model.addAttribute("exchangeEventList",exchangeEventList);
		}
		return "page-common-product-event-management";
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
}
	
	@RequestMapping(value="/ManageSGLastFiveRowExchangeEvent")
	public String loadManageSGLastFiveRowExchangeEventPage(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
	//	ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		model.addAttribute("brokers", brokers);
		Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
		for(Broker broker:brokers){
			brokerMap.put(broker.getId(), broker);
		}
//		ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		String action = request.getParameter("action");
		AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(19);
		model.addAttribute("product", product);
		Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(19);
		model.addAttribute("url", "ManageSGLastFiveRowExchangeEvent");
		model.addAttribute("auditUrl", "GetSGLastFiveRowExchangeEventAuditPopup");
		model.addAttribute("products", autopricingProducts);
		if(action==""){
			model.addAttribute("message", "Please select valid artist ");
		}
		if(action!=null && action!=""){
			Collection<Event> events = null;
			Collection<SGLastFiveRowExchangeEvent> dbMiniExchangeEventList= null;
			String artistStr = request.getParameter("artist");
			String[] eventStr = request.getParameterValues("events");
			String temp[] = eventStr;
			String venueStr = request.getParameter("venue");
			String grandChildStr = request.getParameter("grandChild");
//			String childStr = request.getParameter("child");
			String selectedEvent = "";
			String eventsCheckAll = request.getParameter("eventsCheckAll");
			boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			Integer selectedId = null;
			if(artistStr!=null && !artistStr.isEmpty()){
				Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
				dbMiniExchangeEventList  = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAllSGLastFiveRowExchangeEventsByArtistId(Integer.valueOf(artistStr));
				selectedOption="Artist";
				selectedValue= artist.getName();
				selectedId = artist.getId();
			}else if(venueStr!=null && !venueStr.isEmpty()){
				Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
				dbMiniExchangeEventList  = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAllSGLastFiveRowExchangeEventsByVenueId(Integer.valueOf(venueStr));
				selectedOption="Venue";
				selectedValue= venue.getBuilding();
				events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
				selectedId = venue.getId();
			}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
				GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
				dbMiniExchangeEventList  = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAllSGLastFiveRowExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
				selectedOption="GrandChildCategory";
				selectedValue= grandChildCategory.getName();
				events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
				selectedId = grandChildCategory.getId();
			}
			
			String brokerIdStr = request.getParameter("brokerId");
			/*Integer brokerId = null;
			
			if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
				brokerId = Integer.parseInt(brokerIdStr);
			}*/
			model.addAttribute("broker", brokerIdStr);
		   
			Map<Integer,SGLastFiveRowExchangeEvent> dbMiniExchangeEventMap = new HashMap<Integer, SGLastFiveRowExchangeEvent>();
			Map<Integer,SGLastFiveRowExchangeEvent> dbMiniExchangeEventUpdateMap = new HashMap<Integer, SGLastFiveRowExchangeEvent>();
			List<SGLastFiveRowExchangeEvent> exchangeEventList = new ArrayList<SGLastFiveRowExchangeEvent>();
			List<SGLastFiveRowExchangeEvent> exchangeEventUpdateList = new ArrayList<SGLastFiveRowExchangeEvent>();
			for(SGLastFiveRowExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				dbMiniExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			
			List<SGLastFiveRowExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<SGLastFiveRowExchangeEventAudit>();
			List<Integer> productIdList = new ArrayList<Integer>();
			List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
			if(action.equalsIgnoreCase("update")){
				List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
				Map<String, String[]> requestParams = request.getParameterMap();
				Date now = new Date();
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					if(key.contains("product")){
						productIdList.add(Integer.parseInt(key.replace("product-","")));
					}
					if(key.contains("checkbox_")){
						Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
						temp = StringUtil.removeFromArray(temp,eventId.toString());
						SGLastFiveRowExchangeEvent dbMiniExchangeEvent = dbMiniExchangeEventUpdateMap.remove(eventId);
						toBeUpdaetEventIdList.add(eventId);
						if(dbMiniExchangeEvent!=null){
							SGLastFiveRowExchangeEventAudit audit = new SGLastFiveRowExchangeEventAudit(dbMiniExchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
							boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
							
							dbMiniExchangeEvent.setAllowSectionRange(isAllowSectionRange);
							dbMiniExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
							/*dbMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));*/
							dbMiniExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
							dbMiniExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
							dbMiniExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
							dbMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbMiniExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
							dbMiniExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
							dbMiniExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
							dbMiniExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
							dbMiniExchangeEvent.setStatus("ACTIVE");
							
							if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
								dbMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
							}else{
								dbMiniExchangeEvent.setShippingMethod(null);
							}
							
							String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
							boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setDiscountZone(isDiscountZone);
							
							if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								//dbMiniExchangeEvent.setTicketNetworkBroker(brokerMap.get(dbMiniExchangeEvent.getTicketNetworkBrokerId()));
							}else{
								dbMiniExchangeEvent.setTicketNetworkBrokerId(null);
								//dbMiniExchangeEvent.setTicketNetworkBroker(null);
							}
							if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setVividBrokerId(null);
							}
							if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setScoreBigBrokerId(null);
							}
							if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setFanxchangeBrokerId(null);
							}
							if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setTicketcityBrokerId(null);
							}
							if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setTicketcityBrokerId(null);
							}
							if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setZoneTicketBrokerId(null);
							}
							
							/*String ticketNetworkStr = request.getParameter("ticketnetwork_broker_"+eventId);
							boolean  isTicketNetwork = ((ticketNetworkStr==null || !ticketNetworkStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setIsTicketNetwork(isTicketNetwork);
							
							String vividSeatStr = request.getParameter("vividseat_broker_"+eventId);
							boolean  isVividSeat = ((vividSeatStr==null || !vividSeatStr.equalsIgnoreCase("on") )?false:true);
							
							
							String scoreBigStr = request.getParameter("scorebig_broker_"+eventId);
							boolean  isScoreBigStr = ((scoreBigStr==null || !scoreBigStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setIsScoreBig(isScoreBigStr);*/
							dbMiniExchangeEvent.setLastUpdatedDate(now);
							dbMiniExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
									.getAuthentication().getName());  // Need to change after login functionality
							exchangeEventUpdateList.add(dbMiniExchangeEvent);
							exchangeEvents.add(dbMiniExchangeEvent);
						}
						
					}
				}
				if(temp!=null){
					for (String eventId : temp) {
						try{
							SGLastFiveRowExchangeEvent exchangeEvent = dbMiniExchangeEventUpdateMap.remove(Integer.parseInt(eventId));
							if(exchangeEvent != null) {
								SGLastFiveRowExchangeEventAudit audit = new SGLastFiveRowExchangeEventAudit(exchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								
								exchangeEvent.setStatus("DELETED");
								exchangeEvent.setLastUpdatedDate(now);
								exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
								exchangeEventUpdateList.add(exchangeEvent);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				DAORegistry.getSgLastFiveRowExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
				DAORegistry.getSgLastFiveRowExchangeEventDAO().updateAll(exchangeEventUpdateList);
				if(!productIdList.isEmpty()){
					for(Integer id : productIdList){
						updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
					}
					
				}
				
			}
			if(eventStr!=null){
				for (String eventId : eventStr) {
					try{
						selectedEvent += eventId + ",";
						SGLastFiveRowExchangeEvent tempLarryExchangeEvent = dbMiniExchangeEventMap.get(Integer.parseInt(eventId));
						 if(tempLarryExchangeEvent!=null){
							 exchangeEventList.add(tempLarryExchangeEvent);
						 }
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			model.addAttribute("eventsCheckAll", isAllEvent);
			model.addAttribute("events", events);
			model.addAttribute("eventStr", selectedEvent);
			model.addAttribute("artist", artistStr);
			model.addAttribute("venue", venueStr);
			model.addAttribute("grandChild", grandChildStr);
			model.addAttribute("selectedValue", selectedValue);
			model.addAttribute("selectedOption", selectedOption);
			model.addAttribute("exchangeEventList",exchangeEventList);
		}
		return "page-common-product-event-management";
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
}
	
	@RequestMapping(value="/LarryLastExchangeEventAuditPopup")
	public String getLarryLastExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<LarryLastExchangeEventAudit> larryLastExchangeEventAudits = DAORegistry.getLarryLastExchangeEventAuditDAO().getAllLarryLastExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", larryLastExchangeEventAudits);
		}
		return "page-larrylast-event-audit";
	}
	
	@RequestMapping(value="/ZoneLastRowExchangeEventAuditPopup")
	public String getZoneLastRowExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<ZoneLastRowMiniExchangeEventAudit> larryLastExchangeEventAudits = DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().getAllZoneLastRowMiniExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", larryLastExchangeEventAudits);
		}
		return "page-larrylast-event-audit";
	}
	
	@RequestMapping(value="/TNSpecialExchangeEventAuditPopup")
	public String getTNSpecialExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<TicketNetworkSSAccountExchangeEventAudit> larryLastExchangeEventAudits = DAORegistry.getTicketNetworkSSAccountExchangeEventAuditDAO().getAllTNSpecialExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", larryLastExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	
	@RequestMapping(value="/GetPresaleAutoCatExchangeEventAuditPopup")
	public String getPresaleAutoCatExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<PresaleAutoCatExchangeEventAudit> presaleAutoCatExchangeEventAudits = DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().getAllPresaleAutoCatExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", presaleAutoCatExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	@RequestMapping(value="/ZonePricingExchangeEventAuditPopup")
	public String getZonePricingExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<ZonePricingExchangeEventAudit> ZonePricingExchangeEventAudits = DAORegistry.getZonePricingExchangeEventAuditDAO().getAllZonePricingExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", ZonePricingExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	@RequestMapping(value="/TixCityZonePricingExchangeEventAuditPopup")
	public String getTixCityZonePricingExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<TixCityZonePricingExchangeEventAudit> TixCityZonePricingExchangeEventAudits = DAORegistry.getTixCityZonePricingExchangeEventAuditDAO().getAllTixCityZonePricingExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", TixCityZonePricingExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	@RequestMapping(value="/ManhattanZonePricingExchangeEventAuditPopup")
	public String getManhattanZonePricingExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<ManhattanZonePricingExchangeEventAudit> ManhattanZonePricingExchangeEventAudits = DAORegistry.getManhattanZonePricingExchangeEventAuditDAO().getAllManhattanZonePricingExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", ManhattanZonePricingExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	@RequestMapping(value="/ZoneLastRowMiniExchangeEventAuditPopup")
	public String getZoneLastRowMiniExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<ZoneLastRowMiniExchangeEventAudit> zoneLastRowMiniExchangeEventAudits = DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().getAllZoneLastRowMiniExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", zoneLastRowMiniExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	@RequestMapping(value="/LastRowMiniExchangeEventAuditPopUp")
	public String getLastRowMiniExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(5);
		model.addAttribute("product", product);
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<LastRowMiniExchangeEventAudit> lastRowMiniExchangeEventAudits = DAORegistry.getLastRowMiniExchangeEventAuditDAO().getAllLastRowMiniExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", lastRowMiniExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	@RequestMapping(value="/VipMiniExchangeEventAuditPopup")
	public String getVipMiniExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(4);
		model.addAttribute("product", product);
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<VipMiniExchangeEventAudit> vipMiniExchangeEventAudits = DAORegistry.getVipMiniExchangeEventAuditDAO().getAllVipMiniExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", vipMiniExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	@RequestMapping(value="/ZoneTicketsProcessorExchangeEventAuditPopup")
	public String getZoneTicketsProcessorExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(9);
		model.addAttribute("product", product);
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<ZoneTicketsProcessorExchangeEventAudit> zoneTicketsProcessorExchangeEventAudits = DAORegistry.getZoneTicketsProcessorExchangeEventAuditDAO().getAllZoneTicketsProcessorExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", zoneTicketsProcessorExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	
	@RequestMapping(value="/PresaleZoneTicketsExchangeEventAuditPopup")
	public String getPresaleZoneTicketsExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<PresaleZoneTicketsExchangeEventAudit> zoneTicketsProcessorExchangeEventAudits = DAORegistry.getPresaleZoneTicketsExchangeEventAuditDAO().getAllZoneTicketsProcessorExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", zoneTicketsProcessorExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	
	@RequestMapping(value="/GetSGLastFiveRowExchangeEventAuditPopup")
	public String getSGLastFiveRowExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(19);
		model.addAttribute("product", product);
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<SGLastFiveRowExchangeEventAudit> larryLastExchangeEventAudits = DAORegistry.getSgLastFiveRowExchangeEventAuditDAO().getAllSGLastFiveRowExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", larryLastExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	
	@RequestMapping(value="/GetMiniExchangeEventAuditPopup")
	public String getMiniExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(3);
		model.addAttribute("product", product);
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<MiniExchangeEventAudit> larryLastExchangeEventAudits = DAORegistry.getMiniExchangeEventAuditDAO().getMiniExchangeEventAuditByEventId(eventId);
			model.addAttribute("exchangeEventList", larryLastExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	/*@RequestMapping(value="/ManageZoneLastRowMiniExchangeEvent")
	public String manageZoneLastRowMiniExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			model.addAttribute("autopricingbrokers", DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers());
			Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			model.addAttribute("brokers", brokers);
			Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
			for(Broker broker:brokers){
				brokerMap.put(broker.getId(), broker);
			}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(7);
			model.addAttribute("url", "ManageZoneLastRowMiniExchangeEvent");
			model.addAttribute("auditUrl", "ZoneLastRowMiniExchangeEventAuditPopup");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<ZoneLastRowMiniExchangeEvent> dbZoneLastRowMiniExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String temp[]= eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbZoneLastRowMiniExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbZoneLastRowMiniExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbZoneLastRowMiniExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,ZoneLastRowMiniExchangeEvent> dbZoneLastRowMiniExchangeEventMap = new HashMap<Integer, ZoneLastRowMiniExchangeEvent>();
				Map<Integer,ZoneLastRowMiniExchangeEvent> dbZoneLastRowMiniExchangeEventUpdateMap = new HashMap<Integer, ZoneLastRowMiniExchangeEvent>();
				
				List<ZoneLastRowMiniExchangeEvent> exchangeEventList = new ArrayList<ZoneLastRowMiniExchangeEvent>();
				List<ZoneLastRowMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<ZoneLastRowMiniExchangeEvent>();
				for(ZoneLastRowMiniExchangeEvent exchangeEvent:dbZoneLastRowMiniExchangeEventList){
					dbZoneLastRowMiniExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbZoneLastRowMiniExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<ZoneLastRowMiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ZoneLastRowMiniExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							ZoneLastRowMiniExchangeEvent dbZoneLastRowMiniExchangeEvent = dbZoneLastRowMiniExchangeEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbZoneLastRowMiniExchangeEvent!=null){
								ZoneLastRowMiniExchangeEventAudit audit = new ZoneLastRowMiniExchangeEventAudit(dbZoneLastRowMiniExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbZoneLastRowMiniExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbZoneLastRowMiniExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
								//dbZoneLastRowMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbZoneLastRowMiniExchangeEvent.setStatus("ACTIVE");
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbZoneLastRowMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbZoneLastRowMiniExchangeEvent.setShippingMethod(null);
								}
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbZoneLastRowMiniExchangeEvent.setDiscountZone(isDiscountZone);
								
								if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbZoneLastRowMiniExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbZoneLastRowMiniExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbZoneLastRowMiniExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbZoneLastRowMiniExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbZoneLastRowMiniExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbZoneLastRowMiniExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbZoneLastRowMiniExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbZoneLastRowMiniExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbZoneLastRowMiniExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbZoneLastRowMiniExchangeEvent.setTicketcityBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbZoneLastRowMiniExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbZoneLastRowMiniExchangeEvent.setZoneTicketBrokerId(null);
								}
								dbZoneLastRowMiniExchangeEvent.setLastUpdatedDate(now);
								dbZoneLastRowMiniExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								exchangeEventUpdateList.add(dbZoneLastRowMiniExchangeEvent);
								exchangeEvents.add(dbZoneLastRowMiniExchangeEvent);
							}
							
						}
					}
					if(temp!=null){
						for (String eventId : temp) {
							try{
								ZoneLastRowMiniExchangeEvent exchangeEvent = dbZoneLastRowMiniExchangeEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									ZoneLastRowMiniExchangeEventAudit audit = new ZoneLastRowMiniExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEventUpdateList.add(exchangeEvent);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getZoneLastRowMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
					if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						try{
							selectedEvent += eventId + ",";
							ZoneLastRowMiniExchangeEvent tempLarryExchangeEvent = dbZoneLastRowMiniExchangeEventMap.get(Integer.parseInt(eventId));
							 if(tempLarryExchangeEvent!=null){
								 exchangeEventList.add(tempLarryExchangeEvent);
							 }
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-common-product-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}*/
	@RequestMapping(value="/ManageLastRowMiniExchangeEvent")
	public String manageLastRowMiniExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			model.addAttribute("brokers", brokers);
			Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
			for(Broker broker:brokers){
				brokerMap.put(broker.getId(), broker);
			}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(5);
			model.addAttribute("product", product);
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(5);
			model.addAttribute("url", "ManageLastRowMiniExchangeEvent");
			model.addAttribute("auditUrl", "LastRowMiniExchangeEventAuditPopUp");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<LastRowMiniExchangeEvent> dbLastRowMiniExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String temp[] = eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbLastRowMiniExchangeEventList  = DAORegistry.getLastRowMiniExchangeEventDAO().getAllLastRowMiniExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbLastRowMiniExchangeEventList  = DAORegistry.getLastRowMiniExchangeEventDAO().getAllLastRowMiniExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbLastRowMiniExchangeEventList  = DAORegistry.getLastRowMiniExchangeEventDAO().getAllLastRowMiniExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				/*Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}*/
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,LastRowMiniExchangeEvent> dbLastRowMiniExchangeEventMap = new HashMap<Integer, LastRowMiniExchangeEvent>();
				Map<Integer,LastRowMiniExchangeEvent> dbLastRowMiniExchangeEventUpdateMap = new HashMap<Integer, LastRowMiniExchangeEvent>();
				List<LastRowMiniExchangeEvent> exchangeEventList = new ArrayList<LastRowMiniExchangeEvent>();
				List<LastRowMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<LastRowMiniExchangeEvent>();
				for(LastRowMiniExchangeEvent exchangeEvent:dbLastRowMiniExchangeEventList){
					dbLastRowMiniExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbLastRowMiniExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<LastRowMiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<LastRowMiniExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							LastRowMiniExchangeEvent dbLastRowMiniExchangeEvent = dbLastRowMiniExchangeEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbLastRowMiniExchangeEvent!=null){
								LastRowMiniExchangeEventAudit audit = new LastRowMiniExchangeEventAudit(dbLastRowMiniExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbLastRowMiniExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbLastRowMiniExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
//								dbLastRowMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								dbLastRowMiniExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbLastRowMiniExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbLastRowMiniExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbLastRowMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//								dbLastRowMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
								dbLastRowMiniExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbLastRowMiniExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbLastRowMiniExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbLastRowMiniExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbLastRowMiniExchangeEvent.setStatus("ACTIVE");
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbLastRowMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbLastRowMiniExchangeEvent.setShippingMethod(null);
								}
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbLastRowMiniExchangeEvent.setDiscountZone(isDiscountZone);
								
								if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbLastRowMiniExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbLastRowMiniExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbLastRowMiniExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbLastRowMiniExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbLastRowMiniExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbLastRowMiniExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbLastRowMiniExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbLastRowMiniExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbLastRowMiniExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbLastRowMiniExchangeEvent.setTicketcityBrokerId(null);
								}
								if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
									dbLastRowMiniExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
								}else{
									dbLastRowMiniExchangeEvent.setTicketcityBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbLastRowMiniExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbLastRowMiniExchangeEvent.setZoneTicketBrokerId(null);
								}
								/*String ticketNetworkStr = request.getParameter("ticketnetwork_broker_"+eventId);
								boolean  isTicketNetwork = ((ticketNetworkStr==null || !ticketNetworkStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setIsTicketNetwork(isTicketNetwork);
								
								String vividSeatStr = request.getParameter("vividseat_broker_"+eventId);
								boolean  isVividSeat = ((vividSeatStr==null || !vividSeatStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setIsVivid(isVividSeat);
								
								String scoreBigStr = request.getParameter("scorebig_broker_"+eventId);
								boolean  isScoreBigStr = ((scoreBigStr==null || !scoreBigStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setIsScoreBig(isScoreBigStr);*/
								dbLastRowMiniExchangeEvent.setLastUpdatedDate(now);
								dbLastRowMiniExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								exchangeEventUpdateList.add(dbLastRowMiniExchangeEvent);
								exchangeEvents.add(dbLastRowMiniExchangeEvent);
							}
	
						}
					}
					if(temp!=null){
						for (String eventId : temp) {
							try{
								LastRowMiniExchangeEvent exchangeEvent = dbLastRowMiniExchangeEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									LastRowMiniExchangeEventAudit audit = new LastRowMiniExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEventUpdateList.add(exchangeEvent);
									
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					DAORegistry.getLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getLastRowMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
					if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						try{
							selectedEvent += eventId + ",";
							LastRowMiniExchangeEvent tempLastRowMiniExchangeEvent = dbLastRowMiniExchangeEventMap.get(Integer.parseInt(eventId));
							 if(tempLastRowMiniExchangeEvent!=null){
								 exchangeEventList.add(tempLastRowMiniExchangeEvent);
							 }
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-common-product-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	
	
	
	public boolean updateExchangeEventsByProdctIdAndEventIdList(Integer productId,List<Integer> eventIds,String filterType,Integer filterId,List<ExchangeEvent> exchangeEvents,
			String[] unCheckedEventIdsStr) throws Exception {
		if(productId == 3){// Mini Exchanges
			Collection<MiniExchangeEvent> dbMiniExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbMiniExchangeEventList  = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbMiniExchangeEventList  = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbMiniExchangeEventList  = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,MiniExchangeEvent> dbMiniEventMap = new HashMap<Integer, MiniExchangeEvent>();
			
			List<MiniExchangeEvent> miniExchangeEventUpdateList = new ArrayList<MiniExchangeEvent>();
			if(dbMiniExchangeEventList==null){
				return false;
			}
			for(MiniExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<MiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<MiniExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				
				MiniExchangeEvent miniExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(miniExchangeEvent!=null){
				MiniExchangeEventAudit audit = new MiniExchangeEventAudit(miniExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				miniExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				miniExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				miniExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				miniExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				miniExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				miniExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				miniExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				miniExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				miniExchangeEvent.setExposure(exchangeEvent.getExposure());
				miniExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				miniExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				miniExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				miniExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				miniExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				miniExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				miniExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				miniExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				miniExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				miniExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				miniExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				miniExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				miniExchangeEventUpdateList.add(miniExchangeEvent);
				}
			}
			
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						MiniExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							MiniExchangeEventAudit audit = new MiniExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							miniExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getMiniExchangeEventDAO().updateAll(miniExchangeEventUpdateList);
		}else if(productId == 4){// VIP MiniCats Exchanges
			Collection<VipMiniExchangeEvent> dbMiniExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbMiniExchangeEventList  = DAORegistry.getVipMiniExchangeEventDAO().getAllVipMiniExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbMiniExchangeEventList  = DAORegistry.getVipMiniExchangeEventDAO().getAllVipMiniExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbMiniExchangeEventList  = DAORegistry.getVipMiniExchangeEventDAO().getAllVipMiniExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,VipMiniExchangeEvent> dbMiniEventMap = new HashMap<Integer, VipMiniExchangeEvent>();
			
			List<VipMiniExchangeEvent> vipMiniExchangeEventUpdateList = new ArrayList<VipMiniExchangeEvent>();
			if(dbMiniExchangeEventList==null){
				return false;
			}
			for(VipMiniExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<VipMiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<VipMiniExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				VipMiniExchangeEvent toBeUpdateExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				VipMiniExchangeEventAudit audit = new VipMiniExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				vipMiniExchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						VipMiniExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							VipMiniExchangeEventAudit audit = new VipMiniExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							vipMiniExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getVipMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getVipMiniExchangeEventDAO().updateAll(vipMiniExchangeEventUpdateList);
			
		}else if(productId == 5){// LastRow MiniCats Exchanges
			Collection<LastRowMiniExchangeEvent> dbLastRowMiniExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbLastRowMiniExchangeEventList  = DAORegistry.getLastRowMiniExchangeEventDAO().getAllLastRowMiniExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbLastRowMiniExchangeEventList  = DAORegistry.getLastRowMiniExchangeEventDAO().getAllLastRowMiniExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbLastRowMiniExchangeEventList  = DAORegistry.getLastRowMiniExchangeEventDAO().getAllLastRowMiniExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,LastRowMiniExchangeEvent> dbMiniEventMap = new HashMap<Integer, LastRowMiniExchangeEvent>();
			
			List<LastRowMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<LastRowMiniExchangeEvent>();
			if(dbLastRowMiniExchangeEventList==null){
				return false;
			}
			for(LastRowMiniExchangeEvent exchangeEvent:dbLastRowMiniExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<LastRowMiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<LastRowMiniExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				LastRowMiniExchangeEvent toBeUpdateExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				LastRowMiniExchangeEventAudit audit = new LastRowMiniExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				exchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						LastRowMiniExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							LastRowMiniExchangeEventAudit audit = new LastRowMiniExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							exchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			DAORegistry.getLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getLastRowMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
			
		}/*else if(productId == 6){// LarryLast Exchanges
			Collection<LarryLastExchangeEvent> dbLarryLastExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbLarryLastExchangeEventList  = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbLarryLastExchangeEventList  = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbLarryLastExchangeEventList  = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,LarryLastExchangeEvent> dbLarryLastEventMap = new HashMap<Integer, LarryLastExchangeEvent>();
			
			List<LarryLastExchangeEvent> exchangeEventUpdateList = new ArrayList<LarryLastExchangeEvent>();
			if(dbLarryLastExchangeEventList==null){
				return false;
			}
			for(LarryLastExchangeEvent exchangeEvent:dbLarryLastExchangeEventList){
				dbLarryLastEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<LarryLastExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<LarryLastExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				LarryLastExchangeEvent toBeUpdateExchangeEvent = dbLarryLastEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				LarryLastExchangeEventAudit audit = new LarryLastExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				exchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						LarryLastExchangeEvent exchangeEvent = dbLarryLastEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							LarryLastExchangeEventAudit audit = new LarryLastExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							exchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getLarryLastExchangeEventDAO().updateAll(exchangeEventUpdateList);
			
		}*//*else if(productId == 7){// Zone LastRow MiniCats Exchanges
			Collection<ZoneLastRowMiniExchangeEvent> dbMiniExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbMiniExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbMiniExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbMiniExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,ZoneLastRowMiniExchangeEvent> dbMiniEventMap = new HashMap<Integer, ZoneLastRowMiniExchangeEvent>();
			
			List<ZoneLastRowMiniExchangeEvent> miniExchangeEventUpdateList = new ArrayList<ZoneLastRowMiniExchangeEvent>();
			if(dbMiniExchangeEventList==null){
				return false;
			}
			for(ZoneLastRowMiniExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<ZoneLastRowMiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ZoneLastRowMiniExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				ZoneLastRowMiniExchangeEvent toBeUpdateExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				ZoneLastRowMiniExchangeEventAudit audit = new ZoneLastRowMiniExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				miniExchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						ZoneLastRowMiniExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							ZoneLastRowMiniExchangeEventAudit audit = new ZoneLastRowMiniExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							miniExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getZoneLastRowMiniExchangeEventDAO().updateAll(miniExchangeEventUpdateList);
			
		}*/else if(productId == 8){//  ZonesPricing Exchanges
			Collection<ZonePricingExchangeEvent> dbMiniExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbMiniExchangeEventList  = DAORegistry.getZonePricingExchangeEventDAO().getAllZonePricingExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbMiniExchangeEventList  = DAORegistry.getZonePricingExchangeEventDAO().getAllZonePricingExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbMiniExchangeEventList  = DAORegistry.getZonePricingExchangeEventDAO().getAllZonePricingExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,ZonePricingExchangeEvent> dbMiniEventMap = new HashMap<Integer, ZonePricingExchangeEvent>();
			
			List<ZonePricingExchangeEvent> miniExchangeEventUpdateList = new ArrayList<ZonePricingExchangeEvent>();
			if(dbMiniExchangeEventList==null){
				return false;
			}
			for(ZonePricingExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<ZonePricingExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ZonePricingExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				ZonePricingExchangeEvent toBeUpdateExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				ZonePricingExchangeEventAudit audit = new ZonePricingExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				miniExchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						ZonePricingExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							ZonePricingExchangeEventAudit audit = new ZonePricingExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							miniExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getZonePricingExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getZonePricingExchangeEventDAO().updateAll(miniExchangeEventUpdateList);
			
		} else if(productId == 11){//  Presale Autocats Exchanges
			Collection<PresaleAutoCatExchangeEvent> dbPresaleAutoCatExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbPresaleAutoCatExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbPresaleAutoCatExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbPresaleAutoCatExchangeEventList  = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,PresaleAutoCatExchangeEvent> dbPresaleAutoCatExchangeEventMap = new HashMap<Integer, PresaleAutoCatExchangeEvent>();
			
			List<PresaleAutoCatExchangeEvent> presaleAutoCatExchangeEventUpdateList = new ArrayList<PresaleAutoCatExchangeEvent>();
			if(dbPresaleAutoCatExchangeEventList==null){
				return false;
			}
			for(PresaleAutoCatExchangeEvent exchangeEvent:dbPresaleAutoCatExchangeEventList){
				dbPresaleAutoCatExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<PresaleAutoCatExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<PresaleAutoCatExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				PresaleAutoCatExchangeEvent toBeUpdateExchangeEvent = dbPresaleAutoCatExchangeEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
					PresaleAutoCatExchangeEventAudit audit = new PresaleAutoCatExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				if(!exchangeEvent.getAllowSectionRange()){
					toBeUpdateExchangeEvent.setStatus("DELETED");															
				}
				
				presaleAutoCatExchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						PresaleAutoCatExchangeEvent exchangeEvent = dbPresaleAutoCatExchangeEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							PresaleAutoCatExchangeEventAudit audit = new PresaleAutoCatExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							presaleAutoCatExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getPresaleAutoCatExchangeEventDAO().updateAll(presaleAutoCatExchangeEventUpdateList);
			
		} else if(productId == 9){//  Zonetickets Exchanges
			Collection<ZoneTicketsProcessorExchangeEvent> dbMiniExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbMiniExchangeEventList  = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbMiniExchangeEventList  = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbMiniExchangeEventList  = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,ZoneTicketsProcessorExchangeEvent> dbMiniEventMap = new HashMap<Integer, ZoneTicketsProcessorExchangeEvent>();
			
			List<ZoneTicketsProcessorExchangeEvent> miniExchangeEventUpdateList = new ArrayList<ZoneTicketsProcessorExchangeEvent>();
			if(dbMiniExchangeEventList==null){
				return false;
			}
			for(ZoneTicketsProcessorExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<ZoneTicketsProcessorExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ZoneTicketsProcessorExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				ZoneTicketsProcessorExchangeEvent toBeUpdateExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				ZoneTicketsProcessorExchangeEventAudit audit = new ZoneTicketsProcessorExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				toBeUpdateExchangeEvent.setTaxPercentage(exchangeEvent.getTaxPercentage());
				//toBeUpdateExchangeEvent.setExcludeHoursBeforeEvent(exchangeEvent.getExcludeHoursBeforeEvent());
				
				miniExchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						ZoneTicketsProcessorExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							ZoneTicketsProcessorExchangeEventAudit audit = new ZoneTicketsProcessorExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							miniExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getZoneTicketsProcessorExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getZoneTicketsProcessorExchangeEventDAO().updateAll(miniExchangeEventUpdateList);
		} 
		else if(productId == 13){// TNSpecial Exchanges
			Collection<TicketNetworkSSAccountExchangeEvent> dbTNSpecialExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbTNSpecialExchangeEventList  = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAllTNSpecialExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbTNSpecialExchangeEventList  = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAllTNSpecialExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbTNSpecialExchangeEventList  = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAllTNSpecialExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,TicketNetworkSSAccountExchangeEvent> dbTNSpecialEventMap = new HashMap<Integer, TicketNetworkSSAccountExchangeEvent>();
			
			List<TicketNetworkSSAccountExchangeEvent> exchangeEventUpdateList = new ArrayList<TicketNetworkSSAccountExchangeEvent>();
			if(dbTNSpecialExchangeEventList==null){
				return false;
			}
			for(TicketNetworkSSAccountExchangeEvent exchangeEvent:dbTNSpecialExchangeEventList){
				dbTNSpecialEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<TicketNetworkSSAccountExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<TicketNetworkSSAccountExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				TicketNetworkSSAccountExchangeEvent toBeUpdateExchangeEvent = dbTNSpecialEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				TicketNetworkSSAccountExchangeEventAudit audit = new TicketNetworkSSAccountExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				exchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						TicketNetworkSSAccountExchangeEvent exchangeEvent = dbTNSpecialEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							TicketNetworkSSAccountExchangeEventAudit audit = new TicketNetworkSSAccountExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							exchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			DAORegistry.getTicketNetworkSSAccountExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().updateAll(exchangeEventUpdateList);
			
		} else if(productId == 14){//  TixCity ZonesPricing Exchanges
			Collection<TixCityZonePricingExchangeEvent> dbMiniExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbMiniExchangeEventList  = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbMiniExchangeEventList  = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbMiniExchangeEventList  = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,TixCityZonePricingExchangeEvent> dbMiniEventMap = new HashMap<Integer, TixCityZonePricingExchangeEvent>();
			
			List<TixCityZonePricingExchangeEvent> miniExchangeEventUpdateList = new ArrayList<TixCityZonePricingExchangeEvent>();
			if(dbMiniExchangeEventList==null){
				return false;
			}
			for(TixCityZonePricingExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<TixCityZonePricingExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<TixCityZonePricingExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				TixCityZonePricingExchangeEvent toBeUpdateExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				TixCityZonePricingExchangeEventAudit audit = new TixCityZonePricingExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				miniExchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						TixCityZonePricingExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							TixCityZonePricingExchangeEventAudit audit = new TixCityZonePricingExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							miniExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getTixCityZonePricingExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getTixCityZonePricingExchangeEventDAO().updateAll(miniExchangeEventUpdateList);
			
		}else if(productId == 15){//  Manhattan ZonesPricing Exchanges
			Collection<ManhattanZonePricingExchangeEvent> dbMiniExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbMiniExchangeEventList  = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAllManhattanZonePricingExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbMiniExchangeEventList  = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAllManhattanZonePricingExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbMiniExchangeEventList  = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAllManhattanZonePricingExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,ManhattanZonePricingExchangeEvent> dbMiniEventMap = new HashMap<Integer, ManhattanZonePricingExchangeEvent>();
			
			List<ManhattanZonePricingExchangeEvent> miniExchangeEventUpdateList = new ArrayList<ManhattanZonePricingExchangeEvent>();
			if(dbMiniExchangeEventList==null){
				return false;
			}
			for(ManhattanZonePricingExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<ManhattanZonePricingExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ManhattanZonePricingExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				ManhattanZonePricingExchangeEvent toBeUpdateExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(toBeUpdateExchangeEvent!=null){
				ManhattanZonePricingExchangeEventAudit audit = new ManhattanZonePricingExchangeEventAudit(toBeUpdateExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				toBeUpdateExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				toBeUpdateExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				toBeUpdateExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				toBeUpdateExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				toBeUpdateExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				toBeUpdateExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				toBeUpdateExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				toBeUpdateExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				toBeUpdateExchangeEvent.setExposure(exchangeEvent.getExposure());
				toBeUpdateExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				toBeUpdateExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				toBeUpdateExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				toBeUpdateExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				toBeUpdateExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				toBeUpdateExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				toBeUpdateExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				toBeUpdateExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				toBeUpdateExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				toBeUpdateExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				toBeUpdateExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				toBeUpdateExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				miniExchangeEventUpdateList.add(toBeUpdateExchangeEvent);
				}
			}
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						ManhattanZonePricingExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							ManhattanZonePricingExchangeEventAudit audit = new ManhattanZonePricingExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							miniExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getManhattanZonePricingExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getManhattanZonePricingExchangeEventDAO().updateAll(miniExchangeEventUpdateList);
			
		} else if(productId == 19){// SGLastFiveRow Exchanges
			Collection<SGLastFiveRowExchangeEvent> dbSGLastFiveRowExchangeEventList = null;
			if(filterType.equalsIgnoreCase("Artist")){
				dbSGLastFiveRowExchangeEventList  = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAllSGLastFiveRowExchangeEventsByArtistId(filterId);
			}else if(filterType.equalsIgnoreCase("Venue")){
				dbSGLastFiveRowExchangeEventList  = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAllSGLastFiveRowExchangeEventsByVenueId(filterId);
			}else if(filterType.equalsIgnoreCase("GrandChildCategory")){
				dbSGLastFiveRowExchangeEventList  = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAllSGLastFiveRowExchangeEventsByGrandChildCategoryId(filterId);
			}
			Map<Integer,SGLastFiveRowExchangeEvent> dbMiniEventMap = new HashMap<Integer, SGLastFiveRowExchangeEvent>();
			
			List<SGLastFiveRowExchangeEvent> miniExchangeEventUpdateList = new ArrayList<SGLastFiveRowExchangeEvent>();
			if(dbSGLastFiveRowExchangeEventList==null){
				return false;
			}
			for(SGLastFiveRowExchangeEvent exchangeEvent:dbSGLastFiveRowExchangeEventList){
				dbMiniEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			List<SGLastFiveRowExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<SGLastFiveRowExchangeEventAudit>();
			for(ExchangeEvent exchangeEvent:exchangeEvents){
				
				SGLastFiveRowExchangeEvent miniExchangeEvent = dbMiniEventMap.get(exchangeEvent.getEvent().getId());
				if(miniExchangeEvent!=null){
				SGLastFiveRowExchangeEventAudit audit = new SGLastFiveRowExchangeEventAudit(miniExchangeEvent);
				exchangeEventUpdateAuditList.add(audit);
				miniExchangeEvent.setAllowSectionRange(exchangeEvent.getAllowSectionRange());
				miniExchangeEvent.setTicketNetworkBrokerId(exchangeEvent.getTicketNetworkBrokerId());
				miniExchangeEvent.setScoreBigBrokerId(exchangeEvent.getScoreBigBrokerId());
				miniExchangeEvent.setFanxchangeBrokerId(exchangeEvent.getFanxchangeBrokerId());
				miniExchangeEvent.setTicketcityBrokerId(exchangeEvent.getTicketcityBrokerId());
				miniExchangeEvent.setSeatGeekBrokerId(exchangeEvent.getSeatGeekBrokerId());
				miniExchangeEvent.setVividBrokerId(exchangeEvent.getVividBrokerId());
				miniExchangeEvent.setZoneTicketBrokerId(exchangeEvent.getZoneTicketBrokerId());
				miniExchangeEvent.setExposure(exchangeEvent.getExposure());
				miniExchangeEvent.setLastUpdatedBy(exchangeEvent.getLastUpdatedBy());
				miniExchangeEvent.setLastUpdatedDate(exchangeEvent.getLastUpdatedDate());
				miniExchangeEvent.setLowerMarkup(exchangeEvent.getLowerMarkup());
				miniExchangeEvent.setLowerShippingFees(exchangeEvent.getLowerShippingFees());
				miniExchangeEvent.setUpperMarkup(exchangeEvent.getUpperMarkup());
				miniExchangeEvent.setUpperShippingFees(exchangeEvent.getUpperShippingFees());
				miniExchangeEvent.setNearTermDisplayOption(exchangeEvent.getNearTermDisplayOption());
				miniExchangeEvent.setPriceBreakup(exchangeEvent.getPriceBreakup());
				miniExchangeEvent.setRptFactor(exchangeEvent.getRptFactor());
				miniExchangeEvent.setShippingDays(exchangeEvent.getShippingDays());
				miniExchangeEvent.setShippingMethod(exchangeEvent.getShippingMethod());
				miniExchangeEvent.setStatus(exchangeEvent.getStatus());
				
				miniExchangeEventUpdateList.add(miniExchangeEvent);
				}
			}
			
			if(unCheckedEventIdsStr!=null){
				for (String eventId : unCheckedEventIdsStr) {
					try{
						SGLastFiveRowExchangeEvent exchangeEvent = dbMiniEventMap.remove(Integer.parseInt(eventId));
						if(exchangeEvent != null) {
							SGLastFiveRowExchangeEventAudit audit = new SGLastFiveRowExchangeEventAudit(exchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							
							exchangeEvent.setStatus("DELETED");
							exchangeEvent.setLastUpdatedDate(new Date());
							exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
							miniExchangeEventUpdateList.add(exchangeEvent);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			DAORegistry.getSgLastFiveRowExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
			DAORegistry.getSgLastFiveRowExchangeEventDAO().updateAll(miniExchangeEventUpdateList);
		}
		return true;
	}
	@RequestMapping(value="/ManageZonePricingExchangeEvent")
	public String manageZonesPricingExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			//	ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			
			Broker zonesPricingBroker = DAORegistry.getBrokerDAO().getBrokerByName("Reserve One Tickets");
			Collection<Broker> brokers = new ArrayList<Broker>();
			brokers.add(zonesPricingBroker);
			
			model.addAttribute("brokers", brokers);
			Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
			for(Broker broker:brokers){
				brokerMap.put(broker.getId(), broker);
			}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(8);
			model.addAttribute("url", "ManageZonePricingExchangeEvent");
			model.addAttribute("auditUrl", "ZonePricingExchangeEventAuditPopup");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<ZonePricingExchangeEvent> dbZonePricingExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String[] temp= eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbZonePricingExchangeEventList  = DAORegistry.getZonePricingExchangeEventDAO().getAllZonePricingExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbZonePricingExchangeEventList  = DAORegistry.getZonePricingExchangeEventDAO().getAllZonePricingExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbZonePricingExchangeEventList  = DAORegistry.getZonePricingExchangeEventDAO().getAllZonePricingExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				/*Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}*/
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,ZonePricingExchangeEvent> dbZonePricingExchangeEventMap = new HashMap<Integer, ZonePricingExchangeEvent>();
				Map<Integer,ZonePricingExchangeEvent> dbZonePricingExchangeEventUpdateMap = new HashMap<Integer, ZonePricingExchangeEvent>();
				List<ZonePricingExchangeEvent> exchangeEventList = new ArrayList<ZonePricingExchangeEvent>();
				List<ZonePricingExchangeEvent> exchangeEventUpdateList = new ArrayList<ZonePricingExchangeEvent>();
				for(ZonePricingExchangeEvent exchangeEvent:dbZonePricingExchangeEventList){
					dbZonePricingExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbZonePricingExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<ZonePricingExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ZonePricingExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							ZonePricingExchangeEvent dbZonePricingExchangeEvent = dbZonePricingExchangeEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbZonePricingExchangeEvent!=null){
								ZonePricingExchangeEventAudit audit = new ZonePricingExchangeEventAudit(dbZonePricingExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbZonePricingExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbZonePricingExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
								//dbZonePricingExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								dbZonePricingExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbZonePricingExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbZonePricingExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbZonePricingExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//								dbZonePricingExchangeEvent.setPriceBreakup(Integer.parseInt(request.getParameter("priceBreakup_"+eventId)));
								dbZonePricingExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbZonePricingExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbZonePricingExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbZonePricingExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbZonePricingExchangeEvent.setStatus("ACTIVE");
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbZonePricingExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbZonePricingExchangeEvent.setShippingMethod(null);
								}
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbZonePricingExchangeEvent.setDiscountZone(isDiscountZone);
								
								if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbZonePricingExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbZonePricingExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbZonePricingExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbZonePricingExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbZonePricingExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbZonePricingExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbZonePricingExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbZonePricingExchangeEvent.setZoneTicketBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbZonePricingExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbZonePricingExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbZonePricingExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbZonePricingExchangeEvent.setTicketcityBrokerId(null);
								}
								if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
									dbZonePricingExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
								}else{
									dbZonePricingExchangeEvent.setTicketcityBrokerId(null);
								}
								
								dbZonePricingExchangeEvent.setLastUpdatedDate(now);
								dbZonePricingExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								exchangeEventUpdateList.add(dbZonePricingExchangeEvent);
								exchangeEvents.add(dbZonePricingExchangeEvent);
							}
							
						}
					}
					if(temp!=null){
						for (String eventId : temp) {
							try{
								ZonePricingExchangeEvent exchangeEvent = dbZonePricingExchangeEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									ZonePricingExchangeEventAudit audit = new ZonePricingExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEventUpdateList.add(exchangeEvent);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					
					DAORegistry.getZonePricingExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getZonePricingExchangeEventDAO().updateAll(exchangeEventUpdateList);
					if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						try{
							selectedEvent += eventId + ",";
							ZonePricingExchangeEvent tempZonesPricingExchangeEvent = dbZonePricingExchangeEventMap.get(Integer.parseInt(eventId));
							 if(tempZonesPricingExchangeEvent!=null){
								 exchangeEventList.add(tempZonesPricingExchangeEvent);
							 }
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-common-product-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="/ManageTixCityZonePricingExchangeEvent")
	public String manageTixCityZonesPricingExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			//	ModelAndView mav = new ModelAndView("page-mini-exchange-event");
				Broker tixCityBroker = DAORegistry.getBrokerDAO().getBrokerByName("Right This Way");
				Collection<Broker> brokers = new ArrayList<Broker>();
				brokers.add(tixCityBroker);
				
				//Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
				model.addAttribute("brokers", brokers);
				Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
				for(Broker broker:brokers){
					brokerMap.put(broker.getId(), broker);
				}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(14);
			model.addAttribute("url", "ManageTixCityZonePricingExchangeEvent");
			model.addAttribute("auditUrl", "TixCityZonePricingExchangeEventAuditPopup");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<TixCityZonePricingExchangeEvent> dbTixCityZonePricingExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String[] temp= eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbTixCityZonePricingExchangeEventList  = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbTixCityZonePricingExchangeEventList  = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbTixCityZonePricingExchangeEventList  = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				/*Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}*/
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,TixCityZonePricingExchangeEvent> dbTixCityZonePricingExchangeEventMap = new HashMap<Integer, TixCityZonePricingExchangeEvent>();
				Map<Integer,TixCityZonePricingExchangeEvent> dbTixCityZonePricingExchangeEventUpdateMap = new HashMap<Integer, TixCityZonePricingExchangeEvent>();
				List<TixCityZonePricingExchangeEvent> exchangeEventList = new ArrayList<TixCityZonePricingExchangeEvent>();
				List<TixCityZonePricingExchangeEvent> exchangeEventUpdateList = new ArrayList<TixCityZonePricingExchangeEvent>();
				for(TixCityZonePricingExchangeEvent exchangeEvent:dbTixCityZonePricingExchangeEventList){
					dbTixCityZonePricingExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbTixCityZonePricingExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<TixCityZonePricingExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<TixCityZonePricingExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							TixCityZonePricingExchangeEvent dbTixCityZonePricingExchangeEvent = dbTixCityZonePricingExchangeEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbTixCityZonePricingExchangeEvent!=null){
								TixCityZonePricingExchangeEventAudit audit = new TixCityZonePricingExchangeEventAudit(dbTixCityZonePricingExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbTixCityZonePricingExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbTixCityZonePricingExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
								//dbTixCityZonePricingExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//								dbTixCityZonePricingExchangeEvent.setPriceBreakup(Integer.parseInt(request.getParameter("priceBreakup_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbTixCityZonePricingExchangeEvent.setStatus("ACTIVE");
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbTixCityZonePricingExchangeEvent.setDiscountZone(isDiscountZone);
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbTixCityZonePricingExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbTixCityZonePricingExchangeEvent.setShippingMethod(null);
								}
								
								if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbTixCityZonePricingExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbTixCityZonePricingExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbTixCityZonePricingExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbTixCityZonePricingExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbTixCityZonePricingExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbTixCityZonePricingExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbTixCityZonePricingExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbTixCityZonePricingExchangeEvent.setZoneTicketBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbTixCityZonePricingExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbTixCityZonePricingExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbTixCityZonePricingExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbTixCityZonePricingExchangeEvent.setTicketcityBrokerId(null);
								}
								if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
									dbTixCityZonePricingExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
								}else{
									dbTixCityZonePricingExchangeEvent.setTicketcityBrokerId(null);
								}
								
								dbTixCityZonePricingExchangeEvent.setLastUpdatedDate(now);
								dbTixCityZonePricingExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								exchangeEventUpdateList.add(dbTixCityZonePricingExchangeEvent);
								exchangeEvents.add(dbTixCityZonePricingExchangeEvent);
							}
							
						}
					}
					if(temp!=null){
						for (String eventId : temp) {
							try{
								TixCityZonePricingExchangeEvent exchangeEvent = dbTixCityZonePricingExchangeEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									TixCityZonePricingExchangeEventAudit audit = new TixCityZonePricingExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEventUpdateList.add(exchangeEvent);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					
					DAORegistry.getTixCityZonePricingExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getTixCityZonePricingExchangeEventDAO().updateAll(exchangeEventUpdateList);
					if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						try{
							selectedEvent += eventId + ",";
							TixCityZonePricingExchangeEvent tempZonesPricingExchangeEvent = dbTixCityZonePricingExchangeEventMap.get(Integer.parseInt(eventId));
							 if(tempZonesPricingExchangeEvent!=null){
								 exchangeEventList.add(tempZonesPricingExchangeEvent);
							 }
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-common-product-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="/ManageManhattanZonePricingExchangeEvent")
	public String manageManhattanZonesPricingExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){

		try {
			//	ModelAndView mav = new ModelAndView("page-mini-exchange-event");
				Broker manhattanBroker = DAORegistry.getBrokerDAO().getBrokerByName("Manhattan");
				Collection<Broker> brokers = new ArrayList<Broker>();
				brokers.add(manhattanBroker);
				
				//Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
				model.addAttribute("brokers", brokers);
				Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
				for(Broker broker:brokers){
					brokerMap.put(broker.getId(), broker);
				}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(15);
			model.addAttribute("url", "ManageManhattanZonePricingExchangeEvent");
			model.addAttribute("auditUrl", "ManhattanZonePricingExchangeEventAuditPopup");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<ManhattanZonePricingExchangeEvent> dbManhattanZonePricingExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String[] temp= eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbManhattanZonePricingExchangeEventList  = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAllManhattanZonePricingExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbManhattanZonePricingExchangeEventList  = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAllManhattanZonePricingExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbManhattanZonePricingExchangeEventList  = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAllManhattanZonePricingExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				/*Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}*/
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,ManhattanZonePricingExchangeEvent> dbManhattanZonePricingExchangeEventMap = new HashMap<Integer, ManhattanZonePricingExchangeEvent>();
				Map<Integer,ManhattanZonePricingExchangeEvent> dbManhattanZonePricingExchangeEventUpdateMap = new HashMap<Integer, ManhattanZonePricingExchangeEvent>();
				List<ManhattanZonePricingExchangeEvent> exchangeEventList = new ArrayList<ManhattanZonePricingExchangeEvent>();
				List<ManhattanZonePricingExchangeEvent> exchangeEventUpdateList = new ArrayList<ManhattanZonePricingExchangeEvent>();
				for(ManhattanZonePricingExchangeEvent exchangeEvent:dbManhattanZonePricingExchangeEventList){
					dbManhattanZonePricingExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbManhattanZonePricingExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<ManhattanZonePricingExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ManhattanZonePricingExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							ManhattanZonePricingExchangeEvent dbManhattanZonePricingExchangeEvent = dbManhattanZonePricingExchangeEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbManhattanZonePricingExchangeEvent!=null){
								ManhattanZonePricingExchangeEventAudit audit = new ManhattanZonePricingExchangeEventAudit(dbManhattanZonePricingExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbManhattanZonePricingExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbManhattanZonePricingExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
								//dbManhattanZonePricingExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//								dbManhattanZonePricingExchangeEvent.setPriceBreakup(Integer.parseInt(request.getParameter("priceBreakup_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbManhattanZonePricingExchangeEvent.setStatus("ACTIVE");
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbManhattanZonePricingExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbManhattanZonePricingExchangeEvent.setShippingMethod(null);
								}
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbManhattanZonePricingExchangeEvent.setDiscountZone(isDiscountZone);
								
								if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbManhattanZonePricingExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbManhattanZonePricingExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbManhattanZonePricingExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbManhattanZonePricingExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbManhattanZonePricingExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbManhattanZonePricingExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbManhattanZonePricingExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbManhattanZonePricingExchangeEvent.setZoneTicketBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbManhattanZonePricingExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbManhattanZonePricingExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbManhattanZonePricingExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbManhattanZonePricingExchangeEvent.setTicketcityBrokerId(null);
								}
								if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
									dbManhattanZonePricingExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
								}else{
									dbManhattanZonePricingExchangeEvent.setTicketcityBrokerId(null);
								}
								
								dbManhattanZonePricingExchangeEvent.setLastUpdatedDate(now);
								dbManhattanZonePricingExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								exchangeEventUpdateList.add(dbManhattanZonePricingExchangeEvent);
								exchangeEvents.add(dbManhattanZonePricingExchangeEvent);
							}
							
						}
					}
					if(temp!=null){
						for (String eventId : temp) {
							try{
								ManhattanZonePricingExchangeEvent exchangeEvent = dbManhattanZonePricingExchangeEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									ManhattanZonePricingExchangeEventAudit audit = new ManhattanZonePricingExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEventUpdateList.add(exchangeEvent);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					
					DAORegistry.getManhattanZonePricingExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getManhattanZonePricingExchangeEventDAO().updateAll(exchangeEventUpdateList);
					if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						try{
							selectedEvent += eventId + ",";
							ManhattanZonePricingExchangeEvent tempZonesPricingExchangeEvent = dbManhattanZonePricingExchangeEventMap.get(Integer.parseInt(eventId));
							 if(tempZonesPricingExchangeEvent!=null){
								 exchangeEventList.add(tempZonesPricingExchangeEvent);
							 }
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-common-product-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	
	}

	@RequestMapping(value="/ManageLarryLastExchangeEvent")
	public String manageLarryLastExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			model.addAttribute("brokers", brokers);
			Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
			for(Broker broker:brokers){
				brokerMap.put(broker.getId(), broker);
			}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(6);
			
			model.addAttribute("url", "ManageLarryLastExchangeEvent");
			model.addAttribute("auditUrl", "LarryLastExchangeEventAuditPopup");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<LarryLastExchangeEvent> dbLarryExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String[] temp = eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbLarryExchangeEventList  = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbLarryExchangeEventList  = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbLarryExchangeEventList  = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				/*Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}*/
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,LarryLastExchangeEvent> dbLarryLastEventMap = new HashMap<Integer, LarryLastExchangeEvent>();
				Map<Integer,LarryLastExchangeEvent> dbLarryLastEventUpdateMap = new HashMap<Integer, LarryLastExchangeEvent>();
				
				
				List<LarryLastExchangeEvent> exchangeEventList = new ArrayList<LarryLastExchangeEvent>();
				List<LarryLastExchangeEvent> exchangeEventUpdateList = new ArrayList<LarryLastExchangeEvent>();
				for(LarryLastExchangeEvent exchangeEvent:dbLarryExchangeEventList){
					dbLarryLastEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbLarryLastEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<LarryLastExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<LarryLastExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							LarryLastExchangeEvent dbLarryExchangeEvent = dbLarryLastEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbLarryExchangeEvent!=null){
								LarryLastExchangeEventAudit audit = new LarryLastExchangeEventAudit(dbLarryExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbLarryExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbLarryExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
//								dbLarryExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								dbLarryExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbLarryExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbLarryExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbLarryExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//								dbLarryExchangeEvent.setPriceBreakup(Integer.parseInt(request.getParameter("priceBreakup_"+eventId)));
								dbLarryExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbLarryExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbLarryExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbLarryExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbLarryExchangeEvent.setStatus("ACTIVE");
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbLarryExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbLarryExchangeEvent.setShippingMethod(null);
								}
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setDiscountZone(isDiscountZone);
								
								String vividSeatEnabledStr = request.getParameter("vividEnabled_"+eventId);
								boolean  vividSeatEnabled = ((vividSeatEnabledStr==null || !vividSeatEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setVividEnabled(vividSeatEnabled);
								
								String rotEnabledStr = request.getParameter("rotEnabled_"+eventId);
								boolean  rotEnabled = ((rotEnabledStr==null || !rotEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setRotEnabled(rotEnabled);
								
								String tixcityEnabledStr = request.getParameter("tixcityEnabled_"+eventId);
								boolean  tixcityEnabled = ((tixcityEnabledStr==null || !tixcityEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setTixcityEnabled(tixcityEnabled);
								
								String rtwEnabledStr = request.getParameter("rtwEnabled_"+eventId);
								boolean  rtwEnabled = ((rtwEnabledStr==null || !rtwEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setRtwEnabled(rtwEnabled);
								
								String zonedLastrowMinicatsEnabledStr = request.getParameter("zonedLastrowMinicatsEnabled_"+eventId);
								boolean  zonedLastrowMinicatsEnabled = ((zonedLastrowMinicatsEnabledStr==null || !zonedLastrowMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setZonedLastrowMinicatsEnabled(zonedLastrowMinicatsEnabled);
								
								String larryLastEnabledStr = request.getParameter("larryLastEnabled_"+eventId);
								boolean  larryLastEnabled = ((larryLastEnabledStr==null || !larryLastEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setLarryLastEnabled(larryLastEnabled);
								
								/*if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setZoneTicketBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setTicketcityBrokerId(null);
								}*/
								
								dbLarryExchangeEvent.setLastUpdatedDate(now);
								dbLarryExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								dbLarryExchangeEvent.setPreSaleSettingsUpdated(Boolean.TRUE);
								exchangeEventUpdateList.add(dbLarryExchangeEvent);
								exchangeEvents.add(dbLarryExchangeEvent);
							}
							
						}
					}
					if(temp!=null){
						for (String eventId : temp) {
							try{
								LarryLastExchangeEvent exchangeEvent = dbLarryLastEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									LarryLastExchangeEventAudit audit = new LarryLastExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEvent.setPreSaleSettingsUpdated(Boolean.TRUE);
									exchangeEventUpdateList.add(exchangeEvent);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getLarryLastExchangeEventDAO().updateAll(exchangeEventUpdateList);
					/*if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}*/
					
					
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						try{
							selectedEvent += eventId + ",";
							LarryLastExchangeEvent tempLarryExchangeEvent = dbLarryLastEventMap.get(Integer.parseInt(eventId));
							if(tempLarryExchangeEvent!=null){
								exchangeEventList.add(tempLarryExchangeEvent);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					//DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					//DAORegistry.getLarryLastExchangeEventDAO().updateAll(exchangeEventUpdateList);
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-larrylast-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	
	
	@RequestMapping(value="/ManageZoneLastRowExchangeEvent")
	public String manageZoneLastRowExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			model.addAttribute("brokers", brokers);
			Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
			for(Broker broker:brokers){
				brokerMap.put(broker.getId(), broker);
			}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(7);
			
			model.addAttribute("url", "ManageZoneLastRowExchangeEvent");
			model.addAttribute("auditUrl", "ZoneLastRowExchangeEventAuditPopup");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<ZoneLastRowMiniExchangeEvent> dbLarryExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String[] temp = eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbLarryExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbLarryExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbLarryExchangeEventList  = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				/*Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}*/
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,ZoneLastRowMiniExchangeEvent> dbLarryLastEventMap = new HashMap<Integer, ZoneLastRowMiniExchangeEvent>();
				Map<Integer,ZoneLastRowMiniExchangeEvent> dbLarryLastEventUpdateMap = new HashMap<Integer, ZoneLastRowMiniExchangeEvent>();
				
				
				List<ZoneLastRowMiniExchangeEvent> exchangeEventList = new ArrayList<ZoneLastRowMiniExchangeEvent>();
				List<ZoneLastRowMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<ZoneLastRowMiniExchangeEvent>();
				for(ZoneLastRowMiniExchangeEvent exchangeEvent:dbLarryExchangeEventList){
					dbLarryLastEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbLarryLastEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<ZoneLastRowMiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ZoneLastRowMiniExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							ZoneLastRowMiniExchangeEvent dbLarryExchangeEvent = dbLarryLastEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbLarryExchangeEvent!=null){
								ZoneLastRowMiniExchangeEventAudit audit = new ZoneLastRowMiniExchangeEventAudit(dbLarryExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbLarryExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbLarryExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
//								dbLarryExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								dbLarryExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbLarryExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbLarryExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbLarryExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//								dbLarryExchangeEvent.setPriceBreakup(Integer.parseInt(request.getParameter("priceBreakup_"+eventId)));
								dbLarryExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbLarryExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbLarryExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbLarryExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbLarryExchangeEvent.setStatus("ACTIVE");
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbLarryExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbLarryExchangeEvent.setShippingMethod(null);
								}
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setDiscountZone(isDiscountZone);
								
								String vividSeatEnabledStr = request.getParameter("vividEnabled_"+eventId);
								boolean  vividSeatEnabled = ((vividSeatEnabledStr==null || !vividSeatEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setVividEnabled(vividSeatEnabled);
								
								String rotEnabledStr = request.getParameter("rotEnabled_"+eventId);
								boolean  rotEnabled = ((rotEnabledStr==null || !rotEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setRotEnabled(rotEnabled);
								
								String tixcityEnabledStr = request.getParameter("tixcityEnabled_"+eventId);
								boolean  tixcityEnabled = ((tixcityEnabledStr==null || !tixcityEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setTixcityEnabled(tixcityEnabled);
								
								String rtwEnabledStr = request.getParameter("rtwEnabled_"+eventId);
								boolean  rtwEnabled = ((rtwEnabledStr==null || !rtwEnabledStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setRtwEnabled(rtwEnabled);
								
								/*if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setZoneTicketBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setTicketcityBrokerId(null);
								}*/
								
								dbLarryExchangeEvent.setLastUpdatedDate(now);
								dbLarryExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								exchangeEventUpdateList.add(dbLarryExchangeEvent);
								exchangeEvents.add(dbLarryExchangeEvent);
							}
							
						}
					}
					if(temp!=null){
						for (String eventId : temp) {
							try{
								ZoneLastRowMiniExchangeEvent exchangeEvent = dbLarryLastEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									ZoneLastRowMiniExchangeEventAudit audit = new ZoneLastRowMiniExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEventUpdateList.add(exchangeEvent);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getZoneLastRowMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
					/*if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}*/
					
					
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						try{
							selectedEvent += eventId + ",";
							ZoneLastRowMiniExchangeEvent tempLarryExchangeEvent = dbLarryLastEventMap.get(Integer.parseInt(eventId));
							if(tempLarryExchangeEvent!=null){
								exchangeEventList.add(tempLarryExchangeEvent);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					//DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					//DAORegistry.getLarryLastExchangeEventDAO().updateAll(exchangeEventUpdateList);
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-larrylast-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	
	
	@RequestMapping(value="/ManageTNSpecialExchangeEvent")
	public String manageTNSpecialExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			model.addAttribute("brokers", brokers);
			Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
			for(Broker broker:brokers){
				brokerMap.put(broker.getId(), broker);
			}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(6);
			
			model.addAttribute("url", "ManageTNSpecialExchangeEvent");
			model.addAttribute("auditUrl", "TNSpecialExchangeEventAuditPopup");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<TicketNetworkSSAccountExchangeEvent> dbLarryExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String[] temp = eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbLarryExchangeEventList  = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAllTNSpecialExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbLarryExchangeEventList  = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAllTNSpecialExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbLarryExchangeEventList  = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAllTNSpecialExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				/*Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}*/
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,TicketNetworkSSAccountExchangeEvent> dbLarryLastEventMap = new HashMap<Integer, TicketNetworkSSAccountExchangeEvent>();
				Map<Integer,TicketNetworkSSAccountExchangeEvent> dbLarryLastEventUpdateMap = new HashMap<Integer, TicketNetworkSSAccountExchangeEvent>();
				
				
				List<TicketNetworkSSAccountExchangeEvent> exchangeEventList = new ArrayList<TicketNetworkSSAccountExchangeEvent>();
				List<TicketNetworkSSAccountExchangeEvent> exchangeEventUpdateList = new ArrayList<TicketNetworkSSAccountExchangeEvent>();
				for(TicketNetworkSSAccountExchangeEvent exchangeEvent:dbLarryExchangeEventList){
					dbLarryLastEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbLarryLastEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<TicketNetworkSSAccountExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<TicketNetworkSSAccountExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							TicketNetworkSSAccountExchangeEvent dbLarryExchangeEvent = dbLarryLastEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbLarryExchangeEvent!=null){
								TicketNetworkSSAccountExchangeEventAudit audit = new TicketNetworkSSAccountExchangeEventAudit(dbLarryExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbLarryExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbLarryExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
								//dbLarryExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								dbLarryExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbLarryExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbLarryExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbLarryExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//								dbLarryExchangeEvent.setPriceBreakup(Integer.parseInt(request.getParameter("priceBreakup_"+eventId)));
								dbLarryExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbLarryExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbLarryExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbLarryExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbLarryExchangeEvent.setStatus("ACTIVE");
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbLarryExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbLarryExchangeEvent.setShippingMethod(null);
								}
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbLarryExchangeEvent.setDiscountZone(isDiscountZone);
								
								if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setZoneTicketBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setTicketcityBrokerId(null);
								}
								if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
									dbLarryExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
								}else{
									dbLarryExchangeEvent.setTicketcityBrokerId(null);
								}
								
								dbLarryExchangeEvent.setLastUpdatedDate(now);
								dbLarryExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								exchangeEventUpdateList.add(dbLarryExchangeEvent);
								exchangeEvents.add(dbLarryExchangeEvent);
							}
							
						}
					}
					if(temp!=null){
						for (String eventId : temp) {
							try{
								TicketNetworkSSAccountExchangeEvent exchangeEvent = dbLarryLastEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									TicketNetworkSSAccountExchangeEventAudit audit = new TicketNetworkSSAccountExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEventUpdateList.add(exchangeEvent);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					DAORegistry.getTicketNetworkSSAccountExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().updateAll(exchangeEventUpdateList);
					if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}
					
					
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						try{
							selectedEvent += eventId + ",";
							TicketNetworkSSAccountExchangeEvent tempLarryExchangeEvent = dbLarryLastEventMap.get(Integer.parseInt(eventId));
							if(tempLarryExchangeEvent!=null){
								exchangeEventList.add(tempLarryExchangeEvent);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					//DAORegistry.getTicketNetworkSSAccountExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					//DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().updateAll(exchangeEventUpdateList);
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-common-product-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	
	
	@RequestMapping(value="/ManageVipMiniExchangeEvent")
	public String manageVipMiniExchangeEventExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			model.addAttribute("brokers", brokers);
			Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
			for(Broker broker:brokers){
				brokerMap.put(broker.getId(), broker);
			}
//			ModelAndView mav = new ModelAndView("page-mini-exchange-event");
			String action = request.getParameter("action");
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(4);
			model.addAttribute("product", product);
			Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(4);
			model.addAttribute("url", "ManageVipMiniExchangeEvent");
			model.addAttribute("auditUrl", "VipMiniExchangeEventAuditPopup");
			model.addAttribute("products", autopricingProducts);
			if(action==""){
				model.addAttribute("message", "Please select valid artist ");
			}
			if(action!=null && action!=""){
				Collection<Event> events = null;
				Collection<VipMiniExchangeEvent> dbVipMiniExchangeEventList= null;
				String artistStr = request.getParameter("artist");
				String[] eventStr = request.getParameterValues("events");
				String[] temp= eventStr;
				String venueStr = request.getParameter("venue");
				String grandChildStr = request.getParameter("grandChild");
//				String childStr = request.getParameter("child");
				String selectedEvent = "";
				String eventsCheckAll = request.getParameter("eventsCheckAll");
				boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
				String selectedValue = "";
				String selectedOption = "";
				Integer selectedId = null;
				if(artistStr!=null && !artistStr.isEmpty()){
					Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
					dbVipMiniExchangeEventList  = DAORegistry.getVipMiniExchangeEventDAO().getAllVipMiniExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedOption="Artist";
					selectedValue= artist.getName();
					selectedId = artist.getId();
				}else if(venueStr!=null && !venueStr.isEmpty()){
					Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
					dbVipMiniExchangeEventList  = DAORegistry.getVipMiniExchangeEventDAO().getAllVipMiniExchangeEventsByVenueId(Integer.valueOf(venueStr));
					selectedOption="Venue";
					selectedValue= venue.getBuilding();
					events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
					selectedId = venue.getId();
				}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbVipMiniExchangeEventList  = DAORegistry.getVipMiniExchangeEventDAO().getAllVipMiniExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}
				
				String brokerIdStr = request.getParameter("brokerId");
				/*Integer brokerId = null;
				
				if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
					brokerId = Integer.parseInt(brokerIdStr);
				}*/
				model.addAttribute("broker", brokerIdStr);
			   
				Map<Integer,VipMiniExchangeEvent> dbVipMiniExchangeEventMap = new HashMap<Integer, VipMiniExchangeEvent>();
				Map<Integer,VipMiniExchangeEvent> dbVipMiniExchangeEventUpdateMap = new HashMap<Integer, VipMiniExchangeEvent>();
				List<VipMiniExchangeEvent> exchangeEventList = new ArrayList<VipMiniExchangeEvent>();
				List<VipMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<VipMiniExchangeEvent>();
				for(VipMiniExchangeEvent exchangeEvent:dbVipMiniExchangeEventList){
					dbVipMiniExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					dbVipMiniExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				}
				
				List<VipMiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<VipMiniExchangeEventAudit>();
				List<Integer> productIdList = new ArrayList<Integer>();
				List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
				if(action.equalsIgnoreCase("update")){
					List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
					Map<String, String[]> requestParams = request.getParameterMap();
					Date now = new Date();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("product")){
							productIdList.add(Integer.parseInt(key.replace("product-","")));
						}
						if(key.contains("checkbox_")){
							Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
							temp = StringUtil.removeFromArray(temp,eventId.toString());
							VipMiniExchangeEvent dbVipMiniExchangeEvent = dbVipMiniExchangeEventUpdateMap.remove(eventId);
							toBeUpdaetEventIdList.add(eventId);
							if(dbVipMiniExchangeEvent!=null){
								VipMiniExchangeEventAudit audit = new VipMiniExchangeEventAudit(dbVipMiniExchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
								boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
								
								dbVipMiniExchangeEvent.setAllowSectionRange(isAllowSectionRange);
								dbVipMiniExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
								/*dbVipMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));*/
								dbVipMiniExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
								dbVipMiniExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
								dbVipMiniExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
								dbVipMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//								dbVipMiniExchangeEvent.setPriceBreakup(Integer.parseInt(request.getParameter("priceBreakup_"+eventId)));
								dbVipMiniExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
								dbVipMiniExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
								dbVipMiniExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
								dbVipMiniExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
								dbVipMiniExchangeEvent.setStatus("ACTIVE");
								
								if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
									dbVipMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
								}else{
									dbVipMiniExchangeEvent.setShippingMethod(null);
								}
								
								String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
								boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
								dbVipMiniExchangeEvent.setDiscountZone(isDiscountZone);
								
								if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
									dbVipMiniExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								}else{
									dbVipMiniExchangeEvent.setTicketNetworkBrokerId(null);
								}
								if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
									dbVipMiniExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
								}else{
									dbVipMiniExchangeEvent.setVividBrokerId(null);
								}
								if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
									dbVipMiniExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
								}else{
									dbVipMiniExchangeEvent.setScoreBigBrokerId(null);
								}
								if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
									dbVipMiniExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
								}else{
									dbVipMiniExchangeEvent.setZoneTicketBrokerId(null);
								}
								if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
									dbVipMiniExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
								}else{
									dbVipMiniExchangeEvent.setFanxchangeBrokerId(null);
								}
								if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
									dbVipMiniExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
								}else{
									dbVipMiniExchangeEvent.setTicketcityBrokerId(null);
								}
								if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
									dbVipMiniExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
								}else{
									dbVipMiniExchangeEvent.setTicketcityBrokerId(null);
								}
								
								dbVipMiniExchangeEvent.setLastUpdatedDate(now);
								dbVipMiniExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
										.getAuthentication().getName());  // Need to change after login functionality
								exchangeEventUpdateList.add(dbVipMiniExchangeEvent);
								exchangeEvents.add(dbVipMiniExchangeEvent);
							}
							
						}
					}
					
					if(temp!=null){
						for (String eventId : temp) {
							try{
								VipMiniExchangeEvent exchangeEvent = dbVipMiniExchangeEventUpdateMap.get(Integer.parseInt(eventId));
								if(exchangeEvent!=null){
									VipMiniExchangeEventAudit audit = new VipMiniExchangeEventAudit(exchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									
									exchangeEvent.setStatus("DELETED");
									exchangeEvent.setLastUpdatedDate(now);
									exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
									exchangeEventUpdateList.add(exchangeEvent);
									
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					
					DAORegistry.getVipMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
					DAORegistry.getVipMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
					
					if(!productIdList.isEmpty()){
						for(Integer id : productIdList){
							updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
						}
						
					}
					
				}
				if(eventStr!=null){
					for (String eventId : eventStr) {
						
						try{
							selectedEvent += eventId + ",";
							VipMiniExchangeEvent tempVipMiniExchangeEvent = dbVipMiniExchangeEventMap.get(Integer.parseInt(eventId));
							 if(tempVipMiniExchangeEvent!=null){
								 exchangeEventList.add(tempVipMiniExchangeEvent);
							 }
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				model.addAttribute("eventsCheckAll", isAllEvent);
				model.addAttribute("events", events);
				model.addAttribute("eventStr", selectedEvent);
				model.addAttribute("artist", artistStr);
				model.addAttribute("venue", venueStr);
				model.addAttribute("grandChild", grandChildStr);
				model.addAttribute("selectedValue", selectedValue);
				model.addAttribute("selectedOption", selectedOption);
				model.addAttribute("exchangeEventList",exchangeEventList);
			}
			return "page-common-product-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}	
	
	@RequestMapping(value="/ManageZoneTicketsProcessorExchangeEvent")
	public String manageZoneTicketsProcessorExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			//	ModelAndView mav = new ModelAndView("page-mini-exchange-event");
				Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
				model.addAttribute("brokers", brokers);
				Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
				for(Broker broker:brokers){
					brokerMap.put(broker.getId(), broker);
				}
				AutopricingProduct product = DAORegistry.getAutopricingProductDAO().get(9);
				model.addAttribute("product", product);
//				ModelAndView mav = new ModelAndView("page-mini-exchange-event");
				String action = request.getParameter("action");
				Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(9);
				model.addAttribute("url", "ManageZoneTicketsProcessorExchangeEvent");
				model.addAttribute("auditUrl", "ZoneTicketsProcessorExchangeEventAuditPopup");
				model.addAttribute("products", autopricingProducts);
				if(action==""){
					model.addAttribute("message", "Please select valid artist ");
				}
				if(action!=null && action!=""){
					Collection<Event> events = null;
					Collection<ZoneTicketsProcessorExchangeEvent> dbZoneTicketsProcessorExchangeEventList= null;
					String artistStr = request.getParameter("artist");
					String[] eventStr = request.getParameterValues("events");
					String temp[] = eventStr;
					String venueStr = request.getParameter("venue");
					String grandChildStr = request.getParameter("grandChild");
//					String childStr = request.getParameter("child");
					String selectedEvent = "";
					String eventsCheckAll = request.getParameter("eventsCheckAll");
					boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
					String selectedValue = "";
					String selectedOption = "";
					Integer selectedId = null;
					String ztUpdateEventIdsStr = "";
					
					if(artistStr!=null && !artistStr.isEmpty()){
						Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
						events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
						dbZoneTicketsProcessorExchangeEventList  = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByArtistId(Integer.valueOf(artistStr));
						selectedOption="Artist";
						selectedValue= artist.getName();
						selectedId = artist.getId();
					}else if(venueStr!=null && !venueStr.isEmpty()){
						Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
						dbZoneTicketsProcessorExchangeEventList  = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByVenueId(Integer.valueOf(venueStr));
						selectedOption="Venue";
						selectedValue= venue.getBuilding();
						events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
						selectedId = venue.getId();
					}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
						GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
						dbZoneTicketsProcessorExchangeEventList  = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
						selectedOption="GrandChildCategory";
						selectedValue= grandChildCategory.getName();
						events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
						selectedId = grandChildCategory.getId();
					}
					
					String brokerIdStr = request.getParameter("brokerId");
					/*Integer brokerId = null;
					
					if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
						brokerId = Integer.parseInt(brokerIdStr);
					}*/
					model.addAttribute("broker", brokerIdStr);
				   
					Map<Integer,ZoneTicketsProcessorExchangeEvent> dbZoneTicketsProcessorExchangeEventMap = new HashMap<Integer, ZoneTicketsProcessorExchangeEvent>();
					Map<Integer,ZoneTicketsProcessorExchangeEvent> dbZoneTicketsProcessorExchangeEventUpdateMap = new HashMap<Integer, ZoneTicketsProcessorExchangeEvent>();
					List<ZoneTicketsProcessorExchangeEvent> exchangeEventList = new ArrayList<ZoneTicketsProcessorExchangeEvent>();
					List<ZoneTicketsProcessorExchangeEvent> exchangeEventUpdateList = new ArrayList<ZoneTicketsProcessorExchangeEvent>();
					for(ZoneTicketsProcessorExchangeEvent exchangeEvent:dbZoneTicketsProcessorExchangeEventList){
						dbZoneTicketsProcessorExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
						dbZoneTicketsProcessorExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					}
					
					List<ZoneTicketsProcessorExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<ZoneTicketsProcessorExchangeEventAudit>();
					List<Integer> productIdList = new ArrayList<Integer>();
					List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
					if(action.equalsIgnoreCase("update")){
						List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
						Map<String, String[]> requestParams = request.getParameterMap();
						Date now = new Date();
						for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
							String key = entry.getKey();
							if(key.contains("product")){
								productIdList.add(Integer.parseInt(key.replace("product-","")));
							}
							if(key.contains("checkbox_")){
								Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
								temp = StringUtil.removeFromArray(temp,eventId.toString());
								ZoneTicketsProcessorExchangeEvent dbZoneTicketsProcessorExchangeEvent = dbZoneTicketsProcessorExchangeEventUpdateMap.remove(eventId);
								toBeUpdaetEventIdList.add(eventId);
								if(dbZoneTicketsProcessorExchangeEvent!=null){
									ZoneTicketsProcessorExchangeEventAudit audit = new ZoneTicketsProcessorExchangeEventAudit(dbZoneTicketsProcessorExchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
									boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
									
									dbZoneTicketsProcessorExchangeEvent.setAllowSectionRange(isAllowSectionRange);
									dbZoneTicketsProcessorExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
									//dbZoneTicketsProcessorExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setTaxPercentage(Double.parseDouble(request.getParameter("taxPercentage_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setExcludeHoursBeforeEvent(Integer.parseInt(request.getParameter("excludeHoursBeforeEvent_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setStatus("ACTIVE");
									
									if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setShippingMethod(null);
									}
									
									String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
									boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
									dbZoneTicketsProcessorExchangeEvent.setDiscountZone(isDiscountZone);
									
									if(request.getParameter("zoneDiscountPerc_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setZoneDiscountPerc(Double.parseDouble(request.getParameter("zoneDiscountPerc_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setZoneDiscountPerc(null);
									}
									
									if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
										//dbZoneTicketsProcessorExchangeEvent.setTicketNetworkBroker(brokerMap.get(dbZoneTicketsProcessorExchangeEvent.getTicketNetworkBrokerId()));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setTicketNetworkBrokerId(null);
										//dbZoneTicketsProcessorExchangeEvent.setTicketNetworkBroker(null);
									}
									if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setVividBrokerId(null);
									}
									if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setScoreBigBrokerId(null);
									}
									if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setZoneTicketBrokerId(null);
									}
									if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setFanxchangeBrokerId(null);
									}
									if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setTicketcityBrokerId(null);
									}
									if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setTicketcityBrokerId(null);
									}
									
									dbZoneTicketsProcessorExchangeEvent.setLastUpdatedDate(now);
									dbZoneTicketsProcessorExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
											.getAuthentication().getName());  // Need to change after login functionality
									exchangeEventUpdateList.add(dbZoneTicketsProcessorExchangeEvent);
									exchangeEvents.add(dbZoneTicketsProcessorExchangeEvent);
									
									if(dbZoneTicketsProcessorExchangeEvent.getZoneTicketBrokerId() == null) {
										ztUpdateEventIdsStr = ztUpdateEventIdsStr + "," + eventId;
									}
								}
								
							}
						}
						if(temp!=null){
							for (String eventId : temp) {
								try{
									ZoneTicketsProcessorExchangeEvent exchangeEvent = dbZoneTicketsProcessorExchangeEventUpdateMap.remove(Integer.parseInt(eventId));
									if(exchangeEvent != null) {
										ZoneTicketsProcessorExchangeEventAudit audit = new ZoneTicketsProcessorExchangeEventAudit(exchangeEvent);
										exchangeEventUpdateAuditList.add(audit);
										
										exchangeEvent.setStatus("DELETED");
										exchangeEvent.setLastUpdatedDate(now);
										exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
										exchangeEventUpdateList.add(exchangeEvent);
										
										ztUpdateEventIdsStr = ztUpdateEventIdsStr + "," + eventId;
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
						
						if(!ztUpdateEventIdsStr.isEmpty()) {
							ztUpdateEventIdsStr = ztUpdateEventIdsStr.substring(1);
							//updateRemovedEventsInZoneTickets(ztUpdateEventIdsStr);
							
							//Remove unbroadcasted events category listings from RTW database for both Zoneticketsprocessor and RTFLAstrow products
							String internalNotesStr =BrokerUtils.getRtfAutopricingProductInternalNotesStrForSqlQuery();
							String[] idsArr =  ztUpdateEventIdsStr.split(",");
							for (String eventIdStr : idsArr) {
								ZoneTicketDAORegistry.getZoneTicketsTicketGroupDAO().deleteAutoPricingCategoryTicketGroupsOnlyByEventId(Integer.parseInt(eventIdStr), internalNotesStr);	
							}
							
						}
						
						DAORegistry.getZoneTicketsProcessorExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
						DAORegistry.getZoneTicketsProcessorExchangeEventDAO().updateAll(exchangeEventUpdateList);
						if(!productIdList.isEmpty()){
							for(Integer id : productIdList){
								updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
							}
							
						}
						
					}
					if(eventStr!=null){
						for (String eventId : eventStr) {
							try{
								selectedEvent += eventId + ",";
								ZoneTicketsProcessorExchangeEvent tempLarryExchangeEvent = dbZoneTicketsProcessorExchangeEventMap.get(Integer.parseInt(eventId));
								 if(tempLarryExchangeEvent!=null){
									 exchangeEventList.add(tempLarryExchangeEvent);
								 }
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					
					model.addAttribute("eventsCheckAll", isAllEvent);
					model.addAttribute("events", events);
					model.addAttribute("eventStr", selectedEvent);
					model.addAttribute("artist", artistStr);
					model.addAttribute("venue", venueStr);
					model.addAttribute("grandChild", grandChildStr);
					model.addAttribute("selectedValue", selectedValue);
					model.addAttribute("selectedOption", selectedOption);
					model.addAttribute("exchangeEventList",exchangeEventList);
				}
				return "page-common-product-event-management";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
	}
	
	
	@RequestMapping(value="/ManagePresaleZoneTicketsExchangeEvent")
	public String managePresaleZoneTicketsExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
				Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
				model.addAttribute("brokers", brokers);
				Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
				for(Broker broker:brokers){
					brokerMap.put(broker.getId(), broker);
				}
//				ModelAndView mav = new ModelAndView("page-mini-exchange-event");
				String action = request.getParameter("action");
				Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(17);
				model.addAttribute("url", "ManagePresaleZoneTicketsExchangeEvent");
				model.addAttribute("auditUrl", "PresaleZoneTicketsExchangeEventAuditPopup");
				model.addAttribute("products", autopricingProducts);
				if(action==""){
					model.addAttribute("message", "Please select valid artist ");
				}
				if(action!=null && action!=""){
					Collection<Event> events = null;
					Collection<PresaleZoneTicketsExchangeEvent> dbZoneTicketsProcessorExchangeEventList= null;
					String artistStr = request.getParameter("artist");
					String[] eventStr = request.getParameterValues("events");
					String temp[] = eventStr;
					String venueStr = request.getParameter("venue");
					String grandChildStr = request.getParameter("grandChild");
//					String childStr = request.getParameter("child");
					String selectedEvent = "";
					String eventsCheckAll = request.getParameter("eventsCheckAll");
					boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
					String selectedValue = "";
					String selectedOption = "";
					Integer selectedId = null;
					String ztUpdateEventIdsStr = "";
					
					if(artistStr!=null && !artistStr.isEmpty()){
						Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
						events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
						dbZoneTicketsProcessorExchangeEventList  = DAORegistry.getPresaleZoneTicketsExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByArtistId(Integer.valueOf(artistStr));
						selectedOption="Artist";
						selectedValue= artist.getName();
						selectedId = artist.getId();
					}else if(venueStr!=null && !venueStr.isEmpty()){
						Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
						dbZoneTicketsProcessorExchangeEventList  = DAORegistry.getPresaleZoneTicketsExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByVenueId(Integer.valueOf(venueStr));
						selectedOption="Venue";
						selectedValue= venue.getBuilding();
						events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
						selectedId = venue.getId();
					}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
						GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
						dbZoneTicketsProcessorExchangeEventList  = DAORegistry.getPresaleZoneTicketsExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
						selectedOption="GrandChildCategory";
						selectedValue= grandChildCategory.getName();
						events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
						selectedId = grandChildCategory.getId();
					}
					
					String brokerIdStr = request.getParameter("brokerId");
					/*Integer brokerId = null;
					
					if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
						brokerId = Integer.parseInt(brokerIdStr);
					}*/
					model.addAttribute("broker", brokerIdStr);
				   
					Map<Integer,PresaleZoneTicketsExchangeEvent> dbZoneTicketsProcessorExchangeEventMap = new HashMap<Integer, PresaleZoneTicketsExchangeEvent>();
					Map<Integer,PresaleZoneTicketsExchangeEvent> dbZoneTicketsProcessorExchangeEventUpdateMap = new HashMap<Integer, PresaleZoneTicketsExchangeEvent>();
					List<PresaleZoneTicketsExchangeEvent> exchangeEventList = new ArrayList<PresaleZoneTicketsExchangeEvent>();
					List<PresaleZoneTicketsExchangeEvent> exchangeEventUpdateList = new ArrayList<PresaleZoneTicketsExchangeEvent>();
					for(PresaleZoneTicketsExchangeEvent exchangeEvent:dbZoneTicketsProcessorExchangeEventList){
						dbZoneTicketsProcessorExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
						dbZoneTicketsProcessorExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					}
					
					List<PresaleZoneTicketsExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<PresaleZoneTicketsExchangeEventAudit>();
					List<Integer> productIdList = new ArrayList<Integer>();
					List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
					if(action.equalsIgnoreCase("update")){
						List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
						Map<String, String[]> requestParams = request.getParameterMap();
						Date now = new Date();
						for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
							String key = entry.getKey();
							if(key.contains("product")){
								productIdList.add(Integer.parseInt(key.replace("product-","")));
							}
							if(key.contains("checkbox_")){
								Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
								temp = StringUtil.removeFromArray(temp,eventId.toString());
								PresaleZoneTicketsExchangeEvent dbZoneTicketsProcessorExchangeEvent = dbZoneTicketsProcessorExchangeEventUpdateMap.remove(eventId);
								toBeUpdaetEventIdList.add(eventId);
								if(dbZoneTicketsProcessorExchangeEvent!=null){
									PresaleZoneTicketsExchangeEventAudit audit = new PresaleZoneTicketsExchangeEventAudit(dbZoneTicketsProcessorExchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
									boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
									
									dbZoneTicketsProcessorExchangeEvent.setAllowSectionRange(isAllowSectionRange);
									dbZoneTicketsProcessorExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
									//dbZoneTicketsProcessorExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
									dbZoneTicketsProcessorExchangeEvent.setStatus("ACTIVE");
									
									if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setShippingMethod(null);
									}
									
									String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
									boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
									dbZoneTicketsProcessorExchangeEvent.setDiscountZone(isDiscountZone);
									
									if(request.getParameter("zoneDiscountPerc_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setZoneDiscountPerc(Double.parseDouble(request.getParameter("zoneDiscountPerc_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setZoneDiscountPerc(null);
									}
									
									if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
										//dbZoneTicketsProcessorExchangeEvent.setTicketNetworkBroker(brokerMap.get(dbZoneTicketsProcessorExchangeEvent.getTicketNetworkBrokerId()));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setTicketNetworkBrokerId(null);
										//dbZoneTicketsProcessorExchangeEvent.setTicketNetworkBroker(null);
									}
									if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setVividBrokerId(null);
									}
									if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setScoreBigBrokerId(null);
									}
									if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setZoneTicketBrokerId(null);
									}
									if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setFanxchangeBrokerId(null);
									}
									if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setTicketcityBrokerId(null);
									}
									if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
										dbZoneTicketsProcessorExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
									}else{
										dbZoneTicketsProcessorExchangeEvent.setTicketcityBrokerId(null);
									}
									
									dbZoneTicketsProcessorExchangeEvent.setLastUpdatedDate(now);
									dbZoneTicketsProcessorExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
											.getAuthentication().getName());  // Need to change after login functionality
									exchangeEventUpdateList.add(dbZoneTicketsProcessorExchangeEvent);
									exchangeEvents.add(dbZoneTicketsProcessorExchangeEvent);
									
									if(dbZoneTicketsProcessorExchangeEvent.getZoneTicketBrokerId() == null) {
										ztUpdateEventIdsStr = ztUpdateEventIdsStr + "," + eventId;
									}
								}
								
							}
						}
						if(temp!=null){
							for (String eventId : temp) {
								try{
									PresaleZoneTicketsExchangeEvent exchangeEvent = dbZoneTicketsProcessorExchangeEventUpdateMap.remove(Integer.parseInt(eventId));
									if(exchangeEvent != null) {
										PresaleZoneTicketsExchangeEventAudit audit = new PresaleZoneTicketsExchangeEventAudit(exchangeEvent);
										exchangeEventUpdateAuditList.add(audit);
										
										exchangeEvent.setStatus("DELETED");
										exchangeEvent.setLastUpdatedDate(now);
										exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
										exchangeEventUpdateList.add(exchangeEvent);
										
										ztUpdateEventIdsStr = ztUpdateEventIdsStr + "," + eventId;
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
						
						/*if(!ztUpdateEventIdsStr.isEmpty()) {
							ztUpdateEventIdsStr = ztUpdateEventIdsStr.substring(1);
							updateRemovedEventsInZoneTickets(ztUpdateEventIdsStr);
						}*/
						
						DAORegistry.getPresaleZoneTicketsExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
						DAORegistry.getPresaleZoneTicketsExchangeEventDAO().updateAll(exchangeEventUpdateList);
						if(!productIdList.isEmpty()){
							for(Integer id : productIdList){
								updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
							}
							
						}
						
					}
					if(eventStr!=null){
						for (String eventId : eventStr) {
							try{
								selectedEvent += eventId + ",";
								PresaleZoneTicketsExchangeEvent tempLarryExchangeEvent = dbZoneTicketsProcessorExchangeEventMap.get(Integer.parseInt(eventId));
								 if(tempLarryExchangeEvent!=null){
									 exchangeEventList.add(tempLarryExchangeEvent);
								 }
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					
					model.addAttribute("eventsCheckAll", isAllEvent);
					model.addAttribute("events", events);
					model.addAttribute("eventStr", selectedEvent);
					model.addAttribute("artist", artistStr);
					model.addAttribute("venue", venueStr);
					model.addAttribute("grandChild", grandChildStr);
					model.addAttribute("selectedValue", selectedValue);
					model.addAttribute("selectedOption", selectedOption);
					model.addAttribute("exchangeEventList",exchangeEventList);
				}
				return "page-common-product-event-management";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
	}
	

	@RequestMapping(value="/ManageAutoCats96ExchangeEvent")
	public String loadManageAutoCats96ExchangeEventPage(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
	//	ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		model.addAttribute("brokers", brokers);
		Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
		for(Broker broker:brokers){
			brokerMap.put(broker.getId(), broker);
		}
//		ModelAndView mav = new ModelAndView("page-mini-exchange-event");
		String action = request.getParameter("action");
		Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(3);
		model.addAttribute("url", "ManageAutoCats96ExchangeEvent");
		model.addAttribute("auditUrl", "GetAutoCats96ExchangeEventAuditPopup");
		model.addAttribute("products", autopricingProducts);
		if(action==""){
			model.addAttribute("message", "Please select valid artist ");
		}
		if(action!=null && action!=""){
			Collection<Event> events = null;
			Collection<AutoCats96ExchangeEvent> dbMiniExchangeEventList= null;
			String artistStr = request.getParameter("artist");
			String[] eventStr = request.getParameterValues("events");
			String temp[] = eventStr;
			String venueStr = request.getParameter("venue");
			String grandChildStr = request.getParameter("grandChild");
//			String childStr = request.getParameter("child");
			String selectedEvent = "";
			String eventsCheckAll = request.getParameter("eventsCheckAll");
			boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			Integer selectedId = null;
			if(artistStr!=null && !artistStr.isEmpty()){
				Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
				dbMiniExchangeEventList  = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByArtistId(Integer.valueOf(artistStr));
				selectedOption="Artist";
				selectedValue= artist.getName();
				selectedId = artist.getId();
			}else if(venueStr!=null && !venueStr.isEmpty()){
				Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
				dbMiniExchangeEventList  = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByVenueId(Integer.valueOf(venueStr));
				selectedOption="Venue";
				selectedValue= venue.getBuilding();
				events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
				selectedId = venue.getId();
			}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
				GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
				dbMiniExchangeEventList  = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
				selectedOption="GrandChildCategory";
				selectedValue= grandChildCategory.getName();
				events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
				selectedId = grandChildCategory.getId();
			}
			
			String brokerIdStr = request.getParameter("brokerId");

			model.addAttribute("broker", brokerIdStr);
		   
			Map<Integer,AutoCats96ExchangeEvent> dbMiniExchangeEventMap = new HashMap<Integer, AutoCats96ExchangeEvent>();
			Map<Integer,AutoCats96ExchangeEvent> dbMiniExchangeEventUpdateMap = new HashMap<Integer, AutoCats96ExchangeEvent>();
			List<AutoCats96ExchangeEvent> exchangeEventList = new ArrayList<AutoCats96ExchangeEvent>();
			List<AutoCats96ExchangeEvent> exchangeEventUpdateList = new ArrayList<AutoCats96ExchangeEvent>();
			for(AutoCats96ExchangeEvent exchangeEvent:dbMiniExchangeEventList){
				dbMiniExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
				dbMiniExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
			}
			
			List<AutoCats96ExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<AutoCats96ExchangeEventAudit>();
			List<Integer> productIdList = new ArrayList<Integer>();
			List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
			if(action.equalsIgnoreCase("update")){
				List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
				Map<String, String[]> requestParams = request.getParameterMap();
				Date now = new Date();
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					if(key.contains("product")){
						productIdList.add(Integer.parseInt(key.replace("product-","")));
					}
					if(key.contains("checkbox_")){
						Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
						temp = StringUtil.removeFromArray(temp,eventId.toString());
						AutoCats96ExchangeEvent dbMiniExchangeEvent = dbMiniExchangeEventUpdateMap.remove(eventId);
						toBeUpdaetEventIdList.add(eventId);
						if(dbMiniExchangeEvent!=null){
							AutoCats96ExchangeEventAudit audit = new AutoCats96ExchangeEventAudit(dbMiniExchangeEvent);
							exchangeEventUpdateAuditList.add(audit);
							String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
							boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
							
							dbMiniExchangeEvent.setAllowSectionRange(isAllowSectionRange);
							dbMiniExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
							//dbMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
							dbMiniExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
							dbMiniExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
							dbMiniExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
							dbMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
							dbMiniExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
							dbMiniExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
							dbMiniExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
							dbMiniExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
							dbMiniExchangeEvent.setStatus("ACTIVE");
							
							if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
								dbMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
							}else{
								dbMiniExchangeEvent.setShippingMethod(null);
							}
							
							String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
							boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setDiscountZone(isDiscountZone);
							
							String vividSeatEnabledStr = request.getParameter("vividEnabled_"+eventId);
							boolean  vividSeatEnabled = ((vividSeatEnabledStr==null || !vividSeatEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setVividEnabled(vividSeatEnabled);
							
							String rotEnabledStr = request.getParameter("rotEnabled_"+eventId);
							boolean  rotEnabled = ((rotEnabledStr==null || !rotEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setRotEnabled(rotEnabled);
							
							String tixcityEnabledStr = request.getParameter("tixcityEnabled_"+eventId);
							boolean  tixcityEnabled = ((tixcityEnabledStr==null || !tixcityEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setTixcityEnabled(tixcityEnabled);
							
							String rtwEnabledStr = request.getParameter("rtwEnabled_"+eventId);
							boolean  rtwEnabled = ((rtwEnabledStr==null || !rtwEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setRtwEnabled(rtwEnabled);
							
							String minicatsEnabledStr = request.getParameter("minicatsEnabled_"+eventId);
							boolean  minicatsEnabled = ((minicatsEnabledStr==null || !minicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setMinicatsEnabled(minicatsEnabled);
							
							String lastrowMinicatsEnabledStr = request.getParameter("lastrowMinicatsEnabled_"+eventId);
							boolean  lastrowMinicatsEnabled = ((lastrowMinicatsEnabledStr==null || !lastrowMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setLastrowMinicatsEnabled(lastrowMinicatsEnabled);
							
							String vipLastrowMinicatsEnabledStr = request.getParameter("vipLastrowMinicatsEnabled_"+eventId);
							boolean  vipLastrowMinicatsEnabled = ((vipLastrowMinicatsEnabledStr==null || !vipLastrowMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setVipLastrowMinicatsEnabled(vipLastrowMinicatsEnabled);
							
							String vipMinicatsEnabledStr = request.getParameter("vipMinicatsEnabled_"+eventId);
							boolean  vipMinicatsEnabled = ((vipMinicatsEnabledStr==null || !vipMinicatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setVipMinicatsEnabled(vipMinicatsEnabled);
							
							String presaleAutocatsEnabledStr = request.getParameter("presaleAutocatsEnabled_"+eventId);
							boolean  presaleAutocatsEnabled = ((presaleAutocatsEnabledStr==null || !presaleAutocatsEnabledStr.equalsIgnoreCase("on") )?false:true);
							dbMiniExchangeEvent.setPresaleAutocatsEnabled(presaleAutocatsEnabled);
							
							
							
							if(null != request.getParameter("ticketnetwork_broker_"+eventId) && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
								//dbMiniExchangeEvent.setTicketNetworkBroker(brokerMap.get(dbMiniExchangeEvent.getTicketNetworkBrokerId()));
							}else{
								dbMiniExchangeEvent.setTicketNetworkBrokerId(null);
								//dbMiniExchangeEvent.setTicketNetworkBroker(null);
							}
							if(null != request.getParameter("vividseat_broker_"+eventId) && request.getParameter("vividseat_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setVividBrokerId(null);
							}
							if(null != request.getParameter("scorebig_broker_"+eventId) && request.getParameter("scorebig_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setScoreBigBrokerId(null);
							}
							if(null != request.getParameter("fanxchange_broker_"+eventId)  && request.getParameter("fanxchange_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setFanxchangeBrokerId(null);
							}
							if(null != request.getParameter("ticketcity_broker_"+eventId)  && request.getParameter("ticketcity_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setTicketcityBrokerId(null);
							}
							if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
								dbMiniExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
							}else{
								dbMiniExchangeEvent.setTicketcityBrokerId(null);
							}
							
							dbMiniExchangeEvent.setLastUpdatedDate(now);
							dbMiniExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
									.getAuthentication().getName());  // Need to change after login functionality
							dbMiniExchangeEvent.setPreSaleSettingsUpdated(Boolean.TRUE);
							exchangeEventUpdateList.add(dbMiniExchangeEvent);
							exchangeEvents.add(dbMiniExchangeEvent);
						}
						
					}
				}
				if(temp!=null){
					for (String eventId : temp) {
						try{
							AutoCats96ExchangeEvent exchangeEvent = dbMiniExchangeEventUpdateMap.remove(Integer.parseInt(eventId));
							if(exchangeEvent != null) {
								AutoCats96ExchangeEventAudit audit = new AutoCats96ExchangeEventAudit(exchangeEvent);
								exchangeEventUpdateAuditList.add(audit);
								
								exchangeEvent.setStatus("DELETED");
								exchangeEvent.setLastUpdatedDate(now);
								exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
								exchangeEvent.setPreSaleSettingsUpdated(Boolean.TRUE);
								exchangeEventUpdateList.add(exchangeEvent);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				DAORegistry.getAutoCats96ExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
				DAORegistry.getAutoCats96ExchangeEventDAO().updateAll(exchangeEventUpdateList);
				/*if(!productIdList.isEmpty()){
					for(Integer id : productIdList){
						updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
					}
					
				}*/
			}
			if(eventStr!=null){
				for (String eventId : eventStr) {
					try{
						selectedEvent += eventId + ",";
						AutoCats96ExchangeEvent tempLarryExchangeEvent = dbMiniExchangeEventMap.get(Integer.parseInt(eventId));
						 if(tempLarryExchangeEvent!=null){
							 exchangeEventList.add(tempLarryExchangeEvent);
						 }
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			model.addAttribute("eventsCheckAll", isAllEvent);
			model.addAttribute("events", events);
			model.addAttribute("eventStr", selectedEvent);
			model.addAttribute("artist", artistStr);
			model.addAttribute("venue", venueStr);
			model.addAttribute("grandChild", grandChildStr);
			model.addAttribute("selectedValue", selectedValue);
			model.addAttribute("selectedOption", selectedOption);
			model.addAttribute("exchangeEventList",exchangeEventList);
		}
		return "page-autocats96-event-management";
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
}
	
	@RequestMapping(value="/GetAutoCats96ExchangeEventAuditPopup")
	public String getAutoCats96ExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<AutoCats96ExchangeEventAudit> larryLastExchangeEventAudits = DAORegistry.getAutoCats96ExchangeEventAuditDAO().getAutoCats96ExchangeEventAuditByEventId(eventId);
			model.addAttribute("exchangeEventList", larryLastExchangeEventAudits);
		}
		return "page-autocats96-event-audit";
	}
	
	@RequestMapping(value="/GetPresaleAutocatExchangeEventAuditPopup")
	public String getPresaleAutocatExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<PresaleAutoCatExchangeEventAudit> larryLastExchangeEventAudits = DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().getAllPresaleAutoCatExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", larryLastExchangeEventAudits);
		}
		return "page-presale-autocats-event-audit";
	}
	
	public static void updateRemovedEventsInZoneTickets(String eventIdsStr) throws Exception {
		
		String url = zoneWSUrl+"RemoveZoneEvent.json?configId="+zoneWSConfigId+"&eventIds="+eventIdsStr;
		RestTemplate restTemplate = new RestTemplate();
		String jsonData = restTemplate.getForObject(url, String.class);
		JSONObject jsonObject = new JSONObject(jsonData);
		jsonData = jsonObject.getString("removeZoneEvent");
		ObjectMapper mapper = new ObjectMapper();
		RemoveZoneEvent removeZoneEvent = mapper.readValue(jsonData, RemoveZoneEvent.class);
		
	}
	
	// Mehul : added for vip last row
		@RequestMapping(value="/ManageVipLastRowMiniExchangeEvent")
		public String manageVipLastRowMiniExchangeEvent(HttpServletRequest request,Model model,HttpServletResponse response){
			try {
				Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
				model.addAttribute("brokers", brokers);
				Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
				for(Broker broker:brokers){
					brokerMap.put(broker.getId(), broker);
				}
//				ModelAndView mav = new ModelAndView("page-mini-exchange-event");
				String action = request.getParameter("action");
				Collection<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllAutopricingProductExceptGivenIdForExEventManagement(16);
				model.addAttribute("url", "ManageVipLastRowMiniExchangeEvent");
				model.addAttribute("auditUrl", "VipLastRowMiniExchangeEventAuditPopUp");
				model.addAttribute("products", autopricingProducts);
				if(action==""){
					model.addAttribute("message", "Please select valid artist ");
				}
				if(action!=null && action!=""){
					Collection<Event> events = null;
					Collection<VipLastRowMiniExchangeEvent> dbVipLastRowMiniExchangeEventList= null;
					String artistStr = request.getParameter("artist");
					String[] eventStr = request.getParameterValues("events");
					String temp[] = eventStr;
					String venueStr = request.getParameter("venue");
					String grandChildStr = request.getParameter("grandChild");
//					String childStr = request.getParameter("child");
					String selectedEvent = "";
					String eventsCheckAll = request.getParameter("eventsCheckAll");
					boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
					String selectedValue = "";
					String selectedOption = "";
					Integer selectedId = null;
					if(artistStr!=null && !artistStr.isEmpty()){
						Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
						events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
						dbVipLastRowMiniExchangeEventList  = DAORegistry.getVipLastRowMiniExchangeEventDAO().getAllVipLastRowMiniExchangeEventsByArtistId(Integer.valueOf(artistStr));
						selectedOption="Artist";
						selectedValue= artist.getName();
						selectedId = artist.getId();
					}else if(venueStr!=null && !venueStr.isEmpty()){
						Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
						dbVipLastRowMiniExchangeEventList  = DAORegistry.getVipLastRowMiniExchangeEventDAO().getAllVipLastRowMiniExchangeEventsByVenueId(Integer.valueOf(venueStr));
						selectedOption="Venue";
						selectedValue= venue.getBuilding();
						events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
						selectedId = venue.getId();
					}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
						GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
						dbVipLastRowMiniExchangeEventList  = DAORegistry.getVipLastRowMiniExchangeEventDAO().getAllVipLastRowMiniExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
						selectedOption="GrandChildCategory";
						selectedValue= grandChildCategory.getName();
						events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
						selectedId = grandChildCategory.getId();
					}
					
					String brokerIdStr = request.getParameter("brokerId");
					/*Integer brokerId = null;
					
					if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
						brokerId = Integer.parseInt(brokerIdStr);
					}*/
					model.addAttribute("broker", brokerIdStr);
				   
					Map<Integer,VipLastRowMiniExchangeEvent> dbVipLastRowMiniExchangeEventMap = new HashMap<Integer, VipLastRowMiniExchangeEvent>();
					Map<Integer,VipLastRowMiniExchangeEvent> dbVipLastRowMiniExchangeEventUpdateMap = new HashMap<Integer, VipLastRowMiniExchangeEvent>();
					List<VipLastRowMiniExchangeEvent> exchangeEventList = new ArrayList<VipLastRowMiniExchangeEvent>();
					List<VipLastRowMiniExchangeEvent> exchangeEventUpdateList = new ArrayList<VipLastRowMiniExchangeEvent>();
					for(VipLastRowMiniExchangeEvent exchangeEvent:dbVipLastRowMiniExchangeEventList){
						dbVipLastRowMiniExchangeEventMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
						dbVipLastRowMiniExchangeEventUpdateMap.put(exchangeEvent.getEvent().getId(), exchangeEvent);
					}
					
					List<VipLastRowMiniExchangeEventAudit> exchangeEventUpdateAuditList = new ArrayList<VipLastRowMiniExchangeEventAudit>();
					List<Integer> productIdList = new ArrayList<Integer>();
					List<Integer> toBeUpdaetEventIdList = new ArrayList<Integer>();
					if(action.equalsIgnoreCase("update")){
						List<ExchangeEvent> exchangeEvents = new ArrayList<ExchangeEvent>();
						Map<String, String[]> requestParams = request.getParameterMap();
						Date now = new Date();
						for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
							String key = entry.getKey();
							if(key.contains("product")){
								productIdList.add(Integer.parseInt(key.replace("product-","")));
							}
							if(key.contains("checkbox_")){
								Integer eventId = Integer.parseInt(key.replace("checkbox_", ""));
								temp = StringUtil.removeFromArray(temp,eventId.toString());
								VipLastRowMiniExchangeEvent dbVipLastRowMiniExchangeEvent = dbVipLastRowMiniExchangeEventUpdateMap.remove(eventId);
								toBeUpdaetEventIdList.add(eventId);
								if(dbVipLastRowMiniExchangeEvent!=null){
									VipLastRowMiniExchangeEventAudit audit = new VipLastRowMiniExchangeEventAudit(dbVipLastRowMiniExchangeEvent);
									exchangeEventUpdateAuditList.add(audit);
									String isAllowSectionRangeStr = request.getParameter("allowSectionRange_"+eventId);
									boolean  isAllowSectionRange = ((isAllowSectionRangeStr==null || !isAllowSectionRangeStr.equalsIgnoreCase("on") )?false:true);
									
									dbVipLastRowMiniExchangeEvent.setAllowSectionRange(isAllowSectionRange);
									dbVipLastRowMiniExchangeEvent.setExposure(request.getParameter("exposure_"+eventId));
//									dbVipLastRowMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setNearTermDisplayOption(Integer.parseInt(request.getParameter("near_term_display_option_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setShippingDays(Integer.parseInt(request.getParameter("shipping_days_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setRptFactor(Double.parseDouble(request.getParameter("rptFactor_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
//									dbVipLastRowMiniExchangeEvent.setPriceBreakup(Double.parseDouble(request.getParameter("priceBreakup_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setLowerMarkup(Double.parseDouble(request.getParameter("lowerMarkup_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setUpperMarkup(Double.parseDouble(request.getParameter("upperMarkup_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setLowerShippingFees(Double.parseDouble(request.getParameter("lowerShippingFees_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setUpperShippingFees(Double.parseDouble(request.getParameter("upperShippingFees_"+eventId)));
									dbVipLastRowMiniExchangeEvent.setStatus("ACTIVE");
									
									if(request.getParameter("shipping_method_"+eventId) != null && request.getParameter("shipping_method_"+eventId)!=""){
										dbVipLastRowMiniExchangeEvent.setShippingMethod(Integer.parseInt(request.getParameter("shipping_method_"+eventId)));
									}else{
										dbVipLastRowMiniExchangeEvent.setShippingMethod(null);
									}
									
									String isDiscountZoneStr = request.getParameter("discount_zone_"+eventId);
									boolean  isDiscountZone = ((isDiscountZoneStr==null || !isDiscountZoneStr.equalsIgnoreCase("on") )?false:true);
									dbVipLastRowMiniExchangeEvent.setDiscountZone(isDiscountZone);
									
									if(request.getParameter("ticketnetwork_broker_"+eventId) != null && request.getParameter("ticketnetwork_broker_"+eventId)!=""){
										dbVipLastRowMiniExchangeEvent.setTicketNetworkBrokerId(Integer.valueOf(request.getParameter("ticketnetwork_broker_"+eventId)));
									}else{
										dbVipLastRowMiniExchangeEvent.setTicketNetworkBrokerId(null);
									}
									if(request.getParameter("vividseat_broker_"+eventId) != null && request.getParameter("vividseat_broker_"+eventId)!=""){
										dbVipLastRowMiniExchangeEvent.setVividBrokerId(Integer.valueOf(request.getParameter("vividseat_broker_"+eventId)));
									}else{
										dbVipLastRowMiniExchangeEvent.setVividBrokerId(null);
									}
									if(request.getParameter("scorebig_broker_"+eventId) != null && request.getParameter("scorebig_broker_"+eventId)!=""){
										dbVipLastRowMiniExchangeEvent.setScoreBigBrokerId(Integer.valueOf(request.getParameter("scorebig_broker_"+eventId)));
									}else{
										dbVipLastRowMiniExchangeEvent.setScoreBigBrokerId(null);
									}
									if(request.getParameter("fanxchange_broker_"+eventId) != null && request.getParameter("fanxchange_broker_"+eventId)!=""){
										dbVipLastRowMiniExchangeEvent.setFanxchangeBrokerId(Integer.valueOf(request.getParameter("fanxchange_broker_"+eventId)));
									}else{
										dbVipLastRowMiniExchangeEvent.setFanxchangeBrokerId(null);
									}
									if(request.getParameter("ticketcity_broker_"+eventId) != null && request.getParameter("ticketcity_broker_"+eventId)!=""){
										dbVipLastRowMiniExchangeEvent.setTicketcityBrokerId(Integer.valueOf(request.getParameter("ticketcity_broker_"+eventId)));
									}else{
										dbVipLastRowMiniExchangeEvent.setTicketcityBrokerId(null);
									}
									if(request.getParameter("seatgeek_broker_"+eventId) != null && request.getParameter("seatgeek_broker_"+eventId)!=""){
										dbVipLastRowMiniExchangeEvent.setSectionTicketCount(Integer.valueOf(request.getParameter("seatgeek_broker_"+eventId)));
									}else{
										dbVipLastRowMiniExchangeEvent.setTicketcityBrokerId(null);
									}
									if(request.getParameter("zoneTicket_broker_"+eventId) != null && request.getParameter("zoneTicket_broker_"+eventId)!=""){
										dbVipLastRowMiniExchangeEvent.setZoneTicketBrokerId(Integer.valueOf(request.getParameter("zoneTicket_broker_"+eventId)));
									}else{
										dbVipLastRowMiniExchangeEvent.setZoneTicketBrokerId(null);
									}
									/*String ticketNetworkStr = request.getParameter("ticketnetwork_broker_"+eventId);
									boolean  isTicketNetwork = ((ticketNetworkStr==null || !ticketNetworkStr.equalsIgnoreCase("on") )?false:true);
									dbLarryExchangeEvent.setIsTicketNetwork(isTicketNetwork);
									
									String vividSeatStr = request.getParameter("vividseat_broker_"+eventId);
									boolean  isVividSeat = ((vividSeatStr==null || !vividSeatStr.equalsIgnoreCase("on") )?false:true);
									dbLarryExchangeEvent.setIsVivid(isVividSeat);
									
									String scoreBigStr = request.getParameter("scorebig_broker_"+eventId);
									boolean  isScoreBigStr = ((scoreBigStr==null || !scoreBigStr.equalsIgnoreCase("on") )?false:true);
									dbLarryExchangeEvent.setIsScoreBig(isScoreBigStr);*/
									dbVipLastRowMiniExchangeEvent.setLastUpdatedDate(now);
									dbVipLastRowMiniExchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext()
											.getAuthentication().getName());  // Need to change after login functionality
									exchangeEventUpdateList.add(dbVipLastRowMiniExchangeEvent);
									exchangeEvents.add(dbVipLastRowMiniExchangeEvent);
								}
		
							}
						}
						if(temp!=null){
							for (String eventId : temp) {
								try{
									VipLastRowMiniExchangeEvent exchangeEvent = dbVipLastRowMiniExchangeEventUpdateMap.get(Integer.parseInt(eventId));
									if(exchangeEvent!=null){
										VipLastRowMiniExchangeEventAudit audit = new VipLastRowMiniExchangeEventAudit(exchangeEvent);
										exchangeEventUpdateAuditList.add(audit);
										
										exchangeEvent.setStatus("DELETED");
										exchangeEvent.setLastUpdatedDate(now);
										exchangeEvent.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
										exchangeEventUpdateList.add(exchangeEvent);
										
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
						DAORegistry.getVipLastRowMiniExchangeEventAuditDAO().saveAll(exchangeEventUpdateAuditList);
						DAORegistry.getVipLastRowMiniExchangeEventDAO().updateAll(exchangeEventUpdateList);
						if(!productIdList.isEmpty()){
							for(Integer id : productIdList){
								updateExchangeEventsByProdctIdAndEventIdList(id,toBeUpdaetEventIdList,selectedOption,selectedId,exchangeEvents,temp);
							}
							
						}
						
					}
					if(eventStr!=null){
						for (String eventId : eventStr) {
							try{
								selectedEvent += eventId + ",";
								VipLastRowMiniExchangeEvent tempVipLastRowMiniExchangeEvent = dbVipLastRowMiniExchangeEventMap.get(Integer.parseInt(eventId));
								 if(tempVipLastRowMiniExchangeEvent!=null){
									 exchangeEventList.add(tempVipLastRowMiniExchangeEvent);
								 }
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					
					model.addAttribute("eventsCheckAll", isAllEvent);
					model.addAttribute("events", events);
					model.addAttribute("eventStr", selectedEvent);
					model.addAttribute("artist", artistStr);
					model.addAttribute("venue", venueStr);
					model.addAttribute("grandChild", grandChildStr);
					model.addAttribute("selectedValue", selectedValue);
					model.addAttribute("selectedOption", selectedOption);
					model.addAttribute("exchangeEventList",exchangeEventList);
				}
				return "page-common-product-event-management";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	
	@RequestMapping(value="/VipLastRowMiniExchangeEventAuditPopUp")
	public String getVipLastRowMiniExchangeEventAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response){
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			Collection<VipLastRowMiniExchangeEventAudit> vipLastRowMiniExchangeEventAudits = DAORegistry.getVipLastRowMiniExchangeEventAuditDAO().getAllVipLastRowMiniExchangeEventAuditsByEventId(eventId);
			model.addAttribute("exchangeEventList", vipLastRowMiniExchangeEventAudits);
		}
		return "page-common-product-event-audit";
	}
	
	public static String getZoneWSUrl() {
		return zoneWSUrl;
	}
	public  void setZoneWSUrl(String zoneWSUrl) {
		ExchangeEventController.zoneWSUrl = zoneWSUrl;
	}
	public static String getZoneWSConfigId() {
		return zoneWSConfigId;
	}
	public void setZoneWSConfigId(String zoneWSConfigId) {
		ExchangeEventController.zoneWSConfigId = zoneWSConfigId;
	}
}
