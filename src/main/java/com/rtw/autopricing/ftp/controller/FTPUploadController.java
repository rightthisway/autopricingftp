package com.rtw.autopricing.ftp.controller;

//import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.FtpFileUploadAudit;
import com.rtw.autopricing.ftp.data.FtpLocationSettings;
import com.rtw.autopricing.ftp.data.FtpUserAccount;
import com.rtw.autopricing.ftp.data.FtpUserFileInformation;
import com.rtw.autopricing.ftp.upload.FTPUploader;
import com.rtw.autopricing.ftp.utils.FTPType;
import com.rtw.autopricing.ftp.utils.FTPVendor;


@Controller
@RequestMapping({"/","/GetAutomaticFileUpload","/GetManualFileUpload","/LoadAutomaticFileUploadInfo",
	"/LoadFTPAuditInfo","/LoadAllFTPAuditInfo","/LocationSettings","/AutoPricing"})
public class FTPUploadController {
	
	public File convertToFile(MultipartFile file) throws Exception{    
	    File convFile = new File(file.getOriginalFilename());
	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(file.getBytes());
	    fos.close(); 
	    return convFile;
	}


	@RequestMapping(value="/")
	public String getHome(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			return "page-autopricing";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	@RequestMapping(value="/AutoPricing")
	public String getAutoPricing(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			return "page-autopricing-event-management";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="/GetAutomaticFileUpload")
	public String getAutomaticUFileUpload(HttpServletRequest request,Model model,HttpServletResponse response){
		DateFormat uiDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try {
			
			String action  = request.getParameter("action");
			model.addAttribute("showEditAndDelete", false);
			
			if(null != action && !action.isEmpty() && action.equals("delete")){
				String id  = request.getParameter("id");
				FtpUserAccount ftpUserAccount = DAORegistry.getFtpUserAccountDAO().get(Integer.parseInt(id));
				ftpUserAccount.setStatus("DELETE");
				DAORegistry.getFtpUserAccountDAO().saveOrUpdate(ftpUserAccount);
				model.addAttribute("ftpUserAccount", new FtpUserAccount());
				model.addAttribute("message", ftpUserAccount.getUserName()+" account details deleted Successfully..");
				return "page-automatic-file-upload";
				
			}else if(null != action && !action.isEmpty() && action.equals("update")){
				String id  = request.getParameter("id");
				String host  = request.getParameter("ftpHost");
				String ftpType  = request.getParameter("ftpType");
				String ftpPort  = request.getParameter("ftpPort");
				String userName  = request.getParameter("ftpUserName");
				String password  = request.getParameter("ftpPassword");
				String uploadDateStr  = request.getParameter("uploadDate");
				String uploadDateHrs  = request.getParameter("uploadDateHrs");
				String uploadDateMins  = request.getParameter("uploadDateMins");
				String frequency  = request.getParameter("uploadFrequency");
				String changeFileOptions  = request.getParameter("changeFileOption");
				String filePath  = request.getParameter("changeFilePath");
				String addFilesAction = request.getParameter("addFilesAction");
				String subDirectoryHost  = request.getParameter("subDirectoryHost");
				
				MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
				List<MultipartFile> multipartFiles = multi.getFiles("changeFile");
				FtpUserAccount ftpUserAccount = DAORegistry.getFtpUserAccountDAO().get(Integer.parseInt(id));
				
			if(addFilesAction!=null && !addFilesAction.isEmpty()){
					
				if( multipartFiles!=null &&  !multipartFiles.isEmpty()){
															
					for (MultipartFile multipartFile : multipartFiles) {
											
						
					if(null ==multipartFile || null == multipartFile.getOriginalFilename()){
						
						if(null != ftpUserAccount){
							model.addAttribute("ftpUserAccount", ftpUserAccount);
							model.addAttribute("showEditAndDelete", true);
						}
						model.addAttribute("redmessage", "Please Choose Proper autopricing file to process.");
						return "page-manage-account-automatic-file-upload";
					}
					
					String fileName  = multipartFile.getOriginalFilename(); 
					
					String originalFileName = filePath+fileName;
					
					File file = new File(originalFileName);
					
					if(!file.exists()){
						ftpUserAccount.setFtpType(FTPType.valueOf(ftpType));
						ftpUserAccount.setHostName(host);
						ftpUserAccount.setPortNumber(ftpPort);
						ftpUserAccount.setUserName(userName);
						ftpUserAccount.setPassword(password);
						ftpUserAccount.setFrequency(Integer.parseInt(frequency));
						ftpUserAccount.setDestination(subDirectoryHost);
					//	ftpUserAccount.setFilePath(filePath);
						//ftpUserAccount.setFileSource(fileName);
						ftpUserAccount.setChangeOption(changeFileOptions);
						model.addAttribute("ftpUserAccount", ftpUserAccount);
						model.addAttribute("redmessage", "File Path and File Location are mismatching. Please choose valid file from  "+filePath);
						model.addAttribute("showEditAndDelete", true);
						return "page-manage-account-automatic-file-upload";
					}
				}	
					List<FtpUserFileInformation> userFileInfoList=new ArrayList<FtpUserFileInformation>();
				//	DAORegistry.getFtpUserFileInformationDAO().deleteAll(Integer.parseInt(id));
					for (MultipartFile multipartFile : multipartFiles) {
						
						FtpUserFileInformation 	userFileInfo=new FtpUserFileInformation();		
						String fileName  = multipartFile.getOriginalFilename(); 
						userFileInfo.setFtpUserAccount(ftpUserAccount);
						String originalFileName = filePath+fileName;
						userFileInfo.setFileName(fileName);
						userFileInfo.setFileSource(originalFileName);
						userFileInfo.setFilePath(filePath);
						userFileInfoList.add(userFileInfo);
					}
				
					ftpUserAccount.setFtpUserFileInformation(userFileInfoList);
					//ftpUserAccount.setDestination("ftp://"+host+"/");
				//	ftpUserAccount.setFilePath(filePath);
					/*ftpUserAccount.setFileSource(originalFileName);
					ftpUserAccount.setDestination("ftp://"+host+"/");
					ftpUserAccount.setFilePath(filePath);
					ftpUserAccount.setFileName(fileName);*/
			  }
			}
				
				String uploadDateDbStr = dbDateFormat.format(uiDateFormat.parse(uploadDateStr));
				uploadDateDbStr = uploadDateDbStr+" "+uploadDateHrs+":"+uploadDateMins+":00.000";
				
				Date uploadDate = dbDateTimeFormat.parse(uploadDateDbStr);
				
				/*Calendar calendar = new GregorianCalendar();
				calendar.setTime(uploadDate);
				calendar.add(Calendar.MINUTE, Integer.parseInt(frequency));*/
				
				Integer port = null != ftpPort && !ftpPort.isEmpty()?Integer.parseInt(ftpPort.trim()):0;
				
				ftpUserAccount.setFtpType(FTPType.valueOf(ftpType));
				//ftpUserAccount.setDestination("ftp://"+host+"/");
				ftpUserAccount.setDestination(subDirectoryHost);
				ftpUserAccount.setHostName(host);
				ftpUserAccount.setPortNumber(String.valueOf(port));
				ftpUserAccount.setUserName(userName);
				ftpUserAccount.setPassword(password);
				ftpUserAccount.setUploadStartTime(uploadDate);
				ftpUserAccount.setNextUploadTime(uploadDate);				
				ftpUserAccount.setLastUploadTime(null);
				ftpUserAccount.setFrequency(Integer.parseInt(frequency));
				DAORegistry.getFtpUserAccountDAO().saveOrUpdate(ftpUserAccount);
				 FtpUserAccount ftpUserAccountNew = DAORegistry.getFtpUserAccountDAO().get(Integer.parseInt(id));
					
			        if(null != ftpUserAccount){
			      	model.addAttribute("ftpUserAccount", ftpUserAccountNew);
			     }
				model.addAttribute("message", ftpUserAccount.getUserName()+" account details updated successfully..");
				
				return "page-manage-account-automatic-file-upload";
				
			}else if(null != action && !action.isEmpty() && action.equals("save")){
				
				String host  = request.getParameter("ftpHost");
				String ftpType  = request.getParameter("ftpType");
				String ftpPort  = request.getParameter("ftpPort");
				String userName  = request.getParameter("ftpUserName");
				String password  = request.getParameter("ftpPassword");
				String uploadDateStr  = request.getParameter("uploadDate");
				String uploadDateHrs  = request.getParameter("uploadDateHrs");
				String uploadDateMins  = request.getParameter("uploadDateMins");
				String frequency  = request.getParameter("uploadFrequency");
				String filePath  = request.getParameter("filePath");
				String path=request.getParameter("file");
				String subDirectoryHost  = request.getParameter("subDirectoryHost");
				
				
				MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
				
								
				List<MultipartFile> multipartFiles = multi.getFiles("file");
				
				
				for (MultipartFile multipartFileObj : multipartFiles) {
					String fileName  = multipartFileObj.getOriginalFilename();
					System.out.println("fileName----->"+fileName);
				}
				
				
				
				if(null ==multipartFiles || multipartFiles.isEmpty()){
					FtpUserAccount currentAccount = new FtpUserAccount();
					currentAccount.setHostName(host);
					currentAccount.setUserName(userName);
					currentAccount.setPassword(password);
					currentAccount.setFtpType(FTPType.valueOf(ftpType));
					currentAccount.setFrequency(Integer.parseInt(frequency));
					currentAccount.setDestination(subDirectoryHost);
				//	currentAccount.setFilePath(filePath);
					model.addAttribute("ftpUserAccount", currentAccount);
					model.addAttribute("redmessage", "Please Choose Proper autopricing file to process.");
					return "page-automatic-file-upload";
				}
				
				//MultipartFile multipartFile = multipartFiles.get(0);
				
				
				String uploadDateDbStr = dbDateFormat.format(uiDateFormat.parse(uploadDateStr));
				uploadDateDbStr = uploadDateDbStr+" "+uploadDateHrs+":"+uploadDateMins+":00.000";
				
				Date uploadDate = dbDateTimeFormat.parse(uploadDateDbStr);
				
				/*Calendar calendar = new GregorianCalendar();
				calendar.setTime(uploadDate);
				calendar.add(Calendar.MINUTE, Integer.parseInt(frequency));*/
			    FtpUserAccount existingUserAccount = DAORegistry.getFtpUserAccountDAO().getuserAccountByUserName(userName, host);
				
			for (MultipartFile multipartFile : multipartFiles) {
				
							
				String fileName  = multipartFile.getOriginalFilename(); 
				
				String originalFileName = filePath+fileName;
				
				File file = new File(originalFileName);
				
				if(!file.exists()){
					FtpUserAccount currentAccount = new FtpUserAccount();
					currentAccount.setHostName(host);
					currentAccount.setFtpType(FTPType.valueOf(ftpType));
					currentAccount.setPortNumber(ftpPort);
					currentAccount.setUserName(userName);
					currentAccount.setPassword(password);
					currentAccount.setFrequency(Integer.parseInt(frequency));
					currentAccount.setDestination(subDirectoryHost);
					//currentAccount.setFilePath(filePath);
				//	currentAccount.setFileSource(fileName);
					currentAccount.setUploadStartTime(uploadDate);
					model.addAttribute("ftpUserAccount", currentAccount);
					model.addAttribute("filePath", filePath);
					model.addAttribute("redmessage", "File Path and File Location are mismatching. Please choose valid file from  "+filePath);
					return "page-automatic-file-upload";
				}
				
			
							
			    if(existingUserAccount!=null)
			    {						
						FtpUserAccount currentAccount = new FtpUserAccount();
						currentAccount.setHostName(host);
						currentAccount.setFtpType(FTPType.valueOf(ftpType));
						currentAccount.setPortNumber(ftpPort);
						currentAccount.setUserName(userName);
						currentAccount.setPassword(password);
						currentAccount.setDestination(subDirectoryHost);
					//	currentAccount.setFileSource(originalFileName);
						currentAccount.setUploadStartTime(uploadDate);
						currentAccount.setFrequency(Integer.parseInt(frequency));
					//	currentAccount.setFilePath(filePath);
					//	currentAccount.setFileSource(multipartFile.getOriginalFilename());
						currentAccount.setJspFile(file);
						model.addAttribute("filePath", filePath);
						model.addAttribute("ftpUserAccount", currentAccount);
						model.addAttribute("redmessage", "Account Details are conflicting with User Name "+existingUserAccount.getUserName()+" and Host "+
								existingUserAccount.getHostName());
						return "page-automatic-file-upload";
				}
			
			}		
				
					
			FtpUserAccount ftpUserAccount = new FtpUserAccount();
			List<FtpUserFileInformation> userFileInfoList=new ArrayList<FtpUserFileInformation>();
			for (MultipartFile multipartFile : multipartFiles) {
				
				FtpUserFileInformation 	userFileInfo=new FtpUserFileInformation();		
				String fileName  = multipartFile.getOriginalFilename(); 
				userFileInfo.setFtpUserAccount(ftpUserAccount);
				String originalFileName = filePath+fileName;
				userFileInfo.setFileName(fileName);
				userFileInfo.setFileSource(originalFileName);
				userFileInfo.setFilePath(filePath);
				userFileInfoList.add(userFileInfo);
			}
			
				Integer port = null != ftpPort && !ftpPort.isEmpty()?Integer.parseInt(ftpPort.trim()):0;
			
				ftpUserAccount.setFtpType(FTPType.valueOf(ftpType));
				ftpUserAccount.setHostName(host);
				ftpUserAccount.setPortNumber(String.valueOf(port));
				ftpUserAccount.setUserName(userName);
				ftpUserAccount.setPassword(password);
			//	ftpUserAccount.setFileSource(" ");
				ftpUserAccount.setDestination(subDirectoryHost);
				//ftpUserAccount.setDestination("ftp://"+host+"/");
				ftpUserAccount.setUploadStartTime(uploadDate);
				ftpUserAccount.setNextUploadTime(uploadDate);
				ftpUserAccount.setFrequency(Integer.parseInt(frequency));
				ftpUserAccount.setStatus("ACTIVE");
			//	ftpUserAccount.setFilePath(filePath);
			//	ftpUserAccount.setFileName(" ");
				ftpUserAccount.setFtpUserFileInformation(userFileInfoList);
				DAORegistry.getFtpUserAccountDAO().saveOrUpdate(ftpUserAccount);
				model.addAttribute("message", "FTP Account Details Saved Successfully..");
				
				
			}else if(null != action && !action.isEmpty() && action.equals("remove")){
				
				String id  = request.getParameter("id");
				String filePath="";
				String fileName="";
				String source="";
		       
		        
		        Integer accountNo=Integer.parseInt(id);
		        List<FtpUserFileInformation> ftpUserFileInformmationList=DAORegistry.getFtpUserFileInformationDAO().getFtpUserFileInformationById(accountNo);
		       
		          for (FtpUserFileInformation ftpUserFileInformation : ftpUserFileInformmationList) {
		        	   filePath=request.getParameter(ftpUserFileInformation.getFilePath());
				       fileName=request.getParameter(ftpUserFileInformation.getFileName());
				}
		         source=filePath+fileName;
		        DAORegistry.getFtpUserFileInformationDAO().deleteFtpUserFileByIdAndSource(id,source);
				
		        FtpUserAccount ftpUserAccount = DAORegistry.getFtpUserAccountDAO().get(Integer.parseInt(id));
		
		        if(null != ftpUserAccount){
		      	model.addAttribute("ftpUserAccount", ftpUserAccount);
		    	model.addAttribute("showEditAndDelete", true);
		     }
			
	           return "page-manage-account-automatic-file-upload";
				
			}
			return "page-automatic-file-upload";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	@RequestMapping(value="/GetEditFtpUserAccount")
	public String getEditFtpUserAccount(HttpServletRequest request,Model model,HttpServletResponse response){
		String id  = request.getParameter("id");
		
				
		FtpUserAccount ftpUserAccount = DAORegistry.getFtpUserAccountDAO().get(Integer.parseInt(id));
		
		if(null != ftpUserAccount){
			model.addAttribute("ftpUserAccount", ftpUserAccount);
			model.addAttribute("showEditAndDelete", true);
		}
			
	return "page-manage-account-automatic-file-upload";
		
}
	@RequestMapping(value="/GetManualFileUpload")
	public String getManualFileUpload(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			
			String action  = request.getParameter("action");
			
			if(null != action && !action.isEmpty() && action.equals("save")){
				
				DateFormat uiDateFormat = new SimpleDateFormat("MM/dd/yyyy");
				DateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				
				String host  = request.getParameter("ftpHost");
				String ftpPort  = request.getParameter("ftpPort");
				String userName  = request.getParameter("ftpUserName");
				String password  = request.getParameter("ftpPassword");
				String subDirectoryHost  = request.getParameter("subDirectoryHost");
				String ftpType  = request.getParameter("ftpType");
				
				MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
				//MultipartFile multipartFile = multi.getFile("file");
		   
				List<MultipartFile>  multipartFiles = multi.getFiles("file");	
				List<String> messageList =new ArrayList<String>();
				List<String> failureMessageList =new ArrayList<String>();
			for (MultipartFile multipartFile : multipartFiles) {
					
				
				if(null ==multipartFile || null == multipartFile.getOriginalFilename()){
					
					model.addAttribute("ftpHost", host);
					model.addAttribute("ftpPort", ftpPort);
					model.addAttribute("ftpUserName", userName);
					model.addAttribute("ftpPassword", password);
					model.addAttribute("subDirectoryHost", subDirectoryHost);
					model.addAttribute("ftpType", ftpType);
					
					model.addAttribute("message", "Please Choose Proper autopricing file to process.");
					return "page-manual-file-upload";
				}
				
				File file = convertToFile(multipartFile);
				

				String originalFileName  = multipartFile.getOriginalFilename(); 
				String originalFilePath = originalFileName;
				
				Integer port = null != ftpPort && !ftpPort.isEmpty()?Integer.parseInt(ftpPort.trim()):0;

				FtpFileUploadAudit ftpFileUploadAudit = new FtpFileUploadAudit(host, port, userName,password,
						originalFilePath, file, subDirectoryHost, "MANUAL",FTPType.valueOf(ftpType));
				ftpFileUploadAudit.setLastUploadTime(new Date());
				ftpFileUploadAudit.setFileSize(file.length()/1024);
				ftpFileUploadAudit.setDestination(subDirectoryHost);
				
				FTPVendor ftpVendor = FTPVendor.VIVIDSEAT;
				if(host.contains("scorebig")){
					ftpVendor = FTPVendor.SCOREBIG;
				}
				
				String status ="FAILURE";
				if(file.length()>0){
				    status = FTPUploader.uploadFile(ftpFileUploadAudit,ftpVendor);
				}
				//ftpFileUploadAudit.setDestination("ftp://"+host+"/");
				ftpFileUploadAudit.setStatus(status);
				
				if(status.equals("FAILURE")){
					failureMessageList.add(originalFilePath+" file upload Failed...");
					//model.addAttribute("message", "File Upload Failed..");
					ftpFileUploadAudit.setFailedReason("File Upload Failed while uploading Manually.");
				}else{
					 messageList.add(originalFilePath+" File Uploaded Successfully..");
					//model.addAttribute("message", "File Uploaded Successfully..");
					ftpFileUploadAudit.setFailedReason("File Uploaded Successfully");
				}
				
				DAORegistry.getFtpFileUploadAuditDAO().saveOrUpdate(ftpFileUploadAudit);
				
			}
			model.addAttribute("ftpHost", host);
			model.addAttribute("ftpPort", ftpPort);
			model.addAttribute("ftpUserName", userName);
			model.addAttribute("ftpPassword", password);
			model.addAttribute("subDirectoryHost", subDirectoryHost);
			model.addAttribute("ftpType", ftpType);
			model.addAttribute("failureMessageList", failureMessageList);
			model.addAttribute("messageList", messageList);
		}	
			
			return "page-manual-file-upload";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	
	@RequestMapping(value="/LoadAutomaticFileUploadInfo")
	public String loadAutomaticFileUploadInfo(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			Collection<FtpUserAccount> ftpUserAccounts = DAORegistry.getFtpUserAccountDAO().getAllActiveuserAccount();
			
			model.addAttribute("ftpUserAccounts", ftpUserAccounts);
			
			return "page-load-automatic-file-upload-info";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="/LoadFTPAuditInfo")
	public String loadFTPAuditInfo(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			
			String selectedUserName = request.getParameter("userName");
			
			if(null == selectedUserName || selectedUserName.isEmpty() || selectedUserName.equals("")){
				selectedUserName= "All";
			}
			
			Collection<FtpUserAccount> ftpUserAccounts = DAORegistry.getFtpUserAccountDAO().getAllActiveuserAccount();
			List<FtpFileUploadAudit> ftpFileUploadAuditsFinalList =  new ArrayList<FtpFileUploadAudit>();
			
			if(selectedUserName.equalsIgnoreCase("All")){
				for (FtpUserAccount ftpUserAccount : ftpUserAccounts) {
					
					List<FtpFileUploadAudit> ftpFileUploadAudits = DAORegistry.getFtpFileUploadAuditDAO().getAllFTPAuditsByUserNameAndStatus(ftpUserAccount.getUserName(),"SUCCESS");
					if(null != ftpFileUploadAudits && !ftpFileUploadAudits.isEmpty()){
						ftpFileUploadAuditsFinalList.addAll(ftpFileUploadAudits);
					}
				}
				
				List<FtpFileUploadAudit> manualFtpFileUploadAudits = DAORegistry.getFtpFileUploadAuditDAO().getAllManualFTPAuditsByStatus("SUCCESS");
				if(null != manualFtpFileUploadAudits && !manualFtpFileUploadAudits.isEmpty()){
					ftpFileUploadAuditsFinalList.addAll(manualFtpFileUploadAudits);
				}
				
			}else{
				List<FtpFileUploadAudit> ftpFileUploadAudits = DAORegistry.getFtpFileUploadAuditDAO().getAllFTPAuditsByUserNameAndStatus(selectedUserName,"SUCCESS");
				if(null != ftpFileUploadAudits && !ftpFileUploadAudits.isEmpty()){
					ftpFileUploadAuditsFinalList.addAll(ftpFileUploadAudits);
				}
			}
			
			List<FtpFileUploadAudit> finalFTpAudits = new ArrayList<FtpFileUploadAudit>();
			
			int i=1;
			
			for (FtpFileUploadAudit ftpFileUploadAudit : ftpFileUploadAuditsFinalList) {
				
				if(i > 20){
					break;
				}
				finalFTpAudits.add(ftpFileUploadAudit);
				i++;
			}
			
			model.addAttribute("ftpFileUploadAudits", finalFTpAudits);
			model.addAttribute("ftpUserAccounts", ftpUserAccounts);
			model.addAttribute("selectedUserName", selectedUserName);
			
			return "page-load-ftp-audit-info";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="/LoadAllFTPAuditInfo")
	public String loadAllFTPAuditInfo(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			
			String selectedUserName = request.getParameter("userName");
			
			if(null == selectedUserName || selectedUserName.isEmpty() || selectedUserName.equals("")){
				selectedUserName= "All";
			}
			
			Collection<FtpUserAccount> ftpUserAccounts = DAORegistry.getFtpUserAccountDAO().getAllActiveuserAccount();
			List<FtpFileUploadAudit> ftpFileUploadAuditsFinalList =  new ArrayList<FtpFileUploadAudit>();
			
			if(selectedUserName.equalsIgnoreCase("All")){
				
				for (FtpUserAccount ftpUserAccount : ftpUserAccounts) {
					
					List<FtpFileUploadAudit> ftpFileUploadAudits = DAORegistry.getFtpFileUploadAuditDAO().getAllFTPAuditsByUserName(ftpUserAccount.getUserName());
					if(null != ftpFileUploadAudits && !ftpFileUploadAudits.isEmpty()){
						ftpFileUploadAuditsFinalList.addAll(ftpFileUploadAudits);
					}
				}
				
				List<FtpFileUploadAudit> manualFtpFileUploadAudits = DAORegistry.getFtpFileUploadAuditDAO().getAllManualFTPAudits();
				if(null != manualFtpFileUploadAudits && !manualFtpFileUploadAudits.isEmpty()){
					ftpFileUploadAuditsFinalList.addAll(manualFtpFileUploadAudits);
				}
			}else{
				List<FtpFileUploadAudit> ftpFileUploadAudits = DAORegistry.getFtpFileUploadAuditDAO().getAllFTPAuditsByUserName(selectedUserName);
				if(null != ftpFileUploadAudits && !ftpFileUploadAudits.isEmpty()){
					ftpFileUploadAuditsFinalList.addAll(ftpFileUploadAudits);
				}
			}
			
			
			List<FtpFileUploadAudit> finalFTpAudits = new ArrayList<FtpFileUploadAudit>();
			
			int i=1;
			
			for (FtpFileUploadAudit ftpFileUploadAudit : ftpFileUploadAuditsFinalList) {
				
				if(i > 20){
					break;
				}
				finalFTpAudits.add(ftpFileUploadAudit);
				i++;
			}
			
			model.addAttribute("ftpFileUploadAudits", finalFTpAudits);
			model.addAttribute("ftpUserAccounts", ftpUserAccounts);
			model.addAttribute("selectedUserName", selectedUserName);
			
			return "page-load-ftp-all-audit-info";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	@RequestMapping(value="/LocationSettings")
	public String loadFTPLocationSettings(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			FtpLocationSettings locationSettings = null;
			
			String action = request.getParameter("action");
			
			Collection<FtpLocationSettings> ftpLocationSettingsList = DAORegistry.getFtpLocationSettingsDAO().getAll();
			
			if(null == ftpLocationSettingsList || ftpLocationSettingsList.isEmpty()){
				ftpLocationSettingsList= new ArrayList<FtpLocationSettings>();
			}
			
			if(null != action && action.equals("save")){
				
				String rowCount = request.getParameter("ftpLocationsRowCount");
				int rowSize = Integer.parseInt(rowCount);
				
				List<FtpLocationSettings> toBeSaveOrUpdateList = new ArrayList<FtpLocationSettings>();
				
				for (int i = 0; i < rowSize; i++) {
					
					String rowHiddenId = request.getParameter("rowHiddenId_"+i);
					String locationId = request.getParameter("locationId_"+i);
					String ftpLocation = request.getParameter("ftpLocation_"+i);
					
					if(null != rowHiddenId && !rowHiddenId.isEmpty() && !rowHiddenId.equals("")){
						
						locationSettings = new FtpLocationSettings();
						
						if(null != locationId && !locationId.isEmpty() && !locationId.equals("")){
							locationSettings.setId(Integer.parseInt(locationId.trim()));
						}
						locationSettings.setLocation(ftpLocation.trim());
						toBeSaveOrUpdateList.add(locationSettings);
					}
				}
				
				if(toBeSaveOrUpdateList.size()>0){
					ftpLocationSettingsList.removeAll(toBeSaveOrUpdateList);
					if(ftpLocationSettingsList.size() >0 ){
						DAORegistry.getFtpLocationSettingsDAO().deleteAll(ftpLocationSettingsList);
					}
					
					DAORegistry.getFtpLocationSettingsDAO().saveOrUpdateAll(toBeSaveOrUpdateList);
				}
			}
			
			ftpLocationSettingsList = DAORegistry.getFtpLocationSettingsDAO().getAll();
			if(null == ftpLocationSettingsList || ftpLocationSettingsList.isEmpty()){
				ftpLocationSettingsList= new ArrayList<FtpLocationSettings>();
				locationSettings = new FtpLocationSettings();
				ftpLocationSettingsList.add(locationSettings);
			}
			
			model.addAttribute("ftpLocationList", ftpLocationSettingsList);
			model.addAttribute("ftpLocationListSize", ftpLocationSettingsList.size());
			
			return "page-ftp-location-settings";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
