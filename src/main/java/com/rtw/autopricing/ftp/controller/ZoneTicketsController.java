package com.rtw.autopricing.ftp.controller;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Artist;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.GrandChildCategory;
import com.rtw.autopricing.ftp.data.Venue;
import com.rtw.autopricing.ftp.zonetickets.dao.implementaion.ZoneTicketDAORegistry;
import com.rtw.autopricing.ftp.zonetickets.data.WebServiceActionType;
import com.rtw.autopricing.ftp.zonetickets.data.WebServiceTracking;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsAdditionalMarkup;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsAdditionalMarkupAudit;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsEvent;


@Controller
@RequestMapping({"/ManageZoneTicketsAdditionalMarkup","/ZoneTicketsAdditionalMarkupAuditPopup","/ZoneTicketsTracking","/ZoneTicketsOrderTracking"})
public class ZoneTicketsController {
	
	
	@RequestMapping(value="/ManageZoneTicketsAdditionalMarkup")
	public String loadManageMiniExchangeEventPage(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
		String action = request.getParameter("action");
		if(action==""){
			model.addAttribute("message", "Please select valid artist ");
		}
		if(action!=null && action!=""){
			Collection<Event> events = null;
			Collection<ZoneTicketsEvent> zoneTicketEvents = null;
			//List<ZoneTicketsAdditionalMarkup> dbZoneTicketsAdditionalMarkups = null;
			String artistStr = request.getParameter("artist");
			String[] eventStr = request.getParameterValues("events");
			String venueStr = request.getParameter("venue");
			String grandChildStr = request.getParameter("grandChild");
//			String childStr = request.getParameter("child");
			String selectedEvent = "";
			String eventsCheckAll = request.getParameter("eventsCheckAll");
			boolean  isAllEvent = ((eventsCheckAll==null || !eventsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			Integer selectedId = null;
			if(artistStr!=null && !artistStr.isEmpty()){
				Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
				zoneTicketEvents  = ZoneTicketDAORegistry.getZoneTicketsEventDAO().getAllActiveZoneTicketEventsByValues(Integer.parseInt(artistStr), null, null);
				//dbZoneTicketsAdditionalMarkups = ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupDAO().getAllZoneTicketsAdditionalMarkupByValues(Integer.parseInt(artistStr), null, null);
				selectedOption="Artist";
				selectedValue= artist.getName();
				selectedId = artist.getId();
			}else if(venueStr!=null && !venueStr.isEmpty()){
				Venue venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr));
				zoneTicketEvents  = ZoneTicketDAORegistry.getZoneTicketsEventDAO().getAllActiveZoneTicketEventsByValues(null,Integer.parseInt(venueStr), null);
				//dbZoneTicketsAdditionalMarkups = ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupDAO().getAllZoneTicketsAdditionalMarkupByValues(null,Integer.parseInt(venueStr), null);
				selectedOption="Venue";
				selectedValue= venue.getBuilding();
				events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
				selectedId = venue.getId();
			}else if(grandChildStr!=null && !grandChildStr.isEmpty()){
				GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
				zoneTicketEvents  = ZoneTicketDAORegistry.getZoneTicketsEventDAO().getAllActiveZoneTicketEventsByValues(null,null, Integer.parseInt(grandChildStr));
				//dbZoneTicketsAdditionalMarkups = ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupDAO().getAllZoneTicketsAdditionalMarkupByValues(null,null, Integer.parseInt(grandChildStr));
				selectedOption="GrandChildCategory";
				selectedValue= grandChildCategory.getName();
				events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
				selectedId = grandChildCategory.getId();
			}
			
			Map<Integer,ZoneTicketsEvent> dbZoneTicketsEventMap = new HashMap<Integer, ZoneTicketsEvent>();
			for(ZoneTicketsEvent zoneTicketEvent:zoneTicketEvents) {
				dbZoneTicketsEventMap.put(zoneTicketEvent.getId(), zoneTicketEvent);
			}
			
			Map<String,ZoneTicketsAdditionalMarkup> dbZoneTicketsAdditionalMarkupMap = new HashMap<String, ZoneTicketsAdditionalMarkup>();
			if(action.equalsIgnoreCase("update")){
				String user = SecurityContextHolder.getContext().getAuthentication().getName();
				Date now = new Date();
				
				List<ZoneTicketsAdditionalMarkup> zoneTicketAdditionalMarkups = new ArrayList<ZoneTicketsAdditionalMarkup>();
				List<ZoneTicketsAdditionalMarkupAudit> zoneTicketAdditionalMarkupAudits = new ArrayList<ZoneTicketsAdditionalMarkupAudit>();
				for (String eventIdStr : eventStr) {
					List<ZoneTicketsAdditionalMarkup> dbZoneTicketsAdditionalMarkups = ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupDAO().getAllZoneTicketsAdditionalMarkupByEventId(Integer.parseInt(eventIdStr));
					for(ZoneTicketsAdditionalMarkup ztAdditionalMarkup : dbZoneTicketsAdditionalMarkups) {
						dbZoneTicketsAdditionalMarkupMap.put(ztAdditionalMarkup.getEventId()+"-"+ztAdditionalMarkup.getZone().replaceAll("\\s+", " "), ztAdditionalMarkup);
					}
				}
				Map<String, String[]> requestParams = request.getParameterMap();
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					
					if(key.contains("checkbox_")){
						
						String zoneKey = key.replace("checkbox_", "");
						Integer eventId = Integer.parseInt(zoneKey.split("-")[0]);
						String eventZone = (zoneKey.split("-")[1]).trim();
						String userAction = "Update";
						ZoneTicketsAdditionalMarkup ztAdditioonalMarkup = dbZoneTicketsAdditionalMarkupMap.remove(zoneKey);
						if(ztAdditioonalMarkup == null){
							ztAdditioonalMarkup = new ZoneTicketsAdditionalMarkup();
							userAction = "Create";
						}
						
						String additionalMarkupStr = request.getParameter("additionalMarkup_"+zoneKey);
						Double markup = 0.0;
						if(additionalMarkupStr != null && additionalMarkupStr.trim().length() > 0) {
							markup = Double.parseDouble(additionalMarkupStr.trim());
						}
						ztAdditioonalMarkup.setEventId(eventId);
						ztAdditioonalMarkup.setZone(eventZone);
						ztAdditioonalMarkup.setAdditionalMarkup(markup);
						ztAdditioonalMarkup.setLastupdatedBy(user);
						ztAdditioonalMarkup.setLastupdatedDate(now);
						
						zoneTicketAdditionalMarkups.add(ztAdditioonalMarkup);
						
						ZoneTicketsAdditionalMarkupAudit audit = new ZoneTicketsAdditionalMarkupAudit(ztAdditioonalMarkup);
						audit.setAction(userAction);
						audit.setLastupdatedBy(user);
						audit.setLastupdatedDate(now);
						zoneTicketAdditionalMarkupAudits.add(audit);
					}
				}
				List<ZoneTicketsAdditionalMarkup> tobeDeletedList = new ArrayList<ZoneTicketsAdditionalMarkup>(dbZoneTicketsAdditionalMarkupMap.values());
				
				for (ZoneTicketsAdditionalMarkup zoneTicketsAdditionalMarkup : tobeDeletedList) {
					ZoneTicketsAdditionalMarkupAudit audit = new ZoneTicketsAdditionalMarkupAudit(zoneTicketsAdditionalMarkup);
					audit.setAction("Deleted");
					audit.setLastupdatedBy(user);
					audit.setLastupdatedDate(now);
					zoneTicketAdditionalMarkupAudits.add(audit);
				}
				
				ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupDAO().saveOrUpdateAll(zoneTicketAdditionalMarkups);
				ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupDAO().deleteAll(tobeDeletedList);
				ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupAuditDAO().saveOrUpdateAll(zoneTicketAdditionalMarkupAudits);
				
				String info = "Additional Markup Updated successfully.";
				model.addAttribute("info", info);
			}
			if(eventStr!=null) {
				dbZoneTicketsAdditionalMarkupMap = new HashMap<String, ZoneTicketsAdditionalMarkup>();
				List<ZoneTicketsAdditionalMarkup> ztAdditionalMarkups = new ArrayList<ZoneTicketsAdditionalMarkup>();
				for (String eventId : eventStr) {
					try {
						ZoneTicketsEvent zoneTicektEvent = dbZoneTicketsEventMap.get(Integer.parseInt(eventId));
						if(zoneTicektEvent != null) {
							selectedEvent += eventId + ",";
							List<String> categoryList = DAORegistry.getCategoryDAO().getAllCategoryByVenueCategoryId(zoneTicektEvent.getVenueCategoryId());
							
							if(categoryList!=null) {
								List<ZoneTicketsAdditionalMarkup> dbZoneTicketsAdditionalMarkups = ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupDAO().getAllZoneTicketsAdditionalMarkupByEventId(zoneTicektEvent.getId());
								for(ZoneTicketsAdditionalMarkup ztAdditionalMarkup : dbZoneTicketsAdditionalMarkups) {
									dbZoneTicketsAdditionalMarkupMap.put(ztAdditionalMarkup.getEventId()+"-"+ztAdditionalMarkup.getZone().replaceAll("\\s+", " "), ztAdditionalMarkup);
								}
								for (String category : categoryList) {
									String key = zoneTicektEvent.getId()+"-"+category.replaceAll("\\s+", " ");
									ZoneTicketsAdditionalMarkup ztAdditionalMarkup = dbZoneTicketsAdditionalMarkupMap.get(key);
									if(ztAdditionalMarkup == null) {
										ztAdditionalMarkup = new ZoneTicketsAdditionalMarkup();
									}
									ztAdditionalMarkup.setZone(category.replaceAll("\\s+", " "));
									ztAdditionalMarkup.setZtEvent(zoneTicektEvent);
									ztAdditionalMarkups.add(ztAdditionalMarkup);
								}		
							 }
						}
						
					} catch(Exception e){
						e.printStackTrace();
					}
				}
				model.addAttribute("ztAdditionalMarkups", ztAdditionalMarkups);
			}
			
			model.addAttribute("eventsCheckAll", isAllEvent);
			model.addAttribute("events", events);
			model.addAttribute("eventStr", selectedEvent);
			model.addAttribute("artist", artistStr);
			model.addAttribute("venue", venueStr);
			model.addAttribute("grandChild", grandChildStr);
			model.addAttribute("selectedValue", selectedValue);
			model.addAttribute("selectedOption", selectedOption);
		}
		return "page-zonetickets-additional-markup";
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return null;
	}
	}
	
	@RequestMapping(value="/ZoneTicketsAdditionalMarkupAuditPopup")
	public String getZoneTicketsAdditionalMarkupAuditPopup(HttpServletRequest request,Model model,HttpServletResponse response) throws Exception{
		String eventIdStr = request.getParameter("eventId");
		String zone = request.getParameter("zone");
		
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			Integer eventId = Integer.parseInt(eventIdStr);
			zone = zone.replaceAll("\\s+", " ");
			ZoneTicketsEvent ztEvent = ZoneTicketDAORegistry.getZoneTicketsEventDAO().getZoneTicketEventByEventId(eventId);
			List<ZoneTicketsAdditionalMarkupAudit> audits = ZoneTicketDAORegistry.getZoneTicketsAdditionalMarkupAuditDAO().getAllZoneTicketsAdditionalMarkupAuditByEventIdandZone(eventId, zone);
			model.addAttribute("ztEvent", ztEvent);
			model.addAttribute("audits", audits);
		}
		return "page-zonetickets-additional-markup-audit";
	}
	
	@RequestMapping(value="/ZoneTicketsTracking")
	public  ModelAndView zoneTicketsTracking(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException, ParseException{
		      ModelAndView mav=new ModelAndView("page-load-zone-ticket-tracking");  
		try {
			
			String fromDateString = request.getParameter("fromDate");
			String toDateString = request.getParameter("toDate");
			
			String fromHour = request.getParameter("fromHour");
			String toHour = request.getParameter("toHour");
			
			String fromMinute = request.getParameter("fromMinute");
			String toMinute = request.getParameter("toMinute");
			
			String actionType = request.getParameter("actionType");
			String webConfigId = request.getParameter("webConfigId");
			
			Date fromDate = null;
			Date toDate = null;
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy h:m");
			DateFormat df1 = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			if(fromDateString!= null){
				if(fromHour!=null && !fromHour.isEmpty()){
					if(fromMinute!=null  && !fromMinute.isEmpty()){
						fromDate = df.parse(fromDateString +  " " + fromHour + ":" + fromMinute);
					}else{
						fromDate = df.parse(fromDateString +  " " + fromHour + ":00");
					}
				}else{
					fromHour = "00";
					fromMinute = "00";
					fromDate = df.parse(fromDateString + " 00:00");
				}
			}else{
				
				fromDateString = df1.format(new Date());
			}
			
			
			if(toDateString!= null){
				if(toHour!=null  && !toHour.isEmpty()){
					if(toMinute!=null  && !toMinute.isEmpty()){
						toDate = df.parse(toDateString +  " " + toHour + ":" + toMinute);
					}else{
						toDate = df.parse(toDateString +  " " + toHour + ":00");
					}
				}else{
					toHour = "23";
					toMinute = "59";
					toDate = df.parse(toDateString + " 23:59");
				}
			}else{
				toDateString = df1.format(new Date());
			}
			
			List<WebServiceTracking> trackings = new ArrayList<WebServiceTracking>();
			if(null !=actionType && !actionType.isEmpty() && !actionType.equals("SELECT")){
				
				trackings = ZoneTicketDAORegistry.getWebServiceTrackingDAO().getAllZoneTicketsTrackingByDateandActionType(dateFormat.format(fromDate),
						dateFormat.format(toDate), actionType,webConfigId);
			}
			
			List<String> actionTypes = new ArrayList<String>();
			for(WebServiceActionType type :WebServiceActionType.values()){
				actionTypes.add(type.toString());
			}
			mav.addObject("webConfigList", ZoneTicketDAORegistry.getWebServiceTrackingDAO().getAllWebConfigInfo());
			mav.addObject("selectedWebConfigId", webConfigId);
			mav.addObject("selectedActionType", actionType);
			mav.addObject("zoneTicketsTrackings",trackings);
			mav.addObject("actionList", actionTypes);
			mav.addObject("fromDate", fromDateString);
			mav.addObject("toDate", toDateString);
			mav.addObject("fromHour", fromHour);
			mav.addObject("toHour", toHour);
			mav.addObject("fromMinute", fromMinute);
			mav.addObject("toMinute", toMinute);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	
	@RequestMapping(value="/ZoneTicketsOrderTracking")
	public  ModelAndView zoneTicketsOrderTracking(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException, ParseException{
		      ModelAndView mav=new ModelAndView("page-load-zone-ticket-order-tracking");  
		try {
			
			String fromDateString = request.getParameter("fromDate");
			String toDateString = request.getParameter("toDate");
			
			String fromHour = request.getParameter("fromHour");
			String toHour = request.getParameter("toHour");
			
			String fromMinute = request.getParameter("fromMinute");
			String toMinute = request.getParameter("toMinute");
			
			String actionType = request.getParameter("actionType");
			
			String webConfigId = request.getParameter("webConfigId");
			String gpsSearch = request.getParameter("gpsSearch");
			
			Date fromDate = null;
			Date toDate = null;
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy h:m");
			DateFormat df1 = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			if(fromDateString!= null){
				if(fromHour!=null && !fromHour.isEmpty()){
					if(fromMinute!=null  && !fromMinute.isEmpty()){
						fromDate = df.parse(fromDateString +  " " + fromHour + ":" + fromMinute);
					}else{
						fromDate = df.parse(fromDateString +  " " + fromHour + ":00");
					}
				}else{
					fromHour = "00";
					fromMinute = "00";
					fromDate = df.parse(fromDateString + " 00:00");
				}
			}else{
				
				fromDateString = df1.format(new Date());
			}
			
			
			if(toDateString!= null){
				if(toHour!=null  && !toHour.isEmpty()){
					if(toMinute!=null  && !toMinute.isEmpty()){
						toDate = df.parse(toDateString +  " " + toHour + ":" + toMinute);
					}else{
						toDate = df.parse(toDateString +  " " + toHour + ":00");
					}
				}else{
					toHour = "23";
					toMinute = "59";
					toDate = df.parse(toDateString + " 23:59");
				}
			}else{
				toDateString = df1.format(new Date());
			}
			
			if(null == gpsSearch || gpsSearch.isEmpty() ){
				gpsSearch = "ALL";
			}
			
			List<WebServiceTracking> trackings = new ArrayList<WebServiceTracking>();
			if(null !=actionType && !actionType.isEmpty() && !actionType.equals("SELECT")){
				
				if(actionType.equals("CREATEORDER")){
					
					trackings = ZoneTicketDAORegistry.getWebServiceTrackingDAO().getAllZoneTicketsOrdersTrackingByDate(dateFormat.format(fromDate),
							dateFormat.format(toDate),webConfigId);
					mav.addObject("showGrid", "Order");
				}else if(actionType.equals("GENERALISEDSEARCH")){
					
					trackings = ZoneTicketDAORegistry.getWebServiceTrackingDAO().getAllZoneTicketsEventsTrackingByDate(dateFormat.format(fromDate),
							dateFormat.format(toDate),webConfigId,gpsSearch);
					mav.addObject("showGrid", "Event");
				}else{
					
					trackings = ZoneTicketDAORegistry.getWebServiceTrackingDAO().getAllZoneTicketsTicketTrackingByDate(dateFormat.format(fromDate),
							dateFormat.format(toDate),webConfigId);
					mav.addObject("showGrid", "Ticket");
				}
			}
			
			List<String> actionTypes = new ArrayList<String>();
			actionTypes.add(WebServiceActionType.CREATEORDER.toString());
			actionTypes.add(WebServiceActionType.GENERALISEDSEARCH.toString());
			actionTypes.add(WebServiceActionType.TICKET.toString());
			
			mav.addObject("webConfigList", ZoneTicketDAORegistry.getWebServiceTrackingDAO().getAllWebConfigInfo());
			mav.addObject("selectedWebConfigId", webConfigId);
			mav.addObject("selectedActionType", actionType);
			mav.addObject("selectedGpsSearchOption", gpsSearch);
			mav.addObject("zoneTicketsTrackings",trackings);
			mav.addObject("actionList", actionTypes);
			mav.addObject("fromDate", fromDateString);
			mav.addObject("toDate", toDateString);
			mav.addObject("fromHour", fromHour);
			mav.addObject("toHour", toHour);
			mav.addObject("fromMinute", fromMinute);
			mav.addObject("toMinute", toMinute);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
}
