package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.Category;
import com.rtw.autopricing.ftp.enums.EventStatus;


public interface CategoryDAO extends RootDAO<Integer, Category> {
	public void deleteByVenueCategoryId(Integer venueId)throws Exception;
	public Collection<Category> getAllCategoriesByVenueCategoryId(Integer venueCategoryId) throws Exception;
	public Collection<Category> getAllCategoriesByVenueIdAndCategoryGroup(Integer venueId, String categoryGroup)throws Exception;
	public Category getCategoryByVenueCategoryIdAndCategorySymbol(Integer venueCategoryId, String categorySymbol)throws Exception;
	public String getCategorieGroupByVenueCategoryId(Integer venueCategoryId)throws Exception;
	public Collection<Category> getAllCategoriesByVenuCategoryIdsByArtistId(Integer artistId,EventStatus eventSataus)throws Exception;
	public int countByVenueCategoryID(Integer id)throws Exception;
	public List<String> getAllCategoryByVenueCategoryId(Integer venueCategoryId)throws Exception;
	
}