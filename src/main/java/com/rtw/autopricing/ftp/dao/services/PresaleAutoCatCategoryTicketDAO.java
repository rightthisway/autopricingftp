package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.PresaleAutoCatCategoryTicket;



public interface PresaleAutoCatCategoryTicketDAO extends RootDAO<Integer, PresaleAutoCatCategoryTicket> {

	public Collection<PresaleAutoCatCategoryTicket> getAllActivePresaleAutoCatsCategoryTickets() throws Exception;
	public List<PresaleAutoCatCategoryTicket> getAllPresaleAutoCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	
	public List<PresaleAutoCatCategoryTicket> getAllPresaleAutoCatsCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<PresaleAutoCatCategoryTicket> getAllPresaleAutoCatCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInPresaleAutoCatCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<PresaleAutoCatCategoryTicket> getAllTNPresaleAutoCatCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllPresaleAutoCatCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllPresaleAutoCatCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnPresaleAutoCatCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnPresaleAutoCatCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllPresaleAutoCatTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<PresaleAutoCatCategoryTicket> getAllPresaleAutoCatCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
	public List<PresaleAutoCatCategoryTicket> getAllTNPresaleAutoCatCategoryTicketsByEventIdByBrokerID(Integer eventId,Integer brokerId) throws Exception;
}

