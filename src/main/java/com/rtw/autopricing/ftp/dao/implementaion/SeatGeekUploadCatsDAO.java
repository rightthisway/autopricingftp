package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.ftp.data.SGLastFiveRowCategoryTicket;
import com.rtw.autopricing.ftp.data.SeatGeekUploadCats;


public class SeatGeekUploadCatsDAO extends HibernateDAO<Integer, SeatGeekUploadCats> implements com.rtw.autopricing.ftp.dao.services.SeatGeekUploadCatsDAO {
	
	public List<SeatGeekUploadCats> getAllActiveSeatGeekUploadCats() throws Exception {
		return find(" FROM SeatGeekUploadCats where status=? ",new Object[] {"ACTIVE"}); 
	}
	
	public List<SeatGeekUploadCats> getAllActiveSeatGeekListingsByEventId(Integer eventId) throws Exception {
		return find("FROM SeatGeekUploadCats where status='ACTIVE' and eventId=?", new Object[] {eventId}); 
	}
	public List<SeatGeekUploadCats> getAllActiveSeatGeekListingsByEventIdAndInternalNotes(Integer eventId,String internalNotes) throws Exception {
		return find("FROM SeatGeekUploadCats where status='ACTIVE' and eventId=? and internalNotes=?", new Object[] {eventId,internalNotes}); 
	}
	public List<SeatGeekUploadCats> getAllActiveSeatGeekListingsNotInsgLastFiveRowCategoryTicket() throws Exception {
		return find("FROM SeatGeekUploadCats where status='ACTIVE' and internalNotes='SGLFR' and catTicketId not in(" +
				"select id FROM SGLastFiveRowCategoryTicket where status='ACTIVE')", new Object[] {}); 
	}
	public List<SeatGeekUploadCats> getAllActiveSeatGeekListingsNotInMinicatCategoryTicket() throws Exception {
		return find("FROM SeatGeekUploadCats where status='ACTIVE' and internalNotes='MINICATS' and catTicketId not in(" +
				"select id FROM MiniCategoryTicket where status='ACTIVE')", new Object[] {}); 
	}
}

