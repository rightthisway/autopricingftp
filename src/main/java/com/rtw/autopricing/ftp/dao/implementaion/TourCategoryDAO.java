package com.rtw.autopricing.ftp.dao.implementaion;
import java.util.List;

import com.rtw.autopricing.ftp.data.TourCategory;



public class TourCategoryDAO extends HibernateDAO<Integer, TourCategory> implements com.rtw.autopricing.ftp.dao.services.TourCategoryDAO {

	
	public TourCategory getTourCategoryByName(String name) {
		return findSingle("FROM TourCategory WHERE name = ? ORDER BY name", new Object[]{name});
	}

	
	public List<TourCategory> getAllTourCategoryOrderByName() {
		return find("FROM TourCategory ORDER BY name");
	}
	
	
	public Integer save(TourCategory entity) {
		// TODO Auto-generated method stub
		super.save(entity);
		return entity.getId();
	}
	
	
	public void update(TourCategory entity) {
		// TODO Auto-generated method stub
		super.update(entity);
	}
	
}
