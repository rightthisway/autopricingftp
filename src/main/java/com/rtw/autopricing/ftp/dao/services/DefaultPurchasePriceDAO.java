package com.rtw.autopricing.ftp.dao.services;


import java.util.List;

import com.rtw.autopricing.ftp.data.DefaultPurchasePrice;


public interface DefaultPurchasePriceDAO extends RootDAO<Integer , DefaultPurchasePrice> {
	DefaultPurchasePrice getAllDefaultTourPriceByTourId(Integer Id)throws Exception;
	List<DefaultPurchasePrice> getAllDefaultTourPrice()throws Exception;
	DefaultPurchasePrice getDefaultTourPriceByExchangeAndTicketType(String exchange,String ticketType)throws Exception;
}
