package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEventAudit;

public interface SGLastFiveRowExchangeEventAuditDAO extends RootDAO<Integer, SGLastFiveRowExchangeEventAudit>{
	public Collection<SGLastFiveRowExchangeEventAudit> getAllSGLastFiveRowExchangeEventAuditsByEventId(Integer eventId);
}
