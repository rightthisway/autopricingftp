package com.rtw.autopricing.ftp.dao.implementaion;



import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.MarketMakerEventTracking;
import com.rtw.autopricing.ftp.data.User;



public class MarketMakerEventTrackingDAO extends HibernateDAO<Integer, MarketMakerEventTracking> implements com.rtw.autopricing.ftp.dao.services.MarketMakerEventTrackingDAO {
	public User getCurrentMarketMakerForEvent(Integer eventId) {
		List<User> users = getHibernateTemplate().find("SELECT u FROM User u, MarketMakerEventTracking mmet WHERE u.username=mmet.marketMakerUsername AND mmet.eventId=? AND mmet.enabled=true", new Object[]{eventId});
		if (users.isEmpty()) {
			return null;
		}
		
		return users.get(0);
	}
	
	public MarketMakerEventTracking getCurrentMarketMakerEventTracking(Integer eventId) {
		return findSingle("FROM MarketMakerEventTracking WHERE eventId=? AND enabled=true", new Object[]{eventId});
	}
	
	public Collection<Event> getCurrentEventsManagedByMarketMaker(String username) {
		return getHibernateTemplate().find("SELECT e FROM Event e, MarketMakerEventTracking mmet WHERE mmet.marketMakerUsername=? AND mmet.eventId=e.id AND mmet.enabled=true", new Object[]{username});
	}
	
	public Collection<Event> getAllEventsManagedByMarketMaker(String username) {
		return getHibernateTemplate().find("SELECT e FROM Event e, MarketMakerEventTracking mmet WHERE mmet.marketMakerUsername=? AND mmet.eventId=e.id", new Object[]{username});		
	}
}
