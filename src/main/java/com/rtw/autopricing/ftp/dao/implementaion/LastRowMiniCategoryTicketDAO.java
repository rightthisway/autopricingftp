package com.rtw.autopricing.ftp.dao.implementaion;



import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.ftp.data.LastRowMiniCategoryTicket;


public class LastRowMiniCategoryTicketDAO extends HibernateDAO<Integer, LastRowMiniCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.LastRowMiniCategoryTicketDAO {

	public void deleteAll(Collection<LastRowMiniCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<LastRowMiniCategoryTicket> getAllActiveLastRowMiniCategoryTickets()  throws Exception {
		return find("FROM LastRowMiniCategoryTicket where status ='ACTIVE'");
	}
	
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsByAll(Integer eventId, String section, String row, String quantity)  throws Exception {
		String query = "FROM LastRowMiniCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND lastRow=?";
			param.add(row);
		}
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(Integer.parseInt(quantity));							
		
			}
		
		return find(query, param.toArray());
	}
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsById(Integer Id)  throws Exception {
		return find("FROM LastRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id)  throws Exception {
		return find(" select priceHistory FROM LastRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM LastRowMiniCategoryTicket where eventId=? AND status=?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<LastRowMiniCategoryTicket> getAllTNLastRowMiniCategoryTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM LastRowMiniCategoryTicket where eventId=? and status = ? and tnCategoryTicketGroupId is not null and tnCategoryTicketGroupId <> 0" ,new Object[]{eventId,"ACTIVE"});
	}
	public Integer deleteAllLastRowMiniCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllLastRowMiniCategoryTicketsnotExistinExchangeEventforAnyExchange()  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for all Exchanges' " +
				" FROM lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
			    " WHERE tg.status='ACTIVE' AND event_id NOT IN (" +
			    " SELECT event_id FROM lastrow_mini_exchange_event le WITH(NOLOCK)" +
			    " inner join event e on e.id=le.event_id " +
			    " inner join venue_category vc on vc.id=e.venue_category_id" +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND vc.id is not null AND e.presale_event=0 AND e.venue_id is not null " +//AND vc.category_group !='PRESALE'
				" AND (le.tn_broker_id is not null OR le.vivid_broker_id is not null OR le.scorebig_broker_id is not null OR le.fanxchange_broker_id is not null OR le.ticketcity_broker_id is not null OR le.seatgeek_broker_id is not null)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllTnLastRowMiniCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception {
		String sql ="  UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' AND e.event_status='ACTIVE' " +
	    		" AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllTnLastRowMiniCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId)  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for TN' " +
	    " FROM lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' AND event_id NOT IN (" +
	    " SELECT event_id FROM lastrow_mini_exchange_event le WITH(NOLOCK)" +
	    " inner join event e on e.id=le.event_id " +
	    " inner join venue_category vc on vc.id=e.venue_category_id" +
	    " where e.event_status='ACTIVE' AND status='ACTIVE' AND le.tn_broker_id ="+brokerId+" AND vc.id is not null AND e.presale_event=0 ) ";//AND vc.category_group !='PRESALE'
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllLastRowMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingproduct)  throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Ticket not Exist in TN' " +
	    		" FROM lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
	    		" WHERE tg.tn_broker_id=" +broker.getId()+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' " +
	    		" and tg.tn_category_ticket_group_id not in(select distinct category_ticket_group_id from "+serverName+".category_ticket_group WITH(NOLOCK) " +
	    		" where internal_notes='"+autopricingproduct.getInternalNotes()+"')";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return result;
	}
	
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInLastRowMiniCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingproduct)  throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql = " SELECT ctg.category_ticket_group_id as groupId " +
					" FROM " + serverName +".category_ticket_group ctg WITH(NOLOCK) " + 
					 " INNER JOIN " + serverName + ".category_ticket ct ON ct.category_ticket_group_id = ctg.category_ticket_group_id " +
					 " LEFT JOIN " + serverName + ".invoice i ON i.invoice_id = ct.invoice_id " +
					 " WHERE ctg.category_ticket_group_id NOT IN (select distinct tn_category_ticket_group_id from lastrow_minicat_category_ticket WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND tn_broker_id="+broker.getId()+" AND tn_category_ticket_group_id is not null) AND ctg.internal_notes = '"+autopricingproduct.getInternalNotes()+"' " +
					 " and datediff(minute,ctg.create_date,GETDATE())>15" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 " GROUP BY ctg.category_ticket_group_id " +
					 " HAVING SUM(CASE WHEN i.invoice_id IS NOT NULL THEN 1 ELSE 0 END )=0";
		
		Session session=null;
		
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	
	}
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCatsCategoryTicketsByPosCategoryTicektGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId)  throws Exception {
		
		List<LastRowMiniCategoryTicket> result = new ArrayList<LastRowMiniCategoryTicket>();
		try {
			Map<Integer,String> map = new HashMap<Integer, String>();
			String tnCategoryTicketGroupIdsStr = "";
			int i = 0;
			int count =0;
			for(Integer id:categoryTicektGroupIds){
				if(count==2000){
					count =0;
					i++;
					map.put(i, tnCategoryTicketGroupIdsStr);
					tnCategoryTicketGroupIdsStr ="";
				}
				tnCategoryTicketGroupIdsStr = tnCategoryTicketGroupIdsStr + id + ",";
				count++;
			}
			if(!tnCategoryTicketGroupIdsStr.isEmpty()){
				map.put(++i, tnCategoryTicketGroupIdsStr);
			}
			for(Integer ii:map.keySet()){
				tnCategoryTicketGroupIdsStr = map.get(ii);
				if(!tnCategoryTicketGroupIdsStr.isEmpty()){
					tnCategoryTicketGroupIdsStr = tnCategoryTicketGroupIdsStr.substring(0, tnCategoryTicketGroupIdsStr.length()-1);
				}
				Collection<LastRowMiniCategoryTicket> temp = find("FROM LastRowMiniCategoryTicket where tnBrokerId="+brokerId+" and tnCategoryTicketGroupId IN ("  + tnCategoryTicketGroupIdsStr + ")" +
						" and status<>'SOLD' "); 
				if(temp!=null){
					result.addAll(temp);
				}
			}
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//return result;
	}
}

