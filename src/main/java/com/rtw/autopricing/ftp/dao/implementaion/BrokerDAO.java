package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.Broker;


public class BrokerDAO extends HibernateDAO<Integer, Broker> implements com.rtw.autopricing.ftp.dao.services.BrokerDAO{

	
	public Collection<Broker> getAllActiveBrokers() {
		return find("FROM Broker WHERE status='ACTIVE'", new Object[]{});
	}
	public Collection<Broker> getAllActiveAutopricingBrokers() {
		return find("FROM Broker WHERE status='ACTIVE' AND isAutopricingBroker=?", new Object[]{Boolean.TRUE});
	}
	
	public Collection<Broker> getAllBrokers() {
    	return find("FROM Broker WHERE  is_autopricing_broker = 1");
    }
	 
	 
	 public Broker getBrokerByPOSBrokerId(int brokerId) {
    	List<Broker> brokers = find("FROM Broker WHERE posBrokerId = ? " , new Object[]{brokerId});
    	if(brokers !=null && !brokers.isEmpty()){
    		return brokers.get(0);
    	}
    	return null;
    }
	 public Broker getBrokerByName(String brokerName) {
		 return findSingle("FROM Broker WHERE status='ACTIVE' AND name = ? " , new Object[]{brokerName});
	 }
	 public List<Integer> getAllActiveAutopricingBrokerIds() {
		 return find("SELECT DISTINCT id FROM Broker WHERE status='ACTIVE' AND isAutopricingBroker=?", new Object[]{Boolean.TRUE});
	 }
	 public Collection<Broker> getAllActiveAutopricingBrokersWithMZTixBroker() {
			return find("FROM Broker WHERE status='ACTIVE' AND (isAutopricingBroker=? OR id=1)", new Object[]{Boolean.TRUE});
	}

}
