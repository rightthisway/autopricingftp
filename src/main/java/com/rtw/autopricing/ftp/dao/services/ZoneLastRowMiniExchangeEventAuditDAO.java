package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEventAudit;

public interface ZoneLastRowMiniExchangeEventAuditDAO extends RootDAO<Integer, ZoneLastRowMiniExchangeEventAudit>{
	public Collection<ZoneLastRowMiniExchangeEventAudit> getAllZoneLastRowMiniExchangeEventAuditsByEventId(Integer eventId);
	public Collection<ZoneLastRowMiniExchangeEventAudit> getAllZoneLastRowMiniExchangeEventAuditsByVenueId(Integer venueId);
	public Collection<ZoneLastRowMiniExchangeEventAudit> getAllZoneLastRowMiniExchangeEventAuditsByArtistId(Integer artistId);
	public Collection<ZoneLastRowMiniExchangeEventAudit> getAllZoneLastRowMiniExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId);
}
