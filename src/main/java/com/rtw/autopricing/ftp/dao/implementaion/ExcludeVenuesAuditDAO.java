package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.ExcludeVenuesAudit;

public class ExcludeVenuesAuditDAO extends HibernateDAO<Integer, ExcludeVenuesAudit> implements com.rtw.autopricing.ftp.dao.services.ExcludeVenuesAuditDAO{

	public List<ExcludeVenuesAudit> getExcludeVenuesAuditList(Integer brokerId,
			Integer productId, Integer venueId) throws Exception  {
		return find("From ExcludeVenuesAudit where brokerId = ? and productId = ? and venueId= ? order by lastUpdatedDate",new Object[]{brokerId,productId,venueId});
	}

	public ExcludeVenuesAudit getExcludeVenuesAudit(Integer brokerId,
			Integer productId, Integer venueId) throws Exception  {
		// TODO Auto-generated method stub
		return findSingle("From ExcludeVenuesAudit where brokerId = ? and productId = ? and venueId= ? ",new Object[]{brokerId,productId,venueId});
	}

}
