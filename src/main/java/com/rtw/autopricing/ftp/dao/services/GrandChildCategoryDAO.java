package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.GrandChildCategory;


public interface GrandChildCategoryDAO extends RootDAO<Integer, GrandChildCategory> {
	public Collection<GrandChildCategory> getGrandChildCategoryByChildTourCategory(Integer id);
	public Collection<GrandChildCategory> getGrandChildCategoryByChildTourCategoryName(String name);
	public GrandChildCategory getGrandChildCategoryByNameAndChildTourCategoryNameAndGrandChildCategoryName(String name,String categoryName,String partentCategoryName);
	public void deleteAllById(List<Integer> ids);
	public Collection<GrandChildCategory> getAllGrandChildTourOrderByName();
	public Collection<GrandChildCategory> getGrandChildCategoryByChildTourCategorys(List<Integer> ids);
	public Collection<GrandChildCategory> getGrandChildCategoriesByName(String name);
}
