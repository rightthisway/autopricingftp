package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.ExcludeEventZones;



public interface ExcludeEventZonesDAO extends RootDAO<Integer,  ExcludeEventZones>{

	public List<ExcludeEventZones> getExcludeEventZonesByProductId(Integer productId)  throws Exception;
	public List<ExcludeEventZones> getExcludeEventZonesByProductIdandBrokerId(Integer productId,Integer brokerId)  throws Exception;
	public List<ExcludeEventZones> getExcludeEventZonesByBrokerIdandProductIds(Integer brokerId,String productIdStr)  throws Exception;
	public List<ExcludeEventZones> getAllExcludeEventZonesByEventId(Integer eventId)  throws Exception;
	public List<ExcludeEventZones> getExcludeEventZonesByProductIdAndBrokerIdAndEventId(
			Integer productId, Integer brokerId, Integer eventId)  throws Exception;
}
