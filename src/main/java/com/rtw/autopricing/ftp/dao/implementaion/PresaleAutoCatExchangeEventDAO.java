package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEvent;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.enums.EventStatus;

public class PresaleAutoCatExchangeEventDAO  extends HibernateDAO<Integer, PresaleAutoCatExchangeEvent> implements com.rtw.autopricing.ftp.dao.services.PresaleAutoCatExchangeEventDAO{

	public PresaleAutoCatExchangeEvent getPresaleAutoCatExchangeEventByEventId(Integer eventId) throws Exception {
		return findSingle("FROM PresaleAutoCatExchangeEvent WHERE eventId=?", new Object[]{eventId});
	}
	
	public Collection<PresaleAutoCatExchangeEvent> getAllActivePresaleAutoCatExchangeEvents()  throws Exception {
		return find("FROM PresaleAutoCatExchangeEvent WHERE status='ACTIVE'", new Object[]{});
	}

	public Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByVenueId(Integer venueId)  throws Exception {
		return find("SELECT le FROM PresaleAutoCatExchangeEvent le ,Event e WHERE e.id = le.eventId And e.venueId=?", new Object[]{venueId});
	}

	public Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByArtistId(Integer artistId)  throws Exception {
		return find("SELECT le FROM PresaleAutoCatExchangeEvent le ,Event e WHERE e.id = le.eventId And e.artistId=?", new Object[]{artistId});
	}

	public Collection<PresaleAutoCatExchangeEvent>  getAllPresaleAutoCatExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId)  throws Exception {
		return find("FROM le FROM PresaleAutoCatExchangeEvent le ,Event e WHERE e.id = le.eventId And e.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}
	public List<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsofDeletedTmatEvents()  throws Exception {
		return find("FROM PresaleAutoCatExchangeEvent WHERE status='ACTIVE' and eventId in(" +
				"SELECT id FROM Event WHERE eventStatus <> ?)", new Object[]{EventStatus.ACTIVE});
	}

	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType)  throws Exception {
		
		Session session=null;
		
		try {
			String query = "";
			
			if(globalAudit.getExposure() != null) {
				query = query + ",ee.exposure='"+globalAudit.getExposure()+"'";
			}
			if(globalAudit.getShippingMethod() != null) {
				query = query + ",ee.shipping_method='"+globalAudit.getShippingMethod()+"'";
			}
			if(globalAudit.getNearTermDisplayOption() != null) {
				query = query + ",ee.near_term_display_option='"+globalAudit.getNearTermDisplayOption()+"'";
			}
			if(globalAudit.getRptFactor() != null) {
				query = query + ",ee.rpt_factor="+globalAudit.getRptFactor();
			}
			if(globalAudit.getPriceBreakup() != null) {
				query = query + ",ee.price_breakup="+globalAudit.getPriceBreakup();
			}
			if(globalAudit.getLowerMarkup() != null) {
				query = query + ",ee.lower_markup="+globalAudit.getLowerMarkup();
			}
			if(globalAudit.getUpperMarkup() != null) {
				query = query + ",ee.upper_markup="+globalAudit.getUpperMarkup();
			}
			if(globalAudit.getLowerShippingFees() != null) {
				query = query + ",ee.lower_shipping_fees="+globalAudit.getLowerShippingFees();
			}
			if(globalAudit.getUpperShippingFees() != null) {
				query = query + ",ee.upper_shipping_fees="+globalAudit.getUpperShippingFees();
			}
			if(globalAudit.getShippingDays()!= null) {
				query = query + ",ee.shipping_days="+globalAudit.getShippingDays();
			}
			
			query = query + ",last_updated_date=GETDATE()";
			
			query = "update ee set " + query.substring(1);
			
			query = query + " from presale_autocat_exchange_event ee " +
			" inner join event e on e.id=ee.event_id " +
			" inner join artist t on t.id=e.artist_id" +
			" inner join grand_child_tour_category gc on gc.id=t.grand_child_category_id" +
			" inner join child_tour_category cc on cc.id=gc.child_category_id" +
			" inner join tour_category tc on tc.id=cc.tour_category_id " +
			" where tc.name in("+parentType+")";
			
			
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			session.close();
		}
	}
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception {
		Session session=null;
		
		try {
			String query = "";
			
			
			if(autopricingVenueSetting.getShippingMethod() != null) {
				query = query + ",ee.shipping_method="+autopricingVenueSetting.getShippingMethod();
			} else {
				query = query + ",ee.shipping_method=null";
			}
			if(autopricingVenueSetting.getNearTermDisplayOption() != null) {
				query = query + ",ee.near_term_display_option="+autopricingVenueSetting.getNearTermDisplayOption();
			} else {
				query = query + ",ee.near_term_display_option=null";
			}
			if(autopricingVenueSetting.getMarkup() != null) {
				query = query + ",ee.lower_markup="+autopricingVenueSetting.getMarkup()+",ee.upper_markup="+autopricingVenueSetting.getMarkup();
			}
			if(autopricingVenueSetting.getShippingDays() != null) {
				query = query + ",ee.shipping_days="+autopricingVenueSetting.getShippingDays();
			}
			
			query = query + ",last_updated_date=GETDATE(),last_updated_by='"+autopricingVenueSetting.getLastUpdatedBy()+"'";
			query = "update ee set " + query.substring(1);
			
			
			query = query + " from presale_autocat_exchange_event ee " +
			" inner join event e on e.id=ee.event_id " +
			" where e.venue_id="+autopricingVenueSetting.getVenueId()+"";
			
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			session.close();
		}
	}

	public Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct,boolean isAllPresaleEvents)  throws Exception {
		String sql =" select distinct le.id as id,le.event_id as eventId, le.exposure as exposure, " +
					" le.rpt_factor as rptFactor,le.price_breakup as priceBreakup,le.lower_markup as lowerMarkup, " +
					" le.upper_markup as upperMarkup,le.lower_shipping_fees as lowerShippingFees, " +
					" le.upper_shipping_fees as upperShippingFees,le.shipping_method as shippingMethod, " +
					" le.near_term_display_option as nearTermDisplayOption,le.allow_section_range as allowSectionRange, " +
					" le.shipping_days as shippingDays,le.vivid_broker_id as vividBrokerId,le.tn_broker_id as ticketNetworkBrokerId," +
					" le.scorebig_broker_id as scoreBigBrokerId,le.fanxchange_broker_id as fanxchangeBrokerId,le.ticketcity_broker_id as ticketcityBrokerId,le.seatgeek_broker_id as seatGeekBrokerId,le.is_zone as zone," +
					" minicats_enabled as minicatsEnabled,lastrowminicats_enabled as lastrowMinicatsEnabled," +
					" vipminicats_enabled as vipMinicatsEnabled,presale_autocats_enabled as presaleAutocatsEnabled," +
					" rot_pos_enabled as rotEnabled,rtw_pos_enabled as rtwEnabled,1 as isPresale " + 
					" from presale_autocat_exchange_event le " + 
					" INNER join ticket_listing_crawl tc on tc.event_id = le.event_id " +
					" INNER JOIN event e on e.id = le.event_id " +
					" INNER JOIN venue_category vc on vc.id = e.venue_category_id " +
					" left join exclude_event_zones ez on ez.event_id=e.id and ez.product_id="+autopricingProduct.getId() +
					" left join exclude_venue_category_zones vcz on vcz.venue_category_id=e.venue_category_id and vcz.product_id="+autopricingProduct.getId() +
					" WHERE le.status = 'ACTIVE'  AND e.presale_event=1";//AND vc.category_group='presale'
		
		//Tamil : to consider all presale events
		if(!isAllPresaleEvents) {
					//" AND le.allow_section_range = 1 "+ 
			sql = sql + " AND tc.site_id in ('ticketnetworkdirect','stubhub','ticketevolution','flashseats','vividseat') " +
					//" AND (le.tn_broker_id is not null OR le.vivid_broker_id is not null OR le.scorebig_broker_id is not null OR le.fanxchange_broker_id is not null OR le.ticketcity_broker_id is not null)" +
					" AND ((DATEDIFF(MINUTE,tc.end_crawl,GETDATE())<=  " + minute +
					" OR DATEDIFF(MINUTE,vc.last_updated_date,GETDATE())<=  " + minute +
					" OR (DATEDIFF(MINUTE,le.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (DATEDIFF(MINUTE,e.last_update,GETDATE())<= " + minute + ")" +
					" OR (ez.id is not null and DATEDIFF(MINUTE,ez.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (vcz.id is not null and DATEDIFF(MINUTE,vcz.last_updated_date,GETDATE())<= " + minute + "))) " ;
		}
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			sqlQuery.addScalar("id",Hibernate.INTEGER);
			sqlQuery.addScalar("exposure",Hibernate.STRING);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("rptFactor",Hibernate.DOUBLE);
			sqlQuery.addScalar("shippingMethod",Hibernate.INTEGER);
			sqlQuery.addScalar("nearTermDisplayOption",Hibernate.INTEGER);
			
			sqlQuery.addScalar("priceBreakup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("vividBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("scoreBigBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("ticketNetworkBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("fanxchangeBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("ticketcityBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("seatGeekBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("allowSectionRange",Hibernate.BOOLEAN);
			sqlQuery.addScalar("zone",Hibernate.BOOLEAN);
			sqlQuery.addScalar("shippingDays",Hibernate.INTEGER);
			
			sqlQuery.addScalar("rotEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("rtwEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("minicatsEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("lastrowMinicatsEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("vipMinicatsEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("presaleAutocatsEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("isPresale", Hibernate.BOOLEAN);
			
			List<PresaleAutoCatExchangeEvent> exchangeEvents =  sqlQuery.setResultTransformer(Transformers.aliasToBean(PresaleAutoCatExchangeEvent.class)).list();
			return exchangeEvents;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return null;
	}

	public List<PresaleAutoCatExchangeEvent> getPresaleAutoCatExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId)  throws Exception {
		Session session =getSession();
		List<PresaleAutoCatExchangeEvent> list=null;
		String sql = "";
		
		if(brokerId.equals(10)){//RTW-2 //ROT
		     sql=" select * from presale_autocat_exchange_event ee inner join event e on ee.event_id=e.id " +
			" where status='ACTIVE' and e.venue_id="+venueId+" and (ee.rot_pos_enabled=" + 1 +") ";
	    }else if(brokerId.equals(5)){//RTW
		      sql=" select * from presale_autocat_exchange_event ee inner join event e on ee.event_id=e.id " +
			" where status='ACTIVE' and e.venue_id="+venueId+" and (ee.rtw_pos_enabled=" + 1 +") ";
	    }else if(brokerId.equals(4)){//Manhattan
		      sql=" select * from presale_autocat_exchange_event ee inner join event e on ee.event_id=e.id " +
				" where status='ACTIVE' and e.venue_id="+venueId+" and (manhattan_pos_enabled =" + 1 +") ";
		}
		try{
			list=  session.createSQLQuery(sql).addEntity(PresaleAutoCatExchangeEvent.class).list();
		   }catch(Exception e){
			   e.printStackTrace();
			   throw e;
		   }finally{
			   session.close();
		   }
		return list;
	}
	
	public Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception {
		return find("FROM PresaleAutoCatExchangeEvent WHERE eventId in(" +
				"SELECT id FROM Event WHERE admitoneId in ("+ admitoneEventIds +"))");
	}
	public Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByEventIds(String eventIds)  throws Exception {
		return find("FROM PresaleAutoCatExchangeEvent WHERE eventId in("+ eventIds +")");
	}
}