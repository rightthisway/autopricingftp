package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import com.rtw.autopricing.ftp.data.GrandChildCategory;


public class GrandChildCategoryDAO extends HibernateDAO<Integer, GrandChildCategory> implements com.rtw.autopricing.ftp.dao.services.GrandChildCategoryDAO {

	
	public Collection<GrandChildCategory> getGrandChildCategoryByChildTourCategory(Integer id) {
		return find("FROM GrandChildCategory ctc where ctc.childTourCategory.id= ? ORDER BY name",new Object[]{id});
	}

	
	public Collection<GrandChildCategory> getGrandChildCategoryByChildTourCategoryName(String name) {
		return find("FROM GrandChildCategory ctc where ctc.childTourCategory.name=? ORDER BY name",new Object[]{name});
	}

	
	public GrandChildCategory getGrandChildCategoryByNameAndChildTourCategoryNameAndGrandChildCategoryName(
			String name, String categoryName,String parentCategoryName) {
		return findSingle("FROM GrandChildCategory ctc WHERE name = ? AND ctc.childTourCategory.name=? and ctc.childTourCategory.tourCategory.name = ? ",new Object[]{name,categoryName,parentCategoryName});
	}

	public void deleteAllById(List<Integer> ids) {
		/*String stringIds="";
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		
		for(Integer id:ids){
			if(id==null){
				continue;
			}
			List<TMATDependentGrandChildCategory> list = DAORegistry.getTmatDependentGrandChildCategoryDAO().getTmatDependentsByGrandChildCategoryId(id);
			Map<String, TMATDependentGrandChildCategory> map = new HashMap<String, TMATDependentGrandChildCategory>();
			if(list!=null && !list.isEmpty()){
				for(TMATDependentGrandChildCategory cat:list){
					map.put(cat.getDependent(), cat);
				}
			}
			
			stringIds+=id +",";
			if(property!=null && !property.getValue().isEmpty()){
				String temp[] = property.getValue().split(",");
				for(String zone:temp){
					TMATDependentGrandChildCategory dependent = map.get(zone);
					if(dependent==null){
						 dependent = new TMATDependentGrandChildCategory();
					}
					dependent.setGrandChildCategoryId(id);
					dependent.setDependent(zone);
					dependent.setAdd(false);
					DAORegistry.getTmatDependentGrandChildCategoryDAO().saveOrUpdate(dependent);
				}
			}
		}
		
		stringIds=stringIds.substring(0,stringIds.length()-1);
		if(!"".equals(stringIds)){
			bulkUpdate("DELETE FROM GrandChildCategory WHERE id in(" + stringIds + ")");
		}*/
		
	}

	/**
	 * return grand child by name where name should match the input
	 */
	public Collection<GrandChildCategory> getGrandChildCategoriesByName(String name) {
		String hql="FROM GrandChildCategory WHERE name like ? and name <> 'NONE' and name <> 'OTHER' ORDER BY name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
	}

	public Collection<GrandChildCategory> getAllGrandChildTourOrderByName() {
		return find("FROM GrandChildCategory ORDER BY name");
		
	}


	public Collection<GrandChildCategory> getGrandChildCategoryByChildTourCategorys(
			List<Integer> ids) {
		
			Query query = getSession().createQuery("FROM GrandChildCategory where childTourCategory.id in (:Ids)  ORDER By name").setParameterList("Ids", ids);
		   
		  @SuppressWarnings("unchecked")
		  List<GrandChildCategory> list= query.list();
		  return list;
	}

	@Override
	public Integer save(GrandChildCategory entity) {
		/*// TODO Auto-generated method stub
		super.save(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentGrandChildCategory dependent = new TMATDependentGrandChildCategory();
				dependent.setGrandChildCategoryId(entity.getId());
				dependent.setDependent(zone);
				dependent.setAdd(true);
				DAORegistry.getTmatDependentGrandChildCategoryDAO().save(dependent);
			}
		}
		return entity.getId();*/
		return 0;
	}
	
	@Override
	public void update(GrandChildCategory entity) {
		/*super.update(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		List<TMATDependentGrandChildCategory> list = DAORegistry.getTmatDependentGrandChildCategoryDAO().getTmatDependentsByGrandChildCategoryId(entity.getId());
		Map<String, TMATDependentGrandChildCategory> map = new HashMap<String, TMATDependentGrandChildCategory>();
		if(list!=null && !list.isEmpty()){
			for(TMATDependentGrandChildCategory cat:list){
				map.put(cat.getDependent(), cat);
			}
		}
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentGrandChildCategory dependent = map.get(zone);
				if(dependent==null){
					 dependent = new TMATDependentGrandChildCategory();
				}
				dependent.setGrandChildCategoryId(entity.getId());
				dependent.setDependent(zone);
				dependent.setAdd(true);
				DAORegistry.getTmatDependentGrandChildCategoryDAO().saveOrUpdate(dependent);
			}
		}*/
	}
	
	@Override
	public void delete(GrandChildCategory entity) {
		/*super.delete(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		List<TMATDependentGrandChildCategory> list = DAORegistry.getTmatDependentGrandChildCategoryDAO().getTmatDependentsByGrandChildCategoryId(entity.getId());
		Map<String, TMATDependentGrandChildCategory> map = new HashMap<String, TMATDependentGrandChildCategory>();
		if(list!=null && !list.isEmpty()){
			for(TMATDependentGrandChildCategory cat:list){
				map.put(cat.getDependent(), cat);
			}
		}
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentGrandChildCategory dependent = map.get(zone);
				if(dependent==null){
					 dependent = new TMATDependentGrandChildCategory();
				}
				dependent.setGrandChildCategoryId(entity.getId());
				dependent.setDependent(zone);
				dependent.setAdd(false);
				DAORegistry.getTmatDependentGrandChildCategoryDAO().saveOrUpdate(dependent);
			}
		}*/
	}
	
	@Override
	public void deleteAll(Collection<GrandChildCategory> entities) {
		/*Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(GrandChildCategory entity:entities){
				List<TMATDependentGrandChildCategory> list = DAORegistry.getTmatDependentGrandChildCategoryDAO().getTmatDependentsByGrandChildCategoryId(entity.getId());
				Map<String, TMATDependentGrandChildCategory> map = new HashMap<String, TMATDependentGrandChildCategory>();
				if(list!=null && !list.isEmpty()){
					for(TMATDependentGrandChildCategory cat:list){
						map.put(cat.getDependent(), cat);
					}
				}
				for(String zone:temp){
					TMATDependentGrandChildCategory dependent = map.get(zone);
					if(dependent==null){
						 dependent = new TMATDependentGrandChildCategory();
					}
					dependent.setGrandChildCategoryId(entity.getId());
					dependent.setDependent(zone);
					dependent.setAdd(true);
					DAORegistry.getTmatDependentGrandChildCategoryDAO().saveOrUpdate(dependent);
				}
			}
		}
		super.deleteAll(entities);*/
	}
}
