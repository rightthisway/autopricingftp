package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingSettingsAudit;


public class AutopricingSettingsAuditDAO extends HibernateDAO<Integer, AutopricingSettingsAudit> implements com.rtw.autopricing.ftp.dao.services.AutopricingSettingsAuditDAO {

	public List<AutopricingSettingsAudit> getAutopricingSettingsAuditList(Integer brokerId,Integer productId,Integer exchangeId) {
			
		return find(" FROM AutopricingSettingsAudit where brokerId=? and productId=? and exchangeId=? order by lastUpdated",new Object[]{brokerId,productId,exchangeId});
	}
}
