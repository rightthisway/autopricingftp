package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.ExcludeVenuesAudit;

public interface ExcludeVenuesAuditDAO extends RootDAO<Integer,ExcludeVenuesAudit> {
    
}
