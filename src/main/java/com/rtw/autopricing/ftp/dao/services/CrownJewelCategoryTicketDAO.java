package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.CrownJewelCategoryTicket;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;


public interface CrownJewelCategoryTicketDAO extends RootDAO<Integer, CrownJewelCategoryTicket> {
	
	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTicketsByEventId(Integer eventId) throws Exception;
	public void updateCrownJewelCategoryTicketstoDeletedByEventId(Integer eventId) throws Exception;
	public List<CrownJewelCategoryTicket> getAllCrownJewelCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllCrownJewelCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllCrownJewelCategoryTicketsnotExistinCrownJewelEventforZoneTicketExchange()  throws Exception;
	public Integer deleteAllCrownJewelCategoryTicketGroupIdsNotExistInZoneTicketsCJCategoryTicket()  throws Exception;
	public List<Integer> getAllCrownJewelTicketsTicketGroupIdsNotExistInTmat()  throws Exception;

}
