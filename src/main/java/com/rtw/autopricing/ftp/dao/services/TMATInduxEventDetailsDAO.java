package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.TMATInduxEventDetails;

public interface TMATInduxEventDetailsDAO extends RootDAO<Integer, TMATInduxEventDetails>{
	
	public TMATInduxEventDetails getTMATInduxEventDetailsByEventId(Integer eventId)  throws Exception;
}
