package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;

public interface PresaleAutoCatExchangeEventDAO extends RootDAO<Integer, PresaleAutoCatExchangeEvent> {

	PresaleAutoCatExchangeEvent getPresaleAutoCatExchangeEventByEventId(Integer eventId) throws Exception;
	public Collection<PresaleAutoCatExchangeEvent> getAllActivePresaleAutoCatExchangeEvents() throws Exception;
	Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct,boolean isAllPresaleEvents) throws Exception;
	public List<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<PresaleAutoCatExchangeEvent> getAllPresaleAutoCatExchangeEventsByEventIds(String eventIds)  throws Exception;
}
