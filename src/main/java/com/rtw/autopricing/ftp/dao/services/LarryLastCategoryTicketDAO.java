package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.LarryLastCategoryTicket;



public interface LarryLastCategoryTicketDAO extends RootDAO<Integer, LarryLastCategoryTicket> {

	public Collection<LarryLastCategoryTicket> getAllActiveLarryLastCategoryTickets() throws Exception;
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsByAll(Integer eventId, String section, String row,String quantity) throws Exception;
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<LarryLastCategoryTicket> getAllTNLarryLastCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<LarryLastCategoryTicket> getAllTNLarryLastCategoryTicketsByEventIdByBrokerID(Integer eventId,Integer brokerId);
	
	public Integer deleteAllTnLarryLastCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,int excludeEventDays) throws Exception;
	public Integer deleteAllLarryLastCategoryTicketswithinExcludeEventDays(int excludeEventDays) throws Exception;
	public Integer deleteAllTnLarryLastCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllLarryLastCategoryTicketsnotExistinExchangeEventforAnyExchange(Integer brokerId) throws Exception;
	public Integer deleteAllLarryLastTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInLarryLastCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsByPosTicketGroupIds(List<Integer> ticektGroupIds,Integer brokerId) throws Exception;
	public Integer updateCategoryTicketReasonByTnTicketGroupIandBrokerId(Integer tnTicketGroupId,Integer brokerId,String reason) throws Exception;
	public Integer deleteAllLarryLastCategoryTicketsInWhichProductDisabled(String internalNotes) throws Exception;
}
