package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.VipMiniExchangeEventAudit;

public class VipMiniExchangeEventAuditDAO extends HibernateDAO<Integer, VipMiniExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.VipMiniExchangeEventAuditDAO{

	public Collection<VipMiniExchangeEventAudit> getAllVipMiniExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM VipMiniExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<VipMiniExchangeEventAudit> getAllVipMiniExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM VipMiniExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<VipMiniExchangeEventAudit> getAllVipMiniExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM VipMiniExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<VipMiniExchangeEventAudit> getAllVipMiniExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM VipMiniExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}

}