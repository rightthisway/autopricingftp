package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.rtw.autopricing.ftp.data.DuplicateTicketMap;


public class DuplicateTicketMapDAO extends HibernateDAO<Integer, DuplicateTicketMap> implements com.rtw.autopricing.ftp.dao.services.DuplicateTicketMapDAO {
	
	public Collection<DuplicateTicketMap> getAllDuplicateTicketByEventId(Integer eventId) {
		return find("FROM DuplicateTicketMap WHERE eventId=?", new Object[]{eventId});
	}
	
	public void deleteByGroupId(Long groupId) {
		bulkUpdate("DELETE FROM DuplicateTicketMap WHERE groupId=?", new Object[]{groupId});
	}
	
	public void deleteGroupsWithOneEntry(Integer eventId) {
		Map<Long, Integer> groupCountMap = new HashMap<Long, Integer>();
		for (DuplicateTicketMap ticketMap: getAllDuplicateTicketByEventId(eventId)) {
			if (groupCountMap.get(ticketMap.getGroupId()) == null) {
				groupCountMap.put(ticketMap.getGroupId(), 1);
			} else {
				groupCountMap.put(ticketMap.getGroupId(), groupCountMap.get(ticketMap.getGroupId()) + 1);
			}
		}
		//bulkUpdate("DELETE FROM DuplicateTicketMap WHERE groupId IN (SELECT groupId FROM DuplicateTicketMap GROUP BY groupId HAVING COUNT(*) = 1)");
		
		for (Entry<Long, Integer> entry: groupCountMap.entrySet()) {
			if (entry.getValue() > 1) {
				continue;
			}
			
			deleteByGroupId(entry.getKey());
		}
	}
	
	public void delete(Integer ticketId, boolean deleteGroup) {
		if (deleteGroup) {
			bulkUpdate("DELETE FROM DuplicateTicketMap WHERE groupId IN(SELECT groupId FROM DuplicateTicketMap WHERE ticketId=?)", new Object[]{ticketId});
		} else {
			deleteById(ticketId);
		}
	}
	
	public void deleteById(Integer ticketId) {
		bulkUpdate("DELETE FROM DuplicateTicketMap WHERE ticketId=?", new Object[]{ticketId});
	}
}
