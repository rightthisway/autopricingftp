package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingSettings;


public interface AutopricingSettingsDAO extends RootDAO<Integer, AutopricingSettings>{

	public AutopricingSettings getAutopricingSettings(Integer brokerId,Integer productId,Integer exchangeId);
	public List<AutopricingSettings> getAutopricingSettingsByProductIdandExchangeId(Integer productId,Integer exchangeId);
	public Integer getMinimumExcludeEventDaysByProductId(Integer productId);
	public Boolean getIsBrokerEnabled(Integer brokerId);
	public Boolean getIsBrokerEnabledForExchange(Integer brokerId,Integer exchangeId);
}
