package com.rtw.autopricing.ftp.dao.implementaion;




import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.LarryLastCategoryTicket;
import com.rtw.autopricing.util.AutoExchangeEventLoader;
import com.rtw.autopricing.util.CategoryTicket;



public class LarryLastCategoryTicketDAO extends HibernateDAO<Integer, LarryLastCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.LarryLastCategoryTicketDAO {


	
	public void deleteAll(Collection<LarryLastCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<LarryLastCategoryTicket> getAllActiveLarryLastCategoryTickets()throws Exception {
		return find("FROM LarryLastCategoryTicket where status ='ACTIVE'");
	}
	
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsByAll(Integer eventId, String section, String row,String quantity) throws Exception{
		String query = "FROM LarryLastCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND lastRow=?";
			param.add(row);
		}
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(Integer.parseInt(quantity));								
		
			}
		
		
		return find(query, param.toArray());
	}
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsById(Integer Id) throws Exception{
		return find("FROM LarryLastCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) throws Exception{
		return find("SELECT priceHistory FROM LarryLastCategoryTicket where id=?" ,new Object[]{Id});
	}

	public List<LarryLastCategoryTicket> getAllTnPriceMismatchLarryLastCategoryTicketsByBrokerId(Integer brokerId){
		return null;
	}
	
	/*public void deleteLarryLastMiniCatTickets(List<LarryLastCategoryTicket> tickets) {
		
		
	}*/

	public List<LarryLastCategoryTicket> getAllLarryTicketsNotExistInLarryExchangeEventByBrokerId(
			Integer brokerId) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsByEventId(Integer eventId) throws Exception{
		return find("FROM LarryLastCategoryTicket where eventId=? and status = ?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<LarryLastCategoryTicket> getAllTNLarryLastCategoryTicketsByEventId(Integer eventId) throws Exception{
		return find("FROM LarryLastCategoryTicket where eventId=? and status = ? and tnTicketGroupId is not null and tnTicketGroupId <> 0" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<LarryLastCategoryTicket> getAllTNLarryLastCategoryTicketsByEventIdByBrokerID(Integer eventId,Integer brokerId) {
		return find("FROM LarryLastCategoryTicket where eventId=? and status = ? and tnBrokerId = ? " ,new Object[]{eventId,"ACTIVE",brokerId});
	}
	public Integer deleteAllTnLarryLastCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,int excludeEventDays) throws Exception{
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM larrylast_category_ticket tg WITH(NOLOCK)  " +
	    " inner join event e on e.id=tg.event_id" +
	    " WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_ticket_group_id > 0 AND tg.status='ACTIVE' " +
		" AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllLarryLastCategoryTicketswithinExcludeEventDays(int excludeEventDays)throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
		" FROM larrylast_category_ticket tg WITH(NOLOCK)  " +
	    " inner join event e on e.id=tg.event_id" +
	    " WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllTnLarryLastCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception{
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for TN' " +
		" FROM larrylast_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_ticket_group_id > 0 AND tg.status='ACTIVE' AND event_id NOT IN (" +
	    " SELECT event_id FROM larrylast_exchange_event le WITH(NOLOCK)" +
	    " inner join event e on e.id=le.event_id " +
	    " inner join venue_category vc on vc.id=e.venue_category_id" +
	    " where e.event_status='ACTIVE' AND status='ACTIVE' AND le.tn_broker_id ="+brokerId+" AND vc.id is not null ) ";//AND vc.category_group !='PRESALE'

		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer updateCategoryTicketReasonByTnTicketGroupIandBrokerId(Integer tnTicketGroupId,Integer brokerId,String reason) throws Exception{
		
		String sql =" UPDATE tg set reason='"+reason+"' " +
		" FROM larrylast_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_ticket_group_id ="+tnTicketGroupId+" AND tg.tn_broker_id="+brokerId+"";

		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
public Integer deleteAllLarryLastCategoryTicketsnotExistinExchangeEventforAnyExchange(Integer brokerId)throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for all Exchanges' " +
		" FROM larrylast_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.status='ACTIVE' AND  tn_broker_id="+brokerId+" AND event_id NOT IN (" +
	    " SELECT event_id FROM larrylast_exchange_event le WITH(NOLOCK)" +
	    " inner join event e on e.id=le.event_id " +
	    " inner join venue_category vc on vc.id=e.venue_category_id" +
	    " where e.event_status='ACTIVE' AND status='ACTIVE' AND vc.id is not null AND e.venue_id is not null ";//AND vc.category_group !='PRESALE'
	    
		if(brokerId.equals(10)){//RTW-2	/*Reserve One Tickets*/
			sql += " AND le.rot_pos_enabled > 0";
		} else if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//Tixcity
			sql += " AND le.tixcity_pos_enabled > 0";
		} else if(brokerId.equals(5)){	/*RTW*/
			sql += " AND le.rtw_pos_enabled > 0";
		}
		sql += ")";

		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}

public Integer deleteAllLarryLastCategoryTicketsInWhichProductDisabled(String internalNotes) throws Exception {
	
	String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Product not Exist in Exchange Event for TN' " +
			" FROM larrylast_category_ticket tg WITH(NOLOCK)  " +
			" WHERE tg.status='ACTIVE'  AND internal_notes='"+internalNotes+"'  AND event_id NOT IN (" +//AND tg.is_presale=0
		    " SELECT event_id FROM larrylast_exchange_event le WITH(NOLOCK)" +
		    " inner join event e on e.id=le.event_id " +
		    " inner join venue_category vc on vc.id=e.venue_category_id" +
		    " where e.event_status='ACTIVE' AND status='ACTIVE' AND vc.id is not null AND " +//AND vc.category_group !='PRESALE' 
		    " e.venue_id is not null  ";
	
	if(internalNotes.equals(AutoExchangeEventLoader.ZONED_LASTROW_INTERNAL_NOTES)){		/*MINICATS*/
		sql += " AND le.zoned_lastrowminicats_enabled > 0";
	}else if(internalNotes.equals(AutoExchangeEventLoader.LARRYLAST_INTERNAL_NOTES)){	/*LASTROW MINICATS*/
		sql += " AND le.larrylast_enabled > 0";
	}
	sql += ")";
		
	int result = 0;
	Session session=null;
	try{
		session = getSession();
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		result = sqlQuery.executeUpdate();
		
		return result;
	}catch (Exception e) {
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
	//return result;
}
	public Integer deleteAllLarryLastTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception{
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Ticket not Exist in TN' " +
		" FROM larrylast_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_broker_id="+broker.getId()+" AND tg.tn_ticket_group_id > 0 AND tg.status='ACTIVE' " +
		" and tg.tn_ticket_group_id not in(select distinct ticket_group_id from "+serverName+".ticket_group WITH(NOLOCK) " +
		" where internal_notes in ('"+AutoExchangeEventLoader.ZONED_LASTROW_INTERNAL_NOTES+"','"+AutoExchangeEventLoader.LARRYLAST_INTERNAL_NOTES+"'))";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInLarryLastCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception{
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		/*String sql = " SELECT ctg.ticket_group_id as groupId " +
					" FROM " + serverName +".ticket_group ctg WITH(NOLOCK) " + 
					 " INNER JOIN " + serverName + ".ticket ct ON ct.ticket_group_id = ctg.ticket_group_id " +
					 " LEFT JOIN " + serverName + ".invoice i ON i.invoice_id = ct.invoice_id " +
					 " WHERE ctg.ticket_group_id NOT IN (select distinct tn_ticket_group_id from larrylast_category_ticket WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND tn_broker_id="+broker.getId()+" AND tn_ticket_group_id is not null) AND ctg.internal_notes in " +
					 " ('"+AutoExchangeEventLoader.ZONED_LASTROW_INTERNAL_NOTES+"','"+AutoExchangeEventLoader.LARRYLAST_INTERNAL_NOTES+"') " +
					 " and datediff(minute,ctg.create_date,GETDATE())>15" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 " GROUP BY ctg.ticket_group_id " +
					 " HAVING SUM(CASE WHEN i.invoice_id IS NOT NULL THEN 1 ELSE 0 END )=0";*/
		
		String sql = " SELECT ctg.ticket_group_id as groupId " +
				" FROM     " + serverName + ".ticket_group ctg WITH(NOLOCK)" +
				" WHERE ctg.office_id="+broker.getSsAccountOfficeId()+" and ctg.internal_notes " +
				" in ('"+AutoExchangeEventLoader.ZONED_LASTROW_INTERNAL_NOTES+"','"+AutoExchangeEventLoader.LARRYLAST_INTERNAL_NOTES+"')" +
				" and NOT exists (select distinct tn_ticket_group_id from larrylast_category_ticket tn" +
				" WITH(NOLOCK) where tn.tn_ticket_group_id=ctg.ticket_group_id and tn.status = 'ACTIVE'" +
				" AND tn_broker_id="+broker.getId()+") AND" +
				" not exists (select * from   " + serverName + ".ticket t " +
				" where t.ticket_group_id=ctg.ticket_group_id and t.invoice_id is not null) ";
		
		Session session=null;
		
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	}
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsByPosTicketGroupIds(List<Integer> ticektGroupIds,Integer brokerId) throws Exception{
		
		List<LarryLastCategoryTicket> result = new ArrayList<LarryLastCategoryTicket>();
		try {
			Map<Integer,String> map = new HashMap<Integer, String>();
			String tnTicketGroupIdsStr = "";
			int i = 0;
			int count =0;
			for(Integer id:ticektGroupIds){
				if(count==2000){
					count =0;
					i++;
					map.put(i, tnTicketGroupIdsStr);
					tnTicketGroupIdsStr ="";
				}
				tnTicketGroupIdsStr = tnTicketGroupIdsStr + id + ",";
				count++;
			}
			if(!tnTicketGroupIdsStr.isEmpty()){
				map.put(++i, tnTicketGroupIdsStr);
			}
			for(Integer ii:map.keySet()){
				tnTicketGroupIdsStr = map.get(ii);
				if(!tnTicketGroupIdsStr.isEmpty()){
					tnTicketGroupIdsStr = tnTicketGroupIdsStr.substring(0, tnTicketGroupIdsStr.length()-1);
				}
				Collection<LarryLastCategoryTicket> temp = find("FROM LarryLastCategoryTicket where tnBrokerId="+brokerId+" and tnTicketGroupId IN ("  + tnTicketGroupIdsStr + ")" +
						" and status<>'SOLD'"); 
				if(temp!=null){
					result.addAll(temp);
				}
			}
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//return result;
	}
}
