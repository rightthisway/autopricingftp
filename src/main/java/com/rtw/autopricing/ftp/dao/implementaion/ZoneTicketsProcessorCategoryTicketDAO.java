package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;


public class ZoneTicketsProcessorCategoryTicketDAO extends HibernateDAO<Integer, ZoneTicketsProcessorCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.ZoneTicketsProcessorCategoryTicketDAO {

	public void deleteAll(Collection<ZoneTicketsProcessorCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<ZoneTicketsProcessorCategoryTicket> getAllActiveTgCatsCategoryTickets()  throws Exception {
		return find("FROM ZoneTicketsProcessorCategoryTicket where status ='ACTIVE'");
	}
	
	public List<ZoneTicketsProcessorCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity)  throws Exception {
		String query = "FROM ZoneTicketsProcessorCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(Integer.parseInt(quantity));								
		
			} 
		
		/*if (row !=null && !row.isEmpty())
		{
			query += " AND (rowRange=?)";
			param.add(row);
		}*/
		
		
		return find(query, param.toArray());
	}
	
	public List<ZoneTicketsProcessorCategoryTicket> getAllZoneTicektsProcessorCategoryTicketsByIds(List<Integer> zoneTicektGroupIds)  throws Exception {
		
		List<ZoneTicketsProcessorCategoryTicket> result = new ArrayList<ZoneTicketsProcessorCategoryTicket>();
		try {
			Map<Integer,String> map = new HashMap<Integer, String>();
			String zoneTicketProcessorIdsStr = "";
			int i = 0;
			int count =0;
			for(Integer id:zoneTicektGroupIds){
				if(count==2000){
					count =0;
					i++;
					map.put(i, zoneTicketProcessorIdsStr);
					zoneTicketProcessorIdsStr ="";
				}
				zoneTicketProcessorIdsStr = zoneTicketProcessorIdsStr + id + ",";
				count++;
			}
			if(!zoneTicketProcessorIdsStr.isEmpty()){
				map.put(++i, zoneTicketProcessorIdsStr);
			}
			for(Integer ii:map.keySet()){
				zoneTicketProcessorIdsStr = map.get(ii);
				if(!zoneTicketProcessorIdsStr.isEmpty()){
					zoneTicketProcessorIdsStr = zoneTicketProcessorIdsStr.substring(0, zoneTicketProcessorIdsStr.length()-1);
				}
				Collection<ZoneTicketsProcessorCategoryTicket> temp = find("FROM ZoneTicketsProcessorCategoryTicket WHERE zone_tickets_ticket_group_id IN ("  + zoneTicketProcessorIdsStr + ")" + " AND status<>'SOLD'"); 
				if(temp!=null){
					result.addAll(temp);
				}
			}
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//return result;
	}
	
	public List<ZoneTicketsProcessorCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id)  throws Exception {
		return find("FROM ZoneTicketsProcessorCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id)  throws Exception {
		return find(" select priceHistory FROM ZoneTicketsProcessorCategoryTicket where id=?" ,new Object[]{Id});
	}
	
	public List<ZoneTicketsProcessorCategoryTicket> getAllZoneTicketsProcessorCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM ZoneTicketsProcessorCategoryTicket where eventId=? AND status=?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<ZoneTicketsProcessorCategoryTicket> getAllTNZoneTicketsProcessorCategoryTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM ZoneTicketsProcessorCategoryTicket where eventId=? and status = ? and tnCategoryTicketGroupId is not null and tnCategoryTicketGroupId <> 0" ,new Object[]{eventId,"ACTIVE"});
	}
	
	public Integer deleteAllZoneTicketsProcessorCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM zone_tickets_category_ticket_group tg WITH(NOLOCK)  " +
				" inner join zone_tickets_processor_exchange_event tn with(nolock) on tn.event_id=tg.event_id " +
				" inner join event e with(nolock) on e.id=tg.event_id" +
				" WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' " +
				" AND (datediff(HH,getdate(),e.event_date)+iif(e.event_time is not null, DATEPART(HH,e.event_time),0))<tn.exclude_hours_before_event";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllZoneTicketsProcessorCategoryTicketsnotExistinExchangeEventforZoneTicketExchange()  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event not Exist in Exchange Event for ZoneTickets' " +
				" FROM zone_tickets_category_ticket_group tg WITH(NOLOCK)  " +
			    " WHERE tg.status='ACTIVE' AND event_id NOT IN (" +
			    " SELECT event_id FROM zone_tickets_processor_exchange_event le WITH(NOLOCK)" +
			    " inner join event e with(nolock) on e.id=le.event_id " +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND e.venue_category_id is not null AND e.venue_id is not null " +
			    " AND le.zoneticket_broker_id > 0) ";
				//" AND (le.tn_broker_id > 0 OR le.vivid_broker_id > 0 OR le.scorebig_broker_id > 0)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllZoneTicketsProcessorCategoryTicketGroupIdsNotExistInZoneTicketsTicektGroup(AutopricingProduct autopricingProduct)  throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("zone.tickets.linked.server");
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Ticket not Exist in ZoneTickets'  " +
	    		" FROM zone_tickets_category_ticket_group tg WITH(NOLOCK)  " +
	    		" WHERE tg.zone_tickets_ticket_group_id is not null AND tg.status='ACTIVE' " +
	    		" and tg.zone_tickets_ticket_group_id not in(select distinct id from "+serverName+".category_ticket_group WITH(NOLOCK) " +
	    		" where status='ACTIVE' and internal_notes='"+autopricingProduct.getInternalNotes()+"')";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public List<Integer> getAllZoneTicketsTicketGroupIdsNotExistInTmat(AutopricingProduct autopricingProduct)  throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("zone.tickets.linked.server");
		
		String sql = " SELECT ctg.id as groupId " +
					" FROM " + serverName +".category_ticket_group ctg WITH(NOLOCK) " + 
					 " WHERE ctg.status='ACTIVE' and ctg.internal_notes='"+autopricingProduct.getInternalNotes()+"' " +
					 " AND ctg.id NOT IN (select distinct zone_tickets_ticket_group_id from zone_tickets_category_ticket_group WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND zone_tickets_ticket_group_id is not null) and datediff(minute,ctg.last_updated,GETDATE())>3" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 "";
		
		Session session=null;
		
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	
	}
	
}

