package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.rtw.autopricing.ftp.data.AdmitoneInventory;


public class AdmitoneInventoryDAO extends HibernateDAO<Integer, AdmitoneInventory> implements com.rtw.autopricing.ftp.dao.services.AdmitoneInventoryDAO {

	@Deprecated
	public Collection<AdmitoneInventory> getAllInventoryByDate(Date eventDate)  {
		return new ArrayList<AdmitoneInventory>();
		//return find("FROM AdmitoneInventory WHERE eventId=?", new Object[]{eventId});
	}
	
	public Collection<AdmitoneInventory> getAllInventoryByAdmitOneEventId(Integer eventId) throws Exception {
		return find("FROM AdmitoneInventory WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<AdmitoneInventory> getAllInventoryByEventId(Integer eventId) throws Exception {
		return find("SELECT ae FROM AdmitoneInventory ae, Event e WHERE e.id=? AND e.admitoneId=ae.eventId", new Object[]{eventId});
	}

	
}
