package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.DuplicateTicketMap;


public interface DuplicateTicketMapDAO extends RootDAO<Integer, DuplicateTicketMap> {
	Collection<DuplicateTicketMap> getAllDuplicateTicketByEventId(Integer eventId);
	void deleteByGroupId(Long groupId);
	void deleteGroupsWithOneEntry(Integer eventId);
	void delete(Integer ticketId, boolean deleteGroup);
}