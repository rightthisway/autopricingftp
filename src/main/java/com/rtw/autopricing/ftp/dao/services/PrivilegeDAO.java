package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.Privilege;


public interface PrivilegeDAO extends RootDAO<Integer, Privilege> {
}
