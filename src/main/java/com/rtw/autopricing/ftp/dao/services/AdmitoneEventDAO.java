package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.autopricing.ftp.data.AdmitoneEvent;


public interface AdmitoneEventDAO extends RootDAO<Integer, AdmitoneEvent> {
	List<AdmitoneEvent> getAllEventsByEventNamesAndVenueName(String[] eventNames,String venueName,List<String> parentCategoryList)throws Exception ;
	List<String> getAllDistinctZoneEventsByVenueName(String venueName,List<String> parentCategoryList)throws Exception ;
	List<AdmitoneEvent> getAllZoneEventsByEventNamesAndVenueName(String[] eventNames,String venueName,List<String> parentCategoryList)throws Exception ;
	List<AdmitoneEvent> getAllZoneEventsByVenueName(String venueName,List<String> parentCategoryList)throws Exception ;
	public List<Integer> getAllEventIdsByVenue(String venue,List<String> parentCategoryList)throws Exception ;
	List<AdmitoneEvent> getAllEventsByEventIds(List<Integer> eventIds)throws Exception ;
	List<String> getDistinctVenues(List<String> parentCategoryList)throws Exception ;
	Collection<Integer> getAllEventIds()throws Exception ;
	Collection<AdmitoneEvent> getAllEventsNearDate(Date eventDate)throws Exception ;
	AdmitoneEvent getEventByEventId(Integer eventId)throws Exception ;
	public List<Integer> getAllEventIdsByParentCategoryList(List<String> parentCategoryList)throws Exception ;
	public List<AdmitoneEvent> getEventsByName(String eventName)throws Exception ;
	List<String> getAllDistinctZonedVenues()throws Exception ;
}
