package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.List;

import com.rtw.autopricing.ftp.data.CategoryMapping;
import com.rtw.autopricing.ftp.enums.EventStatus;

public class CategoryMappingDAO extends HibernateDAO<Integer, CategoryMapping> implements com.rtw.autopricing.ftp.dao.services.CategoryMappingDAO {

	public List<CategoryMapping> getAllCategoryMappingsByVenueCategoryId(Integer venueCategoryId) throws Exception {
		return find("FROM CategoryMapping WHERE categoryId IN (SELECT id FROM Category WHERE venueCategoryId = ? ) ORDER BY id", new Object[]{venueCategoryId});
	}

	public List<CategoryMapping> getAllCategoryMappingsByCategoryId(Integer categoryId) throws Exception {
		return find("FROM CategoryMapping WHERE categoryId = ? ", new Object[]{categoryId}); 
	}
	
	public List<String> getAllCategoryGroupNamesByTourIdByVenueCategoryId(Integer tourId,EventStatus eventStatus) throws Exception {
		return find("select categoryGroup from VenueCategory WHERE id in (Select distinct venueCategoryId from Event where tourId = ? and eventStatus =? )", 
				new Object[]{tourId,eventStatus});
	}
	
}
