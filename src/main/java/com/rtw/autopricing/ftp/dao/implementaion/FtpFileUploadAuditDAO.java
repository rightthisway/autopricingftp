package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.FtpFileUploadAudit;

public class FtpFileUploadAuditDAO extends HibernateDAO<Integer, FtpFileUploadAudit> implements com.rtw.autopricing.ftp.dao.services.FtpFileUploadAuditDAO {
	
	public List<FtpFileUploadAudit> getAllFTPAuditsByUserName(String userName){
		
		if(userName.equalsIgnoreCase("All")){
			return find("FROM FtpFileUploadAudit order by lastUploadTime desc");
		}else{
			return find("FROM FtpFileUploadAudit WHERE userName = ? order by lastUploadTime desc", new Object[]{userName});
		}
		
		
	}
	
	public List<FtpFileUploadAudit> getAllFTPAuditsByUserNameAndStatus(String userName,String status){
		
		if(userName.equalsIgnoreCase("All")){
			return find("FROM FtpFileUploadAudit where status=? order by lastUploadTime desc", new Object[]{status});
		}else{
			return find("FROM FtpFileUploadAudit WHERE userName = ? and status=? order by lastUploadTime desc", new Object[]{userName,status});
		}
		
		
	}
	
	public List<FtpFileUploadAudit> getAllManualFTPAuditsByStatus(String status){
		
			return find("FROM FtpFileUploadAudit where status=? and uploadType='MANUAL' order by lastUploadTime desc", new Object[]{status});
		
	}
	
	public List<FtpFileUploadAudit> getAllManualFTPAudits(){
		
		return find("FROM FtpFileUploadAudit where uploadType='MANUAL' order by lastUploadTime desc");
	
}
}
