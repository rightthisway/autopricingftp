package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEvent;

public interface ZonePricingExchangeEventDAO extends RootDAO<Integer, ZonePricingExchangeEvent> {

	public ZonePricingExchangeEvent getZonePricingExchangeEventByEventId(Integer eventId)  throws Exception;
	Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	public Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	List<ZonePricingExchangeEvent> getZonePricingExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByEventIds(String eventIds)  throws Exception;
}
