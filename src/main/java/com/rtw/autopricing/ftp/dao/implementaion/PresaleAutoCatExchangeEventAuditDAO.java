package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEventAudit;

public class PresaleAutoCatExchangeEventAuditDAO extends HibernateDAO<Integer, PresaleAutoCatExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.PresaleAutoCatExchangeEventAuditDAO{

	
	
	public Collection<PresaleAutoCatExchangeEventAudit> getAllPresaleAutoCatExchangeEventAuditsByEventId(
			Integer eventId) {                          
		return find("FROM PresaleAutoCatExchangeEvent WHERE eventId=?", new Object[]{eventId});
		
	}

	public Collection<PresaleAutoCatExchangeEventAudit> getAllPresaleAutoCatExchangeEventAuditsByVenueId(
			Integer venueId) {
		// TODO Auto-generated method stub
		return find("FROM PresaleAutoCatExchangeEvent WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<PresaleAutoCatExchangeEventAudit> getAllPresaleAutoCatExchangeEventAuditsByArtistId(
			Integer artistId) {
		// TODO Auto-generated method stub
		return find("FROM PresaleAutoCatExchangeEvent WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<PresaleAutoCatExchangeEventAudit> getAllPresaleAutoCatExchangeEventAuditsByGrandChildCategoryId(
			Integer grandChildCategoryId) {
		// TODO Auto-generated method stub
		return find("FROM PresaleAutoCatExchangeEvent WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}

}