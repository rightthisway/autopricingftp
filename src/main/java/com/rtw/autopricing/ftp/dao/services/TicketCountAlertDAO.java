package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.TicketCountAlert;

public interface TicketCountAlertDAO extends RootDAO<Integer, TicketCountAlert> {
	public TicketCountAlert getTicketCountAlertByExchange(String exchange);

}
