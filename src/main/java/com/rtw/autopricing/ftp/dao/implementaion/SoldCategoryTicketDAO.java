package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.SoldCategoryTicket;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;


public class SoldCategoryTicketDAO extends HibernateDAO<Integer, SoldCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.SoldCategoryTicketDAO {

	public List<SoldCategoryTicket> getAllSoldCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM SoldCategoryTicket where eventId=?" ,new Object[]{eventId});
	}
}
