package com.rtw.autopricing.ftp.dao.services;

import java.util.Date;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;

public interface AutoCatsProjectAuditDAO  extends RootDAO<Integer,AutoCatsProjectAudit> {
	List<AutoCatsProjectAudit> getAllAutoCatsProjectAudits();
	List<AutoCatsProjectAudit> getAllAutoCatsProjectAuditsByProductBrokerAndDateRange(Integer productId,Integer brokerId,Date fromDate,Date toDate)throws Exception;
}
