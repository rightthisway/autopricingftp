package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;


public class AutopricingProductDAO extends HibernateDAO<Integer, AutopricingProduct> implements com.rtw.autopricing.ftp.dao.services.AutopricingProductDAO{

	public Collection<AutopricingProduct> getAllAutopricingProductExceptGivenId(Integer id) {
		return find("FROM AutopricingProduct WHERE id != ?" , new Object[]{id});
	}
	public Collection<AutopricingProduct> getAllAutopricingProductExceptGivenIdForExEventManagement(Integer id) {
		return find("FROM AutopricingProduct WHERE id != ? and status='ACTIVE' and name in('MiniCats','VIP MiniCats','LastRow MiniCats','Zoned LastRow MiniCats','ZonesPricing','RewardTheFan Listings'," +
				"'TNSpecial','RTWZonesPricing','MHZonePricing','VIPlastrowcats','PresaleZoneTicketsProcessor','SGLastFiveRow')" , new Object[]{id});
	}
	public Collection<AutopricingProduct> getAllAutopricingProductForMaangeScheduler() {
		return find("FROM AutopricingProduct WHERE status='ACTIVE' and name in('MiniCats','VIP MiniCats','LastRow MiniCats','SS Account','ZonesPricing','RewardTheFan Listings'," +//LarryLast','Zoned LastRow MiniCats
				"'AutoCats96','TNSpecial','RTWZonesPricing','MHZonePricing','VIPlastrowcats','PresaleZoneTicketsProcessor','SGLastFiveRow') " , new Object[]{});
	}
	public Collection<AutopricingProduct> getAllAutopricingProductForVenueSettings() {
		return find("FROM AutopricingProduct WHERE status='ACTIVE' and name in('MiniCats','VIP MiniCats','LastRow MiniCats','SS Account','ZonesPricing'," +////LarryLast','Zoned LastRow MiniCats
				"'AutoCats96','PresaleAutoCat','RTWZonesPricing','MHZonePricing','VIPlastrowcats','RewardTheFan Listings','PresaleZoneTicketsProcessor','SGLastFiveRow') " , new Object[]{});
	}

	public AutopricingProduct getAutopricingProductByName(String name) {
		List<AutopricingProduct> list = find("FROM AutopricingProduct WHERE name = ?" , new Object[]{name});
		if(list==null || list.isEmpty()){
			return null;
		}
		return list.get(0);
	}

	public List<String> getAutopricingProductInternalNotes() {
		return find("select DISTINCT internalNotes FROM AutopricingProduct WHERE internalNotes is not NULL");
	}
	public List<String> getRTFAutopricingProductInternalNotes() {
		return find("select DISTINCT internalNotes FROM AutopricingProduct WHERE name in ('RewardTheFan Listings','RTFLastRowCats')");
	}
	
	public Collection<AutopricingProduct> getAllActiveAutopricingProducts() {
		return find("FROM AutopricingProduct WHERE internalNotes is not NULL and status = 'ACTIVE' ");
	}

	public List<AutopricingProduct> getAllActiveProducts() {
		return find("FROM AutopricingProduct WHERE status = 'ACTIVE' ");
	}
}
