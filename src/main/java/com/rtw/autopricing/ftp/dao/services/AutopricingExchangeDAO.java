package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingExchange;


public interface AutopricingExchangeDAO extends RootDAO<Integer, AutopricingExchange>{
	
 public AutopricingExchange getAutopricingExchangeByName(String name);
 public List<AutopricingExchange> getAllActiveExchanges();
}
