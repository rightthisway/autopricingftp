package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutopricingVenueSettingsAudit;
import com.rtw.autopricing.ftp.data.ExcludeVenuesAudit;

public class AutopricingVenueSettingsAuditDAO extends HibernateDAO<Integer, AutopricingVenueSettingsAudit> implements com.rtw.autopricing.ftp.dao.services.AutopricingVenueSettingsAuditDAO{

	public List<AutopricingVenueSettingsAudit> getAutopricingVenueSettingsAuditList(Integer productId, Integer venueId) throws Exception  {
		return find("From AutopricingVenueSettingsAudit where productId = ? and venueId= ? order by lastUpdatedDate",new Object[]{productId,venueId});
	}


}
