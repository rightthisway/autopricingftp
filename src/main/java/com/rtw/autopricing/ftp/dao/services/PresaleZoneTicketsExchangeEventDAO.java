package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEvent;

public interface PresaleZoneTicketsExchangeEventDAO extends RootDAO<Integer, PresaleZoneTicketsExchangeEvent> {

	PresaleZoneTicketsExchangeEvent getPresaleZoneTicketsProcessorExchangeEventByEventId(Integer eventId) throws Exception;
	Collection<PresaleZoneTicketsExchangeEvent> getAllZoneTicketsProcessorExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<PresaleZoneTicketsExchangeEvent> getAllZoneTicketsProcessorExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<PresaleZoneTicketsExchangeEvent> getAllZoneTicketsProcessorExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	Collection<PresaleZoneTicketsExchangeEvent> getAllPresaleZoneTicketsExchangeEventsEligibleForUpdate(Long minute,Integer productId) throws Exception;
	List<PresaleZoneTicketsExchangeEvent> getPresaleZoneTicketsExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<PresaleZoneTicketsExchangeEvent> getAllZoneTicketsProcessorExchangeEventsofDeletedTmatEvents() throws Exception;
	public PresaleZoneTicketsExchangeEvent getZoneTicketsProcessorExchangeEventsByEventId(Integer eventId) throws Exception;
}
