package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.FtpUserFileInformation;

public class FtpUserFileInformationDAO extends HibernateDAO<Integer, FtpUserFileInformation > implements com.rtw.autopricing.ftp.dao.services.FtpUserFileInformationDAO{

	public void deleteAll(Integer accountNo) {
	     /* Session session=getSession();
	      String sql="delete from ftp_user_file_information where account_no="+accountNo;
	      SQLQuery sqlQuery = session.createSQLQuery(sql);
	      sqlQuery.executeUpdate();*/
		  bulkUpdate("DELETE FROM FtpUserFileInformation WHERE account_no=?", new Object[]{accountNo});
		
	}

	public void deleteFtpUserFileByIdAndSource(String id, String source) {
		  Integer accountNo=Integer.parseInt(id);
		  try{
		//  bulkUpdate("DELETE FROM FtpUserFileInformation WHERE  account_no=? AND source=?", new Object[]{accountNo,source});	
			  //source=source.replace('', 'c');
			  Session session=getSession();
		      String sql="delete from ftp_user_file_information where account_no="+accountNo+" and source='"+source+"'";
		      SQLQuery sqlQuery = session.createSQLQuery(sql);
		      sqlQuery.executeUpdate();
		  }catch(Exception e){
			  e.printStackTrace();
		  }
	}
	
	public List<FtpUserFileInformation> getFtpUserFileInformationById(
			Integer accountNo) {
		return 	find("FROM FtpUserFileInformation WHERE account_no=?",new Object[]{accountNo});
	}

}
