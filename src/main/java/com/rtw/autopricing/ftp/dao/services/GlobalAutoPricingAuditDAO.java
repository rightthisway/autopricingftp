package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;

public interface GlobalAutoPricingAuditDAO extends RootDAO<Integer, GlobalAutoPricingAudit> {

	public List<GlobalAutoPricingAudit> getAllAuditsOrderByCreatedDate();
}
