package com.rtw.autopricing.ftp.dao.implementaion;



import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.ftp.data.LarryLastCategoryTicket;
import com.rtw.autopricing.ftp.data.ZonedLastRowMiniCategoryTicket;
import com.rtw.autopricing.util.AutoExchangeEventLoader;



public class ZonedLastRowMiniCategoryTicketDAO extends HibernateDAO<Integer, ZonedLastRowMiniCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.ZonedLastRowMiniCategoryTicketDAO {

	public void deleteAll(Collection<ZonedLastRowMiniCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<ZonedLastRowMiniCategoryTicket> getAllActiveZonedLastRowMiniCategoryTickets()  throws Exception {
		return find("FROM ZonedLastRowMiniCategoryTicket where status ='ACTIVE'");
	}
	
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsByAll(Integer eventId, String section, String row, String quantity)  throws Exception {
		String query = "FROM ZonedLastRowMiniCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND lastRow=?";
			param.add(row);
		}
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(Integer.parseInt(quantity));								
		
			}
		
		return find(query, param.toArray());
	}
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsById(Integer Id)  throws Exception {
		return find("FROM ZonedLastRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id)  throws Exception {
		return find(" select priceHistory FROM ZonedLastRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM ZonedLastRowMiniCategoryTicket where eventId=? and status = ?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<ZonedLastRowMiniCategoryTicket> getAllTNZonedLastRowMiniCategoryTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM ZonedLastRowMiniCategoryTicket where eventId=? and status = ? and tnTicketGroupId is not null and tnTicketGroupId <> 0" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<ZonedLastRowMiniCategoryTicket> getAllTNZonedLastRowMiniCategoryTicketsByEventIdByBrokerID(Integer eventId,Integer brokerId) {
		return find("FROM ZonedLastRowMiniCategoryTicket where eventId=? and status = ? and tnBrokerId = ? " ,new Object[]{eventId,"ACTIVE",brokerId});
	}
	
	public Integer deleteAllTnZonedLastRowMiniCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,int excludeEventDays)  throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
		" FROM zone_lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
	    " inner join event e on e.id=tg.event_id" +
	    " WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_ticket_group_id > 0 AND tg.status='ACTIVE' AND e.event_status='ACTIVE' " +
		" AND datediff(HOUR,GETDATE(),event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllZonedLastRowMiniCategoryTicketswithinExcludeEventDays(int excludeEventDays)  throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
	    " FROM zone_lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
	    " inner join event e on e.id=tg.event_id" +
	    " WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllTnZonedLastRowMiniCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId)  throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for TN' " +
		" FROM zone_lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_ticket_group_id > 0 AND tg.status='ACTIVE' AND event_id NOT IN (" +
	    " SELECT event_id FROM zone_lastrow_mini_exchange_event le WITH(NOLOCK)" +
	    " inner join event e on e.id=le.event_id " +
	    " inner join venue_category vc on vc.id=e.venue_category_id" +
	    " where e.event_status='ACTIVE' AND status='ACTIVE' AND le.tn_broker_id ="+brokerId+" AND vc.id is not null ) ";//AND vc.category_group !='PRESALE'
		
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	     
	}
	
	public Integer updateCategoryTicketReasonByTnTicketGroupIandBrokerId(Integer tnTicketGroupId,Integer brokerId,String reason) throws Exception {
		
		String sql =" UPDATE tg set tg.reason='"+reason+"' " +
		" FROM zone_lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_ticket_group_id ="+tnTicketGroupId+" AND tg.tn_broker_id="+brokerId+"";

		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
public Integer deleteAllZonedLastRowMiniCategoryTicketsnotExistinExchangeEventforAnyExchange(Integer brokerId)  throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for all Exchanges' " +
		" FROM zone_lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.status='ACTIVE' AND  tn_broker_id="+brokerId+" AND event_id NOT IN (" +
	    " SELECT event_id FROM zone_lastrow_mini_exchange_event le WITH(NOLOCK)" +
	    " inner join event e on e.id=le.event_id " +
	    " inner join venue_category vc on vc.id=e.venue_category_id" +
	    " where e.event_status='ACTIVE' AND status='ACTIVE' AND vc.id is not null AND e.venue_id is not null ";//AND vc.category_group !='PRESALE'
	   // " AND (le.tn_broker_id is not null OR le.vivid_broker_id is not null OR le.scorebig_broker_id is not null OR le.fanxchange_broker_id is not null OR le.ticketcity_broker_id is not null)) ";
	    if(brokerId.equals(10)){//RTW-2	/*Reserve One Tickets*/
			sql += " AND le.rot_pos_enabled > 0";
		} else if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//RTW-2	/*Reserve One Tickets*/
			sql += " AND le.tixcity_pos_enabled > 0";
		} else if(brokerId == 5){	/*RTW*/
			sql += " AND le.rtw_pos_enabled > 0";
		}
		sql += ")";
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}

	public Integer deleteAllZoneLastRowTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct)  throws Exception {
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Ticket not Exist in TN' " +
		" FROM zone_lastrow_minicat_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_broker_id="+broker.getId()+" AND tg.tn_ticket_group_id > 0 AND tg.status='ACTIVE' " +
		" and tg.tn_ticket_group_id not in(select distinct ticket_group_id from "+serverName+".ticket_group WITH(NOLOCK) " +
		" where internal_notes='"+autopricingProduct.getInternalNotes()+"')";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInZonedLastRowMiniCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct)  throws Exception {
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql = " SELECT ctg.ticket_group_id as groupId " +
					" FROM " + serverName +".ticket_group ctg WITH(NOLOCK) " + 
					 " INNER JOIN " + serverName + ".ticket ct ON ct.ticket_group_id = ctg.ticket_group_id " +
					 " LEFT JOIN " + serverName + ".invoice i ON i.invoice_id = ct.invoice_id " +
					 " WHERE ctg.ticket_group_id NOT IN (select distinct tn_ticket_group_id from zone_lastrow_minicat_category_ticket WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND tn_broker_id="+broker.getId()+" AND tn_ticket_group_id is not null) AND ctg.internal_notes = '"+autopricingProduct.getInternalNotes()+"' " +
					 " and datediff(minute,ctg.create_date,GETDATE())>15" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 " GROUP BY ctg.ticket_group_id " +
					 " HAVING SUM(CASE WHEN i.invoice_id IS NOT NULL THEN 1 ELSE 0 END )=0";
		
		Session session=null;
		
		List<Integer> ticketGroupIdList =  new ArrayList<Integer>();
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			/*if(list != null || list.size() > 0) {
				for (Object[] obj : list) {
					ticketGroupIdList.add(Integer.parseInt(obj[0].toString()));
				}
			}
			return ticketGroupIdList;*/
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	}
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsByPosTicketGroupIds(List<Integer> ticektGroupIds,Integer brokerId)  throws Exception {
		
		List<ZonedLastRowMiniCategoryTicket> result = new ArrayList<ZonedLastRowMiniCategoryTicket>();
		try {
			Map<Integer,String> map = new HashMap<Integer, String>();
			String tnTicketGroupIdsStr = "";
			int i = 0;
			int count =0;
			for(Integer id:ticektGroupIds){
				if(count==2000){
					count =0;
					i++;
					map.put(i, tnTicketGroupIdsStr);
					tnTicketGroupIdsStr ="";
				}
				tnTicketGroupIdsStr = tnTicketGroupIdsStr + id + ",";
				count++;
			}
			if(!tnTicketGroupIdsStr.isEmpty()){
				map.put(++i, tnTicketGroupIdsStr);
			}
			for(Integer ii:map.keySet()){
				tnTicketGroupIdsStr = map.get(ii);
				if(!tnTicketGroupIdsStr.isEmpty()){
					tnTicketGroupIdsStr = tnTicketGroupIdsStr.substring(0, tnTicketGroupIdsStr.length()-1);
				}
				Collection<ZonedLastRowMiniCategoryTicket> temp = find("FROM ZonedLastRowMiniCategoryTicket where tnBrokerId="+brokerId+" and tnTicketGroupId IN ("  + tnTicketGroupIdsStr + ")" +
						" and status<>'SOLD'"); 
				if(temp!=null){
					result.addAll(temp);
				}
			}
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//return result;
	}
}

