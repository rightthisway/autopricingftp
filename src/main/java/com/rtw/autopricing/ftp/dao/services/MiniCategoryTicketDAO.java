package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;



public interface MiniCategoryTicketDAO extends RootDAO<Integer, MiniCategoryTicket> {

	public Collection<MiniCategoryTicket> getAllActiveTgCatsCategoryTickets() throws Exception;
	public List<MiniCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	
	public List<MiniCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<MiniCategoryTicket> getAllMiniCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInMiniCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<MiniCategoryTicket> getAllTNMiniCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllMiniCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllMiniCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnMiniCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnMiniCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<MiniCategoryTicket> getAllMiniCatsCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
}

