package com.rtw.autopricing.ftp.dao.implementaion;

import com.rtw.autopricing.ftp.data.Role;


public class RoleDAO extends HibernateDAO<Integer, Role> implements com.rtw.autopricing.ftp.dao.services.RoleDAO {

	public Role getRoleByName(String name) {
		return findSingle("FROM Role WHERE name = ?", new Object[]{name});
	}
}
