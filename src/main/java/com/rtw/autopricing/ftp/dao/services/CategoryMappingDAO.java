package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.CategoryMapping;
import com.rtw.autopricing.ftp.enums.EventStatus;

public interface CategoryMappingDAO extends RootDAO<Integer, CategoryMapping> {
	List<CategoryMapping> getAllCategoryMappingsByVenueCategoryId(Integer venueCategoryId) throws Exception;
	List<CategoryMapping> getAllCategoryMappingsByCategoryId(Integer categoryId) throws Exception;
	List<String> getAllCategoryGroupNamesByTourIdByVenueCategoryId(Integer tourId,EventStatus eventStatus) throws Exception;
	
}