package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZones;
import com.rtw.autopricing.ftp.data.ExcludeVenues;

public class ExcludeVenueCategoryZonesDAO extends HibernateDAO<Integer, ExcludeVenueCategoryZones> implements com.rtw.autopricing.ftp.dao.services.ExcludeVenueCategoryZonesDAO{

	
	public List<ExcludeVenueCategoryZones> getExcludeVenueCategoryZonesByProductId(Integer productId)  throws Exception{
		return find("FROM ExcludeVenueCategoryZones where productId=? and status='ACTIVE'",new Object[]{productId});
				
	}
	public List<ExcludeVenueCategoryZones> getExcludeVenueCategoryZonesByBrokerIdandProductIds(Integer brokerId,String productIdStr)  throws Exception{
		return find("FROM ExcludeVenueCategoryZones where brokerId=? and status='ACTIVE' and productId in("+productIdStr+") ",new Object[]{brokerId});
				
	}
	public List<ExcludeVenueCategoryZones> getExcludeVenueCategoryZonesByProductIdandBrokerId(Integer productId,Integer brokerId)  throws Exception{
		return find("FROM ExcludeVenueCategoryZones where productId=? and brokerId=? and status='ACTIVE'",new Object[]{productId,brokerId});
				
	}
	public List<ExcludeVenueCategoryZones> getAllExcludeVenueCategoryZonesByVenueCategoryId(Integer venueCategoryId)  throws Exception{
		return find("FROM ExcludeVenueCategoryZones where venueCategoryId=? ",new Object[]{venueCategoryId});
				
	}
	public List<ExcludeVenueCategoryZones> getExcludeVenueCategoryZonesByProductIdAndBrokerIdAndVenueCategoryId(
			Integer productId, Integer brokerId, Integer venueCategoryId)  throws Exception{
		return find("FROM ExcludeVenueCategoryZones where venueCategoryId=? AND productId=? AND brokerId=? and status='ACTIVE'",new Object[]{venueCategoryId,productId,brokerId});
				
	}



}
