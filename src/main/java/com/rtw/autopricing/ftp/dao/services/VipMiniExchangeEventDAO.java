package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEvent;

public interface VipMiniExchangeEventDAO extends RootDAO<Integer, VipMiniExchangeEvent>{

	public VipMiniExchangeEvent getVipMiniExchangeEventByEventId(Integer eventId) throws Exception;
	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByVenueId(Integer venueId) throws Exception;
	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByArtistId(Integer artistId) throws Exception;
	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	public List<VipMiniExchangeEvent> getVipMiniExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<VipMiniExchangeEvent> getAllVipMiniExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByEventIds(String eventIds)  throws Exception;
}
