package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEvent;

public interface SGLastFiveRowExchangeEventDAO extends RootDAO<Integer, SGLastFiveRowExchangeEvent>{

	public SGLastFiveRowExchangeEvent getSGLastFiveRowExchangeEventByEventId(Integer eventId)  throws Exception;
	public Collection<SGLastFiveRowExchangeEvent> getAllSGLastFiveRowExchangeEventsByArtistId(Integer artistId) throws Exception;
	public Collection<SGLastFiveRowExchangeEvent> getAllSGLastFiveRowExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	public Collection<SGLastFiveRowExchangeEvent> getAllSGLastFiveRowExchangeEventsByVenueId(Integer venueId) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	public Collection<SGLastFiveRowExchangeEvent> getAllSGLastFiveRowExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	public List<SGLastFiveRowExchangeEvent> getSGLastFiveRowExchangeEventByVenueAndExchangeBrokerId(Integer venueId,Integer brokerId) throws Exception;
	public List<SGLastFiveRowExchangeEvent> getAllSGLastFiveRowExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<SGLastFiveRowExchangeEvent> getAllSGLastFiveRowExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<SGLastFiveRowExchangeEvent> getAllSGLastFiveRowExchangeEventsByEventIds(String eventIds)  throws Exception;
}
