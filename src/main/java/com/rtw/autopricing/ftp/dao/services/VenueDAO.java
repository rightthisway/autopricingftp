package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.Venue;


public interface VenueDAO extends RootDAO<Integer, Venue> {
	Collection<Venue> getVenues(String name);
	Venue getVenue(String name);
	Venue getVenueByStubhubId(Integer stubhubId);
	void deleteEmptyVenues();
	Collection<Venue> filterByVenue(String pattern);
	Collection<Venue> filterVenue(String venue,String city, String state, String country, String venueType);
	Collection<Venue> getVenueByTour(Integer tourId);
	Collection<Venue> getVenueById(Integer id);
	public void deleteVenue(Integer venueId);	
	Collection<Venue> getVenuesWithActiveEvents(String pattern);	
	Collection<Venue> getVenuesForArtistWithActiveEvents(String artist);
	
}