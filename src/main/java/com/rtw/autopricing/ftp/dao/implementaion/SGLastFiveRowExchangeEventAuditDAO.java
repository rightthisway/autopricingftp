package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEventAudit;

public class SGLastFiveRowExchangeEventAuditDAO extends HibernateDAO<Integer, SGLastFiveRowExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.SGLastFiveRowExchangeEventAuditDAO{

	public Collection<SGLastFiveRowExchangeEventAudit> getAllSGLastFiveRowExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM SGLastFiveRowExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}
}
