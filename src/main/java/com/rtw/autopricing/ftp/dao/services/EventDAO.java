package com.rtw.autopricing.ftp.dao.services;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.enums.EventStatus;


public interface EventDAO extends RootDAO<Integer, Event> {
	
	public List<Event> getAllActiveEventsWithParentType(String parentType) throws Exception;
//	Collection<Event> getAllEventsByTour(int tourId) throws Exception;
//	public Collection<Event> getAllEventsByTour(int tourId,Integer brokerId) throws Exception;
	Collection<Event> getAllActiveEvents() throws Exception;
	Collection<Event> getAllEventsByStatus(EventStatus eventStatus) throws Exception;
	Collection<Event> getAllEventsByStatusAndName(EventStatus eventStatus) throws Exception;
	Collection<Event> getAllEventsByVenue(int venueId) throws Exception;
	public Collection<Event> getAllEventByAdmitoneIdFromUSandCA()  throws Exception;
	public List<Integer> getAllEventsAdmitoneIdFromUSandCA() throws Exception;
	Collection<Event> getAllEventsByVenue(int venueId,Integer brokerId) throws Exception;
	Collection<Event> getAllEventsActiveandExpiredByVenue(int venueId) throws Exception;
	Collection<Event> getAllEventsByVenueAndStatus(int venueId, EventStatus eventStatus) throws Exception;
	Collection<Event> getAllEventsByNameExcerpt(String excerpt, EventStatus eventStatus) throws Exception;
	public Collection<Event> getAllEventsByNameBrokerId(String excerpt, EventStatus eventStatus,Integer brokerId) throws Exception;
	Collection<Event> getAllEventsByDates(Date startDate, Date endDate, EventStatus eventStatus) throws Exception;
	Collection<Event> getAllEventsByGrandChildCategoryAndDateRange(Date startDate, Date endDate,Integer grandChildCategoryId, EventStatus eventStatus) throws Exception;
	Collection<Event> getAllEventsByVenueAndDates(int venueId, Date startDate, Date endDate) throws Exception;
//	Collection<Event> getAllEventsByTourVenueAndDates(int tourId, int venueId, Date startDate, Date endDate) throws Exception;
//	Collection<Event> getAllEventsByTourVenue(int tourId, int venueId) throws Exception;
//	Collection<Event> getAllEventsByTourIdAndCategoryGroup(int tourId, String categoryGroup) throws Exception;
//	Date getMinDateFromEvents(Integer tourId) throws Exception;
//	Date getMaxDateFromEvents(Integer tourId) throws Exception;
	Event getEventByStubhubId(Integer stubhubId) throws Exception;
	Event getEventByAdmitoneId(Integer admitoneId) throws Exception;
	Collection<Event> getAllActiveEventsByArtistId(Integer artistId) throws Exception;
	Collection<Event> getAllActiveEventsByArtistIdAndBrokerId(Integer artistId,Integer brokerId) throws Exception;
	Collection<Event> getAllActiveEventsByArtistIds(List<Integer> artistIds) throws Exception;
	Collection<Event> getAllEventsNotInStubhub() throws Exception;
	Collection<Event> getAllEventsByIds(List<Integer> eventId) throws Exception;
	Collection<Event> getEvent(String name, int venueId, Date date, Time time) throws Exception;
	Collection<Event> searchEvents(String name, java.sql.Date date, Time time, String location) throws Exception;
	Integer getUnusedId() throws Exception;
    void deleteById(Integer id) throws Exception;
//    void updateEventTicketValues(Event event) throws Exception;
//    void updateAllEventTicketValues() throws Exception;
    Collection<Event> getEmptyEvents() throws Exception;
    void deleteEmptyEvents() throws Exception;
    void delete(Event event) ;
//	Event getEventWithSameTourAndVenueButDifferentFromEvent(Integer tourId, Integer venueId, Integer eventId) throws Exception;
	int getEventCount() throws Exception;
	int getMaxEventId() throws Exception;
//	int getEventCountByTour(Integer tourId) throws Exception;
	Collection<Event> getAllEventsByNameAndDate(String name, Date dateAndTime) throws Exception;
	Event.EventTicketStat getNonDuplicateEventTicketStat(int eventId) throws Exception;
	
	public Collection<Event> getAllActiveZoneEvents() throws Exception;
//	public Collection<Event> getAllActiveZoneEventsByTour(Integer tourId) throws Exception;
	public Collection<Event> getAllEventByAdmitoneId() throws Exception;
	public Collection<Event> getAllEventByAdmitoneIdNull() throws Exception;
	Event getEventWithSameArtistAndVenueAndNameAndDateTBDAndTimeTBD(Integer artistId, Integer venueId, String eventName) throws Exception;
	Collection<Event> getTBDEvents(String firstLetter) throws Exception;
	List<Event> getToBeAutoCorrectEvents() throws Exception;
	List<Event> getAllEventsByVenueCategoryId(Integer venueCategoryId) throws Exception;
	public List<Event> getAllActiveEventsByVenueCategoryId(Integer venueCategoryId)  throws Exception;
	public boolean unLinkAdmintoneEvent(Integer eventId) throws Exception;
	int getEventCountSport() throws Exception;
	int getEventCountConcerts() throws Exception;
	int getEventCountDefault() throws Exception;
	int getEventCountTheater() throws Exception;
	int getEventCountLasVegas() throws Exception;
	int getEventCountOther() throws Exception;
	public Collection<Event> getAllEventsByVenueOrderdByEventName(int venueId) throws Exception;
	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventName(Integer grandChildCategoryId) throws Exception;
	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventDate(Integer grandChildCategoryId) throws Exception;
	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventDate(Integer grandChildCategoryId,Integer brokerId) throws Exception;
	public Collection<Event> getAllEventsByChildCategory(Integer ChildCategoryId,Integer brokerId) throws Exception;
	public Collection<Event> getAllEventsByChildCategory(Integer ChildCategoryId) throws Exception;
	public void updateBrokerEvents(ArrayList<Integer> eventIds,Integer brokerId,String brokerStatus) throws Exception;
	public void updateBrokerEvents(ArrayList<Integer> eventIds) throws Exception;
	public void updateBrokerEventsBrowseLite(ArrayList<Integer> eventIds,String brokerStatus) throws Exception;
	public void deleteEventsByArtistId(int artistId) throws Exception;
	public Collection<Event> getAllEventsByArtistVenueAndDates(int artistId, int venueId, Date startDate, Date endDate) throws Exception;
	public List<Event> getAllActiveEventsWithParentId() throws Exception;
	public Collection<Event> getAllEventsByArtistAndVenueAndStatus(
			int artistId, int venueId, EventStatus active) throws Exception;
	public Integer getParentCategoryIdByEventId(Integer eventId) throws Exception;
	public Collection<Event> getAllEventsByAdmitoneIds(String admitoneIds) throws Exception;
	public Collection<Event> getAllEventsByeventIds(String eventIds) throws Exception;
} 
