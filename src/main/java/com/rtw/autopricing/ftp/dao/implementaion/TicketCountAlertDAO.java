package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.TicketCountAlert;

public class TicketCountAlertDAO extends HibernateDAO<Integer, TicketCountAlert> implements com.rtw.autopricing.ftp.dao.services.TicketCountAlertDAO{

	public TicketCountAlert getTicketCountAlertByExchange(String exchange) {
		List<TicketCountAlert> list = find("FROM TicketCountAlert WHERE exchange = ? ", new Object[]{exchange});
		if(list==null || list.isEmpty()){
			return null;
		}
		return list.get(0);
	}

}
