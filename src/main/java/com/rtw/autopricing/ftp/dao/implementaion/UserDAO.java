package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.User;
import com.rtw.autopricing.ftp.data.UserBrokerDetails;


public class UserDAO extends HibernateDAO<Integer, User> implements com.rtw.autopricing.ftp.dao.services.UserDAO {
	public User getUserByUsernameOrEmail(String username, String email) {
		Collection<User> list = find("FROM User WHERE username=? OR email=?", new Object[]{username, email});
		if (list.size() == 0) {
			return null;
		}
		
		return list.iterator().next();
	}	

	public void removePasswordFromIdleUser(Integer period) {
		bulkUpdate("UPDATE User SET password=null WHERE lastLogin<? AND (SELECT COUNT(s.id) FROM Snipe s, SnipeList l WHERE s.snipeListId=l.id AND l.username=?) > 0)", new Object[]{});
	}
	
	public boolean checkCode(String username, String code) {
		Collection<User> list = find("FROM User WHERE username=? AND code=?", new Object[]{username, code});
		if (list.size() == 0) {
			return false;
		}
		
		return true;
	}
	
	public int getUserCount() {
		List list = find("SELECT count(id) FROM User");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}

	public Collection<User> getAllUsersByRole(String role) {
		Collection<User> result = new ArrayList<User>();
		for (User user: getAll()) {
			if (user.hasRole(role)) {
				result.add(user);
			}
		}
		
		return result;
	}
	
	
	/*public Collection<User> getUsersByBrokerid(String brokerid) {
		Collection<User> sub_result = new ArrayList<User>();
		Collection<User> result = new ArrayList<User>();
		sub_result=getAll();
		for (User user: sub_result) {
			Integer brok1=user.getBrokerId();
			Integer brok2=Integer.parseInt(brokerid);
			int x=brok1.compareTo(brok2);
//			System.out.println(brok1);
//			System.out.println(brok2);
//			System.out.println(x);
			if(x==0)
			{
//				System.out.println(user.getBrokerId());
//				System.out.println(user);
				result.add(user);
			}
		}
		
		return result;
	}*/
	
	public Collection<User> getUsersByBrokerId(Integer brokerId) {
		
		return find("From User where id  IN (select user.id from UserBrokerDetails where broker_id=?)", new Object[]{brokerId});
		// return find("From User WHERE broker_id=?", new Object[]{brokerId});
	}
	
	public Collection<User> getBrowseLiteUsersByBrokerId(Integer brokerId) {
		 Collection<User> users =  find("From User WHERE brokerId=? AND ", new Object[]{brokerId});
		/* Collection<User> result = new ArrayList<User>();
		 for(User user:users){
			 for(Role role: user.getRoles()){
				 
			 }
		 }*/
		 return users;
	}
		public User getUserByUsername(String username) {
		Collection<User> list = find("FROM User WHERE username=?", new Object[]{username});
		if (list.size() == 0) {
			return null;
		}
		
		return list.iterator().next();
	}	

	public List<String> getBrokers(){
		
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("select first_name+' '+last_name from admitone_users where broker = true");
			List<String> brokers = query.list();
			return brokers;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;		
	}
	
	public User getUserByUsernameAndBrokerid(String username , String brokerid)
	{
		try
		{
	//	Collection<User> user = find("From User WHERE username=? AND broker_id=?", new Object[]{username,brokerid});
			Collection<User> user=	find("From User WHERE username=?", new Object[]{username});
			Collection<UserBrokerDetails> userBrokerDetails=find("From UserBrokerDetails WHERE broker_id=?", new Object[]{brokerid});
			
	      for (UserBrokerDetails userBrokerDetail : userBrokerDetails) {
			if(user.iterator().next().getId().equals(userBrokerDetail.getUser().getId())){
				return user.iterator().next();
			}
		}
		if(user.size() ==0){
			user = find("From User WHERE username=? AND multibroker_access=1", new Object[]{username});
		}
		if(user.size() ==0)
		{
			return null;
		}
		
		return user.iterator().next();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
