package com.rtw.autopricing.ftp.dao.implementaion;

import com.rtw.autopricing.ftp.dao.services.AdmitoneInventoryDAO;
import com.rtw.autopricing.ftp.dao.services.ArtistDAO;
import com.rtw.autopricing.ftp.dao.services.AutoPricingErrorDAO;
import com.rtw.autopricing.ftp.dao.services.AutopricingExchangeDAO;
import com.rtw.autopricing.ftp.dao.services.AutopricingProductDAO;
import com.rtw.autopricing.ftp.dao.services.AutopricingSettingsAuditDAO;
import com.rtw.autopricing.ftp.dao.services.AutopricingSettingsDAO;
import com.rtw.autopricing.ftp.dao.services.AutopricingVenueSettingsAuditDAO;
import com.rtw.autopricing.ftp.dao.services.AutopricingVenueSettingsDAO;
import com.rtw.autopricing.ftp.dao.services.BrokerAuditDAO;
import com.rtw.autopricing.ftp.dao.services.BrokerDAO;
import com.rtw.autopricing.ftp.dao.services.ChildCategoryDAO;
import com.rtw.autopricing.ftp.dao.services.DefaultPurchasePriceDAO;
import com.rtw.autopricing.ftp.dao.services.DuplicateTicketMapDAO;
import com.rtw.autopricing.ftp.dao.services.EventDAO;
import com.rtw.autopricing.ftp.dao.services.ExcludeEventZonesAuditDAO;
import com.rtw.autopricing.ftp.dao.services.ExcludeEventZonesDAO;
import com.rtw.autopricing.ftp.dao.services.ExcludeVenueCategoryZonesAuditDAO;
import com.rtw.autopricing.ftp.dao.services.ExcludeVenueCategoryZonesDAO;
import com.rtw.autopricing.ftp.dao.services.FtpLocationSettingsDAO;
import com.rtw.autopricing.ftp.dao.services.FtpUserAccountDAO;
import com.rtw.autopricing.ftp.dao.services.GlobalAutoPricingAuditDAO;
import com.rtw.autopricing.ftp.dao.services.GrandChildCategoryDAO;
import com.rtw.autopricing.ftp.dao.services.LarryLastExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.LarryLastExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.LastRowMiniExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.LastRowMiniExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.ManagePurchasePriceDAO;
import com.rtw.autopricing.ftp.dao.services.ManualCategoryTicketDAO;
import com.rtw.autopricing.ftp.dao.services.MiniExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.MiniExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.ParentCategoryDAO;
import com.rtw.autopricing.ftp.dao.services.PresaleZoneTicketsCategoryTicketDAO;
import com.rtw.autopricing.ftp.dao.services.PresaleZoneTicketsExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.PresaleZoneTicketsExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.PresaleZoneTicketsLockedTicketEventDetailsDAO;
import com.rtw.autopricing.ftp.dao.services.PrivilegeDAO;
import com.rtw.autopricing.ftp.dao.services.PropertyDAO;
import com.rtw.autopricing.ftp.dao.services.RTFLastRowCategoryTicketDAO;
import com.rtw.autopricing.ftp.dao.services.RTFLastRowExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.RoleDAO;
import com.rtw.autopricing.ftp.dao.services.SGLastFiveRowCategoryTicketDAO;
import com.rtw.autopricing.ftp.dao.services.SGLastFiveRowExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.SGLastFiveRowExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.SeatGeekUploadCatsDAO;
import com.rtw.autopricing.ftp.dao.services.SeatGeekWSTrackingDAO;
import com.rtw.autopricing.ftp.dao.services.SoldCategoryTicketDAO;
import com.rtw.autopricing.ftp.dao.services.StubHubApiTrackingDAO;
import com.rtw.autopricing.ftp.dao.services.TMATInduxEventDetailsAuditDAO;
import com.rtw.autopricing.ftp.dao.services.TMATInduxEventDetailsDAO;
import com.rtw.autopricing.ftp.dao.services.TNDRealCategoryTicketDAO;
import com.rtw.autopricing.ftp.dao.services.TNDZonesCategoryTicketDAO;
import com.rtw.autopricing.ftp.dao.services.TicketCountAlertDAO;
import com.rtw.autopricing.ftp.dao.services.TicketDAO;
import com.rtw.autopricing.ftp.dao.services.TicketNetworkSSAccountExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.TicketNetworkSSAccountExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.TourCategoryDAO;
import com.rtw.autopricing.ftp.dao.services.UnbroadcastedEventsDAO;
import com.rtw.autopricing.ftp.dao.services.UserBrokerDetailsDAO;
import com.rtw.autopricing.ftp.dao.services.UserDAO;
import com.rtw.autopricing.ftp.dao.services.VenueCategoryDAO;
import com.rtw.autopricing.ftp.dao.services.VenueDAO;
import com.rtw.autopricing.ftp.dao.services.VipMiniExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.VipMiniExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.ZoneLastRowMiniCatsErrorDAO;
import com.rtw.autopricing.ftp.dao.services.ZoneLastRowMiniExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.ZoneLastRowMiniExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.ZonePricingExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.ZonePricingExchangeEventDAO;
import com.rtw.autopricing.ftp.dao.services.ZoneTicketsLockedTicketEventDetailsDAO;
import com.rtw.autopricing.ftp.dao.services.ZoneTicketsProcessorCategoryTicketDAO;
import com.rtw.autopricing.ftp.dao.services.ZoneTicketsProcessorExchangeEventAuditDAO;
import com.rtw.autopricing.ftp.dao.services.ZoneTicketsProcessorExchangeEventDAO;

public class DAORegistry {
	private static PropertyDAO propertyDAO;
	
	private static FtpUserAccountDAO ftpUserAccountDAO;
	private static FtpFileUploadAuditDAO ftpFileUploadAuditDAO;
	private static FtpLocationSettingsDAO ftpLocationSettingsDAO;
	
	private static ArtistDAO artistDAO;
	private static BrokerDAO brokerDAO;
	private static VenueDAO venueDAO;
	private static ParentCategoryDAO parentCategoryDAO;
	private static ChildCategoryDAO childCategoryDAO;
	private static GrandChildCategoryDAO grandChildCategoryDAO;
	private static EventDAO eventDAO;
	private static VenueCategoryDAO venueCategoryDAO;
	private static AutopricingProductDAO autopricingProductDAO;
	private static AutopricingExchangeDAO autopricingExchangeDAO;
	private static AutopricingSettingsDAO autopricingSettingsDAO;
	private static AutopricingSettingsAuditDAO autopricingSettingsAuditDAO;
	private static DefaultAutoPricingPropertiesDAO defaultAutoPricingPropertiesDAO;
	private static DefaultAutoPricingPropertiesAuditDAO defaultAutoPricingPropertiesAuditDAO;
	private static MiniExchangeEventDAO miniExchangeEventDAO;
	private static MiniExchangeEventAuditDAO miniExchangeEventAuditDAO;
	private static LarryLastExchangeEventDAO larryLastExchangeEventDAO;
	private static LarryLastExchangeEventAuditDAO larryLastExchangeEventAuditDAO;
	private static ZoneTicketsProcessorExchangeEventDAO zoneTicketsProcessorExchangeEventDAO;
	private static ZoneTicketsProcessorExchangeEventAuditDAO zoneTicketsProcessorExchangeEventAuditDAO;
	private static ZoneTicketsProcessorCategoryTicketDAO zoneTicketsProcessorCategoryTicketDAO;
	private static PresaleZoneTicketsExchangeEventDAO presaleZoneTicketsExchangeEventDAO;
	private static PresaleZoneTicketsExchangeEventAuditDAO presaleZoneTicketsExchangeEventAuditDAO;
	private static PresaleZoneTicketsCategoryTicketDAO presaleZoneTicketsCategoryTicketDAO;
	private static ZoneTicketsLockedTicketEventDetailsDAO zoneTicketsLockedTicketEventDetailsDAO;
	private static PresaleZoneTicketsLockedTicketEventDetailsDAO presaleZoneTicketsLockedTicketEventDetailsDAO;
	private static TicketNetworkSSAccountExchangeEventDAO ticketNetworkSSAccountExchangeEventDAO;
	private static TicketNetworkSSAccountExchangeEventAuditDAO ticketNetworkSSAccountExchangeEventAuditDAO;
	
	private static VipMiniExchangeEventDAO vipMiniExchangeEventDAO;
	private static VipMiniExchangeEventAuditDAO vipMiniExchangeEventAuditDAO;
	private static LastRowMiniExchangeEventDAO lastRowMiniExchangeEventDAO;
	private static LastRowMiniExchangeEventAuditDAO lastRowMiniExchangeEventAuditDAO;
	private static ZoneLastRowMiniExchangeEventDAO zoneLastRowMiniExchangeEventDAO;
	private static ZoneLastRowMiniExchangeEventAuditDAO zoneLastRowMiniExchangeEventAuditDAO;
	private static ZonePricingExchangeEventDAO zonePricingExchangeEventDAO;
	private static ZonePricingExchangeEventAuditDAO zonePricingExchangeEventAuditDAO;
	private static TixCityZonePricingExchangeEventDAO tixCityZonePricingExchangeEventDAO;
	private static ManhattanZonePricingExchangeEventDAO manhattanZonePricingExchangeEventDAO;
	private static TixCityZonePricingExchangeEventAuditDAO tixCityZonePricingExchangeEventAuditDAO;
	private static ManhattanZonePricingExchangeEventAuditDAO manhattanZonePricingExchangeEventAuditDAO;
	private static TixCityZonesPricingCategoryTicketDAO tixCityZonesPricingCategoryTicketDAO;
	private static ManhattanZonesPricingCategoryTicketDAO manhattanZonesPricingCategoryTicketDAO;
	private static SGLastFiveRowExchangeEventDAO sgLastFiveRowExchangeEventDAO;
	private static SGLastFiveRowExchangeEventAuditDAO sgLastFiveRowExchangeEventAuditDAO;
	private static SGLastFiveRowCategoryTicketDAO sgLastFiveRowCategoryTicketDAO;
	private static SeatGeekUploadCatsDAO seatGeekUploadCatsDAO;
	private static SeatGeekWSTrackingDAO seatGeekWSTrackingDAO;
	private static GlobalAutoPricingAuditDAO  globalAutoPricingAuditDAO;
	private static TourCategoryDAO tourCategoryDAO;
	private static CategoryDAO categoryDAO;
	private static CategoryMappingDAO categoryMappingDAO;
	
	private static DefaultPurchasePriceDAO defaultPurchasePriceDAO;
	private static ManagePurchasePriceDAO managePurchasePriceDAO;
	
	private static QueryManagerDAO queryManagerDAO;
	
	private static RoleDAO roleDAO;
	private static UserDAO  userDAO;
	private static UserBrokerDetailsDAO  userBrokerDetailsDAO;
	private static PrivilegeDAO privilegeDAO;
	private static AutopricingExchangeAuditDAO autopricingExchangeAuditDAO;
	private static TicketDAO ticketDAO;
	private static DuplicateTicketMapDAO duplicateTicketMapDAO;
	private static AdmitoneInventoryDAO admitoneInventoryDAO;
	private static MiniCategoryTicketDAO miniCategoryTicketDAO;
	private static TicketNetworkSSAccountTicketDAO ticketNetworkSSAccountTicketDAO;
	private static AutoCatsCategoryTicketDAO autoCatsCategoryTicketDAO;
	private static VipCatsCategoryTicketDAO vipCatsCategoryTicketDAO;
	private static LastRowMiniCategoryTicketDAO lastRowMiniCategoryTicketDAO;
	private static LarryLastCategoryTicketDAO larryLastCategoryTicketDAO;
	private static JKTZonesPricingCategoryTicketDAO jKTZonesPricingCategoryTicketDAO;
	private static ZonesPricingCategoryTicketDAO zonesPricingCategoryTicketDAO;
	private static ZonedLastRowMiniCategoryTicketDAO zonedLastRowMiniCategoryTicketDAO;
	private static MarketMakerEventTrackingDAO marketMakerEventTrackingDAO;
	private static AutoPricingErrorDAO autoPricingErrorDAO;
	private static ZoneLastRowMiniCatsErrorDAO zoneLastRowMiniCatsErrorDAO;
	private static ManualCategoryTicketDAO manualCategoryTicketDAO;
	private static BrokerAuditDAO brokerAuditDAO;
	private static SoldCategoryTicketDAO soldCategoryTicketDAO;
	private static AutoCats96ExchangeEventDAO autoCats96ExchangeEventDAO;
	private static AutoCats96ExchangeEventAuditDAO autoCats96ExchangeEventAuditDAO;
	private static AutoCats96CategoryTicketDAO autoCats96CategoryTicketDAO;
	private static StubHubApiTrackingDAO stubHubApiTrackingDAO;

	private static HistoricalTicketDAO historicalTicketDAO;
	private static VipAutoCategoryTicketDAO vipAutoCategoryTicketDAO;
	private static AutoCatsProjectAuditDAO autoCatsProjectAuditDAO;
	private static FtpUserFileInformationDAO ftpUserFileInformationDAO;
	private static PresaleAutoCatExchangeEventAuditDAO presaleAutoCatExchangeEventAuditDAO;
	private static PresaleAutoCatExchangeEventDAO presaleAutoCatExchangeEventDAO;
	private static PresaleAutoCatCategoryTicketDAO presaleAutoCatCategoryTicketDAO;
	private static AutopricingVenueSettingsDAO autopricingVenueSettingsDAO;
	private static AutopricingVenueSettingsAuditDAO autopricingVenueSettingsAuditDAO;
	private static ExcludeVenuesDAO excludeVenuesDAO;
	private static ExcludeVenuesAuditDAO excludeVenuesAuditDAO;
	private static ExcludeEventZonesDAO excludeEventZonesDAO;
	private static ExcludeEventZonesAuditDAO excludeEventZonesAuditDAO;
	private static ExcludeVenueCategoryZonesDAO excludeVenueCategoryZonesDAO;
	private static ExcludeVenueCategoryZonesAuditDAO excludeVenueCategoryZonesAuditDAO;
	private static TicketCountAlertDAO ticketCountAlertDAO;
	
	private static VipLastRowMiniExchangeEventDAO vipLastRowMiniExchangeEventDAO;
	private static VipLastRowMiniExchangeEventAuditDAO vipLastRowMiniExchangeEventAuditDAO;
	private static VipLastRowMiniCategoryTicketDAO vipLastRowMiniCategoryTicketDAO;
	private static TMATInduxEventDetailsDAO tmatInduxEventDetailsDAO;
	private static TMATInduxEventDetailsAuditDAO tmatInduxEventDetailsAuditDAO;
	
	private static CrownJewelEventsDAO crownJewelEventsDAO;
	private static CrownJewelCategoryTicketDAO crownJewelCategoryTicketDAO;
	private static TNDZonesCategoryTicketDAO tndZonesCategoryTicketDAO;
	private static TNDRealCategoryTicketDAO tndRealCategoryTicketDAO;
	private static UnbroadcastedEventsDAO unbroadcastedEventsDAO;
	private static RTFLastRowCategoryTicketDAO RtfLastRowCategoryTicketDAO;
	private static RTFLastRowExchangeEventDAO RtfLastRowExchangeEventDAO;
	
	public final static PropertyDAO getPropertyDAO() {
		return propertyDAO;
	}

	public final void setPropertyDAO(PropertyDAO propertyDAO) {
		DAORegistry.propertyDAO = propertyDAO;
	}

	public final static FtpUserAccountDAO getFtpUserAccountDAO() {
		return ftpUserAccountDAO;
	}

	public final void setFtpUserAccountDAO(FtpUserAccountDAO ftpUserAccountDAO) {
		DAORegistry.ftpUserAccountDAO = ftpUserAccountDAO;
	}

	public final static FtpFileUploadAuditDAO getFtpFileUploadAuditDAO() {
		return ftpFileUploadAuditDAO;
	}

	public final void setFtpFileUploadAuditDAO(
			FtpFileUploadAuditDAO ftpFileUploadAuditDAO) {
		DAORegistry.ftpFileUploadAuditDAO = ftpFileUploadAuditDAO;
	}

	public final static FtpLocationSettingsDAO getFtpLocationSettingsDAO() {
		return ftpLocationSettingsDAO;
	}

	public final void setFtpLocationSettingsDAO(
			FtpLocationSettingsDAO ftpLocationSettingsDAO) {
		DAORegistry.ftpLocationSettingsDAO = ftpLocationSettingsDAO;
	}

	public final static ArtistDAO getArtistDAO() {
		return artistDAO;
	}

	public final void setArtistDAO(ArtistDAO artistDAO) {
		DAORegistry.artistDAO = artistDAO;
	}

	public static final VenueDAO getVenueDAO() {
		return venueDAO;
	}

	public final void setVenueDAO(VenueDAO venueDAO) {
		DAORegistry.venueDAO = venueDAO;
	}

	public static final ParentCategoryDAO getParentCategoryDAO() {
		return parentCategoryDAO;
	}

	public final void setParentCategoryDAO(ParentCategoryDAO parentCategoryDAO) {
		DAORegistry.parentCategoryDAO = parentCategoryDAO;
	}

	public static final ChildCategoryDAO getChildCategoryDAO() {
		return childCategoryDAO;
	}

	public final void setChildCategoryDAO(ChildCategoryDAO childCategoryDAO) {
		DAORegistry.childCategoryDAO = childCategoryDAO;
	}

	public static final GrandChildCategoryDAO getGrandChildCategoryDAO() {
		return grandChildCategoryDAO;
	}

	public final void setGrandChildCategoryDAO(
			GrandChildCategoryDAO grandChildCategoryDAO) {
		DAORegistry.grandChildCategoryDAO = grandChildCategoryDAO;
	}

	public static final BrokerDAO getBrokerDAO() {
		return brokerDAO;
	}

	public final void setBrokerDAO(BrokerDAO brokerDAO) {
		DAORegistry.brokerDAO = brokerDAO;
	}

	public static final EventDAO getEventDAO() {
		return eventDAO;
	}

	public final void setEventDAO(EventDAO eventDAO) {
		DAORegistry.eventDAO = eventDAO;
	}

	public static final VenueCategoryDAO getVenueCategoryDAO() {
		return venueCategoryDAO;
	}

	public final void setVenueCategoryDAO(VenueCategoryDAO venueCategoryDAO) {
		DAORegistry.venueCategoryDAO = venueCategoryDAO;
	}

	public static  final AutopricingProductDAO getAutopricingProductDAO() {
		return autopricingProductDAO;
	}

	public final void setAutopricingProductDAO(AutopricingProductDAO autopricingProductDAO) {
		DAORegistry.autopricingProductDAO = autopricingProductDAO;
	}

	public static final AutopricingExchangeDAO getAutopricingExchangeDAO() {
		return autopricingExchangeDAO;
	}

	public final void setAutopricingExchangeDAO(AutopricingExchangeDAO autopricingExchangeDAO) {
		DAORegistry.autopricingExchangeDAO = autopricingExchangeDAO;
	}

	public static final AutopricingSettingsDAO getAutopricingSettingsDAO() {
		return autopricingSettingsDAO;
	}

	public final void setAutopricingSettingsDAO(AutopricingSettingsDAO autopricingSettingsDAO) {
		DAORegistry.autopricingSettingsDAO = autopricingSettingsDAO;
	}

	public static final AutopricingSettingsAuditDAO getAutopricingSettingsAuditDAO() {
		return autopricingSettingsAuditDAO;
	}

	public final void setAutopricingSettingsAuditDAO(
			AutopricingSettingsAuditDAO autopricingSettingsAuditDAO) {
		DAORegistry.autopricingSettingsAuditDAO = autopricingSettingsAuditDAO;
	}

	public static final DefaultAutoPricingPropertiesDAO getDefaultAutoPricingPropertiesDAO() {
		return defaultAutoPricingPropertiesDAO;
	}

	public final void setDefaultAutoPricingPropertiesDAO(
			DefaultAutoPricingPropertiesDAO defaultAutoPricingPropertiesDAO) {
		DAORegistry.defaultAutoPricingPropertiesDAO = defaultAutoPricingPropertiesDAO;
	}

	public static final DefaultAutoPricingPropertiesAuditDAO getDefaultAutoPricingPropertiesAuditDAO() {
		return defaultAutoPricingPropertiesAuditDAO;
	}

	public final void setDefaultAutoPricingPropertiesAuditDAO(
			DefaultAutoPricingPropertiesAuditDAO defaultAutoPricingPropertiesAuditDAO) {
		DAORegistry.defaultAutoPricingPropertiesAuditDAO = defaultAutoPricingPropertiesAuditDAO;
	}

	public static final MiniExchangeEventDAO getMiniExchangeEventDAO() {
		return miniExchangeEventDAO;
	}

	public final void setMiniExchangeEventDAO(MiniExchangeEventDAO miniExchangeEventDAO) {
		DAORegistry.miniExchangeEventDAO = miniExchangeEventDAO;
	}

	public static final LarryLastExchangeEventDAO getLarryLastExchangeEventDAO() {
		return larryLastExchangeEventDAO;
	}

	public final void setLarryLastExchangeEventDAO(LarryLastExchangeEventDAO larryLastExchangeEventDAO) {
		DAORegistry.larryLastExchangeEventDAO = larryLastExchangeEventDAO;
	}

	public static final LarryLastExchangeEventAuditDAO getLarryLastExchangeEventAuditDAO() {
		return larryLastExchangeEventAuditDAO;
	}

	public final void setLarryLastExchangeEventAuditDAO(LarryLastExchangeEventAuditDAO larryLastExchangeEventAuditDAO) {
		DAORegistry.larryLastExchangeEventAuditDAO = larryLastExchangeEventAuditDAO;
	}
	

	public static final TicketNetworkSSAccountExchangeEventDAO getTicketNetworkSSAccountExchangeEventDAO() {
		return ticketNetworkSSAccountExchangeEventDAO;
	}

	public final void setTicketNetworkSSAccountExchangeEventDAO(
			TicketNetworkSSAccountExchangeEventDAO ticketNetworkSSAccountExchangeEventDAO) {
		DAORegistry.ticketNetworkSSAccountExchangeEventDAO = ticketNetworkSSAccountExchangeEventDAO;
	}

	public static final TicketNetworkSSAccountExchangeEventAuditDAO getTicketNetworkSSAccountExchangeEventAuditDAO() {
		return ticketNetworkSSAccountExchangeEventAuditDAO;
	}

	public final void setTicketNetworkSSAccountExchangeEventAuditDAO(
			TicketNetworkSSAccountExchangeEventAuditDAO ticketNetworkSSAccountExchangeEventAuditDAO) {
		DAORegistry.ticketNetworkSSAccountExchangeEventAuditDAO = ticketNetworkSSAccountExchangeEventAuditDAO;
	}

	public static final VipMiniExchangeEventDAO getVipMiniExchangeEventDAO() {
		return vipMiniExchangeEventDAO;
	}

	public final void setVipMiniExchangeEventDAO(VipMiniExchangeEventDAO vipMiniExchangeEventDAO) {
		DAORegistry.vipMiniExchangeEventDAO = vipMiniExchangeEventDAO;
	}

	public static final LastRowMiniExchangeEventDAO getLastRowMiniExchangeEventDAO() {
		return lastRowMiniExchangeEventDAO;
	}

	public final void setLastRowMiniExchangeEventDAO(LastRowMiniExchangeEventDAO lastRowMiniExchangeEventDAO) {
		DAORegistry.lastRowMiniExchangeEventDAO = lastRowMiniExchangeEventDAO;
	}

	public static final ZoneLastRowMiniExchangeEventDAO getZoneLastRowMiniExchangeEventDAO() {
		return zoneLastRowMiniExchangeEventDAO;
	}

	public final void setZoneLastRowMiniExchangeEventDAO(ZoneLastRowMiniExchangeEventDAO zoneLastRowMiniExchangeEventDAO) {
		DAORegistry.zoneLastRowMiniExchangeEventDAO = zoneLastRowMiniExchangeEventDAO;
	}

	public static final ZonePricingExchangeEventDAO getZonePricingExchangeEventDAO() {
		return zonePricingExchangeEventDAO;
	}

	public final void setZonePricingExchangeEventDAO(ZonePricingExchangeEventDAO zonePricingExchangeEventDAO) {
		DAORegistry.zonePricingExchangeEventDAO = zonePricingExchangeEventDAO;
	}

	public static final TixCityZonePricingExchangeEventDAO getTixCityZonePricingExchangeEventDAO() {
		return tixCityZonePricingExchangeEventDAO;
	}

	public final void setTixCityZonePricingExchangeEventDAO(
			TixCityZonePricingExchangeEventDAO tixCityZonePricingExchangeEventDAO) {
		DAORegistry.tixCityZonePricingExchangeEventDAO = tixCityZonePricingExchangeEventDAO;
	}

	public static final TixCityZonePricingExchangeEventAuditDAO getTixCityZonePricingExchangeEventAuditDAO() {
		return tixCityZonePricingExchangeEventAuditDAO;
	}

	public final void setTixCityZonePricingExchangeEventAuditDAO(
			TixCityZonePricingExchangeEventAuditDAO tixCityZonePricingExchangeEventAuditDAO) {
		DAORegistry.tixCityZonePricingExchangeEventAuditDAO = tixCityZonePricingExchangeEventAuditDAO;
	}

	public static final TixCityZonesPricingCategoryTicketDAO getTixCityZonesPricingCategoryTicketDAO() {
		return tixCityZonesPricingCategoryTicketDAO;
	}

	public final void setTixCityZonesPricingCategoryTicketDAO(
			TixCityZonesPricingCategoryTicketDAO tixCityZonesPricingCategoryTicketDAO) {
		DAORegistry.tixCityZonesPricingCategoryTicketDAO = tixCityZonesPricingCategoryTicketDAO;
	}

	public static final MiniExchangeEventAuditDAO getMiniExchangeEventAuditDAO() {
		return miniExchangeEventAuditDAO;
	}

	public final void setMiniExchangeEventAuditDAO(
			MiniExchangeEventAuditDAO miniExchangeEventAuditDAO) {
		DAORegistry.miniExchangeEventAuditDAO = miniExchangeEventAuditDAO;
	}

	public static final VipMiniExchangeEventAuditDAO getVipMiniExchangeEventAuditDAO() {
		return vipMiniExchangeEventAuditDAO;
	}

	public final void setVipMiniExchangeEventAuditDAO(
			VipMiniExchangeEventAuditDAO vipMiniExchangeEventAuditDAO) {
		DAORegistry.vipMiniExchangeEventAuditDAO = vipMiniExchangeEventAuditDAO;
	}

	public static final ZoneLastRowMiniExchangeEventAuditDAO getZoneLastRowMiniExchangeEventAuditDAO() {
		return zoneLastRowMiniExchangeEventAuditDAO;
	}

	public final void setZoneLastRowMiniExchangeEventAuditDAO(
			ZoneLastRowMiniExchangeEventAuditDAO zoneLastRowMiniExchangeEventAuditDAO) {
		DAORegistry.zoneLastRowMiniExchangeEventAuditDAO = zoneLastRowMiniExchangeEventAuditDAO;
	}

	public static final ZonePricingExchangeEventAuditDAO getZonePricingExchangeEventAuditDAO() {
		return zonePricingExchangeEventAuditDAO;
	}

	public final void setZonePricingExchangeEventAuditDAO(
			ZonePricingExchangeEventAuditDAO zonePricingExchangeEventAuditDAO) {
		DAORegistry.zonePricingExchangeEventAuditDAO = zonePricingExchangeEventAuditDAO;
	}

	public static final LastRowMiniExchangeEventAuditDAO getLastRowMiniExchangeEventAuditDAO() {
		return lastRowMiniExchangeEventAuditDAO;
	}

	public final void setLastRowMiniExchangeEventAuditDAO(
			LastRowMiniExchangeEventAuditDAO lastRowMiniExchangeEventAuditDAO) {
		DAORegistry.lastRowMiniExchangeEventAuditDAO = lastRowMiniExchangeEventAuditDAO;
	}

	public static final GlobalAutoPricingAuditDAO getGlobalAutoPricingAuditDAO() {
		return globalAutoPricingAuditDAO;
	}

	public final void setGlobalAutoPricingAuditDAO(
			GlobalAutoPricingAuditDAO globalAutoPricingAuditDAO) {
		DAORegistry.globalAutoPricingAuditDAO = globalAutoPricingAuditDAO;
	}

	public static final  TourCategoryDAO getTourCategoryDAO() {
		return tourCategoryDAO;
	}

	public final void setTourCategoryDAO(TourCategoryDAO tourCategoryDAO) {
		DAORegistry.tourCategoryDAO = tourCategoryDAO;
	}

	public final static RoleDAO getRoleDAO() {
		return roleDAO;
	}

	public final void setRoleDAO(RoleDAO roleDAO) {
		DAORegistry.roleDAO = roleDAO;
	}

	public final static UserDAO getUserDAO() {
		return userDAO;
	}

	public final void setUserDAO(UserDAO userDAO) {
		DAORegistry.userDAO = userDAO;
	}

	public final static PrivilegeDAO getPrivilegeDAO() {
		return privilegeDAO;
	}

	public final void setPrivilegeDAO(PrivilegeDAO privilegeDAO) {
		DAORegistry.privilegeDAO = privilegeDAO;
	}

	public final static UserBrokerDetailsDAO getUserBrokerDetailsDAO() {
		return userBrokerDetailsDAO;
	}

	public final void setUserBrokerDetailsDAO(UserBrokerDetailsDAO userBrokerDetailsDAO) {
		DAORegistry.userBrokerDetailsDAO = userBrokerDetailsDAO;
	}


	public final static AutopricingExchangeAuditDAO getAutopricingExchangeAuditDAO() {
		return autopricingExchangeAuditDAO;
	}

	public final void setAutopricingExchangeAuditDAO(
			AutopricingExchangeAuditDAO autopricingExchangeAuditDAO) {
		DAORegistry.autopricingExchangeAuditDAO = autopricingExchangeAuditDAO;
	}

	public static final QueryManagerDAO getQueryManagerDAO() {
		return queryManagerDAO;
	}

	public final void setQueryManagerDAO(QueryManagerDAO queryManagerDAO) {
		DAORegistry.queryManagerDAO = queryManagerDAO;
	}
	
	public final static CategoryDAO getCategoryDAO() {
		return categoryDAO;
	}

	public final void setCategoryDAO(CategoryDAO categoryDAO) {
		DAORegistry.categoryDAO = categoryDAO;
	}

	public final static CategoryMappingDAO getCategoryMappingDAO() {
		return categoryMappingDAO;
	}

	public final void setCategoryMappingDAO(CategoryMappingDAO categoryMappingDAO) {
		DAORegistry.categoryMappingDAO = categoryMappingDAO;
	}

	public final static TicketDAO getTicketDAO() {
		return ticketDAO;
	}

	public final void setTicketDAO(TicketDAO ticketDAO) {
		DAORegistry.ticketDAO = ticketDAO;
	}

	public final static DuplicateTicketMapDAO getDuplicateTicketMapDAO() {
		return duplicateTicketMapDAO;
	}

	public final void setDuplicateTicketMapDAO(
			DuplicateTicketMapDAO duplicateTicketMapDAO) {
		DAORegistry.duplicateTicketMapDAO = duplicateTicketMapDAO;
	}

	public final static AdmitoneInventoryDAO getAdmitoneInventoryDAO() {
		return admitoneInventoryDAO;
	}

	public final void setAdmitoneInventoryDAO(
			AdmitoneInventoryDAO admitoneInventoryDAO) {
		DAORegistry.admitoneInventoryDAO = admitoneInventoryDAO;
	}

	public static final MiniCategoryTicketDAO getMiniCategoryTicketDAO() {
		return miniCategoryTicketDAO;
	}

	public final void setMiniCategoryTicketDAO(
			MiniCategoryTicketDAO miniCategoryTicketDAO) {
		DAORegistry.miniCategoryTicketDAO = miniCategoryTicketDAO;
	}

	public static final AutoCatsCategoryTicketDAO getAutoCatsCategoryTicketDAO() {
		return autoCatsCategoryTicketDAO;
	}

	public final void setAutoCatsCategoryTicketDAO(
			AutoCatsCategoryTicketDAO autoCatsCategoryTicketDAO) {
		DAORegistry.autoCatsCategoryTicketDAO = autoCatsCategoryTicketDAO;
	}

	public static final VipCatsCategoryTicketDAO getVipCatsCategoryTicketDAO() {
		return vipCatsCategoryTicketDAO;
	}

	public final void setVipCatsCategoryTicketDAO(
			VipCatsCategoryTicketDAO vipCatsCategoryTicketDAO) {
		DAORegistry.vipCatsCategoryTicketDAO = vipCatsCategoryTicketDAO;
	}

	public static final LastRowMiniCategoryTicketDAO getLastRowMiniCategoryTicketDAO() {
		return lastRowMiniCategoryTicketDAO;
	}

	public final void setLastRowMiniCategoryTicketDAO(
			LastRowMiniCategoryTicketDAO lastRowMiniCategoryTicketDAO) {
		DAORegistry.lastRowMiniCategoryTicketDAO = lastRowMiniCategoryTicketDAO;
	}

	public static final LarryLastCategoryTicketDAO getLarryLastCategoryTicketDAO() {
		return larryLastCategoryTicketDAO;
	}

	public final void setLarryLastCategoryTicketDAO(
			LarryLastCategoryTicketDAO larryLastCategoryTicketDAO) {
		DAORegistry.larryLastCategoryTicketDAO = larryLastCategoryTicketDAO;
	}

	public static final BrokerAuditDAO getBrokerAuditDAO() {
		return brokerAuditDAO;
	}

	public final void setBrokerAuditDAO(BrokerAuditDAO brokerAuditDAO) {
		DAORegistry.brokerAuditDAO = brokerAuditDAO;
	}

	public static final AutoPricingErrorDAO getAutoPricingErrorDAO() {
		return autoPricingErrorDAO;
	}

	public final void setAutoPricingErrorDAO(AutoPricingErrorDAO autoPricingErrorDAO) {
		DAORegistry.autoPricingErrorDAO = autoPricingErrorDAO;
	}

	public static final ZoneLastRowMiniCatsErrorDAO getZoneLastRowMiniCatsErrorDAO() {
		return zoneLastRowMiniCatsErrorDAO;
	}

	public final void setZoneLastRowMiniCatsErrorDAO(
			ZoneLastRowMiniCatsErrorDAO zoneLastRowMiniCatsErrorDAO) {
		DAORegistry.zoneLastRowMiniCatsErrorDAO = zoneLastRowMiniCatsErrorDAO;
	}

	public static final ManualCategoryTicketDAO getManualCategoryTicketDAO() {
		return manualCategoryTicketDAO;
	}

	public final void setManualCategoryTicketDAO(
			ManualCategoryTicketDAO manualCategoryTicketDAO) {
		DAORegistry.manualCategoryTicketDAO = manualCategoryTicketDAO;
	}

	public static final JKTZonesPricingCategoryTicketDAO getjKTZonesPricingCategoryTicketDAO() {
		return jKTZonesPricingCategoryTicketDAO;
	}

	public final void setjKTZonesPricingCategoryTicketDAO(
			JKTZonesPricingCategoryTicketDAO jKTZonesPricingCategoryTicketDAO) {
		DAORegistry.jKTZonesPricingCategoryTicketDAO = jKTZonesPricingCategoryTicketDAO;
	}

	public static final ZonesPricingCategoryTicketDAO getZonesPricingCategoryTicketDAO() {
		return zonesPricingCategoryTicketDAO;
	}

	public final void setZonesPricingCategoryTicketDAO(
			ZonesPricingCategoryTicketDAO zonesPricingCategoryTicketDAO) {
		DAORegistry.zonesPricingCategoryTicketDAO = zonesPricingCategoryTicketDAO;
	}

	public static final ZonedLastRowMiniCategoryTicketDAO getZonedLastRowMiniCategoryTicketDAO() {
		return zonedLastRowMiniCategoryTicketDAO;
	}

	public final void setZonedLastRowMiniCategoryTicketDAO(
			ZonedLastRowMiniCategoryTicketDAO zonedLastRowMiniCategoryTicketDAO) {
		DAORegistry.zonedLastRowMiniCategoryTicketDAO = zonedLastRowMiniCategoryTicketDAO;
	}

	public static final MarketMakerEventTrackingDAO getMarketMakerEventTrackingDAO() {
		return marketMakerEventTrackingDAO;
	}

	public final void setMarketMakerEventTrackingDAO(
			MarketMakerEventTrackingDAO marketMakerEventTrackingDAO) {
		DAORegistry.marketMakerEventTrackingDAO = marketMakerEventTrackingDAO;
	}

	public static final HistoricalTicketDAO getHistoricalTicketDAO() {
		return historicalTicketDAO;
	}

	public final void setHistoricalTicketDAO(
			HistoricalTicketDAO historicalTicketDAO) {
		DAORegistry.historicalTicketDAO = historicalTicketDAO;
	}

	public static final VipAutoCategoryTicketDAO getVipAutoCategoryTicketDAO() {
		return vipAutoCategoryTicketDAO;
	}

	public final void setVipAutoCategoryTicketDAO(
			VipAutoCategoryTicketDAO vipAutoCategoryTicketDAO) {
		DAORegistry.vipAutoCategoryTicketDAO = vipAutoCategoryTicketDAO;
	}

	public final static DefaultPurchasePriceDAO getDefaultPurchasePriceDAO() {
		return defaultPurchasePriceDAO;
	}

	public final void setDefaultPurchasePriceDAO(
			DefaultPurchasePriceDAO defaultPurchasePriceDAO) {
		DAORegistry.defaultPurchasePriceDAO = defaultPurchasePriceDAO;
	}

	public final static ManagePurchasePriceDAO getManagePurchasePriceDAO() {
		return managePurchasePriceDAO;
	}

	public final void setManagePurchasePriceDAO(
			ManagePurchasePriceDAO managePurchasePriceDAO) {
		DAORegistry.managePurchasePriceDAO = managePurchasePriceDAO;
	}

	public static final AutoCatsProjectAuditDAO getAutoCatsProjectAuditDAO() {
		return autoCatsProjectAuditDAO;
	}

	public final void setAutoCatsProjectAuditDAO(
			AutoCatsProjectAuditDAO autoCatsProjectAuditDAO) {
		DAORegistry.autoCatsProjectAuditDAO = autoCatsProjectAuditDAO;
	}

	public static final SoldCategoryTicketDAO getSoldCategoryTicketDAO() {
		return soldCategoryTicketDAO;
	}

	public final void setSoldCategoryTicketDAO(
			SoldCategoryTicketDAO soldCategoryTicketDAO) {
		DAORegistry.soldCategoryTicketDAO = soldCategoryTicketDAO;
	}

	public static final ZoneTicketsProcessorExchangeEventDAO getZoneTicketsProcessorExchangeEventDAO() {
		return zoneTicketsProcessorExchangeEventDAO;
	}

	public final void setZoneTicketsProcessorExchangeEventDAO(
			ZoneTicketsProcessorExchangeEventDAO zoneTicketsProcessorExchangeEventDAO) {
		DAORegistry.zoneTicketsProcessorExchangeEventDAO = zoneTicketsProcessorExchangeEventDAO;
	}

	public static final ZoneTicketsProcessorExchangeEventAuditDAO getZoneTicketsProcessorExchangeEventAuditDAO() {
		return zoneTicketsProcessorExchangeEventAuditDAO;
	}

	public final void setZoneTicketsProcessorExchangeEventAuditDAO(
			ZoneTicketsProcessorExchangeEventAuditDAO zoneTicketsProcessorExchangeEventAuditDAO) {
		DAORegistry.zoneTicketsProcessorExchangeEventAuditDAO = zoneTicketsProcessorExchangeEventAuditDAO;
	}

	public static final ZoneTicketsProcessorCategoryTicketDAO getZoneTicketsProcessorCategoryTicketDAO() {
		return zoneTicketsProcessorCategoryTicketDAO;
	}

	public final void setZoneTicketsProcessorCategoryTicketDAO(
			ZoneTicketsProcessorCategoryTicketDAO zoneTicketsProcessorCategoryTicketDAO) {
		DAORegistry.zoneTicketsProcessorCategoryTicketDAO = zoneTicketsProcessorCategoryTicketDAO;
	}
	
	public static final PresaleZoneTicketsExchangeEventDAO getPresaleZoneTicketsExchangeEventDAO() {
		return presaleZoneTicketsExchangeEventDAO;
	}

	public final void setPresaleZoneTicketsExchangeEventDAO(
			PresaleZoneTicketsExchangeEventDAO presaleZoneTicketsExchangeEventDAO) {
		DAORegistry.presaleZoneTicketsExchangeEventDAO = presaleZoneTicketsExchangeEventDAO;
	}

	public static final PresaleZoneTicketsExchangeEventAuditDAO getPresaleZoneTicketsExchangeEventAuditDAO() {
		return presaleZoneTicketsExchangeEventAuditDAO;
	}

	public final void setPresaleZoneTicketsExchangeEventAuditDAO(
			PresaleZoneTicketsExchangeEventAuditDAO presaleZoneTicketsExchangeEventAuditDAO) {
		DAORegistry.presaleZoneTicketsExchangeEventAuditDAO = presaleZoneTicketsExchangeEventAuditDAO;
	}

	public static final PresaleZoneTicketsCategoryTicketDAO getPresaleZoneTicketsCategoryTicketDAO() {
		return presaleZoneTicketsCategoryTicketDAO;
	}

	public final void setPresaleZoneTicketsCategoryTicketDAO(
			PresaleZoneTicketsCategoryTicketDAO presaleZoneTicketsCategoryTicketDAO) {
		DAORegistry.presaleZoneTicketsCategoryTicketDAO = presaleZoneTicketsCategoryTicketDAO;
	}

	public final static FtpUserFileInformationDAO getFtpUserFileInformationDAO() {
		return ftpUserFileInformationDAO;
	}

	public final void setFtpUserFileInformationDAO(
			FtpUserFileInformationDAO ftpUserFileInformationDAO) {
		DAORegistry.ftpUserFileInformationDAO = ftpUserFileInformationDAO;
	}

	public final static PresaleAutoCatExchangeEventAuditDAO getPresaleAutoCatExchangeEventAuditDAO() {
		return presaleAutoCatExchangeEventAuditDAO;
	}

	public final void setPresaleAutoCatExchangeEventAuditDAO(
			PresaleAutoCatExchangeEventAuditDAO presaleAutoCatExchangeEventAuditDAO) {
		DAORegistry.presaleAutoCatExchangeEventAuditDAO = presaleAutoCatExchangeEventAuditDAO;
	}

	public final static PresaleAutoCatExchangeEventDAO getPresaleAutoCatExchangeEventDAO() {
		return presaleAutoCatExchangeEventDAO;
	}

	public final void setPresaleAutoCatExchangeEventDAO(
			PresaleAutoCatExchangeEventDAO presaleAutoCatExchangeEventDAO) {
		DAORegistry.presaleAutoCatExchangeEventDAO = presaleAutoCatExchangeEventDAO;
	}

	public final static PresaleAutoCatCategoryTicketDAO getPresaleAutoCatCategoryTicketDAO() {
		return presaleAutoCatCategoryTicketDAO;
	}

	public final void setPresaleAutoCatCategoryTicketDAO(
			PresaleAutoCatCategoryTicketDAO presaleAutoCatCategoryTicketDAO) {
		DAORegistry.presaleAutoCatCategoryTicketDAO = presaleAutoCatCategoryTicketDAO;
	}

	public final static AutopricingVenueSettingsDAO getAutopricingVenueSettingsDAO() {
		return autopricingVenueSettingsDAO;
	}

	public final void setAutopricingVenueSettingsDAO(
			AutopricingVenueSettingsDAO autopricingVenueSettingsDAO) {
		DAORegistry.autopricingVenueSettingsDAO = autopricingVenueSettingsDAO;
	}

	public final static AutopricingVenueSettingsAuditDAO getAutopricingVenueSettingsAuditDAO() {
		return autopricingVenueSettingsAuditDAO;
	}

	public final void setAutopricingVenueSettingsAuditDAO(
			AutopricingVenueSettingsAuditDAO autopricingVenueSettingsAuditDAO) {
		DAORegistry.autopricingVenueSettingsAuditDAO = autopricingVenueSettingsAuditDAO;
	}

	public final static ExcludeVenuesDAO getExcludeVenuesDAO() {
		return excludeVenuesDAO;
	}

	public final void setExcludeVenuesDAO(ExcludeVenuesDAO excludeVenuesDAO) {
		DAORegistry.excludeVenuesDAO = excludeVenuesDAO;
	}

	public final static ExcludeVenuesAuditDAO getExcludeVenuesAuditDAO() {
		return excludeVenuesAuditDAO;
	}

	public final  void setExcludeVenuesAuditDAO(
			ExcludeVenuesAuditDAO excludeVenuesAuditDAO) {
		DAORegistry.excludeVenuesAuditDAO = excludeVenuesAuditDAO;
	}
    
	public final static ExcludeEventZonesDAO getExcludeEventZonesDAO() {
		return excludeEventZonesDAO;
	}

	public final void setExcludeEventZonesDAO(
			ExcludeEventZonesDAO excludeEventZonesDAO) {
		DAORegistry.excludeEventZonesDAO = excludeEventZonesDAO;
	}

	public final static ExcludeEventZonesAuditDAO getExcludeEventZonesAuditDAO() {
		return excludeEventZonesAuditDAO;
	}

	public final void setExcludeEventZonesAuditDAO(
			ExcludeEventZonesAuditDAO excludeEventZonesAuditDAO) {
		DAORegistry.excludeEventZonesAuditDAO = excludeEventZonesAuditDAO;
	}

	public final static ExcludeVenueCategoryZonesDAO getExcludeVenueCategoryZonesDAO() {
		return excludeVenueCategoryZonesDAO;
	}

	public final void setExcludeVenueCategoryZonesDAO(
			ExcludeVenueCategoryZonesDAO excludeVenueCategoryZonesDAO) {
		DAORegistry.excludeVenueCategoryZonesDAO = excludeVenueCategoryZonesDAO;
	}

	public final static ExcludeVenueCategoryZonesAuditDAO getExcludeVenueCategoryZonesAuditDAO() {
		return excludeVenueCategoryZonesAuditDAO;
	}

	public final void setExcludeVenueCategoryZonesAuditDAO(
			ExcludeVenueCategoryZonesAuditDAO excludeVenueCategoryZonesAuditDAO) {
		DAORegistry.excludeVenueCategoryZonesAuditDAO = excludeVenueCategoryZonesAuditDAO;
	}

	public static final AutoCats96ExchangeEventDAO getAutoCats96ExchangeEventDAO() {
		return autoCats96ExchangeEventDAO;
	}

	public final void setAutoCats96ExchangeEventDAO(
			AutoCats96ExchangeEventDAO autoCats96ExchangeEventDAO) {
		DAORegistry.autoCats96ExchangeEventDAO = autoCats96ExchangeEventDAO;
	}

	public static final AutoCats96ExchangeEventAuditDAO getAutoCats96ExchangeEventAuditDAO() {
		return autoCats96ExchangeEventAuditDAO;
	}

	public final void setAutoCats96ExchangeEventAuditDAO(
			AutoCats96ExchangeEventAuditDAO autoCats96ExchangeEventAuditDAO) {
		DAORegistry.autoCats96ExchangeEventAuditDAO = autoCats96ExchangeEventAuditDAO;
	}

	public static final AutoCats96CategoryTicketDAO getAutoCats96CategoryTicketDAO() {
		return autoCats96CategoryTicketDAO;
	}

	public final void setAutoCats96CategoryTicketDAO(
			AutoCats96CategoryTicketDAO autoCats96CategoryTicketDAO) {
		DAORegistry.autoCats96CategoryTicketDAO = autoCats96CategoryTicketDAO;
	}

	public static final ZoneTicketsLockedTicketEventDetailsDAO getZoneTicketsLockedTicketEventDetailsDAO() {
		return zoneTicketsLockedTicketEventDetailsDAO;
	}

	public final void setZoneTicketsLockedTicketEventDetailsDAO(
			ZoneTicketsLockedTicketEventDetailsDAO zoneTicketsLockedTicketEventDetailsDAO) {
		DAORegistry.zoneTicketsLockedTicketEventDetailsDAO = zoneTicketsLockedTicketEventDetailsDAO;
	}

	public static final PresaleZoneTicketsLockedTicketEventDetailsDAO getPresaleZoneTicketsLockedTicketEventDetailsDAO() {
		return presaleZoneTicketsLockedTicketEventDetailsDAO;
	}

	public final void setPresaleZoneTicketsLockedTicketEventDetailsDAO(
			PresaleZoneTicketsLockedTicketEventDetailsDAO presaleZoneTicketsLockedTicketEventDetailsDAO) {
		DAORegistry.presaleZoneTicketsLockedTicketEventDetailsDAO = presaleZoneTicketsLockedTicketEventDetailsDAO;
	}

	public static final TicketNetworkSSAccountTicketDAO getTicketNetworkSSAccountTicketDAO() {
		return ticketNetworkSSAccountTicketDAO;
	}

	public final void setTicketNetworkSSAccountTicketDAO(TicketNetworkSSAccountTicketDAO ticketNetworkSSAccountTicketDAO) {
		DAORegistry.ticketNetworkSSAccountTicketDAO = ticketNetworkSSAccountTicketDAO;
	}

	public static final ManhattanZonePricingExchangeEventDAO getManhattanZonePricingExchangeEventDAO() {
		return manhattanZonePricingExchangeEventDAO;
	}

	public final void setManhattanZonePricingExchangeEventDAO(
			ManhattanZonePricingExchangeEventDAO manhattanZonePricingExchangeEventDAO) {
		DAORegistry.manhattanZonePricingExchangeEventDAO = manhattanZonePricingExchangeEventDAO;
	}

	public static final ManhattanZonesPricingCategoryTicketDAO getManhattanZonesPricingCategoryTicketDAO() {
		return manhattanZonesPricingCategoryTicketDAO;
	}

	public final void setManhattanZonesPricingCategoryTicketDAO(
			ManhattanZonesPricingCategoryTicketDAO manhattanZonesPricingCategoryTicketDAO) {
		DAORegistry.manhattanZonesPricingCategoryTicketDAO = manhattanZonesPricingCategoryTicketDAO;
	}

	public static final ManhattanZonePricingExchangeEventAuditDAO getManhattanZonePricingExchangeEventAuditDAO() {
		return manhattanZonePricingExchangeEventAuditDAO;
	}

	public final void setManhattanZonePricingExchangeEventAuditDAO(
			ManhattanZonePricingExchangeEventAuditDAO manhattanZonePricingExchangeEventAuditDAO) {
		DAORegistry.manhattanZonePricingExchangeEventAuditDAO = manhattanZonePricingExchangeEventAuditDAO;
	}

	public static final SGLastFiveRowExchangeEventDAO getSgLastFiveRowExchangeEventDAO() {
		return sgLastFiveRowExchangeEventDAO;
	}

	public final void setSgLastFiveRowExchangeEventDAO(
			SGLastFiveRowExchangeEventDAO sgLastFiveRowExchangeEventDAO) {
		DAORegistry.sgLastFiveRowExchangeEventDAO = sgLastFiveRowExchangeEventDAO;
	}

	public static final SGLastFiveRowExchangeEventAuditDAO getSgLastFiveRowExchangeEventAuditDAO() {
		return sgLastFiveRowExchangeEventAuditDAO;
	}

	public final void setSgLastFiveRowExchangeEventAuditDAO(
			SGLastFiveRowExchangeEventAuditDAO sgLastFiveRowExchangeEventAuditDAO) {
		DAORegistry.sgLastFiveRowExchangeEventAuditDAO = sgLastFiveRowExchangeEventAuditDAO;
	}

	public static final SGLastFiveRowCategoryTicketDAO getSgLastFiveRowCategoryTicketDAO() {
		return sgLastFiveRowCategoryTicketDAO;
	}

	public final void setSgLastFiveRowCategoryTicketDAO(
			SGLastFiveRowCategoryTicketDAO sgLastFiveRowCategoryTicketDAO) {
		DAORegistry.sgLastFiveRowCategoryTicketDAO = sgLastFiveRowCategoryTicketDAO;
	}

	public static final SeatGeekUploadCatsDAO getSeatGeekUploadCatsDAO() {
		return seatGeekUploadCatsDAO;
	}

	public final void setSeatGeekUploadCatsDAO(
			SeatGeekUploadCatsDAO seatGeekUploadCatsDAO) {
		DAORegistry.seatGeekUploadCatsDAO = seatGeekUploadCatsDAO;
	}

	public static final SeatGeekWSTrackingDAO getSeatGeekWSTrackingDAO() {
		return seatGeekWSTrackingDAO;
	}

	public final void setSeatGeekWSTrackingDAO(
			SeatGeekWSTrackingDAO seatGeekWSTrackingDAO) {
		DAORegistry.seatGeekWSTrackingDAO = seatGeekWSTrackingDAO;
	}

	public static final TicketCountAlertDAO getTicketCountAlertDAO() {
		return ticketCountAlertDAO;
	}

	public final void setTicketCountAlertDAO(
			TicketCountAlertDAO ticketCountAlertDAO) {
		DAORegistry.ticketCountAlertDAO = ticketCountAlertDAO;
	}
	
	public static final VipLastRowMiniExchangeEventDAO getVipLastRowMiniExchangeEventDAO() {
		return vipLastRowMiniExchangeEventDAO;
	}

	public final void setVipLastRowMiniExchangeEventDAO(
			VipLastRowMiniExchangeEventDAO vipLastRowMiniExchangeEventDAO) {
		DAORegistry.vipLastRowMiniExchangeEventDAO = vipLastRowMiniExchangeEventDAO;
	}

	public static final VipLastRowMiniCategoryTicketDAO getVipLastRowMiniCategoryTicketDAO() {
		return vipLastRowMiniCategoryTicketDAO;
	}

	public final void setVipLastRowMiniCategoryTicketDAO(
			VipLastRowMiniCategoryTicketDAO vipLastRowMiniCategoryTicketDAO) {
		DAORegistry.vipLastRowMiniCategoryTicketDAO = vipLastRowMiniCategoryTicketDAO;
	}

	public static final VipLastRowMiniExchangeEventAuditDAO getVipLastRowMiniExchangeEventAuditDAO() {
		return vipLastRowMiniExchangeEventAuditDAO;
	}

	public final void setVipLastRowMiniExchangeEventAuditDAO(
			VipLastRowMiniExchangeEventAuditDAO vipLastRowMiniExchangeEventAuditDAO) {
		DAORegistry.vipLastRowMiniExchangeEventAuditDAO = vipLastRowMiniExchangeEventAuditDAO;
	}

	public static final UnbroadcastedEventsDAO getUnbroadcastedEventsDAO() {
		return unbroadcastedEventsDAO;
	}

	public final void setUnbroadcastedEventsDAO(
			UnbroadcastedEventsDAO unbroadcastedEventsDAO) {
		DAORegistry.unbroadcastedEventsDAO = unbroadcastedEventsDAO;
	}

	public static final TMATInduxEventDetailsDAO getTmatInduxEventDetailsDAO() {
		return tmatInduxEventDetailsDAO;
	}

	public final void setTmatInduxEventDetailsDAO(
			TMATInduxEventDetailsDAO tmatInduxEventDetailsDAO) {
		DAORegistry.tmatInduxEventDetailsDAO = tmatInduxEventDetailsDAO;
	}

	public static final TMATInduxEventDetailsAuditDAO getTmatInduxEventDetailsAuditDAO() {
		return tmatInduxEventDetailsAuditDAO;
	}

	public final void setTmatInduxEventDetailsAuditDAO(
			TMATInduxEventDetailsAuditDAO tmatInduxEventDetailsAuditDAO) {
		DAORegistry.tmatInduxEventDetailsAuditDAO = tmatInduxEventDetailsAuditDAO;
	}

	public static final StubHubApiTrackingDAO getStubHubApiTrackingDAO() {
		return stubHubApiTrackingDAO;
	}

	public final void setStubHubApiTrackingDAO(
			StubHubApiTrackingDAO stubHubApiTrackingDAO) {
		DAORegistry.stubHubApiTrackingDAO = stubHubApiTrackingDAO;
	}

	public static final CrownJewelEventsDAO getCrownJewelEventsDAO() {
		return crownJewelEventsDAO;
	}

	public final void setCrownJewelEventsDAO(
			CrownJewelEventsDAO crownJewelEventsDAO) {
		DAORegistry.crownJewelEventsDAO = crownJewelEventsDAO;
	}

	public static final CrownJewelCategoryTicketDAO getCrownJewelCategoryTicketDAO() {
		return crownJewelCategoryTicketDAO;
	}

	public final void setCrownJewelCategoryTicketDAO(
			CrownJewelCategoryTicketDAO crownJewelCategoryTicketDAO) {
		DAORegistry.crownJewelCategoryTicketDAO = crownJewelCategoryTicketDAO;
	}

	public static final TNDZonesCategoryTicketDAO getTndZonesCategoryTicketDAO() {
		return tndZonesCategoryTicketDAO;
	}

	public final void setTndZonesCategoryTicketDAO(
			TNDZonesCategoryTicketDAO tndZonesCategoryTicketDAO) {
		DAORegistry.tndZonesCategoryTicketDAO = tndZonesCategoryTicketDAO;
	}

	public static final TNDRealCategoryTicketDAO getTndRealCategoryTicketDAO() {
		return tndRealCategoryTicketDAO;
	}

	public final void setTndRealCategoryTicketDAO(
			TNDRealCategoryTicketDAO tndRealCategoryTicketDAO) {
		DAORegistry.tndRealCategoryTicketDAO = tndRealCategoryTicketDAO;
	}

	public static final RTFLastRowCategoryTicketDAO getRtfLastRowCategoryTicketDAO() {
		return RtfLastRowCategoryTicketDAO;
	}

	public final void setRtfLastRowCategoryTicketDAO(
			RTFLastRowCategoryTicketDAO rtfLastRowCategoryTicketDAO) {
		RtfLastRowCategoryTicketDAO = rtfLastRowCategoryTicketDAO;
	}

	public static final RTFLastRowExchangeEventDAO getRtfLastRowExchangeEventDAO() {
		return RtfLastRowExchangeEventDAO;
	}

	public final void setRtfLastRowExchangeEventDAO(
			RTFLastRowExchangeEventDAO rtfLastRowExchangeEventDAO) {
		RtfLastRowExchangeEventDAO = rtfLastRowExchangeEventDAO;
	}

	
	
}


