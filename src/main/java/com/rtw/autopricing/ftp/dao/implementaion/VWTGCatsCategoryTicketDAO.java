package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.dao.services.RootDAO;
import com.rtw.autopricing.ftp.indux.data.VWTGCatsCategoryTicket;



public interface VWTGCatsCategoryTicketDAO extends RootDAO<Integer, VWTGCatsCategoryTicket> {

	public Collection<VWTGCatsCategoryTicket> getAllActiveTnTgCatsCategoryTickets();
}
