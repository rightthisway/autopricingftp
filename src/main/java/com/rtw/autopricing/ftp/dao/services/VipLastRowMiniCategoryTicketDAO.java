package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.LastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.VipLastRowMiniCategoryTicket;

public interface VipLastRowMiniCategoryTicketDAO extends RootDAO<Integer, VipLastRowMiniCategoryTicket> {
	public List<VipLastRowMiniCategoryTicket> getAllVipLastRowMiniCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<VipLastRowMiniCategoryTicket> getAllTNVipLastRowMiniCategoryTicketsByEventId(Integer eventId)  throws Exception;
	public Integer deleteAllVipLastRowMiniCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnVipLastRowMiniCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllVipLastRowMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingproduct)  throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInVipLastRowMiniCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingproduct)  throws Exception;
	public List<VipLastRowMiniCategoryTicket> getAllVipLastRowMiniCategoryTicketsByAll(Integer eventId, String section, String row, String quantity)  throws Exception;
}
