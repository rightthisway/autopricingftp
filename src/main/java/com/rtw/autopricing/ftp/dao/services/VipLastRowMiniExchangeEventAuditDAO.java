package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEventAudit;

public interface VipLastRowMiniExchangeEventAuditDAO extends RootDAO<Integer, VipLastRowMiniExchangeEventAudit> {
	public Collection<VipLastRowMiniExchangeEventAudit> getAllVipLastRowMiniExchangeEventAuditsByEventId(Integer eventId);
}
