package com.rtw.autopricing.ftp.dao.services;



import java.util.List;

import com.rtw.autopricing.ftp.data.ManualCategoryTicket;



public interface ManualCategoryTicketDAO extends RootDAO<Integer, ManualCategoryTicket> {

	
	public List<ManualCategoryTicket> getAllManualCategoryTicketsByBrokerId(Integer brokerId) throws Exception;
}

