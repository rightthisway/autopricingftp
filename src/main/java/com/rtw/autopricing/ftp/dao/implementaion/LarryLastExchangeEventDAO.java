package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.enums.EventStatus;
import com.rtw.autopricing.util.AutoExchangeEventLoader;

public class LarryLastExchangeEventDAO  extends HibernateDAO<Integer, LarryLastExchangeEvent> implements com.rtw.autopricing.ftp.dao.services.LarryLastExchangeEventDAO{

	public LarryLastExchangeEvent getLarryLastExchangeEventByEventId(Integer eventId) throws Exception {
		return findSingle("FROM LarryLastExchangeEvent WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByVenueId(Integer venueId) throws Exception {
		return find("SELECT le FROM LarryLastExchangeEvent le ,Event e WHERE e.id = le.eventId And e.venueId=?", new Object[]{venueId});
	}

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByArtistId(Integer artistId)  throws Exception {
		return find("SELECT le FROM LarryLastExchangeEvent le ,Event e WHERE e.id = le.eventId And e.artistId=?", new Object[]{artistId});
	}

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId)  throws Exception {
		return find("FROM le FROM LarryLastExchangeEvent le ,Event e WHERE e.id = le.eventId And e.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}
	
	public List<LarryLastExchangeEvent> getAllLarryLastExchangeEventsofDeletedTmatEvents()  throws Exception {
		return find("FROM LarryLastExchangeEvent WHERE status='ACTIVE' and eventId in(" +
				"SELECT id FROM Event WHERE eventStatus <> ?)", new Object[]{EventStatus.ACTIVE});
	}

	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType)  throws Exception {
		
		Session session=null;
		
		try {
			String query = "";
			
			if(globalAudit.getExposure() != null) {
				query = query + ",ee.exposure='"+globalAudit.getExposure()+"'";
			}
			if(globalAudit.getShippingMethod() != null) {
				query = query + ",ee.shipping_method='"+globalAudit.getShippingMethod()+"'";
			}
			if(globalAudit.getNearTermDisplayOption() != null) {
				query = query + ",ee.near_term_display_option='"+globalAudit.getNearTermDisplayOption()+"'";
			}
			if(globalAudit.getRptFactor() != null) {
				query = query + ",ee.rpt_factor="+globalAudit.getRptFactor();
			}
			if(globalAudit.getPriceBreakup() != null) {
				query = query + ",ee.price_breakup="+globalAudit.getPriceBreakup();
			}
			if(globalAudit.getLowerMarkup() != null) {
				query = query + ",ee.lower_markup="+globalAudit.getLowerMarkup();
			}
			if(globalAudit.getUpperMarkup() != null) {
				query = query + ",ee.upper_markup="+globalAudit.getUpperMarkup();
			}
			if(globalAudit.getLowerShippingFees() != null) {
				query = query + ",ee.lower_shipping_fees="+globalAudit.getLowerShippingFees();
			}
			if(globalAudit.getUpperShippingFees() != null) {
				query = query + ",ee.upper_shipping_fees="+globalAudit.getUpperShippingFees();
			}
			if(globalAudit.getShippingDays()!= null) {
				query = query + ",ee.shipping_days="+globalAudit.getShippingDays();
			}
			
			query = query + ",last_updated_date=GETDATE()";
			
			query = "update ee set " + query.substring(1);
			
			query = query + " from larrylast_exchange_event ee " +
			" inner join event e on e.id=ee.event_id " +
			" inner join artist t on t.id=e.artist_id" +
			" inner join grand_child_tour_category gc on gc.id=t.grand_child_category_id" +
			" inner join child_tour_category cc on cc.id=gc.child_category_id" +
			" inner join tour_category tc on tc.id=cc.tour_category_id " +
			" where tc.name in("+parentType+")";
			
			
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			session.close();
		}
	}
	
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception {
		Session session=null;
		
		try {
			String query = "";
			
			
			if(autopricingVenueSetting.getShippingMethod() != null) {
				query = query + ",ee.shipping_method="+autopricingVenueSetting.getShippingMethod();
			} else {
				query = query + ",ee.shipping_method=null";
			}
			if(autopricingVenueSetting.getNearTermDisplayOption() != null) {
				query = query + ",ee.near_term_display_option="+autopricingVenueSetting.getNearTermDisplayOption();
			} else {
				query = query + ",ee.near_term_display_option=null";
			}
			if(autopricingVenueSetting.getMarkup() != null) {
				query = query + ",ee.lower_markup="+autopricingVenueSetting.getMarkup()+",ee.upper_markup="+autopricingVenueSetting.getMarkup();
			}
			if(autopricingVenueSetting.getShippingDays() != null) {
				query = query + ",ee.shipping_days="+autopricingVenueSetting.getShippingDays();
			}
			
			query = query + ",last_updated_date=GETDATE(),last_updated_by='"+autopricingVenueSetting.getLastUpdatedBy()+"'";
			query = "update ee set " + query.substring(1);
			
			
			query = query + " from larrylast_exchange_event ee " +
			" inner join event e on e.id=ee.event_id " +
			" where e.venue_id="+autopricingVenueSetting.getVenueId()+"";
			
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			session.close();
		}
	}

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct)  throws Exception {
		String sql =" select distinct le.id as id,le.event_id as eventId, le.exposure as exposure, " +
					" le.rpt_factor as rptFactor,le.price_breakup as priceBreakup,le.lower_markup as lowerMarkup, " +
					" le.upper_markup as upperMarkup,le.lower_shipping_fees as lowerShippingFees, " +
					" le.upper_shipping_fees as upperShippingFees,le.shipping_method as shippingMethod, " +
					" le.near_term_display_option as nearTermDisplayOption,le.allow_section_range as allowSectionRange, " +
					" le.shipping_days as shippingDays,le.vivid_broker_id as vividBrokerId,le.tn_broker_id as ticketNetworkBrokerId," +
					" le.scorebig_broker_id as scoreBigBrokerId,le.fanxchange_broker_id as fanxchangeBrokerId,le.ticketcity_broker_id as ticketcityBrokerId,le.seatgeek_broker_id as seatGeekBrokerId,le.is_zone as zone," +
					" rot_pos_enabled as rotEnabled,tixcity_pos_enabled as tixcityEnabled,rtw_pos_enabled as rtwEnabled,vivid_enabled as vividEnabled," +
					" zoned_lastrowminicats_enabled as zonedLastrowMinicatsEnabled,larrylast_enabled as larryLastEnabled," +
					" e.event_date as eventDate " + 
					" from larrylast_exchange_event le with(nolock)" + 
					" INNER join ticket_listing_crawl tc with(nolock) on tc.event_id = le.event_id " +
					" INNER JOIN event e with(nolock) on e.id = le.event_id " +
					" INNER JOIN venue_category vc with(nolock) on vc.id = e.venue_category_id " +
					" left join exclude_event_zones ez with(nolock) on ez.event_id=e.id and ez.product_id="+autopricingProduct.getId() +
					" left join exclude_venue_category_zones vcz with(nolock) on vcz.venue_category_id=e.venue_category_id and vcz.product_id="+autopricingProduct.getId() +
					" WHERE le.status = 'ACTIVE'  " +//AND vc.category_group!='presale'
					" AND tc.site_id in ('ticketnetworkdirect','stubhub','ticketevolution','flashseats','vividseat') " +
					//" AND (le.tn_broker_id is not null OR le.vivid_broker_id is not null OR le.scorebig_broker_id is not null OR le.fanxchange_broker_id is not null OR le.ticketcity_broker_id is not null)" +
					" AND ((DATEDIFF(MINUTE,tc.end_crawl,GETDATE())<=  " + minute +
					" OR DATEDIFF(MINUTE,vc.last_updated_date,GETDATE())<=  " + minute +
					" OR (DATEDIFF(MINUTE,le.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (DATEDIFF(MINUTE,e.last_update,GETDATE())<= " + minute + ")" +
					" OR (ez.id is not null and DATEDIFF(MINUTE,ez.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (vcz.id is not null and DATEDIFF(MINUTE,vcz.last_updated_date,GETDATE())<= " + minute + "))) " +
					" order by eventDate asc" ;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			sqlQuery.addScalar("id",Hibernate.INTEGER);
			sqlQuery.addScalar("exposure",Hibernate.STRING);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("rptFactor",Hibernate.DOUBLE);
			sqlQuery.addScalar("shippingMethod",Hibernate.INTEGER);
			sqlQuery.addScalar("nearTermDisplayOption",Hibernate.INTEGER);
			
			sqlQuery.addScalar("priceBreakup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("vividBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("scoreBigBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("ticketNetworkBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("fanxchangeBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("ticketcityBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("seatGeekBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("allowSectionRange",Hibernate.BOOLEAN);
			sqlQuery.addScalar("zone",Hibernate.BOOLEAN);
			sqlQuery.addScalar("shippingDays",Hibernate.INTEGER);
			sqlQuery.addScalar("rotEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("tixcityEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("rtwEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("vividEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("zonedLastrowMinicatsEnabled",Hibernate.BOOLEAN);
			sqlQuery.addScalar("larryLastEnabled",Hibernate.BOOLEAN);
			List<LarryLastExchangeEvent> exchangeEvents =  sqlQuery.setResultTransformer(Transformers.aliasToBean(LarryLastExchangeEvent.class)).list();
			return exchangeEvents;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return null;
	}

	public List<LarryLastExchangeEvent> getLastRowMiniExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId)  throws Exception {
		Session session =getSession();
		List<LarryLastExchangeEvent> list=null;
		String  sql=null;
		
		if(brokerId.equals(10)){//RTW-2 //ROT
		     sql=" select * from larrylast_exchange_event ee inner join event e on ee.event_id=e.id " +
			" where e.venue_id="+venueId+"  ";//and (ee.rot_pos_enabled=" + 1 +")
	    } else if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//Tixcity
		     sql=" select * from larrylast_exchange_event ee inner join event e on ee.event_id=e.id " +
				" where e.venue_id="+venueId+" ";//and (ee.tixcity_pos_enabled=" + 1 +") 
	    } else if(brokerId.equals(5)){//RTW
		      sql=" select * from larrylast_exchange_event ee inner join event e on ee.event_id=e.id " +
			" where e.venue_id="+venueId+"  ";//and (ee.rtw_pos_enabled=" + 1 +")
	    }
		
		try{
			list=  session.createSQLQuery(sql).addEntity(LarryLastExchangeEvent.class).list();
		   }catch(Exception e){
			   e.printStackTrace();
			   throw e;
		   }finally{
			   session.close();
		   }
		return list;
	}
	
	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception {
		return find("FROM LarryLastExchangeEvent WHERE eventId in(" +
				"SELECT id FROM Event WHERE admitoneId in ("+ admitoneEventIds +"))");
	}
	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByEventIds(String eventIds)  throws Exception {
		return find("FROM LarryLastExchangeEvent WHERE eventId in("+ eventIds +")");
	}

}