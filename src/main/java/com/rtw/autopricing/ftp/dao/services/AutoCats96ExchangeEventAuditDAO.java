package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEventAudit;

public interface AutoCats96ExchangeEventAuditDAO extends RootDAO<Integer, AutoCats96ExchangeEventAudit>{

	List<AutoCats96ExchangeEventAudit> getAutoCats96ExchangeEventAuditByEventId(Integer eId);
  
}
