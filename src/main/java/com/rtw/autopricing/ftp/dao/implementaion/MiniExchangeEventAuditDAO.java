package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;


import com.rtw.autopricing.ftp.data.MiniExchangeEventAudit;

public class MiniExchangeEventAuditDAO extends HibernateDAO<Integer,MiniExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.MiniExchangeEventAuditDAO{

	public List<MiniExchangeEventAudit> getMiniExchangeEventAuditByEventId(Integer eId) {
		
		Session session = getSession();
		List<MiniExchangeEventAudit> list = new ArrayList<MiniExchangeEventAudit>();
		try{
			Query query = session.createQuery("FROM MiniExchangeEventAudit where event_id = :eIds");
			query.setParameter("eIds", eId);
			list= query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}				
		return list;
	}
}
