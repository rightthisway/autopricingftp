package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEventAudit;

public class LastRowMiniExchangeEventAuditDAO extends HibernateDAO<Integer, LastRowMiniExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.LastRowMiniExchangeEventAuditDAO{

	public Collection<LastRowMiniExchangeEventAudit> getAllLastRowMiniExchangeEventAuditsByEventId(Integer eventId){
		return find("FROM LastRowMiniExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<LastRowMiniExchangeEventAudit> getAllLastRowMiniExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM LastRowMiniExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<LastRowMiniExchangeEventAudit> getAllLastRowMiniExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM LastRowMiniExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<LastRowMiniExchangeEventAudit> getAllLastRowMiniExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM LastRowMiniExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}

}