package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingExchangeAudit;

public interface AutopricingExchangeAuditDAO extends RootDAO<Integer,AutopricingExchangeAudit> {

	public List<AutopricingExchangeAudit> getAutopricingExchangeAuditByExchangeId(Integer exchangeId);
}

