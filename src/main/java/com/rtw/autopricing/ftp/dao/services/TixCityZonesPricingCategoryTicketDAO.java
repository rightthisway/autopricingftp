package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.TixCityZonesPricingCategoryTicket;



public interface TixCityZonesPricingCategoryTicketDAO extends RootDAO<Integer, TixCityZonesPricingCategoryTicket> {

	public Collection<TixCityZonesPricingCategoryTicket> getAllActiveTixCityZonePricingCategoryTickets() throws Exception;
	public List<TixCityZonesPricingCategoryTicket> getAllTixCityZonePricingCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	
	public List<TixCityZonesPricingCategoryTicket> getAllTixCityZonePricingCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<TixCityZonesPricingCategoryTicket> getAllTixCityZonesPricingCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInTixCityZonesPricingCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<TixCityZonesPricingCategoryTicket> getAllTNTixCityZonesPricingCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllTixCityZonesPricingCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllTixCityZonesPricingCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnTixCityZonesPricingCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnTixCityZonesPricingCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllTixCityZonePricingTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<TixCityZonesPricingCategoryTicket> getAllTixCityZonePricingCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
}

