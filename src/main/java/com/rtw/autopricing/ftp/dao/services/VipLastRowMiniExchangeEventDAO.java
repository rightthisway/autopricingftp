package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEvent;


public interface VipLastRowMiniExchangeEventDAO extends RootDAO<Integer, VipLastRowMiniExchangeEvent> {
	public Collection<VipLastRowMiniExchangeEvent> getAllVipLastRowMiniExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	public Collection<VipLastRowMiniExchangeEvent> getAllVipLastRowMiniExchangeEventsByArtistId(Integer artistId)  throws Exception;
	public Collection<VipLastRowMiniExchangeEvent> getAllVipLastRowMiniExchangeEventsByVenueId(Integer venueId)  throws Exception;
	public VipLastRowMiniExchangeEvent getVipLastRowMiniExchangeEventByEventId(Integer venueId)  throws Exception;
	public Collection<VipLastRowMiniExchangeEvent> getAllVipLastRowMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId)  throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType)  throws Exception;
	public List<VipLastRowMiniExchangeEvent> getVipLastRowMiniExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId)  throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<VipLastRowMiniExchangeEvent> getAllVipLastRowMiniExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<VipLastRowMiniExchangeEvent> getAllVipLastRowMiniExchangeEventsByEventIds(String eventIds)  throws Exception;
}
