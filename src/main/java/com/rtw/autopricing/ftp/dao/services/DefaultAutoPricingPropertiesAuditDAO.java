package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.DefaultAutoPricingPropertiesAudit;


public interface DefaultAutoPricingPropertiesAuditDAO extends RootDAO<Integer, DefaultAutoPricingPropertiesAudit>{

	public List<DefaultAutoPricingPropertiesAudit> getDefaultAutoPricingPropertiesAudit(Integer productId,Integer parentCategoryId);
}
