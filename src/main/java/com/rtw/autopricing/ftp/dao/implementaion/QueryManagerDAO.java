package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import com.rtw.autopricing.ftp.data.AutoCats96TnCategoryTicket;
import com.rtw.autopricing.ftp.data.AutoExchangeEventUpdateDTO;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Category;
import com.rtw.autopricing.ftp.data.EventDTO;
import com.rtw.autopricing.ftp.data.SeatGeekUploadCats;
import com.rtw.autopricing.ftp.data.TNDRealCategoryTicket;
import com.rtw.autopricing.ftp.data.TNDZonesCategoryTicket;
import com.rtw.autopricing.ftp.data.TicketListingCrawl;
import com.rtw.autopricing.ftp.indux.data.LarryLastTnCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.VWLastRowMiniCatsCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.VWTGCatsCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.VWVipCatsCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.ZonePricingTnCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.ZonedLastRowTnCategoryTicket;
import com.rtw.autopricing.util.BrokerStatus;
import com.rtw.autopricing.util.BrokerUtils;
import com.rtw.autopricing.util.CategoryTicketDownloader;
import com.rtw.autopricing.util.ZonesPricingFloorCap;


public class QueryManagerDAO {
	private SessionFactory sessionFactory;
	private String tnInduxDbName;
//	private String tn2RotPos;
//	private String tixCityPos;
	private String rtwPos;
	private static Session session = null;
	
	public String getRtwPos() {
		return rtwPos;
	}


	public void setRtwPos(String rtwPos) {
		this.rtwPos = rtwPos;
	}



	public List<Integer> getAllEventsIdswithTnCrawlsOnly() throws Exception {
		  List<Integer> eventIds =null;
		  String sql ="select distinct event_id from ticket_listing_crawl with(nolock) " +
		  		" where site_id='ticketnetworkdirect' and enabled=1" +
		  		" and event_id not in (select event_id from ticket_listing_crawl with(nolock) where site_id!='ticketnetworkdirect' and enabled=1)" ;
		  
		  Query query = null;
//		  Session session = null;
		  try {
//			  session = QueryManagerDAO.getSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
		   query = session.createSQLQuery(sql);
		   eventIds =  query.list();
		  } catch (Exception e) {
		   e.printStackTrace();
		  } finally  {
//		   
		  }
		  return eventIds;
		 }
	

	public List<EventDTO> getAutopricingAutoEvents() throws Exception {
		  List<EventDTO> events =null;
		  String sql ="SELECT  distinct e.id as id,e.name as name,e.event_date as eventDate,e.event_time as eventTime," +
		     " e.event_status as eventStatus," +
		     " v.id as venueId,e.admitone_id as admitoneId,a.id as artistId,e.venue_category_id as venueCategoryId," +
		     " count(tc.id) as noOfCrawls " +
		     " ,a.grand_child_category_id as grandChildCategoryId,pc.id as parentCategoryId,ae.is_zone_event as isZoneEvent," +
		     " ae.EventId as posExchangeEventId,vc.category_group as venueCategoryName,ae.child_category as posChildCategoryName," +
		     " ae.parent_category as posParentCategoryName,v.state as state,a.name as artistName,e.presale_Event as presaleEvent," +
		     " v.country as country,ae.grandchild_Category as posGrandChildCategoryName,e.creation_date as eventCreatedDate  " +
		     " from event e with(nolock)" +
		     " inner join artist a with(nolock) on a.id = e.artist_id" +
		     " inner join venue v with(nolock) on v.id = e.venue_id" +
		     " inner join venue_category vc with(nolock) on vc.id = e.venue_category_id" +
		     " inner join grand_child_tour_category gc with(nolock) on gc.id = a.grand_child_category_id" +
		     " inner join child_tour_category cc with(nolock) on cc.id = gc.child_category_id" +
		     " inner join tour_category pc with(nolock) on pc.id = cc.tour_category_id" +
		     " left join ticket_listing_crawl tc with(nolock) on tc.event_id = e.id" +
		     " left join admitone_event ae with(nolock) on ae.EventId=e.admitone_id" +
		     " where event_status = 'ACTIVE'" +
		     " and v.country in ('US','USA','CA','RU')" +
		     
		     //" and e.id not in (select event_id from tn_exchange_event)" +
		     
		     //" and e.id not in (select event_id from " + propertySetting.getZonesDbUrl() +".tg_cats_removed_category_event)" +
		     
		     " group by e.id ,e.name ,e.event_date ,e.event_time ,e.event_status ," +
		     " v.id,e.admitone_id,a.id ,e.venue_category_id,e.presale_event, v.country," +
		     " e.venue_category_id,a.grand_child_category_id,pc.id,ae.is_zone_event,ae.EventId,vc.category_group,ae.child_category," +
		     " ae.parent_category,v.state,a.name,ae.grandchild_Category,e.creation_date" ;
		  
		  Query query = null;
//		  Session session = null;
		  try {
//			  session = QueryManagerDAO.getSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
		   query = session.createSQLQuery(sql);
		   events =  query.setResultTransformer(Transformers.aliasToBean(EventDTO.class)).list();
		  } catch (Exception e) {
		   e.printStackTrace();
		  } finally  {
//		   
		  }
		  return events;
		 }
	
	
	public Integer insertDataintoTmatInduxEventDetailsTable() throws Exception {
		
		 Query query = null;
//		  Session session = null;
		 int  count = 0;
		  try {
//			  session = QueryManagerDAO.getSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				query = session.createSQLQuery("{CALL SP_Insert_data_into_tmat_indux_event_details}");
				count =  query.executeUpdate();
		   /*if(list != null && !list.isEmpty() && list.get(0) != null) {
			  count = Integer.parseInt(list.get(0)[0].toString());
		   }*/
		  } catch (Exception e) {
		   e.printStackTrace();
		   throw e;
		  } finally  {
//		   
		  }
		  return count;
	}
	
	
	public List<AutoExchangeEventUpdateDTO> getAutoExchangeEventUpdateList() throws Exception {
		  List<AutoExchangeEventUpdateDTO> events =null;

		  String sql ="select distinct e.id as eventId,e.admitone_id as posExchangeEventId,ae.parent_category as posParentCategoryName," +
		  		" ae.child_category as posChildCategoryName,ae.grandchild_category as posGrandChildCategoryName,ae.is_zone_event as isZoneEvent" +
		  		" from event e" +
		  		" inner join admitone_event ae on ae.EventId=e.admitone_id" +
		  		" inner join tmat_indux_event_details te on te.event_id=e.id" +
		  		" where e.event_status='ACTIVE' and(te.pos_exchange_event_id != e.admitone_id or te.pos_parent_category != ae.parent_category " +
		  		" or te.pos_child_category != ae.child_category or te.pos_grandchild_category != ae.grandchild_category " +
		  		" or te.is_zone_event != ae.is_zone_event)";
		     
		  
		  Query query = null;
//		  Session session = null;
		  try {
//			  session = QueryManagerDAO.getSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
		   query = session.createSQLQuery(sql);
		   events =  query.setResultTransformer(Transformers.aliasToBean(AutoExchangeEventUpdateDTO.class)).list();
		  } catch (Exception e) {
		   e.printStackTrace();
		   throw e;
		  } finally  {
//		   
		  }
		  return events;
		 }
	
	public  List<Category> getAllCategoriesByVenueIdAndCategoryGroup(Integer venueId, String categoryGroup) throws Exception {
		String sql=" SELECT c.id,c.symbol,c.description,c.group_name as groupName,c.equal_cats as equalCats,c.normalized_zone as normalizedZone,";
			sql += " c.seat_quantity as seatQuantity,c.category_capacity as categoryCapacity,c.venue_category_id as venueCategoryId FROM category_new c "; 
			sql += " INNER JOIN category_mapping_new cm on cm.category_id = c.id ";
			sql += " INNER JOIN venue_category vc on vc.id = c.venue_category_id "; 
			sql += " WHERE vc.venue_id = " + venueId ;
			sql += " AND vc.category_group = '" + categoryGroup  + "'";
			sql += " GROUP BY c.id,c.symbol,c.description,c.group_name,c.equal_cats,c.normalized_zone, "; 
			sql += " c.seat_quantity,c.category_capacity,c.venue_category_id "; 
			sql += " ORDER BY min(cm.id) "; 

		Session session=null;
		List<Category> list = new ArrayList<Category>();
		try{
			session=sessionFactory.getCurrentSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(Category.class)).list();
		}catch (Exception e) {
			// TODO: handle exception
			throw e;
		}finally{
			
		}
		return list;
	}
	
	public  List<Category> getAllCategoriesByVenueCategoryId(Integer venueCategoryId) throws Exception {
		String sql=" SELECT c.id,c.symbol,c.description,c.group_name as groupName,c.equal_cats as equalCats,c.normalized_zone as normalizedZone,";
		sql += " c.seat_quantity as seatQuantity,c.category_capacity as categoryCapacity,c.venue_category_id as venueCategoryId FROM category_new c "; 
			sql += " LEFT JOIN category_mapping_new cm on cm.category_id = c.id ";
			sql += " LEFT JOIN venue_category vc on vc.id = c.venue_category_id "; 
			sql += " WHERE vc.id = " + venueCategoryId ;
			sql += " GROUP BY c.id,c.symbol,c.description,c.group_name,c.equal_cats,c.normalized_zone, "; 
			sql += " c.seat_quantity,c.category_capacity,c.venue_category_id "; 
			sql += " ORDER BY min(cm.id) "; 

		Session session=null;
		List<Category> list = new ArrayList<Category>();
		try{
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(Category.class)).list();
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			
		}
		return list;
	}
	/*public List<EventDTO> getTNTGCatsAutoEvents(){
		List<EventDTO> events =null;
//		String tmat = "tmtdb3.[tmatprodscale].[dbo]";
//		String zones = "tmtdb2.zoneplatform.[dbo]";
		String sql ="SELECT distinct  e.id as id,e.name as name,e.event_date as eventDate,e.event_time as eventTime," +
					" e.event_status as eventStatus," +
					" v.id as venueId,e.admitone_id as admitoneId,a.id as tourId,e.venue_category_id as venueCategoryId," +
					" count(tc.id) as noOfCrawls " +
					" ,a.grand_child_category_id as grandChildCategoryId,ae.is_zone_event as isZoneEvent" +
					" from event e" +
					" inner join artist a on a.id = e.artist_id" +
					" inner join venue v on v.id = e.venue_id" +
					" inner join grand_child_tour_category gc on gc.id = a.grand_child_category_id" +
					" inner join child_tour_category cc on cc.id = gc.child_category_id" +
					" inner join tour_category pc on pc.id = cc.tour_category_id" +
					" left join ticket_listing_crawl tc on tc.event_id = e.id" +
					" left join admitone_event ae on ae.EventId=e.admitone_id" +
					" where event_status = 'ACTIVE' and e.broker_id is not null" +
					
					//" and e.id not in (select event_id from tn_exchange_event)" +
					
					//" and e.id not in (select event_id from " + propertySetting.getZonesDbUrl() +".tg_cats_removed_category_event)" +
					
					" group by e.id ,e.name ,e.event_date ,e.event_time ,e.event_status ," +
					" v.id,e.admitone_id,a.id ,e.venue_category_id," +
					" e.venue_category_id,a.grand_child_category_id,ae.is_zone_event" ;
		
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(EventDTO.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			
		}
		return events;
	}*/
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	public String getTnInduxDbName() {
		return tnInduxDbName;
	}

	public void setTnInduxDbName(String tnInduxDbName) {
		this.tnInduxDbName = tnInduxDbName;
	}

	public List<Object[]> getTNSpeacialCsv() throws Exception {
		
		Session session = sessionFactory.openSession();
		
		try{
			String sql ="select distinct 'Y' as Edit,e.name as Event,v.building + ','+ v.city +','+ v.state  as Venue, " +
						" CONVERT(VARCHAR(8), e.event_date, 1) as EventDate,CONVERT(VARCHAR(8), e.event_time, 108) as EventTime," +
						" t.quantity as Quantity,t.normalized_section as Section,t.row as Row, " +
						" '' as SeatFrom,'' as SeatThru,'TNSP' as Notes,CONVERT(DECIMAL(10,2),1.15*t.current_price) as Cost,t.id as TicketId, " +
						" '' as TicketGroupTypeID, '' as	TicketGroupStockTypeID,'' as ShippingMethodSpecialID, " +
						" '' as ShowNearTermOptionID, '' as InHandDate	,0 as TicketGroupObstructionDescriptionID " +
						" from ticket t " +
						" INNER JOIN event e on e.id = t.event_id " +
						" INNER JOIN venue v on v.id = e.venue_id " +
						" WHERE site_id ='ticketevolution' " +
						" AND e.event_status ='ACTIVE' and t.ticket_status ='ACTIVE' " +
						" AND seller like 'Golden Tickets'  " +
						" AND datediff(day,GETDATE(),e.event_date)>3 " +
						" ORDER BY e.name,CONVERT(VARCHAR(8), e.event_date, 1),CONVERT(VARCHAR(8), e.event_time, 108), " +
						" t.quantity,t.normalized_section,t.row";
			Query queryString = session.createSQLQuery(sql);
			List<Object[]> list = queryString.list();
			return list;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			
		}
		//return null;
		
	}


	
	
	public List<VWTGCatsCategoryTicket> getTnTgCategoryTickets(String brokerId) throws Exception {
		
//		System.out.println("In Mini cat");
//		Session session = null;
	    List<VWTGCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
		
//		String  brokerLinkedServer = "";
		
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		String serverName ="";
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
		try{
			
		queryString.append(" select distinct e.event_name as EventName, ");
		queryString.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
		queryString.append(" CASE WHEN e.event_datetime is not null THEN"); 
		queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))"); 
		queryString.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
		queryString.append(" v.name as VenueName,vc.section_low as Section,vc.row_low as RowRange,tg.ticket_count as Quantity,retail_price as TnPrice,e.event_datetime ");
		queryString.append(" from "+serverName+".category_ticket_group tg ");
		queryString.append(" inner join "+serverName+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
		queryString.append(" inner join "+serverName+".event e on e.event_id=tg.event_id ");
		queryString.append(" inner join "+serverName+".venue v on v.venue_id=e.venue_id ");
		queryString.append(" where internal_notes='MINICATS' and retail_price>0 and broadcast=1 ");
		queryString.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+serverName+".category_ticket where invoice_id is not null) "); 
		queryString.append(" order by e.event_datetime,EventName,Section,RowRange,Quantity ");
		
		System.out.println("download category "+queryString.toString());
		
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("RowRange", new StringType()).addScalar("TnPrice", new DoubleType());
		
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWTGCatsCategoryTicket.class)).list();
	    tickets=query.list();    
	   
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			
		}
		
		 return tickets;
			
		}
	public List<VWTGCatsCategoryTicket> getTnPresaleAutoCategoryTickets(String brokerId) throws Exception {
		
//			System.out.println("In Mini cat");
//		Session session = null;
	    List<VWTGCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
		
//		String  brokerLinkedServer = "";
		
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		String serverName ="";
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
		try{
			
			
			queryString.append(" select e.event_name as EventName,format(e.event_datetime, 'yyyy-MM-dd') as EventDate, ");
			queryString.append(" format(e.event_datetime, 'HH:MM:ss') as EventTime,v.name as VenueName,ctg.ticket_count as Quantity,vc.section_low as Section, ");
			queryString.append(" vc.row_low as RowRange,vc.seat_low as Seat,ctg.retail_price as TnPrice,pact.id as TmatTicketGroupId ");
			queryString.append(" from "+serverName+".category_ticket_group ctg ");
			queryString.append(" inner join "+serverName+".event e on e.event_id=ctg.event_id ");
			queryString.append(" inner join "+serverName+".venue_category vc on vc.venue_category_id = ctg.venue_category_id ");
			queryString.append(" inner join "+serverName+".venue v on v.venue_id = e.venue_id ");
			queryString.append(" left join presale_autocat_category_ticket pact on ctg.category_ticket_group_id = pact.tn_category_ticket_group_id ");
			queryString.append(" left join presale_autocat_exchange_event ee on ee.event_id = pact.event_id ");
			queryString.append(" left join event te on te.id = ee.event_id ");
			queryString.append(" WHERE ctg.broadcast = 1 and ctg.internal_notes like 'AUTOCAT' ");
			
			
						
		System.out.println("download category "+queryString.toString());
		
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("RowRange", new StringType()).addScalar("TnPrice", new DoubleType()).addScalar("RowRange", new StringType()).addScalar("TmatTicketGroupId", new StringType()).addScalar("Seat", new StringType());
		
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWTGCatsCategoryTicket.class)).list();
	    tickets=query.list();    
	   
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			
		}
		
		 return tickets;
			
}
		
	public List<VWTGCatsCategoryTicket> getScoreBigPresaleAutoCategoryTickets(String brokerId) throws Exception {
		

//		System.out.println("In Mini cat");
//		Session session = null;
	    List<VWTGCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
		
//		String  brokerLinkedServer = "";
		
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		/*String serverName ="";
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}*/
		try{
			
			
			queryString.append(" select e.name as EventName,format(e.event_date, 'yyyy-MM-dd') as EventDate ,format(e.event_date, 'HH:MM:ss') as EventTime,v.building as VenueName,ctg.quantity as Quantity,ctg.section as Section,ctg.row_range as RowRange,ctg.actual_price as TnPrice,ctg.id as TmatTicketGroupId ");
			queryString.append(" from presale_autocat_category_ticket ctg ");
			queryString.append(" inner join presale_autocat_exchange_event ee on ee.event_id = ctg.event_id ");
			queryString.append(" inner join event e on e.id = ee.event_id ");
			queryString.append(" inner join venue v on v.id = e.venue_id ");
			queryString.append(" inner join artist a WITH(NOLOCK) on a.id= e.artist_id  ");
			queryString.append(" inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			queryString.append(" inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id ");
			queryString.append(" inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			queryString.append(" WHERE  ee.scorebig_broker_id = " +brokerId+ " and e.event_status ='ACTIVE' ");
			queryString.append(" and tc.id <> 1 and datediff(day,getdate(),e.event_date)>7 and ctg.vivid_price>0 ");
			queryString.append(" and ctg.section not like 'ZONE%' and (ctg.section IS NOT NULL AND ltrim(ctg.section) != '') ");
			queryString.append(" and ctg.quantity in (2,4,6) and ee.status='ACTIVE' order by e.event_date,e.event_time,e.name,ctg.section,ctg.quantity ");
		
			
			
						
		System.out.println("download category "+queryString.toString());
		
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("RowRange", new StringType()).addScalar("TnPrice", new DoubleType()).addScalar("RowRange", new StringType()).addScalar("TmatTicketGroupId", new StringType());
		
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWTGCatsCategoryTicket.class)).list();
	    tickets=query.list();    
	   
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			
		}
		
		 return tickets;
			
		}
public List<VWTGCatsCategoryTicket> getVividPresaleAutoCategoryTickets(String brokerId) throws Exception {
		

//		System.out.println("In Mini cat");
//		Session session = null;
	    List<VWTGCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
		
//		String  brokerLinkedServer = "";
		
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		/*String serverName ="";
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}*/
		try{
			
			
			queryString.append(" select e.name as EventName,format(e.event_date, 'yyyy-MM-dd') as EventDate ,format(e.event_date, 'HH:MM:ss') as EventTime,v.building as VenueName,ctg.quantity as Quantity,ctg.section as Section,ctg.row_range as RowRange,ctg.actual_price as TnPrice,ctg.id as TmatTicketGroupId ");
			queryString.append(" from presale_autocat_category_ticket ctg ");
			queryString.append(" inner join presale_autocat_exchange_event ee on ee.event_id = ctg.event_id ");
			queryString.append(" inner join event e on e.id = ee.event_id ");
			queryString.append(" inner join venue v on v.id = e.venue_id ");
			queryString.append(" inner join artist a WITH(NOLOCK) on a.id= e.artist_id  ");
			queryString.append(" inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			queryString.append(" inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id ");
			queryString.append(" inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			queryString.append(" WHERE  ee.vivid_broker_id = " +brokerId+ " and e.event_status ='ACTIVE' ");
			queryString.append(" and tc.id <> 1 and datediff(day,getdate(),e.event_date)>7 and ctg.vivid_price>0 ");
			queryString.append(" and ctg.section not like 'ZONE%' and (ctg.section IS NOT NULL AND ltrim(ctg.section) != '') ");
			queryString.append(" and ctg.quantity in (2,4,6) and ee.status='ACTIVE' order by e.event_date,e.event_time,e.name,ctg.section,ctg.quantity ");
			
			
					
						
		System.out.println("download category "+queryString.toString());
		
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("RowRange", new StringType()).addScalar("TnPrice", new DoubleType()).addScalar("RowRange", new StringType()).addScalar("TmatTicketGroupId", new StringType());
		
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWTGCatsCategoryTicket.class)).list();
	    tickets=query.list();    
	   
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			
		}
		
		 return tickets;
			
		}
	public List<VWVipCatsCategoryTicket> getVwVipCatsCategoryTicketDAO(String brokerId)
	 throws Exception {
		System.out.println("In VIP MINI ");
		
//		Session session = null;
		List<VWVipCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
	    String  brokerLinkedServer = "";
		
	   /* if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
	    
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
	    
		try
		{
		queryString.append(" select distinct e.event_name as EventName,");
		queryString.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
		queryString.append(" CASE WHEN e.event_datetime is not null THEN "); 
		queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7)) ");
		queryString.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
		queryString.append(" v.name as VenueName,vc.section_low as Section,vc.row_low as AlternateRow,tg.ticket_count as Quantity,retail_price as TnPrice,e.event_datetime ");
		queryString.append(" from "+brokerLinkedServer+".category_ticket_group tg "); 
		queryString.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
		queryString.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
		queryString.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
		queryString.append(" where internal_notes='VIPMINICATS' and retail_price>0 and broadcast=1 ");
		queryString.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null) "); 
		queryString.append(" order by e.event_datetime,EventName,section,AlternateRow,Quantity");
		

		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		System.out.println(" download category "+queryString.toString());
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("AlternateRow", new StringType()).addScalar("TnPrice", new DoubleType());
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWVipCatsCategoryTicket.class)).list();
	    tickets=query.list();  
	    
	   
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally{
			
		}
		return tickets;
		
	}
	public List<VWLastRowMiniCatsCategoryTicket> getTnLastRowMiniCatsCatsCategoryTickets(String brokerId) throws Exception {
		
		System.out.println("In Last Row MINI");
//		Session session = null;
		List<VWLastRowMiniCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
		String   brokerLinkedServer = "";
			
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
		try{
		queryString.append("select distinct e.event_name as EventName,");
		queryString.append("CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
	    queryString.append("CASE WHEN e.event_datetime is not null THEN "); 
		queryString.append("LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7)) ");
		queryString.append("WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
		queryString.append("v.name as VenueName,vc.section_low as Section,vc.row_low as LastRow,tg.ticket_count as Quantity,retail_price as TnPrice,e.event_datetime ");
		queryString.append("from "+brokerLinkedServer+".category_ticket_group tg "); 
		queryString.append("inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
		queryString.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
		queryString.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
		queryString.append("where internal_notes='LASTROW MINICATS' and retail_price>0 and broadcast=1 ");
		queryString.append("and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null) "); 
		queryString.append("order by e.event_datetime,EventName,Section,LastRow,Quantity");
		
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("TnPrice", new DoubleType()).addScalar("LastRow", new StringType());
		System.out.println("query"+query);
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWLastRowMiniCatsCategoryTicket.class)).list();
	    tickets=query.list();  
	    

		}catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally{
			
		}
		return tickets;
	}
	
	// Mehul : added for vip last row product
public List<VWLastRowMiniCatsCategoryTicket> getTnVipLastRowMiniCatsCatsCategoryTickets(String brokerId) throws Exception {
		
		System.out.println("In Last Row MINI");
//		Session session = null;
		List<VWLastRowMiniCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
		String   brokerLinkedServer = "";
			
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
		try{
		queryString.append("select distinct e.event_name as EventName,");
		queryString.append("CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
	    queryString.append("CASE WHEN e.event_datetime is not null THEN "); 
		queryString.append("LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7)) ");
		queryString.append("WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
		queryString.append("v.name as VenueName,vc.section_low as Section,vc.row_low as LastRow,tg.ticket_count as Quantity,retail_price as TnPrice,e.event_datetime ");
		queryString.append("from "+brokerLinkedServer+".category_ticket_group tg "); 
		queryString.append("inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
		queryString.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
		queryString.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
		queryString.append("where internal_notes='VIPLR' and retail_price>0 and broadcast=1 ");
		queryString.append("and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null) "); 
		queryString.append("order by e.event_datetime,EventName,Section,LastRow,Quantity");
		
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("TnPrice", new DoubleType()).addScalar("LastRow", new StringType());
		System.out.println("query"+query);
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWLastRowMiniCatsCategoryTicket.class)).list();
	    tickets=query.list();  
	    

		}catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally{
			
		}
		return tickets;
	}

	public List<ZonePricingTnCategoryTicket> getTnZonesCategoryTickets(String brokerId) throws Exception {
		
		System.out.println("In Zones Categrry");
//		Session session = null;
		StringBuilder queryString = new StringBuilder();
		List<ZonePricingTnCategoryTicket> tickets=null;
		String  brokerLinkedServer = "";
		String internalNotes="";
		    
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
			if(broker.getId()==2) {
				internalNotes="'SCZP'";
			}else if(broker.getId()==3){
				internalNotes="'SCZP'";
			} else if(broker.getId()==5){
				internalNotes="'SCZP'";
			}
		}
		
		
			try
			{
				queryString.append(" select distinct e.event_name as EventName,");
				queryString.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
				queryString.append(" CASE WHEN e.event_datetime is not null THEN");
				queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))"); 
				queryString.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
				queryString.append(" v.name as Venue,vc.section_low as Section,vc.row_low as Row,tg.ticket_count as Quantity,retail_price as Price,e.event_datetime");
				queryString.append(" from "+brokerLinkedServer+".category_ticket_group tg"); 
				queryString.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id");
				queryString.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
				queryString.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
			   	queryString.append(" where internal_notes="+internalNotes+" and retail_price>0 and broadcast=1");
			   	queryString.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)"); 
			    queryString.append(" order by e.event_datetime,EventName,section,row,Quantity");

			    if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}	
			
				Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType());
				System.out.println("query"+query);
				tickets=query.setResultTransformer(Transformers.aliasToBean(ZonePricingTnCategoryTicket.class)).list();
			    tickets=query.list();  
			    
			    
			    
			}catch(Exception e)
			{
				e.printStackTrace();
				throw e;
			}finally
			{
				
			}
		
		return tickets;
	}
public List<ZonePricingTnCategoryTicket> getTnTixCityZonesCategoryTickets(String brokerId) throws Exception {
		
//		Session session = null;
		StringBuilder queryString = new StringBuilder();
		List<ZonePricingTnCategoryTicket> tickets=null;
		String  brokerLinkedServer = "";
		String internalNotes = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RTWZonesPricing").getInternalNotes();
		    
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
			try
			{
				queryString.append(" select distinct e.event_name as EventName,");
				queryString.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
				queryString.append(" CASE WHEN e.event_datetime is not null THEN");
				queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))"); 
				queryString.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
				queryString.append(" v.name as Venue,vc.section_low as Section,vc.row_low as Row,tg.ticket_count as Quantity,retail_price as Price,e.event_datetime");
				queryString.append(" from "+brokerLinkedServer+".category_ticket_group tg"); 
				queryString.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id");
				queryString.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
				queryString.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
			   	queryString.append(" where internal_notes='"+internalNotes+"' and retail_price>0 and broadcast=1");
			   	queryString.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)"); 
			    queryString.append(" order by e.event_datetime,EventName,section,row,Quantity");

			    if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}	
			
				Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType());
				tickets=query.setResultTransformer(Transformers.aliasToBean(ZonePricingTnCategoryTicket.class)).list();
			    tickets=query.list();  
			    
			    
			    
			}catch(Exception e)
			{
				e.printStackTrace();
				throw e;
			}finally
			{
				
			}
		
		return tickets;
	}

public List<ZonePricingTnCategoryTicket> getTnManhattanZonesCategoryTickets(String brokerId) throws Exception {
	
//	Session session = null;
	StringBuilder queryString = new StringBuilder();
	List<ZonePricingTnCategoryTicket> tickets=null;
	String  brokerLinkedServer = "";
	String internalNotes = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MHZonePricing").getInternalNotes();
	    
	if(brokerId!=null && !brokerId.isEmpty()){
		Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
	}
		try
		{
			queryString.append(" select distinct e.event_name as EventName,");
			queryString.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
			queryString.append(" CASE WHEN e.event_datetime is not null THEN");
			queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))"); 
			queryString.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
			queryString.append(" v.name as Venue,vc.section_low as Section,vc.row_low as Row,tg.ticket_count as Quantity,retail_price as Price,e.event_datetime");
			queryString.append(" from "+brokerLinkedServer+".category_ticket_group tg"); 
			queryString.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id");
			queryString.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
			queryString.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
		   	queryString.append(" where internal_notes='"+internalNotes+"' and retail_price>0 and broadcast=1");
		   	queryString.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)"); 
		    queryString.append(" order by e.event_datetime,EventName,section,row,Quantity");

		    if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}	
		
			Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType());
			tickets=query.setResultTransformer(Transformers.aliasToBean(ZonePricingTnCategoryTicket.class)).list();
		    tickets=query.list();  
		    
		    
		    
		}catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}finally
		{
			
		}
	
	return tickets;
}
	
	public List<CategoryTicketDownloader> getZoneTicketProcessorCategoryTickets(String parentCategory)  throws Exception {
		
//		Session session = null;
		StringBuilder queryString = new StringBuilder();
		List<CategoryTicketDownloader> tickets=null;
		try {
			queryString.append(" select e.name as eventName,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
					" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))" +
					" WHEN e.event_time is null THEN 'TBD' END as eventTime,v.building as venueName,ztc.section as section," +
					" ztc.quantity as quantity,ztc.priority as priority,ztc.zone_price as zoneTicketPrice,ztc.section_range as sectionRange," +
					" ztc.row_range as rowRange," +
					" CASE WHEN ztc.ticket_delivery_type != 'FEDEX' THEN 'Email' ELSE ztc.ticket_delivery_type END as ticketDeliveryType" +
					" from zone_tickets_category_ticket_group ztc with(nolock) " +
					" inner join zone_tickets_processor_exchange_event tn with(nolock) on tn.event_id=ztc.event_id" +
					" INNER JOIN event e with(nolock) on e.id = ztc.event_id" +
					" inner join venue v with(nolock) on v.id=e.venue_id" +
					" inner join artist a with(nolock) on a.id=e.artist_id" +
					" inner join grand_child_tour_category gc with(nolock) on gc.id=a.grand_child_category_id" +
					" inner join child_tour_category cc with(nolock) on cc.id=gc.child_category_id" +
					" inner join tour_category tc with(nolock) on tc.id=cc.tour_category_id" +
					" where ztc.status='ACTIVE' and tc.name='"+parentCategory+"'" +
					" order by e.event_date,e.event_time,e.name,ztc.section,ztc.quantity,ztc.priority");

			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}	
			
				SQLQuery sqlQuery = session.createSQLQuery(queryString.toString());
				sqlQuery.addScalar("eventName",Hibernate.STRING);
				sqlQuery.addScalar("eventDate",Hibernate.STRING);
				sqlQuery.addScalar("eventTime",Hibernate.STRING);
				sqlQuery.addScalar("venueName",Hibernate.STRING);
				sqlQuery.addScalar("section",Hibernate.STRING);
				sqlQuery.addScalar("quantity",Hibernate.INTEGER);
				sqlQuery.addScalar("priority",Hibernate.INTEGER);
				sqlQuery.addScalar("zoneTicketPrice",Hibernate.DOUBLE);
				sqlQuery.addScalar("rowRange",Hibernate.STRING);
				sqlQuery.addScalar("sectionRange",Hibernate.STRING);
				sqlQuery.addScalar("ticketDeliveryType",Hibernate.STRING);
				
				tickets=sqlQuery.setResultTransformer(Transformers.aliasToBean(CategoryTicketDownloader.class)).list();
			    tickets=sqlQuery.list();  
			    
			    
			    
			}catch(Exception e)
			{
				e.printStackTrace();
				throw e;
			}finally
			{
				
			}
		
		return tickets;
	}
	public List<ZonedLastRowTnCategoryTicket> getTnZonedLastRowCategoryTickets(String brokerId) throws Exception {
		
		System.out.println("In Zoned Last Row Categrry");
//		Session session = null;
		StringBuilder queryString = new StringBuilder();
		List<ZonedLastRowTnCategoryTicket> tickets=null;
		String  brokerLinkedServer = "";
		
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
		
			try{
				queryString.append(" select distinct e.event_name as EventName,");
				queryString.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
				queryString.append(" CASE WHEN e.event_datetime is not null THEN"); 
				queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))"); 
				queryString.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
				queryString.append(" v.name as Venue,section as Section,row as Row,tg.original_ticket_count as Quantity,retail_price as Price");
				queryString.append(" ,e.event_datetime");
				queryString.append(" from "+brokerLinkedServer+".ticket_group tg"); 
				queryString.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
				queryString.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
				queryString.append(" where internal_notes like 'ZLR NOSTUB NOTNOW NOVIVID NOTEVO' and retail_price>0 ");
				queryString.append(" and ticket_group_id in (select distinct ticket_group_id from "+brokerLinkedServer+".ticket_group_distribution_method)");
				queryString.append(" and ticket_group_id not in(select ticket_group_id from "+brokerLinkedServer+".ticket where invoice_id is not null)"); 
				queryString.append(" order by e.event_datetime,EventName,section,row,Quantity");
		
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}	
				Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType());
				System.out.println("query"+query);
				tickets=query.setResultTransformer(Transformers.aliasToBean(ZonedLastRowTnCategoryTicket.class)).list();
			    tickets=query.list();  
			}catch(Exception e){
				e.printStackTrace();
				throw e;
			}finally{
				
			}	
		return tickets;
	}

	public List<LarryLastTnCategoryTicket> getTnLarryLastCategoryTickets(String brokerId) throws Exception {

		
		System.out.println("In Larry Last Row Categrry");
//		Session session = null;
		StringBuilder queryString = new StringBuilder();
		List<LarryLastTnCategoryTicket> tickets=null;
		String  brokerLinkedServer = "";
		
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
			
			try{
				
				queryString.append(" select distinct e.event_name as EventName,");
				queryString.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
				queryString.append(" CASE WHEN e.event_datetime is not null THEN"); 
			    queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))"); 
			    queryString.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
			    queryString.append(" v.name as Venue,section as Section,row as Row,tg.original_ticket_count as Quantity,retail_price as Price,e.event_datetime");
			    queryString.append(" from "+brokerLinkedServer+".ticket_group tg"); 
			    queryString.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
				queryString.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
			    queryString.append(" where internal_notes like 'LarryLast NOSTUB NOTNOW NOVIVID NOTEVO' and retail_price>0"); 
			    queryString.append(" and ticket_group_id in (select distinct ticket_group_id from "+brokerLinkedServer+".ticket_group_distribution_method)");
			    queryString.append(" and ticket_group_id not in(select ticket_group_id from "+brokerLinkedServer+".ticket where invoice_id is not null)"); 
			    queryString.append(" order by e.event_datetime,EventName,section,row,Quantity");
				
				
				
			    if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}	
				Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType());
				System.out.println("query"+query);
				tickets=query.setResultTransformer(Transformers.aliasToBean(LarryLastTnCategoryTicket.class)).list();
			    tickets=query.list();  
			}catch(Exception e){
				e.printStackTrace();
				throw e;
			}finally{
				
			}	
			
			return tickets;
	}	
	public List<Object[]> getCategoryTicketListingCount(String brokerId) throws Exception {
		List<Object[]> list = null;
		String  brokerLinkedServer = "";
//		Session session = null;
		StringBuilder queryString = new StringBuilder();
		
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
		try
		{
		queryString.append(" select count(category_ticket_group_id) as count,'TotaLisitngs' as Type,internal_notes as ProductType from "+brokerLinkedServer+".category_ticket_group ctg");
		queryString.append(" where retail_price > 0 and category_ticket_group_id not in(");
		queryString.append(" select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)");
		queryString.append(" and internal_notes in('MINICATS','LASTROW MINICATS','VIPMINICATS','ZP')");
		queryString.append(" group by internal_notes union all");
		queryString.append(" select count(category_ticket_group_id) as count, 'BroadCastedListings' as  Type,internal_notes as ProductType from "+brokerLinkedServer+".category_ticket_group ctg");
		queryString.append(" where retail_price > 0 and broadcast=1 and category_ticket_group_id not in(");
		queryString.append(" select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)");
		queryString.append(" and internal_notes in('MINICATS','LASTROW MINICATS','VIPMINICATS','ZP')");
		queryString.append(" group by internal_notes");
		
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}	
		Query query = session.createSQLQuery(queryString.toString());
		System.out.println("query"+query);
		list=query.list();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			
		}	
		return list;
	}
	public List<Object[]> getTicketGroupListingCount(String brokerId) throws Exception {
		
		String  brokerLinkedServer = "";
		List<Object[]> list = null;
//		Session session = null;
		StringBuilder queryString = new StringBuilder();
		
		/*if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		}*/
		
		Broker broker = null;
		if(brokerId!=null && !brokerId.isEmpty()){
			broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
		
		if(broker == null) {
			return null;
		}
			try{

				queryString.append(" select count(ticket_group_id) as count,'TotaLisitngs' as Type,internal_notes as ProductType from "+brokerLinkedServer+".ticket_group"); 
				queryString.append(" where retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" and ticket_group_id not in(");
				queryString.append(" select ticket_group_id from "+brokerLinkedServer+".ticket where invoice_id is not null)");
				queryString.append(" and internal_notes in('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')");
				queryString.append(" group by internal_notes");
				queryString.append(" union all");
				queryString.append(" select count(ticket_group_id) as count, 'BroadCastedListings' as  Type,internal_notes as ProductType from "+brokerLinkedServer+".ticket_group");
				queryString.append(" where retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" and ticket_group_id not in(");
				queryString.append(" select ticket_group_id from "+brokerLinkedServer+".ticket where invoice_id is not null)");
				queryString.append(" and internal_notes in('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')");
				queryString.append(" and ticket_group_id in(select distinct ticket_group_id from "+brokerLinkedServer+".ticket_group_distribution_method)");
				queryString.append(" group by internal_notes");
				
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}	
				Query query = session.createSQLQuery(queryString.toString());
				System.out.println("query"+query);
				list=query.list();
			}catch(Exception e){
				e.printStackTrace();
				throw e;
			}	
		return list;
	}	
	
	/*public List<Object[]> getRotCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
		
		String posUrl = tn2RotPos;
		
		Session session = null;
		List<Object[]> list = null;
		StringBuilder queryString = new StringBuilder();
		
		try{
//			queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,vc.section_low as Section,");
//			queryString.append("vc.row_low as Row,ct.category_ticket_group_id as CategoryTicketGroupId,i.invoice_id as InvoiceId,");
//			queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,tg.retail_price as RetailPrice,");
//			queryString.append("ct.actual_price as ActualPrice,i.invoice_total as InvoiceTotal,p.po_total as POTotal,tg.internal_notes,");
//			queryString.append("ccb.client_broker_company_name,c.client_type_desc from category_ticket ct inner join category_ticket_group tg on ");
//			queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id inner join invoice i on i.invoice_id = ct.invoice_id ");
//			queryString.append("inner join venue_category vc on vc.venue_category_id = tg.venue_category_id inner join event e on ");
//			queryString.append("e.event_id = tg.event_id left join ticket t on t.ticket_id  = ct.ticket_id ");
//			queryString.append("left join (select cb.client_broker_id,ii.invoice_id,cb.client_broker_company_name from invoice ii ");
//			queryString.append("inner join client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
//			queryString.append("inner join client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id ");
//			queryString.append("left join (select cy.client_type_desc,ci.invoice_id from client c inner join client_invoice ci on ");
//			queryString.append("c.client_id=ci.client_id inner join client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id ");
//			queryString.append("left join purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
			
			queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
			queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
			queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
			queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
			queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
			queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
			queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
			queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+posUrl+".category_ticket ct inner join "+posUrl+".category_ticket_group tg on "); 
			queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
			queryString.append("inner join "+posUrl+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
			queryString.append("UNION ");
			queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
			queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
			queryString.append("from "+posUrl+".ticket t inner join "+posUrl+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NoStub','ZLR NOStub')) as ctgg "); 
			queryString.append("inner join "+posUrl+".invoice i on i.invoice_id = ctgg.invoice_id ");
			queryString.append("inner join "+posUrl+".event e on e.event_id = ctgg.event_id left join "+posUrl+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
			queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+posUrl+".invoice ii ");
			queryString.append("inner join "+posUrl+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
			queryString.append("inner join "+posUrl+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
			queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+posUrl+".client c inner join "+posUrl+".client_invoice ci on ");
			queryString.append("c.client_id=ci.client_id inner join "+posUrl+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
			queryString.append("left join "+posUrl+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
			
			
			if(reportType.equalsIgnoreCase("AUTO")){
				queryString.append("ctgg.internal_notes ='AUTO' and ");
			}else if(reportType.equalsIgnoreCase("MINICATS")){
				queryString.append("ctgg.internal_notes = 'MINICATS' and ");
			}else if(reportType.equalsIgnoreCase("VIPCATS")){
				queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
			}else if(reportType.equalsIgnoreCase("VIPAUTO")){
				queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
			}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
				queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
			}else if(reportType.equalsIgnoreCase("LARRYLAST")){
				queryString.append("ctgg.internal_notes = 'LarryLast' and ");
			}else if(reportType.equalsIgnoreCase("ZONES")){
				queryString.append("ctgg.internal_notes !='AUTO' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
				queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
			}
			else if(reportType.equalsIgnoreCase("ZONES PRICING")){
				queryString.append("ctgg.internal_notes = 'ZP' and ");
			}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
				queryString.append("ctgg.internal_notes = 'ZLR NOStub' and ");
			}else if(reportType.equalsIgnoreCase("MANUAL")){
				queryString.append("ctgg.internal_notes !='AUTO' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
				queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
			}else if(reportType.equalsIgnoreCase("VIVID MINICATS")){
				queryString.append("ccb.client_broker_id=4759 and ");
			}else if(reportType.equalsIgnoreCase("VIVID LARRY")){
				queryString.append("ccb.client_broker_id=4760 and ");
			}else if(reportType.equalsIgnoreCase("SCOREBIG MINICATS")){
				queryString.append("ccb.client_broker_id=4763 and ");
			}
//			/*else if(reportType.equalsIgnoreCase("SCOREBIG AUTO")){
//				queryString.append("ccb.client_broker_id=4761 and ");
//			}* /
			else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
				queryString.append("ccb.client_broker_id=4761 and ");
			}
			
			if(poInfo.equalsIgnoreCase("WITHPO")){
				queryString.append("p.purchase_order_id is not null and ");
			}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
				queryString.append("p.purchase_order_id is null and ");
			}
				
			queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
			System.out.println("Rot download category sales report query "+queryString.toString());
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
			list = query.list();
						
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
		}
		return list;
	}*/

/*public List<Object[]> getJktCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
//		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,vc.section_low as Section,");
//		queryString.append("vc.row_low as Row,ct.category_ticket_group_id as CategoryTicketGroupId,i.invoice_id as InvoiceId,");
//		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,tg.retail_price as RetailPrice,");
//		queryString.append("ct.actual_price as ActualPrice,i.invoice_total as InvoiceTotal,p.po_total as POTotal,tg.internal_notes,");
//		queryString.append("ccb.client_broker_company_name,c.client_type_desc from category_ticket ct inner join category_ticket_group tg on ");
//		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id inner join invoice i on i.invoice_id = ct.invoice_id ");
//		queryString.append("inner join venue_category vc on vc.venue_category_id = tg.venue_category_id inner join event e on ");
//		queryString.append("e.event_id = tg.event_id left join ticket t on t.ticket_id  = ct.ticket_id ");
//		queryString.append("left join (select cb.client_broker_id,ii.invoice_id,cb.client_broker_company_name from invoice ii ");
//		queryString.append("inner join client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
//		queryString.append("inner join client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id ");
//		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from client c inner join client_invoice ci on ");
//		queryString.append("c.client_id=ci.client_id inner join client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id ");
//		queryString.append("left join purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+stubJktPos+".category_ticket ct inner join "+stubJktPos+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+stubJktPos+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+stubJktPos+".ticket t inner join "+stubJktPos+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NoStub','ZLR NOStub')) as ctgg "); 
		queryString.append("inner join "+stubJktPos+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+stubJktPos+".event e on e.event_id = ctgg.event_id left join "+stubJktPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+stubJktPos+".invoice ii ");
		queryString.append("inner join "+stubJktPos+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+stubJktPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+stubJktPos+".client c inner join "+stubJktPos+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+stubJktPos+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+stubJktPos+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		
		if(reportType.equalsIgnoreCase("AUTO")){
			queryString.append("ctgg.internal_notes ='AUTO' and ");
		}else if(reportType.equalsIgnoreCase("MINICATS")){
			queryString.append("ctgg.internal_notes = 'MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPCATS")){
			queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPAUTO")){
			queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
		}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("LARRYLAST")){
			queryString.append("ctgg.internal_notes = 'LarryLast' and ");
		}else if(reportType.equalsIgnoreCase("ZONES")){
			queryString.append("ctgg.internal_notes !='AUTO' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
		}
		else if(reportType.equalsIgnoreCase("ZONES PRICING")){
			queryString.append("ctgg.internal_notes = 'ZP' and ");
		}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'ZLR NOStub' and ");
		}else if(reportType.equalsIgnoreCase("MANUAL")){
			queryString.append("ctgg.internal_notes !='AUTO' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
		}else if(reportType.equalsIgnoreCase("VIVID MINICATS")){
			queryString.append("ccb.client_broker_id=3903 and ");
		}else if(reportType.equalsIgnoreCase("VIVID LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=3904 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG MINICATS")){
			queryString.append("ccb.client_broker_id=1234567890 and ");
		}
//		else if(reportType.equalsIgnoreCase("SCOREBIG AUTO")){
//			queryString.append("ccb.client_broker_id=3902 and ");
//		}
		else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=3902 and ");
		}
		
		if(poInfo.equalsIgnoreCase("WITHPO")){
			queryString.append("p.purchase_order_id is not null and ");
		}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
			queryString.append("p.purchase_order_id is null and ");
		}
			
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		System.out.println("Jkt download category sales report query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		
	}
	return list;
}*/

/*public List<Object[]> getTixcityCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
	
	String posUrl = tixCityPos;
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+posUrl+".category_ticket ct inner join "+posUrl+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+posUrl+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+posUrl+".ticket t inner join "+posUrl+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NoStub','ZLR NOStub')) as ctgg "); 
		queryString.append("inner join "+posUrl+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+posUrl+".event e on e.event_id = ctgg.event_id left join "+posUrl+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+posUrl+".invoice ii ");
		queryString.append("inner join "+posUrl+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+posUrl+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+posUrl+".client c inner join "+posUrl+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+posUrl+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+posUrl+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		
		if(reportType.equalsIgnoreCase("AUTO")){
			queryString.append("ctgg.internal_notes ='AUTO' and ");
		}else if(reportType.equalsIgnoreCase("MINICATS")){
			queryString.append("ctgg.internal_notes = 'MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPCATS")){
			queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPAUTO")){
			queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
		}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("LARRYLAST")){
			queryString.append("ctgg.internal_notes = 'LarryLast' and ");
		}else if(reportType.equalsIgnoreCase("ZONES")){
			queryString.append("ctgg.internal_notes !='AUTO' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
		}
		else if(reportType.equalsIgnoreCase("ZONES PRICING")){
			queryString.append("ctgg.internal_notes = 'ZP' and ");
		}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'ZLR NOStub' and ");
		}else if(reportType.equalsIgnoreCase("MANUAL")){
			queryString.append("ctgg.internal_notes !='AUTO' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
		}
		
		if(poInfo.equalsIgnoreCase("WITHPO")){
			queryString.append("p.purchase_order_id is not null and ");
		}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
			queryString.append("p.purchase_order_id is null and ");
		}
			
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		System.out.println("Rot download category sales report query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
					
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		
	}
	return list;
}*/


/*public List<Object[]> getRotCategorySalesReportToday(Date startDate,Date endDate){
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
//		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,vc.section_low as Section,");
//		queryString.append("vc.row_low as Row,ct.category_ticket_group_id as CategoryTicketGroupId,i.invoice_id as InvoiceId,");
//		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,tg.retail_price as RetailPrice,");
//		queryString.append("ct.actual_price as ActualPrice,i.invoice_total as InvoiceTotal,p.po_total as POTotal,tg.internal_notes,");		
//		//queryString.append("case when tg.internal_notes='AUTO' or tg.internal_notes='MINICATS' or tg.internal_notes='TGCATS' then 'TN ' + tg.internal_notes when ccb.client_broker_company_name = 'Vivid2@ticketgallery.com' then 'VIVID TGCATS' when ccb.client_broker_company_name = 'VividTheater@ticketgallery.com' then 'VIVID AUTO'  else 'MANUAL' end as ProcessedAs, ");
//		queryString.append("ccb.client_broker_company_name,c.client_type_desc from category_ticket ct inner join category_ticket_group tg on ");
//		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id inner join invoice i on i.invoice_id = ct.invoice_id ");
//		queryString.append("inner join venue_category vc on vc.venue_category_id = tg.venue_category_id inner join event e on ");
//		queryString.append("e.event_id = tg.event_id left join ticket t on t.ticket_id  = ct.ticket_id ");
//		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from invoice ii ");
//		queryString.append("inner join client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
//		queryString.append("inner join client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id ");
//		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from client c inner join client_invoice ci on ");
//		queryString.append("c.client_id=ci.client_id inner join client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id ");
//		queryString.append("left join purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+tn2RotPos+".category_ticket ct inner join "+tn2RotPos+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+tn2RotPos+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+tn2RotPos+".ticket t inner join "+tn2RotPos+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NoStub','ZLR NOStub')) as ctgg ");  
		queryString.append("inner join "+tn2RotPos+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+tn2RotPos+".event e on e.event_id = ctgg.event_id left join "+tn2RotPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from "+tn2RotPos+".invoice ii ");
		queryString.append("inner join "+tn2RotPos+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+tn2RotPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+tn2RotPos+".client c inner join "+tn2RotPos+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+tn2RotPos+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+tn2RotPos+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		
		//System.out.println("Rot daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
					
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		
	}
	return list;
}*/

/*public List<Object[]> getJktCategorySalesReportToday(Date startDate,Date endDate){
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
//		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,vc.section_low as Section,");
//		queryString.append("vc.row_low as Row,ct.category_ticket_group_id as CategoryTicketGroupId,i.invoice_id as InvoiceId,");
//		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,tg.retail_price as RetailPrice,");
//		queryString.append("ct.actual_price as ActualPrice,i.invoice_total as InvoiceTotal,p.po_total as POTotal,tg.internal_notes,");		
//		//queryString.append("case when tg.internal_notes='AUTO' or tg.internal_notes='MINICATS' or tg.internal_notes='TGCATS' then 'TN ' + tg.internal_notes when ccb.client_broker_company_name = 'Vivid2@ticketgallery.com' then 'VIVID TGCATS' when ccb.client_broker_company_name = 'VividTheater@ticketgallery.com' then 'VIVID AUTO'  else 'MANUAL' end as ProcessedAs, ");
//		queryString.append("ccb.client_broker_company_name,c.client_type_desc from category_ticket ct inner join category_ticket_group tg on ");
//		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id inner join invoice i on i.invoice_id = ct.invoice_id ");
//		queryString.append("inner join venue_category vc on vc.venue_category_id = tg.venue_category_id inner join event e on ");
//		queryString.append("e.event_id = tg.event_id left join ticket t on t.ticket_id  = ct.ticket_id ");
//		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from invoice ii ");
//		queryString.append("inner join client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
//		queryString.append("inner join client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id ");
//		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from client c inner join client_invoice ci on ");
//		queryString.append("c.client_id=ci.client_id inner join client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id ");
//		queryString.append("left join purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+stubJktPos+".category_ticket ct inner join "+stubJktPos+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+stubJktPos+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+stubJktPos+".ticket t inner join "+stubJktPos+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NoStub','ZLR NOStub')) as ctgg ");  
		queryString.append("inner join "+stubJktPos+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+stubJktPos+".event e on e.event_id = ctgg.event_id left join "+stubJktPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from "+stubJktPos+".invoice ii ");
		queryString.append("inner join "+stubJktPos+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+stubJktPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+stubJktPos+".client c inner join "+stubJktPos+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+stubJktPos+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+stubJktPos+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		
		//System.out.println("Jkt daily query "+queryString.toString());
//		session = sessionFactory.openSession();
		session = QueryManagerDAO.getSession();
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
					
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		
	}
	return list;
}*/
/*public List<Object[]> getTixCityCategorySalesReportToday(Date startDate,Date endDate){
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+tixCityPos+".category_ticket ct inner join "+tixCityPos+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+tixCityPos+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+tixCityPos+".ticket t inner join "+tixCityPos+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NoStub','ZLR NOStub')) as ctgg "); 
		queryString.append("inner join "+tixCityPos+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+tixCityPos+".event e on e.event_id = ctgg.event_id left join "+tixCityPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from "+tixCityPos+".invoice ii ");
		queryString.append("inner join "+tixCityPos+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+tixCityPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+tixCityPos+".client c inner join "+tixCityPos+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+tixCityPos+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+tixCityPos+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		
		//System.out.println("Jkt daily query "+queryString.toString());
//		session = sessionFactory.openSession();
		session = QueryManagerDAO.getSession();
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
					
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		
	}
	return list;
}*/

// queries for tnd sales report in tmat admin - admin tab
// tmatprodscale db table name = tndsalesreport

	public List<String> getAllChild() throws Exception {
//		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct child from tndsalesreport";
//			session = sessionFactory.openSession();
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public List<String> getAllVenue() throws Exception {
//		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct venue_name from tndsalesreport";
//			session = sessionFactory.openSession();
//			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public List<String> getAllCompanyName() throws Exception {
//		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct owner_company_name from tndsalesreport";
//			session = sessionFactory.openSession();
//			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public List<String> getAllStockType() throws Exception {
		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct stock_type from tndsalesreport";
//			session = sessionFactory.openSession();
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}

	public List<Object[]> getTndSalesReport(String orderStartDate,
			String orderEndDate, String parent, String child, String venueName,
			String acceptStatus, String ticketType,
			String stockType)  throws Exception {
		
		Session session = null;
		List<Object[]> list = null;
		try{
			String sql = "select order_id,CASE WHEN order_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), order_date, 101) END +'  '+ "+
						 "CASE WHEN order_date is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), order_date, 100), 7)) END as 'order_date', "+
						 "parent,child,event_name,event_id,venue_name, "+
						 "CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END +'  '+ "+
						 "CASE WHEN event_date is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), event_date, 100), 7)) END as 'event_date', "+
						 "ticket_quantity,section,row,wholesale_price,revenue,accept_sub_status_desc,owner_customer_id, "+
						 "owner_company_name,is_category,cost,ticket_type,city_name,state_province,stock_type from tndsalesreport "+
						 "where order_date>= '"+orderStartDate+"' and order_date<= '"+orderEndDate+"' ";
			if(!parent.equalsIgnoreCase("all")){
				sql += "and parent='"+parent+"' ";
			}
			if(!child.equalsIgnoreCase("all")){
				sql += "and child='"+child+"' ";
			}
			if(!venueName.equalsIgnoreCase("all")){
				sql += "and venue_name='"+venueName+"' ";
			}
			if(!acceptStatus.equalsIgnoreCase("all")){
				sql += "and accept_sub_status_desc like '%"+acceptStatus+"%' ";
			}
			if(!ticketType.equalsIgnoreCase("all")){
				sql += "and ticket_type='"+ticketType+"' ";
			}
			/*if(!companyName.equalsIgnoreCase("all")){
				sql += "and owner_company_name='"+companyName+"' ";
			}*/
			if(!stockType.equalsIgnoreCase("all")){
				sql += "and stock_type='"+stockType+"' ";
			}
			
			sql += "order by order_date";
			System.out.println("sql for tnd sales report -> "+sql);
//			session = sessionFactory.openSession();
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);//.setParameter("orderStartDate", orderStartDate).setParameter("orderEndDate", orderEndDate);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public List<String> getChildsByParent(String parent) throws Exception {
		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct child from tndsalesreport where parent= :parent";
//			session = sessionFactory.openSession();
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql).setParameter("parent", parent);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public List<Integer> getAllVenuesTobeRemovedFromAutopricingProducts()  throws Exception {
		Session session = null;
		List<Integer> list = null;
		
		try{
			String sql = "select distinct venue_id from autopricing_unchecked_venues";
//			session = sessionFactory.openSession();
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}

	public Map<String,Set<String>> getAllLarrySectionAndLastRowByVenueCategoryId(Integer venueCategoryId) throws Exception{
		String sql = " SELECT distinct cm.larry_section,cm.last_row " +
				     " FROM category_new c " + 
					 " INNER JOIN category_mapping_new cm ON c.id = cm.category_id " +
					 " WHERE venue_category_id =  :venueCategoryId and cm.larry_section is not null and cm.larry_section !='' " +
					 " and last_row is not null and last_row !=''";//order by cm.id
		Map<String,Set<String>> larrySectionMap = new HashMap<String, Set<String>>();
		try {
//			PreparedStatement statement = connection.prepareStatement(sql);
//			statement.setInt(1, venueCategoryId);
//			resultSet = statement.executeQuery();
			Session session = null;
//			session = sessionFactory.openSession();
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql).setParameter("venueCategoryId", venueCategoryId);
			List<Object> list = null;
			list = query.list();
			if(list != null && !list.isEmpty()) { 
				for(Object object:list){
					Object[] row = (Object[]) object;
					String larrySec = (String)row[0];
					//String larrySec = resultSet.getString("larry_section").replaceAll("\\s+", " ").trim().toLowerCase();
					
					Set<String> lastRows = larrySectionMap.get(larrySec);
					if(lastRows == null) {
						lastRows = new HashSet<String>();
					}
					lastRows.add(((String)row[1]).toUpperCase());
					larrySectionMap.put(larrySec, lastRows);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		return larrySectionMap;
	}
	
	public List<ZonesPricingFloorCap> getAllExchangeEventFloorCap() throws Exception {
		  List<ZonesPricingFloorCap> zonesPricingFloorCaps =null;
		
		  String sql = "select fc.id as id,fc.event_id as tnExchangeEventId ,z.zone as section,fc.zone_price_cap as floorCap,fc.broker_id as brokerId " +
		  		" from tn_exchange_event_floor_cap fc WITH(NOLOCK) " +
		  		" inner join tn_exchange_theatre_zones z on z.id=fc.zone_id " +
		  		" inner join event e on e.admitone_id=fc.event_id" +
		  		" order by fc.event_id,z.zone";
		  
		  SQLQuery query = null;
		  Session session = null;
		  try {
			  session = QueryManagerDAO.getSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
		   query = session.createSQLQuery(sql);
		   query.addScalar("id",Hibernate.INTEGER);
		   query.addScalar("section",Hibernate.STRING);
		   query.addScalar("tnExchangeEventId",Hibernate.INTEGER);
		   query.addScalar("floorCap",Hibernate.DOUBLE);
		   query.addScalar("brokerId",Hibernate.INTEGER);
		   zonesPricingFloorCaps =  query.setResultTransformer(Transformers.aliasToBean(ZonesPricingFloorCap.class)).list();
		  } catch (Exception e) {
		   e.printStackTrace();
		   throw e;
		  } finally  {
		   
		  }
		  return zonesPricingFloorCaps;
	 }
	
	public List<Object[]> getMiniCatsVividCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,ct.section as Section,");
			query.append("ct.row_range as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'TC-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'TC' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[tgcat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");
			query.append("where tc.id <> 1 and ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.vivid_broker_id is not null and mvee.vivid_broker_id="+autopricingSettings.getBrokerId()+")  and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			query.append(" and ct.quantity in (2,4,6) ");
			
			/*if(autopricingSettings.getBrokerId().equals(3)) {
				//query.append(" and ct.quantity in (2) ");
				query.append(" and (ct.quantity in (2)  or (e.id in(1000150795,1000130177,1000131683,1000135339,1000136540,1000104351,1000111402,1000183100,1000184148,1000184536) " +
				" and quantity in(2,4,6)))");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}*/
			query.append(" order by e.event_date,e.event_time,e.name,ct.section,ct.quantity");
			
//			Session session = null;
			try{
//				session = sessionFactory.openSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			} finally {

			}
		}
		return list;
		
	}
	
 public List<Object[]> getManualCatsVividCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			//CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate
			
			query.append("SELECT DISTINCT 'Y' as Edit,event_name as Event,venue_name as Venue,CONVERT(VARCHAR(19),event_date,101) as EventDate,");
			query.append("CASE WHEN event_date is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), event_date, 100), 7)) ");
			query.append("WHEN event_date is null THEN 'TBD' END as EventTime,quantity as Quantity,section as Section,");
			query.append("row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN quantity>4 THEN 'MC-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'MC' END as Notes, ");
			query.append("cost as Cost,id as TicketId,CONVERT(VARCHAR(19),in_hand_date,101) as InHandDate,edelivery,event_date ");
			query.append("FROM brokers_manual_category_tickets ct WITH(NOLOCK)  ");
			query.append("where broker_id is not null and broker_id="+autopricingSettings.getBrokerId()+" " +
					" and datediff(day,getdate(),event_date)>"+autopricingSettings.getExcludeEventDays()+" and cost > 0 ");
			query.append(" and section not like '%ZONE%' and (section IS NOT NULL AND ltrim(section) != '') ");
			
			query.append(" and status='ACTIVE' ");
			
			/*if(autopricingSettings.getBrokerId().equals(3)) {
				query.append(" and quantity in(2,4,6)))");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}*/
			query.append(" order by event_date,event_name,section,quantity");
			
//			Session session = null;
			try{
//				session = sessionFactory.openSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			} finally {

			}
		}
		return list;
		
	}
	public List<Object[]> getScoreBigCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,");
			query.append("ct.quantity as Quantity,ct.section as Section, ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'TC-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'TC' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,e.event_date,e.event_time " );
			query.append(" FROM [tmatprodscale].[dbo].[tgcat_category_ticket] ct WITH(NOLOCK) ");
			query.append("left join [tmatprodscale].[dbo].[event] e WITH(NOLOCK) on e.id=ct.event_id left join [tmatprodscale].[dbo].[venue] v WITH(NOLOCK) on v.id=e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id ");
			query.append("inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and(mvee.scorebig_broker_id is not null and mvee.scorebig_broker_id="+autopricingSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			
			if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			query.append(" order by e.event_date,e.event_time,e.name,ct.section,ct.quantity");

//			Session session = null;
			try{
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
	public List<Object[]> getFanXchangeCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,");
			query.append("ct.quantity as Quantity,ct.section as Section, ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'TC-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'TC' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.event_date,e.event_time " );
			query.append(" FROM [tmatprodscale].[dbo].[tgcat_category_ticket] ct WITH(NOLOCK) ");
			query.append("left join [tmatprodscale].[dbo].[event] e WITH(NOLOCK) on e.id=ct.event_id left join [tmatprodscale].[dbo].[venue] v WITH(NOLOCK) on v.id=e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id ");
			query.append("inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.fanxchange_broker_id is not null and mvee.fanxchange_broker_id="+autopricingSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			
			if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			query.append(" order by e.event_date,e.event_time,e.name,ct.section,ct.quantity");

//			Session session = null;
			try{
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
	public List<Object[]> getTicketCityCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,");
			query.append("ct.quantity as Quantity,ct.section as Section, ct.row_range as Row,'*' as SeatFrom,'*' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'TC-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'TC' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,e.id,e.venue_category_id,ct.tmat_zone,e.event_date,e.event_time " );
			query.append(" FROM [tmatprodscale].[dbo].[tgcat_category_ticket] ct WITH(NOLOCK) ");
			query.append("left join [tmatprodscale].[dbo].[event] e WITH(NOLOCK) on e.id=ct.event_id left join [tmatprodscale].[dbo].[venue] v WITH(NOLOCK) on v.id=e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id ");
			query.append("inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and(mvee.ticketcity_broker_id is not null and mvee.ticketcity_broker_id="+autopricingSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			
			if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				//Tamil : for minicat ticketcity we have to generate concert listings only 
				query.append(" and tc.id in(2) and ct.quantity in (2,4) and ct.event_id not in (1000281552)  ") ; 
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			query.append("order by e.event_date,e.event_time,e.name,ct.section,ct.quantity");

//			Session session = null;
			try{
//				session = sessionFactory.openSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
public List<SeatGeekUploadCats> getSeatGeekLastFiveRowsWithMinicats(AutopricingSettings autopricingSettings,AutopricingSettings miniApSettings) throws Exception {
		
	List<SeatGeekUploadCats> sgOrdersList = new ArrayList<SeatGeekUploadCats>();
		boolean eligibleFlag = false;
		String query ="";
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			eligibleFlag = true;
			query = "SELECT DISTINCT 'Y' as edit,e.name as eventName,v.building as venueName,e.event_date as eventDate," +
					" e.event_time as eventTime,ct.quantity as quantity,ct.section as section, ct.last_row as row,'*' as seatFrom,'*' as seatThru," +
					" 'SGK' as notes,FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as price,ct.id as catTicketId," +
					" DATEADD(day, -1, e.event_date) as inHandDate,ct.shipping_method_id as shippingMethodId,e.id as eventId,e.venue_category_id as venueCategoryId," +
					" ct.tmat_zone as tmatZone,'SGLFR' as internalNotes,e.event_date,e.event_time" +
					" FROM seatgeek_lastfiverow_category_ticket ct WITH(NOLOCK)" +
					" left join event e WITH(NOLOCK) on e.id=ct.event_id " +
					" left join venue v WITH(NOLOCK) on v.id=e.venue_id " +
					" inner join artist a WITH(NOLOCK) on a.id= e.artist_id" +
					" inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id " +
					" inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id " +
					" inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id " +
					" inner join admitone_event ae on ae.eventId=e.admitone_id" +
					" inner join seatgeek_lastfiverow_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id " +
					" inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" " +
					" where ct.status='ACTIVE' and e.event_status='ACTIVE' and e.name not like 'hamilton'" +
					" and(mvee.seatgeek_broker_id is not null and seatgeek_broker_id="+autopricingSettings.getBrokerId()+") " +
					" and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 " +
					" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '')" +
					" and mvee.status='ACTIVE' and ct.quantity in (2,4,6)";
					//" order by e.event_date,e.event_time,e.name,ct.section,ct.last_row,ct.quantity";
		}
		if(miniApSettings.getIsEnabled() != null && miniApSettings.getIsEnabled()) {
			if(eligibleFlag) {
				query =query + " union ";
			}
			eligibleFlag = true;
			query = query +  "SELECT DISTINCT 'Y' as edit,e.name as eventName,v.building as venueName,e.event_date as eventDate, " +
					" e.event_time as eventTime,ct.quantity as quantity,ct.section as section, ct.row_range as row,'*' as seatFrom,'*' as seatThru," +
					" 'SGK' as notes,FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as price,ct.id as catTicketId," +
					" DATEADD(day, -1, e.event_date) as inHandDate,ct.shipping_method_id as shippingMethodId,e.id as eventId,e.venue_category_id as venueCategoryId," +
					" ct.tmat_zone as tmatZone,'MINICATS' as internalNotes,e.event_date,e.event_time" +
					" FROM tgcat_category_ticket ct WITH(NOLOCK)" +
					" left join event e WITH(NOLOCK) on e.id=ct.event_id" +
					" left join venue v WITH(NOLOCK) on v.id=e.venue_id" +
					" inner join artist a WITH(NOLOCK) on a.id= e.artist_id" +
					" inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id" +
					" inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id" +
					" inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id" +
					" inner join admitone_event ae on ae.eventId=e.admitone_id" +
					" inner join seatgeek_lastfiverow_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id" +
					" inner join autopricing_exchange aem WITH(NOLOCK) on aem.id=8" +
					" where ct.status='ACTIVE' and e.event_status='ACTIVE' and e.name not like 'hamilton'" +
					//" and ae.parent_category='SPORTS'" +
					" and(mvee.seatgeek_broker_id is not null and seatgeek_broker_id="+autopricingSettings.getBrokerId()+") " +
					" and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 " +
					" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '')" +
					" and mvee.status='ACTIVE' and ct.quantity in (2,4,6) ";	
		}

		if(eligibleFlag) {
			query =query + " order by event_date,event_time,eventName,venueName,section,row,quantity";
//			Session session = null;
			try{
//				session = sessionFactory.openSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				SQLQuery sqlQuery = session.createSQLQuery(query);
				sqlQuery.addScalar("edit", Hibernate.STRING);
				sqlQuery.addScalar("eventName", Hibernate.STRING);
				sqlQuery.addScalar("eventDate", Hibernate.TIMESTAMP);
				sqlQuery.addScalar("eventTime", Hibernate.TIMESTAMP);
				sqlQuery.addScalar("venueName", Hibernate.STRING);
				sqlQuery.addScalar("catTicketId", Hibernate.INTEGER);
				sqlQuery.addScalar("section", Hibernate.STRING);
				sqlQuery.addScalar("row", Hibernate.STRING);
				sqlQuery.addScalar("seatFrom", Hibernate.STRING);
				sqlQuery.addScalar("seatThru", Hibernate.STRING);
				sqlQuery.addScalar("quantity", Hibernate.INTEGER);
				sqlQuery.addScalar("price", Hibernate.DOUBLE);
				sqlQuery.addScalar("inHandDate", Hibernate.TIMESTAMP);
				sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
				sqlQuery.addScalar("eventId", Hibernate.INTEGER);
				sqlQuery.addScalar("venueCategoryId", Hibernate.INTEGER);
				sqlQuery.addScalar("tmatZone", Hibernate.STRING);
				sqlQuery.addScalar("notes", Hibernate.STRING);
				sqlQuery.addScalar("internalNotes", Hibernate.STRING);
				
			 sgOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(SeatGeekUploadCats.class)).list();
				//return sgOrdersList;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return sgOrdersList;
		
	}
public List<SeatGeekUploadCats> getSeatGeekLastFiveRows(AutopricingSettings autopricingSettings) throws Exception {
		
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			String query = "SELECT DISTINCT 'Y' as edit,e.name as eventName,v.building as venueName,e.event_date as eventDate," +
					" e.event_time as eventTime,ct.quantity as quantity,ct.section as section, ct.last_row as row,'*' as seatFrom,'*' as seatThru," +
					" 'SGK' as notes,FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as price,ct.id as catTicketId," +
					" DATEADD(day, -1, e.event_date) as inHandDate,ct.shipping_method_id as shippingMethodId,e.id as eventId,e.venue_category_id as venueCategoryId," +
					" ct.tmat_zone as tmatZone" +
					" FROM seatgeek_lastfiverow_category_ticket ct WITH(NOLOCK)" +
					" left join event e WITH(NOLOCK) on e.id=ct.event_id " +
					" left join venue v WITH(NOLOCK) on v.id=e.venue_id " +
					" inner join artist a WITH(NOLOCK) on a.id= e.artist_id" +
					" inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id " +
					" inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id " +
					" inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id " +
					" inner join admitone_event ae on ae.eventId=e.admitone_id" +
					" inner join seatgeek_lastfiverow_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id " +
					" inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" " +
					" where ct.status='ACTIVE' and e.event_status='ACTIVE' and e.name not like 'hamilton'" +
					" and(mvee.seatgeek_broker_id is not null and seatgeek_broker_id="+autopricingSettings.getBrokerId()+") " +
					" and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 " +
					" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '')" +
					" and mvee.status='ACTIVE' and ct.quantity in (2,4,6)" +
					" order by e.event_date,e.event_time,e.name,ct.section,ct.last_row,ct.quantity";

//			Session session = null;
			try{
//				session = sessionFactory.openSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				SQLQuery sqlQuery = session.createSQLQuery(query);
				sqlQuery.addScalar("edit", Hibernate.STRING);
				sqlQuery.addScalar("eventName", Hibernate.STRING);
				sqlQuery.addScalar("eventDate", Hibernate.TIMESTAMP);
				sqlQuery.addScalar("eventTime", Hibernate.TIMESTAMP);
				sqlQuery.addScalar("venueName", Hibernate.STRING);
				sqlQuery.addScalar("catTicketId", Hibernate.INTEGER);
				sqlQuery.addScalar("section", Hibernate.STRING);
				sqlQuery.addScalar("row", Hibernate.STRING);
				sqlQuery.addScalar("seatFrom", Hibernate.STRING);
				sqlQuery.addScalar("seatThru", Hibernate.STRING);
				sqlQuery.addScalar("quantity", Hibernate.INTEGER);
				sqlQuery.addScalar("price", Hibernate.DOUBLE);
				sqlQuery.addScalar("inHandDate", Hibernate.TIMESTAMP);
				sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
				sqlQuery.addScalar("eventId", Hibernate.INTEGER);
				sqlQuery.addScalar("venueCategoryId", Hibernate.INTEGER);
				sqlQuery.addScalar("tmatZone", Hibernate.STRING);
				sqlQuery.addScalar("notes", Hibernate.STRING);
				
			 List<SeatGeekUploadCats> sgOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(SeatGeekUploadCats.class)).list();
				return sgOrdersList;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return null;
		
	}

public List<Object[]> getSeatGeekUploadCatsCsv(AutopricingSettings autopricingSettings) throws Exception {
	
	List<Object[]> list = null;
	
	if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
		
		StringBuilder query = new StringBuilder();
		query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate, "); 
		query.append(" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");  
		query.append(" WHEN e.event_time is null THEN 'TBD' END as EventTime,  ");
		query.append(" uc.quantity as Quantity,uc.section as Section, uc.row as Row,'*' as SeatFrom,'*' as SeatThru, ");
		query.append(" 'SGK' as Notes,   ");
		query.append(" FLOOR(uc.price) as Cost,uc.cat_ticket_id as TicketId,CONVERT(VARCHAR(19),DATEADD(day, -1, e.event_date),101) as InHandDate, ");
		query.append(" uc.shipping_method_id,uc.event_id,e.event_date,e.event_time ");
		query.append(" FROM seatgeek_upload_cats uc WITH(NOLOCK)   ");
		query.append(" inner join event e on e.id=uc.event_id ");
		query.append(" inner join venue v on v.id=e.venue_id ");
		query.append(" WHERE uc.status='ACTIVE'  ");
		
		if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
			//Tamil : for minicat ticketcity we have to generate concert listings only 
			query.append(" and uc.quantity in (2,4) ") ; 
		} else {
			query.append(" and uc.quantity in (2,4,6) ");
		}
		query.append("order by e.event_date,e.event_time,e.name,uc.section,uc.row,uc.quantity");

//		Session session = null;
		try{
//			session = sessionFactory.openSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query queryString = session.createSQLQuery(query.toString());
			list = queryString.list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
	}
	return list;
	
}
	
public List<Object[]> getSeatGeekLastFiveRowCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,");
			query.append("ct.quantity as Quantity,ct.section as Section, ct.last_row as Row,'*' as SeatFrom,'*' as SeatThru,");
			//CASE WHEN ct.quantity>4 THEN 'SG-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'SG' END as Notes
			query.append("'SGK' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,CONVERT(VARCHAR(19),DATEADD(day, -1, e.event_date),101) as InHandDate,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,e.event_date,e.event_time " );
			query.append(" FROM [tmatprodscale].[dbo].[seatgeek_lastfiverow_category_ticket] ct WITH(NOLOCK) ");
			query.append("left join [tmatprodscale].[dbo].[event] e WITH(NOLOCK) on e.id=ct.event_id left join [tmatprodscale].[dbo].[venue] v WITH(NOLOCK) on v.id=e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id ");
			query.append("inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join seatgeek_lastfiverow_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and(mvee.seatgeek_broker_id is not null and seatgeek_broker_id="+autopricingSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			
			if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				//Tamil : for minicat ticketcity we have to generate concert listings only 
				query.append(" and tc.id in(2) and ct.quantity in (2,4) and ct.event_id not in (1000281552)  ") ; 
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			query.append("order by e.event_date,e.event_time,e.name,ct.section,ct.last_row,ct.quantity");

//			Session session = null;
			try{
//				session = sessionFactory.openSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}

public List<Object[]> getTNSeatGeekLastFiveRowCsv(AutopricingSettings autopricingSettings) throws Exception {
	
	List<Object[]> list = null;
	ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
	String serverName =resourceBundle.getString("indux.server." + 156);//RTW-2
	
	if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
		
		String query = "select distinct top 10 'Y' as Edit,e.event_name as Event,v.name as Venue," +
				" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate," +
				" CASE WHEN e.event_datetime is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))" +
				" WHEN e.event_datetime is null THEN 'TBD' END as EventTime," +
				" tg.ticket_count as Quantity,vc.section_low as Section,vc.row_low as Row,'*' as SeatFrom,'*' as SeatThru," +
				" tg.internal_notes as Notes, tg.retail_price+1000 as Cost,tg.category_ticket_group_id as TicketId,CONVERT(VARCHAR(19),DATEADD(day, -1, e.event_datetime),101) as InHandDate," +
				" tg.shipping_method_special_id, e.event_datetime" +
				" from "+serverName+".category_ticket_group tg" +
				" inner join "+serverName+".venue_category vc on vc.venue_category_id=tg.venue_category_id" +
				" inner join "+serverName+".event e on e.event_id=tg.event_id" +
				" inner join "+serverName+".venue v on v.venue_id=e.venue_id" +
				//" inner join seatgeek_lastfiverow_category_ticket ct on ct.tn_category_ticket_group_id=tg.category_ticket_group_id and ct.status='ACTIVE'" +
				" where internal_notes in('SGK')" +
				" and retail_price>0 " +//--and broadcast=1
				" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+serverName+".category_ticket where invoice_id is not null)" +
				" and tg.category_ticket_group_id in(select distinct category_ticket_group_id from "+serverName+".category_ticket)" +
				" and e.event_datetime>getdate()" +
				" order by  e.event_datetime desc";
				//" order by  e.event_datetime, e.event_name,v.name,section,row,Quantity desc";
		

//		Session session = null;
		try{
//			session = sessionFactory.openSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query queryString = session.createSQLQuery(query.toString());
			list = queryString.list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
	}
	return list;
	
}

	public List<Object[]> getTicketCitySportsCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,");
			query.append("ct.quantity as Quantity,ct.section as Section, ct.row_range as Row,'*' as SeatFrom,'*' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'TC-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'TC' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,e.id,e.venue_category_id,ct.tmat_zone,e.event_date,e.event_time " );
			query.append(" FROM [tmatprodscale].[dbo].[tgcat_category_ticket] ct WITH(NOLOCK) ");
			query.append("left join [tmatprodscale].[dbo].[event] e WITH(NOLOCK) on e.id=ct.event_id left join [tmatprodscale].[dbo].[venue] v WITH(NOLOCK) on v.id=e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id ");
			query.append("inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and(mvee.ticketcity_broker_id is not null and mvee.ticketcity_broker_id="+autopricingSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			
			if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				//Tamil : for minicat ticketcity we have to generate concert listings only 
				query.append(" and tc.id in(1) and ct.quantity in (2,4) and ct.event_id not in (1000281552)  ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			query.append("order by e.event_date,e.event_time,e.name,ct.section,ct.quantity");

//			Session session = null;
			try{
//				session = sessionFactory.openSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
	public List<Object[]> getVipminicatsScoreBigCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,");
			query.append("ct.quantity as Quantity,ct.section as Section, ct.alternate_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'VM-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'VM' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.event_date,e.event_time " );
			query.append(" FROM [tmatprodscale].[dbo].[vipcat_category_ticket] ct WITH(NOLOCK) ");
			query.append("left join [tmatprodscale].[dbo].[event] e WITH(NOLOCK) on e.id=ct.event_id left join [tmatprodscale].[dbo].[venue] v WITH(NOLOCK) on v.id=e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id ");
			query.append("inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join vip_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and(mvee.scorebig_broker_id is not null and mvee.scorebig_broker_id="+autopricingSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			
			if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			query.append("order by e.event_date,e.event_time,e.name,ct.section,ct.quantity");

//			Session session = null;
			try{
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
public List<Object[]> getVipMinicatsFanXchangeCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,");
			query.append("ct.quantity as Quantity,ct.section as Section, ct.alternate_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'VM-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'VM' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.event_date,e.event_time " );
			query.append(" FROM [tmatprodscale].[dbo].[vipcat_category_ticket] ct WITH(NOLOCK) ");
			query.append("left join [tmatprodscale].[dbo].[event] e WITH(NOLOCK) on e.id=ct.event_id left join [tmatprodscale].[dbo].[venue] v WITH(NOLOCK) on v.id=e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id ");
			query.append("inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join vip_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.fanxchange_broker_id is not null and mvee.fanxchange_broker_id="+autopricingSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			
			if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			query.append("order by e.event_date,e.event_time,e.name,ct.section,ct.quantity");

//			Session session = null;
			try{
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
	public List<Object[]> getVipMinicatsTicketCityCsv(AutopricingSettings autopricingSettings) throws Exception {
		
		List<Object[]> list = null;
		
		if(autopricingSettings.getIsEnabled() != null && autopricingSettings.getIsEnabled()) {
			
			StringBuilder query = new StringBuilder();
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,");
			query.append("ct.quantity as Quantity,ct.section as Section, ct.alternate_row as Row,'*' as SeatFrom,'*' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'VM-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'VM' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,e.id,e.venue_category_id,ct.tmat_zone,e.event_date,e.event_time " );
			query.append(" FROM [tmatprodscale].[dbo].[vipcat_category_ticket] ct WITH(NOLOCK) ");
			query.append("left join [tmatprodscale].[dbo].[event] e WITH(NOLOCK) on e.id=ct.event_id left join [tmatprodscale].[dbo].[venue] v WITH(NOLOCK) on v.id=e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id ");
			query.append("inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join vip_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+autopricingSettings.getExchangeId()+" ");			
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and(mvee.ticketcity_broker_id is not null and mvee.ticketcity_broker_id="+autopricingSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+autopricingSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%'  and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append(" and mvee.status='ACTIVE' ");
			
			if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (4) and ct.event_id not in (1000281552)   ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			query.append("order by e.event_date,e.event_time,e.name,ct.section,ct.quantity");

			Session session = null;
			try{
//				session = sessionFactory.openSession();
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
	public List<Object[]> getLarryAndAllLastRowVividSeatCsv(AutopricingSettings larrySettings,AutopricingSettings lastrowSettings,
			AutopricingSettings zoneLastrowSettings) throws Exception {
		
		List<Object[]> list = null;
		StringBuilder query = new StringBuilder();
		
		boolean eligibleFlag = false;
		if(larrySettings.getIsEnabled() != null && larrySettings.getIsEnabled()) {
			
			eligibleFlag = true;
			
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,ct.section as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'LL-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'LL' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+larrySettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[larrylast_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join larrylast_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+larrySettings.getExchangeId()+" ");
			query.append(" where tc.id <> 1 and ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.vivid_broker_id is not null and mvee.vivid_broker_id="+larrySettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+larrySettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append("  and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			query.append(" and ct.quantity in (2,4,6) ");
			
			/*if(larrySettings.getBrokerId().equals(3)) {
				//query.append(" and ct.quantity in (2) ");
				
				query.append(" and (ct.quantity in (2)  or (e.id in(1000150795,1000130177,1000131683,1000135339,1000136540,1000104351,1000111402,1000183100,1000184148,1000184536) " +
						" and quantity in(2,4,6)))");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}*/
		}
		
		if(lastrowSettings.getIsEnabled() != null && lastrowSettings.getIsEnabled()) {
			
			if(eligibleFlag) {
				query.	append(" union ");
			}
			eligibleFlag = true;
		
			query.append("SELECT distinct 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,convert(varchar(500), ct.section) as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'LR-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'LR' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+lastrowSettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[lastrow_minicat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join lastrow_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+lastrowSettings.getExchangeId()+" ");
			query.append("where tc.id <> 1 and ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.vivid_broker_id is not null and mvee.vivid_broker_id="+lastrowSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+lastrowSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			query.append(" and ct.quantity in (2,4,6) ");
			
			/*if(lastrowSettings.getBrokerId().equals(3)) {
				//query.append(" and ct.quantity in (2) ");
				
				query.append(" and (ct.quantity in (2)  or (e.id in(1000150795,1000130177,1000131683,1000135339,1000136540,1000104351,1000111402,1000183100,1000184148,1000184536) " +
				" and quantity in(2,4,6)))");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}*/
		}
		
		if(zoneLastrowSettings.getIsEnabled() != null && zoneLastrowSettings.getIsEnabled()) {
			
			if(eligibleFlag) {
				query.	append(" union ");
			}
			eligibleFlag = true;
		
			query.append("SELECT distinct 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,convert(varchar(500), ct.section) as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'ZLR-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'ZLR' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+zoneLastrowSettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[zone_lastrow_minicat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join zone_lastrow_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+zoneLastrowSettings.getExchangeId()+" ");
			query.append("where tc.id <> 1 and ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.vivid_broker_id is not null and mvee.vivid_broker_id="+zoneLastrowSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+zoneLastrowSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			query.append(" and ct.quantity in (2,4,6) ");
			//query.append(" and ct.event_id not in(select distinct event_id from lastrow_minicat_category_ticket WITH(NOLOCK) where status='ACTIVE') "); //Skip events which are processed in Lastrowminicats To avoid Duplicats listings
			
			/*if(zoneLastrowSettings.getBrokerId().equals(3)) {
				//query.append(" and ct.quantity in (2) ");
				
				query.append(" and (ct.quantity in (2)  or (e.id in(1000150795,1000130177,1000131683,1000135339,1000136540,1000104351,1000111402,1000183100,1000184148,1000184536) " +
				" and quantity in(2,4,6)))");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}*/
		}
		query.append(" order by event_date,event_time,Event,Section,Quantity");

		if(eligibleFlag) {
//			Session session = null;
			try{
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
	public List<Object[]> getLarryAndAllLastRowScorebigCsv(AutopricingSettings larrySettings,AutopricingSettings lastrowSettings,
			AutopricingSettings zoneLastrowSettings) throws Exception {
		
		List<Object[]> list = null;
		StringBuilder query = new StringBuilder();
		
		boolean eligibleFlag = false;
		if(larrySettings.getIsEnabled() != null && larrySettings.getIsEnabled()) {
			eligibleFlag = true;
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,ct.section as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'LL-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'LL' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+larrySettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[larrylast_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join larrylast_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+larrySettings.getExchangeId()+" ");
			query.append(" where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.scorebig_broker_id is not null and mvee.scorebig_broker_id="+larrySettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+larrySettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append("  and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE'  ");
			
			if(larrySettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
		}
		
		if(lastrowSettings.getIsEnabled() != null && lastrowSettings.getIsEnabled()) {
			
			if(eligibleFlag) {
				query.append(" union ");
			}
			eligibleFlag = true;
			
			query.append("SELECT distinct 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,convert(varchar(500), ct.section) as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'LR-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'LR' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+lastrowSettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[lastrow_minicat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join lastrow_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+lastrowSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.scorebig_broker_id is not null and mvee.scorebig_broker_id="+lastrowSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+lastrowSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			
			if(lastrowSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
			
		}
		
		if(zoneLastrowSettings.getIsEnabled() != null && zoneLastrowSettings.getIsEnabled()) {
			
			if(eligibleFlag) {
				query.append(" union ");
			}
			eligibleFlag = true;
		
			query.append("SELECT distinct 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,convert(varchar(500), ct.section) as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'ZLR-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'ZLR' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+zoneLastrowSettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[zone_lastrow_minicat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join zone_lastrow_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+zoneLastrowSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.scorebig_broker_id is not null and mvee.scorebig_broker_id="+zoneLastrowSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+zoneLastrowSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			//query.append(" and ct.event_id not in(select distinct event_id from lastrow_minicat_category_ticket WITH(NOLOCK) where status='ACTIVE') ");
			
			if(zoneLastrowSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
		}
		query.append(" order by event_date,event_time,Event,Section,Quantity");
		
		if(eligibleFlag) {
//			Session session = null;
			try{
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
		
	public List<Object[]> getLarryAndAllLastRowFanXchangeCsv(AutopricingSettings larrySettings,AutopricingSettings lastrowSettings,
			AutopricingSettings zoneLastrowSettings) throws Exception {
		
		List<Object[]> list = null;
		StringBuilder query = new StringBuilder();
		
		boolean eligibleFlag = false;
		if(larrySettings.getIsEnabled() != null && larrySettings.getIsEnabled()) {
			eligibleFlag = true;
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,ct.section as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'LL-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'LL' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+larrySettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[larrylast_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join larrylast_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+larrySettings.getExchangeId()+" ");
			query.append(" where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.fanxchange_broker_id is not null and mvee.fanxchange_broker_id="+larrySettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+larrySettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append("  and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			
			if(larrySettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
		}
		
		if(lastrowSettings.getIsEnabled() != null && lastrowSettings.getIsEnabled()) {
			
			if(eligibleFlag) {
				query.append(" union ");
			}
			eligibleFlag = true;
			
			query.append("SELECT distinct 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,convert(varchar(500), ct.section) as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'LR-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'LR' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+lastrowSettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[lastrow_minicat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join lastrow_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+lastrowSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.fanxchange_broker_id is not null and mvee.fanxchange_broker_id="+lastrowSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+lastrowSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			
			if(lastrowSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
		}
		
		if(zoneLastrowSettings.getIsEnabled() != null && zoneLastrowSettings.getIsEnabled()) {
			
			if(eligibleFlag) {
				query.append(" union ");
			}
			eligibleFlag = true;
		
			query.append("SELECT distinct 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,convert(varchar(500), ct.section) as Section,");
			query.append("ct.last_row as Row,'' as SeatFrom,'' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'ZLR-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'ZLR' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,e.id,e.venue_category_id,ct.tmat_zone,"+zoneLastrowSettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[zone_lastrow_minicat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join zone_lastrow_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+zoneLastrowSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.fanxchange_broker_id is not null and mvee.fanxchange_broker_id="+zoneLastrowSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+zoneLastrowSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			//query.append(" and ct.event_id not in(select distinct event_id from lastrow_minicat_category_ticket WITH(NOLOCK) where status='ACTIVE') ");
			
			if(zoneLastrowSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (2,4) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
		}
		query.append(" order by event_date,event_time,Event,Section,Quantity");
		
		if(eligibleFlag) {
//			Session session = null;
			try{
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
	public List<Object[]> getLarryAndAllLastRowTicketCityCsv(AutopricingSettings larrySettings,AutopricingSettings lastrowSettings,
			AutopricingSettings zoneLastrowSettings) throws Exception {
		
		List<Object[]> list = null;
		StringBuilder query = new StringBuilder();
		
		boolean eligibleFlag = false;
		if(larrySettings.getIsEnabled() != null && larrySettings.getIsEnabled()) {
			eligibleFlag = true;
			query.append("SELECT DISTINCT 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,ct.section as Section,");
			query.append("ct.last_row as Row,'*' as SeatFrom,'*' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'LL-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'LL' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,e.id,e.venue_category_id,ct.tmat_zone,"+larrySettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[larrylast_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join larrylast_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+larrySettings.getExchangeId()+" ");
			query.append(" where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.ticketcity_broker_id is not null and mvee.ticketcity_broker_id="+larrySettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+larrySettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append("  and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			
			if(zoneLastrowSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (4) and ct.event_id not in (1000281552)  ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
		}
		
		if(lastrowSettings.getIsEnabled() != null && lastrowSettings.getIsEnabled()) {
			
			if(eligibleFlag) {
				query.append(" union ");
			}
			eligibleFlag = true;
			
			query.append("SELECT distinct 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,convert(varchar(500), ct.section) as Section,");
			query.append("ct.last_row as Row,'*' as SeatFrom,'*' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'LR-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'LR' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,e.id,e.venue_category_id,ct.tmat_zone,"+lastrowSettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[lastrow_minicat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join lastrow_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+lastrowSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.ticketcity_broker_id is not null and mvee.ticketcity_broker_id="+lastrowSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+lastrowSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			
			if(zoneLastrowSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (4) and ct.event_id not in (1000281552)   ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
		}
		
		if(zoneLastrowSettings.getIsEnabled() != null && zoneLastrowSettings.getIsEnabled()) {
			
			if(eligibleFlag) {
				query.append(" union ");
			}
			eligibleFlag = true;
		
			query.append("SELECT distinct 'Y' as Edit,e.name as Event,v.building as Venue,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,");
			query.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) ");
			query.append("WHEN e.event_time is null THEN 'TBD' END as EventTime,ct.quantity as Quantity,convert(varchar(500), ct.section) as Section,");
			query.append("ct.last_row as Row,'*' as SeatFrom,'*' as SeatThru,");
			query.append("CASE WHEN ct.quantity>4 THEN 'ZLR-if more than four (4) tickets are purchased from this group your tickets may be split All tickets are guaranteed to be within the same section' ELSE 'ZLR' END as Notes, ");
			query.append("FLOOR(ct.actual_price*(1+(aem.additional_markup/100))) as Cost,ct.id as TicketId,ct.shipping_method_id,CONVERT(VARCHAR(19),DATEADD(day, -5, e.event_date),101) as InHandDate,e.id,e.venue_category_id,ct.tmat_zone,"+zoneLastrowSettings.getProductId()+" as productId,e.event_date,e.event_time ");
			query.append("FROM [tmatprodscale].[dbo].[zone_lastrow_minicat_category_ticket] ct WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = ct.event_id ");
			query.append("inner join venue v WITH(NOLOCK) on v.id = e.venue_id inner join artist a WITH(NOLOCK) on a.id= e.artist_id inner join grand_child_tour_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id ");
			query.append("inner join child_tour_category cc WITH(NOLOCK) on cc.id =gc.child_category_id inner join tour_category tc WITH(NOLOCK) on tc.id = cc.tour_category_id ");
			query.append("inner join zone_lastrow_mini_exchange_event mvee WITH(NOLOCK) on ct.event_id=mvee.event_id ");
			query.append("inner join autopricing_exchange aem WITH(NOLOCK) on aem.id="+zoneLastrowSettings.getExchangeId()+" ");
			query.append("where ct.status='ACTIVE' and e.event_status='ACTIVE' and (mvee.ticketcity_broker_id is not null and mvee.ticketcity_broker_id="+zoneLastrowSettings.getBrokerId()+") and datediff(day,getdate(),e.event_date)>"+zoneLastrowSettings.getExcludeEventDays()+" and ct.actual_price > 0 ");
			query.append(" and ct.section not like 'ZONE%' and (ct.section IS NOT NULL AND ltrim(ct.section) != '') ");
			query.append("and mvee.status='ACTIVE' ");
			//query.append(" and ct.event_id not in(select distinct event_id from lastrow_minicat_category_ticket WITH(NOLOCK) where status='ACTIVE') ");
			
			if(zoneLastrowSettings.getBroker().getName().equals("Manhattan")) {
				query.append(" and tc.id in(1,2) and ct.quantity in (4) and ct.event_id not in (1000281552) ");
			} else {
				query.append(" and ct.quantity in (2,4,6) ");
			}
		}
		query.append(" order by event_date,event_time,Event,Section,Quantity");
		
		if(eligibleFlag) {
//			Session session = null;
			try{
				if(session==null || !session.isOpen() || !session.isConnected()){
					session = sessionFactory.openSession();
					QueryManagerDAO.setSession(session);
				}
				Query queryString = session.createSQLQuery(query.toString());
				list = queryString.list();
				return list;
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{

			}
		}
		return list;
		
	}
	
public List<AutoCats96TnCategoryTicket> getTnAutoCatsAbove96CategoryTicket(String brokerId)  throws Exception {

	    StringBuilder sql=new StringBuilder();
//		Session session=null;
//		session = sessionFactory.openSession();
	    if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		 String  brokerLinkedServer = "";
			    
			if(brokerId!=null && !brokerId.isEmpty()){
				Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
				ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
				brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
			}
		sql.append("select distinct e.event_name as EventName,");
		sql.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
		sql.append(" CASE WHEN e.event_datetime is not null THEN ");
		sql.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))");
		sql.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
		sql.append(" v.name as Venue,vc.section_low as Section,vc.row_low as Row,tg.ticket_count as Quantity,retail_price as Price,e.event_datetime ,tg.internal_notes as InternalNote");
		sql.append(" from "+brokerLinkedServer+".category_ticket_group tg ");
		sql.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
		sql.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
		sql.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
		sql.append(" where internal_notes in ('MINICATS','VIPMINICATS','LASTROW MINICATS','AUTOCAT','VIPLR') and retail_price>0 and broadcast=1 and e.event_datetime > GETDATE()");
		sql.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)  ");
		sql.append(" order by e.event_datetime,EventName,Section,Row,Quantity");
	 try{
		 Query query = session.createSQLQuery(sql.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new StringType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType()).addScalar("InternalNote",new StringType());
		 List<AutoCats96TnCategoryTicket> tix=query.setResultTransformer(Transformers.aliasToBean(AutoCats96TnCategoryTicket.class)).list();		
		return tix;
	 }catch(Exception e){
		 e.printStackTrace();
		 throw e;
	 }
}

public List<AutoCats96TnCategoryTicket> getTnSgLastFiveRowCategoryTicket(String brokerId)  throws Exception {

    StringBuilder sql=new StringBuilder();
//	Session session=null;
//	session = sessionFactory.openSession();
    if(session==null || !session.isOpen() || !session.isConnected()){
		session = sessionFactory.openSession();
		QueryManagerDAO.setSession(session);
	}
	 String  brokerLinkedServer = "";
		    
		if(brokerId!=null && !brokerId.isEmpty()){
			Broker broker = BrokerUtils.getBrokerById(Integer.parseInt(brokerId));
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			brokerLinkedServer =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		}
	sql.append("select distinct e.event_name as EventName,");
	sql.append(" CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate,");
	sql.append(" CASE WHEN e.event_datetime is not null THEN ");
	sql.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))");
	sql.append(" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,");
	sql.append(" v.name as Venue,vc.section_low as Section,vc.row_low as Row,tg.ticket_count as Quantity,retail_price as Price,e.event_datetime ,tg.internal_notes as InternalNote");
	sql.append(" from "+brokerLinkedServer+".category_ticket_group tg ");
	sql.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
	sql.append(" inner join "+brokerLinkedServer+".event e on e.event_id=tg.event_id ");
	sql.append(" inner join "+brokerLinkedServer+".venue v on v.venue_id=e.venue_id ");
	sql.append(" where internal_notes in ('SGK') and retail_price>0 and broadcast=1 and e.event_datetime > GETDATE()");
	sql.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)  ");
	sql.append(" order by e.event_datetime,EventName,Section,Row,Quantity");
 try{
	 Query query = session.createSQLQuery(sql.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new StringType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType()).addScalar("InternalNote",new StringType());
	 List<AutoCats96TnCategoryTicket> tix=query.setResultTransformer(Transformers.aliasToBean(AutoCats96TnCategoryTicket.class)).list();		
	return tix;
 }catch(Exception e){
	 e.printStackTrace();
	 throw e;
 }
}

public List<TicketListingCrawl> getActiveCrawlsByEventId(String eventId) throws Exception {
	try{
		List<TicketListingCrawl> list = null;
//		Session session=null;
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		String sql = "select id as id,event_id as eventId,end_crawl as endCrawl from ticket_listing_crawl where event_id = :eventId and enabled = 1";
		Query query = session.createSQLQuery(sql);
		query.setParameter("eventId", eventId);
		list = query.setResultTransformer(Transformers.aliasToBean(TicketListingCrawl.class)).list();
		return list;
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}
}

public TicketListingCrawl getActiveCrawlById(String crawlId) throws Exception {
	try{
		List<TicketListingCrawl> list = null;
		Session session=null;
		session = sessionFactory.openSession();
		String sql = "select id as id,event_id as eventId,end_crawl as endCrawl from ticket_listing_crawl where id = :crawlId and enabled = 1";
		Query query = session.createSQLQuery(sql);
		query.setParameter("crawlId", crawlId);
		list = query.setResultTransformer(Transformers.aliasToBean(TicketListingCrawl.class)).list();
		return list.get(0);
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}
}

public List<TNDZonesCategoryTicket> getAllTNDZonesElibibleOrders(Integer hourstoConsiderOrders,Integer excludeEventDays) throws Exception {
	try{
		List<TNDZonesCategoryTicket> catsList = null;
		Session session=null;
		session = sessionFactory.openSession();
		String sql = "select order_id as tndOrderId,event_id as tnExchangeEventId,section as section,row as lastRow,ticket_quantity as quantity," +
				" wholesale_price as actualPrice,wholesale_price as tnPrice from tnd_sales_autopricing" +
				" where order_date>dateadd(hh,-"+hourstoConsiderOrders+",getdate()) and section like '%zone%' and row=''" +
				" and event_date>dateadd(hh,24*"+excludeEventDays+",getdate())" +
				" and owner_company_name not like '%right this way%'";
		SQLQuery query = session.createSQLQuery(sql);
		query.addScalar("tndOrderId",Hibernate.INTEGER);
		query.addScalar("tnExchangeEventId",Hibernate.INTEGER);
		query.addScalar("section",Hibernate.STRING);
		query.addScalar("lastRow",Hibernate.STRING);
		query.addScalar("quantity",Hibernate.INTEGER);
		query.addScalar("actualPrice",Hibernate.DOUBLE);
		query.addScalar("tnPrice",Hibernate.DOUBLE);
		catsList = query.setResultTransformer(Transformers.aliasToBean(TNDZonesCategoryTicket.class)).list();
		return catsList;
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}
}

public List<TNDRealCategoryTicket> getAllTNDRealTixElibibleOrders(Integer hourstoConsiderOrders,Integer excludeEventDays) throws Exception {
	try{
		List<TNDRealCategoryTicket> catsList = null;
		Session session=null;
		session = sessionFactory.openSession();
		String sql = "select order_id as tndOrderId,event_id as tnExchangeEventId,section as section,row as lastRow,ticket_quantity as quantity," +
				" wholesale_price as actualPrice,wholesale_price as tnPrice from tnd_sales_autopricing" +
				" where  order_date>dateadd(hh,-"+hourstoConsiderOrders+",getdate()) and is_category='N' and section  not like 'zone%'" +
				" and event_date>dateadd(hh,24*"+excludeEventDays+",getdate())" +
				" and owner_company_name not like '%right this way%'";
		SQLQuery query = session.createSQLQuery(sql);
		query.addScalar("tndOrderId",Hibernate.INTEGER);
		query.addScalar("tnExchangeEventId",Hibernate.INTEGER);
		query.addScalar("section",Hibernate.STRING);
		query.addScalar("lastRow",Hibernate.STRING);
		query.addScalar("quantity",Hibernate.INTEGER);
		query.addScalar("actualPrice",Hibernate.DOUBLE);
		query.addScalar("tnPrice",Hibernate.DOUBLE);
		catsList = query.setResultTransformer(Transformers.aliasToBean(TNDRealCategoryTicket.class)).list();
		return catsList;
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}
}
public List<Integer> getUserUnBroadcastedEventIdsForTN() throws Exception {
//	Session session = null;
	List<Integer> list = null;
	
	try{
		String sql = "select event_id from autocats96_exchange_event with(nolock) where status='DELETED' and last_updated_by not in ('AUTO','MANUAL')" +
				" union all" +
				" select event_id from larrylast_exchange_event with(nolock) where status='DELETED' and last_updated_by not in ('AUTO','MANUAL')" +
				" union all" +
				" select event_id from zone_lastrow_mini_exchange_event with(nolock) where status='DELETED' and last_updated_by not in ('AUTO','MANUAL')";
		
		session = QueryManagerDAO.getSession();
		if(session==null || !session.isOpen() || !session.isConnected()){
			session = sessionFactory.openSession();
			QueryManagerDAO.setSession(session);
		}
		Query query = session.createSQLQuery(sql);
		list = query.list();
		return list;
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}
}

	public static Session getSession(){
		return QueryManagerDAO.session;
		
	}

	public static void setSession(Session session) {
		QueryManagerDAO.session = session;
	}

	public List<Integer> getAllActiveManhattanExEvents() throws Exception {
//		Session session = null;
		List<Integer> list = null;
		
		try{
			String sql = "select e.admitone_id from manhattan_zone_pricing_exchange_event mee inner join event e on e.id=mee.event_id " +
					"where  e.event_status='ACTIVE'";
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	public List<Integer> getAllActiveTixCityNewExEvents() throws Exception {
//		Session session = null;
		List<Integer> list = null;
		
		try{
			String sql = "select e.admitone_id from tixcity_zone_pricing_exchange_event mee inner join event e on e.id=mee.event_id " +
					"where  e.event_status='ACTIVE'";
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	
	public List<BrokerStatus> getAllBrokersStatus(){
		List<BrokerStatus> list = null;
		try{
			String sql ="SELECT MAX(t.id) AS id, product_id as productId, broker_id as brokerId, process_type as processType, MAX(last_run_time) as lastRunTime, MAX(COUNT) AS count " +
			   	" FROM (SELECT apa.*, ROW_NUMBER() OVER (PARTITION BY product_id, broker_id, process_type ORDER BY id DESC) AS RN " + 
			   	" FROM autocat_projects_audit apa) AS t " +
				" WHERE RN = 1 " + //and DATEDIFF(day,last_run_time,GETDATE())=0 " +
				" GROUP BY product_id, broker_id, process_type " +
				" ORDER BY productId, brokerId" ;
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql)
			.addScalar("id", Hibernate.INTEGER)
			.addScalar("productId", Hibernate.INTEGER)
			.addScalar("brokerId", Hibernate.INTEGER)
			.addScalar("processType", Hibernate.STRING)
			.addScalar("lastRunTime", Hibernate.DATE)
			.addScalar("count", Hibernate.INTEGER);
			list = query.setResultTransformer(Transformers.aliasToBean(BrokerStatus.class)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/*public List<Object[]> getLocalEventIdsToUnbroadcastEvents(Date now, Date before15MinsFromNow){
		List<Object[]> list = null;
		try{
			String sql ="select rtw.event_id,rtw.exchange_event_id from rtw_sold_ticket_detail_vw rtw " +
					//"inner join "+rtwPos+".exchange_event ee on rtw.exchange_event_id = ee.Id "+
						//"inner join "+ rtwPos +".event_parent_category epc on ee.parentID = epc.event_parent_category_id " +
						" where rtw.invoice_datetime <= :now and " +
						"rtw.invoice_datetime >= :before15MinsFromNow group by event_id,exchange_event_id having count(*) > 2";
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql).setParameter("now", now).setParameter("before15MinsFromNow", before15MinsFromNow);
			list = query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}*/
	
	public List<Integer> getExchangeEventIdsToUnbroadcastEventsByBroker(Broker broker,Date now, Date before15MinsFromNow){
		List<Integer> list = null;
		try{
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
			
			String sql ="select e.exchange_Event_id from (" +
					" select distinct i.invoice_id,event_id from "+serverName+".invoice i" +
					" inner join "+serverName+".category_ticket ct on ct.invoice_id=i.invoice_id" +
					" inner join "+serverName+".category_ticket_group ctg on ctg.category_ticket_group_id=ct.category_ticket_group_id" +
					" where i.create_date >= :before15MinsFromNow and i.create_date <= :now " +
					" union" +
					" select distinct i.invoice_id,event_id from "+serverName+".invoice i" +
					" inner join "+serverName+".ticket t on t.invoice_id=i.invoice_id" +
					" inner join "+serverName+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id" +
					" where i.create_date >= :before15MinsFromNow and i.create_date <= :now" +
					" and tg.office_id="+broker.getSsAccountOfficeId()+" and internal_notes in('ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') " +
					" ) as g" +
					" inner join "+serverName+".event e on e.event_id=g.event_id" +
					" inner join "+serverName+".exchange_event ee on ee.id=e.exchange_Event_id" +
					" where e.exchange_event_id is not null and e.exchange_event_id >0 and ee.grandchildID != 32 " +
					" and e.event_name not like '%ICC - International Champions Cup:%' " +//32--NFL and e.event_name !='pink' and e.event_name not like '%taylor swift%' 
					" group by g.event_id,e.exchange_Event_id" +
					" having count(distinct invoice_id)>2";
			
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql).setParameter("before15MinsFromNow", before15MinsFromNow).setParameter("now", now);
			list = query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	public List<Integer> getAllEventIdssInUnbroadcatedStatusByIdStr(String eventIdsStr){
		List<Integer> eventIds = null;
		try{
			String sql ="select distinct id from event where event_status='ACTIVE' and id in (" +
					" select distinct event_id from autocats96_exchange_Event where status='DELETED' and last_updated_by='UNBROADCAST EVENT JOB'" +
					" and event_id in ("+eventIdsStr+")" +
					" union" +
					" select distinct event_id from larrylast_exchange_Event where status='DELETED' and last_updated_by='UNBROADCAST EVENT JOB'" +
					" and event_id in ("+eventIdsStr+")" +
					" union" +
					" select distinct event_id from zone_tickets_processor_exchange_Event where status='DELETED' and last_updated_by='UNBROADCAST EVENT JOB'" +
					" and event_id in ("+eventIdsStr+"))" ;
			
			session = QueryManagerDAO.getSession();
			if(session==null || !session.isOpen() || !session.isConnected()){
				session = sessionFactory.openSession();
				QueryManagerDAO.setSession(session);
			}
			Query query = session.createSQLQuery(sql);
			eventIds =  query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return eventIds;
	}
	

	public List<TicketListingCrawl> getAllTicketListingCrawlForPresaleSetingsCrawlFrequencyUpdate() throws Exception {
		try{
			List<TicketListingCrawl> crawls = null;
			Session session=null;
			session = sessionFactory.openSession();
			String sql = "select id as id,event_id as eventId,site_id as siteId,crawl_frequency as crawlFrequency,automatic_crawl_frequency as automaticCrawlFrequency " +
					" from ticket_listing_crawl where enabled=1 and site_id in ('stubhub','ticketevolution','ticketnetworkdirect')" + 
					" and (crawl_frequency>86400)" + 
					" and event_id in (select event_id from autocats96_exchange_event where status='ACTIVE' and lower_markup=0 and upper_markup=0)" + 
					" order by event_id";
			SQLQuery query = session.createSQLQuery(sql);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("eventId",Hibernate.INTEGER);
			query.addScalar("siteId",Hibernate.STRING);
			query.addScalar("crawlFrequency",Hibernate.INTEGER);
			query.addScalar("automaticCrawlFrequency",Hibernate.BOOLEAN);
			crawls = query.setResultTransformer(Transformers.aliasToBean(TicketListingCrawl.class)).list();
			return crawls;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	public Integer updateTicketListingCrawlPresaleSetingsCrawlFrequency(String crawlIdStr) throws Exception {
		Integer count =0;
		try{
			Session session=null;
			session = sessionFactory.openSession();
			String query = "update ticket_listing_crawl set automatic_crawl_frequency=0,crawl_frequency=86400,last_updater='AUTO_CATS',last_updated=getdate() where id in ("+crawlIdStr+")";
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			return count;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	public List<TicketListingCrawl> getAllTicketListingCrawlOfNonPresaleSetingsEvenstForUpdate() throws Exception {
		try{
			List<TicketListingCrawl> crawls = null;
			Session session=null;
			session = sessionFactory.openSession();
			String sql = "select id as id,event_id as eventId,site_id as siteId,crawl_frequency as crawlFrequency,automatic_crawl_frequency as automaticCrawlFrequency " + 
					" from ticket_listing_crawl where enabled=1 and last_updater='AUTO_CATS'  " + 
					" and event_id not in (select event_id from autocats96_exchange_event where status='ACTIVE' and lower_markup=0 and upper_markup=0) " + 
					" order by event_id";
			SQLQuery query = session.createSQLQuery(sql);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("eventId",Hibernate.INTEGER);
			query.addScalar("siteId",Hibernate.STRING);
			query.addScalar("crawlFrequency",Hibernate.INTEGER);
			query.addScalar("automaticCrawlFrequency",Hibernate.BOOLEAN);
			crawls = query.setResultTransformer(Transformers.aliasToBean(TicketListingCrawl.class)).list();
			return crawls;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	public Integer updateTicketListingCrawlFrequencytoNormalForNonPresaleEvents(String crawlIdStr) throws Exception {
		Integer count =0;
		try{
			Session session=null;
			session = sessionFactory.openSession();
			String query = "update  ticket_listing_crawl set automatic_crawl_frequency=1,last_updater='AUTO',last_updated=getdate()  where enabled=1 and last_updater='AUTO_CATS' and id in ("+crawlIdStr+") ";  
					//" and event_id not in (select event_id from autocats96_exchange_event where status='ACTIVE' and lower_markup=0 and upper_markup=0)";
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			return count;
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
