package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;



public interface AutopricingVenueSettingsDAO extends RootDAO<Integer,  AutopricingVenueSettings>{

	public List<AutopricingVenueSettings> getAllActiveAutopricingVenueSettings()  throws Exception;
	public List<AutopricingVenueSettings> getAutopricingVenueSettingsByVenueId(Integer venueId)  throws Exception;
}
