package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleAutoCatCategoryTicket;


public class PresaleAutoCatCategoryTicketDAO extends HibernateDAO<Integer, PresaleAutoCatCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.PresaleAutoCatCategoryTicketDAO {

	public void deleteAll(Collection<PresaleAutoCatCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<PresaleAutoCatCategoryTicket> getAllActivePresaleAutoCatsCategoryTickets()  throws Exception {
		return find("FROM PresaleAutoCatCategoryTicket where status ='ACTIVE'");
	}
	
	public List<PresaleAutoCatCategoryTicket> getAllPresaleAutoCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity)  throws Exception {
		String query = "FROM PresaleAutoCatCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(Integer.parseInt(quantity));								
		
			} 
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND (rowRange=? OR lastRow=?)";
			param.add(row);
			param.add(row);
		}
		
		
		return find(query, param.toArray());
	}
	
	public List<PresaleAutoCatCategoryTicket> getAllPresaleAutoCatCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId)  throws Exception {
		
		List<PresaleAutoCatCategoryTicket> result = new ArrayList<PresaleAutoCatCategoryTicket>();
		try {
			Map<Integer,String> map = new HashMap<Integer, String>();
			String tnCategoryTicketGroupIdsStr = "";
			int i = 0;
			int count =0;
			for(Integer id:categoryTicektGroupIds){
				if(count==2000){
					count =0;
					i++;
					map.put(i, tnCategoryTicketGroupIdsStr);
					tnCategoryTicketGroupIdsStr ="";
				}
				tnCategoryTicketGroupIdsStr = tnCategoryTicketGroupIdsStr + id + ",";
				count++;
			}
			if(!tnCategoryTicketGroupIdsStr.isEmpty()){
				map.put(++i, tnCategoryTicketGroupIdsStr);
			}
			for(Integer ii:map.keySet()){
				tnCategoryTicketGroupIdsStr = map.get(ii);
				if(!tnCategoryTicketGroupIdsStr.isEmpty()){
					tnCategoryTicketGroupIdsStr = tnCategoryTicketGroupIdsStr.substring(0, tnCategoryTicketGroupIdsStr.length()-1);
				}
				Collection<PresaleAutoCatCategoryTicket> temp = find("FROM PresaleAutoCatCategoryTicket where tnBrokerId="+brokerId+" and tnCategoryTicketGroupId IN ("  + tnCategoryTicketGroupIdsStr + ")" +
						" and status<>'SOLD'"); 
				if(temp!=null){
					result.addAll(temp);
				}
			}
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//return result;
	}
	
	public List<PresaleAutoCatCategoryTicket> getAllPresaleAutoCatsCategoryTicketsById(Integer Id)  throws Exception {
		return find("FROM PresaleAutoCatCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id)  throws Exception {
		return find(" select priceHistory FROM PresaleAutoCatCategoryTicket where id=?" ,new Object[]{Id});
	}
	
	public List<PresaleAutoCatCategoryTicket> getAllPresaleAutoCatCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM PresaleAutoCatCategoryTicket where eventId=? AND status=?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<PresaleAutoCatCategoryTicket> getAllTNPresaleAutoCatCategoryTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM PresaleAutoCatCategoryTicket where eventId=? and status = ? and tnCategoryTicketGroupId is not null and tnCategoryTicketGroupId <> 0" ,new Object[]{eventId,"ACTIVE"});
	}
	
	public List<PresaleAutoCatCategoryTicket> getAllTNPresaleAutoCatCategoryTicketsByEventIdByBrokerID(Integer eventId,Integer brokerId)  throws Exception {
		return find("FROM PresaleAutoCatCategoryTicket where eventId=? and status = ? and tnBrokerId = ? and tnCategoryTicketGroupId is not null" ,new Object[]{eventId,"ACTIVE",brokerId});
	}
	
	public Integer deleteAllPresaleAutoCatCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
				" FROM presale_autocat_category_ticket tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllPresaleAutoCatCategoryTicketsnotExistinExchangeEventforAnyExchange()  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
				" FROM presale_autocat_category_ticket tg WITH(NOLOCK)  " +
			    " WHERE tg.status='ACTIVE' AND event_id NOT IN (" +
			    " SELECT event_id FROM presale_autocat_exchange_event le WITH(NOLOCK)" +
			    " inner join event e on e.id=le.event_id " +
			    " inner join venue_category vc on vc.id=e.venue_category_id" +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND e.presale_event=1 AND e.venue_category_id is not null AND e.venue_id is not null " +//AND vc.category_group='presale'
			    " AND (le.rot_pos_enabled=1 OR le.rtw_pos_enabled=1 OR le.vivid_enabled=1)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllTnPresaleAutoCatCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception {
		String sql ="  UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
				" FROM presale_autocat_category_ticket tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_category_ticket_group_id is not null AND tg.status='ACTIVE' AND e.event_status='ACTIVE' " +
	    		" AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllTnPresaleAutoCatCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId)  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
	    " FROM presale_autocat_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_category_ticket_group_id is not null AND tg.status='ACTIVE' AND event_id NOT IN (" +
	    " SELECT event_id FROM presale_autocat_exchange_event le WITH(NOLOCK)" +
	    " inner join event e on e.id=le.event_id " +
	    " where e.event_status='ACTIVE' AND status='ACTIVE'";
	    
	    if(brokerId == 10){//RTW-2	/*Reserve One Tickets*/
			sql += " AND le.rot_pos_enabled =1";
		}else if(brokerId == 5){	/*RTW*/
			sql += " AND le.rtw_pos_enabled =1";
		}
	    sql += " ) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllPresaleAutoCatTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct)  throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
	    		" FROM presale_autocat_category_ticket tg WITH(NOLOCK)  " +
	    		" WHERE tg.tn_broker_id="+broker.getId()+" AND tg.tn_category_ticket_group_id is not null AND tg.status='ACTIVE' " +
	    		" and tg.tn_category_ticket_group_id not in(select distinct category_ticket_group_id from "+serverName+".category_ticket_group WITH(NOLOCK) " +
	    		" where internal_notes='"+autopricingProduct.getInternalNotes()+"')";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	

	public List<Integer> getAllTicketNetworkGroupIdsNotExistInPresaleAutoCatCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct)  throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql = " SELECT ctg.category_ticket_group_id as groupId " +
					" FROM " + serverName +".category_ticket_group ctg WITH(NOLOCK) " + 
					 " INNER JOIN " + serverName + ".category_ticket ct ON ct.category_ticket_group_id = ctg.category_ticket_group_id " +
					 " LEFT JOIN " + serverName + ".invoice i ON i.invoice_id = ct.invoice_id " +
					 " WHERE ctg.category_ticket_group_id NOT IN (select distinct tn_category_ticket_group_id from presale_autocat_category_ticket WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND tn_broker_id="+broker.getId()+" AND tn_category_ticket_group_id is not null) AND ctg.internal_notes = '"+autopricingProduct.getInternalNotes()+"' " +
					 " and datediff(minute,ctg.create_date,GETDATE())>15" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 " GROUP BY ctg.category_ticket_group_id " +
					 " HAVING SUM(CASE WHEN i.invoice_id IS NOT NULL THEN 1 ELSE 0 END )=0";
		
		Session session=null;
		
//		List<Integer> ticketGroupIdList =  new ArrayList<Integer>();
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			/*if(list != null || list.size() > 0) {
				for (Object[] obj : list) {
					ticketGroupIdList.add(Integer.parseInt(obj[0].toString()));
				}
			}
			return ticketGroupIdList;*/
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	
	}

}

