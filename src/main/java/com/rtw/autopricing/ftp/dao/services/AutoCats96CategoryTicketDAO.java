package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;



public interface AutoCats96CategoryTicketDAO extends RootDAO<Integer, AutoCats96CategoryTicket> {

	public Collection<AutoCats96CategoryTicket> getAllActiveTgCatsCategoryTickets()throws Exception;
	public List<AutoCats96CategoryTicket> getAllAutoCats96CatsCategoryTicketsByAll(Integer eventId,String productName,String section,String row,String quantity)throws Exception;
	
	public List<AutoCats96CategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id)throws Exception;
	public List<String> getPriceHistoryById(Integer Id)throws Exception;
	public List<AutoCats96CategoryTicket> getAllMiniCategoryTicketsByEventId(Integer eventId)throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInAutoCats96CategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct)throws Exception;
	public List<AutoCats96CategoryTicket> getAllTNAutoCats96CategoryTicketsByEventId(Integer eventId)throws Exception;
	public List<AutoCats96CategoryTicket> getAllTNAutoCats96CategoryTicketsByEventIdNew(Integer eventId)throws Exception;
	public List<AutoCats96CategoryTicket> getAllTNAutoCats96CategoryTicketsByEventIdByBrokerID(Integer eventId,Integer brokerId)throws Exception;
	public List<AutoCats96CategoryTicket> getAllTNAutoCats96CategoryTicketsByEventIdByBrokerIDByProduct(Integer eventId,Integer brokerId,String ProductName)throws Exception;
	public Integer deleteAllAutoCats96CategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays)throws Exception;
	public Integer deleteAllAutoCatsCategoryTicketsnotExistinExchangeEventforAnyExchange()throws Exception;
	public Integer deleteAllTnAutoCats96CategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays)throws Exception;
	public Integer deleteAllTnAutoCats96CategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId)throws Exception;
	public Integer deleteAllAutoCats96TnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct)throws Exception;
	public List<AutoCats96CategoryTicket> getAllAutoCats96CategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
	public Integer deleteAllAutoCats96CategoryTicketsnotExistinExchangeEventByBroker(Integer brokerId)throws Exception;
	public Integer deleteAllAutoCats96CategoryTicketsInWhichProductDisabled(String internalNotes)throws Exception;
}

