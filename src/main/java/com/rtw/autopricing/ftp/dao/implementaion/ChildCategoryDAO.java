package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import com.rtw.autopricing.ftp.data.ChildCategory;


public class ChildCategoryDAO extends HibernateDAO<Integer, ChildCategory> implements com.rtw.autopricing.ftp.dao.services.ChildCategoryDAO {
	public List<ChildCategory> getChildCategoryByTourCategory(Integer id){
		return find("FROM ChildCategory ctc where ctc.tourCategory.id= ? ORDER BY name ",new Object[]{id});
	}

	public ChildCategory getChildCategoryByNameAndTourCategoryName(String name, String tourCategoryName) {
		return findSingle("FROM ChildCategory ctc WHERE name = ? AND ctc.tourCategory.name=? ",new Object[]{name,tourCategoryName});
	}

	@SuppressWarnings("unchecked")
	public List<ChildCategory> getChildCategoryOrderByName() {
		return find("FROM ChildCategory ORDER BY name ");
	}

	public Collection<ChildCategory> getChildCategoryByTourCategories(List<Integer> ids) {
		Query query = getSession().createQuery("FROM ChildCategory where tourCategory.id in (:Ids)  ORDER By name").setParameterList("Ids", ids);
		@SuppressWarnings("unchecked")
		List<ChildCategory> list= query.list();
		return list;
	}
	
	/**
	 * return grand child by name where name should match the input
	 */
	public Collection<ChildCategory> getChildTourCategoriesByName(String name) {
		String hql="FROM ChildCategory WHERE name like ? and name <> 'NONE' and name <> 'OTHER' ORDER BY name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
	}
	
	@Override
	public Integer save(ChildCategory entity) {
		/*super.save(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentChildCategory dependentChildCategory = new TMATDependentChildCategory();
				dependentChildCategory.setChildCategoryId(entity.getId());
				dependentChildCategory.setDependent(zone);
				dependentChildCategory.setAdd(true);
				DAORegistry.getTmatDependentChildCategoryDAO().save(dependentChildCategory);
			}
		}
		return  entity.getId();
		*/
		return  0;
	}
	
	@Override
	public void update(ChildCategory entity) {
		/*super.update(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		List<TMATDependentChildCategory> list = DAORegistry.getTmatDependentChildCategoryDAO().getTmatDependentsByChildCategoryId(entity.getId());
		Map<String, TMATDependentChildCategory> map = new HashMap<String, TMATDependentChildCategory>();
		if(list!=null && !list.isEmpty()){
			for(TMATDependentChildCategory cat:list){
				map.put(cat.getDependent(), cat);
			}
		}
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentChildCategory dependentChildCategory = map.get(zone);
				if(dependentChildCategory==null){
					dependentChildCategory = new TMATDependentChildCategory();
				}
				dependentChildCategory.setChildCategoryId(entity.getId());
				dependentChildCategory.setDependent(zone);
				dependentChildCategory.setAdd(true);
				DAORegistry.getTmatDependentChildCategoryDAO().saveOrUpdate(dependentChildCategory);
			}
		}*/
	}
	
	@Override
	public void delete(ChildCategory entity) {
		/*super.delete(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		List<TMATDependentChildCategory> list = DAORegistry.getTmatDependentChildCategoryDAO().getTmatDependentsByChildCategoryId(entity.getId());
		Map<String, TMATDependentChildCategory> map = new HashMap<String, TMATDependentChildCategory>();
		if(list!=null && !list.isEmpty()){
			for(TMATDependentChildCategory cat:list){
				map.put(cat.getDependent(), cat);
			}
		}
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentChildCategory dependentChildCategory = map.get(zone);
				if(dependentChildCategory==null){
					dependentChildCategory = new TMATDependentChildCategory();
				}
				dependentChildCategory.setChildCategoryId(entity.getId());
				dependentChildCategory.setDependent(zone);
				dependentChildCategory.setAdd(false);
				DAORegistry.getTmatDependentChildCategoryDAO().saveOrUpdate(dependentChildCategory);
			}
		}*/
	}
}
