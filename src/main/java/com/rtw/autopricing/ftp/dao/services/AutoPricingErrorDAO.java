package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.AutoPricingError;


public interface AutoPricingErrorDAO extends RootDAO<Integer, AutoPricingError>{
	
}
