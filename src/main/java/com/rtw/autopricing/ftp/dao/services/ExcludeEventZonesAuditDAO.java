package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.ExcludeEventZonesAudit;

public interface ExcludeEventZonesAuditDAO extends RootDAO<Integer,ExcludeEventZonesAudit> {
    
	public List<ExcludeEventZonesAudit> getExcludeEventZonesAuditByProductIdAndBrokerIdAndEventId(
			Integer productId, Integer brokerId, Integer eventId,String zone)  throws Exception;
}
