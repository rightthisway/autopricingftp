package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.ChildCategory;


public interface ChildCategoryDAO extends RootDAO<Integer, ChildCategory> {
	public List<ChildCategory> getChildCategoryByTourCategory(Integer id);
	public ChildCategory getChildCategoryByNameAndTourCategoryName(String name, String tourCategoryName);
	public List<ChildCategory> getChildCategoryOrderByName();
	public Collection<ChildCategory> getChildCategoryByTourCategories(List<Integer> ids);
	public Collection<ChildCategory> getChildTourCategoriesByName(String name);
}
