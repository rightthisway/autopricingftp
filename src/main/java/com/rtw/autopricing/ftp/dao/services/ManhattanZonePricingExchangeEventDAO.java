package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEvent;

public interface ManhattanZonePricingExchangeEventDAO extends RootDAO<Integer, ManhattanZonePricingExchangeEvent> {
	public Collection<ManhattanZonePricingExchangeEvent> getAllManhattanZonePricingExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	public ManhattanZonePricingExchangeEvent getManhattanZonePricingExchangeEventByEventId(Integer eventId)  throws Exception;
	Collection<ManhattanZonePricingExchangeEvent> getAllManhattanZonePricingExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<ManhattanZonePricingExchangeEvent> getAllManhattanZonePricingExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<ManhattanZonePricingExchangeEvent> getAllManhattanZonePricingExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	Collection<ManhattanZonePricingExchangeEvent> getAllActiveManhattanZonePricingExchangeEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public List<ManhattanZonePricingExchangeEvent> getManhattanZonePricingExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId)  throws Exception;
	public Collection<ManhattanZonePricingExchangeEvent> getAllManhattanZonePricingExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<ManhattanZonePricingExchangeEvent> getAllManhattanZonePricingExchangeEventsByEventIds(String eventIds)  throws Exception;
}
