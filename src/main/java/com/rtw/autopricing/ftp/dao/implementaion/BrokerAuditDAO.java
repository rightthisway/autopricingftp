package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.BrokerAudit;


public class BrokerAuditDAO extends HibernateDAO<Integer, BrokerAudit> implements com.rtw.autopricing.ftp.dao.services.BrokerAuditDAO{

	
	public List<BrokerAudit> getAllAuditsByBrokerId(Integer brokerId) {
		return find("FROM BrokerAudit WHERE brokerId=?", new Object[]{brokerId});
	}
}
