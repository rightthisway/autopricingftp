package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEvent;

public interface TixCityZonePricingExchangeEventDAO extends RootDAO<Integer, TixCityZonePricingExchangeEvent> {

	public TixCityZonePricingExchangeEvent getTixCityZonePricingExchangeEventByEventId(Integer eventId)  throws Exception;
	Collection<TixCityZonePricingExchangeEvent> getAllTixCityZonePricingExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<TixCityZonePricingExchangeEvent> getAllTixCityZonePricingExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<TixCityZonePricingExchangeEvent> getAllTixCityZonePricingExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	public Collection<TixCityZonePricingExchangeEvent> getAllTixCityZonePricingExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	List<TixCityZonePricingExchangeEvent> getTixCityZonePricingExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<TixCityZonePricingExchangeEvent> getAllTixCityZonePricingExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<TixCityZonePricingExchangeEvent> getAllTixCityZonePricingExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<TixCityZonePricingExchangeEvent> getAllTixCityZonePricingExchangeEventsByEventIds(String eventIds)  throws Exception;
}
