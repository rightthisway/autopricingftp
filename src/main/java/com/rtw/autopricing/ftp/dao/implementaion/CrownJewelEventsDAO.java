package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.CrownJewelEvents;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEvent;

public class CrownJewelEventsDAO extends HibernateDAO<Integer, CrownJewelEvents> implements com.rtw.autopricing.ftp.dao.services.CrownJewelEventsDAO{

	public CrownJewelEvents getCrownJewelEventByEventId(Integer eventId) throws Exception{
		return findSingle("FROM CrownJewelEvents where eventId=?", new Object[] {eventId});
	}
	public List<CrownJewelEvents> getCrownJewelEventByParentType(String parentType) throws Exception{
		return find("FROM CrownJewelEvents where parentType=?", new Object[] {parentType});
	}
	public Collection<CrownJewelEvents> getAllCrownJewelEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct)  throws Exception {
		String sql =" select distinct le.id as id,le.event_id as eventId, le.exposure as exposure, " +
					" le.rpt_factor as rptFactor,le.price_breakup as priceBreakup,le.lower_markup as lowerMarkup, " +
					" le.upper_markup as upperMarkup,le.lower_shipping_fees as lowerShippingFees, " +
					" le.upper_shipping_fees as upperShippingFees,le.shipping_method as shippingMethod, " +
					" le.near_term_display_option as nearTermDisplayOption,le.allow_section_range as allowSectionRange, " +
					" le.shipping_days as shippingDays,le.parent_type as cjParentType," +
					//" le.vivid_broker_id as vividBrokerId,le.tn_broker_id as ticketNetworkBrokerId," +
					//" le.scorebig_broker_id as scoreBigBrokerId,le.fanxchange_broker_id as fanxchangeBrokerId,le.ticketcity_broker_id as ticketcityBrokerId," +
					//" le.zoneticket_broker_id as zoneTicketBrokerId,le.is_zone as zone " + 
					" le.zoneticket_broker_id as zoneTicketBrokerId" +
					" from crownjewel_events le " + 
					" INNER join ticket_listing_crawl tc on tc.event_id = le.event_id " +
					" INNER JOIN event e on e.id = le.event_id " +
					" INNER JOIN venue_category vc on vc.id = e.venue_category_id " +
					//" LEFT JOIN zone_ticket_locked_ticket_event_details zlt on zlt.event_id=e.id" +
					" LEFT JOIN autopricing_error aer on aer.event_id=e.id" +
					" left join exclude_event_zones ez on ez.event_id=e.id and ez.product_id="+autopricingProduct.getId() +
					" left join exclude_venue_category_zones vcz on vcz.venue_category_id=e.venue_category_id and vcz.product_id="+autopricingProduct.getId() +
					" WHERE le.status = 'ACTIVE' " +
					" AND tc.site_id in ('ticketnetworkdirect','stubhub','ticketevolution','flashseats','vividseat') " +
					" AND le.zoneticket_broker_id is not null" +
					" AND (DATEDIFF(MINUTE,tc.end_crawl,GETDATE())<=  " + minute +
					" OR DATEDIFF(MINUTE,vc.last_updated_date,GETDATE())<=  " + minute +
					" OR (DATEDIFF(MINUTE,le.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (DATEDIFF(MINUTE,e.last_update,GETDATE())<= " + minute + ")" +
				//	" OR (zlt.created_date is not null AND DATEDIFF(MINUTE,zlt.created_date,GETDATE())<= " + minute + ")" +
					" OR (aer.product_id="+autopricingProduct.getId()+" and aer.time_stamp is not null and DATEDIFF(MINUTE,aer.time_stamp,GETDATE())<= " + minute + ")" +
					" OR (ez.id is not null and DATEDIFF(MINUTE,ez.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (vcz.id is not null and DATEDIFF(MINUTE,vcz.last_updated_date,GETDATE())<= " + minute + ")) " ;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			sqlQuery.addScalar("id",Hibernate.INTEGER);
			sqlQuery.addScalar("exposure",Hibernate.STRING);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("rptFactor",Hibernate.DOUBLE);
			sqlQuery.addScalar("shippingMethod",Hibernate.INTEGER);
			sqlQuery.addScalar("nearTermDisplayOption",Hibernate.INTEGER);
			
			sqlQuery.addScalar("priceBreakup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperShippingFees",Hibernate.DOUBLE);
			//sqlQuery.addScalar("vividBrokerId",Hibernate.INTEGER);
			//sqlQuery.addScalar("scoreBigBrokerId",Hibernate.INTEGER);
			//sqlQuery.addScalar("ticketNetworkBrokerId",Hibernate.INTEGER);
			//sqlQuery.addScalar("fanxchangeBrokerId",Hibernate.INTEGER);
			//sqlQuery.addScalar("ticketcityBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("zoneTicketBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("allowSectionRange",Hibernate.BOOLEAN);
			//sqlQuery.addScalar("zone",Hibernate.BOOLEAN);
			sqlQuery.addScalar("shippingDays",Hibernate.INTEGER);
			sqlQuery.addScalar("cjParentType",Hibernate.STRING);
			List<CrownJewelEvents> exchangeEvents =  sqlQuery.setResultTransformer(Transformers.aliasToBean(CrownJewelEvents.class)).list();
			return exchangeEvents;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return null;
	}
}
