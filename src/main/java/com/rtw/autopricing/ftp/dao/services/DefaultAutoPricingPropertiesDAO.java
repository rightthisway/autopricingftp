package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;


public interface DefaultAutoPricingPropertiesDAO extends RootDAO<Integer, DefaultAutoPricingProperties>{

	public DefaultAutoPricingProperties getDefaultAutoPricingProperties(Integer productId,Integer parentCategoryId)throws Exception;
	public List<DefaultAutoPricingProperties> getDefaultAutoPricingPropertiesByProductId(Integer productId)throws Exception;
}
