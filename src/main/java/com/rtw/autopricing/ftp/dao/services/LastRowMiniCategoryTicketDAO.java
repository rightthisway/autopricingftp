package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.LastRowMiniCategoryTicket;



public interface LastRowMiniCategoryTicketDAO extends RootDAO<Integer, LastRowMiniCategoryTicket> {

	public Collection<LastRowMiniCategoryTicket> getAllActiveLastRowMiniCategoryTickets() throws Exception;
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsByAll(Integer eventId,String section,String row,String quantity) throws Exception;
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<LastRowMiniCategoryTicket> getAllTNLastRowMiniCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllLastRowMiniCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllLastRowMiniCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnLastRowMiniCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnLastRowMiniCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllLastRowMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingproduct) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInLastRowMiniCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingproduct) throws Exception;
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCatsCategoryTicketsByPosCategoryTicektGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
}
