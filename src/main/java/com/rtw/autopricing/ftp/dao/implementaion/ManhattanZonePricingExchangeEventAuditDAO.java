package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEventAudit;

public class ManhattanZonePricingExchangeEventAuditDAO extends HibernateDAO<Integer, ManhattanZonePricingExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.ManhattanZonePricingExchangeEventAuditDAO{

	public Collection<ManhattanZonePricingExchangeEventAudit> getAllManhattanZonePricingExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM ManhattanZonePricingExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}
}
