package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.Artist;
import com.rtw.autopricing.ftp.enums.EventStatus;

public interface ArtistDAO extends RootDAO<Integer, Artist> {
    void deleteEmptyArtists();
	int getArtistCount();
	Artist getArtistByStubhubId(Integer stubhubId);
    Artist getArtistByName(String name);
    Collection<Artist> filterByName(String pattern);
    Collection<Artist> filterByName(String pattern, EventStatus eventStatus);
    Collection<Artist> searchArtists(String name);
    Collection<Artist> searchArtistsByWord(String name);
    Collection<Artist> getAllActiveArtists();
    Collection<Artist> getAllActiveArtistsByGrandChildCategoryId(Integer id);
    public Collection<Artist> getAllActiveArtistsByGrandChildCategorys(List<Integer> ids);
    public Collection<Artist> getArtistsByIds(List<Integer> ids);
    public void bulkDelete(List<Integer> ids);
    Collection<Artist> getAllArtistsBySqlQuery();
    public Collection<Artist> getAllActiveArtistsWithEvents();
    public Collection<Artist> getAllActiveArtistsWithEvents(String pattern);
}