package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.MiniExchangeEventAudit;

public interface MiniExchangeEventAuditDAO extends RootDAO<Integer, MiniExchangeEventAudit>{

	List<MiniExchangeEventAudit> getMiniExchangeEventAuditByEventId(Integer eId);
  
}
