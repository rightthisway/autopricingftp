package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.RTFLastRowExchangeEvent;

public interface RTFLastRowExchangeEventDAO extends RootDAO<Integer, RTFLastRowExchangeEvent> {

	RTFLastRowExchangeEvent getRTFLastRowExchangeEventByEventId(Integer eventId) throws Exception;
	Collection<RTFLastRowExchangeEvent> getAllRTFLastRowExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<RTFLastRowExchangeEvent> getAllRTFLastRowExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<RTFLastRowExchangeEvent> getAllRTFLastRowExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	Collection<RTFLastRowExchangeEvent> getAllRTFLastRowExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	List<RTFLastRowExchangeEvent> getRTFLastRowExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<RTFLastRowExchangeEvent> getAllRTFLastRowExchangeEventsofDeletedTmatEvents() throws Exception;
	public RTFLastRowExchangeEvent getRTFLastRowExchangeEventsByEventId(Integer eventId) throws Exception;
	public Collection<RTFLastRowExchangeEvent> getAllRTFLastRowExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<RTFLastRowExchangeEvent> getAllRTFLastRowExchangeEventsByEventIds(String eventIds)  throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<RTFLastRowExchangeEvent> getAllRTFLastRowExchangeEventsNotYetCreatedinZoneTickets(AutopricingProduct autopricingProduct)  throws Exception;
}
