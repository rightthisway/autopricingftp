package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;

public class AutopricingVenueSettingsDAO extends HibernateDAO<Integer, AutopricingVenueSettings> implements com.rtw.autopricing.ftp.dao.services.AutopricingVenueSettingsDAO{

	
	public List<AutopricingVenueSettings> getAllActiveAutopricingVenueSettings()  throws Exception {
		return find("FROM AutopricingVenueSettings where status='ACTIVE' ",new Object[]{});
				
	}
	
	public List<AutopricingVenueSettings> getAutopricingVenueSettingsByVenueId(Integer venueId)  throws Exception{
		return find("FROM AutopricingVenueSettings where venueId=? ",new Object[]{venueId});
				
	}



}
