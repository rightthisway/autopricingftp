package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEventAudit;

public interface ZoneTicketsProcessorExchangeEventAuditDAO extends RootDAO<Integer, ZoneTicketsProcessorExchangeEventAudit> {

	Collection<ZoneTicketsProcessorExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByEventId(Integer eventId);
	Collection<ZoneTicketsProcessorExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByVenueId(Integer venueId);
	Collection<ZoneTicketsProcessorExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByArtistId(Integer artistId);
	Collection<ZoneTicketsProcessorExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId);
}
