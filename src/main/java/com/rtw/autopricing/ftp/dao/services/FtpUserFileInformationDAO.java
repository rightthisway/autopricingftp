package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.FtpUserFileInformation;

public interface FtpUserFileInformationDAO extends RootDAO<Integer, FtpUserFileInformation> {
    public void deleteAll(Integer accountNo);
    public void deleteFtpUserFileByIdAndSource(String accountNo, String source);
}
