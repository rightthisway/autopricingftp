package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEventAudit;

public class TicketNetworkSSAccountExchangeEventAuditDAO extends HibernateDAO<Integer, TicketNetworkSSAccountExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.TicketNetworkSSAccountExchangeEventAuditDAO{

	public Collection<TicketNetworkSSAccountExchangeEventAudit> getAllTNSpecialExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM TicketNetworkSSAccountExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<TicketNetworkSSAccountExchangeEventAudit> getAllTNSpecialExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM TicketNetworkSSAccountExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<TicketNetworkSSAccountExchangeEventAudit> getAllTNSpecialExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM TicketNetworkSSAccountExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<TicketNetworkSSAccountExchangeEventAudit> getAllTNSpecialExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM TicketNetworkSSAccountExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}

}