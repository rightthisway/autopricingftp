package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;


public interface AutopricingProductDAO extends RootDAO<Integer, AutopricingProduct>{
	Collection<AutopricingProduct> getAllAutopricingProductExceptGivenId(Integer id);
	AutopricingProduct getAutopricingProductByName(String name);
	public List<String> getAutopricingProductInternalNotes();
	public Collection<AutopricingProduct> getAllAutopricingProductForMaangeScheduler();
	public Collection<AutopricingProduct> getAllAutopricingProductExceptGivenIdForExEventManagement(Integer id);
	public Collection<AutopricingProduct> getAllAutopricingProductForVenueSettings();
	public Collection<AutopricingProduct> getAllActiveAutopricingProducts();
	public List<String> getRTFAutopricingProductInternalNotes();
	public List<AutopricingProduct> getAllActiveProducts();
}