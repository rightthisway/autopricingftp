package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingSettings;


public class AutopricingSettingsDAO extends HibernateDAO<Integer, AutopricingSettings> implements com.rtw.autopricing.ftp.dao.services.AutopricingSettingsDAO {

	public AutopricingSettings getAutopricingSettings(Integer brokerId,Integer productId,Integer exchangeId) {
		return findSingle(" FROM AutopricingSettings where brokerId=? and productId=? and exchangeId=?",new Object[]{brokerId,productId,exchangeId});
	}

	public List<AutopricingSettings> getAutopricingSettingsByProductIdandExchangeId(Integer productId,Integer exchangeId) {
		return find(" FROM AutopricingSettings where productId=? and exchangeId=? ",new Object[]{productId,exchangeId});
	}
	
	public Integer getMinimumExcludeEventDaysByProductId(Integer productId) {
		List list = find("SELECT MIN(excludeEventDays) FROM AutopricingSettings where productId=?",new Object[]{productId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Integer)list.get(0)).intValue();		
	}
	
	public Boolean getIsBrokerEnabled(Integer brokerId) {
		
		List<Integer> list = find(" SELECT DISTINCT brokerId FROM AutopricingSettings where brokerId=? and isEnabled=?",new Object[]{brokerId,Boolean.TRUE});
		
		if (list == null || list.isEmpty()) {
			return false;
		}
		return true;
	}
	public Boolean getIsBrokerEnabledForExchange(Integer brokerId,Integer exchangeId) {
		
		List<Integer> list = find(" SELECT DISTINCT brokerId FROM AutopricingSettings where brokerId=? and exchangeId=? and isEnabled=?",new Object[]{brokerId,exchangeId,Boolean.TRUE});
		
		if (list == null || list.isEmpty()) {
			return false;
		}
		return true;
	}
}
