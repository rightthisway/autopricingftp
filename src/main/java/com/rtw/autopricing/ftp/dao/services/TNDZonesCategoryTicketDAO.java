package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.TNDZonesCategoryTicket;



public interface TNDZonesCategoryTicketDAO extends RootDAO<Integer, TNDZonesCategoryTicket> {

	public List<TNDZonesCategoryTicket> getAllActiveTndZonesCategoryTickets()  throws Exception ;
	public List<TNDZonesCategoryTicket> getAllSoldTNDZonesCategoryTicketsByEventId(Integer eventId)  throws Exception;
	public List<TNDZonesCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	public List<TNDZonesCategoryTicket> getAllTNDZonesCategoryTicketsByTnExchangeEventId(Integer tnExchangeEventId) throws Exception;
	public List<TNDZonesCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<TNDZonesCategoryTicket> getAllTNDZonesCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInTNDZonesCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<TNDZonesCategoryTicket> getAllTNTNDZonesCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllTNDZonesCategoryTicketswithinMinimumExcludeEventDays(Integer hourstoConsiderOrders,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTNDZonesCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnTNDZonesCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnTNDZonesCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<TNDZonesCategoryTicket> getAllMiniCatsCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
}

