package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEvent;
import com.rtw.autopricing.ftp.enums.EventStatus;

public class PresaleZoneTicketsExchangeEventDAO  extends HibernateDAO<Integer, PresaleZoneTicketsExchangeEvent> implements com.rtw.autopricing.ftp.dao.services.PresaleZoneTicketsExchangeEventDAO{

	public PresaleZoneTicketsExchangeEvent getPresaleZoneTicketsProcessorExchangeEventByEventId(Integer eventId)  throws Exception {
		return findSingle("FROM PresaleZoneTicketsExchangeEvent WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<PresaleZoneTicketsExchangeEvent> getAllZoneTicketsProcessorExchangeEventsByVenueId(Integer venueId)  throws Exception {
		return find("FROM le FROM PresaleZoneTicketsExchangeEvent le ,Event e WHERE e.id = le.eventId And e.venueId=?", new Object[]{venueId});
	}

	public Collection<PresaleZoneTicketsExchangeEvent> getAllZoneTicketsProcessorExchangeEventsByArtistId(Integer artistId)  throws Exception {
		return find("SELECT le FROM PresaleZoneTicketsExchangeEvent le ,Event e WHERE e.id = le.eventId And e.artistId=?", new Object[]{artistId});
	}

	public Collection<PresaleZoneTicketsExchangeEvent> getAllZoneTicketsProcessorExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId)  throws Exception {
		return find("FROM le FROM PresaleZoneTicketsExchangeEvent le ,Event e WHERE e.id = le.eventId And e.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}
	
	public List<PresaleZoneTicketsExchangeEvent> getAllZoneTicketsProcessorExchangeEventsofDeletedTmatEvents()  throws Exception {
		return find("FROM PresaleZoneTicketsExchangeEvent WHERE status='ACTIVE' and eventId in(" +
				"SELECT id FROM Event WHERE eventStatus <> ?)", new Object[]{EventStatus.ACTIVE});
	}

	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType)  throws Exception {
		
		Session session=null;
		
		try {
			String query = "";
			
			if(globalAudit.getExposure() != null) {
				query = query + ",ee.exposure='"+globalAudit.getExposure()+"'";
			}
			if(globalAudit.getShippingMethod() != null) {
				query = query + ",ee.shipping_method='"+globalAudit.getShippingMethod()+"'";
			}
			if(globalAudit.getNearTermDisplayOption() != null) {
				query = query + ",ee.near_term_display_option='"+globalAudit.getNearTermDisplayOption()+"'";
			}
			if(globalAudit.getRptFactor() != null) {
				query = query + ",ee.rpt_factor="+globalAudit.getRptFactor();
			}
			if(globalAudit.getPriceBreakup() != null) {
				query = query + ",ee.price_breakup="+globalAudit.getPriceBreakup();
			}
			if(globalAudit.getLowerMarkup() != null) {
				query = query + ",ee.lower_markup="+globalAudit.getLowerMarkup();
			}
			if(globalAudit.getUpperMarkup() != null) {
				query = query + ",ee.upper_markup="+globalAudit.getUpperMarkup();
			}
			if(globalAudit.getLowerShippingFees() != null) {
				query = query + ",ee.lower_shipping_fees="+globalAudit.getLowerShippingFees();
			}
			if(globalAudit.getUpperShippingFees() != null) {
				query = query + ",ee.upper_shipping_fees="+globalAudit.getUpperShippingFees();
			}
			if(globalAudit.getShippingDays()!= null) {
				query = query + ",ee.shipping_days="+globalAudit.getShippingDays();
			}
			
			query = query + ",last_updated_date=GETDATE()";
			
			query = "update ee set " + query.substring(1);
			
			query = query + " from presale_zone_tickets_exchange_event ee " +
			" inner join event e on e.id=ee.event_id " +
			" inner join artist t on t.id=e.artist_id" +
			" inner join grand_child_tour_category gc on gc.id=t.grand_child_category_id" +
			" inner join child_tour_category cc on cc.id=gc.child_category_id" +
			" inner join tour_category tc on tc.id=cc.tour_category_id " +
			" where tc.name in("+parentType+")";
			
			
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			session.close();
		}
	}

	public Collection<PresaleZoneTicketsExchangeEvent> getAllPresaleZoneTicketsExchangeEventsEligibleForUpdate(Long minute,Integer productId)  throws Exception {
		String sql =" select distinct le.id as id,le.event_id as eventId, le.exposure as exposure, " +
					" le.rpt_factor as rptFactor,le.price_breakup as priceBreakup,le.lower_markup as lowerMarkup, " +
					" le.upper_markup as upperMarkup,le.lower_shipping_fees as lowerShippingFees, " +
					" le.upper_shipping_fees as upperShippingFees,le.shipping_method as shippingMethod, " +
					" le.near_term_display_option as nearTermDisplayOption,le.allow_section_range as allowSectionRange, " +
					" le.shipping_days as shippingDays,le.vivid_broker_id as vividBrokerId,le.tn_broker_id as ticketNetworkBrokerId," +
					" le.scorebig_broker_id as scoreBigBrokerId,le.fanxchange_broker_id as fanxchangeBrokerId,le.ticketcity_broker_id as ticketcityBrokerId," +
					" le.seatgeek_broker_id as seatGeekBrokerId,le.zoneticket_broker_id as zoneTicketBrokerId,le.is_zone as zone " + 
					" from presale_zone_tickets_exchange_event le " + 
					" INNER join ticket_listing_crawl tc on tc.event_id = le.event_id " +
					" INNER JOIN event e on e.id = le.event_id " +
					" INNER JOIN venue_category vc on vc.id = e.venue_category_id " +
					" LEFT JOIN presale_zone_ticket_locked_ticket_event_details zlt on zlt.event_id=e.id" +
					" LEFT JOIN autopricing_error aer on aer.event_id=e.id" +
					" WHERE le.status = 'ACTIVE' " +
					" AND tc.site_id in ('ticketnetworkdirect','stubhub','ticketevolution','flashseats','vividseat') " +
					" AND le.zoneticket_broker_id is not null" +
					" AND (DATEDIFF(MINUTE,tc.end_crawl,GETDATE())<=  " + minute +
					" OR DATEDIFF(MINUTE,vc.last_updated_date,GETDATE())<=  " + minute +
					" OR (DATEDIFF(MINUTE,le.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (DATEDIFF(MINUTE,e.last_update,GETDATE())<= " + minute + ")" +
					" OR (zlt.created_date is not null AND DATEDIFF(MINUTE,zlt.created_date,GETDATE())<= " + minute + ")" +
					" OR (aer.product_id="+productId+" and aer.time_stamp is not null and DATEDIFF(MINUTE,aer.time_stamp,GETDATE())<= " + minute + ")) " ;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			sqlQuery.addScalar("id",Hibernate.INTEGER);
			sqlQuery.addScalar("exposure",Hibernate.STRING);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("rptFactor",Hibernate.DOUBLE);
			sqlQuery.addScalar("shippingMethod",Hibernate.INTEGER);
			sqlQuery.addScalar("nearTermDisplayOption",Hibernate.INTEGER);
			
			sqlQuery.addScalar("priceBreakup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("vividBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("scoreBigBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("ticketNetworkBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("fanxchangeBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("ticketcityBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("seatGeekBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("zoneTicketBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("allowSectionRange",Hibernate.BOOLEAN);
			sqlQuery.addScalar("zone",Hibernate.BOOLEAN);
			sqlQuery.addScalar("shippingDays",Hibernate.INTEGER);
			List<PresaleZoneTicketsExchangeEvent> exchangeEvents =  sqlQuery.setResultTransformer(Transformers.aliasToBean(PresaleZoneTicketsExchangeEvent.class)).list();
			return exchangeEvents;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return null;
	}

	
	public List<PresaleZoneTicketsExchangeEvent> getPresaleZoneTicketsExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId)  throws Exception {
		Session session =getSession();
		List<PresaleZoneTicketsExchangeEvent> list=null;
		String  sql=" select * from presale_zone_tickets_exchange_event ee inner join event e on ee.event_id=e.id " +
				" where status='ACTIVE' and e.venue_id="+ venueId +" and (ee.scorebig_broker_id=" + brokerId +" or ee.vivid_broker_id=" + brokerId +" or ee.tn_broker_id=" + brokerId+" or ee.fanxchange_broker_id="+brokerId + " or ee.zoneticket_broker_id="+brokerId+" or ee.ticketcity_broker_id="+brokerId + "  or ee.seatgeek_broker_id="+brokerId + " ) ";
		try{
			list=  session.createSQLQuery(sql).addEntity(PresaleZoneTicketsExchangeEvent.class).list();
		   }catch(Exception e){
			   e.printStackTrace();
			   throw e;
		   }finally{
			   session.close();
		   }
		return list;
	}
	
	public PresaleZoneTicketsExchangeEvent getZoneTicketsProcessorExchangeEventsByEventId(Integer eventId)  throws Exception {
		List<PresaleZoneTicketsExchangeEvent> list = find("FROM PresaleZoneTicketsExchangeEvent WHERE eventId =?", new Object[]{eventId});
		if(list==null || list.isEmpty()){
			return null;
		}
		return list.get(0);
	}

}