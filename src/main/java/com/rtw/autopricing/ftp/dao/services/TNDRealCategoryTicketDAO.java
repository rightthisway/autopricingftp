package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.TNDRealCategoryTicket;



public interface TNDRealCategoryTicketDAO extends RootDAO<Integer, TNDRealCategoryTicket> {

	public List<TNDRealCategoryTicket> getAllActiveTNDRealCategoryTickets()  throws Exception ;
	public List<TNDRealCategoryTicket> getAllSoldTNDRealCategoryTicketsByEventId(Integer eventId)  throws Exception;
	public List<TNDRealCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	public Integer deleteAllTNDRealCategoryTicketsofUnbroadcastedEvents()  throws Exception;
	public List<TNDRealCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<TNDRealCategoryTicket> getAllTNDRealCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInTNDRealCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<TNDRealCategoryTicket> getAllTNTNDRealCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllTNDRealCategoryTicketswithinMinimumExcludeEventDays(Integer hourstoConsiderOrders,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTNDRealCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnTNDRealCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnTNDRealCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<TNDRealCategoryTicket> getAllMiniCatsCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
}

