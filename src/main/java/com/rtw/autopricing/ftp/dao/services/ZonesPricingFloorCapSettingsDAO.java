package com.rtw.autopricing.ftp.dao.services;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.enums.TicketStatus;


public interface ZonesPricingFloorCapSettingsDAO extends RootDAO<Integer, Ticket> {
	
}