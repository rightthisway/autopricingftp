package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.ZonesPricingCategoryTicket;



public interface ZonesPricingCategoryTicketDAO extends RootDAO<Integer, ZonesPricingCategoryTicket> {

	public Collection<ZonesPricingCategoryTicket> getAllActiveZonePricingCategoryTickets() throws Exception;
	public List<ZonesPricingCategoryTicket> getAllZonePricingCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	
	public List<ZonesPricingCategoryTicket> getAllZonePricingCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInZonesPricingCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<ZonesPricingCategoryTicket> getAllTNZonesPricingCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllZonesPricingCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllZonesPricingCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnZonesPricingCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnZonesPricingCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllZonePricingTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<ZonesPricingCategoryTicket> getAllZonePricingCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
}

