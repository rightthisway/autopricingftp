package com.rtw.autopricing.ftp.dao.implementaion;

import com.rtw.autopricing.ftp.data.UserBrokerDetails;


public class UserBrokerDetailsDAO extends HibernateDAO<Integer, UserBrokerDetails> implements com.rtw.autopricing.ftp.dao.services.UserBrokerDetailsDAO {

	public void deleteBrokerDetailsByUserId(Integer userId) {
		// TODO Auto-generated method stub
		bulkUpdate("DELETE FROM UserBrokerDetails WHERE user_id=?", new Object[]{userId});
	}
}
