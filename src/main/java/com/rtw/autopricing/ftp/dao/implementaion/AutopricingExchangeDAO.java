package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;


public class AutopricingExchangeDAO extends HibernateDAO<Integer, AutopricingExchange> implements com.rtw.autopricing.ftp.dao.services.AutopricingExchangeDAO {

	public AutopricingExchange getAutopricingExchangeByName(String name)  {
		List<AutopricingExchange> list = find("FROM AutopricingExchange WHERE name = ? " , new Object[]{name});
		if(list==null || list.isEmpty()){
			return null;
		}
		return list.get(0);
	}

	public List<AutopricingExchange> getAllActiveExchanges()  {
		return find("FROM AutopricingExchange WHERE status='ACTIVE' ");
		
	}
}
