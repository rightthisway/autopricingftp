package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEvent;

public interface TicketNetworkSSAccountExchangeEventDAO extends RootDAO<Integer, TicketNetworkSSAccountExchangeEvent> {

	TicketNetworkSSAccountExchangeEvent getTNSpecialExchangeEventByEventId(Integer eventId) throws Exception;
	Collection<TicketNetworkSSAccountExchangeEvent> getAllTNSpecialExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<TicketNetworkSSAccountExchangeEvent> getAllTNSpecialExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<TicketNetworkSSAccountExchangeEvent> getAllTNSpecialExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	Collection<TicketNetworkSSAccountExchangeEvent> getAllTicketNetworkSSAccountExchangeEventsEligibleForUpdate(Long minute) throws Exception;
	List<TicketNetworkSSAccountExchangeEvent> getTNSpecialExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<TicketNetworkSSAccountExchangeEvent> getAllTNSpecialExchangeEventsofDeletedTmatEvents() throws Exception;
}
