package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEventAudit;

public class PresaleZoneTicketsExchangeEventAuditDAO extends HibernateDAO<Integer, PresaleZoneTicketsExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.PresaleZoneTicketsExchangeEventAuditDAO{

	public Collection<PresaleZoneTicketsExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM PresaleZoneTicketsExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<PresaleZoneTicketsExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM PresaleZoneTicketsExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<PresaleZoneTicketsExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM PresaleZoneTicketsExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<PresaleZoneTicketsExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM PresaleZoneTicketsExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}

}