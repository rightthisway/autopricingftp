package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeVenues;

public class ExcludeEventZonesDAO extends HibernateDAO<Integer, ExcludeEventZones> implements com.rtw.autopricing.ftp.dao.services.ExcludeEventZonesDAO{

	
	public List<ExcludeEventZones> getExcludeEventZonesByProductId(Integer productId)  throws Exception{
		return find("FROM ExcludeEventZones where productId=? and status='ACTIVE'",new Object[]{productId});
				
	}
	public List<ExcludeEventZones> getExcludeEventZonesByBrokerIdandProductIds(Integer brokerId,String productIdStr)  throws Exception{
		return find("FROM ExcludeEventZones where brokerId=? and status='ACTIVE' and productId in("+productIdStr+") ",new Object[]{brokerId});
				
	}
	public List<ExcludeEventZones> getExcludeEventZonesByProductIdandBrokerId(Integer productId,Integer brokerId)  throws Exception{
		return find("FROM ExcludeEventZones where productId=? and brokerId=? and status='ACTIVE'",new Object[]{productId,brokerId});
				
	}
	public List<ExcludeEventZones> getAllExcludeEventZonesByEventId(Integer eventId)  throws Exception{
		return find("FROM ExcludeEventZones where eventId=? ",new Object[]{eventId});
				
	}
	public List<ExcludeEventZones> getExcludeEventZonesByProductIdAndBrokerIdAndEventId(
			Integer productId, Integer brokerId, Integer eventId)  throws Exception{
		return find("FROM ExcludeEventZones where eventId=? AND productId=? AND brokerId=? and status='ACTIVE'",new Object[]{eventId,productId,brokerId});
				
	}



}
