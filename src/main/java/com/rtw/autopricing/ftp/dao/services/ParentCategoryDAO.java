package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.ParentCategory;


public interface ParentCategoryDAO extends RootDAO<Integer, ParentCategory>{
	public ParentCategory getTourCategoryByName(String name);
	public List<ParentCategory> getAllTourCategoryOrderByName();
}
