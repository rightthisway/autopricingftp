package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;

public interface LastRowMiniExchangeEventDAO extends RootDAO<Integer, LastRowMiniExchangeEvent> {

	LastRowMiniExchangeEvent getLastRowMiniExchangeEventByEventId(Integer eventId) throws Exception;
	Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	public Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	List<LastRowMiniExchangeEvent> getLastRowMiniExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByEventIds(String eventIds)  throws Exception ;
}
