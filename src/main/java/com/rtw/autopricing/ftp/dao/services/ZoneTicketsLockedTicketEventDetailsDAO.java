package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.ZoneTicketsLockedTicketEventDetails;


public interface ZoneTicketsLockedTicketEventDetailsDAO extends RootDAO<Integer, ZoneTicketsLockedTicketEventDetails>{
	
}
