package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.VipAutoCategoryTicket;


public interface VipAutoCategoryTicketDAO extends RootDAO<Integer, VipAutoCategoryTicket> {

	public Collection<VipAutoCategoryTicket> getAllActiveVipAutoCategoryTickets();
	public List<VipAutoCategoryTicket> getAllVipAutoCategoryTicketsByAll(Integer eventId, String section, String row, String quantity);
	public List<VipAutoCategoryTicket> getAllVipAutoCategoryTicketsById(Integer Id);
}

