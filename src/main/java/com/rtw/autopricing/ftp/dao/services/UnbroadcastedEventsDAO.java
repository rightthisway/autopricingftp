package com.rtw.autopricing.ftp.dao.services;

import java.util.Date;
import java.util.List;

import com.rtw.autopricing.ftp.data.UnbroadcastedEvents;

public interface UnbroadcastedEventsDAO extends RootDAO<Integer, UnbroadcastedEvents>{

	public UnbroadcastedEvents getUnbrodacastedEventsByDate(String date) throws Exception;
	public List<UnbroadcastedEvents> getUnbrodacastedEventsByDateRange(String fromDate,String toDate) throws Exception;
}
