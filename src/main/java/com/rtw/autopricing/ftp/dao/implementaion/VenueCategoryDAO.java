package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.VenueCategory;


public class VenueCategoryDAO extends HibernateDAO<Integer, VenueCategory> implements
		com.rtw.autopricing.ftp.dao.services.VenueCategoryDAO {

	public VenueCategory getVenueCategoryByVenueAndCategoryGroup(Integer venueId, String categroyGroup) {
		return findSingle("FROM VenueCategory WHERE venue.id=? AND categoryGroup=?",new Object[]{venueId,categroyGroup});
	}
	
	public List<String> getCategoryGroupByVenueCategoryId(Integer venuCatergoryGroupId)
	{
		return find("SELECT categoryGroup FROM VenueCategory WHERE id=?",new Object[]{venuCatergoryGroupId});
	}

	public List<VenueCategory> getVenueCategoriesByVenueId(Integer venueId) {
			return find("FROM VenueCategory WHERE venue.id=? order by categoryGroup",new Object[]{venueId});
	}

	public List<String> getCategoryGroupByVenueId(Integer venueId) {
		return find("SELECT categoryGroup FROM VenueCategory WHERE venue.id = ?",new Object[]{venueId});
	}
	
	@Override
	public void delete(VenueCategory entity) {
		/*List<Event> events = DAORegistry.getEventDAO().getAllEventsByVenueCategoryId(entity.getId());
		if(events!=null && !events.isEmpty()){
			for(Event event:events){
				event.setVenueCategoryId(null);
				event.setVenueCategory(null);
			}
			DAORegistry.getEventDAO().saveOrUpdateAll(events);
		}
		DAORegistry.getCategoryDAO().deleteByVenueCategoryId(entity.getId());
		super.delete(entity);*/
	}
	
	public void deleteVenueCategory(List venueCategoryId) {
		/*
		Session session = getSession();
		
		try{
			Query query = session.createQuery("UPDATE Event SET venueCategoryId = null WHERE venueCategoryId IN (:venueCategoryId)").setParameterList("venueCategoryId", venueCategoryId);
			query.executeUpdate();
			query = session.createQuery("DELETE FROM CategoryMapping WHERE categoryId IN (SELECT id FROM Category WHERE venueCategoryId IN (:venueCategoryId))").setParameterList("venueCategoryId", venueCategoryId);
			query.executeUpdate();
			query = session.createQuery("DELETE FROM Category WHERE venueCategoryId IN (:venueCategoryId)").setParameterList("venueCategoryId", venueCategoryId);
			query.executeUpdate();
			query = session.createQuery("DELETE FROM VenueCategory WHERE id IN (:venueCategoryId)").setParameterList("venueCategoryId", venueCategoryId);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		*/
	}
}
