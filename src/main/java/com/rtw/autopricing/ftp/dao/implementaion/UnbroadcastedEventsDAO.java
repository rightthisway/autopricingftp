package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.UnbroadcastedEvents;

public class UnbroadcastedEventsDAO  extends HibernateDAO<Integer, UnbroadcastedEvents> implements com.rtw.autopricing.ftp.dao.services.UnbroadcastedEventsDAO{
	
	public UnbroadcastedEvents getUnbrodacastedEventsByDate(String date) throws Exception{
		return findSingle("From UnbroadcastedEvents where unbroadcastedDate = ?", new Object[]{date});
	}
	public List<UnbroadcastedEvents> getUnbrodacastedEventsByDateRange(String fromDate,String toDate) throws Exception{
		return find("From UnbroadcastedEvents where unbroadcastedDate >= ? and unbroadcastedDate <=?", new Object[]{fromDate,toDate});
	}
}
