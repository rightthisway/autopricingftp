package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;


import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEventAudit;

public class TixCityZonePricingExchangeEventAuditDAO extends HibernateDAO<Integer, TixCityZonePricingExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.TixCityZonePricingExchangeEventAuditDAO{
    
	public Collection<TixCityZonePricingExchangeEventAudit> getAllTixCityZonePricingExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM TixCityZonePricingExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<TixCityZonePricingExchangeEventAudit> getAllTixCityZonePricingExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM TixCityZonePricingExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<TixCityZonePricingExchangeEventAudit> getAllTixCityZonePricingExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM TixCityZonePricingExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<TixCityZonePricingExchangeEventAudit> getAllTixCityZonePricingExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM TixCityZonePricingExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}
}
