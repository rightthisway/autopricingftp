package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.SGLastFiveRowCategoryTicket;



public interface SGLastFiveRowCategoryTicketDAO extends RootDAO<Integer, SGLastFiveRowCategoryTicket> {

	public Collection<SGLastFiveRowCategoryTicket> getAllActiveSGLastFiveRowCategoryTickets() throws Exception;
	public List<SGLastFiveRowCategoryTicket> getAllSGLastFiveRowCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	
	public List<SGLastFiveRowCategoryTicket> getAllSGLastFiveRowCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<SGLastFiveRowCategoryTicket> getAllSGLastFiveRowCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInSGLastFiveRowCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<SGLastFiveRowCategoryTicket> getAllTNSGLastFiveRowCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllSGLastFiveRowCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllSGLastFiveRowCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnSGLastFiveRowCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnSGLastFiveRowCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllSGLastFiveRowsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<SGLastFiveRowCategoryTicket> getAllSGLastFiveRowsCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
	public Integer updatePriceById(Integer id,Double price)  throws Exception;
}

