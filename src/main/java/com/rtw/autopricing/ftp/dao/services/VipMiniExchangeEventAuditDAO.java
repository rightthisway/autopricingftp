package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.VipMiniExchangeEventAudit;

public interface VipMiniExchangeEventAuditDAO extends RootDAO<Integer, VipMiniExchangeEventAudit> {

	Collection<VipMiniExchangeEventAudit> getAllVipMiniExchangeEventAuditsByEventId(Integer eventId);
	Collection<VipMiniExchangeEventAudit> getAllVipMiniExchangeEventAuditsByVenueId(Integer venueId);
	Collection<VipMiniExchangeEventAudit> getAllVipMiniExchangeEventAuditsByArtistId(Integer artistId);
	Collection<VipMiniExchangeEventAudit> getAllVipMiniExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId);
}
