package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeEventZonesAudit;

public class ExcludeEventZonesAuditDAO extends HibernateDAO<Integer, ExcludeEventZonesAudit> implements com.rtw.autopricing.ftp.dao.services.ExcludeEventZonesAuditDAO{

	
	public List<ExcludeEventZonesAudit> getExcludeEventZonesAuditByProductIdAndBrokerIdAndEventId(
			Integer productId, Integer brokerId, Integer eventId,String zone)  throws Exception{
		return find("FROM ExcludeEventZonesAudit where eventId=? AND productId=? AND brokerId=? AND zone=?",new Object[]{eventId,productId,brokerId,zone});
				
	}

}
