package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.FtpUserAccount;

public class FtpUserAccountDAO extends HibernateDAO<Integer, FtpUserAccount> implements com.rtw.autopricing.ftp.dao.services.FtpUserAccountDAO {
	
	public FtpUserAccount getuserAccountByUserName(String userName,String hostName){
		return findSingle("FROM FtpUserAccount WHERE hostName = ? AND userName = ?", new Object[]{hostName,userName});
	}
	public List<FtpUserAccount> getuserAccountListByUserNameAndHost(String userName,String hostName){
		return find("FROM FtpUserAccount WHERE hostName = ? AND userName = ? AND status = 'ACTIVE'", new Object[]{hostName,userName});
	}
	public List<FtpUserAccount> getAllActiveuserAccount(){
		return find("FROM FtpUserAccount WHERE status = 'ACTIVE'");
	}
}
