package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingVenueSettingsAudit;



public interface AutopricingVenueSettingsAuditDAO extends RootDAO<Integer,  AutopricingVenueSettingsAudit>{

	public List<AutopricingVenueSettingsAudit> getAutopricingVenueSettingsAuditList(Integer productId, Integer venueId) throws Exception;
}
