package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEventAudit;

public interface LastRowMiniExchangeEventAuditDAO extends RootDAO<Integer, LastRowMiniExchangeEventAudit> {

	Collection<LastRowMiniExchangeEventAudit> getAllLastRowMiniExchangeEventAuditsByEventId(Integer eventId);
	Collection<LastRowMiniExchangeEventAudit> getAllLastRowMiniExchangeEventAuditsByVenueId(Integer venueId);
	Collection<LastRowMiniExchangeEventAudit> getAllLastRowMiniExchangeEventAuditsByArtistId(Integer artistId);
	Collection<LastRowMiniExchangeEventAudit> getAllLastRowMiniExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId);
}
