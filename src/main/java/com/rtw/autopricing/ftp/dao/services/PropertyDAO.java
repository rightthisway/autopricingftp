package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.Property;

/**
 * interface having db related methods for zone property
 * @author hamin
 *
 */ 
public interface PropertyDAO extends RootDAO<String, Property> {
	/**
	 * method to get all zone properies
	 * @return list of properies
	 */
	public List<Property> getAllProperty();
	/**
	 * method to  zone property by name
	 * @return Property
	 */
	public Property getPropertyByName(String name);
	public Integer getEventAPIRemovalTime();
	public Integer getEventAPIRemovalTimeForNY();

}
