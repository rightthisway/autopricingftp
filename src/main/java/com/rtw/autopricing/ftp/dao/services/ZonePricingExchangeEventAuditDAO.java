package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.ZonePricingExchangeEventAudit;

public interface ZonePricingExchangeEventAuditDAO extends RootDAO<Integer, ZonePricingExchangeEventAudit>{
	Collection<ZonePricingExchangeEventAudit> getAllZonePricingExchangeEventAuditsByEventId(Integer eventId);
	Collection<ZonePricingExchangeEventAudit> getAllZonePricingExchangeEventAuditsByVenueId(Integer venueId);
	Collection<ZonePricingExchangeEventAudit> getAllZonePricingExchangeEventAuditsByArtistId(Integer artistId);
	Collection<ZonePricingExchangeEventAudit> getAllZonePricingExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId);
}
