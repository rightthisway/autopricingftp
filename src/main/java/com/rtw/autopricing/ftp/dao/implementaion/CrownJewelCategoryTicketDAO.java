package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.CrownJewelCategoryTicket;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;

public class CrownJewelCategoryTicketDAO extends HibernateDAO<Integer, CrownJewelCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.CrownJewelCategoryTicketDAO{

	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTicketsByEventId(Integer eventId) throws Exception{
		return find("FROM CrownJewelCategoryTicket where status='ACTIVE' and eventId=?", new Object[] {eventId});
	}
	
	public void updateCrownJewelCategoryTicketstoDeletedByEventId(Integer eventId) throws Exception{
		 bulkUpdate("UPDATE CrownJewelCategoryTicket set status='DELETED',lastUpdated=? WHERE status='ACTIVE' and eventID=?", new Object[]{new Date(),eventId});
		}
	
	public List<CrownJewelCategoryTicket> getAllCrownJewelCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM CrownJewelCategoryTicket where eventId=? AND status=?" ,new Object[]{eventId,"ACTIVE"});
	}
	
	public Integer deleteAllCrownJewelCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM crownjewel_category_ticket tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllCrownJewelCategoryTicketsnotExistinCrownJewelEventforZoneTicketExchange()  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event not Exist in Exchange Event for ZoneTickets' " +
				" FROM crownjewel_category_ticket tg WITH(NOLOCK)  " +
			    " WHERE tg.status='ACTIVE' AND event_id NOT IN (" +
			    " SELECT event_id FROM crownjewel_events le WITH(NOLOCK)" +
			    " inner join event e on e.id=le.event_id " +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND e.venue_category_id is not null AND e.venue_id is not null " +
			    " ) ";//AND le.zoneticket_broker_id > 0
				//" AND (le.tn_broker_id > 0 OR le.vivid_broker_id > 0 OR le.scorebig_broker_id > 0)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllCrownJewelCategoryTicketGroupIdsNotExistInZoneTicketsCJCategoryTicket()  throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("zone.tickets.linked.server");
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Ticket not Exist in ZoneTickets'  " +
	    		" FROM crownjewel_category_ticket tg WITH(NOLOCK)  " +
	    		" WHERE tg.zone_tickets_ticket_group_id is not null AND tg.status='ACTIVE' " +
	    		" and tg.zone_tickets_ticket_group_id not in(select distinct id from "+serverName+".crownjewel_category_ticket WITH(NOLOCK) " +
	    		" where status='ACTIVE')";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public List<Integer> getAllCrownJewelTicketsTicketGroupIdsNotExistInTmat()  throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("zone.tickets.linked.server");
		
		String sql = " SELECT ztg.id as groupId " +
					" FROM " + serverName +".crownjewel_category_ticket ztg WITH(NOLOCK) " + 
					 " WHERE ztg.status='ACTIVE' AND ztg.id NOT IN (select distinct zone_tickets_ticket_group_id from crownjewel_category_ticket WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND zone_tickets_ticket_group_id is not null) and datediff(minute,ztg.last_updated,GETDATE())>3" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 "";
		
		Session session=null;
		
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	
	}
}
