package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;



public interface ZoneTicketsProcessorCategoryTicketDAO extends RootDAO<Integer, ZoneTicketsProcessorCategoryTicket> {

	public Collection<ZoneTicketsProcessorCategoryTicket> getAllActiveTgCatsCategoryTickets() throws Exception;
	public List<ZoneTicketsProcessorCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	
	public List<ZoneTicketsProcessorCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<ZoneTicketsProcessorCategoryTicket> getAllZoneTicketsProcessorCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<ZoneTicketsProcessorCategoryTicket> getAllTNZoneTicketsProcessorCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllZoneTicketsProcessorCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllZoneTicketsProcessorCategoryTicketsnotExistinExchangeEventforZoneTicketExchange() throws Exception;
	public List<ZoneTicketsProcessorCategoryTicket> getAllZoneTicektsProcessorCategoryTicketsByIds(List<Integer> zoneTicektGroupIds) throws Exception;
	public List<Integer> getAllZoneTicketsTicketGroupIdsNotExistInTmat(AutopricingProduct autopricingProduct) throws Exception;
	public Integer deleteAllZoneTicketsProcessorCategoryTicketGroupIdsNotExistInZoneTicketsTicektGroup(AutopricingProduct autopricingProduct) throws Exception;
}

