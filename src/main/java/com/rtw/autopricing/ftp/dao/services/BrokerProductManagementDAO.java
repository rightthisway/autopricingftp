package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.BrokerProductManagement;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;

public interface BrokerProductManagementDAO extends RootDAO<Integer, BrokerProductManagement>  {

}
