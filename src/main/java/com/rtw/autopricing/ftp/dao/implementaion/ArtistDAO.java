package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Artist;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.ftp.enums.ArtistStatus;
import com.rtw.autopricing.ftp.enums.EventStatus;
//import com.rtw.autopricing.ftp.utils.TextUtil;

public class ArtistDAO extends HibernateDAO<Integer, Artist> implements com.rtw.autopricing.ftp.dao.services.ArtistDAO {
	
    public Collection<Artist> getAll() {
    	return find("FROM Artist ORDER BY name ASC");
    }

    public Collection<Artist> filterByName(String pattern, EventStatus eventStatus) {
//    	return new HashSet<Artist>(find("SELECT a FROM Artist a, Event e WHERE a.artistStatus=? AND a.name LIKE ? AND e.artistId=a.id AND e.eventStatus=? ORDER BY a.name ASC", new Object[]{ArtistStatus.ACTIVE, "%" + TextUtil.removeExtraWhitespaces(pattern) + "%", eventStatus}));
    	return null;
    }

    public Collection<Artist> filterByName(String pattern) {
    	
    	String hql="FROM Artist WHERE artistStatus=? AND name LIKE ? ORDER BY name ASC";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(ArtistStatus.ACTIVE);
		parameters.add("%" + pattern + "%");	
		return find(hql, parameters.toArray());
		
    }

	public Artist getArtistByStubhubId(Integer stubhubId) {
    	Collection<Artist> artists = find("FROM Artist WHERE artistStatus=? AND stubhubId=?", new Object[]{ArtistStatus.ACTIVE, stubhubId});
    	if (artists.size() == 0) {
    		return null;
    	}
    	
    	return artists.iterator().next();
	}

	public void deleteEmptyArtist(Artist artist) {
	}
	
	public Collection<Artist> getAllActiveArtistsByGrandChildCategoryId(Integer id) {
		return find("FROM Artist WHERE grandChildTourCategoryId = ? and artistStatus = ? ", new Object[]{id,ArtistStatus.ACTIVE});
	}
	
	public Collection<Artist> getAllActiveArtistsByGrandChildCategorys(List<Integer> ids) {
		Query query = getSession().createQuery("FROM Artist where artistStatus = :status  and grandChildTourCategoryId in (:Ids)  ORDER By name").setParameter("status", ArtistStatus.ACTIVE).setParameterList("Ids", ids);
	   
	  @SuppressWarnings("unchecked")
	  List<Artist> list= query.list();
	  return list;
}
	public void deleteById(Integer id) {
    }	

	public void bulkDelete(List<Integer> ids) {
		Query query = getSession().createQuery("UPDATE Artist SET artistStatus = :status  where id in (:Ids)").setParameter("status", ArtistStatus.DELETED).setParameterList("Ids", ids);
		query.executeUpdate();
		
	}
	
	@Override
    public void delete(Artist artist) {
    }
	
    public void deleteEmptyArtists() {
    }
	
	public int getArtistCount() {
		List list = find("SELECT count(id) FROM Artist WHERE artistStatus=?", new Object[]{ArtistStatus.ACTIVE});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}

	public Collection<Artist> searchArtists(String name) {
		
		String hql="FROM Artist WHERE name like ?";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
		
	}
	
    public Artist getArtistByName(String name) {
    	// use like so that it is not case sensitive
    	name = name.trim();
    	List<Artist> artists = find("FROM Artist WHERE name LIKE ? AND artistStatus=?", new Object[]{name, ArtistStatus.ACTIVE});
    	if (artists == null || artists.size() == 0) { 
    		return null;
    	}
    	return artists.get(0);
    }

    public Collection<Artist> getAllActiveArtists() {
    	Collection<Artist> list = find("FROM Artist WHERE artistStatus=? ORDER BY name", new Object[]{ArtistStatus.ACTIVE});
		return  list;
	}
    
    public Collection<Artist> getAllActiveArtistsWithEvents() {
    	Collection<Artist> list = find("FROM Artist WHERE artistStatus=? AND id IN (SELECT DISTINCT artistId FROM Event WHERE artistId is not null AND eventStatus='ACTIVE') ORDER BY name", new Object[]{ArtistStatus.ACTIVE});
		return  list;
	}

	public Collection<Artist> searchArtistsByWord(String name) {
		
		String hql="FROM Artist WHERE (name like ? OR name like ?) AND artistStatus = ?";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("% " + name + "%");
		parameters.add("%" + name + " %");
		parameters.add(ArtistStatus.ACTIVE);
		return find(hql, parameters.toArray());
		
	}

	public Collection<Artist> getArtistsByIds(List<Integer> ids) {
		Query query = getSession().createQuery("FROM Artist where id in (:Ids)  ORDER By name").setParameterList("Ids", ids);
		   
		  @SuppressWarnings("unchecked")
		  List<Artist> list= query.list();
		  return list;
	}
	
	@Override
	public Integer save(Artist artist) {
		return 0;
	}
	
	@Override
	public void saveAll(Collection<Artist> artists) {
		
	}
	
	@Override
	public void update(Artist artist) {
	}
	
	@Override
	public boolean updateAll(Collection<Artist> artists) {
		return true;
	}
	
	@Override
	public void saveOrUpdate(Artist artist) {
	}
	
	@Override
	public void saveOrUpdateAll(Collection<Artist> artists) {
		for(Artist artist:artists){
			if(artist.getArtistStatus().equals(ArtistStatus.DELETED)){
				updateArtistDependant(artist,false);
			}else{
				updateArtistDependant(artist,true);
			}
		}
		super.saveOrUpdateAll(artists);
	}
	
	
	@Override
	public void deleteAll(Collection<Artist> artists) {
		for(Artist artist:artists){
			updateArtistDependant(artist, false);
		}
		super.deleteAll(artists);
		
	}
	
	public void updateArtistDependant(Artist artist,boolean isAdd){

	}
	
	
public Collection<Artist> getAllArtistsBySqlQuery() {
		
		Session session = getSession();
		SQLQuery query = session.createSQLQuery("select id,name from artist order by name asc");
		
		List<Object[]> list = query.list();
		List<Artist> artists= new ArrayList<Artist>();
		Artist artist =null;
		for(Object[] obj:list){
			artist = new Artist();
			artist.setId((Integer)obj[0]);
			artist.setName((String)obj[1]);
			artists.add(artist);
		}
		releaseSession(session);
		return artists;	
	}

public Collection<Artist> getAllActiveArtistsWithEvents(String pattern) {	
	String hql="FROM Artist WHERE artistStatus=? AND name LIKE ? AND id IN (SELECT DISTINCT artistId FROM Event WHERE artistId is not null AND eventStatus='ACTIVE') ORDER BY name";
	List<Object> parameters = new ArrayList<Object>();
	parameters.add(ArtistStatus.ACTIVE);
	parameters.add("%" + pattern + "%");	
	return find(hql, parameters.toArray());
	
}



}


