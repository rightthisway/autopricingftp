package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.Role;


public interface RoleDAO extends RootDAO<Integer, Role> {
	public Role getRoleByName(String name);
}
