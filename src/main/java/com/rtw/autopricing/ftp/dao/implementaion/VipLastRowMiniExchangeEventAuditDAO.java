package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEventAudit;

public class VipLastRowMiniExchangeEventAuditDAO extends HibernateDAO<Integer, VipLastRowMiniExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.VipLastRowMiniExchangeEventAuditDAO{

	
	public Collection<VipLastRowMiniExchangeEventAudit> getAllVipLastRowMiniExchangeEventAuditsByEventId(Integer eventId){
		return find("FROM VipLastRowMiniExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}
}
