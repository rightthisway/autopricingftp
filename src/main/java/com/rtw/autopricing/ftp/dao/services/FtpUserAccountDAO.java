package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.FtpUserAccount;

public interface FtpUserAccountDAO extends RootDAO<Integer, FtpUserAccount> {
	public FtpUserAccount getuserAccountByUserName(String userName,String hostName);
	public List<FtpUserAccount> getAllActiveuserAccount();
	public List<FtpUserAccount> getuserAccountListByUserNameAndHost(
			String userName, String host);
	
}
