package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.List;

import com.rtw.autopricing.ftp.data.DefaultPurchasePrice;


public class DefaultPurchasePriceDAO extends HibernateDAO<Integer, DefaultPurchasePrice> implements com.rtw.autopricing.ftp.dao.services.DefaultPurchasePriceDAO {

	public DefaultPurchasePrice getAllDefaultTourPriceByTourId(Integer id) throws Exception{
		return findSingle("FROM DefaultPurchasePrice WHERE id = ? ", new Object[]{id});
	}

	public List<DefaultPurchasePrice> getAllDefaultTourPrice() throws Exception{
		return find("FROM DefaultPurchasePrice");
	}

	public DefaultPurchasePrice getDefaultTourPriceByExchangeAndTicketType(String exchange, String ticketType) throws Exception{
		return findSingle("FROM DefaultPurchasePrice WHERE exchange= ? AND ticketType=?", new Object[]{exchange,ticketType});
	}


}
