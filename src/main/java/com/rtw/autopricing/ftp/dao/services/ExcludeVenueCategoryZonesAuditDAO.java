package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.ExcludeEventZonesAudit;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZonesAudit;

public interface ExcludeVenueCategoryZonesAuditDAO extends RootDAO<Integer,ExcludeVenueCategoryZonesAudit> {
    
	public List<ExcludeVenueCategoryZonesAudit> getExcludeVenueCategoryZonesAuditByProductIdAndBrokerIdAndVenueCategoryIdAndZone(
			Integer productId, Integer brokerId, Integer venueCategoryId,String zone)  throws Exception;
}
