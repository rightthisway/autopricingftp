package com.rtw.autopricing.ftp.dao.services;



import java.util.List;

import com.rtw.autopricing.ftp.data.SeatGeekUploadCats;



public interface SeatGeekUploadCatsDAO extends RootDAO<Integer, SeatGeekUploadCats> {
	
	public List<SeatGeekUploadCats> getAllActiveSeatGeekUploadCats() throws Exception;
	public List<SeatGeekUploadCats> getAllActiveSeatGeekListingsByEventIdAndInternalNotes(Integer eventId,String internalNotes) throws Exception;
	public List<SeatGeekUploadCats> getAllActiveSeatGeekListingsNotInsgLastFiveRowCategoryTicket() throws Exception;
	public List<SeatGeekUploadCats> getAllActiveSeatGeekListingsNotInMinicatCategoryTicket() throws Exception;
	public List<SeatGeekUploadCats> getAllActiveSeatGeekListingsByEventId(Integer eventId) throws Exception;
}

