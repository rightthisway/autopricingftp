package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.VenueCategory;


public interface VenueCategoryDAO extends RootDAO<Integer, VenueCategory> {
	public VenueCategory getVenueCategoryByVenueAndCategoryGroup(Integer venueId,String categroyGroup);
	public List<VenueCategory> getVenueCategoriesByVenueId(Integer venueId);
	public List<String> getCategoryGroupByVenueId(Integer venueId);
	public void deleteVenueCategory(List venueCategoryId);
	public List<String> getCategoryGroupByVenueCategoryId(Integer venuCatergoryGroupId );
}
