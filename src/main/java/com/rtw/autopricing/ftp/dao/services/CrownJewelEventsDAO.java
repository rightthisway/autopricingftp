package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.CrownJewelEvents;


public interface CrownJewelEventsDAO extends RootDAO<Integer, CrownJewelEvents> {
	
	public CrownJewelEvents getCrownJewelEventByEventId(Integer eventId) throws Exception;
	public List<CrownJewelEvents> getCrownJewelEventByParentType(String parentType) throws Exception;
	public Collection<CrownJewelEvents> getAllCrownJewelEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct)  throws Exception;
}
