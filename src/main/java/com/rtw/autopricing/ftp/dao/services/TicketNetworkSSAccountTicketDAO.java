package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountTicket;


public interface TicketNetworkSSAccountTicketDAO extends RootDAO<Integer, TicketNetworkSSAccountTicket> {

	public List<TicketNetworkSSAccountTicket> getAllActiveTicketNetworkSSAccountTickets() throws Exception;
	
	public List<TicketNetworkSSAccountTicket> getAllTicketNetworkSSAccountTicketById(Integer Id) throws Exception;
	
	public List<TicketNetworkSSAccountTicket> getAllTNTicketNetworkSSAccountTicketsByEventId(Integer eventId) throws Exception;
	
	public Integer deleteAllTicketNetworkSSAccountTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	
	public Integer deleteAllTicketNetworkSSAccountTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllTnTicketNetworkSSAccountTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnTicketNetworkSSAccountTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	
	public Integer deleteAllTNSpecialCatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInTicketNetworkSSAccountTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	
}

