package com.rtw.autopricing.ftp.dao.services;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.JKTZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.data.ZonesPricingCategoryTicket;


public interface JKTZonesPricingCategoryTicketDAO extends RootDAO<Integer, JKTZonesPricingCategoryTicket> {

	public Collection<ZonesPricingCategoryTicket> getAllActiveZonesPricingCategoryTickets();
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketByAll(Integer eventId,String section,String row,String quantity );
	
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketById(Integer Id);
	public List<String> getPriceHistoryById(Integer Id);
	List<String> getDistinctZonesByTnExchangeEventId(Integer tnExchangeEventId);
	List<JKTZonesPricingCategoryTicket> getAllActiveZonesByEventIds(ArrayList<Integer> eventIds);
	List<String> getAllDistinctVenues();
	public List<JKTZonesPricingCategoryTicket> getAllJktZonesPricingCategoryTicketByAll(
			Integer eventId, String section, String row, String quantity);
}

