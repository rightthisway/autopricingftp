package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.UserBrokerDetails;


public interface UserBrokerDetailsDAO extends RootDAO<Integer, UserBrokerDetails> {
    void deleteBrokerDetailsByUserId(Integer userId);
}
