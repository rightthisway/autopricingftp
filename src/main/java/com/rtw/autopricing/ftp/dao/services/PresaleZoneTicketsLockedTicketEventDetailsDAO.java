package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.PresaleZoneTicketsLockedTicketEventDetails;


public interface PresaleZoneTicketsLockedTicketEventDetailsDAO extends RootDAO<Integer, PresaleZoneTicketsLockedTicketEventDetails>{
	
}
