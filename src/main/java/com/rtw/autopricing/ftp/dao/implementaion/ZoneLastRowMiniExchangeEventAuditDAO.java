package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.dao.services.RootDAO;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEventAudit;

public class ZoneLastRowMiniExchangeEventAuditDAO extends HibernateDAO<Integer, ZoneLastRowMiniExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.ZoneLastRowMiniExchangeEventAuditDAO{

	public Collection<ZoneLastRowMiniExchangeEventAudit> getAllZoneLastRowMiniExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM ZoneLastRowMiniExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<ZoneLastRowMiniExchangeEventAudit> getAllZoneLastRowMiniExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM ZoneLastRowMiniExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<ZoneLastRowMiniExchangeEventAudit> getAllZoneLastRowMiniExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM ZoneLastRowMiniExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<ZoneLastRowMiniExchangeEventAudit> getAllZoneLastRowMiniExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM ZoneLastRowMiniExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}
}
