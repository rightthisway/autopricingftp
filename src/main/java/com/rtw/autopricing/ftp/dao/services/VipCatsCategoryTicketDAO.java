package com.rtw.autopricing.ftp.dao.services;





import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.VipCatsCategoryTicket;


public interface VipCatsCategoryTicketDAO extends RootDAO<Integer, VipCatsCategoryTicket> {

	public Collection<VipCatsCategoryTicket> getAllActiveVipCatsCategoryTickets() throws Exception;
	public List<VipCatsCategoryTicket> getAllVipCatsCategoryTicketsByAll(Integer eventId, String section, String row,String quantity) throws Exception;
	
	public List<VipCatsCategoryTicket> getAllVipCatsCategoryTicketsById(Integer Id) throws Exception;
	List<VipCatsCategoryTicket> getAllVipCatsCategoryTicketsByEventId(Integer eventId) throws Exception;
	List<VipCatsCategoryTicket> getAllTNVipCatsCategoryTicketsByEventId(Integer eventId) throws Exception;
	
	public Integer deleteAllVipMiniCategoryTicketsnotExistinExchangeEventforAnyExchange() throws Exception;
	public Integer deleteAllVipMiniCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnVipMiniCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception;
	public Integer deleteAllTnVipMiniCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllVipMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingproduct) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInVipMiniCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<VipCatsCategoryTicket> getAllVipMiniCatsCategoryTicketsByPosCategoryTicektGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception;
}
