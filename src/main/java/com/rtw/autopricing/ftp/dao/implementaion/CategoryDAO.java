package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.Category;
import com.rtw.autopricing.ftp.enums.EventStatus;

public class CategoryDAO extends HibernateDAO<Integer, Category> implements com.rtw.autopricing.ftp.dao.services.CategoryDAO {

	public void deleteByVenueCategoryId(Integer venueCategoryId) throws Exception{
		bulkUpdate("DELETE FROM CategoryMapping WHERE categoryId in (SELECT id from Category WHERE venueCategoryId = ?)" , new Object[]{venueCategoryId});
//		bulkUpdate("DELETE FROM Category WHERE venueCategoryId = ? " , new Object[]{venueCategoryId});
	}
	
	public Collection<Category> getAllCategoriesByVenueCategoryId(Integer venueCategoryId) throws Exception {
//		return find("FROM Category WHERE venueCategoryId=?", new Object[]{venueCategoryId});
		return DAORegistry.getQueryManagerDAO().getAllCategoriesByVenueCategoryId(venueCategoryId);
	}
	
	public Collection<Category> getAllCategoriesByVenueIdAndCategoryGroup(Integer venueId, String categoryGroup)throws Exception{
		
		Session session = getSession();
		Collection<Category> category = null;
		try{
//			Query query = session.createQuery("FROM Category WHERE venueCategoryId in (SELECT id FROM VenueCategory WHERE venue.id = :venueId AND categoryGroup = :categoryGroup)").setParameter("venueId", venueId).setParameter("categoryGroup", categoryGroup);
//			category = query.list();
			return DAORegistry.getQueryManagerDAO().getAllCategoriesByVenueIdAndCategoryGroup(venueId, categoryGroup);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}/*finally{
			session.close();
		}*/
		//return category;
	}

	public Category getCategoryByVenueCategoryIdAndCategorySymbol(Integer venueCategoryId, String categorySymbol) throws Exception {
		return findSingle("FROM Category WHERE venueCategoryId = ? AND symbol = ?)", new Object[]{venueCategoryId,categorySymbol});
	}
	
	public String getCategorieGroupByVenueCategoryId(Integer venueCategoryId) throws Exception {
		
		Session session = getSession();
		List<String> groupName = null;
		
		try{
//			Query query = session.createQuery("SELECT distinct groupName FROM Category WHERE venueCategoryId = :venueCategoryId").setParameter("venueCategoryId", venueCategoryId);
			groupName = find("SELECT distinct groupName FROM Category WHERE venueCategoryId = " + venueCategoryId);
//			groupName = query.list();
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}/*finally{
			session.close();
		}*/
		return (groupName != null && groupName.size() > 0 ? (String) groupName.get(0) : null);
	}
	public List<String> getAllCategoryByVenueCategoryId(Integer venueCategoryId) throws Exception {
		
		try{
//			Query query = session.createQuery("SELECT distinct groupName FROM Category WHERE venueCategoryId = :venueCategoryId").setParameter("venueCategoryId", venueCategoryId);
			return find("SELECT distinct symbol FROM Category WHERE venueCategoryId = " + venueCategoryId+" order by symbol");
//			groupName = query.list();
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}/*finally{
			session.close();
		}*/
		//return null;
	}
	
	public Collection<Category> getAllCategoriesByVenuCategoryIdsByArtistId(Integer artistId,EventStatus eventSataus) throws Exception {
		
		return find("FROM Category WHERE venueCategoryId in (Select distinct venueCategoryId from Event eve where artistId = ? and eventStatus =? )", 
				new Object[]{artistId,eventSataus});
	}

	public int countByVenueCategoryID(Integer venueCategoryId) throws Exception {
		List list = find("select count(*) FROM CategoryMapping WHERE categoryId in (SELECT id from Category WHERE venueCategoryId = ?)" , new Object[]{venueCategoryId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
}
