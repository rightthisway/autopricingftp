package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.SoldCategoryTicket;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;


public interface SoldCategoryTicketDAO extends RootDAO<Integer, SoldCategoryTicket>{

	public List<SoldCategoryTicket> getAllSoldCategoryTicketsByEventId(Integer eventId) throws Exception;
}
