package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.FtpFileUploadAudit;

public interface FtpFileUploadAuditDAO extends RootDAO<Integer, FtpFileUploadAudit> {
	public List<FtpFileUploadAudit> getAllFTPAuditsByUserName(String userName);
	public List<FtpFileUploadAudit> getAllFTPAuditsByUserNameAndStatus(String userName,String status);
	
	public List<FtpFileUploadAudit> getAllManualFTPAuditsByStatus(String status);
	
	public List<FtpFileUploadAudit> getAllManualFTPAudits();
	
}
