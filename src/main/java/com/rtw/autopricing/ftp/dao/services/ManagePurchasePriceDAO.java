package com.rtw.autopricing.ftp.dao.services;


import java.util.List;

import com.rtw.autopricing.ftp.data.ManagePurchasePrice;


public interface ManagePurchasePriceDAO extends RootDAO<Integer , ManagePurchasePrice> {

	List<Integer> getDistinctArtistIds() throws Exception;

	List<ManagePurchasePrice> getAllByArtistIds(List<Integer> ids) throws Exception;

//	Collection<Artist> getDistinctArtists() throws Exception;

	List<ManagePurchasePrice> getAllByArtistExchangeTicketTypes(List<Integer> ids,List<String> exchangeIds, List<String> ticketTypeIds) throws Exception;
	List<ManagePurchasePrice> getAllByManagePurchasePriceByArtistId(Integer Id) throws Exception;
	List<ManagePurchasePrice> getAllManagePurchasePriceByEventId(Integer eventId) throws Exception;

}
