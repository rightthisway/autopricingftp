package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsCategoryTicket;


public class PresaleZoneTicketsCategoryTicketDAO extends HibernateDAO<Integer, PresaleZoneTicketsCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.PresaleZoneTicketsCategoryTicketDAO {

	public void deleteAll(Collection<PresaleZoneTicketsCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<PresaleZoneTicketsCategoryTicket> getAllActiveTgCatsCategoryTickets()  throws Exception {
		return find("FROM PresaleZoneTicketsCategoryTicket where status ='ACTIVE'");
	}
	
	public List<PresaleZoneTicketsCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity)  throws Exception {
		String query = "FROM PresaleZoneTicketsCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(Integer.parseInt(quantity));								
		
			} 
		
		/*if (row !=null && !row.isEmpty())
		{
			query += " AND (rowRange=?)";
			param.add(row);
		}*/
		
		
		return find(query, param.toArray());
	}
	
	public List<PresaleZoneTicketsCategoryTicket> getAllZoneTicektsProcessorCategoryTicketsByIds(List<Integer> zoneTicektGroupIds)  throws Exception {
		
		List<PresaleZoneTicketsCategoryTicket> result = new ArrayList<PresaleZoneTicketsCategoryTicket>();
		try {
			Map<Integer,String> map = new HashMap<Integer, String>();
			String zoneTicketProcessorIdsStr = "";
			int i = 0;
			int count =0;
			for(Integer id:zoneTicektGroupIds){
				if(count==2000){
					count =0;
					i++;
					map.put(i, zoneTicketProcessorIdsStr);
					zoneTicketProcessorIdsStr ="";
				}
				zoneTicketProcessorIdsStr = zoneTicketProcessorIdsStr + id + ",";
				count++;
			}
			if(!zoneTicketProcessorIdsStr.isEmpty()){
				map.put(++i, zoneTicketProcessorIdsStr);
			}
			for(Integer ii:map.keySet()){
				zoneTicketProcessorIdsStr = map.get(ii);
				if(!zoneTicketProcessorIdsStr.isEmpty()){
					zoneTicketProcessorIdsStr = zoneTicketProcessorIdsStr.substring(0, zoneTicketProcessorIdsStr.length()-1);
				}
				Collection<PresaleZoneTicketsCategoryTicket> temp = find("FROM PresaleZoneTicketsCategoryTicket WHERE zone_tickets_ticket_group_id IN ("  + zoneTicketProcessorIdsStr + ")" + " AND status<>'SOLD'"); 
				if(temp!=null){
					result.addAll(temp);
				}
			}
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//return result;
	}
	
	public List<PresaleZoneTicketsCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id)  throws Exception {
		return find("FROM PresaleZoneTicketsCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id)  throws Exception {
		return find(" select priceHistory FROM PresaleZoneTicketsCategoryTicket where id=?" ,new Object[]{Id});
	}
	
	public List<PresaleZoneTicketsCategoryTicket> getAllZoneTicketsProcessorCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM PresaleZoneTicketsCategoryTicket where eventId=? AND status=?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<PresaleZoneTicketsCategoryTicket> getAllTNZoneTicketsProcessorCategoryTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM PresaleZoneTicketsCategoryTicket where eventId=? and status = ? and tnCategoryTicketGroupId is not null and tnCategoryTicketGroupId <> 0" ,new Object[]{eventId,"ACTIVE"});
	}
	
	public Integer deleteAllPresaleZoneTicketsCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM presale_zone_tickets_category_ticket_group tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllPresaleZoneTicketsCategoryTicketsnotExistinExchangeEventforZoneTicketExchange()  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event not Exist in Exchange Event for Presale ZoneTickets' " +
				" FROM presale_zone_tickets_category_ticket_group tg WITH(NOLOCK)  " +
			    " WHERE tg.status='ACTIVE' AND event_id NOT IN (" +
			    " SELECT event_id FROM presale_zone_tickets_exchange_event le WITH(NOLOCK)" +
			    " inner join event e on e.id=le.event_id " +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND e.venue_category_id is not null AND e.venue_id is not null " +
			    " AND le.zoneticket_broker_id > 0) ";
				//" AND (le.tn_broker_id > 0 OR le.vivid_broker_id > 0 OR le.scorebig_broker_id > 0)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllPresaleZoneTicketsCategoryTicketGroupIdsNotExistInZoneTicketsTicektGroup()  throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("zone.tickets.linked.server");
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Ticket not Exist in PresaleZoneTickets'  " +
	    		" FROM presale_zone_tickets_category_ticket_group tg WITH(NOLOCK)  " +
	    		" WHERE tg.zone_tickets_ticket_group_id is not null AND tg.status='ACTIVE' " +
	    		" and tg.zone_tickets_ticket_group_id not in(select distinct id from "+serverName+".presale_zone_ticket_group WITH(NOLOCK) " +
	    		" where ticket_status=1)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public List<Integer> getAllPresaleZoneTicketsTicketGroupIdsNotExistInTmat()  throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("zone.tickets.linked.server");
		
		String sql = " SELECT ztg.id as groupId " +
					" FROM " + serverName +".presale_zone_ticket_group ztg WITH(NOLOCK) " + 
					 " WHERE ztg.ticket_status=1 AND ztg.id NOT IN (select distinct zone_tickets_ticket_group_id from presale_zone_tickets_category_ticket_group WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND zone_tickets_ticket_group_id is not null) and datediff(minute,ztg.last_updated_date,GETDATE())>3" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 "";
		
		Session session=null;
		
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	
	}
	
}

