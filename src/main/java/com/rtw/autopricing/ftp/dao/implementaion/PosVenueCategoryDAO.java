package com.rtw.autopricing.ftp.dao.implementaion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;
import com.rtw.autopricing.util.connection.Connections;

public class PosVenueCategoryDAO {
	
	public Integer deleteByVenueCategoryId(Integer venueCategoryId,Integer brokerId) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "DELETE FROM venue_category where venue_category_id=? ";
		try {
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			deleteStatement.setInt(1, venueCategoryId);
			deleteStatement.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}finally{

		}
		return 1;
	}
	                              
	                              
	

	
 public List<PosVenueCategory> getAllVenueCategorysByVenueId(Integer VenueId,Integer brokerId) throws Exception{
		
	 Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT venue_category_id,section_low,row_low FROM venue_category where venue_id=? and section_high=''" +
				" and row_high='' and text_desc='' and show_to_sales_staff=1 and default_ticket_group_notes='' " +
				" and ticket_group_stock_type_id=1 and ticket_group_type_id=1 and default_wholesale_price=0.00 and " +
				" default_retail_price=0.00 " +
				" and((seat_high='0' and seat_low='0') or (seat_high='' and seat_low=''))";
		ResultSet resultSet =null;
		List<PosVenueCategory> venuecatList = new ArrayList<PosVenueCategory>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, VenueId);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				PosVenueCategory venuecategory = new PosVenueCategory();
				venuecategory.setId(resultSet.getInt("venue_category_id"));
				venuecategory.setSection(resultSet.getString("section_low"));
				venuecategory.setRow(resultSet.getString("row_low"));
				venuecatList.add(venuecategory);
			}
			return venuecatList;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		return venuecatList;
	}
	
	public Integer save(MiniCategoryTicket catTicket,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "INSERT INTO venue_category (section_high,section_low,row_low,row_high,seat_low,seat_high,default_wholesale_price" +
				",default_retail_price,venue_id,text_desc,show_to_sales_staff,default_ticket_group_notes ,ticket_group_stock_type_id," +
				"ticket_group_type_id) VALUES (''," +
//				"'"+catTicket.getSection()+"','"+catTicket.getRowRange()+"','','','',0.00,0.00, "+catTicket.getPosVenueId()+",'',1,'',1,1)"
				"";
		ResultSet rs= null;
		Statement insertVenueCategoryStatement =null;
		try {
			/*PreparedStatement insertStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			insertStatement.setString(1, "");
			insertStatement.setString(2, ticket.getSection());
			insertStatement.setString(3, "");
			insertStatement.setString(4, ticket.getRow());
			insertStatement.setString(5, "");
			insertStatement.setString(6, "");
			insertStatement.setDouble(7, 0.00);
			insertStatement.setDouble(8, 0.00);
			insertStatement.setInt(9, ticket.getPosVenueId());
			insertStatement.setString(10, "");
			insertStatement.setInt(11, 1);
			insertStatement.setString(12, "");
			insertStatement.setInt(13, 1);
			insertStatement.setInt(14, 1);
			insertStatement.executeUpdate();
			connection.commit();*/
			
			connection.setAutoCommit(false);
			insertVenueCategoryStatement = connection.createStatement();
			insertVenueCategoryStatement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
			rs = insertVenueCategoryStatement.getGeneratedKeys();
			Integer venueCategoryId =null;
			if(rs.next()){
				venueCategoryId = rs.getInt(1);
			}
			connection.commit();
			return venueCategoryId ;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertVenueCategoryStatement!=null){
				insertVenueCategoryStatement.close();
			}
		}
		return -1;
	}
	
	public Integer getVenueCategoryId(MiniCategoryTicket catTicket,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT venue_category_id FROM venue_category where section_low=? and row_low = ? ";
		ResultSet resultSet =null;
		Integer venueCatId = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, catTicket.getSection());
			statement.setString(2, catTicket.getRowRange());
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				venueCatId = resultSet.getInt("venue_category_id");
			}
			return venueCatId;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		return null;
	}
	
	
}
