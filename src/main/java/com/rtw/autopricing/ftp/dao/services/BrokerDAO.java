package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.Broker;


public interface BrokerDAO extends RootDAO<Integer, Broker>{

	public Collection<Broker> getAllActiveBrokers();
	public Collection<Broker> getAllActiveAutopricingBrokers();
	Collection<Broker> getAllBrokers();
//	Collection<Broker> getAll() ;
	Broker getBrokerByPOSBrokerId(int brokerId);
	Broker getBrokerByName(String brokerName);
	public List<Integer> getAllActiveAutopricingBrokerIds();
	public Collection<Broker> getAllActiveAutopricingBrokersWithMZTixBroker();
	
}
