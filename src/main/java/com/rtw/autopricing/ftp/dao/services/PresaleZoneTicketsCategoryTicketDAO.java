package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.PresaleZoneTicketsCategoryTicket;



public interface PresaleZoneTicketsCategoryTicketDAO extends RootDAO<Integer, PresaleZoneTicketsCategoryTicket> {

	public Collection<PresaleZoneTicketsCategoryTicket> getAllActiveTgCatsCategoryTickets() throws Exception;
	public List<PresaleZoneTicketsCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	
	public List<PresaleZoneTicketsCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<PresaleZoneTicketsCategoryTicket> getAllZoneTicketsProcessorCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<PresaleZoneTicketsCategoryTicket> getAllTNZoneTicketsProcessorCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllPresaleZoneTicketsCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllPresaleZoneTicketsCategoryTicketsnotExistinExchangeEventforZoneTicketExchange() throws Exception;
	public List<PresaleZoneTicketsCategoryTicket> getAllZoneTicektsProcessorCategoryTicketsByIds(List<Integer> zoneTicektGroupIds) throws Exception;
	public List<Integer> getAllPresaleZoneTicketsTicketGroupIdsNotExistInTmat() throws Exception;
	public Integer deleteAllPresaleZoneTicketsCategoryTicketGroupIdsNotExistInZoneTicketsTicektGroup() throws Exception;
}

