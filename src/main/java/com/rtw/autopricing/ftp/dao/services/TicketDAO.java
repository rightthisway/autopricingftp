package com.rtw.autopricing.ftp.dao.services;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.enums.TicketStatus;


public interface TicketDAO extends RootDAO<Integer, Ticket> {
	Collection<Ticket> getAllActiveTicketByEventId(int eventId) throws Exception;
	public Collection<Ticket> getAllActiveTicketForAutoCatsByEventId(Integer eventId,Boolean isTNCrawlOnly,Boolean isSkipVividCrawls) throws Exception;
	public Collection<Ticket> getAllActiveInstantTicketForAutoCatByEventId(Integer eventId,Boolean isTNCrawlOnly,Boolean isSkipVividCrawls) throws Exception;
//	Collection<Ticket> getAllTicketsMatchingUserAlert(UserAlert userAlert) throws Exception;
	Collection<Ticket> getAllTicketsByEventAndSiteId(int event_id, String siteId) throws Exception;
	Collection<Ticket> getAllTNSpecialTicketsByEventAndSiteId(Integer eventId) throws Exception;
	Collection<Ticket> getAllActiveTicketsByEventAndSiteId(int event_id, String siteId) throws Exception;
//	Collection<Ticket> getAllSimpleTicketsByEventAndSiteId(int event_id, String siteId) throws Exception;
	Collection<Ticket> getAllTicketsByEvent(int event_id, TicketStatus status) throws Exception;
	Collection<Ticket> getAllBuyItNowTickets(TicketStatus status) throws Exception;
	Collection<Ticket> getAllOutDatedExpiredTickets() throws Exception;
	Collection<Ticket> getAllOutDatedExpiredTicketsFromCrawl(Integer crawlId) throws Exception;
	void updateStatusOfOutdatedExpiredTicketsFromCrawl(Integer crawlId, TicketStatus ticketStatus) throws Exception;
	Collection<Ticket> getAllActiveTicketsUpdatedBefore(Date date) throws Exception;
	Collection<Ticket> getAllActiveTicketsUpdatedBeforeFromCrawl(Integer crawl_id, Date date) throws Exception;
	void updateStatusOfTicketsUpdatedBeforeFromCrawl(Integer crawlId, Date date, TicketStatus ticketStatus) throws Exception;
	Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId) throws Exception;
	Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId) throws Exception;
	Collection<Ticket> getAllActiveRegularTicketsByEvent(Integer eventId) throws Exception;
	Collection<Ticket> getAllActiveTicketsBySiteId(String siteId) throws Exception;
	Collection<Ticket> getAllActiveTicketsByEventAndSiteIds(Integer eventId, String siteId) throws Exception;
	Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId, Integer categoryId) throws Exception;
	Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId, List<Integer> categoryIds) throws Exception;
	Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId, Integer categoryId) throws Exception;
	Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId, List<Integer> categoryIds) throws Exception;
	Collection<Ticket> getAllTicketsByEvent(Integer eventId, Integer categoryId, String day) throws Exception;
	Collection<Ticket> getUncategorizedCompletedTicketsByEvent(Integer eventId, String catScheme) throws Exception;
	Collection<Ticket> getUncategorizedActiveTicketsByEvent(Integer eventId, String catScheme) throws Exception;
	Collection<Ticket> getCategorizedCompletedTicketsByEvent(Integer eventId, String catScheme) throws Exception;
	Collection<Ticket> getCategorizedActiveTicketsByEvent(Integer eventId, String catScheme) throws Exception;
	Collection<Object[]> getSectionsAndRowsFromEventId(Integer eventId) throws Exception;
	Collection<Ticket> getAllActiveTicketsByTour(Integer tourId) throws Exception;
//	List<Object[]> getTicketSiteStatsByEventId(int event_id, TicketStatus ticketStatus) throws Exception;
//	List<Object[]> getNonDuplicateTicketSiteStatsByEventId(int event_id, TicketStatus ticketStatus, RemoveDuplicatePolicy removeDuplicatePolicy) throws Exception;
//	List<Object[]> getTicketQuantityStatsByEventId(int event_id, TicketStatus ticketStatus) throws Exception;
	void deleteTicketsByCrawlId(int crawlId) throws Exception;
	void deleteTicketsByCrawlId(int crawlId, Date startDate, Date endDate) throws Exception;
//	Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) throws Exception;
//	Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) throws Exception;
//	Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, List<Integer> categoryId, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) throws Exception;
	int getTicketCount(TicketStatus status) throws Exception;
	int getTicketCountByEvent(int event_id, TicketStatus status) throws Exception;
	int getTicketQuantitySumByEvent(int event_id, TicketStatus status) throws Exception;
	int getTicketCountByCrawl(int crawl_id, TicketStatus status) throws Exception;
	int getTicketCountByCrawl(int crawl_id, Date startDate, Date endDate) throws Exception;
	int getTicketQuantitySumByCrawl(int crawl_id, TicketStatus status) throws Exception;
	int getTicketCountByCrawl(int crawl_id) throws Exception;
	int getTicketQuantitySumByCrawl(int crawl_id) throws Exception;
	void deleteTicketsByEventId(Integer eventId) throws Exception;
	void enableTickets(Integer crawlerId) throws Exception;
	void disableTickets(Integer crawlerId) throws Exception;
	void disableAndUnassignCrawlTickets(Integer crawlId) throws Exception;
	void recomputeTicketRemainingQuantities() throws Exception;
	void saveOrUpdateAllTicketsWithSameEventAndSite(final Collection<Ticket> tickets) throws Exception;
	Collection<Ticket> getAllTicketsByEventAndDate(Integer eventId, Date date) throws Exception;
	public Collection<Ticket> getAllTicketsByEventUpdatedAfter(int event_id, Date afterDate) throws Exception;
	public Collection<Ticket> getAllActiveNonZonesTicketsByEvent(Integer eventId) throws Exception;
	public List<Ticket> getAllTmatTicketsById(Integer Id) throws Exception;
	public String getTicketDeliveryType(String exchangeName) throws Exception;
	public List<Ticket> getAllActiveInstantTicketByEventId(Integer eventId) throws Exception;
	public List<Ticket> getAllEligibleTNSpecailActiveTicketByEventId(Integer eventId) throws Exception;
}
