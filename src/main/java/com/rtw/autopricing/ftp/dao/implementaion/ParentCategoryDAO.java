package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.ParentCategory;


public class ParentCategoryDAO extends HibernateDAO<Integer, ParentCategory> implements com.rtw.autopricing.ftp.dao.services.ParentCategoryDAO {

	
	public ParentCategory getTourCategoryByName(String name) {
		return findSingle("FROM ParentCategory WHERE name = ? ORDER BY name", new Object[]{name});
	}

	
	public List<ParentCategory> getAllTourCategoryOrderByName() {
		return find("FROM ParentCategory ORDER BY name");
	}
	
	
	public Integer save(ParentCategory entity) {
		// TODO Auto-generated method stub
		super.save(entity);
		return entity.getId();
	}
	
	
	public void update(ParentCategory entity) {
		// TODO Auto-generated method stub
		super.update(entity);
	}
	
}
