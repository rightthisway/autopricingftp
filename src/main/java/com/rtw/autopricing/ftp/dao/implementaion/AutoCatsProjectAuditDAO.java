package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;


public class AutoCatsProjectAuditDAO extends HibernateDAO<Integer, AutoCatsProjectAudit> implements com.rtw.autopricing.ftp.dao.services.AutoCatsProjectAuditDAO {

	public List<AutoCatsProjectAudit> getAllAutoCatsProjectAudits() {
		return find("FROM AutoCatsProjectAudit");
	}

	public List<AutoCatsProjectAudit> getAllAutoCatsProjectAuditsByProductBrokerAndDateRange(Integer productId,Integer brokerId,Date fromDate,Date toDate) throws Exception{
		String sql = "FROM AutoCatsProjectAudit WHERE 1=1 ";
		List<Object> params = new ArrayList<Object>();
		
		if(productId != null){
			sql+= "  AND product.id = ? ";
			params.add(productId);
		}
		
		if(brokerId != null){
			sql+= "  AND broker.id = ? ";
			params.add(brokerId);
		}
		
		if(fromDate != null){
			sql+= "  AND lastRunTime >= ? ";
			params.add(fromDate);
		}
		
		if(toDate != null){
			sql+= "  AND lastRunTime <= ? ";
			params.add(toDate);
		}
		
		return find(sql,params.toArray());
	}

	
	
}
