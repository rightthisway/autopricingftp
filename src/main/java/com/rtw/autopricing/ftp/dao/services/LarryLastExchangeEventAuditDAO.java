package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.LarryLastExchangeEventAudit;

public interface LarryLastExchangeEventAuditDAO extends RootDAO<Integer, LarryLastExchangeEventAudit> {

	Collection<LarryLastExchangeEventAudit> getAllLarryLastExchangeEventAuditsByEventId(Integer eventId);
	Collection<LarryLastExchangeEventAudit> getAllLarryLastExchangeEventAuditsByVenueId(Integer venueId);
	Collection<LarryLastExchangeEventAudit> getAllLarryLastExchangeEventAuditsByArtistId(Integer artistId);
	Collection<LarryLastExchangeEventAudit> getAllLarryLastExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId);
}
