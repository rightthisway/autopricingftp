package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEventAudit;

public interface ManhattanZonePricingExchangeEventAuditDAO extends RootDAO<Integer, ManhattanZonePricingExchangeEventAudit>{
	public Collection<ManhattanZonePricingExchangeEventAudit> getAllManhattanZonePricingExchangeEventAuditsByEventId(Integer eventId);
}
