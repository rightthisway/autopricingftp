package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.SeatGeekWSTracking;

public interface SeatGeekWSTrackingDAO extends RootDAO<Integer, SeatGeekWSTracking> {
	
}
