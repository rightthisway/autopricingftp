package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.TourCategory;



public interface TourCategoryDAO extends RootDAO<Integer, TourCategory>{
	public TourCategory getTourCategoryByName(String name);
	public List<TourCategory> getAllTourCategoryOrderByName();
}

