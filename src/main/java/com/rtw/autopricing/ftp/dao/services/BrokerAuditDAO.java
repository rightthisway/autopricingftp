package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.BrokerAudit;


public interface BrokerAuditDAO extends RootDAO<Integer, BrokerAudit>{

	public List<BrokerAudit> getAllAuditsByBrokerId(Integer brokerId);
}
