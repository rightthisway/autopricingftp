package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingSettingsAudit;


public interface AutopricingSettingsAuditDAO extends RootDAO<Integer, AutopricingSettingsAudit>{

	public List<AutopricingSettingsAudit> getAutopricingSettingsAuditList(Integer brokerId,Integer productId,Integer exchangeId);
}
