package com.rtw.autopricing.ftp.dao.implementaion;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.data.Venue;
import com.rtw.autopricing.ftp.enums.EventStatus;
import com.rtw.autopricing.ftp.utils.TextUtil;


public class EventDAO extends HibernateDAO<Integer, Event> implements com.rtw.autopricing.ftp.dao.services.EventDAO {	
	private Date lastEventUpdate = new Date(new Date().getTime() - 3600000L);
	public final static int OFFSET_AUTO_GEN_EVENT_ID = 10000000;
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(EventDAO.class);
	public Collection<Event> getAllEventsByNameAndDate(String name, Date dateAndTime) throws Exception {
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		
		Date date=null;
		try {
			date = dateFormat.parse(df.format(dateAndTime));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		Date time=null;
		String timeStr="";
		try {
			timeStr=timeFormat.format(dateAndTime);
			int hour = Integer.parseInt(timeStr.split(":")[0]);
			int min = Integer.parseInt(timeStr.split(":")[1]);
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, 1970);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.HOUR_OF_DAY, hour);
			cal.set(Calendar.MINUTE, min);
			cal.set(Calendar.SECOND, 0);
			time = cal.getTime();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		if(date == null){
			return find("FROM Event WHERE name=? AND event_date is null AND eventStatus = ? ", new Object[]{name,EventStatus.ACTIVE});
		}else {
//			return find("FROM Event WHERE name=? AND event_date =? AND event_time=?", new Object[]{name, date,time});
			Session session = getSession();
//			session.beginTransaction();
			
			SQLQuery query = session.createSQLQuery("SELECT id,name,venue_id FROM event WHERE name=:name AND event_date=:date AND event_time=:time AND event_status = 'ACTIVE'");
			query.setString("name", name);
//			query.setDate("date", date);
//			query.setDate("time", time);
			query.setString("date", df.format(date));
			query.setString("time", "1970-01-01 " + timeStr);
			List<Object[]> list = query.list();
			List<Event> events= new ArrayList<Event>();
			Event event =null;
			for(Object[] obj:list){
				event = new Event();
				event.setId((Integer)obj[0]);
				event.setVenueId((Integer)obj[2]);
				event.setLocalDate(date);
				event.setLocalTime(new Time(time.getTime()));
				events.add(event);
			}
			releaseSession(session);
			return events;
		}
		
	}
	
	public Collection<Event> getAllActiveEvents()  throws Exception{
		return find("FROM Event WHERE eventStatus=? ORDER BY name, event_date, event_time Asc", new Object[]{EventStatus.ACTIVE});
	}
	
	public Collection<Event> getAllActiveZoneEvents() throws Exception{
		return find("FROM Event WHERE eventStatus=? AND zoneEvent=? ORDER BY name, event_date, event_time Asc", new Object[]{EventStatus.ACTIVE, Boolean.TRUE});
	}
	
	public Collection<Event> getAllActiveZoneEventsByTour(Integer artistId) throws Exception{
		return find("FROM Event WHERE eventStatus=? AND zoneEvent=? AND artistId=? ORDER BY name, event_date, event_time Asc", new Object[]{EventStatus.ACTIVE, Boolean.TRUE, artistId});
	}
	
	public Collection<Event> getAllEventsByTour(int artistId)  throws Exception{
		return find("FROM Event WHERE artistId=? AND event_status='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{artistId});
	}
	public Collection<Event> getAllEventsByTour(int artistId,Integer brokerId)  throws Exception{
		return find("FROM Event WHERE artistId=? AND event_status='ACTIVE' AND brokerId=? ORDER BY event_date, event_time Asc", new Object[]{artistId,brokerId});
	}
	public Collection<Event> getAllEventsByTourOrderdByEventName(int artistId)  throws Exception{
		return find("FROM Event WHERE artistId=? AND event_status='ACTIVE' ORDER BY name,event_date, event_time Asc", new Object[]{artistId});
	}

	public Collection<Event> getAllActiveEventsByArtistId(Integer artistId)  throws Exception{
		return find("FROM Event WHERE artistId=? AND eventStatus='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{artistId});
	}
	
	public Collection<Event> getAllActiveEventsByArtistIdAndBrokerId(Integer artistId,Integer brokerId)  throws Exception{
		return find("FROM Event WHERE artistId=? AND brokerId = ? AND eventStatus='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{artistId,brokerId});
	}
	public Collection<Event> getAllActiveEventsByArtistIds(List<Integer> artistIds)  throws Exception{
		String list = artistIds.toString().replace("[", "").replace("]", "").replaceAll(", ", ",");
		return find("FROM Event WHERE artistId in (" + list +") AND eventStatus='ACTIVE' ORDER BY event_date, event_time Asc");//, new Object[]{artistId});
	}
	public Collection<Event> getAllEventsByStatus(EventStatus eventStatus)  throws Exception{
		return find("FROM Event WHERE eventStatus=? ORDER BY event_date, event_time Asc", new Object[]{eventStatus});
	}
	
	public Collection<Event> getAllEventsByStatusAndName(EventStatus eventStatus)  throws Exception{
		return find("FROM Event WHERE eventStatus=? ORDER BY name Asc", new Object[]{eventStatus});
	}

	public Collection<Event> getAllEventsByVenueAndStatus(int venueId, EventStatus eventStatus)  throws Exception{
		return find("FROM Event WHERE venueId=? AND eventStatus=? ", new Object[]{venueId, eventStatus});
	}
	public Collection<Event> getAllEventsByArtistAndVenueAndStatus(int artistId,int venueId, EventStatus eventStatus)  throws Exception{
		return find("FROM Event WHERE venueId=? AND eventStatus=? AND artistId=?", new Object[]{venueId, eventStatus,artistId});
	}	
	public Collection<Event> getAllEventsByVenue(int venueId)  throws Exception{
		return find("FROM Event WHERE venueId=? AND event_status='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{venueId});
	}
	public Collection<Event> getAllEventsByVenue(int venueId,Integer brokerId)  throws Exception{
		return find("FROM Event WHERE venueId=? AND event_status='ACTIVE' AND brokerId = ? ORDER BY event_date, event_time Asc", new Object[]{venueId,brokerId});
	}
	public Collection<Event> getAllEventsByVenueOrderdByEventName(int venueId)  throws Exception{
		return find("FROM Event WHERE venueId=? AND event_status='ACTIVE' ORDER BY name,event_date, event_time Asc", new Object[]{venueId});
	}
	
	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventName(Integer grandChildCategoryId)  throws Exception{
		return find("SELECT e FROM Event e,Artist a WHERE e.artistId=a.id AND a.grandChildTourCategory.id=? AND e.eventStatus='ACTIVE' " +
				" ORDER BY e.name,e.localDate, e.localTime", new Object[]{grandChildCategoryId});			
	}

	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventDate(Integer grandChildCategoryId)  throws Exception{
		return find("SELECT e FROM Event e,Artist a WHERE e.artistId=a.id AND a.grandChildTourCategoryId=? AND e.eventStatus='ACTIVE' " +
				" ORDER BY e.localDate, e.localTime", new Object[]{grandChildCategoryId});			
	}
	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventDate(Integer grandChildCategoryId,Integer brokerId)  throws Exception{
		return find("SELECT e FROM Event e,Artist a WHERE e.artistId=a.id AND a.grandChildTourCategoryId=? AND e.eventStatus='ACTIVE' " +
				" AND e.brokerId = ? ORDER BY e.localDate, e.localTime", new Object[]{grandChildCategoryId,brokerId});			
	}
	
	public Collection<Event> getAllEventsByChildCategory(Integer ChildCategoryId, Integer brokerId)  throws Exception{
		return find("SELECT e FROM Event e,Artist a, GrandChildTourCategory g WHERE e.artistId=a.id AND a.grandChildTourCategoryId = g.id AND g.childTourCategory.id=? AND e.eventStatus='ACTIVE' " +
				" ORDER BY e.name,e.localDate, e.localTime", new Object[]{ChildCategoryId,brokerId});			
	}
	
	public Collection<Event> getAllEventsByChildCategory(Integer ChildCategoryId)  throws Exception{
		return find("SELECT e FROM Event e,Artist a, GrandChildTourCategory g WHERE e.artistId=a.id AND a.grandChildTourCategoryId = g.id AND g.childTourCategory.id=? AND e.eventStatus='ACTIVE' " +
				" ORDER BY e.name,e.localDate, e.localTime", new Object[]{ChildCategoryId});			
	}
	
	public Collection<Event> getAllEventsActiveandExpiredByVenue(int venueId) throws Exception{
		return find("FROM Event WHERE venueId=?", new Object[]{venueId});
	}
	
	public Collection<Event> getAllEventsByNameExcerpt(String excerpt, EventStatus eventStatus)  throws Exception{
		String eventStatusQuery = "";
		if (eventStatus != null) {
			eventStatusQuery = " AND eventStatus='" + eventStatus.toString() + "'";
		}
		return find("FROM Event WHERE name LIKE ? " + eventStatusQuery + " ORDER BY localDate, event_time ASC", new Object[]{"%" + TextUtil.removeExtraWhitespaces(excerpt) + "%"});
	}
	
	public Collection<Event> getAllEventsByNameBrokerId(String excerpt, EventStatus eventStatus,Integer brokerId)  throws Exception{
		String eventStatusQuery = "";
		if (eventStatus != null) {
			eventStatusQuery = " AND eventStatus='" + eventStatus.toString() + "' AND brokerId = "+brokerId+" AND brokerStatus = 'ACTIVE' ";
		}
		return find("FROM Event WHERE name LIKE ? " + eventStatusQuery + " ORDER BY localDate, event_time ASC", new Object[]{"%" + TextUtil.removeExtraWhitespaces(excerpt) + "%"});
	}

	public Collection<Event> getAllEventsByDates(Date startDate, Date endDate, EventStatus eventStatus)  throws Exception{
		String andEventStatusQuery = "";
		if (eventStatus != null) {
			andEventStatusQuery = "AND event_status='" + eventStatus.toString() + "'";
		}
		
		if (startDate == null) {
			if (endDate == null) {
				return find("FROM Event WHERE 1=1 " + andEventStatusQuery + " ORDER BY localDate");				
			} else {
				return find("FROM Event WHERE localDate <= ? " + andEventStatusQuery + " ORDER BY localDate, event_time", new Object[]{endDate});								
			}
		} else if (endDate == null) {
			return find("FROM Event WHERE localDate >= ? " + andEventStatusQuery + " ORDER BY localDate, event_time", new Object[]{startDate});								
		} else {
			return find("FROM Event WHERE localDate >= ? AND localDate <= ? " + andEventStatusQuery + " ORDER BY localDate, event_time", new Object[]{startDate, endDate});			
		}
	}

	public Collection<Event> getAllEventsByVenueAndDates(int venueId, Date startDate, Date endDate)  throws Exception{
		return find("FROM Event WHERE venueId=? AND localDate >= ? AND localDate <= ? AND event_status='ACTIVE'", new Object[]{venueId, startDate, endDate});
	}

	public Collection<Event> getAllEventsByTourVenueAndDates(int artistId, int venueId, Date startDate, Date endDate)  throws Exception{
		return find("FROM Event WHERE artistId=? AND venueId=? AND localDate >= ? AND localDate <= ? AND event_status='ACTIVE'", new Object[]{artistId, venueId, startDate, endDate});
	}

	public Collection<Event> getAllEventsByTourVenue(int artistId, int venueId)  throws Exception{
		return find("FROM Event WHERE artistId=? AND venueId=? AND event_status='ACTIVE'", new Object[]{artistId, venueId});
	}

	public Collection<Event> getAllEventsByIds(List<Integer> eventIds)  throws Exception{
		Session session = getSession();
		Collection<Event> list = null;
		try{
			Properties paramEventStatus = new Properties();
			paramEventStatus.put("enumClass", "com.admitone.tmat.enums.EventStatus");
			paramEventStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			Properties paramCreationType = new Properties();
			paramCreationType.put("enumClass", "com.admitone.tmat.enums.CreationType");
			paramCreationType.put("type", "12"); 
			
			Properties paramEventType = new Properties();
			paramEventType.put("enumClass", "com.admitone.tmat.enums.TourType");
			paramEventType.put("type", "12"); 
			String sql  = " SELECT id,name , admitone_id as admitoneId, artist_id as artistId, broker_id as brokerId, broker_status as brokerStatus, creation_date as creationDate, creation_type as creationType ,event_status as eventStatus, event_type as eventType,event_date as localDate, " +
			" event_time as localTime , name , venue_category_id as venueCategoryId,venue_id as venueId,presale_event as presaleEvent  FROM event WHERE id in (:eventIds)";
			list = session.createSQLQuery(sql)
			.addScalar("eventStatus", Hibernate.custom(EnumType.class,paramEventStatus))
			.addScalar("creationType", Hibernate.custom(EnumType.class,paramCreationType))
			.addScalar("eventType", Hibernate.custom(EnumType.class,paramEventType))
			.addScalar("id", Hibernate.INTEGER)
			.addScalar("admitoneId", Hibernate.INTEGER)
			.addScalar("artistId", Hibernate.INTEGER)
			.addScalar("brokerId", Hibernate.INTEGER)
			.addScalar("brokerStatus", Hibernate.STRING)
			.addScalar("name", Hibernate.STRING)
			.addScalar("creationDate", Hibernate.DATE)
			.addScalar("localDate", Hibernate.DATE)
			.addScalar("localTime", Hibernate.TIME)
			.addScalar("venueCategoryId", Hibernate.INTEGER)
			.addScalar("venueId", Hibernate.INTEGER)
			.addScalar("presaleEvent", Hibernate.BOOLEAN)
			.setParameterList("eventIds", eventIds).setResultTransformer(Transformers.aliasToBean(Event.class)).list();
		}catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		if (list.size() == 0) {
			return null;
		}
		return list;
	}
	public Event getEvent(Integer artistId, Date date) throws Exception {
		Collection<Event> list = find("FROM Event WHERE artistId=? and day(event_date)=day(?) AND event_status='ACTIVE'", new Object[]{artistId, date});
		if (list.size() == 0) {
			return null;
		}
		
		return list.iterator().next();
	}
	
	public Integer getUnusedId()  throws Exception{
//		Collection<Integer> list = find("SELECT MAX(id) FROM Event WHERE id BETWEEN ? AND ?", new Object[]{CreationEventListManager.OFFSET_AUTO_GEN_EVENT_ID, CreationEventListManager.OFFSET_STUBHUB_EVENT_ID});
		Collection<Integer> list = find("SELECT MAX(id) FROM Event");
		if (list == null || list.size() == 0) {
			return OFFSET_AUTO_GEN_EVENT_ID;
		}
		Object maxId =  list.iterator().next();
		if(maxId != null){
			return ((Integer)maxId + 1);
		} else {
			return OFFSET_AUTO_GEN_EVENT_ID;

		}
	}
	
	public Collection<Event> getEvent(String name, int venueId, Date date, Time time)  throws Exception{
		
		Collection<Event> list = null;
		if(time != null) {
			list = find("FROM Event WHERE name=? and venueId=? and day(event_date)=day(?) and event_time=? AND event_status='ACTIVE'", new Object[]{name, venueId, date, time});
		} else {
			list= find("FROM Event WHERE name=? and venueId=? and day(event_date)=day(?) AND event_status='ACTIVE'", new Object[]{name, venueId, date});
		}
		if (list.isEmpty()) {
			return null;
		}
		
		return list;
	}
	
	
	
	public boolean updateAll(Collection<Event> entities) {
		/***/
    	/*Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null){
			for(Event event:entities){
				String dependents = property.getValue();
				if(dependents!= null && !dependents.isEmpty()){
					List<TMATDependentEvent> list = DAORegistry.getTmatDependentEventDAO().getTmatDependentsByEventId(event.getId());
					List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
					for(TMATDependentEvent dependent:list){
						if(dependents.contains(dependent.getDependent())){
								dependents= dependents.replace(dependent.getDependent(),"");
								dependent.setAdd(true);
								tmatDependents.add(dependent);
						}
					}
					String depenedentsList[] = dependents.split(",");
					
					for(String dependent:depenedentsList){
						if(dependent==null ||  dependent.isEmpty()){
							continue;
						}
						
						TMATDependentEvent tmatDependent = new TMATDependentEvent();
						tmatDependent.setDependent(dependent);
						tmatDependent.setEventId(event.getId());
						tmatDependent.setAdd(true);
						tmatDependents.add(tmatDependent);
						
					}
					if(!tmatDependents.isEmpty()){
						DAORegistry.getTmatDependentEventDAO().saveOrUpdateAll(tmatDependents);
					}
				}
			}
			
		}*/
		/***/
		return super.updateAll(entities);
	}
    public void deleteById(Integer id) {
    }
    

    public Collection<Event> getEmptyEvents()  throws Exception{
    	Collection<Event> emptyEvents = (Collection<Event>)find("FROM Event e WHERE e.id NOT IN (SELECT DISTINCT(tlc.eventId) FROM TicketListingCrawl tlc WHERE tlc.eventId IS NOT NULL AND tlc.enabled=1) AND event_status!='ACTIVE'");
    	//Collection<Event> emptyEvents = (Collection<Event>)find("FROM Event e WHERE e.id NOT IN (SELECT DISTINCT(tlc.eventId) FROM TicketListingCrawl tlc WHERE tlc.eventId IS NOT NULL)");
    	return emptyEvents;
    }
    
    public void deleteEmptyEvents() {
    	
    }
    
	public Event.EventTicketStat getNonDuplicateEventTicketStat(int eventId)  throws Exception{
		
		Session session = getSession();
		
//		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("SELECT COUNT(remaining_quantity), SUM(remaining_quantity) FROM (select remaining_quantity FROM ticket WHERE event_id=:eventId and ticket_status='ACTIVE' GROUP BY ticket_duplicate_key) as o");
		query.setInteger("eventId", eventId);
		List<Object[]> list = query.list();
		releaseSession(session);
		
		if (list == null || list.size() == 0) {
			return null;
		}
		
		int ticketListingCount = 0;
		int ticketCount = 0;
		
		if (list.get(0)[0] != null) {
			ticketListingCount = ((BigInteger)list.get(0)[0]).intValue(); 
		}
		if (list.get(0)[1] != null) {
			ticketCount = ((BigDecimal)list.get(0)[1]).intValue(); 
		}
		
		return new Event.EventTicketStat(ticketListingCount, ticketCount);
	}
    
    public void delete(Event event) {
    	deleteById(event.getId());
    }    
    
	public int getEventCount()  throws Exception{
		List list = find("SELECT count(id) FROM Event WHERE event_status = 'ACTIVE'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}
	
	public int getEventCountSport()  throws Exception{
		List sports = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Sports'");
		if (sports == null || sports.get(0) == null) {
			return 0;
		}
		return ((Long)sports.get(0)).intValue();		
	}
	public int getEventCountConcerts()  throws Exception{
		List concerts = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Concerts'");
		if (concerts == null || concerts.get(0) == null) {
			return 0;
		}
		return ((Long)concerts.get(0)).intValue();		
	}
	public int getEventCountDefault()  throws Exception{
		List defaultList = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Default'");
		if (defaultList == null || defaultList.get(0) == null) {
			return 0;
		}
		return ((Long)defaultList.get(0)).intValue();		
	}
	public int getEventCountTheater()  throws Exception{
		List theaterList = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Theater'");
		if (theaterList == null || theaterList.get(0) == null) {
			return 0;
		}
		return ((Long)theaterList.get(0)).intValue();		
	}
	public int getEventCountOther()  throws Exception{
		List otherList = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Other'");
		if (otherList == null || otherList.get(0) == null) {
			return 0;
		}
		return ((Long)otherList.get(0)).intValue();		
	}
	public int getEventCountLasVegas()  throws Exception{
		List sports = find("SELECT count(*)  FROM Event a,  Tour b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and a.grandChildTourCategoryId=c.id and c.childTourCategoryId=d.id and d.tourCategoryId=e.id  and a.eventStatus='ACTIVE' and e.name='Las Vegas'");
		if (sports == null || sports.get(0) == null) {
			return 0;
		}
		return ((Long)sports.get(0)).intValue();		
	}

	public int getEventCountByTour(Integer artistId)  throws Exception{
		List list = find("SELECT count(id) FROM Event WHERE artistId=? AND event_status = 'ACTIVE'", new Object[]{artistId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}
	
	public int getMaxEventId()  throws Exception{
		List list = find("SELECT MAX(id) FROM Event");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Integer)list.get(0)).intValue();		
	}

	public Event getEventByStubhubId(Integer stubhubId)  throws Exception{
    	Collection<Event> events = find("FROM Event WHERE stubhubId=?", new Object[]{stubhubId});
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events.iterator().next();
	}

	public Event getEventByAdmitoneId(Integer admitoneId)  throws Exception{
		Collection<Event> events = find("FROM Event WHERE admitoneId=? AND eventStatus = ? ", new Object[]{admitoneId,EventStatus.ACTIVE});
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events.iterator().next();
	}

	public Collection<Event> getAllEventByAdmitoneId()  throws Exception{
    	Collection<Event> events = find("FROM Event WHERE admitoneId is not null AND event_status = 'ACTIVE'");
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events;
	}
	
	public Collection<Event> getAllEventByAdmitoneIdFromUSandCA()  throws Exception{
    	Collection<Event> events = find("SELECT e FROM Event e,Venue v WHERE e.venueId=v.id and e.admitoneId is not null AND e.eventStatus = 'ACTIVE' and v.country in ('US','USA','CA')");
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events;
	}
	public List<Integer> getAllEventsAdmitoneIdFromUSandCA() throws Exception{
		List<Integer> list = null;
		Session session = getSession();
		try{
			String sql = "select e.admitone_id as admitoneID from event e" +
					" inner join venue v on v.id=e.venue_id" +
					" where e.event_status='ACTIVE' and v.country in ('US','CA') and e.event_date>getdate()";
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally {
			session.close();
		}
	}
	
	//TMATT-385
	
	/*public Collection<Event> getAllEventByAdmitoneIdNull() {
    	Collection<Event> events = find("FROM Event WHERE admitoneId is null AND event_status = 'ACTIVE' order by name");
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events;
	}*/
	public Collection<Event> getAllEventByAdmitoneIdNull() throws Exception{
    	Collection<Event> events = find("FROM Event WHERE event_status = 'ACTIVE' and (admitoneId is null or admitoneId not in(select eventId from AdmitoneEvent))");
    	if (events.size() == 0) {
    		return null;
    	}
		return events;
	}//select * from [tmatprodscale].[dbo].[event] where event_status = 'ACTIVE' and 
	//(admitone_id is null or admitone_id not in(select eventid from [tmatprodscale].[dbo].[admitone_event]) )
	
	
	
	public Collection<Event> getAllEventsByTourAndAdmitoneId(int artistId)  throws Exception{
		return find("FROM Event WHERE artistId=? AND event_status='ACTIVE' AND admitoneId is not null ORDER BY event_date, event_time Asc", new Object[]{artistId});
	}
	public Collection<Event> searchEvents(String name, java.sql.Date date, Time time,
			String location) {
		Collection<Event> result = new ArrayList<Event>();
		
		// get all events where date and time matches then do the filtering on name and location
		Collection<Event> events = find("FROM Event WHERE localDate=? and localTime=?", new Object[]{date, time});
		
		for (Event event: events) {
			Venue venue = event.getVenue();
			String eventLocation = venue.getBuilding() + " " + venue.getCity() + " " + venue.getState() + " " + venue.getCountry();
			if (TextUtil.isSimilar(name, event.getName())
				&& TextUtil.isSimilar(location, eventLocation)) {
				result.add(event);
			}
		}
		
		return result;
	}
	
	public Collection<Event> getAllEventsNotInStubhub()  throws Exception{
		return find("FROM Event WHERE stubhubId=null");
	}
	

    public void update(Event event)  {
    	super.update(event);
//    	DAORegistry.getTourDAO().update(event.getTour());
    }
    
    public Integer save(Event event) {
    	return 0;
    }

    public void saveOrUpdate(Event event) {
    }

    public void saveOrUpdateAll(Collection<Event> list) {
    }

    public void saveAll(final Collection<Event> list) {
    }
    
    public void deleteAll(final Collection<Event> list) {
    }

	public Date getMaxDateFromEvents(Integer artistId)  throws Exception{
		List list = find("SELECT max(localDate) FROM Event WHERE artistId=? AND event_status='ACTIVE'", new Object[]{artistId});
		if (list == null || list.get(0) == null) {
			return null;
		}
		return (Date)list.get(0);		
	}

	public Date getMinDateFromEvents(Integer artistId)  throws Exception{
		List list = find("SELECT min(localDate) FROM Event WHERE artistId=? AND event_status='ACTIVE'", new Object[]{artistId});
		if (list == null || list.get(0) == null) {
			return null;
		}
		return (Date)list.get(0);		
	}

	public Event getEventWithSameTourAndVenueButDifferentFromEvent(Integer artistId, Integer venueId, Integer eventId)  throws Exception{
		return findSingle("FROM Event WHERE artistId=? AND venueId=? and id<>?", new Object[]{artistId, venueId, eventId});
	}
	
	
	public Event getEventWithSameTourAndVenueAndNameAndDateTBDAndTimeTBD(
			Integer artistId, Integer venueId, String eventName)  throws Exception{
		// TODO Auto-generated method stub
		return findSingle("FROM Event WHERE artistId=? AND venueId=? and name=? and localDate = null and localTime = null", new Object[]{artistId, venueId, eventName});
	}
	
	
	public Collection<Event> getTBDEvents(String firstLetter)  throws Exception{
		Collection<Event> tbdEvents = (Collection<Event>)find("FROM Event e WHERE e.localDate IS NULL  AND event_status ='ACTIVE' AND e.name like ? ORDER BY e.name ",new Object[]{firstLetter + "%"});
		return tbdEvents;
	}

	
	public List<Event> getToBeAutoCorrectEvents()  throws Exception{
		return find("FROM Event e WHERE e.autoCorrect = ?" , new Object[]{true});
	}

	/*public Collection<Event> getAllEventsByTourIdAndCategoryGroup(int artistId,String categoryGroup) {
		return find("FROM Event WHERE artistId = ? AND venueCategory.categoryGroup = ?", new Object[]{artistId,categoryGroup});
	}*/
	
	public Collection<Event> getAllEventsByTourIdAndCategoryGroup(int artistId,String categoryGroup)  throws Exception{
		return find("FROM Event WHERE artistId = ? AND venueCategoryId IN (SELECT id FROM VenueCategory WHERE categoryGroup = ?)",
				new Object[]{artistId,categoryGroup});
	}

	public List<Event> getAllEventsByVenueCategoryId(Integer venueCategoryId)  throws Exception{
		return find("FROM Event Where venueCategoryId = ?" ,new Object[]{venueCategoryId});
	}
	
	public List<Event> getAllActiveEventsByVenueCategoryId(Integer venueCategoryId)  throws Exception{
		return find("FROM Event Where venueCategoryId = ? AND event_status='ACTIVE' ORDER BY localDate, localTime" ,new Object[]{venueCategoryId});
	}
	
	public boolean unLinkAdmintoneEvent(Integer eventId){
		/*Session session = getSession();
		try{
			Query query = session.createSQLQuery("UPDATE event SET admitone_id=null where id="+eventId+"");
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			session.close();
		}*/
		return true;
	}

	public Collection<Event> getAllEventsByGrandChildCategoryAndDateRange(Date startDate, Date endDate, Integer grandChildCategoryId, EventStatus eventStatus)  throws Exception{
		String query = "SELECT e FROM Event e,Tour t WHERE e.artistId=t.id ";
		List<Object> param = new ArrayList<Object>();
		if (eventStatus != null) {
			query += " AND e.eventStatus=?";
			param.add(eventStatus);
		}
		if (startDate != null) {
			query += " AND e.localDate>=?";
			param.add(startDate);
		} 
		if (endDate != null) {
			query += " AND e.localDate<=?";
			param.add(endDate);								
		} 
		if(grandChildCategoryId!=null){
			query+= " AND t.grandChildTourCategory.id=?";
			param.add(grandChildCategoryId);						
		}
		return find(query + " ORDER BY e.localDate, e.localTime", param.toArray());			
		
	}
	
	public List<Event> getAllActiveEventsWithParentType(String parentType) throws Exception{
			
		Session session = getSession();
		try{
			Query query = session.createQuery("select e from Event e,Tour t,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory tc where " +
					" e.artistId=t.id and t.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=tc.id and " +
					" tc.name = :parentType and tc.id is not null and e.eventStatus = :eventStatus " +
					" order by e.name,e.localDate,e.localTime").setParameter("parentType", parentType).setParameter("eventStatus", EventStatus.ACTIVE);
			return query.list();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		
		//return new ArrayList<Event>();		
		
	}
	
	public List<Event> getAllActiveEventsWithParentId() throws Exception{
		
		Properties paramEventStatus = new Properties();
		paramEventStatus.put("enumClass", "com.rtw.autopricing.ftp.enums.EventStatus");
		paramEventStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
		
		Properties paramCreationType = new Properties();
		paramCreationType.put("enumClass", "com.rtw.autopricing.ftp.enums.CreationType");
		paramCreationType.put("type", "12"); 
		
		Properties paramEventType = new Properties();
		paramEventType.put("enumClass", "com.rtw.autopricing.ftp.enums.TourType");
		paramEventType.put("type", "12"); 
		
		String sql =" SELECT e.id as id,e.name as name , e.admitone_id as admitoneId, e.artist_id as artistId,e.creation_date as creationDate, " +
				" e.creation_type as creationType ,e.event_status as eventStatus, e.event_type as eventType,e.event_date as localDate, " +
				" e.event_time as localTime , e.venue_category_id as venueCategoryId,e.venue_id as venueId,e.last_update as lastUpdate," +
				" tc.id as parentCategoryId,vc.category_group as zoneCategoryGroupName,ae.child_category as posChildCategoryName,vc.Venue_map as venueMap," +
				" e.presale_event as presaleEvent" +
				" from event e" +
				" inner join venue v on v.id=e.venue_id" +
				" inner join artist a on a.id=e.artist_id " +
				" inner join grand_child_tour_category gc on gc.id=a.grand_child_category_id" +
				" left join venue_category vc on vc.id=e.venue_category_id" +
				" inner join child_tour_category cc on cc.id=gc.child_category_id "+
				" inner join tour_category tc on tc.id=cc.tour_category_id" +
				" left join admitone_event ae on ae.eventid=e.admitone_id" +
				" where e.event_status='ACTIVE' " ;
				/*" and v.country in ('US','USA','CA')";//AND vc.category_group not in ('PRESALE') 
*/				
		Session session=null;
		try{
		session = getSession();
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		sqlQuery.addScalar("eventStatus", Hibernate.custom(EnumType.class,paramEventStatus));
		sqlQuery.addScalar("creationType", Hibernate.custom(EnumType.class,paramCreationType));
		sqlQuery.addScalar("eventType", Hibernate.custom(EnumType.class,paramEventType));
		sqlQuery.addScalar("id", Hibernate.INTEGER);
		sqlQuery.addScalar("admitoneId", Hibernate.INTEGER);
		sqlQuery.addScalar("artistId", Hibernate.INTEGER);
		sqlQuery.addScalar("name", Hibernate.STRING);
		sqlQuery.addScalar("creationDate", Hibernate.DATE);
		sqlQuery.addScalar("lastUpdate", Hibernate.DATE);
		sqlQuery.addScalar("localDate", Hibernate.DATE);
		sqlQuery.addScalar("localTime", Hibernate.TIME);
		sqlQuery.addScalar("venueCategoryId", Hibernate.INTEGER);
		sqlQuery.addScalar("venueId", Hibernate.INTEGER);
		sqlQuery.addScalar("parentCategoryId", Hibernate.INTEGER);
		sqlQuery.addScalar("zoneCategoryGroupName", Hibernate.STRING);
		sqlQuery.addScalar("posChildCategoryName", Hibernate.STRING);
		sqlQuery.addScalar("venueMap", Hibernate.BOOLEAN);
		sqlQuery.addScalar("presaleEvent", Hibernate.BOOLEAN);
		List<Event> events =  sqlQuery.setResultTransformer(Transformers.aliasToBean(Event.class)).list();
		return events;
		
		}catch (Exception e) {
		e.printStackTrace();
		throw e;
		}finally{
		session.close();
		}
		//return new ArrayList<Event>();		
		
	}
	
	public void updateBrokerEvents(ArrayList<Integer> eventIds,Integer brokerId,String brokerStatus) throws Exception{
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("update event set broker_id = :brokerId,broker_status = :brokerStatus WHERE id in (:eventIds) ").setParameterList("eventIds", eventIds).setParameter("brokerId", brokerId).setParameter("brokerStatus", brokerStatus);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
	}
	
	public void updateBrokerEvents(ArrayList<Integer> eventIds) throws Exception{
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("update event set broker_id = '',broker_status = '' WHERE id in (:eventIds) ").setParameterList("eventIds", eventIds);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
	}
	
	public void updateBrokerEventsBrowseLite(ArrayList<Integer> eventIds,String brokerStatus) throws Exception{
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("update event set broker_status = :brokerStatus WHERE id in (:eventIds) ").setParameterList("eventIds", eventIds).setParameter("brokerStatus", brokerStatus);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
	}

	public void deleteEventsByArtistId(int artistId) throws Exception{
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("UPDATE event SET event_status = :status WHERE artist_id = :artistId").setParameter("status", EventStatus.DELETED).setParameter("artistId",artistId);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
	}

	public Event getEventWithSameArtistAndVenueAndNameAndDateTBDAndTimeTBD(Integer artistId, Integer venueId, String eventName)  throws Exception{
		
		return findSingle("FROM Event WHERE artistId=? AND venueId=? and name=? and localDate = null and localTime = null", new Object[]{artistId, venueId, eventName});
	}
	
	public Collection<Event> getAllEventsByArtistVenueAndDates(int artistId, int venueId, Date startDate, Date endDate)  throws Exception{
		return find("FROM Event WHERE artistId=? AND venueId=? AND localDate >= ? AND localDate <= ? AND event_status='ACTIVE'", new Object[]{artistId, venueId, startDate, endDate});
	}

	public Integer getParentCategoryIdByEventId(Integer eventId) throws Exception{
		List<Object[]> list = null;
		try{
			String sql = "select tc.* from event e inner join artist a on e.artist_id = a.id "+
						 "inner join grand_child_tour_category gcc on a.grand_child_category_id = gcc.id "+
						 "inner join child_tour_category ctc on gcc.child_category_id = ctc.id "+
						 "inner join tour_category tc on ctc.tour_category_id = tc.id "+
						 "where e.id = :eventId";
			Session session = getSession();
			Query query = session.createSQLQuery(sql);
			query.setParameter("eventId", eventId);
			list = query.list();
			session.close();
			if(list != null){
				return Integer.parseInt(list.get(0)[0].toString());
			}else{
				return null;
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public Collection<Event> getAllEventsByAdmitoneIds(String admitoneIds) throws Exception{
		return find("From Event where admitoneId in ("+ admitoneIds +")");
	}
	public Collection<Event> getAllEventsByeventIds(String eventIds) throws Exception{
		return find("From Event where id in ("+ eventIds +")");
	}
	
}
