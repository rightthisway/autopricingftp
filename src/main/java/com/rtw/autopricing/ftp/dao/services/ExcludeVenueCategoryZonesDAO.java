package com.rtw.autopricing.ftp.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZones;



public interface ExcludeVenueCategoryZonesDAO extends RootDAO<Integer,  ExcludeVenueCategoryZones>{

	public List<ExcludeVenueCategoryZones> getExcludeVenueCategoryZonesByProductId(Integer productId)  throws Exception;
	public List<ExcludeVenueCategoryZones> getExcludeVenueCategoryZonesByProductIdandBrokerId(Integer productId,Integer brokerId)  throws Exception;
	public List<ExcludeVenueCategoryZones> getExcludeVenueCategoryZonesByBrokerIdandProductIds(Integer brokerId,String productIdStr)  throws Exception;
	public List<ExcludeVenueCategoryZones> getAllExcludeVenueCategoryZonesByVenueCategoryId(Integer venueCategoryId)  throws Exception;
	public List<ExcludeVenueCategoryZones> getExcludeVenueCategoryZonesByProductIdAndBrokerIdAndVenueCategoryId(
			Integer productId, Integer brokerId, Integer venueCategoryId)  throws Exception;
}
