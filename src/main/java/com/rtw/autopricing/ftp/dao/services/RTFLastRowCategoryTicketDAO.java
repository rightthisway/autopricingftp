package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.RTFLastRowCategoryTicket;



public interface RTFLastRowCategoryTicketDAO extends RootDAO<Integer, RTFLastRowCategoryTicket> {

	public Collection<RTFLastRowCategoryTicket> getAllActiveTgCatsCategoryTickets() throws Exception;
	public List<RTFLastRowCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity ) throws Exception;
	
	public List<RTFLastRowCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<RTFLastRowCategoryTicket> getAllRTFLastRowCategoryTicketsByEventId(Integer eventId) throws Exception;
	public List<RTFLastRowCategoryTicket> getAllTNRTFLastRowCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllRTFLastRowCategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception;
	public Integer deleteAllRTFLastRowCategoryTicketsnotExistinExchangeEventforZoneTicketExchange() throws Exception;
	public List<RTFLastRowCategoryTicket> getAllRTFLastRowCategoryTicketsByIds(List<Integer> zoneTicektGroupIds) throws Exception;
	public List<Integer> getAllZoneTicketsTicketGroupIdsNotExistInTmat(AutopricingProduct autopricingProduct) throws Exception;
	public Integer deleteAllRTFLastRowCategoryTicketGroupIdsNotExistInZoneTicketsTicektGroup(AutopricingProduct autopricingProduct) throws Exception;
}

