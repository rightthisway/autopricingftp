package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEvent;

public interface ZoneLastRowMiniExchangeEventDAO extends RootDAO<Integer, ZoneLastRowMiniExchangeEvent> {

	ZoneLastRowMiniExchangeEvent getZoneLastRowMiniExchangeEventByEventId(Integer eventId) throws Exception;
	Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	public Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	List<ZoneLastRowMiniExchangeEvent> getZoneLastRowMiniExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByEventIds(String eventIds)  throws Exception;
}
