package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.util.AutoExchangeEventLoader;


public class AutoCats96CategoryTicketDAO extends HibernateDAO<Integer, AutoCats96CategoryTicket> implements com.rtw.autopricing.ftp.dao.services.AutoCats96CategoryTicketDAO {

	public void deleteAll(Collection<AutoCats96CategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<AutoCats96CategoryTicket> getAllActiveTgCatsCategoryTickets() {
		return find("FROM AutoCats96CategoryTicket where status ='ACTIVE'");
	}
	
	public List<AutoCats96CategoryTicket> getAllAutoCats96CatsCategoryTicketsByAll(Integer eventId,String productName,String section,String row,String quantity) {
		String query = "FROM AutoCats96CategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		/*query +=" AND productName=? ";
		param.add(productName);*/
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(Integer.parseInt(quantity));								
		
			} 
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND (rowRange=? OR lastRow=? OR cat_row=?)";
			param.add(row);
			param.add(row);
			param.add(row);
		}
		
		query +=" order by tnBrokerId , internalNotes ";
		
		return find(query, param.toArray());
	}
	
	public List<AutoCats96CategoryTicket> getAllAutoCats96CategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId) throws Exception {
		
		List<AutoCats96CategoryTicket> result = new ArrayList<AutoCats96CategoryTicket>();
		try {
			Map<Integer,String> map = new HashMap<Integer, String>();
			String tnCategoryTicketGroupIdsStr = "";
			int i = 0;
			int count =0;
			for(Integer id:categoryTicektGroupIds){
				if(count==2000){
					count =0;
					i++;
					map.put(i, tnCategoryTicketGroupIdsStr);
					tnCategoryTicketGroupIdsStr ="";
				}
				tnCategoryTicketGroupIdsStr = tnCategoryTicketGroupIdsStr + id + ",";
				count++;
			}
			if(!tnCategoryTicketGroupIdsStr.isEmpty()){
				map.put(++i, tnCategoryTicketGroupIdsStr);
			}
			for(Integer ii:map.keySet()){
				tnCategoryTicketGroupIdsStr = map.get(ii);
				if(!tnCategoryTicketGroupIdsStr.isEmpty()){
					tnCategoryTicketGroupIdsStr = tnCategoryTicketGroupIdsStr.substring(0, tnCategoryTicketGroupIdsStr.length()-1);
				}
				Collection<AutoCats96CategoryTicket> temp = find("FROM AutoCats96CategoryTicket where tnBrokerId="+brokerId+" and tnCategoryTicketGroupId IN ("  + tnCategoryTicketGroupIdsStr + ")" +
						" and status<>'SOLD'"); 
				if(temp!=null){
					result.addAll(temp);
				}
			}
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//return result;
	}
	
	public List<AutoCats96CategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id) {
		return find("FROM AutoCats96CategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) {
		return find(" select priceHistory FROM AutoCats96CategoryTicket where id=?" ,new Object[]{Id});
	}
	
	public List<AutoCats96CategoryTicket> getAllMiniCategoryTicketsByEventId(Integer eventId){
		return find("FROM AutoCats96CategoryTicket where eventId=? AND status=?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<AutoCats96CategoryTicket> getAllTNAutoCats96CategoryTicketsByEventId(Integer eventId) {
		return find("FROM AutoCats96CategoryTicket where eventId=? and status = ? " ,new Object[]{eventId,"ACTIVE"});
	}
	public List<AutoCats96CategoryTicket> getAllTNAutoCats96CategoryTicketsByEventIdNew(Integer eventId) {
		return find("FROM AutoCats96CategoryTicket where eventId=? and status = ?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<AutoCats96CategoryTicket> getAllTNAutoCats96CategoryTicketsByEventIdByBrokerID(Integer eventId,Integer brokerId) {
		return find("FROM AutoCats96CategoryTicket where eventId=? and status = ? and tnBrokerId = ? " ,new Object[]{eventId,"ACTIVE",brokerId});
	}
	
	public List<AutoCats96CategoryTicket> getAllTNAutoCats96CategoryTicketsByEventIdByBrokerIDByProduct(Integer eventId,Integer brokerId,String ProductName) {
		return find("FROM AutoCats96CategoryTicket where eventId=? and status = ? and tnBrokerId = ? and internalNotes = ? " +
				" and tnCategoryTicketGroupId is not null and tnCategoryTicketGroupId <> 0" ,new Object[]{eventId,"ACTIVE",brokerId,ProductName});
	}
	
	public Integer deleteAllAutoCats96CategoryTicketswithinMinimumExcludeEventDays(Integer excludeEventDays)throws Exception{
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
				" inner join event e with(nolock) on e.id=tg.event_id" +
				" WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllAutoCatsCategoryTicketsnotExistinExchangeEventforAnyExchange()throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
				" FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
			    " WHERE tg.status='ACTIVE' AND event_id NOT IN (" +
			    " SELECT event_id FROM autocats96_exchange_event le WITH(NOLOCK)" +
			    " inner join event e with(nolock) on e.id=le.event_id " +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND e.venue_category_id is not null AND e.venue_id is not null " +
			    " AND (le.rot_pos_enabled > 0 OR le.tixcity_pos_enabled > 0 OR le.rtw_pos_enabled > 0 OR le.vivid_broker_id > 0 )) ";
		
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	
	public Integer deleteAllAutoCats96CategoryTicketsnotExistinExchangeEventByBroker(Integer brokerId)throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for TN' " +
				" FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
				" WHERE tg.status='ACTIVE' AND  broker_id="+brokerId+" AND event_id NOT IN (" +//AND tg.is_presale=0
			    " SELECT event_id FROM autocats96_exchange_event le WITH(NOLOCK)" +
			    " inner join event e with(nolock) on e.id=le.event_id " +
			    " inner join venue_category vc with(nolock) on vc.id=e.venue_category_id" +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND vc.id is not null  AND  " +//AND vc.category_group !='PRESALE'
			    " e.venue_id is not null  ";
		
		if(brokerId.equals(10)){//ROT-2	/*Reserve One Tickets*/
			sql += " AND le.rot_pos_enabled > 0";
		}else if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//Tixcity
			sql += " AND le.tixcity_pos_enabled > 0";
		}else if(brokerId.equals(5)){	/*RTW*/
			sql += " AND le.rtw_pos_enabled > 0";
		}
		sql += ")";
			
		int result = 0;
		Session session=null;
		try{
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllPresaleAutoCatsCategoryTicketsnotExistinExchangeEventByBroker(Integer brokerId) throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for TN' " +
				" FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
				" WHERE tg.status='ACTIVE' AND tg.is_presale=1 AND  broker_id="+brokerId+" AND event_id NOT IN (" +
			    " SELECT event_id FROM presale_autocat_exchange_event le WITH(NOLOCK)" +
			    " inner join event e with(nolock) on e.id=le.event_id " +
			    " inner join venue_category vc with(nolock) on vc.id=e.venue_category_id" +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND vc.id is not null AND e.presale_event=1 AND  " +//AND vc.category_group ='PRESALE'
			    " e.venue_id is not null  ";
		
		if(brokerId.equals(10)){//RTW-2	/*Reserve One Tickets*/
			sql += " AND le.rot_pos_enabled > 0";
		}else if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//Tixcity
			sql += " AND le.tixcity_pos_enabled > 0";
		} else if(brokerId.equals(5)){	/*RTW*/
			sql += " AND le.rtw_pos_enabled > 0";
		}
		sql += ")";
			
		int result = 0;
		Session session=null;
		try{
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
 public Integer deleteAllAutoCats96CategoryTicketsInWhichProductDisabled(String internalNotes) throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Product not Exist in Exchange Event for TN' " +
				" FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
				" WHERE tg.status='ACTIVE'  AND internal_notes='"+internalNotes+"'  AND event_id NOT IN (" +//AND tg.is_presale=0
			    " SELECT event_id FROM autocats96_exchange_event le WITH(NOLOCK)" +
			    " inner join event e with(nolock) on e.id=le.event_id " +
			    " inner join venue_category vc with(nolock) on vc.id=e.venue_category_id" +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND vc.id is not null AND " +//AND vc.category_group !='PRESALE' 
			    " e.venue_id is not null  ";
		
		if(internalNotes.equals("MINICATS")){		/*MINICATS*/
			sql += " AND le.minicats_enabled > 0";
		}else if(internalNotes.equals("LASTROW MINICATS")){	/*LASTROW MINICATS*/
			sql += " AND le.lastrowminicats_enabled > 0";
		}else if(internalNotes.equals("VIPMINICATS")){	/*VIPMINICATS*/
			sql += " AND le.vipminicats_enabled > 0";
		} else if(internalNotes.equals("AUTOCAT")){	/*AUTOCAT*/
			sql += " AND le.presale_autocats_enabled > 0";
		}else if(internalNotes.equals("VIPLR")){	/* VIP LASTROW MINICATS*/
			sql += " AND le.vip_lastrowminicats_enabled > 0";
		}
		sql += ")";
			
		int result = 0;
		Session session=null;
		try{
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
 
 public Integer deleteAllPresaleAutoCatsCategoryTicketsInWhichProductDisabled(String internalNotes)throws Exception {
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Product not Exist in Exchange Event for TN' " +
				" FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
				" WHERE tg.status='ACTIVE' AND tg.is_presale=1 AND internal_notes='"+internalNotes+"'  AND event_id NOT IN (" +
			    " SELECT event_id FROM presale_autocat_exchange_event le WITH(NOLOCK)" +
			    " inner join event e with(nolock) on e.id=le.event_id " +
			    " inner join venue_category vc with(nolock) on vc.id=e.venue_category_id" +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND vc.id is not null AND e.presale_event=1  AND " +//AND vc.category_group ='PRESALE'
			    " e.venue_id is not null  ";
		
		if(internalNotes.equals("MINICATS")){		/*MINICATS*/
			sql += " AND le.minicats_enabled > 0";
		}else if(internalNotes.equals("LASTROW MINICATS")){	/*LASTROW MINICATS*/
			sql += " AND le.lastrowminicats_enabled > 0";
		}else if(internalNotes.equals("VIPMINICATS")){	/*VIPMINICATS*/
			sql += " AND le.vipminicats_enabled > 0";
		} else if(internalNotes.equals("AUTOCAT")){	/*AUTOCAT*/
			sql += " AND le.presale_autocats_enabled > 0";
		}else if(internalNotes.equals("VIPLR")){	/* VIP LASTROW MINICATS*/
			sql += " AND le.vip_lastrowminicats_enabled > 0";
		}
		sql += ")";
			
		int result = 0;
		Session session=null;
		try{
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	
	public Integer deleteAllTnAutoCats96CategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays)throws Exception{
		String sql ="  UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
				" inner join event e with(nolock) on e.id=tg.event_id" +
				" WHERE tg.broker_id="+brokerId+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' AND e.event_status='ACTIVE' " +
	    		" AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllTnAutoCats96CategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId)throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
	    " FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.broker_id="+brokerId+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' AND event_id NOT IN (" +
	    " SELECT event_id FROM autocats96_exchange_event le WITH(NOLOCK)" +
	    " inner join event e with(nolock) on e.id=le.event_id " +
	    " where e.event_status='ACTIVE' AND status='ACTIVE' AND " +
	    " (le.rot_pos_enabled = 1 OR le.tixcity_pos_enabled = 1 OR le.rtw_pos_enabled = 1) ) ";
		
		int result = 0;
		Session session=null;
		try{
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllAutoCats96TnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct)throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Ticket not Exist in TN' " +
	    		" FROM autocats96_category_ticket tg WITH(NOLOCK)  " +
	    		" WHERE tg.broker_id="+broker.getId()+" AND tg.tn_category_ticket_group_id is not null AND tg.status='ACTIVE' " +
	    		" and tg.tn_category_ticket_group_id not in(select distinct category_ticket_group_id from "+serverName+".category_ticket_group WITH(NOLOCK) " +
	    		" where internal_notes in ('MINICATS','LASTROW MINICATS','VIPMINICATS','AUTOCAT','VIPLR'))";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	

	public List<Integer> getAllTicketNetworkGroupIdsNotExistInAutoCats96CategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct)throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		/*String sql = " SELECT ctg.category_ticket_group_id as groupId " +
					" FROM " + serverName +".category_ticket_group ctg WITH(NOLOCK) " + 
					 " INNER JOIN " + serverName + ".category_ticket ct ON ct.category_ticket_group_id = ctg.category_ticket_group_id " +
					 " LEFT JOIN " + serverName + ".invoice i ON i.invoice_id = ct.invoice_id " +
					 " WHERE ctg.category_ticket_group_id NOT IN (select distinct tn_category_ticket_group_id from autocats96_category_ticket " +
					 " WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND broker_id="+broker.getId()+" AND tn_category_ticket_group_id is not null) AND " +
					 " ctg.internal_notes in('MINICATS','LASTROW MINICATS','VIPMINICATS','AUTOCAT','VIPLR') " +
					// " and datediff(minute,ctg.create_date,GETDATE())>1" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 " GROUP BY ctg.category_ticket_group_id " +
					 " HAVING SUM(CASE WHEN i.invoice_id IS NOT NULL THEN 1 ELSE 0 END )=0";*/
		
		String sql = " SELECT ctg.category_ticket_group_id as groupId" +
				" FROM  " + serverName + ".category_ticket_group ctg WITH(NOLOCK)" +
				" WHERE ctg.category_ticket_group_id NOT IN (select distinct tn_category_ticket_group_id from autocats96_category_ticket" +
				" WITH(NOLOCK) where status = 'ACTIVE'" +
				" AND broker_id="+broker.getId()+" AND tn_category_ticket_group_id is not null) AND" +
				" ctg.internal_notes in('MINICATS','LASTROW MINICATS','VIPMINICATS','AUTOCAT','VIPLR')" +
				" and ctg.category_ticket_group_id not in (select category_ticket_group_id from " + serverName + ".category_ticket with(nolock) where invoice_id is not null)";		
		Session session=null;
		
//		List<Integer> ticketGroupIdList =  new ArrayList<Integer>();
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			/*if(list != null || list.size() > 0) {
				for (Object[] obj : list) {
					ticketGroupIdList.add(Integer.parseInt(obj[0].toString()));
				}
			}
			return ticketGroupIdList;*/
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	
	}

}

