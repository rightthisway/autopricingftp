package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutopricingExchangeAudit;

public class AutopricingExchangeAuditDAO extends HibernateDAO<Integer,AutopricingExchangeAudit> implements com.rtw.autopricing.ftp.dao.services.AutopricingExchangeAuditDAO{

	public List<AutopricingExchangeAudit> getAutopricingExchangeAuditByExchangeId(Integer exchangeId) {

		return find("FROM AutopricingExchangeAudit WHERE exchangeId=?",new Object[]{exchangeId});
	}
}
