package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.MarketMakerEventTracking;
import com.rtw.autopricing.ftp.data.User;

;

public interface MarketMakerEventTrackingDAO extends RootDAO<Integer, MarketMakerEventTracking> {
	User getCurrentMarketMakerForEvent(Integer eventId);
	MarketMakerEventTracking getCurrentMarketMakerEventTracking(Integer eventId);
	Collection<Event> getCurrentEventsManagedByMarketMaker(String username);
	Collection<Event> getAllEventsManagedByMarketMaker(String username);
}
