package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.LarryLastExchangeEventAudit;

public class LarryLastExchangeEventAuditDAO extends HibernateDAO<Integer, LarryLastExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.LarryLastExchangeEventAuditDAO{

	public Collection<LarryLastExchangeEventAudit> getAllLarryLastExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM LarryLastExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<LarryLastExchangeEventAudit> getAllLarryLastExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM LarryLastExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<LarryLastExchangeEventAudit> getAllLarryLastExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM LarryLastExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<LarryLastExchangeEventAudit> getAllLarryLastExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM LarryLastExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}

}