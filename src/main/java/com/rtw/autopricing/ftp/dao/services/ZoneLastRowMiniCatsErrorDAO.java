package com.rtw.autopricing.ftp.dao.services;

import com.rtw.autopricing.ftp.data.ZoneLastRowMiniCatsError;


public interface ZoneLastRowMiniCatsErrorDAO extends RootDAO<Integer, ZoneLastRowMiniCatsError>{
	
}
