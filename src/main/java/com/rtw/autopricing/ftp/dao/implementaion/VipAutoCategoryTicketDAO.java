package com.rtw.autopricing.ftp.dao.implementaion;




import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutoCatsCategoryTicket;
import com.rtw.autopricing.ftp.data.VipAutoCategoryTicket;
import com.rtw.autopricing.util.CategoryTicket;


public class VipAutoCategoryTicketDAO extends HibernateDAO<Integer, VipAutoCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.VipAutoCategoryTicketDAO {

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<VipAutoCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateAll(Collection<VipAutoCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<VipAutoCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(AutoCatsCategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<VipAutoCategoryTicket> getAllActiveVipAutoCategoryTickets() {
		return find("FROM VipAutoCategoryTicket where status ='ACTIVE'");
	}
	
	public List<VipAutoCategoryTicket> getAllVipAutoCategoryTicketsByAll(Integer eventId, String section, String row, String quantity) {
		String query = "FROM VipAutoCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND rowRange=?";
			param.add(row);
		}
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(quantity);								
		
			}
		
		
		return find(query, param.toArray());
	}
	public List<VipAutoCategoryTicket> getAllVipAutoCategoryTicketsById(Integer Id) {
		return find("FROM VipAutoCategoryTicket where id=?" ,new Object[]{Id});
	}
}

