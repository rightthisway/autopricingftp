package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.ftp.data.TNDRealCategoryTicket;
import com.rtw.autopricing.ftp.data.TNDZonesCategoryTicket;


public class TNDZonesCategoryTicketDAO extends HibernateDAO<Integer, TNDZonesCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.TNDZonesCategoryTicketDAO {

	public void deleteAll(Collection<TNDZonesCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<TNDZonesCategoryTicket> getAllActiveTndZonesCategoryTickets()  throws Exception {
		return find("FROM TNDZonesCategoryTicket where status ='ACTIVE'");
	}
	public List<TNDZonesCategoryTicket> getAllSoldTNDZonesCategoryTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM TNDZonesCategoryTicket where eventId=? and status !='ACTIVE' and tndOrderId is not null",new Object[]{eventId});
	}
	public List<TNDZonesCategoryTicket> getAllTNDZonesCategoryTicketsByTnExchangeEventId(Integer tnExchangeEventId) throws Exception {
		return find("FROM TNDZonesCategoryTicket where tnExchangeEventId=? AND status=?" ,new Object[]{tnExchangeEventId,"ACTIVE"});
	}
	
	public List<TNDZonesCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity)  throws Exception {
		String query = "FROM TNDZonesCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(Integer.parseInt(quantity));								
		
			} 
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND (rowRange=? OR lastRow=?)";
			param.add(row);
			param.add(row);
		}
		
		
		return find(query, param.toArray());
	}
	
	public List<TNDZonesCategoryTicket> getAllMiniCatsCategoryTicketsByPosCategoryTicketGroupIds(List<Integer> categoryTicektGroupIds,Integer brokerId)  throws Exception {
		
		List<TNDZonesCategoryTicket> result = new ArrayList<TNDZonesCategoryTicket>();
		try {
			Map<Integer,String> map = new HashMap<Integer, String>();
			String tnCategoryTicketGroupIdsStr = "";
			int i = 0;
			int count =0;
			for(Integer id:categoryTicektGroupIds){
				if(count==2000){
					count =0;
					i++;
					map.put(i, tnCategoryTicketGroupIdsStr);
					tnCategoryTicketGroupIdsStr ="";
				}
				tnCategoryTicketGroupIdsStr = tnCategoryTicketGroupIdsStr + id + ",";
				count++;
			}
			if(!tnCategoryTicketGroupIdsStr.isEmpty()){
				map.put(++i, tnCategoryTicketGroupIdsStr);
			}
			for(Integer ii:map.keySet()){
				tnCategoryTicketGroupIdsStr = map.get(ii);
				if(!tnCategoryTicketGroupIdsStr.isEmpty()){
					tnCategoryTicketGroupIdsStr = tnCategoryTicketGroupIdsStr.substring(0, tnCategoryTicketGroupIdsStr.length()-1);
				}
				Collection<TNDZonesCategoryTicket> temp = find("FROM TNDZonesCategoryTicket where tnBrokerId="+brokerId+" and tnCategoryTicketGroupId IN ("  + tnCategoryTicketGroupIdsStr + ")" +
						" and status<>'SOLD'"); 
				if(temp!=null){
					result.addAll(temp);
				}
			}
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//return result;
	}
	
	public List<TNDZonesCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id)  throws Exception {
		return find("FROM TNDZonesCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id)  throws Exception {
		return find(" select priceHistory FROM TNDZonesCategoryTicket where id=?" ,new Object[]{Id});
	}
	
	public List<TNDZonesCategoryTicket> getAllTNDZonesCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM TNDZonesCategoryTicket where eventId=? AND status=?" ,new Object[]{eventId,"ACTIVE"});
	}
	public List<TNDZonesCategoryTicket> getAllTNTNDZonesCategoryTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM TNDZonesCategoryTicket where eventId=? and status = ? and tnCategoryTicketGroupId is not null and tnCategoryTicketGroupId <> 0" ,new Object[]{eventId,"ACTIVE"});
	}
	
	public Integer deleteAllTNDZonesCategoryTicketswithinMinimumExcludeEventDays(Integer hourstoConsiderOrders,Integer excludeEventDays) throws Exception {
		String sql ="  UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM tnd_zones_category_ticket tg WITH(NOLOCK)" +
				" WHERE tg.status='ACTIVE' and tnd_order_id not in (" +
				" select  order_id from tnd_sales_autopricing with(nolock)" +
				" where  order_date>dateadd(hh,-"+hourstoConsiderOrders+",getdate()) and event_date>dateadd(hh,24*"+excludeEventDays+",getdate()))";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllTNDZonesCategoryTicketsnotExistinExchangeEventforAnyExchange()  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for all Exchanges' " +
				" FROM tnd_zones_category_ticket tg WITH(NOLOCK)  " +
			    " WHERE tg.status='ACTIVE' AND event_id NOT IN (" +
			    " SELECT event_id FROM mini_exchange_event le WITH(NOLOCK)" +
			    " inner join event e on e.id=le.event_id " +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND e.venue_category_id is not null AND e.venue_id is not null " +
			    " AND (le.tn_broker_id is not null OR le.vivid_broker_id is not null OR le.scorebig_broker_id is not null OR le.fanxchange_broker_id is not null OR le.ticketcity_broker_id is not null OR le.seatgeek_broker_id is not null)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllTnTNDZonesCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception {
		String sql ="  UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event within exclude eventdays' " +
				" FROM tnd_zones_category_ticket tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' AND e.event_status='ACTIVE' " +
	    		" AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllTnTNDZonesCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId)  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Event Disabled for TN' " +
	    " FROM tnd_zones_category_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' AND event_id NOT IN (" +
	    " SELECT event_id FROM mini_exchange_event le WITH(NOLOCK)" +
	    " inner join event e on e.id=le.event_id " +
	    " where e.event_status='ACTIVE' AND status='ACTIVE' AND le.tn_broker_id ="+brokerId+" ) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct)  throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='Ticket not Exist in TN' " +
	    		" FROM tnd_zones_category_ticket tg WITH(NOLOCK)  " +
	    		" WHERE tg.tn_broker_id="+broker.getId()+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' " +
	    		" and tg.tn_category_ticket_group_id not in(select distinct category_ticket_group_id from "+serverName+".category_ticket_group WITH(NOLOCK) " +
	    		" where internal_notes='"+autopricingProduct.getInternalNotes()+"')";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	

	public List<Integer> getAllTicketNetworkGroupIdsNotExistInTNDZonesCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct)  throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql = " SELECT ctg.category_ticket_group_id as groupId " +
					" FROM " + serverName +".category_ticket_group ctg WITH(NOLOCK) " + 
					 " INNER JOIN " + serverName + ".category_ticket ct ON ct.category_ticket_group_id = ctg.category_ticket_group_id " +
					 " LEFT JOIN " + serverName + ".invoice i ON i.invoice_id = ct.invoice_id " +
					 " WHERE ctg.category_ticket_group_id NOT IN (select distinct tn_category_ticket_group_id from tnd_zones_category_ticket WITH(NOLOCK) where status = 'ACTIVE' " +
					 " AND tn_broker_id="+broker.getId()+" AND tn_category_ticket_group_id is not null) AND ctg.internal_notes = '"+autopricingProduct.getInternalNotes()+"' " +
					 " and datediff(minute,ctg.create_date,GETDATE())>15" +//and datediff(minute,ctg.create_date,GETDATE())>15
					 " GROUP BY ctg.category_ticket_group_id " +
					 " HAVING SUM(CASE WHEN i.invoice_id IS NOT NULL THEN 1 ELSE 0 END )=0";
		
		Session session=null;
		
//		List<Integer> ticketGroupIdList =  new ArrayList<Integer>();
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			/*if(list != null || list.size() > 0) {
				for (Object[] obj : list) {
					ticketGroupIdList.add(Integer.parseInt(obj[0].toString()));
				}
			}
			return ticketGroupIdList;*/
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	
	}

}

