package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEvent;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;

public interface AutoCats96ExchangeEventDAO extends RootDAO<Integer, AutoCats96ExchangeEvent>{

	public AutoCats96ExchangeEvent getAutoCats96ExchangeEventByEventId(Integer eventId)throws Exception;
	public Collection<AutoCats96ExchangeEvent> getAllAutoCats96ExchangeEventsByArtistId(Integer artistId)throws Exception;
	public Collection<AutoCats96ExchangeEvent> getAllAutoCats96ExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId)throws Exception;
	public Collection<AutoCats96ExchangeEvent> getAllAutoCats96ExchangeEventsByVenueId(Integer venueId)throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType)throws Exception;
	public Collection<AutoCats96ExchangeEvent> getAllAutoCats96ExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct)throws Exception;
	public void updateAutoCats96ExchangeEventById(Integer id,String productName,Integer tixCount)throws Exception;
	public List<AutoCats96ExchangeEvent> getAllAutoCats96ExchangeEventsofDeletedTmatEvents()throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<AutoCats96ExchangeEvent> getAllAutoCats96ExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<AutoCats96ExchangeEvent> getAllAutoCats96ExchangeEventsByEventIds(String eventIds)  throws Exception;
}
