package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.Site;
import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.enums.TicketDeliveryType;
import com.rtw.autopricing.ftp.enums.TicketStatus;


public class TicketDAO extends HibernateDAO<Integer, Ticket> implements com.rtw.autopricing.ftp.dao.services.TicketDAO {	
		
		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketByEventId(int eventId)  throws Exception {
			return find("FROM Ticket WHERE eventId = ? AND ticketStatus =?", new Object[]{eventId,TicketStatus.ACTIVE});
		}
		
		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketForAutoCatsByEventId(Integer eventId,Boolean isTNCrawlOnly,Boolean isSkipVividCrawls)  throws Exception {
			//Tamil : In autopricing projects we should not consider tickets with price morethan $100000
			String query = "FROM Ticket WHERE eventId = ? AND ticketStatus =? " +//AND id not in (SELECT ticketId FROM SoldCategoryTicket)
						" and ticketType = 'Regular' and currentPrice < 100000 ";
			if(isTNCrawlOnly) {
				query = query + " AND siteId in ('ticketnetworkdirect') ";
			} else {
				
				/* 11/21/2018 11:44AM - Removed Stubhub Tickets from AutoCats & RTF as we are not crawling SH- By Ulaga  */
				if(isSkipVividCrawls) {
					query = query + " AND siteId in ('ticketnetworkdirect','ticketevolution','flashseats') ";
				} else {
					query = query + " AND siteId in ('ticketnetworkdirect','ticketevolution','flashseats') ";					
				}
				//query = query + " AND ticketListingCrawlId not in (select crawlId from StubHubApiTracking where crawlId is not null and status='ACTIVE')";
			}
			return find(query, new Object[]{eventId,TicketStatus.ACTIVE});
		}
		
		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveInstantTicketForAutoCatByEventId(Integer eventId,Boolean isTNCrawlOnly,Boolean isSkipVividCrawls)  throws Exception {
			//Tamil : In autopricing projects we should not consider tickets with price morethan $100000
			
			String query = "FROM Ticket WHERE eventId = ? AND ticketStatus =? " +//AND id not in (SELECT ticketId FROM SoldCategoryTicket)
			" and ticketType = 'Regular' and currentPrice < 100000 AND ticketDeliveryType is not null and ticketDeliveryType not in('MERCURY') ";
			if(isTNCrawlOnly) {
				query = query + " AND siteId in ('ticketnetworkdirect') ";
			} else {
				/* 11/21/2018 11:44AM - Removed Stubhub Tickets from AutoCats & RTF as we are not crawling SH- By Ulaga  */
				if(isSkipVividCrawls) {
					query = query + " AND siteId in ('ticketnetworkdirect','ticketevolution','flashseats') ";
				} else {
					query = query + " AND siteId in ('ticketnetworkdirect','ticketevolution','flashseats') ";					
				}
				//query = query + " AND ticketListingCrawlId not in (select crawlId from StubHubApiTracking where crawlId is not null and status='ACTIVE')";
			}
			return find(query, new Object[]{eventId,TicketStatus.ACTIVE});
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllTicketsByEventAndSiteId(int event_id, String siteId)  throws Exception {
			return find("FROM Ticket WHERE eventId = ? AND siteId = ? AND ticketStatus = ? ", new Object[] {event_id, siteId,TicketStatus.ACTIVE});
		}
		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketsByEventAndSiteId(int event_id, String siteId)  throws Exception {
			return find("FROM Ticket WHERE eventId = ? AND siteId = ? AND ticketStatus='ACTIVE'", new Object[] {event_id, siteId});
		}
		@SuppressWarnings("unchecked")
		// only gets id, siteId, itemId, currentPrice, priceUpdateCount, insertion date, seller, status
		public Collection<Ticket> getAllSimpleTicketsByEventAndSiteId(int event_id, String siteId)  throws Exception {		
			// gets 7 fields instead of 27 fields
			List<Object[]> simpleTickets = find("SELECT id, itemId, insertionDate, currentPrice, priceUpdateCount, seller, ticketStatus FROM Ticket WHERE eventId = ? AND siteId = ?", new Object[] {event_id, siteId});
			
			List<Ticket> tickets = new ArrayList<Ticket>();
			for (Object[] simpleTicket: simpleTickets) {
				Ticket ticket = new Ticket();
				ticket.setId((Integer)simpleTicket[0]);
				ticket.setItemId((String)simpleTicket[1]);
				ticket.setInsertionDate((Date)simpleTicket[2]);
				ticket.setCurrentPrice((Double)simpleTicket[3]);
				ticket.setPriceUpdateCount((Integer)simpleTicket[4]);
				ticket.setSeller((String)simpleTicket[5]);
				ticket.setTicketStatus((TicketStatus)simpleTicket[6]);
				tickets.add(ticket);			
			}
			return tickets;
		}

		
		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllTicketsByEvent(int event_id, TicketStatus status)  throws Exception {
			return find("FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllTicketsByEventUpdatedAfter(int event_id, Date afterDate)  throws Exception {
			return find("FROM Ticket WHERE eventId = ? AND lastUpdate > ?", new Object[]{event_id, afterDate});
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllBuyItNowTickets(TicketStatus status)  throws Exception {
			return find("FROM Ticket WHERE buyItNowPrice = 0 AND ticketStatus=?", new Object[]{status});
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllOutDatedExpiredTickets()  throws Exception {
			// return all the tickets which are expired and which were updated before their expired
			return find("FROM Ticket WHERE endDate < ? AND lastUpdate < endDate", new Object[]{new Date()});		
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllOutDatedExpiredTicketsFromCrawl(Integer crawlId)  throws Exception {
			// return all the tickets which are expired and which were updated before their expired
			return find("FROM Ticket WHERE ticketListingCrawlId = ? AND endDate < ? AND lastUpdate < endDate AND ticketStatus=?", new Object[] {crawlId, new Date(), TicketStatus.ACTIVE});		
		}

		public void updateStatusOfOutdatedExpiredTicketsFromCrawl(Integer crawlId, TicketStatus ticketStatus)  throws Exception {
			bulkUpdate("UPDATE Ticket SET ticketStatus=? WHERE ticketListingCrawlId=? AND endDate < ? and lastUpdate < endDate AND ticketStatus=?", new Object[] {ticketStatus, crawlId, new Date(), TicketStatus.ACTIVE});	
		}
		
		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketsUpdatedBefore(Date date)  throws Exception {
			return find("FROM Ticket WHERE lastUpdate < ? AND ticketStatus=?", new Object[] {date, TicketStatus.ACTIVE});		
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketsUpdatedBeforeFromCrawl(Integer crawlId, Date date)  throws Exception {
			return find("FROM Ticket WHERE ticketListingCrawlId = ? AND lastUpdate < ? AND ticketStatus=?", new Object[] {crawlId, date, TicketStatus.ACTIVE});		
		}
		
		@SuppressWarnings("unchecked")
		public void updateStatusOfTicketsUpdatedBeforeFromCrawl(Integer crawlId, Date date, TicketStatus ticketStatus)  throws Exception {
			bulkUpdate("UPDATE Ticket SET ticketStatus=? WHERE ticketListingCrawlId=? AND lastUpdate < ? AND ticketStatus != ?", new Object[] {ticketStatus, crawlId, date,ticketStatus});				
		}
		
		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId)  throws Exception {
			// return all the tickets which are expired and which were updated before their expired
			return find("FROM Ticket WHERE eventId = ? AND ticketStatus != ?", new Object[] {eventId, TicketStatus.ACTIVE});		
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId)  throws Exception {
			// return all the tickets which are expired and which were updated before their expired
			return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ?", new Object[] {eventId, TicketStatus.ACTIVE});		
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveRegularTicketsByEvent(Integer eventId)  throws Exception {
			// return all the tickets which are expired and which were updated before their expired
			return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? AND ticket_type='REGULAR' ORDER BY buyItNowPrice ASC", new Object[] {eventId, TicketStatus.ACTIVE});		
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketsBySiteId(String siteId)  throws Exception {
			// return all the tickets which are expired and which were updated before their expired
			return find("FROM Ticket WHERE siteId = ? AND ticketStatus = ?", new Object[] {siteId, TicketStatus.ACTIVE});		
		}

		/*@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId, List<Integer> categoryIds) {
			// return all the tickets which are expired and which were updated before their expired
			String query = "FROM Ticket WHERE eventId = ? AND ticketStatus = ?";
			Collection<Ticket> tickets = find(query, new Object[] {eventId, TicketStatus.ACTIVE});		
			return TicketUtil.filterCategories(categoryIds, tickets);
		}
		
		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllTicketsByEvent(Integer eventId, Integer categoryId, String day) {
			// return all the tickets which are expired and which were updated before their expired
			Collection<Ticket> tickets = find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? and ", new Object[] {eventId, TicketStatus.ACTIVE});		
			return TicketUtil.filterCategory(categoryId, tickets);
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getUncategorizedCompletedTicketsByEvent(Integer eventId, String catScheme) {
			// return all the tickets which are expired and which were updated before their expired
			Collection<Ticket> tickets = this.getAllCompletedTicketsByEvent(eventId);		
			return TicketUtil.filterUncategorized(tickets, catScheme);
		}
		

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getUncategorizedActiveTicketsByEvent(Integer eventId, String catScheme) {
			// return all the tickets which are expired and which were updated before their expired
			Collection<Ticket> tickets = this.getAllActiveTicketsByEvent(eventId);
			return TicketUtil.filterUncategorized(tickets, catScheme);
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getCategorizedActiveTicketsByEvent(Integer eventId, String catScheme) {
			// return all the tickets which are expired and which were updated before their expired
			Collection<Ticket> tickets = this.getAllActiveTicketsByEvent(eventId);
			return TicketUtil.filterCategorized(tickets, catScheme);
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getCategorizedCompletedTicketsByEvent(Integer eventId, String catScheme) {
			// return all the tickets which are expired and which were updated before their expired
			Collection<Ticket> tickets = getAllCompletedTicketsByEvent(eventId);		
			return TicketUtil.filterCategorized(tickets, catScheme);
		}*/

		@SuppressWarnings("unchecked")
		public Collection<Object[]> getSectionsAndRowsFromEventId(Integer eventId)  throws Exception {
			return find("SELECT section, row, COUNT(id) FROM Ticket WHERE eventId = ? AND ticketStatus = ? GROUP BY section, row", new Object[] {eventId, TicketStatus.ACTIVE});				
		}

		@SuppressWarnings("unchecked")
		public Collection<Ticket> getAllActiveTicketsByTour(Integer tourId)  throws Exception {
			return find("SELECT t FROM Ticket t, Event e WHERE t.eventId = e.id AND e.tourId=? AND t.ticketStatus = ?", new Object[] {tourId, TicketStatus.ACTIVE});		
		}
		
		@SuppressWarnings("unchecked")
		public List<Object[]> getTicketSiteStatsByEventId(int event_id, TicketStatus ticketStatus)  throws Exception {
			return find("SELECT siteId, COUNT(id), SUM(remainingQuantity) FROM Ticket WHERE eventId=? AND ticketStatus=? GROUP BY siteId", new Object[] {event_id, ticketStatus});						
		}

		/*@SuppressWarnings("unchecked")
		public List<Object[]> getNonDuplicateTicketSiteStatsByEventId(int event_id, TicketStatus ticketStatus, RemoveDuplicatePolicy removeDuplicatePolicy) {
			List<Ticket> tickets = (List<Ticket>)find("FROM Ticket WHERE eventId=? AND ticketStatus=?", new Object[] {event_id, ticketStatus});
			if (tickets == null) {
				return null;
			}
			
			Map<String, Integer> ticketCountBySite = new HashMap<String, Integer>();
			Map<String, Integer> ticketEntryCountBySite = new HashMap<String, Integer>();
			for (String siteId: Constants.getInstance().getSiteIds()) {
				ticketCountBySite.put(siteId, 0);
				ticketEntryCountBySite.put(siteId, 0);
			}
			
			tickets = TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
			for (Ticket ticket: tickets) {
				ticketCountBySite.put(ticket.getSiteId(), ticketCountBySite.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
				ticketEntryCountBySite.put(ticket.getSiteId(), ticketEntryCountBySite.get(ticket.getSiteId()) + 1);
			}
			
			List<Object[]> ticketSiteStats = new  ArrayList<Object[]>();
			for(String siteId: Constants.getInstance().getSiteIds()) {
				ticketSiteStats.add(new Object[]{siteId, new Long(ticketEntryCountBySite.get(siteId)), new Long(ticketCountBySite.get(siteId))});
			}
			return ticketSiteStats;
		}*/

		@SuppressWarnings("unchecked")
		public List<Object[]> getTicketQuantityStatsByEventId(int event_id, TicketStatus ticketStatus)  throws Exception {
			return find("SELECT remainingQuantity, COUNT(id), SUM(remainingQuantity) FROM Ticket WHERE eventId=? AND ticketStatus=? GROUP BY remainingQuantity", new Object[] {event_id, ticketStatus});
		}
		
		public String addSectionToQuery(String query,String section) throws Exception {
			
			String[] splittedSection = new String[10];
			String[] innerSection = new String[10];
			query += "AND lower(section) IN ( ";
			if(section.contains("OR")){
				splittedSection =section.split("OR");
				for (String splitSection : splittedSection) {
					
							splitSection = splitSection.replace(" ", "");
							splitSection = splitSection.replace(" ", "");
							innerSection = new String[10];
							innerSection = splitSection.split("-");
							Integer i = Integer.parseInt(innerSection[0].trim());
							Integer j = Integer.parseInt(innerSection[1].trim());
							Integer start=null;
							Integer end=null;
							if(i > j){
								start = j;
								end=i;
								query +="'"+start+"-"+end+"'";
							}else{
								start = i;
								end=j;
								query +="'"+start+"-"+end+"'";
							}
							int first=start;
							for(int k=start;k <=end;k++){
								
								if(k == first){
									query +="'"+k+"'";
								}else{
									query +=",'"+k+"'";
								}
							}
					}
				
			}else if(section.contains("-")){
				query +="'"+section+"'";
				splittedSection =section.split("-");
				if(null != splittedSection && splittedSection.length >0){
					Integer Start = Integer.parseInt(splittedSection[0]);
					Integer end = Integer.parseInt(splittedSection[1]);
					int j=Start;
					for(int i=Start;i <=end;i++){
						if(i == j){
							query +="'"+i+"'";
						}else{
							query +=",'"+i+"'";
						}
					}
				}
			}else if(section.contains(" ")){
				splittedSection =section.split(" ");
				
				if(null != splittedSection && splittedSection.length >0){
					
					Integer start =Integer.parseInt(splittedSection[0]);
					Integer end =Integer.parseInt(splittedSection[1]);
					int j=start;
					for(int i=start;i <=end;i++){
						if(i == j){
							query +="'"+i+"'";
						}else{
							query +=",'"+i+"'";
						}
					}
					
					 start =Integer.parseInt(splittedSection[2]);
					 end =Integer.parseInt(splittedSection[3]);
					 j=start;
					for(int i=start;i <=end;i++){
						if(i == j){
							query +="'"+i+"'";
						}else{
							query +=",'"+i+"'";
						}
					}
						
				}
			}else{
				section =section.replace(" ", "");
				query +="'"+section+"'";
				
			}
			query +=  " )";
			return query;
		}

		/*@SuppressWarnings("unchecked")
		public ArrayList<SimpleTicket> getEventCategoryLotSimpleTickets(Integer eventId, Integer categoryId, Integer lotSize, String catScheme) {
			return this.getEventCategoryLotSimpleTickets(eventId, categoryId, lotSize, false, catScheme);
		}
		
		@SuppressWarnings("unchecked")
		public ArrayList<SimpleTicket> getEventCategoryLotSimpleTickets(Integer eventId, Integer categoryId, Integer lotSize, boolean removeA1Tix, String catScheme) {
			String query = "FROM Ticket WHERE eventId=? ";

			query += " AND ticket_status='ACTIVE' ";

			if (lotSize != null && lotSize > 0) {
				query += " AND lotSize=" + lotSize;
			}

//			if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//			} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//				query += " AND category_id is not null";
//			} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//				query += " AND category_id is null";
//			} else {
//				query += " AND category_id =" + categoryId;
//			}

			//System.out.println("QUERY=" + query);
			
			Collection<Ticket> tickets;
			ArrayList<SimpleTicket> sTickets = new ArrayList<SimpleTicket>();
			
			tickets = find(query + " ORDER BY buyItNowPrice ASC", new Object[]{eventId});			

			if (tickets.size() == 0) {
				//System.out.println("Returning null Stat");
				return null;
			}

			if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
			} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
				tickets = TicketUtil.filterCategorized(tickets, catScheme);
			} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
				tickets = TicketUtil.filterUncategorized(tickets, catScheme);
			} else {
				tickets = TicketUtil.filterCategory(categoryId, tickets);
			}

			if(removeA1Tix){
				tickets = TicketUtil.removeAdmitOneTickets(tickets);
			}
			
			//This method mutates the Collection tickets
			TicketUtil.removeDuplicateTickets(tickets);

			if (tickets.size() == 0) {
				//System.out.println("Returning null Stat");
				return null;
			}
		
			for(Ticket ticket : tickets){
				
				sTickets.add(new SimpleTicket(ticket, catScheme));
			}
			
			return sTickets;
		}*/
		
		public Collection<Ticket> getAllActiveTicketsByEventAndSiteIds(Integer eventId, String siteId)  throws Exception {
			return find("FROM Ticket WHERE event_id=? and site_id=? and ticket_status='ACTIVE'", new Object[]{eventId, siteId});
		}


		@SuppressWarnings("unchecked")
		public int getTicketCount(TicketStatus status)  throws Exception {
			List list = find("SELECT count(id) FROM Ticket WHERE ticketStatus=?", new Object[]{status});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();
		}

		@SuppressWarnings("unchecked")
		public int getTicketCountByEvent(int event_id, TicketStatus status)  throws Exception {
			List list = find("SELECT count(id) FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();
		}

		@SuppressWarnings("unchecked")
		public int getTicketQuantitySumByEvent(int event_id, TicketStatus status)  throws Exception {
			List list = find("SELECT sum(remainingQuantity) FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();
		}

		@SuppressWarnings("unchecked")
		public int getTicketCountByCrawl(int crawl_id, TicketStatus status)  throws Exception {
			List list = find("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ? AND ticketStatus=?", new Object[]{crawl_id, status});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();
		}

		@SuppressWarnings("unchecked")
		public int getTicketCountByCrawl(int crawl_id, Date startDate, Date endDate)  throws Exception {
			List list = find("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ? AND insertionDate >= ? AND insertionDate < ?", new Object[]{crawl_id, startDate, endDate});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();
		}

		@SuppressWarnings("unchecked")
		public int getTicketQuantitySumByCrawl(int crawl_id, TicketStatus status)  throws Exception {
			List list = find("SELECT sum(remainingQuantity) FROM Ticket WHERE ticketListingCrawlId = ? AND ticketStatus=?", new Object[]{crawl_id, status});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();
		}

		@SuppressWarnings("unchecked")
		public int getTicketCountByCrawl(int crawl_id)  throws Exception {
			List list = find("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ?", new Object[]{crawl_id});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();
		}

		@SuppressWarnings("unchecked")
		public int getTicketQuantitySumByCrawl(int crawl_id)  throws Exception {
			List list = find("SELECT sum(remainingQuantity) FROM Ticket WHERE ticketListingCrawlId = ?", new Object[]{crawl_id});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();
		}

				
		
		public Collection<Ticket> getAllTicketsByEventAndDate(Integer eventId, Date date)  throws Exception {
			return find("FROM Ticket WHERE eventId=? AND date(lastUpdate)=date(?) ORDER BY currentPrice ASC", new Object[]{eventId, date});
		}

		
		public Collection<Ticket> getAllActiveNonZonesTicketsByEvent(Integer eventId)  throws Exception {
			return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? AND ticketListingCrawlId != null", new Object[] {eventId, TicketStatus.ACTIVE});
		}
		
		public List<Ticket> getAllTmatTicketsById(Integer Id)  throws Exception {
			return find("FROM Ticket where id=?" ,new Object[]{Id});
		}
		
		public String getTicketDeliveryType(String exchangeName) throws Exception {
			
			Session session = null;
			String result="";
			try{
//				StringBuilder sb = new StringBuilder();
				session = getSession();
				Query query = session.createQuery("SELECT DISTINCT ticketDeliveryType FROM Ticket WHERE siteId = '"+exchangeName+"'");
				List<TicketDeliveryType> siteList = query.list();
				if(siteList!=null && !siteList.isEmpty()){
					for(TicketDeliveryType siteOb : siteList){
						if(siteOb != null){
							result+= siteOb.name()+",";
						}else{
							result+= "REGULAR,";
						}					
					}
				}else{
					return "REGULAR";
				}
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{
				session.close();
			}
			return result.replaceAll(",$", "");		
		}
		
		public List<Ticket> getAllActiveInstantTicketByEventId(Integer eventId)  throws Exception {
			return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? AND ticketDeliveryType = ?", new Object[]{eventId,TicketStatus.ACTIVE,TicketDeliveryType.INSTANT});
		}
		

		public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId, Integer categoryId) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId, List<Integer> categoryIds) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId,Integer categoryId) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId,
				List<Integer> categoryIds) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Ticket> getAllTicketsByEvent(Integer eventId,
				Integer categoryId, String day) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Ticket> getUncategorizedCompletedTicketsByEvent(
				Integer eventId, String catScheme) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Ticket> getUncategorizedActiveTicketsByEvent(
				Integer eventId, String catScheme) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Ticket> getCategorizedCompletedTicketsByEvent(
				Integer eventId, String catScheme) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Ticket> getCategorizedActiveTicketsByEvent(
				Integer eventId, String catScheme) {
			// TODO Auto-generated method stub
			return null;
		}

		public void deleteTicketsByCrawlId(int crawlId) {
			// TODO Auto-generated method stub
			
		}

		public void deleteTicketsByCrawlId(int crawlId, Date startDate,
				Date endDate) {
			// TODO Auto-generated method stub
			
		}

		public void deleteTicketsByEventId(Integer eventId) {
			// TODO Auto-generated method stub
			
		}

		public void enableTickets(Integer crawlerId) {
			// TODO Auto-generated method stub
			
		}

		public void disableTickets(Integer crawlerId) {
			// TODO Auto-generated method stub
			
		}

		public void disableAndUnassignCrawlTickets(Integer crawlId) {
			// TODO Auto-generated method stub
			
		}

		public void recomputeTicketRemainingQuantities() {
			// TODO Auto-generated method stub
			
		}

		public void saveOrUpdateAllTicketsWithSameEventAndSite(
				Collection<Ticket> tickets) {
			// TODO Auto-generated method stub
			
		}
		
		public void saveOrUpdateAll(Collection<Ticket> tickets) {
			// TODO Auto-generated method stub
		}
		
		public void saveAll(Collection<Ticket> tickets) {
			// TODO Auto-generated method stub
		}
		
		public  boolean updateAll(Collection<Ticket> tickets) {
			return false;
		}

		public Collection<Ticket> getAllTNSpecialTicketsByEventAndSiteId(Integer eventId)  throws Exception {
			//Tamil : In autopricing projects we should not consider tickets with price morethan $100000
			return find("FROM Ticket WHERE siteId = ? AND eventId = ? AND seller = ? and currentPrice < 100000 ",new Object[]{Site.TICKET_EVOLUTION,eventId,"Golden Tickets"});
		}

		public List<Ticket> getAllEligibleTNSpecailActiveTicketByEventId(Integer eventId)  throws Exception {
			//Tamil : In autopricing projects we should not consider tickets with price morethan $100000
			return find("FROM Ticket WHERE siteId in('ticketevolution','ticketnetworkdirect') AND seller not in ('Charm City Tickets','Juliet Tickets')  AND eventId = ? and currentPrice < 100000 ",new Object[]{eventId});
		}
	
}
