package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;

import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEventAudit;

public class ZoneTicketsProcessorExchangeEventAuditDAO extends HibernateDAO<Integer, ZoneTicketsProcessorExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.ZoneTicketsProcessorExchangeEventAuditDAO{

	public Collection<ZoneTicketsProcessorExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM ZoneTicketsProcessorExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<ZoneTicketsProcessorExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM ZoneTicketsProcessorExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<ZoneTicketsProcessorExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM ZoneTicketsProcessorExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<ZoneTicketsProcessorExchangeEventAudit> getAllZoneTicketsProcessorExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM ZoneTicketsProcessorExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}

}