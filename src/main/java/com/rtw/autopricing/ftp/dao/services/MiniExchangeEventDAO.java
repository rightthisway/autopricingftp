package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;

public interface MiniExchangeEventDAO extends RootDAO<Integer, MiniExchangeEvent>{

	public MiniExchangeEvent getMiniExchangeEventByEventId(Integer eventId)  throws Exception;
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsByArtistId(Integer artistId) throws Exception;
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsByVenueId(Integer venueId) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	public List<MiniExchangeEvent> getMiniExchangeEventByVenueAndExchangeBrokerId(Integer venueId,Integer brokerId) throws Exception;
	public List<MiniExchangeEvent> getAllMiniExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsByEventIds(String eventIds)  throws Exception;
}
