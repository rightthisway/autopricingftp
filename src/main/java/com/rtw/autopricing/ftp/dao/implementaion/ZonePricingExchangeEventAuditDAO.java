package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.Collection;


import com.rtw.autopricing.ftp.data.ZonePricingExchangeEventAudit;

public class ZonePricingExchangeEventAuditDAO extends HibernateDAO<Integer, ZonePricingExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.ZonePricingExchangeEventAuditDAO{
    
	public Collection<ZonePricingExchangeEventAudit> getAllZonePricingExchangeEventAuditsByEventId(Integer eventId) {
		return find("FROM ZonePricingExchangeEventAudit WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<ZonePricingExchangeEventAudit> getAllZonePricingExchangeEventAuditsByVenueId(Integer venueId) {
		return find("FROM ZonePricingExchangeEventAudit WHERE event.venueId=?", new Object[]{venueId});
	}

	public Collection<ZonePricingExchangeEventAudit> getAllZonePricingExchangeEventAuditsByArtistId(Integer artistId) {
		return find("FROM ZonePricingExchangeEventAudit WHERE event.artistId=?", new Object[]{artistId});
	}

	public Collection<ZonePricingExchangeEventAudit> getAllZonePricingExchangeEventAuditsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM ZonePricingExchangeEventAudit WHERE event.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}
}
