package com.rtw.autopricing.ftp.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.GlobalAutoPricingAudit;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;

public interface LarryLastExchangeEventDAO extends RootDAO<Integer, LarryLastExchangeEvent> {

	LarryLastExchangeEvent getLarryLastExchangeEventByEventId(Integer eventId) throws Exception;
	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByVenueId(Integer venueId) throws Exception;
	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByArtistId(Integer artistId) throws Exception;
	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) throws Exception;
	void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) throws Exception;
	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct) throws Exception;
	List<LarryLastExchangeEvent> getLastRowMiniExchangeEventByVenueAndExchangeBrokerId(
			Integer venueId, Integer brokerId) throws Exception;
	public List<LarryLastExchangeEvent> getAllLarryLastExchangeEventsofDeletedTmatEvents() throws Exception;
	public void updateAutopricingVenueSettings(AutopricingVenueSettings autopricingVenueSetting)  throws Exception;
	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByAdmitoneIds(String admitoneEventIds)  throws Exception;
	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByEventIds(String eventIds)  throws Exception;
}
