package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEventAudit;

public class AutoCats96ExchangeEventAuditDAO extends HibernateDAO<Integer,AutoCats96ExchangeEventAudit> implements com.rtw.autopricing.ftp.dao.services.AutoCats96ExchangeEventAuditDAO{

	public List<AutoCats96ExchangeEventAudit> getAutoCats96ExchangeEventAuditByEventId(Integer eId) {
		
		Session session = getSession();
		List<AutoCats96ExchangeEventAudit> list = new ArrayList<AutoCats96ExchangeEventAudit>();
		try{
			Query query = session.createQuery("FROM AutoCats96ExchangeEventAudit where event_id = :eIds");
			query.setParameter("eIds", eId);
			list= query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}				
		return list;
	}
}
