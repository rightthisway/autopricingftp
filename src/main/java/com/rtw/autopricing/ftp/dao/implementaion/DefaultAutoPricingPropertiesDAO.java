package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;


public class DefaultAutoPricingPropertiesDAO extends HibernateDAO<Integer, DefaultAutoPricingProperties> implements com.rtw.autopricing.ftp.dao.services.DefaultAutoPricingPropertiesDAO {

	public DefaultAutoPricingProperties getDefaultAutoPricingProperties(Integer productId,Integer parentCategoryId) throws Exception {
		
		return findSingle(" FROM DefaultAutoPricingProperties where productId=? and parentCategoryId=? order by lastUpdated",new Object[]{productId,parentCategoryId});
	}
	public List<DefaultAutoPricingProperties> getDefaultAutoPricingPropertiesByProductId(Integer productId)throws Exception {
		
		return find(" FROM DefaultAutoPricingProperties where productId=?",new Object[]{productId});
	}
	
}
