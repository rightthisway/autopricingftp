package com.rtw.autopricing.ftp.dao.implementaion;




import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutoCatsCategoryTicket;
import com.rtw.autopricing.ftp.data.ManualCategoryTicket;



public class ManualCategoryTicketDAO extends HibernateDAO<Integer, ManualCategoryTicket> implements com.rtw.autopricing.ftp.dao.services.ManualCategoryTicketDAO {

	
	
	public List<ManualCategoryTicket> getAllManualCategoryTicketsByBrokerId(Integer brokerId)  throws Exception {
		String query = "FROM ManualCategoryTicket WHERE brokerId=? AND status = ? ";
		return find(query, new Object[]{brokerId,"ACTIVE"});
	}
	
	
}


