package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeEventZonesAudit;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZonesAudit;

public class ExcludeVenueCategoryZonesAuditDAO extends HibernateDAO<Integer, ExcludeVenueCategoryZonesAudit> implements com.rtw.autopricing.ftp.dao.services.ExcludeVenueCategoryZonesAuditDAO{

	
	public List<ExcludeVenueCategoryZonesAudit> getExcludeVenueCategoryZonesAuditByProductIdAndBrokerIdAndVenueCategoryIdAndZone(
			Integer productId, Integer brokerId, Integer venueCategoryId,String zone)  throws Exception{
		return find("FROM ExcludeVenueCategoryZonesAudit where venueCategoryId=? AND productId=? AND brokerId=? AND zone=?",new Object[]{venueCategoryId,productId,brokerId,zone});
				
	}

}
