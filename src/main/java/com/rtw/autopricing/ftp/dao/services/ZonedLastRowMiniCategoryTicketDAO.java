package com.rtw.autopricing.ftp.dao.services;



import java.util.Collection;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.ZonedLastRowMiniCategoryTicket;



public interface ZonedLastRowMiniCategoryTicketDAO extends RootDAO<Integer, ZonedLastRowMiniCategoryTicket> {

	public Collection<ZonedLastRowMiniCategoryTicket> getAllActiveZonedLastRowMiniCategoryTickets() throws Exception;
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsByAll(Integer eventId,String section,String row,String quantity) throws Exception;
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsById(Integer Id) throws Exception;
	public List<String> getPriceHistoryById(Integer Id) throws Exception;
	public List<ZonedLastRowMiniCategoryTicket> getAllTNZonedLastRowMiniCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllTnZonedLastRowMiniCategoryTicketswithinExcludeEventDaysByBroker(Integer brokerId,int excludeEventDays) throws Exception;
	public Integer deleteAllZonedLastRowMiniCategoryTicketswithinExcludeEventDays(int excludeEventDays) throws Exception;
	public Integer deleteAllTnZonedLastRowMiniCategoryTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId) throws Exception;
	public Integer deleteAllZonedLastRowMiniCategoryTicketsnotExistinExchangeEventforAnyExchange(Integer brokerId)  throws Exception;
	public Integer deleteAllZoneLastRowTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<Integer> getAllTicketNetworkGroupIdsNotExistInZonedLastRowMiniCategoryTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct) throws Exception;
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsByPosTicketGroupIds(List<Integer> ticektGroupIds,Integer brokerId) throws Exception;
	public Integer updateCategoryTicketReasonByTnTicketGroupIandBrokerId(Integer tnTicketGroupId,Integer brokerId,String reason) throws Exception;
	public List<ZonedLastRowMiniCategoryTicket> getAllTNZonedLastRowMiniCategoryTicketsByEventIdByBrokerID(Integer eventId,Integer brokerId);
}
