package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.ExcludeVenues;

public class ExcludeVenuesDAO extends HibernateDAO<Integer, ExcludeVenues> implements com.rtw.autopricing.ftp.dao.services.ExcludeVenuesDAO{


	public Collection<ExcludeVenues> getExcludeVenuesByVenueId(Integer venueId) throws Exception {
		// TODO Auto-generated method stub
		return find("FROM ExcludeVenues where venueId=?",new Object[]{venueId});
	}

	public ExcludeVenues getExcludeVenueByProductIdAndBrokerIdAndVenueId(
			Integer productId, Integer brokerId, Integer venueId)  throws Exception{
		return findSingle("FROM ExcludeVenues where productId=? AND brokerId=? AND venueId=?",new Object[]{productId,brokerId,venueId});
				
	}


}
