package com.rtw.autopricing.ftp.dao.implementaion;

import java.util.List;

import com.rtw.autopricing.ftp.data.DefaultAutoPricingPropertiesAudit;


public class DefaultAutoPricingPropertiesAuditDAO extends HibernateDAO<Integer, DefaultAutoPricingPropertiesAudit> implements com.rtw.autopricing.ftp.dao.services.DefaultAutoPricingPropertiesAuditDAO {

public List<DefaultAutoPricingPropertiesAudit> getDefaultAutoPricingPropertiesAudit(Integer productId,Integer parentCategoryId) {
		
		return find(" FROM DefaultAutoPricingPropertiesAudit where productId=? and parentCategoryId=? order by lastUpdated",new Object[]{productId,parentCategoryId});
	}
	
}
