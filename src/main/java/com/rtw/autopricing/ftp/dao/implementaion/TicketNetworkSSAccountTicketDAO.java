package com.rtw.autopricing.ftp.dao.implementaion;


import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.CategoryTicket;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountTicket;


public class TicketNetworkSSAccountTicketDAO extends HibernateDAO<Integer, TicketNetworkSSAccountTicket> implements com.rtw.autopricing.ftp.dao.services.TicketNetworkSSAccountTicketDAO {

	public void deleteAll(Collection<TicketNetworkSSAccountTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<TicketNetworkSSAccountTicket> getAllActiveTicketNetworkSSAccountTickets()  throws Exception {
		return find("FROM TicketNetworkSSAccountTicket where status ='ACTIVE'");
	}
	
	public List<TicketNetworkSSAccountTicket> getAllTicketNetworkSSAccountTicketById(Integer Id)  throws Exception {
		return find("FROM TicketNetworkSSAccountTicket where id=?" ,new Object[]{Id});
	}
	
	public List<TicketNetworkSSAccountTicket> getAllTNTicketNetworkSSAccountTicketsByEventId(Integer eventId)  throws Exception {
		return find("FROM TicketNetworkSSAccountTicket where eventId=? and status = ? " ,new Object[]{eventId,"ACTIVE"});
	} // and tnCategoryTicketGroupId is not null and tnCategoryTicketGroupId <> 0
	
	public Integer deleteAllTicketNetworkSSAccountTicketswithinMinimumExcludeEventDays(Integer excludeEventDays) throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
				" FROM ticket_network_ssaccount_ticket tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.status='ACTIVE' AND e.event_status='ACTIVE' AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllTicketNetworkSSAccountTicketsnotExistinExchangeEventforAnyExchange()  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
				" FROM ticket_network_ssaccount_ticket tg WITH(NOLOCK)  " +
			    " WHERE tg.status='ACTIVE' AND event_id NOT IN (" +
			    " SELECT event_id FROM ticket_network_ssaccount_exchange_event le WITH(NOLOCK)" +
			    " inner join event e on e.id=le.event_id " +
			    " where e.event_status='ACTIVE' AND status='ACTIVE' AND e.venue_category_id is not null AND e.venue_id is not null " +
			    " AND (le.tn_broker_id is not null OR le.vivid_broker_id is not null OR le.scorebig_broker_id is not null OR le.fanxchange_broker_id is not null OR le.ticketcity_broker_id is not null OR le.seatgeek_broker_id is not null)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllTnTicketNetworkSSAccountTicketswithinExcludeEventDaysByBroker(Integer brokerId,Integer excludeEventDays) throws Exception {
		String sql ="  UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
				" FROM ticket_network_ssaccount_ticket tg WITH(NOLOCK)  " +
				" inner join event e on e.id=tg.event_id" +
				" WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' AND e.event_status='ACTIVE' " +
	    		" AND datediff(HOUR,GETDATE(),e.event_date)<("+excludeEventDays+"*24)";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	public Integer deleteAllTnTicketNetworkSSAccountTicketsnotExistinExchangeEventForTNByBroker(Integer brokerId)  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
	    " FROM ticket_network_ssaccount_ticket tg WITH(NOLOCK)  " +
	    " WHERE tg.tn_broker_id="+brokerId+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' AND event_id NOT IN (" +
	    " SELECT event_id FROM ticket_network_ssaccount_exchange_event le WITH(NOLOCK)" +
	    " inner join event e on e.id=le.event_id " +
	    " where e.event_status='ACTIVE' AND status='ACTIVE' AND le.tn_broker_id ="+brokerId+" ) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteAllTNSpecialCatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(Broker broker,AutopricingProduct autopricingProduct)  throws Exception {

		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE() " +
	    		" FROM ticket_network_ssaccount_ticket tg WITH(NOLOCK)  " +
	    		" WHERE tg.tn_broker_id="+broker.getId()+" AND tg.tn_category_ticket_group_id > 0 AND tg.status='ACTIVE' " +
	    		" and tg.tn_category_ticket_group_id not in(select distinct category_ticket_group_id from "+serverName+".category_ticket_group WITH(NOLOCK) " +
	    		" where internal_notes='"+autopricingProduct.getInternalNotes()+"')";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	

	public List<Integer> getAllTicketNetworkGroupIdsNotExistInTicketNetworkSSAccountTicketByBrokerId(Broker broker,AutopricingProduct autopricingProduct)  throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String serverName =resourceBundle.getString("indux.server." + broker.getPosBrokerId());
		
		String sql = " SELECT ctg.ticket_group_id as groupId " +
		" FROM " + serverName +".ticket_group ctg WITH(NOLOCK) " + 
		 " INNER JOIN " + serverName + ".ticket ct ON ct.ticket_group_id = ctg.ticket_group_id " +
		 " LEFT JOIN " + serverName + ".invoice i ON i.invoice_id = ct.invoice_id " +
		 " WHERE ctg.ticket_group_id NOT IN (select distinct tn_ticket_group_id from ticket_network_ssaccount_ticket WITH(NOLOCK) where status = 'ACTIVE' " +
		 " AND tn_broker_id="+broker.getId()+" AND tn_ticket_group_id is not null) AND ctg.internal_notes = '"+autopricingProduct.getInternalNotes()+"' and ctg.retail_price > 0 " +
		 " and datediff(minute,ctg.create_date,GETDATE())>15" +//and datediff(minute,ctg.create_date,GETDATE())>15
		 " GROUP BY ctg.ticket_group_id " +
		 " HAVING SUM(CASE WHEN i.invoice_id IS NOT NULL THEN 1 ELSE 0 END )=0";
		
		Session session=null;
		
//		List<Integer> ticketGroupIdList =  new ArrayList<Integer>();
		try {
//			session = getSessionFactory().openSession();
			session = getSession();
			List<Integer> list = session.createSQLQuery(sql).list();
			
			/*if(list != null || list.size() > 0) {
				for (Object[] obj : list) {
					ticketGroupIdList.add(Integer.parseInt(obj[0].toString()));
				}
			}
			return ticketGroupIdList;*/
			return list;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		//return null;
	
	}

}

