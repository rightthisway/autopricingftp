package com.rtw.autopricing.ftp.dao.implementaion;

import com.rtw.autopricing.ftp.data.TMATInduxEventDetails;

public class TMATInduxEventDetailsDAO  extends HibernateDAO<Integer, TMATInduxEventDetails> implements com.rtw.autopricing.ftp.dao.services.TMATInduxEventDetailsDAO{
	
	public TMATInduxEventDetails getTMATInduxEventDetailsByEventId(Integer eventId)  throws Exception {
		return findSingle("FROM TMATInduxEventDetails WHERE eventId=?", new Object[]{eventId});
	}
}
