package com.rtw.autopricing.ftp.zonetickets.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class WebServiceTracking implements Serializable{
	private Integer id;
	private String serverIpAddress;
	private String customerIpAddress;
	private String webConfigId;
	private String methodName;
	private String methodDescription;
	private String url;
	private WebServiceActionType actionType;
	private String actionProcess;
	private String eventSearchValue;
	private String eventGpsSearch;
	private String eventId;
	private String orderId;
	private String customerId;
	private String orderTicketGroupId;
	private String orderTicketGroupQty;
	private String orderSoldPrice;
	private String zoneTicketInfoPage;
	private java.util.Date hittingDate;
	private String hittingDateStr;
	private String authenticatedHit;
	private String actionResult;
	private UserOrder userOrder;
	private ZoneEvent zoneEvent;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public String getServerIpAddress() {
		return serverIpAddress;
	}
	public void setServerIpAddress(String serverIpAddress) {
		this.serverIpAddress = serverIpAddress;
	}
	
	
	
	public String getCustomerIpAddress() {
		return customerIpAddress;
	}
	
	public void setCustomerIpAddress(String customerIpAddress) {
		this.customerIpAddress = customerIpAddress;
	}
	
	public String getWebConfigId() {
		return webConfigId;
	}
	public void setWebConfigId(String webConfigId) {
		this.webConfigId = webConfigId;
	}
	
	
	
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public WebServiceActionType getActionType() {
		return actionType;
	}
	public void setActionType(WebServiceActionType actionType) {
		this.actionType = actionType;
	}
	
	
	public String getActionProcess() {
		return actionProcess;
	}
	public void setActionProcess(String actionProcess) {
		this.actionProcess = actionProcess;
	}
	
	
	public String getEventSearchValue() {
		return eventSearchValue;
	}
	public void setEventSearchValue(String eventSearchValue) {
		this.eventSearchValue = eventSearchValue;
	}
	
	
	public String getEventGpsSearch() {
		return eventGpsSearch;
	}
	public void setEventGpsSearch(String eventGpsSearch) {
		this.eventGpsSearch = eventGpsSearch;
	}
	
	
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	
	public String getOrderTicketGroupId() {
		return orderTicketGroupId;
	}
	public void setOrderTicketGroupId(String orderTicketGroupId) {
		this.orderTicketGroupId = orderTicketGroupId;
	}
	
	
	public String getOrderTicketGroupQty() {
		return orderTicketGroupQty;
	}
	public void setOrderTicketGroupQty(String orderTicketGroupQty) {
		this.orderTicketGroupQty = orderTicketGroupQty;
	}
	
	
	public String getOrderSoldPrice() {
		return orderSoldPrice;
	}
	public void setOrderSoldPrice(String orderSoldPrice) {
		this.orderSoldPrice = orderSoldPrice;
	}
	
	
	public String getZoneTicketInfoPage() {
		return zoneTicketInfoPage;
	}
	public void setZoneTicketInfoPage(String zoneTicketInfoPage) {
		this.zoneTicketInfoPage = zoneTicketInfoPage;
	}
	
	
	public java.util.Date getHittingDate() {
		if(null != hittingDateStr && !hittingDateStr.isEmpty()){
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			try{
				hittingDate = df.parse(hittingDateStr);
			}catch(Exception e){
				hittingDate=null;
			}
		}
		return hittingDate;
	}
	public void setHittingDate(java.util.Date hittingDate) {
		this.hittingDate = hittingDate;
	}
	
	
	public String getAuthenticatedHit() {
		return authenticatedHit;
	}
	public void setAuthenticatedHit(String authenticatedHit) {
		this.authenticatedHit = authenticatedHit;
	}
	
	
	public String getActionResult() {
		return actionResult;
	}
	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	
	public String getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getHittingDateStr() {
		return hittingDateStr;
	}
	public void setHittingDateStr(String hittingDateStr) {
		this.hittingDateStr = hittingDateStr;
	}
	public UserOrder getUserOrder() {
		return userOrder;
	}
	public void setUserOrder(UserOrder userOrder) {
		this.userOrder = userOrder;
	}
	public String getMethodDescription() {
		return methodDescription;
	}
	public void setMethodDescription(String methodDescription) {
		this.methodDescription = methodDescription;
	}
	public ZoneEvent getZoneEvent() {
		return zoneEvent;
	}
	public void setZoneEvent(ZoneEvent zoneEvent) {
		this.zoneEvent = zoneEvent;
	}
	
	
}
