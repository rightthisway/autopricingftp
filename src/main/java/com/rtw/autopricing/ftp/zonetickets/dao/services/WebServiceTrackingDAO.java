package com.rtw.autopricing.ftp.zonetickets.dao.services;



import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.WebConfigInfo;
import com.rtw.autopricing.ftp.zonetickets.data.WebServiceTracking;

public interface WebServiceTrackingDAO {
	
	public List<WebServiceTracking> getAllZoneTicketsTrackingByDateandActionType(String fromDate ,String toDate,String actionType
			,String webConfigId) throws Exception;
	public List<WebServiceTracking> getAllZoneTicketsOrdersTrackingByDate(String fromDate ,String toDate,String webConfigId) throws Exception;
	public List<WebServiceTracking> getAllZoneTicketsEventsTrackingByDate(String fromDate ,String toDate,String webConfigId,String gpsSearch) throws Exception;
	public List<WebConfigInfo> getAllWebConfigInfo() throws Exception;
	public List<WebServiceTracking> getAllZoneTicketsTicketTrackingByDate(String fromDate ,String toDate,String webConfigId) throws Exception;
}
