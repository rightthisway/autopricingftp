package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsAdditionalMarkup;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsAdditionalMarkupAudit;
import com.rtw.autopricing.util.connection.Connections;

public class ZoneTicketsAdditionalMarkupAuditDAO implements com.rtw.autopricing.ftp.zonetickets.dao.services.ZoneTicketsAdditionalMarkupAuditDAO  {
	
	
	public List<ZoneTicketsAdditionalMarkupAudit> getAllZoneTicketsAdditionalMarkupAuditByEventIdandZone(Integer eventId,String zone) throws Exception {
		
		String sql = "SELECT DISTINCT am.id as id,am.event_id as eventId,am.zone as zone,am.additional_markup as additionalMarkup," +
				" am.last_updated_date as lastupdatedDate,am.last_updated_by as lastupdatedBy,am.action" +
				" from zone_tickets_additional_markup_audit am " +
				" where am.event_id= "+eventId+" and am.zone='"+zone+"' order by am.last_updated_date";
		
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<ZoneTicketsAdditionalMarkupAudit> additionalMarkupAudits = new ArrayList<ZoneTicketsAdditionalMarkupAudit>();
			while(resultSet.next()){
				ZoneTicketsAdditionalMarkupAudit additionalMarkupAudit = new ZoneTicketsAdditionalMarkupAudit();
				additionalMarkupAudit.setId(resultSet.getInt("id"));
				additionalMarkupAudit.setEventId(resultSet.getInt("eventId"));
				additionalMarkupAudit.setZone(resultSet.getString("zone"));
				additionalMarkupAudit.setAdditionalMarkup(resultSet.getDouble("additionalMarkup"));
				additionalMarkupAudit.setLastupdatedDate(resultSet.getDate("lastupdatedDate"));
				additionalMarkupAudit.setLastupdatedBy(resultSet.getString("lastupdatedBy"));
				additionalMarkupAudit.setAction(resultSet.getString("action"));
				
				additionalMarkupAudits.add(additionalMarkupAudit);
				
			}
			return additionalMarkupAudits;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}

	public int saveOrUpdateAll(List<ZoneTicketsAdditionalMarkupAudit> ztAdditionalMarkupAudits) throws Exception{
		Connection connection = Connections.getZoneTicketsConnection();
		
		String insertSql = "INSERT INTO zone_tickets_additional_markup_audit(event_id,zone,additional_markup,last_updated_date,last_updated_by,action)" +
					"values(?,?,?,?,?,?)";
		String updateSql = "update zone_tickets_additional_markup_audit set event_id=?,zone=?,additional_markup=?,last_updated_date=?,last_updated_by=?,action=? where id=?";
		int[] insertResult ;
		int[] updateResult ;
		try{
			connection.setAutoCommit(false);
			PreparedStatement insertStatement = connection.prepareStatement(insertSql);
			PreparedStatement updateStatement = connection.prepareStatement(updateSql);
			int i =0;
			int u=0;
			for(ZoneTicketsAdditionalMarkupAudit ztAdditionalMarkupAudit : ztAdditionalMarkupAudits){
				if(ztAdditionalMarkupAudit.getId() != null){
					u++;
					updateStatement.setInt(1, ztAdditionalMarkupAudit.getEventId());
					updateStatement.setString(2, ztAdditionalMarkupAudit.getZone());
					updateStatement.setDouble(3, ztAdditionalMarkupAudit.getAdditionalMarkup());
					updateStatement.setTimestamp(4, new Timestamp(ztAdditionalMarkupAudit.getLastupdatedDate().getTime()));
					updateStatement.setString(5, ztAdditionalMarkupAudit.getLastupdatedBy());
					updateStatement.setString(6, ztAdditionalMarkupAudit.getAction());
					updateStatement.setInt(7, ztAdditionalMarkupAudit.getId());
						
					updateStatement.addBatch();
				} else {
					i++;
					insertStatement.setInt(1, ztAdditionalMarkupAudit.getEventId());
					insertStatement.setString(2, ztAdditionalMarkupAudit.getZone());
					insertStatement.setDouble(3, ztAdditionalMarkupAudit.getAdditionalMarkup());
					insertStatement.setTimestamp(4, new Timestamp(ztAdditionalMarkupAudit.getLastupdatedDate().getTime()));
					insertStatement.setString(5, ztAdditionalMarkupAudit.getLastupdatedBy());
					insertStatement.setString(6, ztAdditionalMarkupAudit.getAction());
					insertStatement.addBatch();
				}
				
			}
			int result=0;
			if(i>0){
				insertResult = insertStatement.executeBatch();
				result += insertResult.length;
			}
			if(u>0){
				updateResult = updateStatement.executeBatch();
				result+= updateResult.length;
			}
			connection.commit();
			return result ;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
		}finally {
//			connection.close();
		}
		
		return 0;
	}
	
	public int deleteAll(List<ZoneTicketsAdditionalMarkupAudit> zoneTicketAdditionalMarkupAudits) throws Exception {
		String deleteSql = "DELETE FROM zone_tickets_additional_markup_audit WHERE id = ? " ;
		Connection connection = Connections.getZoneTicketsConnection();
		PreparedStatement deleteStatement;
		int[] deleteResult ;
		try {
			connection.setAutoCommit(false);
			deleteStatement = connection.prepareStatement(deleteSql);
			for(ZoneTicketsAdditionalMarkupAudit zoneTicketAdditionalMarkupAudit : zoneTicketAdditionalMarkupAudits){
				deleteStatement.setInt(1, zoneTicketAdditionalMarkupAudit.getId());
				deleteStatement.addBatch();
			}
			deleteResult = deleteStatement.executeBatch();
			connection.commit();
			return deleteResult.length;
		} catch (SQLException e) {
			connection.rollback();
		}finally{
//			connection.close();
		}
		return 0;
	}
	
}
