package com.rtw.autopricing.ftp.zonetickets.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * class to represent user order for tickets.
 * @author Ulaganathan
 *
 */

public class UserOrder implements Serializable {
	
	private static DecimalFormat decimalFormat = new DecimalFormat("###.##");
	private Integer id;
	private Double deliveryCost;
	
	@XStreamOmitField
	private Date orderDate;
	private Date deliveryDate;
	private String wsOrderDate;
	private String wsDeliveryDate;
	private Double orderTotal;
	private String orderStatus;
	private Double serviceCharge;
	private String transactionId;	
	private Integer customerId;
	private Customer customer;
	private String paymentMethod;
	private Double originalOrderTotal;
	private Double totalDeductedAmount;
	private Double discountAmount;
	private Double loyaltySpent;
	private Double promotionalOffer;
	private String appPlatform;
	private String creditCardType;
	
	@XStreamOmitField
	private ZoneEvent zoneEvent;
	
	@XStreamOmitField
	private TicketGroup ticketGroup;
	
	@JsonIgnore
	private String discountAmountStr;
	@JsonIgnore
	private String loyaltySpentStr;
	@JsonIgnore
	private String promotionalOfferStr;
	@JsonIgnore
	private String orderTotalStr;
	
	private Double loyaltyEarned;
	
	/**
	 * 
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return customerId
	 */
	public Integer getCustomerId() {
		return customerId;
	}
	/**
	 * 
	 * @param customerId, customer-Id to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	/**
	 * 
	 * @return deliveryCost
	 */
	public Double getDeliveryCost() {
		if(null == deliveryCost){
			deliveryCost=0d;	
		}
		return deliveryCost;
	}
	/**
	 * 
	 * @param deliveryCost, deliveryCost to set
	 */
	public void setDeliveryCost(Double deliveryCost) {
		this.deliveryCost = deliveryCost;
	}
	/**
	 * 
	 * @return orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}
	/**
	 * 
	 * @param orderDate, order date to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	/**
	 * 
	 * @return orderStatus
	 */
	public String getOrderStatus() {
		if(null == orderStatus || orderStatus.isEmpty()){
			orderStatus="";	
		}
		return orderStatus;
	}
	/**
	 * 
	 * @param orderStatus, orderStatus to set
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * 
	 * @return orderTotal
	 */
	public Double getOrderTotal() {
		if(null == orderTotal){
			orderTotal=0d;	
		}
		return Double.valueOf(decimalFormat.format(orderTotal));
	}
	/**
	 * 
	 * @param orderTotal, orderTotal to set
	 */
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	/**
	 * 
	 * @return serviceCharge
	 */
	public Double getServiceCharge() {
		if(null == serviceCharge){
			serviceCharge=0d;	
		}
		return serviceCharge;
	}
	/**
	 * 
	 * @param serviceCharge, serviceCharge to set
	 */
	public void setServiceCharge(Double serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	/**
	 * 
	 * @return transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * 
	 * @param transactionId, not in use right now, it will be covered in phase-II, 
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * 	
	 * @return customer
	 */

	public Customer getCustomer() {
		return customer;
	}
	/**
	 * 
	 * @param customer, customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public String getPaymentMethod() {
		if(null == paymentMethod || paymentMethod.isEmpty()){
			paymentMethod="";	
		}
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public Double getOriginalOrderTotal() {
		if(null == originalOrderTotal){
			originalOrderTotal=0d;	
		}
		
		return Double.valueOf(decimalFormat.format(originalOrderTotal));
	}
	public void setOriginalOrderTotal(Double originalOrderTotal) {
		this.originalOrderTotal = originalOrderTotal;
	}
	
	public Double getTotalDeductedAmount() {
		if(null == totalDeductedAmount){
			totalDeductedAmount=0d;	
		}
		return Double.valueOf(decimalFormat.format(totalDeductedAmount));
	}
	public void setTotalDeductedAmount(Double totalDeductedAmount) {
		this.totalDeductedAmount = totalDeductedAmount;
	}
	
	public Double getDiscountAmount() {
		if(null == discountAmount){
			discountAmount=0d;	
		}
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	
	public Double getLoyaltySpent() {
		if(null == loyaltySpent){
			loyaltySpent=0d;	
		}
		return loyaltySpent;
	}
	public void setLoyaltySpent(Double loyaltySpent) {
		this.loyaltySpent = loyaltySpent;
	}
	
	public Double getPromotionalOffer() {
		if(null == promotionalOffer){
			promotionalOffer=0d;	
		}
		return promotionalOffer;
	}
	public void setPromotionalOffer(Double promotionalOffer) {
		this.promotionalOffer = promotionalOffer;
	}
	
	public String getWsOrderDate() {
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		if(orderDate != null && (null == wsOrderDate || wsOrderDate.isEmpty()) ){
			wsOrderDate = formatter.format(orderDate);
		}
		if(null == wsOrderDate || wsOrderDate.isEmpty()){
			wsOrderDate="";	
		}

		return wsOrderDate;
	}
	public void setWsOrderDate(String wsOrderDate) {
		this.wsOrderDate = wsOrderDate;
	}
	
	
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	public String getAppPlatform() {
		return appPlatform;
	}
	public void setAppPlatform(String appPlatform) {
		this.appPlatform = appPlatform;
	}
	
	public String getCreditCardType() {
		if(null ==creditCardType ){
			creditCardType="";
		}
		return creditCardType;
	}
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	public String getWsDeliveryDate() {
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		if(deliveryDate != null && (null == wsDeliveryDate || wsDeliveryDate.isEmpty()) ){
			wsDeliveryDate = formatter.format(deliveryDate);
		}
		if(null == wsDeliveryDate || wsDeliveryDate.isEmpty()){
			wsDeliveryDate="";	
		}
		return wsDeliveryDate;
	}
	public void setWsDeliveryDate(String wsDeliveryDate) {
		this.wsDeliveryDate = wsDeliveryDate;
	}
	
	public ZoneEvent getZoneEvent() {
		return zoneEvent;
	}
	public void setZoneEvent(ZoneEvent zoneEvent) {
		this.zoneEvent = zoneEvent;
	}
	
	public TicketGroup getTicketGroup() {
		return ticketGroup;
	}
	public void setTicketGroup(TicketGroup ticketGroup) {
		this.ticketGroup = ticketGroup;
	}
	
	public String getDiscountAmountStr() {
		if(null != discountAmount){
			try{
				//discountAmountStr =TicketUtil.getRoundedValueString(discountAmount.doubleValue());
			}catch(Exception e){
				discountAmountStr = "0.00";
			}
		}
		return discountAmountStr;
	}
	public void setDiscountAmountStr(String discountAmountStr) {
		this.discountAmountStr = discountAmountStr;
	}
	
	
	public String getLoyaltySpentStr() {
		if(null != loyaltySpent){
			try{
				//loyaltySpentStr =TicketUtil.getRoundedValueString(loyaltySpent.doubleValue());
			}catch(Exception e){
				loyaltySpentStr = "0.00";
			}
		}
		return loyaltySpentStr;
	}
	public void setLoyaltySpentStr(String loyaltySpentStr) {
		this.loyaltySpentStr = loyaltySpentStr;
	}
	
	
	public String getPromotionalOfferStr() {
		if(null != promotionalOffer){
			try{
				//promotionalOfferStr =TicketUtil.getRoundedValueString(promotionalOffer.doubleValue());
			}catch(Exception e){
				promotionalOfferStr = "0.00";
			}
		}
		return promotionalOfferStr;
	}
	
	public void setPromotionalOfferStr(String promotionalOfferStr) {
		this.promotionalOfferStr = promotionalOfferStr;
	}
	
	
	public String getOrderTotalStr() {
		if(null != orderTotal){
			try{
				//orderTotalStr =TicketUtil.getRoundedValueString(orderTotal.doubleValue());
			}catch(Exception e){
				orderTotalStr = "0.00";
			}
		}
		return orderTotalStr;
	}
	public void setOrderTotalStr(String orderTotalStr) {
		this.orderTotalStr = orderTotalStr;
	}
	
	
	public Double getLoyaltyEarned() {
		return loyaltyEarned;
	}
	public void setLoyaltyEarned(Double loyaltyEarned) {
		this.loyaltyEarned = loyaltyEarned;
	}
	
	
	
	
}

