package com.rtw.autopricing.ftp.zonetickets.data;


import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


public final class ZoneTicketsEvent implements Serializable  {
	protected static final long serialVersionUID = -7402732543016575381L;
	
	
	protected Integer id;
	protected Integer tmatEventId;
	protected Integer venueId;
	protected String eventName;
	protected Date eventDate;
	private Time eventTime;
	private Integer venueCategoryId;
	private Integer artistId;
	private String venueName;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Integer getTmatEventId() {
		return tmatEventId;
	}

	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}

	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}

	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

}