package com.rtw.autopricing.ftp.zonetickets.dao.services;

import java.util.List;
import java.util.Set;

import com.rtw.autopricing.ftp.data.PresaleZoneTicketsCategoryTicket;




public interface PresaleZoneTicketsTicketGroupDAO {
	
	public Integer save(PresaleZoneTicketsCategoryTicket catTicket) throws Exception;
	public Integer updateCategoryTicketGroup(PresaleZoneTicketsCategoryTicket catTicket) throws Exception;
	public Integer updateByCategoryTicketGroups(List<PresaleZoneTicketsCategoryTicket> catTixList) throws Exception;
	public Integer deleteByCategoryTicketGroup(Integer zoneTicketsTicketGroupId) throws Exception;
	public Integer deleteByCategoryTicketGroups(List<PresaleZoneTicketsCategoryTicket> catTixList) throws Exception;
	public List<Integer> getZoneTicketsSoldTicketGroupIds(String fromDateStr,String toDateStr) throws Exception;
	public Integer deleteByZoneTicketGroupIds(List<Integer> zoneTicketGroupIds) throws Exception;
	public Set<Integer> getAllActiveLockedTicketEventIds() throws Exception;
	 
}
