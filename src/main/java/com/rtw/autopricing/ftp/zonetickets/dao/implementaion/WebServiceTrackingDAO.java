package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.Customer;
import com.rtw.autopricing.ftp.zonetickets.data.TicketGroup;
import com.rtw.autopricing.ftp.zonetickets.data.TicketUtil;
import com.rtw.autopricing.ftp.zonetickets.data.TrackingUtils;
import com.rtw.autopricing.ftp.zonetickets.data.UserOrder;
import com.rtw.autopricing.ftp.zonetickets.data.WebConfigInfo;
import com.rtw.autopricing.ftp.zonetickets.data.WebServiceActionType;
import com.rtw.autopricing.ftp.zonetickets.data.WebServiceTracking;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneEvent;
import com.rtw.autopricing.util.connection.Connections;

public class WebServiceTrackingDAO implements com.rtw.autopricing.ftp.zonetickets.dao.services.WebServiceTrackingDAO  {
	
	
	public List<WebServiceTracking> getAllZoneTicketsTrackingByDateandActionType(String fromDate ,String toDate,
			String actionType,String webConfigId) throws Exception{
		
		String sql = "select * from web_service_tracking where hitting_date between '"+fromDate+"' and '"+toDate+"'"  ;
		
		if(!actionType.equals("ALL")){
			sql = sql + " and action_type='"+actionType+"'";
		}
		if(!webConfigId.equals("ALL")){
			sql = sql + " and web_config_id='"+webConfigId+"'";
		}
		sql = sql + " order by hitting_date asc";
		
		ResultSet resultSet =null;
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<WebServiceTracking> trackings = new ArrayList<WebServiceTracking>();
			WebServiceTracking tracking = null;
			while(resultSet.next()){
				tracking = new WebServiceTracking();
				tracking.setActionProcess(null !=resultSet.getString("action_process")?resultSet.getString("action_process"):"");
				tracking.setActionResult(null !=resultSet.getString("action_result")?resultSet.getString("action_result"):"");
				tracking.setActionType(WebServiceActionType.valueOf(resultSet.getString("action_type")));
				tracking.setAuthenticatedHit(null !=resultSet.getString("authenticated_hit")?resultSet.getString("authenticated_hit"):"");
				tracking.setCustomerId(null !=resultSet.getString("customer_id")?resultSet.getString("customer_id"):"");
				tracking.setCustomerIpAddress(null !=resultSet.getString("cusomter_ip_address")?resultSet.getString("cusomter_ip_address"):"");
				tracking.setEventGpsSearch(null !=resultSet.getString("event_gps_search")?resultSet.getString("event_gps_search"):"");
				tracking.setEventId(null !=resultSet.getString("event_id")?resultSet.getString("event_id"):"");
				tracking.setEventSearchValue(null !=resultSet.getString("event_search_value")?resultSet.getString("event_search_value"):"");
				tracking.setHittingDateStr(null !=resultSet.getString("hitting_date")?resultSet.getString("hitting_date"):"");
				tracking.setId(resultSet.getInt("id"));
				tracking.setMethodName(null !=resultSet.getString("hitting_method_name")?resultSet.getString("hitting_method_name"):"");
				tracking.setOrderId(null !=resultSet.getString("order_id")?resultSet.getString("order_id"):"");
				tracking.setOrderSoldPrice(null !=resultSet.getString("order_sold_price")?resultSet.getString("order_sold_price"):"");
				tracking.setOrderTicketGroupId(null !=resultSet.getString("order_ticket_group_id")?resultSet.getString("order_ticket_group_id"):"");
				tracking.setOrderTicketGroupQty(null !=resultSet.getString("order_ticket_group_qty")?resultSet.getString("order_ticket_group_qty"):"");
				tracking.setServerIpAddress(null !=resultSet.getString("server_ip_address")?resultSet.getString("server_ip_address"):"");
				tracking.setUrl(null !=resultSet.getString("hitting_url")?resultSet.getString("hitting_url"):"");
				tracking.setWebConfigId(null !=resultSet.getString("web_config_id")?resultSet.getString("web_config_id"):"");
				tracking.setZoneTicketInfoPage(null !=resultSet.getString("zone_ticket_page_info")?resultSet.getString("zone_ticket_page_info"):"");
				tracking.setMethodDescription(TrackingUtils.getActionDescription(tracking.getActionType()));
				trackings.add(tracking);
			}
			return trackings;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	
	public List<WebServiceTracking> getAllZoneTicketsOrdersTrackingByDate(String fromDate ,String toDate,String webConfigId) {
		 
		 String sql ="select user_Order_id,CONVERT(VARCHAR(19),order_date,101) as orderDate,order_total,transaction_id,payment_menthod,original_order_total," +
		 		"total_deducted_amount,user_discount_amount,used_loyalty_amount,user_promotinal_offer,CONVERT(VARCHAR(19),delivery_date,101) as deliveryDate," +
		 		"o.customer_id,o.event_id,event_name,CONVERT(VARCHAR(19),event_date,101) as eventDate," +
		 		"CASE WHEN event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), event_time, 100), 7)) " +
		 		" WHEN event_time is null THEN 'TBD' END as eventTime,CONVERT(VARCHAR(150),REPLACE(building,',','')) as venueBuilding," +
		 		" city,state,country,postal_code,ticket_GRoup_id,category,price,qty,section_range,row_Range,shipping_method,loyalty_reward_earned, " +
		 		" app_platform,credit_card_type ,c.first_name,c.last_name,c.email,t.cusomter_ip_address,t.web_config_id,t.hitting_date" +
		 		" from user_order_vw o WITH(NOLOCK) inner join customer c WITH(NOLOCK) " +
		 		" on o.customer_id=c.id inner join web_service_tracking t on t.order_id=o.user_Order_id " +
		 		" where t.action_result='Success' and t.action_type='CREATEORDER' and t.order_id is not null and t.order_id != '' " +
		 		" and t.hitting_date between '"+fromDate+"' and '"+toDate+"' ";
		 
		if(!webConfigId.equals("ALL")){
				sql = sql + " and web_config_id='"+webConfigId+"'";
		}
		sql = sql + " order by hitting_date asc";
		 
		List<WebServiceTracking> webServiceTrackings = new ArrayList<WebServiceTracking>();
		ResultSet resultSet =null;
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			UserOrder userOrder = null;
			WebServiceTracking serviceTracking = null;
			while(resultSet.next()){
				serviceTracking = new WebServiceTracking();
				serviceTracking.setCustomerIpAddress(resultSet.getString("cusomter_ip_address"));
				serviceTracking.setWebConfigId(resultSet.getString("web_config_id"));
				serviceTracking.setHittingDateStr(resultSet.getString("hitting_date"));
				userOrder = new UserOrder();
				userOrder.setId(resultSet.getInt("user_Order_id"));
				userOrder.setWsOrderDate(resultSet.getString("orderDate"));
				userOrder.setOrderTotal(TicketUtil.getRoundedValue(resultSet.getBigDecimal("order_total").doubleValue()));
				userOrder.setTransactionId(resultSet.getString("transaction_id"));
				userOrder.setPaymentMethod(resultSet.getString("payment_menthod"));
				userOrder.setOriginalOrderTotal(TicketUtil.getRoundedValue(resultSet.getBigDecimal("original_order_total").doubleValue()));
				userOrder.setTotalDeductedAmount(TicketUtil.getRoundedValue(resultSet.getBigDecimal("total_deducted_amount").doubleValue()));
				userOrder.setDiscountAmount(TicketUtil.getRoundedValue(resultSet.getBigDecimal("user_discount_amount").doubleValue()));
				userOrder.setLoyaltySpent(TicketUtil.getRoundedValue(resultSet.getBigDecimal("used_loyalty_amount").doubleValue()));
				userOrder.setPromotionalOffer(TicketUtil.getRoundedValue(resultSet.getBigDecimal("user_promotinal_offer").doubleValue()));
				userOrder.setWsDeliveryDate(resultSet.getString("deliveryDate"));
				userOrder.setCustomerId(resultSet.getInt("customer_id"));	
				
				Customer customer = new Customer();
				customer.setFirstName(resultSet.getString("first_name"));
				customer.setLastName(resultSet.getString("last_name"));
				customer.setEmail(resultSet.getString("email"));
				userOrder.setCustomer(customer);
				
				ZoneEvent zoneEvent = new ZoneEvent();
				zoneEvent.setEventId(resultSet.getInt("event_id"));
				zoneEvent.setEventName(resultSet.getString("event_name"));
				zoneEvent.setEventDate(resultSet.getString("eventDate"));
				zoneEvent.setEventTime(resultSet.getString("eventTime"));
				zoneEvent.setVenueName(resultSet.getString("venueBuilding"));
				zoneEvent.setVenueCity(resultSet.getString("city"));
				zoneEvent.setVenueState(resultSet.getString("state"));
				zoneEvent.setVenueCountry(resultSet.getString("country"));
				zoneEvent.setVenueZipCode(resultSet.getString("postal_code"));
				
				TicketGroup ticketGroup = new TicketGroup();
				ticketGroup.setEventId(resultSet.getInt("event_id"));
				ticketGroup.setId(Integer.parseInt(resultSet.getString("ticket_GRoup_id")));
				String zone = resultSet.getString("category");
				Integer qty = resultSet.getInt("qty");
				ticketGroup.setZone(zone);
				ticketGroup.setUnitPrice(resultSet.getDouble("price"));
				ticketGroup.setQuantity(qty);
				
				//String sectionRange = resultSet.getString("section_range");
				//String rowRange = resultSet.getString("row_Range");
				ticketGroup.setTicketDeliveryType(resultSet.getString("shipping_method"));
				userOrder.setAppPlatform(resultSet.getString("app_platform"));
				
				if(null !=resultSet.getString("credit_card_type")){
					userOrder.setCreditCardType(resultSet.getString("credit_card_type"));
				}
				
				if(userOrder.getPaymentMethod().equals("CREDITCARD")){
					userOrder.setPaymentMethod(userOrder.getPaymentMethod()+" ("+userOrder.getCreditCardType()+")");
				}
				
				if(null == userOrder.getDiscountAmount() || userOrder.getDiscountAmount() == 0){
					userOrder.setDiscountAmount(0.00);
				}
				
				Double unitDiscount = userOrder.getDiscountAmount() / qty;
				ticketGroup.setUnitPrice(ticketGroup.getUnitPrice() - unitDiscount);
				Double totalPrice =ticketGroup.getUnitPrice() * ticketGroup.getQuantity();
				ticketGroup.setTotal(TicketUtil.getRoundedValue(totalPrice));
				
				userOrder.setOrderDate(formatter.parse(userOrder.getWsOrderDate()));
				userOrder.setDeliveryDate(formatter.parse(userOrder.getWsDeliveryDate()));
				userOrder.setZoneEvent(zoneEvent);
				userOrder.setTicketGroup(ticketGroup);
				serviceTracking.setUserOrder(userOrder);
				webServiceTrackings.add(serviceTracking);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			
		}
		return webServiceTrackings;
	 }
	
	
 public List<WebServiceTracking> getAllZoneTicketsEventsTrackingByDate(String fromDate ,String toDate,String webConfigId,String gpsSearch) throws Exception{
		
		String sql = "select * from web_service_tracking where hitting_date between '"+fromDate+"' and '"+toDate+"' " +
				" and action_type= 'GENERALISEDSEARCH' and  action_result = 'Success' "  ;
		
		if(!webConfigId.equals("ALL")){
			sql = sql + " and web_config_id='"+webConfigId+"'";
		}
		if(!gpsSearch.equals("ALL")){
			sql = sql + " and event_gps_search='"+gpsSearch+"'";
		}
		sql = sql + " order by hitting_date asc";
		
		ResultSet resultSet =null;
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<WebServiceTracking> trackings = new ArrayList<WebServiceTracking>();
			WebServiceTracking tracking = null;
			while(resultSet.next()){
				tracking = new WebServiceTracking();
				tracking.setActionProcess(null !=resultSet.getString("action_process")?resultSet.getString("action_process"):"");
				tracking.setActionResult(null !=resultSet.getString("action_result")?resultSet.getString("action_result"):"");
				tracking.setActionType(WebServiceActionType.valueOf(resultSet.getString("action_type")));
				tracking.setAuthenticatedHit(null !=resultSet.getString("authenticated_hit")?resultSet.getString("authenticated_hit"):"");
				tracking.setCustomerId(null !=resultSet.getString("customer_id")?resultSet.getString("customer_id"):"");
				tracking.setCustomerIpAddress(null !=resultSet.getString("cusomter_ip_address")?resultSet.getString("cusomter_ip_address"):"");
				tracking.setEventGpsSearch(null !=resultSet.getString("event_gps_search")?resultSet.getString("event_gps_search"):"");
				tracking.setEventId(null !=resultSet.getString("event_id")?resultSet.getString("event_id"):"");
				tracking.setEventSearchValue(null !=resultSet.getString("event_search_value")?resultSet.getString("event_search_value"):"");
				tracking.setHittingDateStr(null !=resultSet.getString("hitting_date")?resultSet.getString("hitting_date"):"");
				tracking.setId(resultSet.getInt("id"));
				tracking.setServerIpAddress(null !=resultSet.getString("server_ip_address")?resultSet.getString("server_ip_address"):"");
				tracking.setUrl(null !=resultSet.getString("hitting_url")?resultSet.getString("hitting_url"):"");
				tracking.setWebConfigId(null !=resultSet.getString("web_config_id")?resultSet.getString("web_config_id"):"");
				trackings.add(tracking);
			}
			return trackings;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}

 public List<WebServiceTracking> getAllZoneTicketsTicketTrackingByDate(String fromDate ,String toDate,String webConfigId) throws Exception{
	 
	 
	 String sql = "select  t.cusomter_ip_address,t.web_config_id,t.action_type,t.action_result,t.event_id,hitting_date" +
	 		" ,e.id as eventId,REPLACE(e.name,',','') as eventName," +
	 		"v.country as venueCountry ,v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
	 		"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
	 		"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
	 		"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding from " +
	 		" web_service_tracking t left join event e WITH(NOLOCK) on e.id=t.event_id " +
	 		" left join venue v WITH(NOLOCK) on v.id=e.venue_id  where action_type='TICKET' and t.action_result='Success'" +
	 		" and hitting_date between '"+fromDate+"' and '"+toDate+"'  "  ;
	 
	 if(!webConfigId.equals("ALL")){
			sql = sql + " and web_config_id='"+webConfigId+"'";
	 }
	 sql = sql + " order by hitting_date asc";
	
	ResultSet resultSet =null;
	try {
		Connection connection = Connections.getZoneTicketsConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		resultSet = statement.executeQuery();
		List<WebServiceTracking> trackings = new ArrayList<WebServiceTracking>();
		WebServiceTracking tracking = null;
		while(resultSet.next()){
			tracking = new WebServiceTracking();
			tracking.setActionResult(null !=resultSet.getString("action_result")?resultSet.getString("action_result"):"");
			tracking.setActionType(WebServiceActionType.valueOf(resultSet.getString("action_type")));
			tracking.setCustomerIpAddress(null !=resultSet.getString("cusomter_ip_address")?resultSet.getString("cusomter_ip_address"):"");
			tracking.setHittingDateStr(null !=resultSet.getString("hitting_date")?resultSet.getString("hitting_date"):"");
			tracking.setId(resultSet.getInt("id"));
			tracking.setWebConfigId(null !=resultSet.getString("web_config_id")?resultSet.getString("web_config_id"):"");
			tracking.setEventId(null !=resultSet.getString("event_id")?resultSet.getString("event_id"):"");
			ZoneEvent zoneEvent = new ZoneEvent();
			//zoneEvent.setEventId(resultSet.getInt("eventId "));
			zoneEvent.setEventName(null !=resultSet.getString("eventName")?resultSet.getString("eventName"):"");
			zoneEvent.setEventDate(null !=resultSet.getString("eventDate")?resultSet.getString("eventDate"):"");
			zoneEvent.setEventTime(null !=resultSet.getString("eventTime")?resultSet.getString("eventTime"):"");
			zoneEvent.setVenueName(null !=resultSet.getString("venueBuilding")?resultSet.getString("venueBuilding"):"");
			zoneEvent.setVenueCity(null !=resultSet.getString("venueCity")?resultSet.getString("venueCity"):"");
			zoneEvent.setVenueState(null !=resultSet.getString("venueState")?resultSet.getString("venueState"):"");
			zoneEvent.setVenueCountry(null !=resultSet.getString("venueCountry")?resultSet.getString("venueCountry"):"");
			zoneEvent.setVenueZipCode(null !=resultSet.getString("postalCode")?resultSet.getString("postalCode"):"");
			tracking.setZoneEvent(zoneEvent);
			trackings.add(tracking);
		}
		return trackings;
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{

	}
	return null;
}


 public List<WebConfigInfo> getAllWebConfigInfo() throws Exception{
	
	String sql = "select id,config_id,description from webservice_config"  ;
	ResultSet resultSet =null;
	try {
		Connection connection = Connections.getZoneTicketsConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		resultSet = statement.executeQuery();
		List<WebConfigInfo> trackings = new ArrayList<WebConfigInfo>();
		WebConfigInfo tracking = null;
		while(resultSet.next()){
			tracking = new WebConfigInfo();
			tracking.setId(resultSet.getInt("id"));
			tracking.setConfigId(null !=resultSet.getString("config_id")?resultSet.getString("config_id"):"");
			tracking.setDescription(null !=resultSet.getString("description")?resultSet.getString("description"):"");
			trackings.add(tracking);
		}
		return trackings;
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{

	}
	return null;
}
}
