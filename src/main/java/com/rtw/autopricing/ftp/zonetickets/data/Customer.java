package com.rtw.autopricing.ftp.zonetickets.data;

import java.io.Serializable;

/**
 * represents customer entity
 * @author Ulaganathan
 *
 */

public class Customer  implements Serializable{
	
	private Integer id;
	private  String userName;
	private  String email;
	private  String firstName;
	private  String lastName;
	
	private  String	phone;
	
	private  String	signupDate;	
	
	private String isNewUser;
	
	public Customer(String firstName,String lastName,String email){
		this.firstName =firstName;
		this.lastName =lastName;
		this.email =email;
		
	}
	
	public Customer(){
		
	}
	

	/**
	 * 
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return userName
	 */
	public String getUserName() {
		if(null == userName || userName.isEmpty()){
			userName="";
		}
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * 
	 * @return email
	 */
	public String getEmail() {
		if(null == email || email.isEmpty()){
			email="";
		}
		return email;
	}
	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return firstName
	 */
	public String getFirstName() {
		if(null == firstName || firstName.isEmpty()){
			firstName="";
		}
		return firstName;
	}
	/**
	 * 
	 * @param firstName ,First Name to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * 
	 * @return lastName
	 */
	public String getLastName() {
		if(null == lastName || lastName.isEmpty()){
			lastName="";
		}
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * 
	 * @return phone
	 */
	public String getPhone() {
		if(null == phone ){
			phone = "";
		}
		return phone;
	}
	/**
	 * 
	 * @param phone , Phone no to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 
	 * @return signupDate
	 */
	public String getSignupDate() {
		if(null == signupDate ){
			signupDate = "";
		}
		return signupDate;
	}
	/**
	 * 
	 * @param signupDate , Sign-up Date to set
	 */
	public void setSignupDate(String signupDate) {
		this.signupDate = signupDate;
	}

	public String getIsNewUser() {
		if(null == isNewUser || isNewUser.isEmpty()){
			isNewUser="";
		}
		return isNewUser;
	}

	public void setIsNewUser(String isNewUser) {
		this.isNewUser = isNewUser;
	}
	
	
	
	
}
