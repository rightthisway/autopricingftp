package com.rtw.autopricing.ftp.zonetickets.dao.services;



import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.UserOrder;
import com.rtw.autopricing.ftp.zonetickets.data.WebServiceTracking;

public interface UserOrderDAO {
	
	public List<UserOrder> getUserOrdersByOrderId(Integer orderId)  throws Exception;
}
