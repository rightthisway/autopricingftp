package com.rtw.autopricing.ftp.zonetickets.dao.services;



import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsAdditionalMarkupAudit;

public interface ZoneTicketsAdditionalMarkupAuditDAO {
	
	public List<ZoneTicketsAdditionalMarkupAudit> getAllZoneTicketsAdditionalMarkupAuditByEventIdandZone(Integer eventId,String zone) throws Exception;
	public int saveOrUpdateAll(List<ZoneTicketsAdditionalMarkupAudit> ztAdditionalMarkupAudits) throws Exception;
}
