package com.rtw.autopricing.ftp.zonetickets.dao.services;



import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsAdditionalMarkup;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsEvent;

public interface ZoneTicketsAdditionalMarkupDAO {
	
	public List<ZoneTicketsAdditionalMarkup> getAllZoneTicketsAdditionalMarkupByValues(Integer artistId,Integer venueId,Integer grandChindCategoryId) throws Exception ;
	public List<ZoneTicketsAdditionalMarkup> getAllZoneTicketsAdditionalMarkupByEventId(Integer eventId) throws Exception;
	public int saveOrUpdateAll(List<ZoneTicketsAdditionalMarkup> ztAdditionalMarkups) throws Exception;
	public int deleteAll(List<ZoneTicketsAdditionalMarkup> zoneTicketAdditionalMarkups) throws Exception;
}
