package com.rtw.autopricing.ftp.zonetickets.data;

import java.io.Serializable;


public class WebConfigInfo implements Serializable{
	private Integer id;
	private String configId;
	private String description;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getConfigId() {
		return configId;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	
}
