package com.rtw.autopricing.ftp.zonetickets.data;

import java.io.Serializable;
import java.util.Date;


public class ZoneTicketsAdditionalMarkupAudit implements Serializable {

	private Integer id;
	private Integer eventId;
	private String zone;
	private Double additionalMarkup;
	private Date lastupdatedDate;
	private String lastupdatedBy;
	private String action;
	
	public ZoneTicketsAdditionalMarkupAudit(ZoneTicketsAdditionalMarkup ztAdditionalMarkup) {
		this.eventId=ztAdditionalMarkup.getEventId();
		this.zone=ztAdditionalMarkup.getZone();
		this.additionalMarkup=ztAdditionalMarkup.getAdditionalMarkup();
	}
	public ZoneTicketsAdditionalMarkupAudit() {
		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Double getAdditionalMarkup() {
		return additionalMarkup;
	}
	public void setAdditionalMarkup(Double additionalMarkup) {
		this.additionalMarkup = additionalMarkup;
	}
	public Date getLastupdatedDate() {
		return lastupdatedDate;
	}
	public void setLastupdatedDate(Date lastupdatedDate) {
		this.lastupdatedDate = lastupdatedDate;
	}
	public String getLastupdatedBy() {
		return lastupdatedBy;
	}
	public void setLastupdatedBy(String lastupdatedBy) {
		this.lastupdatedBy = lastupdatedBy;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
	}
