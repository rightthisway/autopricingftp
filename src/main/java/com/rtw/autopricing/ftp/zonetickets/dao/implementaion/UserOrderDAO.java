package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.Customer;
import com.rtw.autopricing.ftp.zonetickets.data.TicketGroup;
import com.rtw.autopricing.ftp.zonetickets.data.TicketUtil;
import com.rtw.autopricing.ftp.zonetickets.data.UserOrder;
import com.rtw.autopricing.ftp.zonetickets.data.WebServiceTracking;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneEvent;
import com.rtw.autopricing.util.connection.Connections;

public class UserOrderDAO implements com.rtw.autopricing.ftp.zonetickets.dao.services.UserOrderDAO  {
	
	
	
	public List<UserOrder> getUserOrdersByOrderId(Integer orderId) {
		 
		 String sql ="select user_Order_id,CONVERT(VARCHAR(19),order_date,101) as orderDate,order_total,transaction_id,payment_menthod,original_order_total," +
		 		"total_deducted_amount,user_discount_amount,used_loyalty_amount,user_promotinal_offer,CONVERT(VARCHAR(19),delivery_date,101) as deliveryDate," +
		 		"customer_id,event_id,event_name,CONVERT(VARCHAR(19),event_date,101) as eventDate," +
		 		"CASE WHEN event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), event_time, 100), 7)) " +
		 		" WHEN event_time is null THEN 'TBD' END as eventTime,CONVERT(VARCHAR(150),REPLACE(building,',','')) as venueBuilding," +
		 		" city,state,country,postal_code,ticket_GRoup_id,category,price,qty,section_range,row_Range,shipping_method,loyalty_reward_earned, " +
		 		" app_platform,credit_card_type ,c.first_name,c.last_name,c.email from user_order_vw o WITH(NOLOCK)  inner join customer c WITH(NOLOCK) " +
		 		" on o.customer_id=c.id where user_Order_id="+orderId;
		 
		List<UserOrder> userOrders = new ArrayList<UserOrder>();
		ResultSet resultSet =null;
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			UserOrder userOrder = null;
			while(resultSet.next()){
				userOrder = new UserOrder();
				userOrder.setId(resultSet.getInt("user_Order_id"));
				userOrder.setWsOrderDate(resultSet.getString("orderDate"));
				userOrder.setOrderTotal(TicketUtil.getRoundedValue(resultSet.getBigDecimal("order_total").doubleValue()));
				userOrder.setTransactionId(resultSet.getString("transaction_id"));
				userOrder.setPaymentMethod(resultSet.getString("payment_menthod"));
				userOrder.setOriginalOrderTotal(TicketUtil.getRoundedValue(resultSet.getBigDecimal("original_order_total").doubleValue()));
				userOrder.setTotalDeductedAmount(TicketUtil.getRoundedValue(resultSet.getBigDecimal("total_deducted_amount").doubleValue()));
				userOrder.setDiscountAmount(TicketUtil.getRoundedValue(resultSet.getBigDecimal("user_discount_amount").doubleValue()));
				userOrder.setLoyaltySpent(TicketUtil.getRoundedValue(resultSet.getBigDecimal("used_loyalty_amount").doubleValue()));
				userOrder.setPromotionalOffer(TicketUtil.getRoundedValue(resultSet.getBigDecimal("user_promotinal_offer").doubleValue()));
				userOrder.setWsDeliveryDate(resultSet.getString("deliveryDate"));
				userOrder.setCustomerId(resultSet.getInt("customer_id"));	
				
				Customer customer = new Customer();
				customer.setFirstName(resultSet.getString("first_name"));
				customer.setLastName(resultSet.getString("last_name"));
				customer.setEmail(resultSet.getString("email"));
				userOrder.setCustomer(customer);
				
				ZoneEvent zoneEvent = new ZoneEvent();
				zoneEvent.setEventId(resultSet.getInt("event_id"));
				zoneEvent.setEventName(resultSet.getString("event_name"));
				zoneEvent.setEventDate(resultSet.getString("eventDate"));
				zoneEvent.setEventTime(resultSet.getString("eventTime"));
				zoneEvent.setVenueName(resultSet.getString("venueBuilding"));
				zoneEvent.setVenueCity(resultSet.getString("city"));
				zoneEvent.setVenueState(resultSet.getString("state"));
				zoneEvent.setVenueCountry(resultSet.getString("country"));
				zoneEvent.setVenueZipCode(resultSet.getString("postal_code"));
				
				TicketGroup ticketGroup = new TicketGroup();
				ticketGroup.setEventId(resultSet.getInt("id"));
				ticketGroup.setId(Integer.parseInt(resultSet.getString("ticket_GRoup_id")));
				String zone = resultSet.getString("category");
				Integer qty = resultSet.getInt("qty");
				ticketGroup.setZone(zone);
				ticketGroup.setUnitPrice(resultSet.getDouble("price"));
				ticketGroup.setQuantity(qty);
				
				//String sectionRange = resultSet.getString("section_range");
				//String rowRange = resultSet.getString("row_Range");
				ticketGroup.setTicketDeliveryType(resultSet.getString("shipping_method"));
				userOrder.setAppPlatform(resultSet.getString("app_platform"));
				
				if(null !=resultSet.getString("credit_card_type")){
					userOrder.setCreditCardType(resultSet.getString("credit_card_type"));
				}
				
				if(userOrder.getPaymentMethod().equals("CREDITCARD")){
					userOrder.setPaymentMethod(userOrder.getPaymentMethod()+" ("+userOrder.getCreditCardType()+")");
				}
				
				if(null == userOrder.getDiscountAmount() || userOrder.getDiscountAmount() == 0){
					userOrder.setDiscountAmount(0.00);
				}
				
				Double unitDiscount = userOrder.getDiscountAmount() / qty;
				ticketGroup.setUnitPrice(ticketGroup.getUnitPrice() - unitDiscount);
				Double totalPrice =ticketGroup.getUnitPrice() * ticketGroup.getQuantity();
				ticketGroup.setTotal(TicketUtil.getRoundedValue(totalPrice));
				
				userOrder.setOrderDate(formatter.parse(userOrder.getWsOrderDate()));
				userOrder.setDeliveryDate(formatter.parse(userOrder.getWsDeliveryDate()));
				userOrder.setZoneEvent(zoneEvent);
				userOrder.setTicketGroup(ticketGroup);
				userOrders.add(userOrder);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			
		}
		return userOrders;
	 }
	
	
	
}
