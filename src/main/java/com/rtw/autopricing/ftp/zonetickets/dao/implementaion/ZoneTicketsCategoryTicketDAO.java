package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.indux.data.POSTicketGroup;
import com.rtw.autopricing.ftp.indux.data.PosCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.PosTicket;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsCategoryTicket;
import com.rtw.autopricing.util.connection.Connections;

public class ZoneTicketsCategoryTicketDAO implements com.rtw.autopricing.ftp.zonetickets.dao.services.ZoneTicketsCategoryTicketDAO {
	
	
	public boolean saveAll(List<ZoneTicketsCategoryTicket> ztCategoryTixs) throws Exception {
		
		Connection connection = Connections.getZoneTicketsConnection();  
		connection.setAutoCommit(false);
		
		ResultSet rs= null;
		Statement insertStatement =null;
		insertStatement = connection.createStatement();
		try {
			for(ZoneTicketsCategoryTicket ztCatTix : ztCategoryTixs){	
				String sql = "INSERT INTO category_ticket (category_ticket_group_id,position,username,processed ) " +
				"VALUES ("+ztCatTix.getCategoryTicketGroupId()+","+ztCatTix.getPosition()+",'"+ztCatTix.getUserName()+"',0)";
				insertStatement.addBatch(sql);
			}
			insertStatement.executeBatch();
			connection.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}	
	}
	
	
}
