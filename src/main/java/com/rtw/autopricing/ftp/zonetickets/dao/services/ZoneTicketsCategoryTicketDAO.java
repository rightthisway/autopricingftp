package com.rtw.autopricing.ftp.zonetickets.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsCategoryTicket;

public interface ZoneTicketsCategoryTicketDAO {
	
	public boolean saveAll(List<ZoneTicketsCategoryTicket> ztCategoryTixs) throws Exception;
}
