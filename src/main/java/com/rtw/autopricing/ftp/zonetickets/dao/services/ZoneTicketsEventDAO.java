package com.rtw.autopricing.ftp.zonetickets.dao.services;



import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsEvent;

public interface ZoneTicketsEventDAO {
	
	public List<ZoneTicketsEvent> getAllActiveZoneTicketEvents() throws Exception;
	public List<ZoneTicketsEvent> getAllActiveZoneTicketEventsByValues(Integer artistId,Integer venueId,Integer grandChindCategoryId) throws Exception;
	public ZoneTicketsEvent getZoneTicketEventByEventId(Integer eventId) throws Exception;
}
