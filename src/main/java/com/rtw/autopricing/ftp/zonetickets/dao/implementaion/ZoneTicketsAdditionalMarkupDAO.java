package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsAdditionalMarkup;
import com.rtw.autopricing.util.connection.Connections;

public class ZoneTicketsAdditionalMarkupDAO implements com.rtw.autopricing.ftp.zonetickets.dao.services.ZoneTicketsAdditionalMarkupDAO  {
	
	
	public List<ZoneTicketsAdditionalMarkup> getAllZoneTicketsAdditionalMarkupByValues(Integer artistId,Integer venueId,Integer grandChindCategoryId) throws Exception {
		
		String sql = "SELECT DISTINCT am.id as id,am.event_id as eventId,am.zone as zone,am.additional_markup as additionalMarkup," +
				" am.last_updated_date as lastupdatedDate,am.last_updated_by as lastupdatedBy,am.status as status" +
				" from zone_tickets_additional_markup am " +
				" inner join event e on e.id=am.event_id" +
				" left join artist a on a.id=e.artist_id" +
				//" left join venue v on v.id=e.venue_id" +
				" where e.status=1 ";
		
		if(venueId != null) {
			sql = sql + " and e.venue_id="+venueId;
		}
		if(artistId != null) {
			sql = sql + " and e.artist_id="+artistId;
		}
		if(grandChindCategoryId != null) {
			sql = sql + " and a.grand_child_category_id="+grandChindCategoryId;
		}
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<ZoneTicketsAdditionalMarkup> additionalMarkups = new ArrayList<ZoneTicketsAdditionalMarkup>();
			while(resultSet.next()){
				ZoneTicketsAdditionalMarkup additionalMarkup = new ZoneTicketsAdditionalMarkup();
				additionalMarkup.setId(resultSet.getInt("id"));
				//event.setTmatEventId(resultSet.getInt("tmat_event_id"));
				additionalMarkup.setEventId(resultSet.getInt("eventId"));
				additionalMarkup.setZone(resultSet.getString("zone"));
				additionalMarkup.setAdditionalMarkup(resultSet.getDouble("additionalMarkup"));
				additionalMarkup.setLastupdatedDate(resultSet.getDate("lastupdatedDate"));
				additionalMarkup.setLastupdatedBy(resultSet.getString("lastupdatedBy"));
				additionalMarkup.setStatus(resultSet.getString("status"));
				
				additionalMarkups.add(additionalMarkup);
				
			}
			return additionalMarkups;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	public List<ZoneTicketsAdditionalMarkup> getAllZoneTicketsAdditionalMarkupByEventId(Integer eventId) throws Exception {
		
		String sql = "SELECT DISTINCT am.id as id,am.event_id as eventId,am.zone as zone,am.additional_markup as additionalMarkup," +
				" am.last_updated_date as lastupdatedDate,am.last_updated_by as lastupdatedBy,am.status as status" +
				" from zone_tickets_additional_markup am " +
				" where am.event_id= "+eventId;
		
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<ZoneTicketsAdditionalMarkup> additionalMarkups = new ArrayList<ZoneTicketsAdditionalMarkup>();
			while(resultSet.next()){
				ZoneTicketsAdditionalMarkup additionalMarkup = new ZoneTicketsAdditionalMarkup();
				additionalMarkup.setId(resultSet.getInt("id"));
				//event.setTmatEventId(resultSet.getInt("tmat_event_id"));
				additionalMarkup.setEventId(resultSet.getInt("eventId"));
				additionalMarkup.setZone(resultSet.getString("zone"));
				additionalMarkup.setAdditionalMarkup(resultSet.getDouble("additionalMarkup"));
				additionalMarkup.setLastupdatedDate(resultSet.getDate("lastupdatedDate"));
				additionalMarkup.setLastupdatedBy(resultSet.getString("lastupdatedBy"));
				additionalMarkup.setStatus(resultSet.getString("status"));
				
				additionalMarkups.add(additionalMarkup);
				
			}
			return additionalMarkups;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}

	public int saveOrUpdateAll(List<ZoneTicketsAdditionalMarkup> ztAdditionalMarkups) throws Exception{
		Connection connection = Connections.getZoneTicketsConnection();
		
		String insertSql = "INSERT INTO zone_tickets_additional_markup(event_id,zone,additional_markup,last_updated_date,last_updated_by,status)" +
					"values(?,?,?,?,?,?)";
		String updateSql = "update zone_tickets_additional_markup set event_id=?,zone=?,additional_markup=?,last_updated_date=?,last_updated_by=?,status=? where id=?";
		int[] insertResult ;
		int[] updateResult ;
		try{
			connection.setAutoCommit(false);
			PreparedStatement insertStatement = connection.prepareStatement(insertSql);
			PreparedStatement updateStatement = connection.prepareStatement(updateSql);
			int i =0;
			int u=0;
			java.util.Date now = new java.util.Date();
			for(ZoneTicketsAdditionalMarkup ztAdditionalMarkup: ztAdditionalMarkups){
				if(ztAdditionalMarkup.getId() != null){
					u++;
					updateStatement.setInt(1, ztAdditionalMarkup.getEventId());
					updateStatement.setString(2, ztAdditionalMarkup.getZone());
					updateStatement.setDouble(3, ztAdditionalMarkup.getAdditionalMarkup());
					updateStatement.setTimestamp(4, new Timestamp(ztAdditionalMarkup.getLastupdatedDate().getTime()));
					updateStatement.setString(5, ztAdditionalMarkup.getLastupdatedBy());
					updateStatement.setString(6, ztAdditionalMarkup.getStatus());
					updateStatement.setInt(7, ztAdditionalMarkup.getId());
						
					updateStatement.addBatch();
				} else {
					i++;
					insertStatement.setInt(1, ztAdditionalMarkup.getEventId());
					insertStatement.setString(2, ztAdditionalMarkup.getZone());
					insertStatement.setDouble(3, ztAdditionalMarkup.getAdditionalMarkup());
					insertStatement.setTimestamp(4, new Timestamp(ztAdditionalMarkup.getLastupdatedDate().getTime()));
					insertStatement.setString(5, ztAdditionalMarkup.getLastupdatedBy());
					insertStatement.setString(6, ztAdditionalMarkup.getStatus());
					insertStatement.addBatch();
				}
				
			}
			int result=0;
			if(i>0){
				insertResult = insertStatement.executeBatch();
				result += insertResult.length;
			}
			if(u>0){
				updateResult = updateStatement.executeBatch();
				result+= updateResult.length;
			}
			connection.commit();
			return result ;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
		}finally {
//			connection.close();
		}
		
		return 0;
	}
	
	public int deleteAll(List<ZoneTicketsAdditionalMarkup> zoneTicketAdditionalMarkups) throws Exception {
		String deleteSql = "DELETE FROM zone_tickets_additional_markup WHERE id = ? " ;
		Connection connection = Connections.getZoneTicketsConnection();
		PreparedStatement deleteStatement;
		int[] deleteResult ;
		try {
			connection.setAutoCommit(false);
			deleteStatement = connection.prepareStatement(deleteSql);
			for(ZoneTicketsAdditionalMarkup zoneTicketAdditionalMarkup : zoneTicketAdditionalMarkups){
				deleteStatement.setInt(1, zoneTicketAdditionalMarkup.getId());
				deleteStatement.addBatch();
			}
			deleteResult = deleteStatement.executeBatch();
			connection.commit();
			return deleteResult.length;
		} catch (SQLException e) {
			connection.rollback();
		}finally{
//			connection.close();
		}
		return 0;
	}
	
}
