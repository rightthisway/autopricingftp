package com.rtw.autopricing.ftp.zonetickets.data;

import java.io.Serializable;
import java.util.Date;


public class ZoneTicketsAdditionalMarkup implements Serializable {

	private Integer id;
	private Integer eventId;
	private String zone;
	private Double additionalMarkup;
	private Date lastupdatedDate;
	private String lastupdatedBy;
	private String status;
	private ZoneTicketsEvent ztEvent; 
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Double getAdditionalMarkup() {
		return additionalMarkup;
	}
	public void setAdditionalMarkup(Double additionalMarkup) {
		this.additionalMarkup = additionalMarkup;
	}
	public Date getLastupdatedDate() {
		return lastupdatedDate;
	}
	public void setLastupdatedDate(Date lastupdatedDate) {
		this.lastupdatedDate = lastupdatedDate;
	}
	public String getLastupdatedBy() {
		return lastupdatedBy;
	}
	public void setLastupdatedBy(String lastupdatedBy) {
		this.lastupdatedBy = lastupdatedBy;
	}
	public ZoneTicketsEvent getZtEvent() {
		return ztEvent;
	}
	public void setZtEvent(ZoneTicketsEvent ztEvent) {
		this.ztEvent = ztEvent;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	}
