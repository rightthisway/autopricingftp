package com.rtw.autopricing.ftp.zonetickets.data;


import java.io.Serializable;
import java.util.Date;

public class ZoneTicketsTicketGroup implements Serializable {
	
		private Integer id;
		private Integer tmatTicketGroupId;
		private Integer eventId;
		private Integer tmatEventId;
		private String section;
		private Integer quantity;
		private Integer zonePrice;
		private Integer priority;
		private String sectionRange;
		private String rowRange;
		private Integer status;
		private String shippingMethod;
		private Date lastUpdatedDate;
		private Double taxAmount;
		private Double taxPercentage;
		
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Integer getTmatTicketGroupId() {
			return tmatTicketGroupId;
		}
		public void setTmatTicketGroupId(Integer tmatTicketGroupId) {
			this.tmatTicketGroupId = tmatTicketGroupId;
		}
		public Integer getEventId() {
			return eventId;
		}
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		public Integer getTmatEventId() {
			return tmatEventId;
		}
		public void setTmatEventId(Integer tmatEventId) {
			this.tmatEventId = tmatEventId;
		}
		public String getSection() {
			return section;
		}
		public void setSection(String section) {
			this.section = section;
		}
		public Integer getQuantity() {
			return quantity;
		}
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}
		public Integer getZonePrice() {
			return zonePrice;
		}
		public void setZonePrice(Integer zonePrice) {
			this.zonePrice = zonePrice;
		}
		public Integer getPriority() {
			return priority;
		}
		public void setPriority(Integer priority) {
			this.priority = priority;
		}
		public String getSectionRange() {
			return sectionRange;
		}
		public void setSectionRange(String sectionRange) {
			this.sectionRange = sectionRange;
		}
		public String getRowRange() {
			return rowRange;
		}
		public void setRowRange(String rowRange) {
			this.rowRange = rowRange;
		}
		public Integer getStatus() {
			return status;
		}
		public void setStatus(Integer status) {
			this.status = status;
		}
		public String getShippingMethod() {
			return shippingMethod;
		}
		public void setShippingMethod(String shippingMethod) {
			this.shippingMethod = shippingMethod;
		}
		public Date getLastUpdatedDate() {
			return lastUpdatedDate;
		}
		public void setLastUpdatedDate(Date lastUpdatedDate) {
			this.lastUpdatedDate = lastUpdatedDate;
		}
		public Double getTaxAmount() {
			return taxAmount;
		}
		public void setTaxAmount(Double taxAmount) {
			this.taxAmount = taxAmount;
		}
		public Double getTaxPercentage() {
			return taxPercentage;
		}
		public void setTaxPercentage(Double taxPercentage) {
			this.taxPercentage = taxPercentage;
		}
		
		
			
	}