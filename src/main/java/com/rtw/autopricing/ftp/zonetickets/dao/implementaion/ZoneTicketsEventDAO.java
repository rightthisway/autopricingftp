package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsEvent;
import com.rtw.autopricing.util.connection.Connections;

public class ZoneTicketsEventDAO implements com.rtw.autopricing.ftp.zonetickets.dao.services.ZoneTicketsEventDAO  {
	
	
	public List<ZoneTicketsEvent> getAllActiveZoneTicketEvents() throws Exception {
		
		String sql = "SELECT DISTINCT e.id,event_date,name,venue_id,event_time,venue_category_id,artist_id from event e with(nolock) " +
				" inner join venue v on v.id=e.venue_id" +
				" where (event_datetime is null or event_datetime >= getdate()) and v.country in ('USA','CA','US')" +
				" ";//where reward_thefan_enabled = 1
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<ZoneTicketsEvent> events = new ArrayList<ZoneTicketsEvent>();
			while(resultSet.next()){
				ZoneTicketsEvent event = new ZoneTicketsEvent();
				event.setId(resultSet.getInt("id"));
				//event.setTmatEventId(resultSet.getInt("tmat_event_id"));
				event.setEventName(resultSet.getString("name"));
				event.setVenueId(resultSet.getInt("venue_id"));
				event.setEventDate(resultSet.getDate("event_date"));
				event.setEventTime(resultSet.getTime("event_time"));
				event.setVenueCategoryId(resultSet.getInt("venue_category_id"));
				event.setArtistId(resultSet.getInt("artist_id"));
				
				events.add(event);
				
			}
			return events;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	public ZoneTicketsEvent getZoneTicketEventById(Integer eventId) throws Exception {
		
		String sql = "SELECT DISTINCT id,event_date,name,venue_id,event_time,venue_category_id,artist_id from event with(nolock) " +
				" where (event_datetime is null or event_datetime >= getdate()) and id=" +eventId+
				" ";//where reward_thefan_enabled = 1
		ResultSet resultSet =null;
		ZoneTicketsEvent event = null;
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				event = new ZoneTicketsEvent();
				event.setId(resultSet.getInt("id"));
				//event.setTmatEventId(resultSet.getInt("tmat_event_id"));
				event.setEventName(resultSet.getString("name"));
				event.setVenueId(resultSet.getInt("venue_id"));
				event.setEventDate(resultSet.getDate("event_date"));
				event.setEventTime(resultSet.getTime("event_time"));
				event.setVenueCategoryId(resultSet.getInt("venue_category_id"));
				event.setArtistId(resultSet.getInt("artist_id"));
			}
			return event;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return event;
	}
	public List<ZoneTicketsEvent> getAllActiveEvents() throws Exception {
		
		String sql = "SELECT DISTINCT e.id,event_date,name,venue_id,event_time,venue_category_id,artist_id from event e with(nolock)" +
				" inner join venue v on v.id=e.venue_id" +
				" where status = 1 and (event_datetime is null or event_datetime >= getdate()) and v.country in ('USA','CA','US') ";
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<ZoneTicketsEvent> events = new ArrayList<ZoneTicketsEvent>();
			while(resultSet.next()){
				ZoneTicketsEvent event = new ZoneTicketsEvent();
				event.setId(resultSet.getInt("id"));
				//event.setTmatEventId(resultSet.getInt("tmat_event_id"));
				event.setEventName(resultSet.getString("name"));
				event.setVenueId(resultSet.getInt("venue_id"));
				event.setEventDate(resultSet.getDate("event_date"));
				event.setEventTime(resultSet.getTime("event_time"));
				event.setVenueCategoryId(resultSet.getInt("venue_category_id"));
				event.setArtistId(resultSet.getInt("artist_id"));
				
				events.add(event);
				
			}
			return events;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
public List<ZoneTicketsEvent> getAllActivePresaleZoneTicketEvents() throws Exception {
		
		String sql = "SELECT DISTINCT id,event_date,name,venue_id,event_time,venue_category_id,artist_id from event with(nolock) " +
				"where presale_zonetickets_enabled=1 and (event_datetime is null or event_datetime >= getdate())";
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<ZoneTicketsEvent> events = new ArrayList<ZoneTicketsEvent>();
			while(resultSet.next()){
				ZoneTicketsEvent event = new ZoneTicketsEvent();
				event.setId(resultSet.getInt("id"));
				//event.setTmatEventId(resultSet.getInt("tmat_event_id"));
				event.setEventName(resultSet.getString("name"));
				event.setVenueId(resultSet.getInt("venue_id"));
				event.setEventDate(resultSet.getDate("event_date"));
				event.setEventTime(resultSet.getTime("event_time"));
				event.setVenueCategoryId(resultSet.getInt("venue_category_id"));
				event.setArtistId(resultSet.getInt("artist_id"));
				
				events.add(event);
				
			}
			return events;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	public ZoneTicketsEvent getZoneTicketEventByEventId(Integer eventId) throws Exception {
		
		String sql = "SELECT DISTINCT e.id as id,e.name as eventName,e.venue_id as venueId,e.event_date as eventDate,e.event_time as eventTime," +
				" e.venue_category_id as venueCategoryId,e.artist_id as artistId," +
				" v.building as building,v.city as city,v.state as state,v.country as country " +
				" from event e with(nolock) " +
				" left join venue v with(nolock) on v.id=e.venue_id" +
				" where reward_thefan_enabled = 1 and  e.status=1 and e.id= "+eventId;
		
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			ZoneTicketsEvent event = new ZoneTicketsEvent();
			while(resultSet.next()){
				
				event.setId(resultSet.getInt("id"));
				//event.setTmatEventId(resultSet.getInt("tmat_event_id"));
				event.setEventName(resultSet.getString("eventName"));
				event.setVenueId(resultSet.getInt("venueId"));
				event.setEventDate(resultSet.getDate("eventDate"));
				event.setEventTime(resultSet.getTime("eventTime"));
				event.setVenueCategoryId(resultSet.getInt("venueCategoryId"));
				event.setArtistId(resultSet.getInt("artistId"));
				
				if(resultSet.getString("building") != null) {
					String venueName = resultSet.getString("building")+","+resultSet.getString("city")+","+resultSet.getString("state")+","+resultSet.getString("country");
					event.setVenueName(venueName);
				}
				
			}
			return event;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}

	public List<ZoneTicketsEvent> getAllActiveZoneTicketEventsByValues(Integer artistId,Integer venueId,Integer grandChindCategoryId) throws Exception {
		
		String sql = "SELECT DISTINCT e.id as id,e.name as eventName,e.venue_id as venueId,e.event_date as eventDate,e.event_time as eventTime," +
				" e.venue_category_id as venueCategoryId,e.artist_id as artistId," +
				" v.building as building,v.city as city,v.state as state,v.country as country " +
				" from event e with(nolock) " +
				" left join artist a with(nolock) on a.id=e.artist_id" +
				" left join venue v with(nolock) on v.id=e.venue_id" +
				" where e.status=1 and e.reward_thefan_enabled = 1 ";
		
		if(venueId != null) {
			sql = sql + " and e.venue_id="+venueId;
		}
		if(artistId != null) {
			sql = sql + " and e.artist_id="+artistId;
		}
		if(grandChindCategoryId != null) {
			sql = sql + " and a.grand_child_category_id="+grandChindCategoryId;
		}
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<ZoneTicketsEvent> events = new ArrayList<ZoneTicketsEvent>();
			while(resultSet.next()){
				ZoneTicketsEvent event = new ZoneTicketsEvent();
				event.setId(resultSet.getInt("id"));
				//event.setTmatEventId(resultSet.getInt("tmat_event_id"));
				event.setEventName(resultSet.getString("eventName"));
				event.setVenueId(resultSet.getInt("venueId"));
				event.setEventDate(resultSet.getDate("eventDate"));
				event.setEventTime(resultSet.getTime("eventTime"));
				event.setVenueCategoryId(resultSet.getInt("venueCategoryId"));
				event.setArtistId(resultSet.getInt("artistId"));
				
				if(resultSet.getString("building") != null) {
					String venueName = resultSet.getString("building")+","+resultSet.getString("city")+","+resultSet.getString("state")+","+resultSet.getString("country");
					event.setVenueName(venueName);
				}
				events.add(event);
				
			}
			return events;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	public boolean createNewZoneTicektEvent(Integer eventId) throws Exception {
		 
		Connection connection = Connections.getZoneTicketsConnection();
		 CallableStatement cs = null;
		 try{
			 connection.setAutoCommit(false);
		     cs = connection.prepareCall("{call SP_load_all_data_for_eventID(?)}");
		     cs.setInt(1, eventId);
	         boolean flag = cs.execute();
	         connection.commit();
	         return flag;
		 }catch(Exception e){
			 //e.printStackTrace();
			 //return false;
			 connection.rollback();
			 throw e;
		 } finally{
				if(cs!=null){
					cs.close();
				}
			}
	 }
	public static void main(String[] args) {
		 
		 CallableStatement cs = null;
		 try{
			 System.out.println("Start : "+new Date());
			 Connection connection = Connections.getZoneTicketsConnection();
			 boolean flag = new ZoneTicketsEventDAO().createNewZoneTicektEvent(1);
			 System.out.println("end : "+new Date());
		 }catch(Exception e){
			 e.printStackTrace();
			 //return false;
		 }
	 }
	
}
