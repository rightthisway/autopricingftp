package com.rtw.autopricing.ftp.zonetickets.dao.services;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;
import com.rtw.autopricing.util.CategoryTicket;




public interface ZoneTicketsTicketGroupDAO {
	
	public Integer save(CategoryTicket catTicket,AutopricingProduct product) throws Exception;
	public Integer updateCategoryTicketGroup(CategoryTicket catTicket) throws Exception;
	public Integer updateByCategoryTicketGroups(List<CategoryTicket> catTixList) throws Exception;
	public Integer deleteByCategoryTicketGroup(Integer zoneTicketsTicketGroupId) throws Exception;
	public Integer deleteByCategoryTicketGroups(List<CategoryTicket> catTixList) throws Exception;
	public List<Integer> getZoneTicketsSoldTicketGroupIds(String fromDateStr,String toDateStr) throws Exception;
	public Integer deleteByZoneTicketGroupIds(List<Integer> zoneTicketGroupIds) throws Exception;
	public Set<Integer> getAllActiveLockedTicketEventIds() throws Exception;
	public void deleteCategoryTicketGroupsByEventId(Integer eventId) throws Exception;
	public List<Integer> getEventIdsToUnbroadcastEventsFromRewardTheFan(Date now, Date before15MinsFromNow) throws Exception;
	 
}
