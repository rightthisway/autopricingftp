package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosCategoryTicket;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsCategoryTicket;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsEvent;
import com.rtw.autopricing.util.BrokerUtils;
import com.rtw.autopricing.util.CategoryTicket;
import com.rtw.autopricing.util.connection.Connections;


public class ZoneTicketsTicketGroupDAO implements com.rtw.autopricing.ftp.zonetickets.dao.services.ZoneTicketsTicketGroupDAO {
	DateFormat df = new SimpleDateFormat("yyyy -MM-dd HH:mm:ss");
	
  public Integer save(CategoryTicket catTicket,AutopricingProduct product) throws Exception{
		
		Connection connection = Connections.getZoneTicketsConnection();  
		String row=null;
		String productType=null;
		String sectionRange=null;
		String rowRange=null;
		String actualSection=null;
		String section = null;
		
		if(product.getInternalNotes().equals("ZTP")) {
			actualSection=null;
			section = catTicket.getSection();
			row = null;
			productType="REWARDTHEFAN";
			sectionRange = catTicket.getSectionRange();
			rowRange = catTicket.getRowRange();
			
			
		} else if(product.getInternalNotes().equals("RTFLR")) {
			actualSection = catTicket.getSection();
			section = catTicket.getTmatZone();
			row = catTicket.getLastRow();
			productType="RTF_LASTROWCATS";
			sectionRange = null;
			rowRange = null;
			
		}
		
		String sql = "insert into category_ticket_group(event_id,actual_section,section,row,section_range,row_range,quantity,price,product_type,status," +
				" shipping_method,created_date,created_by,last_updated," +
				" seat_low,seat_high,face_price,cost,wholesale_price,retail_price,broadcast,internal_notes,tax_percentage,broker_id,tmat_price)" +//tax_amount
				" VALUES ("+catTicket.getZoneTicketsEventId()+",";
				
		sql = sql + (actualSection!=null?("'"+actualSection+"',"):("null,"));
		sql = sql + (section!=null?("'"+section+"',"):("null,"));
		sql = sql + (row!=null?("'"+row+"',"):("null,"));
		sql = sql + (sectionRange!=null?("'"+sectionRange+"',"):("null,")) ;
		sql = sql + (rowRange!=null?("'"+rowRange+"',"):("null,"));
								
		sql = sql +	catTicket.getQuantity() +","+catTicket.getZoneTicketPrice() +",'"+productType+"','ACTIVE',"+
				"'"+catTicket.getTicketDeliveryType()+"',getdate(),'AUTO',getdate()," +
				" null,null,0,0,"+catTicket.getZoneTicketPrice()+","+catTicket.getZoneTicketPrice() +",1,'"+product.getInternalNotes()+"'," +
						""+catTicket.getTaxPercentage()+","+BrokerUtils.RTF_EXCHANGE_BROKER_ID+","+catTicket.getTmatPrice() +//"+catTicket.getTaxAmount()+",
				//" getdate())";
				" )";

		ResultSet rs= null;
		Statement insertStatement =null;
		try {
			connection.setAutoCommit(false);
			insertStatement = connection.createStatement();
			insertStatement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
			rs = insertStatement.getGeneratedKeys();
			Integer id =null;
			if(rs.next()){
				id = rs.getInt(1);
			}
			connection.commit();
			
			List<ZoneTicketsCategoryTicket> catTixs = new ArrayList<ZoneTicketsCategoryTicket>();
			for(int i =1;i<=catTicket.getQuantity();i++){
				ZoneTicketsCategoryTicket tix = new ZoneTicketsCategoryTicket();
				tix.setCategoryTicketGroupId(id.intValue());
				tix.setPosition(i);
				tix.setUserName("AUTO");
				catTixs.add(tix);
			}
			if(!ZoneTicketDAORegistry.getZoneTicketsCategoryTicketDAO().saveAll(catTixs)){
				throw new Exception("Error while adding category tickets..");
			}
			return id;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}
		//return null;
	}
  
  public Integer updateCategoryTicketGroup(CategoryTicket catTicket) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();  
		String sql = "UPDATE ctg set ctg.price=?,ctg.retail_price=?,ctg.wholesale_price=?,ctg.section_range=?,ctg.row_range=?," +
				" ctg.shipping_method=?,ctg.last_updated=?,ctg.tax_percentage=?,ctg.tmat_price=? from category_ticket_group ctg where ctg.id=? and ctg.status='ACTIVE'" +//ctg.tax_amount=?,
				" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
		try {
			PreparedStatement updateStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			updateStatement.setDouble(1, catTicket.getZoneTicketPrice());
			updateStatement.setDouble(2, catTicket.getZoneTicketPrice());
			updateStatement.setDouble(3, catTicket.getZoneTicketPrice());
			updateStatement.setString(4, catTicket.getSectionRange());
			updateStatement.setString(5, catTicket.getRowRange());
			updateStatement.setString(6, catTicket.getTicketDeliveryType());
			updateStatement.setTimestamp(7, new Timestamp(catTicket.getLastUpdated().getTime()));
			//updateStatement.setDouble(8, catTicket.getTaxAmount());
			updateStatement.setDouble(8, catTicket.getTaxPercentage());
			updateStatement.setDouble(9, catTicket.getTmatPrice());
			updateStatement.setInt(10, catTicket.getZoneTicketsTicketGroupId());
			updateStatement.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}finally{
		}
		return 1;
	}
  
  public Integer updateByCategoryTicketGroups(List<CategoryTicket> catTixList) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();   
	  String sql = "UPDATE ctg set ctg.price=?,ctg.retail_price=?,ctg.wholesale_price=?,ctg.section_range=?,ctg.row_range=?," +
		" ctg.shipping_method=?,ctg.last_updated=?,ctg.tax_percentage=?,ctg.tmat_price=? from category_ticket_group ctg where ctg.id=? and ctg.status='ACTIVE'" +//ctg.tax_amount=?,
		" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
		
		int[] updateTicketGroupResult ;
		try {
			PreparedStatement updateStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			int u=0;
			for(CategoryTicket catTicket: catTixList){
				u++;
				updateStatement.setDouble(1, catTicket.getZoneTicketPrice());
				updateStatement.setDouble(2, catTicket.getZoneTicketPrice());
				updateStatement.setDouble(3, catTicket.getZoneTicketPrice());
				updateStatement.setString(4, catTicket.getSectionRange());
				updateStatement.setString(5, catTicket.getRowRange());
				updateStatement.setString(6, catTicket.getTicketDeliveryType());
				updateStatement.setTimestamp(7, new Timestamp(catTicket.getLastUpdated().getTime()));
				//updateStatement.setDouble(8, catTicket.getTaxAmount());
				updateStatement.setDouble(8, catTicket.getTaxPercentage());
				updateStatement.setDouble(9, catTicket.getTmatPrice());
				updateStatement.setInt(10, catTicket.getZoneTicketsTicketGroupId());
				updateStatement.addBatch();
			}
			int result=0;
			if(u>0){
				updateTicketGroupResult = updateStatement.executeBatch();
				result+= updateTicketGroupResult.length;
			}
			
			connection.commit();
			
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
		}
		return 0;
	}
  
  public Integer deleteByCategoryTicketGroup(Integer zoneTicketsTicketGroupId) throws Exception{
	  Connection connection = Connections.getZoneTicketsConnection();  
		String sql = "DELETE ctg FROM category_ticket_group ctg where ctg.id=? and ctg.status='ACTIVE' " +
				" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
		String catTicketSql = "DELETE ct FROM category_ticket ct inner join category_ticket_group ctg on ctg.id=ct.category_ticket_group_id" +
  		" where ctg.status='ACTIVE' and ct.invoice_id is null and ct.category_ticket_group_id=?" +
  		" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
		try {
			connection.setAutoCommit(false);

			PreparedStatement catTicketDeleteStatement = connection.prepareStatement(catTicketSql);
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			
			catTicketDeleteStatement.setInt(1, zoneTicketsTicketGroupId);
			catTicketDeleteStatement.executeUpdate();
			
			deleteStatement.setInt(1, zoneTicketsTicketGroupId);
			deleteStatement.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		return 1;
	}
  
  public Integer deleteByCategoryTicketGroups(List<CategoryTicket> catTixList) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();   
	  String sql = "DELETE ctg FROM category_ticket_group ctg where ctg.id=? and ctg.status='ACTIVE' " +
		" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
	  
	  String catTicketSql = "DELETE ct FROM category_ticket ct inner join category_ticket_group ctg on ctg.id=ct.category_ticket_group_id" +
		" where ctg.status='ACTIVE' and ct.invoice_id is null and ct.category_ticket_group_id=?" +
		" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
		
		int[] updateTicketGroupResult ;
		try {
			PreparedStatement catTicketDeleteStatement = connection.prepareStatement(catTicketSql);
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			int u=0;
			for(CategoryTicket catTicket: catTixList){
				if(catTicket.getZoneTicketsTicketGroupId() != null) {
					u++;
					catTicketDeleteStatement.setInt(1, catTicket.getZoneTicketsTicketGroupId());
					catTicketDeleteStatement.addBatch();
					
					deleteStatement.setInt(1, catTicket.getZoneTicketsTicketGroupId());
					deleteStatement.addBatch();
					
				}
			}
			int result=0;
			if(u>0){
				catTicketDeleteStatement.executeBatch();
				
				updateTicketGroupResult = deleteStatement.executeBatch();
				result+= updateTicketGroupResult.length;
			}
			
			connection.commit();
			
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
		}
	}
  
  public Integer deleteByZoneTicketGroupIds(List<Integer> zoneTicketGroupIds) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();   
	  String sql = "DELETE ctg FROM category_ticket_group ctg where ctg.id=? and ctg.status='ACTIVE' " +
		" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
	  
	  String catTicketSql = "DELETE ct FROM category_ticket ct inner join category_ticket_group ctg on ctg.id=ct.category_ticket_group_id" +
	  		" where ctg.status='ACTIVE' and ct.invoice_id is null and ct.category_ticket_group_id=?" +
	  		" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
		
		int[] updateTicketGroupResult ;
		try {
			PreparedStatement catTicketDeleteStatement = connection.prepareStatement(catTicketSql);
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			int u=0;
			for(Integer zoneTicketGroupId : zoneTicketGroupIds){
				u++;
				catTicketDeleteStatement.setInt(1, zoneTicketGroupId);
				catTicketDeleteStatement.addBatch();
				
				deleteStatement.setInt(1, zoneTicketGroupId);
				deleteStatement.addBatch();
			}
			int result=0;
			if(u>0){
				catTicketDeleteStatement.executeBatch();
				
				updateTicketGroupResult = deleteStatement.executeBatch();
				result+= updateTicketGroupResult.length;
			}
			
			connection.commit();
			
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
		}
		//return 0;
	}
  
  public void deleteCategoryTicketGroupsByEventId(Integer eventId) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();   
	  String sql = "DELETE ctg FROM category_ticket_group ctg where ctg.event_id=? and ctg.broker_id="+BrokerUtils.RTF_EXCHANGE_BROKER_ID+" and ctg.status='ACTIVE' " +
		" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
	  
	  String catTicketSql = "DELETE ct FROM category_ticket ct " +
	  		" inner join category_ticket_group ctg on ctg.id=ct.category_ticket_group_id" +
	  		" where ctg.event_id=? and ctg.status='ACTIVE' and ctg.broker_id="+BrokerUtils.RTF_EXCHANGE_BROKER_ID+" " +
	  		" and ct.invoice_id is null and ctg.id not in (select lt.category_ticket_group_id from autocats_locked_tickets lt with(nolock) where lock_status = 'ACTIVE') ";
	  
		try {
			
			connection.setAutoCommit(false);
			PreparedStatement cateTicketGroupDeleteStatement = connection.prepareStatement(sql);
			PreparedStatement catTicketDeleteStatement = connection.prepareStatement(catTicketSql);
			
			catTicketDeleteStatement.setLong(1, eventId);
			catTicketDeleteStatement.executeUpdate();
			
			cateTicketGroupDeleteStatement.setInt(1, eventId);
			cateTicketGroupDeleteStatement.executeUpdate();
			
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
		}
	}
  public void deleteAutoPricingCategoryTicketGroupsOnlyByEventId(Integer eventId,String internalNotesStr) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();   
	  String sql = "DELETE ctg FROM category_ticket_group ctg where ctg.event_id=? and ctg.status='ACTIVE' and internal_notes in ("+internalNotesStr+") and ctg.broker_id="+BrokerUtils.RTF_EXCHANGE_BROKER_ID+"" +
		" and not exists (select * from autocats_locked_tickets lt with(nolock) where lt.category_ticket_group_id = ctg.id  and lock_status = 'ACTIVE')";
	  
	  String catTicketSql = "DELETE ct FROM category_ticket ct " +
	  		" inner join category_ticket_group ctg on ctg.id=ct.category_ticket_group_id" +
	  		" where ctg.event_id=? and internal_notes in ("+internalNotesStr+") and ctg.status='ACTIVE' and ctg.broker_id="+BrokerUtils.RTF_EXCHANGE_BROKER_ID+"" +
	  		" and ct.invoice_id is null and ctg.id not in (select lt.category_ticket_group_id from autocats_locked_tickets lt with(nolock) where lock_status = 'ACTIVE') ";
	  
		try {
			
			connection.setAutoCommit(false);
			PreparedStatement cateTicketGroupDeleteStatement = connection.prepareStatement(sql);
			PreparedStatement catTicketDeleteStatement = connection.prepareStatement(catTicketSql);
			
			catTicketDeleteStatement.setLong(1, eventId);
			catTicketDeleteStatement.executeUpdate();
			
			cateTicketGroupDeleteStatement.setInt(1, eventId);
			cateTicketGroupDeleteStatement.executeUpdate();
			
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
		}
	}
  
	public List<Integer> getZoneTicketsSoldTicketGroupIds(String fromDateStr,String toDateStr) throws Exception {
		// Method gets sold zone ticket group from zones db..
		Connection connection = Connections.getZoneTicketsConnection();   
		String sql = "select distinct id from category_ticket_group " +
				" where status='SOLD' and last_updated>='"+fromDateStr+"' and last_updated<='"+toDateStr+"' ";
				
		ResultSet resultSet =null;
		List<Integer> zoneTicketsTicketGroupIds = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				zoneTicketsTicketGroupIds.add(resultSet.getInt("id"));
			}
			return zoneTicketsTicketGroupIds;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		return zoneTicketsTicketGroupIds;
	}
	
	
	public Set<Integer> getAllActiveLockedTicketEventIds() throws Exception{
		
		String sql = "select eventId as eventId from autocats_locked_tickets " +
				" where lock_status='ACTIVE'";
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			Set<Integer> eventIds = new HashSet<Integer>();
			while(resultSet.next()) {
				eventIds.add(resultSet.getInt("eventId"));
			}
			return eventIds;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	public List<Integer> getEventIdsToUnbroadcastEventsFromRewardTheFan(Date now, Date before15MinsFromNow) throws Exception {
		
		String sql = "select event_id from customer_order co with(nolock)" +
				" inner join event e with(nolock) on e.id=co.event_id" +
				" where product_type in ('REWARDTHEFAN','RTF_LASTROWCATS') and secondary_order_type is null and is_long_sale=0" +
				" and created_date>=? and created_date<=?  and e.grand_child_category_id!=16" +
				" and e.name not like '%ICC - International Champions Cup:%' " +//16--NFL and e.name !='pink' and e.name not like '%taylor swift%' 
				" group by event_id having count(*)>2";
		ResultSet resultSet =null;
		List<Integer> eventIds = new ArrayList<Integer>();
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setTimestamp(1, new Timestamp(before15MinsFromNow.getTime()));
			statement.setTimestamp(2, new Timestamp(now.getTime()));
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				eventIds.add(resultSet.getInt("event_id"));
			}
			return eventIds;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	public List<Integer> getEventIdsToUnbroadcastEventsFromSeatGeek(Date now, Date before15MinsFromNow) throws Exception {
		
		String sql = "select event_id from seatgeek_orders co with(nolock)" +
				" where order_date>=? and order_date<=?" +
				" group by event_id having count(*)>2";
		ResultSet resultSet =null;
		List<Integer> eventIds = new ArrayList<Integer>();
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setTimestamp(1, new Timestamp(before15MinsFromNow.getTime()));
			statement.setTimestamp(2, new Timestamp(now.getTime()));
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				eventIds.add(resultSet.getInt("event_id"));
			}
			return eventIds;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	public List<Integer> getRTFSoldCategoryTicketGroupIds(String fromDateStr,String toDateStr,String internalNotesStr) throws Exception {
		
		Connection connection = Connections.getZoneTicketsConnection(); 
		String sql = "select distinct ctg.id as category_ticket_group_id from category_ticket_group ctg  " +
				"  inner join invoice i on i.id=ctg.invoice_id   " +
				" where ctg.internal_notes in("+internalNotesStr+") and i.created_date>='"+fromDateStr+"' and i.created_date<='"+toDateStr+"' ";
				
		ResultSet resultSet =null;
		List<Integer> catTixIdList = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				catTixIdList.add(resultSet.getInt("category_ticket_group_id"));
			}
			return catTixIdList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		//return catTixIdList;
	}
}
