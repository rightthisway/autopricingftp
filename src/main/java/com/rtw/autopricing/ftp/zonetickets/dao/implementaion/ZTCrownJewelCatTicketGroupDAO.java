package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.rtw.autopricing.ftp.data.CrownJewelCategoryTicket;
import com.rtw.autopricing.util.CategoryTicket;
import com.rtw.autopricing.util.connection.Connections;


public class ZTCrownJewelCatTicketGroupDAO implements com.rtw.autopricing.ftp.zonetickets.dao.services.ZTCrownJewelCatTicketGroupDAO {
	DateFormat df = new SimpleDateFormat("yyyy -MM-dd HH:mm:ss");
	
  public Integer save(CrownJewelCategoryTicket catTicket) throws Exception{
		
		Connection connection = Connections.getZoneTicketsConnection();  
		
		String sql = "insert into crownjewel_category_ticket(event_id,section,quantity,zone_price," +
				" section_range,row_range,status,shipping_method,created_date,last_updated)" +
				" VALUES ("+catTicket.getZoneTicketsEventId()+","+//catTicket.getEventId()+"," +
				"'"+catTicket.getSection()+"',"+catTicket.getQuantity() +","+catTicket.getZoneTicketPrice() +
				",'"+catTicket.getSectionRange()+"','"+catTicket.getRowRange()+"','ACTIVE','"+catTicket.getTicketDeliveryType()+"',getdate(),getdate()" +
				//" getdate())";
				" )";

		ResultSet rs= null;
		Statement insertStatement =null;
		try {
			connection.setAutoCommit(false);
			insertStatement = connection.createStatement();
			insertStatement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
			rs = insertStatement.getGeneratedKeys();
			Integer id =null;
			if(rs.next()){
				id = rs.getInt(1);
			}
			connection.commit();
			
			return id;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}
		//return null;
	}
  
  public Integer updateCategoryTicketGroup(CrownJewelCategoryTicket catTicket) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();  
		String sql = "UPDATE ztg set ztg.zone_price=?,ztg.section_range=?,ztg.row_range=?," +
				" ztg.shipping_method=?,ztg.last_updated=? from crownjewel_category_ticket ztg where ztg.id=? and ztg.status='ACTIVE'" +
				"";
		try {
			PreparedStatement updateStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			updateStatement.setDouble(1, catTicket.getZoneTicketPrice());
			updateStatement.setString(2, catTicket.getSectionRange());
			updateStatement.setString(3, catTicket.getRowRange());
			updateStatement.setString(4, catTicket.getTicketDeliveryType());
			updateStatement.setTimestamp(5, new Timestamp(catTicket.getLastUpdated().getTime()));
			updateStatement.setInt(6, catTicket.getZoneTicketsTicketGroupId());
			updateStatement.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}finally{
		}
		return 1;
	}
  
  public Integer updateByCategoryTicketGroups(List<CrownJewelCategoryTicket> catTixList) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();   
	  String sql = "UPDATE ztg set ztg.zone_price=?,ztg.section_range=?,ztg.row_range=?," +
		" ztg.shipping_method=?,ztg.last_updated=? from crownjewel_category_ticket ztg where ztg.id=? and ztg.status='ACTIVE'" +
		" ";
		
		int[] updateTicketGroupResult ;
		try {
			PreparedStatement updateStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			int u=0;
			for(CategoryTicket catTicket: catTixList){
				u++;
				updateStatement.setDouble(1, catTicket.getZoneTicketPrice());
				updateStatement.setString(2, catTicket.getSectionRange());
				updateStatement.setString(3, catTicket.getRowRange());
				updateStatement.setString(4, catTicket.getTicketDeliveryType());
				updateStatement.setTimestamp(5, new Timestamp(catTicket.getLastUpdated().getTime()));
				updateStatement.setInt(6, catTicket.getZoneTicketsTicketGroupId());
				updateStatement.addBatch();
			}
			int result=0;
			if(u>0){
				updateTicketGroupResult = updateStatement.executeBatch();
				result+= updateTicketGroupResult.length;
			}
			
			connection.commit();
			
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
		}
		return 0;
	}
  
  public Integer deleteByCategoryTicketGroup(Integer zoneTicketsTicketGroupId) throws Exception{
	  Connection connection = Connections.getZoneTicketsConnection();  
		String sql = "DELETE ztg FROM crownjewel_category_ticket ztg where ztg.id=? and ztg.status='ACTIVE' " +
				" ";
		try {
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			deleteStatement.setInt(1, zoneTicketsTicketGroupId);
			deleteStatement.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}finally{

		}
		return 1;
	}
  
  public Integer deleteByCategoryTicketGroups(List<CrownJewelCategoryTicket> catTixList) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();   
	  String sql = "DELETE ztg FROM crownjewel_category_ticket ztg where ztg.id=? and ztg.status='ACTIVE' " +
	  		" ";
		
		int[] updateTicketGroupResult ;
		try {
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			int u=0;
			for(CrownJewelCategoryTicket catTicket: catTixList){
				if(catTicket.getZoneTicketsTicketGroupId() != null) {
					u++;
					deleteStatement.setInt(1, catTicket.getZoneTicketsTicketGroupId());
					deleteStatement.addBatch();
				}
			}
			int result=0;
			if(u>0){
				updateTicketGroupResult = deleteStatement.executeBatch();
				result+= updateTicketGroupResult.length;
			}
			
			connection.commit();
			
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
		}
		return 0;
	}
  
  public Integer deleteByZoneTicketGroupIds(List<Integer> zoneTicketGroupIds) throws Exception{
		
	  Connection connection = Connections.getZoneTicketsConnection();   
	  String sql = "DELETE ztg FROM crownjewel_category_ticket ztg where ztg.id=? and ztg.status='ACTIVE' " +
	  		" ";
		
		int[] updateTicketGroupResult ;
		try {
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			int u=0;
			for(Integer zoneTicketGroupId : zoneTicketGroupIds){
				u++;
				deleteStatement.setInt(1, zoneTicketGroupId);
				deleteStatement.addBatch();
			}
			int result=0;
			if(u>0){
				updateTicketGroupResult = deleteStatement.executeBatch();
				result+= updateTicketGroupResult.length;
			}
			
			connection.commit();
			
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
		}
		return 0;
	}
  
	public List<Integer> getZoneTicketsSoldTicketGroupIds(String fromDateStr,String toDateStr) throws Exception {
		// Method gets sold zone ticket group from zones db..
		Connection connection = Connections.getZoneTicketsConnection();   
		String sql = "select distinct id from crownjewel_category_ticket " +
				" where status='SOLD' and last_updated>='"+fromDateStr+"' and last_updated<='"+toDateStr+"' ";
				
		ResultSet resultSet =null;
		List<Integer> zoneTicketsTicketGroupIds = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				zoneTicketsTicketGroupIds.add(resultSet.getInt("id"));
			}
			return zoneTicketsTicketGroupIds;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		return zoneTicketsTicketGroupIds;
	}
	
	
	public Set<Integer> getAllActiveLockedTicketEventIds() throws Exception{
		
		String sql = "select eventId as eventId from autocats_locked_tickets with(nolock) " +
				" where lock_status='ACTIVE'";
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getZoneTicketsConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			Set<Integer> eventIds = new HashSet<Integer>();
			while(resultSet.next()) {
				eventIds.add(resultSet.getInt("eventId"));
			}
			return eventIds;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
}
