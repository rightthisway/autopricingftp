package com.rtw.autopricing.ftp.zonetickets.data;

public enum Status {
	DEFAULT,ACTIVE,EXPIRED, DELETED,SOLD
}
