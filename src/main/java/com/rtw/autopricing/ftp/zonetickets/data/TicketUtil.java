package com.rtw.autopricing.ftp.zonetickets.data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class TicketUtil {
	private static DecimalFormat decimalFormat = new DecimalFormat("#0.00");
	private static List<String> generalAdmissionList = new ArrayList<String>();; 
	
	static{
		if(generalAdmissionList.size() <=0 ){
			generalAdmissionList.add("GA");
			generalAdmissionList.add("General Admission");
			generalAdmissionList.add("Gen Ad");
			generalAdmissionList.add("General Ad");
			generalAdmissionList.add("General Adm");
			generalAdmissionList.add("GenAd");
		}
	}
	
	public static Double getRoundedValue(Double value) throws Exception {
	
		return Double.valueOf(decimalFormat.format(Double.valueOf(value)));
	}
	
	public static String getRoundedValueString(Double value) throws Exception {
		
		return decimalFormat.format(value);
	}
	
	public static void main(String[] args) throws Exception {
		
		Double loyalty = 2456.35676 * 10 / 100;
		
		/*NumberFormat formatter = new DecimalFormat("#0.00");     
		System.out.println(formatter.format(loyalty));
		
		Double loyaltyDouble = Double.valueOf(formatter.format(loyalty));
		
		System.out.println("loyaltyDouble----->"+loyaltyDouble);*/
		
		
		
		System.out.println(loyalty);
		
		String loyaloytStr = getRoundedValueString(loyalty);
		
		System.out.println(loyaloytStr);
	}
	
	public static Boolean isGeneralAdmissionZone(String sectionRange) throws Exception {
		
		for (String section : generalAdmissionList) {
			if(sectionRange.toUpperCase().contains(section.toUpperCase())){
				return true;
			}
		}
		return false;
	}
	
	
	/*public static TicketGroup getTicketGroupInfo(TicketGroup ticketGroup,DiscountDetails discountDetails,String speedZoneArrivalDate,
			String discountZoneArrivalDate,String instantSpeedZoneArrivalDate,String instantDiscountZoneArrivalDate) throws Exception {
		
		String ticketDeliveryType = "",loyaltyRewardStr="0.00",unitPrice="0.00";
		
		
		if(ticketGroup.getTicketDeliveryType().equals("INSTANT")){
			speedZoneArrivalDate = instantSpeedZoneArrivalDate;
			discountZoneArrivalDate = instantDiscountZoneArrivalDate;
			ticketDeliveryType = "sent by Email";
			ticketGroup.setTicketDeliveryType("Email");
		}else if(ticketGroup.getTicketDeliveryType().equals("EDELIVERY") || ticketGroup.getTicketDeliveryType().equals("ETICKETS") 
				|| ticketGroup.getTicketDeliveryType().equals("E-Ticket")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setTicketDeliveryType("Email");
		}else{
			ticketDeliveryType = "delivered via "+ticketGroup.getTicketDeliveryType();
			ticketGroup.setTicketDeliveryType("FedEx");
		}
		unitPrice =TicketUtil.getRoundedValueString(ticketGroup.getUnitPrice());
		Double loyaltyReward = Math.ceil(ticketGroup.getTotal() * discountDetails.getLoyaltyPerc() / 100);
		//loyaltyRewardStr = TicketUtil.getRoundedValueString(Math.ceil(loyaltyReward));
		//ticketGroup.setZoneStarInfo("Both primary ticket sellers and secondary ticket resellers discount certain zones in the days leading up to the event. Your tickets will be delivered by the day before the event, and we are passing along that discount to you.");
		ticketGroup.setZoneStarInfo("This zone is also being offered by the team, artist, venue or reseller at a discounted price if you are willing to accept delivery on the DAY BEFORE THE EVENT");
		
		ticketGroup.setLoyaltyInfo("You will earn $"+loyaltyReward.intValue()+" in rewards towards next purchase");
		ticketGroup.setTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+speedZoneArrivalDate);
		ticketGroup.setTicketPriceInfo(ticketGroup.getQuantity()+" Ticket(s) @ $"+ticketGroup.getUnitPrice().intValue()+" per Ticket");
		
		Long discountAmt = Math.round(ticketGroup.getTotal() * discountDetails.getZoneDiscountPerc() / 100);
		Double disZoneLoyaltyReward = Math.ceil((ticketGroup.getTotal() - discountAmt) * discountDetails.getLoyaltyPerc() / 100);
		//String discZoneLoyaltyRewardStr = TicketUtil.getRoundedValueString(Math.ceil(disZoneLoyaltyReward));
		ticketGroup.setDiscountZoneLoyaltyInfo("You will earn $"+disZoneLoyaltyReward.intValue()+" in rewards towards next purchase");
		ticketGroup.setDiscountZoneTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+discountZoneArrivalDate+".");
		
		Double unitDiscountAmt = ((ticketGroup.getUnitPrice()*discountDetails.getZoneDiscountPerc())/100); 
		Double discUnitPrice = ticketGroup.getUnitPrice() - Math.round(unitDiscountAmt);
		ticketGroup.setDiscountZoneTicketPriceInfo(ticketGroup.getQuantity()+" Ticket(s) @ $"+discUnitPrice.intValue()+" per Ticket");
		
		ticketGroup.setDiscountZone("Zone "+ticketGroup.getZone()+" Discount");
		ticketGroup.setNormalZone("Zone "+ticketGroup.getZone());
		
		return ticketGroup;
	}
	
	public static ZoneTicketGroup getTicketGroupInfo(ZoneTicketGroup ticketGroup,String arrivalDate,Double loyaltyReward) throws Exception {
		
		String ticketDeliveryType = "";
		
		if(ticketGroup.getShippingMethod().equals("INSTANT")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setShippingMethod("Email");
		}else if(ticketGroup.getShippingMethod().equals("EDELIVERY") || ticketGroup.getShippingMethod().equals("ETICKETS") 
				|| ticketGroup.getShippingMethod().equals("E-Ticket")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setShippingMethod("Email");
		}else{
			ticketDeliveryType = "delivered via "+ticketGroup.getShippingMethod();
			ticketGroup.setShippingMethod("FedEx");
		}
		ticketGroup.setLoyaltyInfo("You will earn $"+loyaltyReward.intValue()+" in rewards towards next purchase");
		ticketGroup.setTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+arrivalDate);
		
		String sectionRange = ticketGroup.getSectionRange();
		String rowRange = ticketGroup.getRowRange();	
		
		sectionRange = sectionRange.replaceAll("-", " thru ");
		rowRange = rowRange.replaceAll("-", " thru ");
		ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+sectionRange+" between row(s) "+rowRange+".");
		boolean isGAZone = TicketUtil.isGeneralAdmissionZone(sectionRange);
		if(isGAZone){
			ticketGroup.setZoneDescription("Tickets will be in section(s) "+sectionRange+" between row(s) "+rowRange+".");
		}
		return ticketGroup;
	}*/
	
}
