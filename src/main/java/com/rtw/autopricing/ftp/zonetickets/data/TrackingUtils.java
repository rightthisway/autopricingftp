package com.rtw.autopricing.ftp.zonetickets.data;



public class TrackingUtils  {
	

	public static String getActionDescription(WebServiceActionType actionType){
		
		String description = "";
		
		switch (actionType) {
			case CHANGEPASSWORD:
				description = "Change Password";
				break;
				
			case CUSTOMEREMAILVALIDATION:
				description = "Customer Email Validation(It is Backend Porcess)";
				break;
				
			case CREATEORDER:
				description = "Order Creation Page";
				break;
				
			case CUSTOMERLOGIN:
				description = "Customer Login";
				break;
				
			case CUSTOMERSIGNUP:
				description = "Customer Signup";
				break;
				
			case EDITCUSTOMER:
				description = "Customer details update";
				break;
				
			case EDITCUSTOMERADDRESS:
				description = "Customer details update";
				break;
				
			case EMAILCUSTOMERREQUEST:
				description = "Customer Request Page";
				break;
				
			case EMAILVENUEMAP:
				description = "Email Venue Map";
				break;
				
			case GENERALISEDSEARCH:
				description = "Event Search";
				break;
				
			case GENERALISEDSEARCHBYGEOLOCATION:
				description = "Event Search by GEO Location";
				break;
				
			case GENERALISEDSEARCHWITHOUTPAGINATION:
				description = "Event Search";
				break;
				
			case GETCITIES:
				description = "Get Available Cities";
				break;
				
			case GETCUSTOMER:
				description = "Customer Information";
				break;
				
			case GETCUSTOMERREWARDS:
				description = "Customer Loyalty Rewards";
				break;
				
			case GETORDERS:
				description = "Customer Order Page";
				break;
				
			case GETPAYPALCREDENTIALS:
				description = "Get Paypal Credentials (Paypal Payment option in Order Page)";
				break;
				
			case GETSERVERTIME:
				description = "Get Server Time (It is Backend call)";
				break;
				
			case GETSOLDTICKETS:
				description = "Get Sold Tickets (It is Backend call)";
				break;
				
			case GETSTATECOUNTRY:
				description = "Get Available State and Country Information";
				break;
				
			case GETTICKETGROUP:
				description = "Ticket Page";
				break;
				
			case GETZONETICKETSINFORMATION:
				description = "Zone Ticket Information Page (About Us , Contact Us ,FAQ , Terms of Use)";
				break;
				
			case MANAGEFAVOURITEEVENTS:
				description = "Favourite Event";
				break;
				
				
			case PAYMENTGATEWAY:
				description = "Payment Gateway";
				break;
				
			case REMOVEZONEEVENT:
				description = "Remove Zone Event";
				break;
				
			case RESETPASSWORD:
				description = "Reset Password";
				break;
				
			case RESETPASSWORDREQUEST:
				description = "Reset Password Request";
				break;
				
			case STRIPETRANSACTION:
				description = "IPay Strip Transaction";
				break;
				
			case TICKET:
				description = "Event Ticket Page";
				break;
				
			case TICKETGROUPLOCKING:
				description = "Ticket View Page";
				break;
				
			case UPDATEORDERSTATUS:
				description = "Update order Status(Only for other payment gateway)";
				break;
				
			case VIEWORDER:
				description = "View Order Page";
				break;
				
			case ZONETICKETSTRACKING:
				description = "Desktop Site About Us or Contact Us or FAQ or Terms of Use Page";
				break;
	
			default:
				description = "";
				break;
		}
		
		
		
		return description;
	}
	
}
