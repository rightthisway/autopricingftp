package com.rtw.autopricing.ftp.zonetickets.data;

import java.io.Serializable;


public class ZoneTicketsCategoryTicket implements Serializable {
	
	private Integer categoryTicketId; 
	private Integer categoryTicketGroupId;
	private Integer position;
	private Double actualPrice;
	private String userName;
	
	public Integer getCategoryTicketId() {
		return categoryTicketId;
	}
	public void setCategoryTicketId(Integer categoryTicketId) {
		this.categoryTicketId = categoryTicketId;
	}
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	

	
}
