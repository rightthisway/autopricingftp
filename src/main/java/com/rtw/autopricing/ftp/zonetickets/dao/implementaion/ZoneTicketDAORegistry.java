package com.rtw.autopricing.ftp.zonetickets.dao.implementaion;
import com.rtw.autopricing.ftp.dao.services.PresaleZoneTicketsCategoryTicketDAO;
import com.rtw.autopricing.ftp.zonetickets.dao.services.PresaleZoneTicketsTicketGroupDAO;
import com.rtw.autopricing.ftp.zonetickets.dao.services.WebServiceTrackingDAO;
import com.rtw.autopricing.ftp.zonetickets.dao.services.ZTCrownJewelCatTicketGroupDAO;
import com.rtw.autopricing.ftp.zonetickets.dao.services.ZoneTicketsAdditionalMarkupAuditDAO;
import com.rtw.autopricing.ftp.zonetickets.dao.services.ZoneTicketsAdditionalMarkupDAO;
import com.rtw.autopricing.ftp.zonetickets.dao.services.ZoneTicketsCategoryTicketDAO;


public class ZoneTicketDAORegistry {
	
	private static WebServiceTrackingDAO webServiceTrackingDAO;
	private static ZoneTicketsEventDAO zoneTicketsEventDAO;
	private static ZoneTicketsTicketGroupDAO zoneTicketsTicketGroupDAO;
	private static PresaleZoneTicketsTicketGroupDAO presaleZoneTicketsTicketGroupDAO;
	private static ZoneTicketsAdditionalMarkupDAO zoneTicketsAdditionalMarkupDAO;
	private static ZoneTicketsAdditionalMarkupAuditDAO zoneTicketsAdditionalMarkupAuditDAO;
	private static ZTCrownJewelCatTicketGroupDAO ztCrownJewelCatTicketGroupDAO;
	private static ZoneTicketsCategoryTicketDAO zoneTicketsCategoryTicketDAO;
	
	
	public final static ZoneTicketsEventDAO getZoneTicketsEventDAO() {
		return zoneTicketsEventDAO;
	}

	public final void setZoneTicketsEventDAO(
			ZoneTicketsEventDAO zoneTicketsEventDAO) {
		ZoneTicketDAORegistry.zoneTicketsEventDAO = zoneTicketsEventDAO;
	}

	public final static ZoneTicketsAdditionalMarkupDAO getZoneTicketsAdditionalMarkupDAO() {
		return zoneTicketsAdditionalMarkupDAO;
	}

	public final void setZoneTicketsAdditionalMarkupDAO(
			ZoneTicketsAdditionalMarkupDAO zoneTicketsAdditionalMarkupDAO) {
		ZoneTicketDAORegistry.zoneTicketsAdditionalMarkupDAO = zoneTicketsAdditionalMarkupDAO;
	}
	public final static ZoneTicketsTicketGroupDAO getZoneTicketsTicketGroupDAO() {
		return zoneTicketsTicketGroupDAO;
	}

	public final void setZoneTicketsTicketGroupDAO(
			ZoneTicketsTicketGroupDAO zoneTicketsTicketGroupDAO) {
		ZoneTicketDAORegistry.zoneTicketsTicketGroupDAO = zoneTicketsTicketGroupDAO;
	}

	public static final WebServiceTrackingDAO getWebServiceTrackingDAO() {
		return webServiceTrackingDAO;
	}

	public final void setWebServiceTrackingDAO(
			WebServiceTrackingDAO webServiceTrackingDAO) {
		ZoneTicketDAORegistry.webServiceTrackingDAO = webServiceTrackingDAO;
	}

	public static final ZoneTicketsAdditionalMarkupAuditDAO getZoneTicketsAdditionalMarkupAuditDAO() {
		return zoneTicketsAdditionalMarkupAuditDAO;
	}

	public final void setZoneTicketsAdditionalMarkupAuditDAO(
			ZoneTicketsAdditionalMarkupAuditDAO zoneTicketsAdditionalMarkupAuditDAO) {
		ZoneTicketDAORegistry.zoneTicketsAdditionalMarkupAuditDAO = zoneTicketsAdditionalMarkupAuditDAO;
	}

	public static final PresaleZoneTicketsTicketGroupDAO getPresaleZoneTicketsTicketGroupDAO() {
		return presaleZoneTicketsTicketGroupDAO;
	}

	public final void setPresaleZoneTicketsTicketGroupDAO(
			PresaleZoneTicketsTicketGroupDAO presaleZoneTicketsTicketGroupDAO) {
		ZoneTicketDAORegistry.presaleZoneTicketsTicketGroupDAO = presaleZoneTicketsTicketGroupDAO;
	}

	public static final ZTCrownJewelCatTicketGroupDAO getZtCrownJewelCatTicketGroupDAO() {
		return ztCrownJewelCatTicketGroupDAO;
	}

	public final void setZtCrownJewelCatTicketGroupDAO(
			ZTCrownJewelCatTicketGroupDAO ztCrownJewelCatTicketGroupDAO) {
		ZoneTicketDAORegistry.ztCrownJewelCatTicketGroupDAO = ztCrownJewelCatTicketGroupDAO;
	}

	public static final ZoneTicketsCategoryTicketDAO getZoneTicketsCategoryTicketDAO() {
		return zoneTicketsCategoryTicketDAO;
	}

	public final void setZoneTicketsCategoryTicketDAO(
			ZoneTicketsCategoryTicketDAO zoneTicketsCategoryTicketDAO) {
		ZoneTicketDAORegistry.zoneTicketsCategoryTicketDAO = zoneTicketsCategoryTicketDAO;
	}

	
	
	
}
