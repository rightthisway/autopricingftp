package com.rtw.autopricing.ftp.security;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.dao.DataAccessException;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Privilege;
import com.rtw.autopricing.ftp.data.Role;
import com.rtw.autopricing.ftp.data.User;


public class AuthenticationService implements UserDetailsService {
//	private PreferenceManager preferenceManager;

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		if (user == null) {
			return null;
		}
		
		
		Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (Role role: user.getRoles()) {
			authorities.add(new GrantedAuthorityImpl(role.getName()));
			for (Privilege privilege: role.getPrivileges()) {
				authorities.add(new GrantedAuthorityImpl(privilege.getName()));
			}
		}
		
		authorities.add(new GrantedAuthorityImpl("PRIV_LOGGED_IN"));

		return new org.springframework.security.userdetails.User(user.getUsername(),
					user.getPassword(), true, true, true, !user.getLocked(), authorities.toArray(new GrantedAuthority[authorities.size()]));
	}

}
