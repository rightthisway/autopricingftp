package com.rtw.autopricing.ftp.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.security.concurrent.SessionInformation;
import org.springframework.security.concurrent.SessionRegistry;


public class SessionListener implements HttpSessionListener {
	private static Map<String, HttpSession> sessionMap = new HashMap<String, HttpSession>();
	private static SessionRegistry sessionRegistry;

	public void sessionCreated(HttpSessionEvent sessionEvent) {
		HttpSession httpSession = sessionEvent.getSession();
		sessionMap.put(httpSession.getId(), httpSession);
	}

	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		HttpSession httpSession = sessionEvent.getSession();
//		User user =(User)httpSession.getAttribute("userObj");
//		String ipAddress = (String)httpSession.getAttribute("ipAddress");
//		Date timeStamp = (Date)httpSession.getAttribute("lastTimeStamp");
//		DAORegistry.getUserActionDAO().logUserAction(user, "/Logout", "",ipAddress,timeStamp);
		sessionMap.remove(httpSession.getId());
	}

	public static boolean isLoggedIn(String username) {
		SessionInformation[] sessionInformations = sessionRegistry.getAllSessions(username, false);
		if (sessionInformations == null) {
			return false;
		}
		
		for(SessionInformation sessionInformation: sessionInformations) {
			if (!sessionInformation.isExpired()) {
				return true;
			}
		}
		
		return false;
	}

	public static void invalidateSession(String sessionId) {
		HttpSession httpSession = sessionMap.get(sessionId);
		if (httpSession == null) {
			return;
		}
		httpSession.invalidate();
		sessionMap.remove(sessionId);
	}

	/**
	 * Invalidate all the sessions belonging to a user.
	 * @param username
	 * @return true if at least a session was invalidated, false otherwise (the user was not logged in)
	 */
	public static boolean invalidateSessionsForUser(String username) {

		SessionInformation[] sessionInformations = sessionRegistry.getAllSessions(username, false);
		if (sessionInformations != null) {
			for(SessionInformation sessionInformation: sessionInformations) {
				sessionInformation.expireNow();
				sessionRegistry.removeSessionInformation(sessionInformation.getSessionId());
				SessionListener.invalidateSession(sessionInformation.getSessionId());
			}
			return true;
		} else {
			return false;
		}

	}

	public void setSessionRegistry(SessionRegistry sessionRegistry) {
		SessionListener.sessionRegistry = sessionRegistry;
	}

}
