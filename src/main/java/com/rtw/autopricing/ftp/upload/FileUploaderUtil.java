package com.rtw.autopricing.ftp.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.FtpFileUploadAudit;
import com.rtw.autopricing.ftp.data.FtpUserFileInformation;
import com.rtw.autopricing.ftp.utils.FTPType;
import com.rtw.autopricing.ftp.utils.ZonesProperty;
import com.rtw.autopricing.util.EmailManager;

public class FileUploaderUtil {
	
	private String host;
	private int port;
	private String username;
	private String password;
	private String subDirDestination;
	private List<FtpUserFileInformation> ftpUserFileInformation;
	private static ZonesProperty zonesProperties ;
	private OutputStream outputStream;
	private static final int BUFFER_SIZE = 4096;
	public FileUploaderUtil(){
		
	}
	
	public FileUploaderUtil(String host, int port, String username, String password,String subDirDestination, List<FtpUserFileInformation> ftpUserFileInformation) {
		super();
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.ftpUserFileInformation=ftpUserFileInformation;
		this.subDirDestination = subDirDestination;
	}
	
	

	public boolean uploadFile()  {
		
		 boolean isUploadStatus= false ;	
		 String subject,fileName,content ="";
		 for (FtpUserFileInformation ftpUserFileInfo : this.ftpUserFileInformation) {
			
			String fileSourcePath=ftpUserFileInfo.getFileSource();
			Map<String, Object> map =  new HashMap<String, Object>();
			
			
			FTPClient ftpClient = new FTPClient();	
			InputStream inputStream = null;
			File originalFile = new File(fileSourcePath);
			FtpFileUploadAudit ftpFileUploadAudit =new FtpFileUploadAudit(host, port, username, password,fileSourcePath, originalFile, subDirDestination, "AUTOMATIC",FTPType.FTP);
			ftpFileUploadAudit.setLastUploadTime(new Date());
			ftpFileUploadAudit.setFileSize(originalFile.length()/1024);
			ftpFileUploadAudit.setDestination("ftp://"+host+"/");
            
		if(originalFile.length()>0){
				
				System.out.println("FTP UTIL : Trying to connect to the host:"+host); 
				try {
	            	if(port == 0){
	            		ftpClient.connect(host);
	            	}else{
	            		ftpClient.connect(host, port);
	            	}
					
					
				} catch (SocketException e) {
					subject = "FTP: File Upload Failed to Host: <b>"+host+"</b> where User Name is: <b>"+username+"</b>";
					content = "File could not be uploaded due to host name is incorrect for the file <b>"+fileSourcePath+"</b>";

					try {
						
						ftpFileUploadAudit.setFailedReason(content);
						ftpFileUploadAudit.setStatus("FAILURE");
						DAORegistry.getFtpFileUploadAuditDAO().saveOrUpdate(ftpFileUploadAudit);
						
					}catch (Exception e1) {
						e1.printStackTrace();
					} 
					System.out.println("FTP UTIL : Failed: "+content);
					try{
					
						map = new HashMap<String, Object>();
						map.put("content", content);
						fileName = "templates/ftp-status-update-message.txt";
						EmailManager.sendEmail(zonesProperties.getToaddressList(), zonesProperties.getCcAddressList(), subject, fileName, map);
						
					}catch(Exception mailException){
						mailException.printStackTrace();
					}
					return false;
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
				
				System.out.println("FTP UTIL : Trying to Login Using User Name:"+username+" and password:"+password);
				
	            try {
	            	
					if(!ftpClient.login(username, password)){
						 throw new IOException();
					}
						
				} catch (IOException e) {

					subject = "FTP: File Upload Failed to Host: <b>"+host+"</b> where User Name is: <b>"+username+"</b>";
					content = "File could not be uploaded due to Authentication failure for file "+
					fileSourcePath + " and host is " + host + " and User Name is "+username;
					
					try {
						
						ftpFileUploadAudit.setFailedReason(content);
						ftpFileUploadAudit.setStatus("FAILURE");
						DAORegistry.getFtpFileUploadAuditDAO().saveOrUpdate(ftpFileUploadAudit);
						
					}catch (Exception e1) {
						e1.printStackTrace();
					}
					System.out.println("FTP UTIL : Failed: "+content);
					try{
					  
						map = new HashMap<String, Object>();
						map.put("content", content);
						fileName = "templates/ftp-status-update-message.txt";
						EmailManager.sendEmail(zonesProperties.getToaddressList(), zonesProperties.getCcAddressList(), subject, fileName, map);
						
					}catch(Exception mailException){
						mailException.printStackTrace();
					}					
					return false;
				}
				
	            ftpClient.enterLocalPassiveMode();
	            
	            try {
					ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
				} catch (IOException e) {
					e.printStackTrace();
				}       
				
				// APPROACH #1: uploads first file using an InputStream
				
				System.out.println("FTP UTIL : fileSourcePath : "+fileSourcePath);
				
				File firstLocalFile = new File(fileSourcePath);
				String firstRemoteFile = firstLocalFile.getName();           
				
				try {
					
					inputStream = new FileInputStream(firstLocalFile);
				
				} catch (FileNotFoundException e) {

					subject = "FTP: File Upload Failed to Host: <b>"+host+"</b> where User Name is: <b>"+username+"</b>";
					content = "File could not be uploaded due to incorrect file path or file not found "
					+ " and host is " + host + " and User Name is "+username;
					
					try {
						
						ftpFileUploadAudit.setFailedReason(content);
						ftpFileUploadAudit.setStatus("FAILURE");
						DAORegistry.getFtpFileUploadAuditDAO().saveOrUpdate(ftpFileUploadAudit);
						
					}catch (Exception e1) {
						e1.printStackTrace();
					}
					System.out.println("FTP UTIL : Failed: "+content);
					try{
					
						map = new HashMap<String, Object>();
						map.put("content", content);
						fileName = "templates/ftp-status-update-message.txt";
						EmailManager.sendEmail(zonesProperties.getToaddressList(), zonesProperties.getCcAddressList(), subject, fileName, map);
						
					}catch(Exception mailException){
						mailException.printStackTrace();
					}
					
					return false;
				}
				
				System.out.println("FTP UTIL : Start uploading first file");           
				
				try {
					
					//isUploadStatus = ftpClient.storeFile(firstRemoteFile, inputStream);
					
					outputStream = ftpClient.storeFileStream(firstLocalFile.getName());
					byte[] buffer = new byte[BUFFER_SIZE];
					int bytesRead = -1;
					long totalBytesRead = 0;
					int percentCompleted = 0;
					long fileSize = firstLocalFile.length();
					
					while ((bytesRead = inputStream.read(buffer)) != -1) {
						writeFileBytes(buffer, 0, bytesRead);
						totalBytesRead += bytesRead;
						percentCompleted = (int) (totalBytesRead * 100 / fileSize);
						//System.out.println("FTP UTIL : % COMPLETED========>"+percentCompleted);
					}
					if(percentCompleted==100){
						isUploadStatus= true;
					}else{
						isUploadStatus= false;
					}
					
					if(!isUploadStatus){
						subject = "FTP: File Upload Failed to Host: <b>"+host+"</b> where User Name is: <b>"+username+"</b>";
						content = "File could not be uploaded: The host is " + host + " and User Name is "+username+ 
						" and Reply Code is :"+ftpClient.getReplyString();
						
						ftpFileUploadAudit.setFailedReason(content);
						ftpFileUploadAudit.setStatus("FAILURE");
						try{
							
							map = new HashMap<String, Object>();
							map.put("content", content);
							fileName = "templates/ftp-status-update-message.txt";
							EmailManager.sendEmail(zonesProperties.getToaddressList(), zonesProperties.getCcAddressList(), subject, fileName, map);
														
						}catch(Exception mailException){
							mailException.printStackTrace();
						}						
					}else{
						
						map = new HashMap<String, Object>();
						subject = "FTP: File Upload Successfully to Host: "+host+" where User Name is:"+username;
						content = "File Upload Successfully to Host: "+host+" where User Name is:"+username+" and File path is :"+fileSourcePath;
						map.put("content", content);
						fileName = "templates/ftp-status-update-message.txt";
						EmailManager.sendEmail(zonesProperties.getToaddressList(), zonesProperties.getCcAddressList(), subject, fileName, map);
						ftpFileUploadAudit.setFailedReason("File Upload Successfully..");
						ftpFileUploadAudit.setStatus("SUCCESS");
						
					}
					DAORegistry.getFtpFileUploadAuditDAO().saveOrUpdate(ftpFileUploadAudit);
					
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}            
            
				try {
					
					inputStream.close();
					
				} catch (IOException e) {
					e.printStackTrace();
				}      
		     }else{
	    	 	subject = "FTP: File Upload Failed to Host: <b>"+host+"</b> where User Name is: <b>"+username+"</b>";
				content = "File could not be uploaded due to empty file "
				+ " and host is " + host + " and User Name is "+username;
				
				try {
					
					ftpFileUploadAudit.setFailedReason(content);
					ftpFileUploadAudit.setStatus("FAILURE");
					DAORegistry.getFtpFileUploadAuditDAO().saveOrUpdate(ftpFileUploadAudit);
					
				}catch (Exception e1) {
					e1.printStackTrace();
				}
				try{
				
					map = new HashMap<String, Object>();
					map.put("content", content);
					fileName = "templates/ftp-status-update-message.txt";
					EmailManager.sendEmail(zonesProperties.getToaddressList(), zonesProperties.getCcAddressList(), subject, fileName, map);
					
				}catch(Exception mailException){
					mailException.printStackTrace();
				}
		     }
		 }
            return isUploadStatus;	
	}
	
	public boolean uploadSFTPFile()  {
		
		 boolean isUploadStatus= true ;	
		 String subject,fileName,content ="";
		 for (FtpUserFileInformation ftpUserFileInfo : this.ftpUserFileInformation) {
			
			String fileSourcePath=ftpUserFileInfo.getFileSource();
			Map<String, Object> map =  new HashMap<String, Object>();
			FtpFileUploadAudit ftpFileUploadAudit =new FtpFileUploadAudit(host, port, username, password,fileSourcePath, null, subDirDestination, "AUTOMATIC",FTPType.SFTP);
			ftpFileUploadAudit.setLastUploadTime(new Date());

			String errorMsg = null;
			try {
				
				File originalFile = new File(fileSourcePath);
				
				ftpFileUploadAudit.setOriginalFile(originalFile);
				ftpFileUploadAudit.setFileSize(originalFile.length()/1024);
				//ftpFileUploadAudit.setDestination("ftp://"+host+"/");
	           
				if(originalFile.length()>0){         
					try {
						errorMsg = FileUploaderUtil.sendSFTPFile(host, port, username, password, subDirDestination, originalFile);
						
					} catch (Exception e) {
						errorMsg = "File could not be uploaded due to Error while Uploading file";
						e.printStackTrace();
					}   
				}else {
					errorMsg = "File could not be uploaded due to empty file";
				}
			} catch(Exception e) {
				errorMsg = "File could not be uploaded due to Error in SFTP Upload";
				e.printStackTrace();
			}
			if(errorMsg != null) {
				subject = "FTP: File Upload Failed to Host: <b>"+host+"</b> where User Name is: <b>"+username+"</b>";
				content = errorMsg + " and host is " + host + " and User Name is "+username;
				try {
					ftpFileUploadAudit.setFailedReason(content);
					ftpFileUploadAudit.setStatus("FAILURE");
					DAORegistry.getFtpFileUploadAuditDAO().saveOrUpdate(ftpFileUploadAudit);
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				try{
					ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
					String toAddress = resourceBundle.getString("emailAODev");
					map = new HashMap<String, Object>();
					map.put("content", content);
					fileName = "templates/ftp-status-update-message.txt";
					EmailManager.sendEmail(toAddress,null, subject, fileName, map);
					
				} catch(Exception mailException){
					mailException.printStackTrace();
				}
				isUploadStatus = false;
				return isUploadStatus;
			} else {
				try {
					ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
					String toAddress = resourceBundle.getString("emailAODev");
					ftpFileUploadAudit.setFailedReason("File Upload Successfully..");
					ftpFileUploadAudit.setStatus("SUCCESS");
					DAORegistry.getFtpFileUploadAuditDAO().saveOrUpdate(ftpFileUploadAudit);
					
					map = new HashMap<String, Object>();
					subject = "FTP: File Upload Successfully to Host: "+host+" where User Name is:"+username;
					content = "File Upload Successfully to Host: "+host+" where User Name is:"+username+" and File path is :"+fileSourcePath;
					map.put("content", content);
					fileName = "templates/ftp-status-update-message.txt";
					//EmailManager.sendEmail(toAddress,null, subject, fileName, map);
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		 }
           return isUploadStatus;	
	}
	
	
	public static String getMailBody(String content){
		
		
		String mailBody ="Hi,\r\n";
		mailBody +="FTP File uploader Status..\r\n";
		mailBody+="\r\n";
		 
		mailBody +=content+"\r\n";
	     
		mailBody+="\r\n";
		mailBody+="\r\n";
		mailBody+="Thank You,\r\n";
		mailBody+="Technical Support Team\r\n";
		mailBody+="RightThisWay.\r\n";
		
		return mailBody;
	}
	/*
	public static void main(String[] args) {
		Map<String, Object> map =  new HashMap<String, Object>();
		String subject = "File Upload Failed";
		String content = "File couldn't not be uploaded due to Authentication failure for file "+
		"C:\\UNCAT\\" + " and host is " + "[::]" + " and User Name is ulaganathan";
		System.out.println("FTP UTIL : Failed: "+content);
		map.put("error", content);
		try{
			FTPMailGenerator.sendMail("kulaganathan@rightthisway.com", "kulaganathan@rightthisway.com", null, subject, getMailBody(content));
		}catch(Exception mailException){
			mailException.printStackTrace();
		}					
	}*/
	
	/**
	 * Write an array of bytes to the output stream.
	 */
	public void writeFileBytes(byte[] bytes, int offset, int length)
			throws IOException {
		outputStream.write(bytes, offset, length);
	}



	public ZonesProperty getZonesProperties() {
		return zonesProperties;
	}



	public void setZonesProperties(ZonesProperty zonesProperties) {
		FileUploaderUtil.zonesProperties = zonesProperties;
	}

	public static void main(String[] args) throws Exception {
		String SFTPHOST = "52.204.130.67";
		int SFTPPORT = 22;
		String SFTPUSER = "sgb05075";
		String SFTPPASS = "JuyOU0Fp2vCsnnAYwrJP";
		String SFTPWORKINGDIR = "upload";
		String fileName ="C:\\autopricing\\RTW\\seatgeek\\RTWtnseatgeeklastfiverowcats.csv";
		File file = new File(fileName);
		sendSFTPFile(SFTPHOST,SFTPPORT,SFTPUSER,SFTPPASS,SFTPWORKINGDIR,file);
	}

	
	public static String sendSFTPFile(String SFTPHOST,int SFTPPORT,String SFTPUSER,String SFTPPASS,String SFTPWORKINGDIR,File file) throws Exception {
       

        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        System.out.println("preparing the host information for sftp.");
        String errorMsg = null;
        try {
        	try {
	            JSch jsch = new JSch();
	            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
	            session.setPassword(SFTPPASS);
	            java.util.Properties config = new java.util.Properties();
	            config.put("StrictHostKeyChecking", "no");
	            session.setConfig(config);
	            session.connect();
	            System.out.println("Host connected.");
        	} catch(Exception e) {
        		errorMsg="File could not be uploaded due to Error while connecting SFTP Host";
        		System.out.println("Exception while connecting SFTP Host.");
        		throw e;
        	}
        	try {
        		channel = session.openChannel("sftp");
                channel.connect();
                System.out.println("sftp channel opened and connected.");
        	} catch(Exception e) {
        		errorMsg="File could not be uploaded due to Error while Opening SFTP channel";
        		System.out.println("Exception while Opening SFTP channel.");
        		throw e;
        	}
        	try {
        		channelSftp = (ChannelSftp) channel;
        		if(SFTPWORKINGDIR != null) {
        			channelSftp.cd(SFTPWORKINGDIR);	
        		}
                //File f = new File(fileName);
                channelSftp.put(new FileInputStream(file), file.getName());
                System.out.println("File transfered successfully to host.");
        	} catch(Exception e) {
        		errorMsg="File could not be uploaded due to Error while transfering file";
        		System.out.println("Exception while transfering file.");
        		throw e;
        	}
            
        } catch (Exception ex) {
        	if(errorMsg == null) {
        		errorMsg="File could not be uploaded due to Error while Uploading file";	
        	}
             System.out.println("Exception found while SFTP file Upload : " + new Date());
             ex.printStackTrace();
             //throw ex;
        }
        finally{

        	if(channelSftp != null) {
        		channelSftp.exit();
        		System.out.println("sftp Channel exited.");
        	}
            if(channel != null) {
            	 channel.disconnect();
                 System.out.println("Channel disconnected."); 	
            }
            if(session != null) {
            	 session.disconnect();
                 System.out.println("Host Session disconnected.");
            }
        }
        return errorMsg;
    }  	
	
}