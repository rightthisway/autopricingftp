package com.rtw.autopricing.ftp.upload;

public class FTPException extends Exception {
	public FTPException(String message) {
		super(message);
	}
}