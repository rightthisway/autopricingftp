package com.rtw.autopricing.ftp.upload;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.rtw.autopricing.ftp.utils.FTPVendor;

/**
 * A utility class that provides functionality for uploading files to a FTP
 * server.
 * 
 * 
 */
public class FTPUtility {

	private String host;
	private int port;
	private String username;
	private String password;

	private FTPClient ftpClient = new FTPClient();
	private int replyCode;
	

	private OutputStream outputStream;
	
	public FTPUtility(String host, int port, String user, String pass) {
		this.host = host;
		this.port = port;
		this.username = user;
		this.password = pass;
	}

	/**
	 * Connect and login to the server using port.
	 * 
	 * @throws FTPException
	 */
	public void connect() throws FTPException {
		try {
			ftpClient.connect(host, port);
			replyCode = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(replyCode)) {
				throw new FTPException("FTP serve refused connection.");
			}

			boolean logged = ftpClient.login(username, password);
			if (!logged) {
				// failed to login
				ftpClient.disconnect();
				throw new FTPException("Could not login to the server.");
			}
			
			System.out.println("logged---->"+logged);

			ftpClient.enterLocalPassiveMode();

		} catch (IOException ex) {
			throw new FTPException("I/O error: " + ex.getMessage());
		}
	}
	
	/**
	 * Connect and login to the server without port.
	 * 
	 * @throws FTPException
	 */
	public void connectWithoutPort() throws FTPException {
		try {
			ftpClient.connect(host);
			replyCode = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(replyCode)) {
				throw new FTPException("FTP serve refused connection.");
			}

			boolean logged = ftpClient.login(username, password);
			if (!logged) {
				// failed to login
				ftpClient.disconnect();
				throw new FTPException("Could not login to the server.");
			}
			System.out.println("logged---->"+logged);
			ftpClient.enterLocalPassiveMode();
		} catch (IOException ex) {
			throw new FTPException("I/O error: " + ex.getMessage());
		}
	}

	/**
	 * Start uploading a file to the server
	 * @param uploadFile the file to be uploaded
	 * @param destDir destination directory on the server 
	 * where the file is stored
	 * @throws FTPException if client-server communication error occurred
	 */
	public void uploadFile(File uploadFile, String destDir) throws FTPException {
		try {
			boolean success = ftpClient.changeWorkingDirectory(destDir);
			if (!success) {
				throw new FTPException("Could not change working directory to "
						+ destDir + ". The directory may not exist.");
			}
			
			success = ftpClient.setFileType(FTP.BINARY_FILE_TYPE);			
			if (!success) {
				throw new FTPException("Could not set binary file type.");
			}
			
			outputStream = ftpClient.storeFileStream(uploadFile.getName());
			System.out.println("outputStream----->"+outputStream);
			
			System.out.println("getReplyCode----->"+ftpClient.getReplyCode());
			System.out.println("getReplyString----->"+ftpClient.getReplyString());
			
		} catch (IOException ex) {
			throw new FTPException("Error uploading file: " + ex.getMessage());
		}
	}
	
	
	/**
	 * Start uploading a file to the server without changing the directory
	 * @param uploadFile the file to be uploaded
	 * @param destDir destination directory on the server 
	 * where the file is stored
	 * @throws FTPException if client-server communication error occurred
	 */
	public void uploadFileWihtoutChangingDirectory(File uploadFile) throws FTPException {
		try {
			/*boolean success = ftpClient.changeWorkingDirectory(destDir);
			if (!success) {
				throw new FTPException("Could not change working directory to "
						+ destDir + ". The directory may not exist.");
			}*/
			boolean success = ftpClient.setFileType(FTP.BINARY_FILE_TYPE);			
			if (!success) {
				throw new FTPException("Could not set binary file type.");
			}
			outputStream = ftpClient.storeFileStream(uploadFile.getName());
			System.out.println("outputStream----->"+outputStream);
			System.out.println("getReplyCode----->"+ftpClient.getReplyCode());
			System.out.println("getReplyString----->"+ftpClient.getReplyString());
		} catch (IOException ex) {
			throw new FTPException("Error uploading file: " + ex.getMessage());
		}
	}

	/**
	 * Write an array of bytes to the output stream.
	 */
	public void writeFileBytes(byte[] bytes, int offset, int length)
			throws IOException {
		outputStream.write(bytes, offset, length);
	}
	
	/**
	 * Complete the upload operation.
	 */
	public void finish() throws IOException {
		outputStream.close();
		ftpClient.completePendingCommand();
	}
	
	/**
	 * Log out and disconnect from the server
	 */
	public void disconnect() throws FTPException {
		if (ftpClient.isConnected()) {
			try {
				if (!ftpClient.logout()) {
					throw new FTPException("Could not log out from the server");
				}
				ftpClient.disconnect();
			} catch (IOException ex) {
				throw new FTPException("Error disconnect from the server: "
						+ ex.getMessage());
			}
		}
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public FTPClient getFtpClient() {
		return ftpClient;
	}

	public void setFtpClient(FTPClient ftpClient) {
		this.ftpClient = ftpClient;
	}

	public int getReplyCode() {
		return replyCode;
	}

	public void setReplyCode(int replyCode) {
		this.replyCode = replyCode;
	}

	public static void main(String[] args) throws Exception {
		FTPUtility ftp = new FTPUtility("52.204.130.67", 22, "sgb05075", "JuyOU0Fp2vCsnnAYwrJP");
		ftp.connect();
		File file = new File("C:\\autopricing\\RTW\\seatgeek\\RTWtnseatgeeklastfiverowcats.csv");
		ftp.uploadFile(file, "upload");
	}
	
	
	
	
}