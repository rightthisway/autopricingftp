package com.rtw.autopricing.ftp.upload;

import java.io.File;
import java.io.FileInputStream;

import com.rtw.autopricing.ftp.data.FtpFileUploadAudit;
import com.rtw.autopricing.ftp.utils.FTPType;
import com.rtw.autopricing.ftp.utils.FTPVendor;



public class FTPUploader {
	
	private static final int BUFFER_SIZE = 4096;
	
	public static String uploadFile(FtpFileUploadAudit fileUploadAudit,FTPVendor ftpVendor) throws Exception {
		
		if(fileUploadAudit.getFtpType().equals(FTPType.SFTP)) {
			return uploadSecureFTPFile(fileUploadAudit, ftpVendor);
		} else {
			return uploadFTPFile(fileUploadAudit, ftpVendor);
		}
	}
	
 public static String uploadFTPFile(FtpFileUploadAudit fileUploadAudit,FTPVendor ftpVendor) throws Exception {
	 
	 FTPUtility util = new FTPUtility(fileUploadAudit.getHostName(),fileUploadAudit.getPort(), fileUploadAudit.getUserName(), fileUploadAudit.getPassword());
	 try{
		 	File uploadFile =fileUploadAudit.getOriginalFile();
		 	switch (ftpVendor) {
		 	
			 	case SCOREBIG:
			 		//Method To Connect the FTP Server using Credentials
					util.connectWithoutPort();
					util.uploadFileWihtoutChangingDirectory(uploadFile);
					break;
					
			 	case VIVIDSEAT:
			 		//Method To Connect the FTP Server using Credentials
					util.connect();
					//Method To Connect Upload the files to the connected servers
					util.uploadFile(uploadFile, fileUploadAudit.getDestination());
					break;
				default:
					//util.connect();
					break;
					
			}
			
			FileInputStream inputStream = new FileInputStream(uploadFile);
			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;
			long totalBytesRead = 0;
			int percentCompleted = 0;
			long fileSize = uploadFile.length();
			
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				util.writeFileBytes(buffer, 0, bytesRead);
				totalBytesRead += bytesRead;
				percentCompleted = (int) (totalBytesRead * 100 / fileSize);
			}
			
			String status="";
			
			if(percentCompleted==100){
				status="SUCCESS";
			}else{
				status="FAILURE";
			}
			inputStream.close();
			util.finish();
			return status;
	} catch (FTPException ex) {
		ex.printStackTrace();
		return "FAILURE";
	} finally {
		util.disconnect();
	}
 }
 
public static String uploadSecureFTPFile(FtpFileUploadAudit fileUploadAudit,FTPVendor ftpVendor) throws Exception {
	 
	 try {
		 File uploadFile =fileUploadAudit.getOriginalFile();
		 String errorMsg = FileUploaderUtil.sendSFTPFile(fileUploadAudit.getHostName(),fileUploadAudit.getPort(), fileUploadAudit.getUserName(),
		 			fileUploadAudit.getPassword(), fileUploadAudit.getDestination(), uploadFile);
		 	
		 String status="SUCCESS";
		 if(errorMsg != null) {
			 status="FAILURE";
		 }
		 return status;
	} catch (Exception ex) {
		ex.printStackTrace();
		return "FAILURE";
	}
 }
 
 
public static String uploadFileOriginal(FtpFileUploadAudit fileUploadAudit) throws Exception {
	 
	 FTPUtility util = new FTPUtility(fileUploadAudit.getHostName(),fileUploadAudit.getPort(), fileUploadAudit.getUserName(), fileUploadAudit.getPassword());
	 
	 try{
		 
		 	File uploadFile =fileUploadAudit.getOriginalFile();
			util.connect();
			util.uploadFile(uploadFile, fileUploadAudit.getDestination());
			FileInputStream inputStream = new FileInputStream(uploadFile);
			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;
			long totalBytesRead = 0;
			int percentCompleted = 0;
			long fileSize = uploadFile.length();
			
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				System.out.println("buffer--->"+buffer);
				System.out.println("bytesRead--->"+bytesRead);
				util.writeFileBytes(buffer, 0, bytesRead);
				totalBytesRead += bytesRead;
				percentCompleted = (int) (totalBytesRead * 100 / fileSize);
			}
			
			String lPath=uploadFile.getAbsolutePath();
			String rPath="ftp://"+fileUploadAudit.getHostName()+"/"+fileUploadAudit.getDestination();
			String status="";
			
			if(percentCompleted==100){
				status="Uploaded Sucessfully";
			}else{
				status="Failed";
			}
			inputStream.close();
			util.finish();
			return status;
	} catch (FTPException ex) {
		ex.printStackTrace();
		return "Exception";
	} finally {
		util.disconnect();
	}
 }

}