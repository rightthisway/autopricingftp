package com.rtw.autopricing.ftp.enums;

import java.io.Serializable;

public enum TourType implements Serializable {
	SPORT, THEATER, CONCERT, OTHER
}
