package com.rtw.autopricing.ftp.enums;

public enum ArtistStatus {
	ACTIVE, EXPIRED, DELETED
}
