package com.rtw.autopricing.ftp.enums;



public enum RemoveDuplicatePolicy {
	NONE, SIMPLE, REMOVE_NON_WORD_CHARACTERS, SMART, MANUAL, SUPER_SMART
}

