package com.rtw.autopricing.ftp.enums;

public enum TicketStatus {
	ACTIVE, EXPIRED, SOLD, DISABLED
}
