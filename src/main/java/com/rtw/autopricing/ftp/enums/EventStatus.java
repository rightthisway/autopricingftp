package com.rtw.autopricing.ftp.enums;

public enum EventStatus {
	ACTIVE, EXPIRED, DELETED,DELETEDEXPIRED
}
