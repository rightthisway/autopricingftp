package com.rtw.autopricing.ftp.enums;


public enum TicketDeliveryType {
	INSTANT, EDELIVERY, MERCURY, MERCURYPOS, ETICKETS
}

