package com.rtw.autopricing.ftp.enums;



public enum UserAlertStatus {
	ACTIVE, DISABLED, EXPIRED, DELETED
}
