package com.rtw.autopricing.ftp.enums;

public enum UserOutlierStatus {
	ACTIVE, DISABLED, EXPIRED, DELETED
}
