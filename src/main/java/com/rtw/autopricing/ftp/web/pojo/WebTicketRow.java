package com.rtw.autopricing.ftp.web.pojo;



import java.util.Date;

import javax.persistence.Transient;

import com.rtw.autopricing.ftp.data.HarvestTicket;
import com.rtw.autopricing.ftp.data.Ticket;


public class WebTicketRow extends Ticket {
	private boolean removedDuplicateTicket;
	private boolean hasDuplicate;
	private Double currentAdjustedPrice;
	private String normalizedSection;
	private String normalizedRow;
	private String normalizedSeat;
	private String categorySymbol;
	private Integer eval;
	private Integer numerizedSection;
	private String priceByQuantity;
	//private String dupColor;
	
	public WebTicketRow(Ticket ticket) {
		super();
		setSiteId(ticket.getSiteId());
		if(ticket.getItemId() != null){
			setItemId(ticket.getItemId().replaceAll("'", " "));
		}
		setId(ticket.getId());
		setAdmitOneTicket(ticket.isAdmitOneTicket());
		setAdmitOneNonTMTicket(ticket.isAdmitOneNonTMTicket());
	//	setAdjustedCurrentPrice(ticket.getAdjustedCurrentPrice());
		setBuyItNowPrice(ticket.getBuyItNowPrice());
		//setCategoryId(ticket.getCategoryId());
		setCurrentPrice(ticket.getCurrentPrice());
		setEndDate(ticket.getEndDate());
		setEventId(ticket.getEventId());
		setInsertionDate(ticket.getInsertionDate());
		setLastUpdate(ticket.getLastUpdate());
		setLotSize(ticket.getLotSize());
		if(ticket.getNormalizedRow() != null){
			setNormalizedRow(ticket.getNormalizedRow().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getNormalizedSection() != null){
			setNormalizedSection(ticket.getNormalizedSection().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getSeat() != null){
			setSeat(ticket.getSeat().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getNormalizedSeat() != null){
			setNormalizedSeat(ticket.getNormalizedSeat().replaceAll("'", " ").replaceAll("\n", " "));
		}
		setPriceUpdateCount(ticket.getPriceUpdateCount());
		setQuantity(ticket.getQuantity());
		setRemainingQuantity(ticket.getRemainingQuantity());
		if(ticket.getRow() != null){
			setRow(ticket.getRow().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getSection() != null){
			setSection(ticket.getSection().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getSeller() != null){
			setSeller(ticket.getSeller().replaceAll("'", " ").replaceAll("\n", " "));
		}
		//setShortPrice(ticket.getShortPrice());
		setSoldQuantity(ticket.getSoldQuantity());
	//	setTicketListingCrawlId(ticket.getTicketListingCrawlId());
		setTicketStatus(ticket.getTicketStatus());
		setTicketType(ticket.getTicketType());
		setTicketDeliveryType(ticket.getTicketDeliveryType());
		setCategoryMappingId(ticket.getCategoryMappingId());
		setInHand(ticket.isInHand());
	}
	
	public WebTicketRow(HarvestTicket ticket) {
		super();
		setSiteId(ticket.getSiteId());
		if(ticket.getItemId() != null){
			setItemId(ticket.getItemId().replaceAll("'", " "));
		}
		setId(ticket.getId());
		setAdmitOneTicket(ticket.isAdmitOneTicket());
		setAdmitOneNonTMTicket(ticket.isAdmitOneNonTMTicket());
		setAdjustedCurrentPrice(ticket.getAdjustedCurrentPrice());
		setBuyItNowPrice(ticket.getBuyItNowPrice());
		//setCategoryId(ticket.getCategoryId());
		setCurrentPrice(ticket.getCurrentPrice());
		setEndDate(ticket.getEndDate());
		setEventId(ticket.getEventId());
		setInsertionDate(ticket.getInsertionDate());
		setLastUpdate(ticket.getLastUpdate());
		setLotSize(ticket.getLotSize());
		if(ticket.getNormalizedRow() != null){
			setNormalizedRow(ticket.getNormalizedRow().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getNormalizedSection() != null){
			setNormalizedSection(ticket.getNormalizedSection().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getSeat() != null){
			setSeat(ticket.getSeat().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getNormalizedSeat() != null){
			setNormalizedSeat(ticket.getNormalizedSeat().replaceAll("'", " ").replaceAll("\n", " "));
		}
		setPriceUpdateCount(ticket.getPriceUpdateCount());
		setQuantity(ticket.getQuantity());
		setRemainingQuantity(ticket.getRemainingQuantity());
		if(ticket.getRow() != null){
			setRow(ticket.getRow().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getSection() != null){
			setSection(ticket.getSection().replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(ticket.getSeller() != null){
			setSeller(ticket.getSeller().replaceAll("'", " ").replaceAll("\n", " "));
		}
		//setShortPrice(ticket.getShortPrice());
		setSoldQuantity(ticket.getSoldQuantity());
	//	setTicketListingCrawlId(ticket.getTicketListingCrawlId());
		setTicketStatus(ticket.getTicketStatus());
		setTicketType(ticket.getTicketType());
		setTicketDeliveryType(ticket.getTicketDeliveryType());
	}
	@Deprecated
	public WebTicketRow(Integer id, Integer qty, String section, String normalizedSection, String row, String normalizedRow, Double wholesale, Double online, String broker, Date startDate, Date endDate) {
		super();
		setId(id);
		setAdjustedCurrentPrice(wholesale);
		setBuyItNowPrice(online);
		//setCategoryId(ticket.getCategoryId());
		setInsertionDate(startDate);
		setLastUpdate(endDate);
		setQuantity(qty);
		setRemainingQuantity(qty);
		if(section != null){
			setSection(section.replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(row != null){
			setRow(row.replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(normalizedRow != null){
			setNormalizedRow(normalizedRow.replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(normalizedSection != null){
			setNormalizedSection(normalizedSection.replaceAll("'", " ").replaceAll("\n", " "));
		}
		if(broker != null){
			setSeller(broker.replaceAll("'", " ").replaceAll("\n", " "));
		}
	}

	public boolean getHasDuplicate() {
		return hasDuplicate;
	}
	
	public void setHasDuplicate(boolean hasDuplicate) {
		this.hasDuplicate = hasDuplicate;
	}

	public boolean isRemovedDuplicateTicket() {
		return removedDuplicateTicket;
	}

	public void setRemovedDuplicateTicket(boolean removedDuplicateTicket) {
		this.removedDuplicateTicket = removedDuplicateTicket;
	}

	public String getNormalizedSection() {
		return normalizedSection;
	}

	public void setNormalizedSection(String normalizedSection) {
		this.normalizedSection = normalizedSection;
	}

	public String getNormalizedRow() {
		return normalizedRow;
	}

	public void setNormalizedRow(String normalizedRow) {
		this.normalizedRow = normalizedRow;
	}

	public String getNormalizedSeat() {
		return normalizedSeat;
	}

	public void setNormalizedSeat(String normalizedSeat) {
		this.normalizedSeat = normalizedSeat;
	}

	public String getCategorySymbol() {
		return categorySymbol;
	}

	public void setCategorySymbol(String categorySymbol) {
		this.categorySymbol = categorySymbol;
	}

	public Integer getEval() {
		return eval;
	}

	public void setEval(Integer eval) {
		this.eval = eval;
	}
	
	public Integer getNumerizedSection(){
		return this.numerizedSection;
	}
	
	public void setNumerizedSection(Integer numerizedSection){
		this.numerizedSection = numerizedSection;
	}

	
	public Double getAdjustedCurrentPrice() {
		return currentAdjustedPrice;
	}

	public void setAdjustedCurrentPrice(Double currentAdjustedPrice) {
		this.currentAdjustedPrice = currentAdjustedPrice;
	}

	public String getPriceByQuantity() {
		return priceByQuantity;
	}

	public void setPriceByQuantity(String priceByQuantity) {
		this.priceByQuantity = priceByQuantity;
	}

	public boolean getInHand() {
		return super.isInHand();
	}
	
}

