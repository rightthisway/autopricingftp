package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.util.connection.Connections;

public class PosEventDAO implements com.rtw.autopricing.ftp.indux.dao.services.PosEventDAO  {
	
	
	public List<PosEvent> getAllActivePOSEventsByPOSBrokerId(Integer brokerId) throws Exception{
		
		String sql = "SELECT DISTINCT MIN(e.event_id) as eventId,ee.id,ee.EventDate,ee.Event,ee.Venue,e.venue_id as venue_id,ee.venue_configuration_id,ee.childID" +
				" FROM exchange_event ee " +
				" INNER JOIN event e on ee.Id = e.exchange_event_id " +
				" inner join venue v on v.venue_id=e.venue_id " +
				" inner join system_country sc on sc.system_country_id=v.system_country_id " +
				" where  sc.country_code in ('US','CA')" +
				" GROUP BY ee.id,ee.EventDate,ee.Event,ee.Venue,e.venue_id ,ee.venue_configuration_id,ee.childID";
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getInduxConnection(brokerId);
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<PosEvent> events = new ArrayList<PosEvent>();
			while(resultSet.next()){
				PosEvent event = new PosEvent();
				event.setId(resultSet.getInt("eventId"));
				event.setExchangeEventId(resultSet.getInt("id"));
				event.setEventDateStr(resultSet.getString("EventDate"));
				event.setEventName(resultSet.getString("Event"));
				event.setVenueId(resultSet.getInt("venue_id"));
				event.setVenueName(resultSet.getString("Venue"));
				event.setChildCategoryId(resultSet.getInt("childID"));
				
				Integer venueConfigId= resultSet.getInt("venue_configuration_id");
				event.setVenueConfigurationId((null !=venueConfigId && venueConfigId != 0 )?venueConfigId:-1);

				events.add(event);
				
			}
			return events;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}
	
	
 public List<PosEvent> getAllActivePOSLocalEventsByPOSBrokerId(Integer brokerId) throws Exception{
		
		String sql = "SELECT DISTINCT MIN(e.event_id) as eventId,e.event_datetime,e.event_name,v.name as venue_name,e.venue_id as venue_id " +
				" FROM event e  " +
				" inner join venue v on v.venue_id=e.venue_id " +
				" inner join system_country sc on sc.system_country_id=v.system_country_id " +
				" where  e.event_datetime>=getdate() and sc.country_code in ('US','CA')" +
				" GROUP BY e.event_datetime,e.event_name,v.name ,e.venue_id";
		ResultSet resultSet =null;
		try {
			Connection connection = Connections.getInduxConnection(brokerId);
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<PosEvent> events = new ArrayList<PosEvent>();
			while(resultSet.next()){
				PosEvent event = new PosEvent();
				event.setId(resultSet.getInt("eventId"));
				event.setEventDateStr(resultSet.getString("event_datetime"));
				event.setEventName(resultSet.getString("event_name"));
				event.setVenueId(resultSet.getInt("venue_id"));
				event.setVenueName(resultSet.getString("venue_name"));
				events.add(event);
			}
			return events;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}
 
 
 public List<PosEvent> getAllActiveExchangeEventWithoutLocalEvent(Integer brokerId) throws Exception{
		
		
		String sql = "SELECT ee.* FROM exchange_event ee where " +
				" ee.Id not in (SELECT exchange_event_id FROM event where " +
				" exchange_event_id != -1 or exchange_event_id != 0) and ee.EventDate >= GETDATE() order by EventDate";
		
		ResultSet resultSet =null;
		
		try {
			Connection connection = Connections.getInduxConnection(brokerId);
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			List<PosEvent> events = new ArrayList<PosEvent>();
			while(resultSet.next()){
				PosEvent event = new PosEvent();
				event.setExchangeEventId(resultSet.getInt("Id"));
				event.setEventDateStr(resultSet.getString("EventDate"));
				event.setEventName(resultSet.getString("Event"));
				event.setVenueName(resultSet.getString("Venue"));
				events.add(event);
			}
			return events;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}
 
 public Boolean checkPOSConnection(Integer brokerId) throws Exception{
		
		
		String sql = "SELECT top 1 ee.* FROM exchange_event ee ";
		
		ResultSet resultSet =null;
		try {
			Connection connection = Connections.getInduxConnection(brokerId);
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			if(resultSet.next()){
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}

 public boolean updateLocalEvent(Integer brokerId,Integer ExchangeEventId,Integer systemUserId) throws Exception {
	 
	 CallableStatement cs = null;
	 try{
		 Connection connection = Connections.getInduxConnection(brokerId);
	     cs = connection.prepareCall("{call event_import(?,?)}");
	     cs.setInt(1, systemUserId);
         cs.setInt(2, ExchangeEventId);
         return cs.execute();
	 }catch(Exception e){
		 //e.printStackTrace();
		 //return false;
		 throw e;
	 }
 }
	
}
