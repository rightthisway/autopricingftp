package com.rtw.autopricing.ftp.indux.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.indux.data.PosVenueConfigurationZone;

public interface PosVenueConfigurationZoneDAO {
	

	
 public List<PosVenueConfigurationZone> getAllVenueConfigZone(Integer brokerId) throws Exception;
	
	
}
