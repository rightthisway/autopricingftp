package com.rtw.autopricing.ftp.indux.data;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="exchange_event")
public final class PosEvent implements Serializable  {
	protected static final long serialVersionUID = -7402732543016575381L;
	
	
	protected Integer id;
	protected Integer venueId;
	protected String eventName;
	protected String venueName;
	protected Integer exchangeEventId;
	protected Date eventDate;
	protected String eventDateStr;
	protected Integer venueConfigurationId;
	
	protected Integer childCategoryId;

	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="venueid")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="event")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name="venue")
	public String getVenueName() {
		return venueName;
	}

	
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Transient
	public String getEventDateStr() {
		return eventDateStr;
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	@Column(name="id")
	public Integer getExchangeEventId() {
		return exchangeEventId;
	}

	public void setExchangeEventId(Integer exchangeEventId) {
		this.exchangeEventId = exchangeEventId;
	}

	@Column(name="eventdate")
	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	@Column(name="venue_configuration_id")
	public Integer getVenueConfigurationId() {
		return venueConfigurationId;
	}

	public void setVenueConfigurationId(Integer venueConfigurationId) {
		this.venueConfigurationId = venueConfigurationId;
	}

	public Integer getChildCategoryId() {
		return childCategoryId;
	}

	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}
	
}