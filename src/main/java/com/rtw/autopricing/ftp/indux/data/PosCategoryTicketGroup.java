package com.rtw.autopricing.ftp.indux.data;

import java.io.Serializable;


public class PosCategoryTicketGroup implements Serializable {
	
	private Integer id; 
	private Integer posEventId; 
	private Integer posExchangeEventId; 
	
	private Double retailPrice;
	private Double wholesalePrice;
	private String notes;
	private String rowVersion;
	private String mergeRowVersion;
	private String priceUpdateDatatimeStr;
	private String updateDateTimeStr;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPosEventId() {
		return posEventId;
	}
	public void setPosEventId(Integer posEventId) {
		this.posEventId = posEventId;
	}
	public Integer getPosExchangeEventId() {
		return posExchangeEventId;
	}
	public void setPosExchangeEventId(Integer posExchangeEventId) {
		this.posExchangeEventId = posExchangeEventId;
	}
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getRowVersion() {
		return rowVersion;
	}
	public void setRowVersion(String rowVersion) {
		this.rowVersion = rowVersion;
	}
	public String getMergeRowVersion() {
		return mergeRowVersion;
	}
	public void setMergeRowVersion(String mergeRowVersion) {
		this.mergeRowVersion = mergeRowVersion;
	}
	public String getPriceUpdateDatatimeStr() {
		return priceUpdateDatatimeStr;
	}
	public void setPriceUpdateDatatimeStr(String priceUpdateDatatimeStr) {
		this.priceUpdateDatatimeStr = priceUpdateDatatimeStr;
	}
	public String getUpdateDateTimeStr() {
		return updateDateTimeStr;
	}
	public void setUpdateDateTimeStr(String updateDateTimeStr) {
		this.updateDateTimeStr = updateDateTimeStr;
	}

	
}
