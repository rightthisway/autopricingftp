package com.rtw.autopricing.ftp.indux.dao.implementation;

import com.rtw.autopricing.ftp.indux.dao.services.PosCategoryTicketDAO;
import com.rtw.autopricing.ftp.indux.dao.services.PosCategoryTicketGroupDAO;
import com.rtw.autopricing.ftp.indux.dao.services.ExchangeEventDAO;
import com.rtw.autopricing.ftp.indux.dao.services.PosEventDAO;
import com.rtw.autopricing.ftp.indux.dao.services.PosVenueCategoryDAO;
import com.rtw.autopricing.ftp.indux.dao.services.PosVenueConfigurationZoneDAO;
import com.rtw.autopricing.ftp.indux.dao.services.PurchaseOrderDAO;
import com.rtw.autopricing.ftp.indux.dao.services.PosTicketDAO;
import com.rtw.autopricing.ftp.indux.dao.services.POSTicketGroupDAO;
import com.rtw.autopricing.ftp.indux.dao.services.UserOfficeDAO;

public class InduxDAORegistry {
	
	private static ExchangeEventDAO exchangeEventDAO;
	private static PosEventDAO posEventDAO;
	private static PurchaseOrderDAO purchaseOrderDAO;
	private static PosTicketDAO posTicketDAO;
	private static POSTicketGroupDAO posTicketGroupDAO;
	private static PosCategoryTicketDAO posCategoryTicketDAO;
	private static PosCategoryTicketGroupDAO posCategoryTicketGroupDAO;
	private static PosVenueCategoryDAO posVenueCategoryDAO;
	private static UserOfficeDAO userOfficeDAO;
	private static PosVenueConfigurationZoneDAO posVenueConfigurationZoneDAO;
	
	public final static ExchangeEventDAO getExchangeEventDAO() {
		return exchangeEventDAO;
	}

	public final void  setExchangeEventDAO(ExchangeEventDAO exchangeEventDAO) {
		InduxDAORegistry.exchangeEventDAO = exchangeEventDAO;
	}

	public final static PurchaseOrderDAO getPurchaseOrderDAO() {
		return purchaseOrderDAO;
	}

	public final void setPurchaseOrderDAO(PurchaseOrderDAO purchaseOrderDAO) {
		InduxDAORegistry.purchaseOrderDAO = purchaseOrderDAO;
	}


	public final static UserOfficeDAO getUserOfficeDAO() {
		return userOfficeDAO;
	}

	public final void setUserOfficeDAO(UserOfficeDAO userOfficeDAO) {
		InduxDAORegistry.userOfficeDAO = userOfficeDAO;
	}

	public final static PosEventDAO getPosEventDAO() {
		return posEventDAO;
	}

	public final void setPosEventDAO(PosEventDAO posEventDAO) {
		InduxDAORegistry.posEventDAO = posEventDAO;
	}

	public final static PosTicketDAO getPosTicketDAO() {
		return posTicketDAO;
	}

	public final void setPosTicketDAO(PosTicketDAO posTicketDAO) {
		InduxDAORegistry.posTicketDAO = posTicketDAO;
	}

	public final static POSTicketGroupDAO getPosTicketGroupDAO() {
		return posTicketGroupDAO;
	}

	public final void setPosTicketGroupDAO(POSTicketGroupDAO posTicketGroupDAO) {
		InduxDAORegistry.posTicketGroupDAO = posTicketGroupDAO;
	}

	public final static PosCategoryTicketDAO getPosCategoryTicketDAO() {
		return posCategoryTicketDAO;
	}

	public final void setPosCategoryTicketDAO(
			PosCategoryTicketDAO posCategoryTicketDAO) {
		InduxDAORegistry.posCategoryTicketDAO = posCategoryTicketDAO;
	}

	public final static PosCategoryTicketGroupDAO getPosCategoryTicketGroupDAO() {
		return posCategoryTicketGroupDAO;
	}

	public final void setPosCategoryTicketGroupDAO(
			PosCategoryTicketGroupDAO posCategoryTicketGroupDAO) {
		InduxDAORegistry.posCategoryTicketGroupDAO = posCategoryTicketGroupDAO;
	}

	public final static PosVenueCategoryDAO getPosVenueCategoryDAO() {
		return posVenueCategoryDAO;
	}

	public final void setPosVenueCategoryDAO(
			PosVenueCategoryDAO posVenueCategoryDAO) {
		InduxDAORegistry.posVenueCategoryDAO = posVenueCategoryDAO;
	}

	public final static PosVenueConfigurationZoneDAO getPosVenueConfigurationZoneDAO() {
		return posVenueConfigurationZoneDAO;
	}

	public final void setPosVenueConfigurationZoneDAO(
			PosVenueConfigurationZoneDAO posVenueConfigurationZoneDAO) {
		InduxDAORegistry.posVenueConfigurationZoneDAO = posVenueConfigurationZoneDAO;
	}

	
}
