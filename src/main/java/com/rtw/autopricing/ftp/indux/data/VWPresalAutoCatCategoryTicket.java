package com.rtw.autopricing.ftp.indux.data;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


public class VWPresalAutoCatCategoryTicket implements Serializable {
   private Integer id;
   private String eventName;
   private String eventDate;
   private String eventTime;
   private String venueName;
   private Integer quantity;
   private String section;
   private String rowRange;
   private String lastRow;
   private Double actualPrice;
   private Double tickPickPrice;
   private Double scoreBigPrice;
   private Double tnPrice;
   private Double vividPrice;
   private Double OtherExchangePrice;
   private String expectedArrivalDate;
   private Integer shippingMethodId;
   private Integer nearTermDisplayOptionId;
   private Integer eventId;
   private Integer tnExchangeEventId;
   private Long tnCategoryTicketGroupId;
   private Integer categoryId;
   private Date createdDate;
   private String status;
   private String eventType;
   private String seat;
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public String getEventName() {
	return eventName;
}
public void setEventName(String eventName) {
	this.eventName = eventName;
}
public String getEventDate() {
	return eventDate;
}
public void setEventDate(String eventDate) {
	this.eventDate = eventDate;
}
public String getEventTime() {
	return eventTime;
}
public void setEventTime(String eventTime) {
	this.eventTime = eventTime;
}
public String getVenueName() {
	return venueName;
}
public void setVenueName(String venueName) {
	this.venueName = venueName;
}
public Integer getQuantity() {
	return quantity;
}
public void setQuantity(Integer quantity) {
	this.quantity = quantity;
}
public String getSection() {
	return section;
}
public void setSection(String section) {
	this.section = section;
}
public String getRowRange() {
	return rowRange;
}
public void setRowRange(String rowRange) {
	this.rowRange = rowRange;
}
public String getLastRow() {
	return lastRow;
}
public void setLastRow(String lastRow) {
	this.lastRow = lastRow;
}
public Double getActualPrice() {
	return actualPrice;
}
public void setActualPrice(Double actualPrice) {
	this.actualPrice = actualPrice;
}
public Double getTickPickPrice() {
	return tickPickPrice;
}
public void setTickPickPrice(Double tickPickPrice) {
	this.tickPickPrice = tickPickPrice;
}
public Double getScoreBigPrice() {
	return scoreBigPrice;
}
public void setScoreBigPrice(Double scoreBigPrice) {
	this.scoreBigPrice = scoreBigPrice;
}
public Double getTnPrice() {
	return tnPrice;
}
public void setTnPrice(Double tnPrice) {
	this.tnPrice = tnPrice;
}
public Double getVividPrice() {
	return vividPrice;
}
public void setVividPrice(Double vividPrice) {
	this.vividPrice = vividPrice;
}
public Double getOtherExchangePrice() {
	return OtherExchangePrice;
}
public void setOtherExchangePrice(Double otherExchangePrice) {
	OtherExchangePrice = otherExchangePrice;
}
public String getExpectedArrivalDate() {
	return expectedArrivalDate;
}
public void setExpectedArrivalDate(String expectedArrivalDate) {
	this.expectedArrivalDate = expectedArrivalDate;
}
public Integer getShippingMethodId() {
	return shippingMethodId;
}
public void setShippingMethodId(Integer shippingMethodId) {
	this.shippingMethodId = shippingMethodId;
}
public Integer getNearTermDisplayOptionId() {
	return nearTermDisplayOptionId;
}
public void setNearTermDisplayOptionId(Integer nearTermDisplayOptionId) {
	this.nearTermDisplayOptionId = nearTermDisplayOptionId;
}
public Integer getEventId() {
	return eventId;
}
public void setEventId(Integer eventId) {
	this.eventId = eventId;
}
public Integer getTnExchangeEventId() {
	return tnExchangeEventId;
}
public void setTnExchangeEventId(Integer tnExchangeEventId) {
	this.tnExchangeEventId = tnExchangeEventId;
}
public Long getTnCategoryTicketGroupId() {
	return tnCategoryTicketGroupId;
}
public void setTnCategoryTicketGroupId(Long tnCategoryTicketGroupId) {
	this.tnCategoryTicketGroupId = tnCategoryTicketGroupId;
}
public Integer getCategoryId() {
	return categoryId;
}
public void setCategoryId(Integer categoryId) {
	this.categoryId = categoryId;
}
public Date getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(Date createdDate) {
	this.createdDate = createdDate;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getEventType() {
	return eventType;
}
public void setEventType(String eventType) {
	this.eventType = eventType;
}
public String getSeat() {
	return seat;
}
public void setSeat(String seat) {
	this.seat = seat;
}
}