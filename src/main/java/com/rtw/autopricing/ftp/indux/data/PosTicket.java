package com.rtw.autopricing.ftp.indux.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ticket")
public class PosTicket implements Serializable {
	
	private Integer ticketId; 
	private Integer ticketGroupId; 
	private Integer invoiceId; 
	private Integer purchaseOrderId; 
	private Integer statusId; 
	private String row;
	private String section;
	private String originalSection;
	private Integer seatNumber;
	private Integer seatOrder;
	
	private Double retailPrice;
	private Double facePrice;
	private Double cost;
	private Double wholesalePrice;
	private Double actualSoldPrice;
	private String generalDesc;
	private Date expectedArrivalDate;
	private Integer systemUserId;
	private Integer modSystemUserId;
	private Integer ticketOnHandStausId;
	private Date createDate;
	
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="purchase_order_id")
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	
	@Column(name="status_id")
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="original_section")
	public String getOriginalSection() {
		return originalSection;
	}
	public void setOriginalSection(String originalSection) {
		this.originalSection = originalSection;
	}
	
	@Column(name="seat_number")
	public Integer getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(Integer seatNumber) {
		this.seatNumber = seatNumber;
	}
	
	@Column(name="seat_order")
	public Integer getSeatOrder() {
		return seatOrder;
	}
	public void setSeatOrder(Integer seatOrder) {
		this.seatOrder = seatOrder;
	}
	
	@Column(name="retaul_price")
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	@Column(name="face_price")
	public Double getFacePrice() {
		return facePrice;
	}
	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}
	
	@Column(name="cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	@Column(name="wholesale_price")
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name="actual_sold_price")
	public Double getActualSoldPrice() {
		return actualSoldPrice;
	}
	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	
	@Column(name="general_desc")
	public String getGeneralDesc() {
		return generalDesc;
	}
	public void setGeneralDesc(String generalDesc) {
		this.generalDesc = generalDesc;
	}
	
	@Column(name="expected_arrival_date")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name="system_user_id")
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	
	@Column(name="ticket_on_hand_status_id")
	public Integer getTicketOnHandStausId() {
		return ticketOnHandStausId;
	}
	public void setTicketOnHandStausId(Integer ticketOnHandStausId) {
		this.ticketOnHandStausId = ticketOnHandStausId;
	}
	
	@Column(name="mod_system_user_id")
	public Integer getModSystemUserId() {
		return modSystemUserId;
	}
	public void setModSystemUserId(Integer modSystemUserId) {
		this.modSystemUserId = modSystemUserId;
	}
	
	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
}
