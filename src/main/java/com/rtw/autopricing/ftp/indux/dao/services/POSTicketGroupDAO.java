package com.rtw.autopricing.ftp.indux.dao.services;

import java.sql.SQLException;
import java.util.List;

import com.rtw.autopricing.ftp.dao.services.RootDAO;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.indux.data.Broker;
import com.rtw.autopricing.ftp.indux.data.POSTicketGroup;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.util.CategoryTicket;


public interface POSTicketGroupDAO extends RootDAO<Integer, POSTicketGroup>{
	//public List<Integer> getEventIdsByOfficeId(Integer brokerId,Integer officeId) throws Exception;
	public POSTicketGroup getTicketGroupById(Integer brokerId,Integer ticketGroupId) throws Exception;
	//public List<POSTicketGroup> getAllLarryTicketGroupsByExchangeEventId(Integer brokerId,Integer exchangeEventId,Integer officeId) throws Exception;
	public boolean addTicketGroupInPOS(Integer brokerId,CategoryTicket tix,AutopricingProduct product,PosEvent posEvent,Integer purchaseOrderId) throws Exception;
	public int updateTicketGroup(Integer brokerId,CategoryTicket catTix) throws Exception;
	public int deleteTicketGroupsByPosTicketGroupId(Broker broker,Integer ticketGroupId) throws Exception;
	public int save(Integer brokerId,POSTicketGroup pOSTicketGroup) throws Exception;
	//public List<LarryLastCategoryTicket> getSoldCategoryTicketGroupIds(Integer brokerId,String fromDateStr,String toDateStr) throws Exception;
	//public Integer getLarryLastActiveTicketsCountinPOS(Integer brokerId,String posEventTypeIdStr) throws Exception;
	public Integer getAllActiveTicketsCountinPOS(Broker broker) throws Exception;
	public List<Integer> getSoldTicketGroupIds(String fromDateStr,String toDateStr,String internalNotesStr,Integer brokerId) throws Exception;
	public Integer getPurchaseOrderIdByExchangeEventId(Broker broke,Integer exchangeEventId,AutopricingProduct autopricingProduct) throws Exception; 
	public boolean unbroadcastTicketGroupsByEventIds(Integer brokerId,String eventIds) throws Exception;
	public boolean unbroadcastTicketGroupsByExchangeEventIds(Broker broker,String exchangeEventIds) throws Exception;
	public Integer getPurchaseOrderIdByExchangeEventIdForSSListing(Broker broker,Integer exchangeEventId) throws Exception ;
}
