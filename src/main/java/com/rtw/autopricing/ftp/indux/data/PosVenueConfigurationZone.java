package com.rtw.autopricing.ftp.indux.data;


import java.io.Serializable;



public final class PosVenueConfigurationZone implements Serializable  {
	protected static final long serialVersionUID = -7402732543016575381L;
	
	
	protected Integer venueConfigurationZoneId;
	protected Integer venueConfigurationId;
	protected String zone;
	
	
	public Integer getVenueConfigurationZoneId() {
		return venueConfigurationZoneId;
	}
	public void setVenueConfigurationZoneId(Integer venueConfigurationZoneId) {
		this.venueConfigurationZoneId = venueConfigurationZoneId;
	}
	public Integer getVenueConfigurationId() {
		return venueConfigurationId;
	}
	public void setVenueConfigurationId(Integer venueConfigurationId) {
		this.venueConfigurationId = venueConfigurationId;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}


	
}