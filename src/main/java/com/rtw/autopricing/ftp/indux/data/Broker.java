package com.rtw.autopricing.ftp.indux.data;

public interface Broker {
	public String getName();
	public void setName(String name);
	public String getPhoneNo();
	public void setPhoneNo(String name);
	
	public Integer getId();
	public Integer getBuyerBrokerId();
	public void setBuyerBrokerId(Integer buyerBrokerId);
	public Integer getClientBrokerEmployeeId();
	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId);
	public Integer getSystemUserId();
	public void setSystemUserId(Integer systemUserId);
	public Integer getClientBrokerId();
	public void setClientBrokerId(Integer clientBrokerId);
	public Integer getPosBrokerId();
	public void setPosBrokerId(Integer posBrokerId);
//	public Integer getMinutePOSAddLimit();
//	public void setMinutePOSAddLimit(Integer minutePOSAddLimit);
	public Integer getMinutePOSUpdateLimit();
	public void setMinutePOSUpdateLimit(Integer minutePOSUpdateLimit);
	public Integer getSsAccountOfficeId();
	public void setSsAccountOfficeId(Integer ssAccountOfficeId);
//	public Integer getMinutePOSDeleteLimit();
//	public void setMinutePOSDeleteLimit(Integer minutePOSDeleteLimit);
	
}
