package com.rtw.autopricing.ftp.indux.data;


import java.io.Serializable;
import java.util.Date;



public final class ExchangeEvent implements Serializable  {
	
	protected static final long serialVersionUID = -7402732543016575381L;
	
	protected Integer id;
	protected Integer venueId;
	protected String eventName;
	protected String venueName;
	protected String city;
	protected String state;
	protected Integer parentId;
	protected Integer grandChildId;
	protected Date eventDate;
	protected String eventDateStr;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Integer getGrandChildId() {
		return grandChildId;
	}

	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}

	public String getEventDateStr() {
		return eventDateStr;
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	
	
	
}