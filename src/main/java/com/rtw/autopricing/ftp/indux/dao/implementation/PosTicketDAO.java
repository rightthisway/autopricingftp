package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.indux.data.POSTicketGroup;
import com.rtw.autopricing.ftp.indux.data.PosTicket;
import com.rtw.autopricing.util.connection.Connections;

public class PosTicketDAO implements com.rtw.autopricing.ftp.indux.dao.services.PosTicketDAO {
	
	
	public List<Integer> getAllDistinctTicketGroupIdByPurchaseOrderId(Integer brokerId,Integer purchaseOrderId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId); 
		
		String sql = "SELECT distinct ticket_group_id FROM ticket where purchase_order_id=?";
		
		ResultSet resultSet =null;
		List<Integer> ticketGroupIds = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, purchaseOrderId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				ticketGroupIds.add(resultSet.getInt("ticket_group_id"));
			}
			return ticketGroupIds;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}
	
	
	public void updateAllTicketsPrice(Integer brokerId,List<POSTicketGroup> pOSTicketGroups) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId);
		String updateSql = "UPDATE ticket SET wholesale_price=?,retail_price=? WHERE ticket_group_id = ? ";
		int[] updateResult ;
		try{
			connection.setAutoCommit(false);
			PreparedStatement updateStatement = connection.prepareStatement(updateSql);
			int u=0;
			for(POSTicketGroup pOSTicketGroup: pOSTicketGroups){
				u++;
				updateStatement.setDouble(1, pOSTicketGroup.getRetailPrice());
				updateStatement.setDouble(2, pOSTicketGroup.getRetailPrice());
				updateStatement.setInt(3, pOSTicketGroup.getTicketGroupId());
				updateStatement.addBatch();
			}
			int result=0;
			if(u>0){
				updateResult = updateStatement.executeBatch();
				result+= updateResult.length;
			}
			connection.commit();
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			
		}
	}
	
	
	public void updateTicketsPriceZeroByTicketGroupId(Integer brokerId,List<POSTicketGroup> pOSTicketGroups) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId);
		String updateSql = "UPDATE ticket SET wholesale_price=?,retail_price=? WHERE ticket_group_id = ? ";
		int[] updateResult ;
		try{
			connection.setAutoCommit(false);
			PreparedStatement updateStatement = connection.prepareStatement(updateSql);
			int u=0;
			for(POSTicketGroup pOSTicketGroup: pOSTicketGroups){
				u++;
				updateStatement.setDouble(1, 0f);
				updateStatement.setDouble(2, 0f);
				updateStatement.setInt(3, pOSTicketGroup.getTicketGroupId());
				updateStatement.addBatch();
			}
			int result=0;
			if(u>0){
				updateResult = updateStatement.executeBatch();
				result+= updateResult.length;
			}
			connection.commit();
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
		}
	}
	
	public Integer getDistinctPurchaseOrderIdByicketgRoupId(Integer brokerId,Integer ticketGroupId) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT distinct purchase_order_id FROM ticket where ticket_group_id=?";
		ResultSet resultSet =null;
		Integer purchaseOrderId = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, ticketGroupId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				purchaseOrderId = resultSet.getInt("purchase_order_id");
			}
			return purchaseOrderId;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}
	
	
	public List<PosTicket> getAllTicketsByTicketGroupId(Integer brokerId,Integer ticketGroupId) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT * FROM ticket where ticket_group_id=?";
		
		ResultSet resultSet =null;
		List<PosTicket> tickets = new ArrayList<PosTicket>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, ticketGroupId);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				PosTicket ticket = new PosTicket();
				ticket.setActualSoldPrice(resultSet.getDouble("actual_sold_price"));
				ticket.setCost(resultSet.getDouble("cost"));
				ticket.setFacePrice(resultSet.getDouble("face_price"));
				ticket.setGeneralDesc(resultSet.getString("general_desc"));
				ticket.setInvoiceId(resultSet.getInt("invoice_id"));
				ticket.setOriginalSection(resultSet.getString("original_section"));
				ticket.setPurchaseOrderId(resultSet.getInt("purchase_order_id"));
				ticket.setRetailPrice(resultSet.getDouble("retail_price"));
				ticket.setRow(resultSet.getString("row"));
				ticket.setSeatNumber(resultSet.getInt("seat_number"));
				ticket.setSeatOrder(resultSet.getInt("seat_order"));
				ticket.setSection(resultSet.getString("section"));
				ticket.setStatusId(resultSet.getInt("status_id"));
				ticket.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				ticket.setTicketId(resultSet.getInt("ticket_id"));
				ticket.setWholesalePrice(resultSet.getDouble("wholesale_price"));
				tickets.add(ticket);
			}
			return tickets;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}
	
	public boolean saveAll(Integer brokerId,List<PosTicket> pOSTickets) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId);
		
		String insertTicketSql = "INSERT INTO ticket " +
           " (section,row,seat_number,retail_price,face_price,cost,seat_order " +
           " ,wholesale_price,actual_sold_price,general_desc,status_id,create_date " +
           " ,expected_arrival_date,ticket_group_id,system_user_id,ticket_on_hand_status_id ,purchase_order_id" +
//         ",mod_system_user_id,original_section "
           ") " +
           " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement insertTicketStatement =null;
		try{
			Date now = new Date(new java.util.Date().getTime());
			connection.setAutoCommit(false);
			insertTicketStatement = connection.prepareStatement(insertTicketSql);
			
			int seatOrder = 1;
			for(PosTicket pOSTicket : pOSTickets){
				insertTicketStatement.setString(1, pOSTicket.getSection());
				insertTicketStatement.setString(2, pOSTicket.getRow());
				insertTicketStatement.setInt(3, pOSTicket.getSeatNumber());
				insertTicketStatement.setDouble(4, pOSTicket.getRetailPrice());
				insertTicketStatement.setDouble(5, pOSTicket.getFacePrice());
				insertTicketStatement.setDouble(6, pOSTicket.getCost());
				insertTicketStatement.setDouble(7, seatOrder++);
				insertTicketStatement.setDouble(8, pOSTicket.getWholesalePrice());
				insertTicketStatement.setDouble(9, -1); //pOSTicket.getActualSoldPrice()
				insertTicketStatement.setString(10, "n/a");
				insertTicketStatement.setInt(11, 1);
				insertTicketStatement.setDate(12, now);
				insertTicketStatement.setDate(13, new Date(pOSTicket.getExpectedArrivalDate().getTime()));
				insertTicketStatement.setInt(14, pOSTicket.getTicketGroupId());
				insertTicketStatement.setInt(15, pOSTicket.getSystemUserId());
				insertTicketStatement.setInt(16, 2);
				insertTicketStatement.setInt(17, pOSTicket.getPurchaseOrderId());
//				insertTicketStatement.setInt(18, pOSTicket.getSystemUserId());
//				insertTicketStatement.setString(19, pOSTicket.getSection());
				insertTicketStatement.addBatch();
			}
			insertTicketStatement.executeBatch();
			connection.commit();
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(insertTicketStatement!=null){
				insertTicketStatement.close();
			}
		}
		//return false;
	}
	
	
}
