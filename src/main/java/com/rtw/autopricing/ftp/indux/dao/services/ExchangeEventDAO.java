package com.rtw.autopricing.ftp.indux.dao.services;

import com.rtw.autopricing.ftp.dao.services.RootDAO;
import com.rtw.autopricing.ftp.indux.data.ExchangeEvent;

public interface ExchangeEventDAO extends RootDAO<Integer, ExchangeEvent>{
	public ExchangeEvent getExchangeEventById(Integer brokerId,Integer exchangeEventId) throws Exception;
}
