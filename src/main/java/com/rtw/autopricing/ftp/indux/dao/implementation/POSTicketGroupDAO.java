package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.indux.data.Broker;
import com.rtw.autopricing.ftp.indux.data.POSTicketGroup;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.ftp.indux.data.PosTicket;
import com.rtw.autopricing.util.AutoExchangeEventLoader;
import com.rtw.autopricing.util.BrokerUtils;
import com.rtw.autopricing.util.CategoryTicket;
import com.rtw.autopricing.util.connection.Connections;

public class POSTicketGroupDAO implements com.rtw.autopricing.ftp.indux.dao.services.POSTicketGroupDAO {
	
	/*public List<Integer> getEventIdsByOfficeId(Integer brokerId,Integer officeId) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId);  //356692,
		String sql = "SELECT distinct event_id FROM ticket_group WITH(NOLOCK) where office_id=? and internal_notes='LarryLast NOSTUB NOTNOW NOVIVID NOTEVO' ";
		ResultSet resultSet =null;
		List<Integer> eventIds = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, officeId);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				eventIds.add(resultSet.getInt("event_id"));
			}
			return eventIds;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}*/
	
	
	
	public POSTicketGroup getTicketGroupById(Integer brokerId,Integer ticketGroupId) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT * FROM ticket_group WITH(NOLOCK) where ticket_group_id=? ";
		
		ResultSet resultSet =null;
		POSTicketGroup pOSTicketGroup = new POSTicketGroup();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, ticketGroupId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				pOSTicketGroup.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				pOSTicketGroup.setFacePrice(resultSet.getDouble("face_price"));
				pOSTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
			}
			return pOSTicketGroup;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	/*public List<POSTicketGroup> getAllLarryTicketGroupsByExchangeEventId(Integer brokerId,Integer exchangeEventId,Integer officeId) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT * FROM ticket_group WITH(NOLOCK) WHERE office_id = " + officeId + " AND exchange_event_id=? and internal_notes='LarryLast NOSTUB NOTNOW NOVIVID NOTEVO' order by ticket_group_id ";//AND retail_price != 0
		
		ResultSet resultSet =null;
		List<POSTicketGroup> pOSTicketGroups = new ArrayList<POSTicketGroup>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, exchangeEventId);
//			statement.setInt(2, eventId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				POSTicketGroup pOSTicketGroup = new POSTicketGroup();
				pOSTicketGroup.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				pOSTicketGroup.setRetailPrice(resultSet.getDouble("retail_price"));
				pOSTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
				pOSTicketGroup.setShippingMethodSpecialId(resultSet.getInt("shipping_method_special_id"));
				pOSTicketGroup.setShowNearTermOptionId(resultSet.getInt("show_near_term_option_id"));
				pOSTicketGroups.add(pOSTicketGroup);
			}
			return pOSTicketGroups;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}*/
	
	public boolean addTicketGroupInPOS(Integer brokerId,CategoryTicket tix,AutopricingProduct product,PosEvent posEvent,Integer purchaseOrderId) throws Exception{
		java.util.Date now = new java.util.Date();
		 POSTicketGroup ticketGroup = new POSTicketGroup();
		 Broker broker = BrokerUtils.getBrokerByPOSBrokerId(brokerId);
		 if(broker==null){
			 return false;
		 }
//		 Calendar cal = Calendar.getInstance();
//			cal.setTime(posEvent.getEventDate());
//			//cal.add(Calendar.DATE, -4);
//			cal.add(Calendar.DAY_OF_MONTH, - (null != event.getShippingDays()?event.getShippingDays():5));
//			java.util.Date arrivedDate = cal.getTime();
			ticketGroup.setTicketGroupExposureId(1);
			ticketGroup.setArrivedOnDate(tix.getExpectedArrivalDate());
			ticketGroup.setOriginalTicketCount(tix.getQuantity());
			ticketGroup.setRemainingTicketCount(tix.getQuantity());
			ticketGroup.setRetailPrice(tix.getTnPrice());
			ticketGroup.setFacePrice(0d);
			ticketGroup.setCost(0d);
			ticketGroup.setWholesalePrice(tix.getTnPrice());
			ticketGroup.setRow(tix.getLastRow());
			ticketGroup.setSection(tix.getSection()+".");
			ticketGroup.setCreatedDate(now);
			ticketGroup.setSystemUserId(broker.getSystemUserId()); // Pt-sk
			if(posEvent.getId()!=null && posEvent.getId()!=0){
				ticketGroup.setEventId(posEvent.getId());
			}
			
			if(tix.getQuantity() > 4) {
				ticketGroup.setNotes(product.getNotes());
			} else {
				ticketGroup.setNotes("");
			}
			ticketGroup.setStatusId(1);
			ticketGroup.setTicketGroupSeatingTypeId(2);
			ticketGroup.setTicketGroupId(1);
			ticketGroup.setClientBrokerId(broker.getBuyerBrokerId());  //Larry Last
			ticketGroup.setClientBrokerEmployeeId(broker.getClientBrokerEmployeeId()); //183377
			ticketGroup.setInternalNotes(tix.getInternalNotes());
			ticketGroup.setActualPurchaseDate(now);
			ticketGroup.setAutoProcessWebRequests(1);  // Auto procees web request set to true.
//			ticketGroup.setTaxExempt(false);
//			ticketGroup.setEbayAuction(false);
			ticketGroup.setEbayAuctionNumber("");
			ticketGroup.setUpdateDateTime(now);
			ticketGroup.setNotOnHandMessage("");
			ticketGroup.setShowTndNonTnd(3); // Need to varify
			ticketGroup.setOfficeId(broker.getSsAccountOfficeId()); // 2 is Office Id for JKT
//			ticketGroup.setDropSale(false);
			ticketGroup.setExchangeEventId(posEvent.getExchangeEventId());
			ticketGroup.setExchangeTicketGroupId(-1);
			ticketGroup.setUnbroadcastDaysBeforeEvent(0);
			ticketGroup.setTicketGroupStockTypeId(1);
			ticketGroup.setShippingMethodSpecialId(tix.getShippingMethodSpecialId());
			ticketGroup.setShowNearTermOptionId(tix.getNearTermOptionId());
			ticketGroup.setTgNoteGrandFathered(0);
//			ticketGroup.setEticketInstantDownload(false);
//			ticketGroup.setEticketStoredLocally(false);
//			ticketGroup.setOverrideInstantDownloadPause(false);
			Integer ticketGroupId = InduxDAORegistry.getPosTicketGroupDAO().save(brokerId,ticketGroup);
			if(ticketGroupId == -1){
				return false;
			}
			tix.setPosPurchaseOrderId(purchaseOrderId);
			tix.setTnTicketGroupId(ticketGroupId);
			
			// Add ticket
			int size =tix.getQuantity();
			int seatNumber = 1000 * size;
			List<PosTicket> posTickets = new ArrayList<PosTicket>();
			for(int i=1;i<=size;i++){
				PosTicket posTicket = new PosTicket();
				posTicket.setSection(tix.getSection()+".");
				posTicket.setRow(tix.getLastRow());
				posTicket.setSeatNumber(seatNumber + i);
				posTicket.setSeatOrder(i);	
				posTicket.setRetailPrice(tix.getTnPrice());
				posTicket.setFacePrice(0d);
				posTicket.setCost(0d);
				posTicket.setWholesalePrice(tix.getTnPrice());
				posTicket.setGeneralDesc("n/a");
				posTicket.setStatusId(1);
				posTicket.setExpectedArrivalDate(tix.getExpectedArrivalDate());
				posTicket.setTicketGroupId(ticketGroupId);
				posTicket.setSystemUserId(broker.getSystemUserId());
				posTicket.setModSystemUserId(broker.getSystemUserId());
				posTicket.setOriginalSection(tix.getSection()+".");
				posTicket.setPurchaseOrderId(purchaseOrderId);
				posTicket.setTicketOnHandStausId(2);
				posTicket.setCreateDate(now);
				posTickets.add(posTicket);
			}
			InduxDAORegistry.getPosTicketDAO().saveAll(brokerId,posTickets);
//			boolean res = false;//;InduxDAORegistry.getPosTicketDAO().saveAll(brokerId,posTickets);
			//Counter.MINUTE_ADD_COUNTER ++;
			//Counter.ADD_COUNTER++;
			return true;
	 }
	
	public int updateTicketGroup(Integer brokerId,CategoryTicket catTix) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId);
	
		String updateTicketGroupSql = " UPDATE ticket_group SET wholesale_price=?,retail_price=?,shipping_method_special_id = ?,show_near_term_option_id=?,arrived_on_date=? " +
				" WHERE ticket_group_id = ? AND ticket_group_id not in (select ticket_group_id from ticket where invoice_id is not null and ticket_group_id=?)";
		String updateTicketSql = " UPDATE ticket SET wholesale_price=?,retail_price=?,expected_arrival_date=? WHERE ticket_group_id = ? " +
			" AND ticket_group_id not in (select ticket_group_id from ticket where invoice_id is not null and ticket_group_id=?)";
		
		try{
			connection.setAutoCommit(false);
			PreparedStatement updateTicketGroupStatement = connection.prepareStatement(updateTicketGroupSql);
			PreparedStatement updateTicketStatement = connection.prepareStatement(updateTicketSql);
			
			updateTicketGroupStatement.setDouble(1, catTix.getTnPrice());
			updateTicketGroupStatement.setDouble(2, catTix.getTnPrice());
			updateTicketGroupStatement.setInt(3, catTix.getShippingMethodSpecialId());
			updateTicketGroupStatement.setInt(4, catTix.getNearTermOptionId());
			updateTicketGroupStatement.setDate(5, new Date(catTix.getExpectedArrivalDate().getTime()));
			updateTicketGroupStatement.setInt(6, catTix.getTnTicketGroupId());
			updateTicketGroupStatement.setInt(7, catTix.getTnTicketGroupId());
			updateTicketGroupStatement.executeUpdate();
				
			updateTicketStatement.setDouble(1, catTix.getTnPrice());
			updateTicketStatement.setDouble(2, catTix.getTnPrice());
			updateTicketStatement.setDate(3, new Date(catTix.getExpectedArrivalDate().getTime()));
			updateTicketStatement.setInt(4, catTix.getTnTicketGroupId());
			updateTicketStatement.setInt(5, catTix.getTnTicketGroupId());
			updateTicketStatement.executeUpdate();
					
			connection.commit();

		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
		}finally {

		}
		return 0;
	}
	
	public int deleteTicketGroupsByPosTicketGroupId(Broker broker,Integer ticketGroupId) throws Exception {
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId());
		
		String ticketHoldSQL =  " SELECT ticket_group_id FROM ticket_hold_queue WHERE  ticket_group_id = ? UNION  SELECT ticket_group_id FROM sales_rep_ticket_hold WHERE  ticket_group_id = ? ";
		
		PreparedStatement ticketHoldStatement = null;
		ticketHoldStatement =  connection.prepareStatement(ticketHoldSQL);
		ticketHoldStatement.setInt(1, ticketGroupId);
		ticketHoldStatement.setInt(2, ticketGroupId);
		ResultSet rs = null;
		rs = ticketHoldStatement.executeQuery();
		if(rs!=null && rs.next()){
			return -1;
		}
		
		String deleteTicketGroupDistributionMethodSql = "DELETE FROM ticket_group_distribution_method where ticket_group_id=? " ;
		//" and ticket_group_id not in(select ticket_group_id from ticket where invoice_id is not null and ticket_group_id=?)" +
//		"";
		
		String deleteTicketSql = " DELETE FROM ticket WHERE ticket_group_id = ? and invoice_id is null" +
				" and ticket_group_id in(select ticket_group_id from ticket_group where ticket_group_id=? and office_id= " + broker.getSsAccountOfficeId() + " )";
		
		//Tamil : need to confirm with amit
		//String deleteTicketHoldQueueSql = " DELETE FROM ticket_hold_queue WHERE  ticket_group_id = ? ";
		
		//String deleteSalesRepTicketSql = " DELETE FROM sales_rep_ticket_hold WHERE  ticket_group_id = ? ";
			
		String deleteTicketGroupSql = " DELETE FROM ticket_group WHERE ticket_group_id = ? and office_id= " + broker.getSsAccountOfficeId() +
				" AND ticket_group_id not in (select ticket_group_id from ticket where invoice_id is not null and ticket_group_id=?)";
		
		
		PreparedStatement deleteTicketGroupStatement = null;
		PreparedStatement deleteTicketStatement = null;
//		PreparedStatement deleteTicketHoldQueueStatement = null;
//		PreparedStatement deleteSalesRepTicketStatement = null;
		PreparedStatement deleteTicketGroupDistributionChannelStatement = null;
		try{
			connection.setAutoCommit(false);
			
			deleteTicketGroupDistributionChannelStatement = connection.prepareStatement(deleteTicketGroupDistributionMethodSql);
			deleteTicketGroupDistributionChannelStatement.setInt(1, ticketGroupId);
			//deleteTicketGroupDistributionChannelStatement.setInt(2, ticketGroupId);
			deleteTicketGroupDistributionChannelStatement.executeUpdate();
			
			/*deleteTicketHoldQueueStatement = connection.prepareStatement(deleteTicketHoldQueueSql);
			deleteTicketHoldQueueStatement.setInt(1, ticketGroupId);
			deleteTicketHoldQueueStatement.executeUpdate();
			
			deleteSalesRepTicketStatement = connection.prepareStatement(deleteSalesRepTicketSql);
			deleteSalesRepTicketStatement.setInt(1, ticketGroupId);
			deleteSalesRepTicketStatement.executeUpdate();*/
			
			deleteTicketStatement = connection.prepareStatement(deleteTicketSql);
			deleteTicketStatement.setInt(1, ticketGroupId);
			deleteTicketStatement.setInt(2, ticketGroupId);
			deleteTicketStatement.executeUpdate();
			
			
			
			deleteTicketGroupStatement = connection.prepareStatement(deleteTicketGroupSql);
			deleteTicketGroupStatement.setInt(1, ticketGroupId);
			deleteTicketGroupStatement.setInt(2, ticketGroupId);
			deleteTicketGroupStatement.executeUpdate();
				
			connection.commit();
			return 0 ;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			try{
				if(rs !=null){
					rs.close();
				}
			}catch (Exception e2) {}
			try{
				if(ticketHoldStatement !=null){
					ticketHoldStatement.close();
				}
			}catch (Exception e2) {}
			if(deleteTicketGroupDistributionChannelStatement !=null){
				deleteTicketGroupDistributionChannelStatement.close();
			}
			/*if(deleteTicketHoldQueueStatement !=null){
				deleteTicketHoldQueueStatement.close();
			}
			if(deleteSalesRepTicketStatement !=null){
				deleteSalesRepTicketStatement.close();
			}*/
			if(deleteTicketStatement !=null){
				deleteTicketGroupStatement.close();
			}
			
			if(deleteTicketGroupStatement !=null){
				deleteTicketGroupStatement.close();
			}
			
		}
		//return 0;
	}
	
	public int save(Integer brokerId,POSTicketGroup pOSTicketGroup) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId);
		/*
		String insertTicketGroupSql = "INSERT INTO ticket_group " +
           " (ticket_group_exposure_type_id,arrived_on_date,original_ticket_count,remaining_ticket_count " + 
           " ,retail_price,face_price,cost,wholesale_price,row,section,notes,create_date,system_user_id,event_id,status_id " + 
           " ,ticket_group_seating_type_id,ticket_group_type_id,client_broker_id,client_broker_employee_id,internal_notes " + 
           " ,actual_purchase_date,auto_process_web_requests,tax_exempt,ebay_auction,ebay_auction_number,update_datetime,not_on_hand_message " + 
           " ,show_tnd_nontnd,office_id,isDropSale,exchange_event_id " + 
           " ,exchange_ticket_group_id,unbroadcast_days_before_event,ticket_group_stock_type_id,shipping_method_special_id,show_near_term_option_id " + 
           " ,tg_note_grandfathered,eticket_instant_download " + 
           " ,etickets_stored_locally ,override_instant_download_pause) " +
           " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		*/
		String eventId="";
		String eventValue = "";
		if(pOSTicketGroup.getEventId()!=null){
        	eventId = "event_id," ;
        	eventValue = pOSTicketGroup.getEventId().toString()+",";
        }
		String sql = "INSERT INTO ticket_group " +
        " (ticket_group_exposure_type_id,arrived_on_date,original_ticket_count,remaining_ticket_count " + 
        " ,retail_price,face_price,cost,wholesale_price,row,section,notes,create_date,system_user_id," +
        eventId + "status_id " + 
        " ,ticket_group_seating_type_id,ticket_group_type_id,client_broker_id,client_broker_employee_id,internal_notes " + 
        " ,actual_purchase_date,auto_process_web_requests,tax_exempt,ebay_auction,ebay_auction_number,update_datetime,not_on_hand_message " + 
        " ,show_tnd_nontnd,office_id,isDropSale " + 
        " ,unbroadcast_days_before_event,ticket_group_stock_type_id,shipping_method_special_id,show_near_term_option_id " + 
        " ,tg_note_grandfathered,eticket_instant_download " + 
        " ,etickets_stored_locally ,override_instant_download_pause,split_rule_id) " +
        //,row_version
        " VALUES (" + pOSTicketGroup.getTicketGroupExposureId() +
        ",'" + new Date(pOSTicketGroup.getArrivedOnDate().getTime()) +
        "'," + pOSTicketGroup.getOriginalTicketCount()  +
        "," + pOSTicketGroup.getRemainingTicketCount() +
        "," + pOSTicketGroup.getRetailPrice() + 
        "," + pOSTicketGroup.getFacePrice() + "," + pOSTicketGroup.getCost() + "," + pOSTicketGroup.getWholesalePrice() + 
        ",'" + pOSTicketGroup.getRow() + "','" + pOSTicketGroup.getSection() + "','" + pOSTicketGroup.getNotes() + 
        "',GETDATE()," + pOSTicketGroup.getSystemUserId() + "," + eventValue + pOSTicketGroup.getStatusId() + 
        "," + pOSTicketGroup.getTicketGroupSeatingTypeId() + "," + pOSTicketGroup.getTicketGroupId()+ "," + pOSTicketGroup.getClientBrokerId() + "," +
        pOSTicketGroup.getClientBrokerEmployeeId() + 
        ",'" + pOSTicketGroup.getInternalNotes() + "',GETDATE()," + pOSTicketGroup.getAutoProcessWebRequests() + 
        ",0,0,'" + pOSTicketGroup.getEbayAuctionNumber() + 
        "','" + new Date(pOSTicketGroup.getUpdateDateTime().getTime()) + 
        "','" + pOSTicketGroup.getNotOnHandMessage() + "'," + pOSTicketGroup.getShowTndNonTnd() + "," + pOSTicketGroup.getOfficeId() + 
        ",0," + pOSTicketGroup.getUnbroadcastDaysBeforeEvent() + "," + pOSTicketGroup.getTicketGroupStockTypeId() + 
        "," + pOSTicketGroup.getShippingMethodSpecialId() + "," + pOSTicketGroup.getShowNearTermOptionId() + 
        "," + pOSTicketGroup.getTgNoteGrandFathered() + 
        " ,0,0,0,1)";
		//'0x000000011BBE224E'
		//System.out.println(sql);
		ResultSet rs= null;
		Statement insertTicketGroupStatement =null;
		Statement insertTicketGroupDistMethodStatement1 =null;
		Statement insertTicketGroupDistMethodStatement2 =null;
		try{
//			Date now = new Date(new java.util.Date().getTime());
			connection.setAutoCommit(false);
			insertTicketGroupStatement = connection.createStatement();
			
			/*PreparedStatement insertTicketGroupStatement = connection.prepareStatement(insertTicketGroupSql);
			insertTicketGroupStatement.setDouble(1, pOSTicketGroup.getTicketGroupExposureId());
			insertTicketGroupStatement.setDate(2, new Date(pOSTicketGroup.getArrivedOnDate().getTime()));
			insertTicketGroupStatement.setInt(3, pOSTicketGroup.getOriginalTicketCount());
			insertTicketGroupStatement.setInt(4, pOSTicketGroup.getRemainingTicketCount());
			insertTicketGroupStatement.setDouble(5, pOSTicketGroup.getRetailPrice());
			insertTicketGroupStatement.setDouble(6, pOSTicketGroup.getFacePrice());
			insertTicketGroupStatement.setDouble(7, pOSTicketGroup.getCost());
			insertTicketGroupStatement.setDouble(8, pOSTicketGroup.getWholesalePrice());
			insertTicketGroupStatement.setString(9, pOSTicketGroup.getRow());
			insertTicketGroupStatement.setString(10, pOSTicketGroup.getSection());
			insertTicketGroupStatement.setString(11, pOSTicketGroup.getNotes());
			insertTicketGroupStatement.setDate(12, now);
			insertTicketGroupStatement.setInt(13, pOSTicketGroup.getSystemUserId());
			insertTicketGroupStatement.setInt(14, pOSTicketGroup.getEventId());
			insertTicketGroupStatement.setInt(15, pOSTicketGroup.getStatusId());
			insertTicketGroupStatement.setInt(16, pOSTicketGroup.getTicketGroupSeatingTypeId());
			insertTicketGroupStatement.setInt(17, pOSTicketGroup.getTicketGroupId());
			insertTicketGroupStatement.setInt(18, pOSTicketGroup.getClientBrokerId());
			insertTicketGroupStatement.setInt(19, pOSTicketGroup.getClientBrokerEmployeeId());
			insertTicketGroupStatement.setString(20, pOSTicketGroup.getInternalNotes());
			insertTicketGroupStatement.setDate(21, now);
			insertTicketGroupStatement.setInt(22, pOSTicketGroup.getAutoProcessWebRequests());
			insertTicketGroupStatement.setBoolean(23, pOSTicketGroup.getTaxExempt());
			insertTicketGroupStatement.setBoolean(24, pOSTicketGroup.getEbayAuction());
			insertTicketGroupStatement.setString(25, pOSTicketGroup.getEbayAuctionNumber());
			insertTicketGroupStatement.setDate(26, new Date(pOSTicketGroup.getUpdateDateTime().getTime()));
			insertTicketGroupStatement.setString(27, pOSTicketGroup.getNotOnHandMessage());
			insertTicketGroupStatement.setInt(28, pOSTicketGroup.getShowTndNonTnd());
			insertTicketGroupStatement.setInt(29, pOSTicketGroup.getOfficeId());
			insertTicketGroupStatement.setBoolean(30, pOSTicketGroup.isDropSale());
			insertTicketGroupStatement.setInt(31, pOSTicketGroup.getExchangeEventId());
			insertTicketGroupStatement.setInt(32, -1);
			insertTicketGroupStatement.setInt(33, pOSTicketGroup.getUnbroadcastDaysBeforeEvent());
			insertTicketGroupStatement.setInt(34, pOSTicketGroup.getTicketGroupStockTypeId());
			insertTicketGroupStatement.setInt(35, pOSTicketGroup.getShippingMethodSpecialId());
			insertTicketGroupStatement.setInt(36, pOSTicketGroup.getShowNearTermOptionId());
			insertTicketGroupStatement.setBoolean(37, pOSTicketGroup.getTgNoteGrandFathered());
			insertTicketGroupStatement.setBoolean(38, pOSTicketGroup.getEticketInstantDownload());
			insertTicketGroupStatement.setBoolean(39, pOSTicketGroup.getEticketStoredLocally());
			insertTicketGroupStatement.setBoolean(40, pOSTicketGroup.getOverrideInstantDownloadPause());
			*/
			
			insertTicketGroupStatement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
			rs = insertTicketGroupStatement.getGeneratedKeys();
			Integer ticketGroupId =null;
			if(rs.next()){
				ticketGroupId = rs.getInt(1);
			}
			String ticketGroupDistributionMethod1 = "INSERT INTO ticket_group_distribution_method(ticket_group_distribution_channel_id,ticket_group_id,create_date,status_id,system_user_id)" +
					" VALUES(1,"+ticketGroupId+",GETDATE(),1," +  pOSTicketGroup.getSystemUserId() +")";
			insertTicketGroupDistMethodStatement1 = connection.createStatement();
			insertTicketGroupDistMethodStatement1.executeUpdate(ticketGroupDistributionMethod1);
			
			String ticketGroupDistributionMethod2 = "INSERT INTO ticket_group_distribution_method(ticket_group_distribution_channel_id,ticket_group_id,create_date,status_id,system_user_id)" +
			" VALUES(2,"+ticketGroupId+",GETDATE(),1," + pOSTicketGroup.getSystemUserId() +")";
			insertTicketGroupDistMethodStatement2 = connection.createStatement();
			insertTicketGroupDistMethodStatement2.executeUpdate(ticketGroupDistributionMethod2);
			
			connection.commit();
			return ticketGroupId ;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertTicketGroupStatement!=null){
				insertTicketGroupStatement.close();
			}
			if(insertTicketGroupDistMethodStatement1!=null){
				insertTicketGroupDistMethodStatement1.close();
			}
			if(insertTicketGroupDistMethodStatement2!=null){
				insertTicketGroupDistMethodStatement2.close();
			}
		}
		return -1;
	}
	
	public List<Integer> getSoldTicketGroupIds(String fromDateStr,String toDateStr,String internalNotesStr,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId);
		String sql = "select distinct tg.ticket_group_id from ticket_group tg " +
				" inner join ticket t on t.ticket_group_id=tg.ticket_group_id " +
				" inner join invoice i on i.invoice_id=t.invoice_id " +
				" where tg.internal_notes in("+internalNotesStr+") and i.create_date>='"+fromDateStr+"' and i.create_date<='"+toDateStr+"' ";
				
		ResultSet resultSet =null;
		List<Integer> ticketGroupIdList = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				ticketGroupIdList.add(resultSet.getInt("ticket_group_id"));
			}
			return ticketGroupIdList;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
		}
		return ticketGroupIdList;
	}
	/*public Integer getLarryLastActiveTicketsCountinPOS(Integer brokerId,String posEventTypeIdStr) throws Exception {
		
		Connection connection = Connections.getInduxConnection(brokerId);  
		String sql = " select count(tg.ticket_group_id) as tixCount from ticket_group tg" +
				" inner join exchange_event ee on ee.id=tg.exchange_event_id" +
				" where tg.retail_price > 0 and tg.office_id=2 and tg.internal_notes='LarryLast NOSTUB NOTNOW NOVIVID NOTEVO'" +
				" and tg.ticket_group_id not in(select ticket_group_id from ticket where invoice_id is not null)" +
				" and ticket_group_id in(select distinct ticket_group_id from ticket_group_distribution_method)";
		
		if(posEventTypeIdStr != null) {
			sql = sql + " and ee.parentId in("+posEventTypeIdStr+")";
		}
				
				
		ResultSet resultSet =null;
		Integer tixCount = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				tixCount = tixCount + resultSet.getInt("tixCount");
			}
			return tixCount;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
		}
		return null;
	}*/
	
	public Integer getAllActiveTicketsCountinPOS(Broker broker) throws Exception{
		
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId());  
		String sql = "select count(category_ticket_group_id) as tixCount from category_ticket_group " +
				" where retail_price > 0 and broadcast=1 and category_ticket_group_id not in(select category_ticket_group_id from category_ticket where invoice_id is not null) " +
				" union " +
				" select count(ticket_group_id) as tixCount from ticket_group " +
				" where retail_price > 0 and office_id= " + broker.getSsAccountOfficeId() +//and internal_notes in('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO')
				" and ticket_group_id not in(select ticket_group_id from ticket where invoice_id is not null)" +
				" and ticket_group_id in(select distinct ticket_group_id from ticket_group_distribution_method)";
				
				
		ResultSet resultSet =null;
		Integer tixCount = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				tixCount = tixCount + resultSet.getInt("tixCount");
			}
			return tixCount;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
		}
		return null;
	}
	public Integer getPurchaseOrderIdByExchangeEventId(Broker broker,Integer exchangeEventId,AutopricingProduct autopricingProduct) throws Exception {
		
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId()); 
			String sql = "select distinct purchase_order_id as poId from ticket_group tg WITH(NOLOCK) " +
					" inner join ticket t on t.ticket_group_id=tg.ticket_group_id " +
					" WHERE tg.event_id IN (SELECT min(event_id) FROM event WITH(NOLOCK) WHERE exchange_event_id = ?) and  tg.internal_notes='"+autopricingProduct.getInternalNotes()+"' " +//tg.internal_notes='LarryLast NOSTUB NOTNOW NOVIVID NOTEVO'
					" and tg.office_id= " + broker.getSsAccountOfficeId() + " and t.purchase_order_id is not null ";
					
					
					
			ResultSet resultSet =null;
			Integer purchaseOrderId = null;
			try {
				PreparedStatement statement = connection.prepareStatement(sql);
				statement.setInt(1, exchangeEventId);
				resultSet = statement.executeQuery();
				if(resultSet.next()){
					purchaseOrderId = resultSet.getInt("poId");
				}
				return purchaseOrderId;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{
			}
			//return null;
		}
	
public Integer getPurchaseOrderIdByExchangeEventIdForSSListing(Broker broker,Integer exchangeEventId) throws Exception {
		
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId()); 
			String sql = "select distinct purchase_order_id as poId from ticket_group tg WITH(NOLOCK) " +
					" inner join ticket t on t.ticket_group_id=tg.ticket_group_id " +
					" WHERE tg.event_id IN (SELECT min(event_id) FROM event WITH(NOLOCK) WHERE exchange_event_id = ?) " +
					" and  tg.internal_notes in ('"+AutoExchangeEventLoader.LARRYLAST_INTERNAL_NOTES+ "','"+AutoExchangeEventLoader.ZONED_LASTROW_INTERNAL_NOTES+ "')" +//and  tg.internal_notes='"+autopricingProduct.getInternalNotes()+"'
					" and tg.office_id= " + broker.getSsAccountOfficeId() + " and t.purchase_order_id is not null ";
					
					
					
			ResultSet resultSet =null;
			Integer purchaseOrderId = null;
			try {
				PreparedStatement statement = connection.prepareStatement(sql);
				statement.setInt(1, exchangeEventId);
				resultSet = statement.executeQuery();
				if(resultSet.next()){
					purchaseOrderId = resultSet.getInt("poId");
				}
				return purchaseOrderId;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{
			}
			//return null;
		}
	/*public List<Integer> getTicketGroupIdsByEventIds(String eventIds,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId);
		String sql = "select ticket_group_id from ticket_group where event_id in (" + eventIds + ")";
				
		ResultSet resultSet =null;
		List<Integer> ticketGroupIdList = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				ticketGroupIdList.add(resultSet.getInt("ticket_group_id"));
			}
			return ticketGroupIdList;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
		}
		return ticketGroupIdList;
	}*/
	
	public boolean unbroadcastTicketGroupsByEventIds(Integer brokerId,String eventIds) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId);
	
		String innerSql = "select ticket_group_id from ticket_group where event_id in (" + eventIds + ")";
		String deleteTicketGroupDistributionMethodSql = "DELETE FROM ticket_group_distribution_method where ticket_group_id in (" + innerSql + ")" ;
		System.out.println("Final Query.."+ deleteTicketGroupDistributionMethodSql);
		try{
			connection.setAutoCommit(false);
			PreparedStatement updateTicketGroupStatement = connection.prepareStatement(deleteTicketGroupDistributionMethodSql);
			
			updateTicketGroupStatement.executeUpdate();
			connection.commit();
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			return false;
		}finally {

		}
	}
	
	public boolean unbroadcastTicketGroupsByExchangeEventIds(Broker broker,String exchangeEventIds) throws Exception{
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId());
	
		String innerSql = "select ticket_group_id from ticket_group tg" +
				" inner join event e on e.event_id=tg.event_id" +
				" where tg.office_id="+broker.getSsAccountOfficeId()+" and internal_notes in('ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') " +
				" and exchange_event_id in (" + exchangeEventIds + ")";
		String deleteTicketGroupDistributionMethodSql = "DELETE FROM ticket_group_distribution_method where ticket_group_id in (" + innerSql + ")" ;
		//System.out.println("Final Query.."+ deleteTicketGroupDistributionMethodSql);
		try{
			connection.setAutoCommit(false);
			PreparedStatement updateTicketGroupStatement = connection.prepareStatement(deleteTicketGroupDistributionMethodSql);
			
			updateTicketGroupStatement.executeUpdate();
			connection.commit();
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			return false;
		}finally {

		}
	}


	public POSTicketGroup get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}



	public void saveOrUpdate(POSTicketGroup entity) {
		// TODO Auto-generated method stub
		
	}



	public void saveAll(Collection<POSTicketGroup> list) {
		// TODO Auto-generated method stub
		
	}



	public boolean updateAll(Collection<POSTicketGroup> list) {
		// TODO Auto-generated method stub
		return false;
	}



	public Collection<POSTicketGroup> getAll() {
		// TODO Auto-generated method stub
		return null;
	}



	public void saveOrUpdateAll(Collection<POSTicketGroup> list) {
		// TODO Auto-generated method stub
		
	}



	public void deleteAll(Collection<POSTicketGroup> List) {
		// TODO Auto-generated method stub
		
	}



	public void delete(POSTicketGroup entity) {
		// TODO Auto-generated method stub
		
	}



	public void update(POSTicketGroup entity) {
		// TODO Auto-generated method stub
		
	}

	
}
