package com.rtw.autopricing.ftp.indux.data;

import java.io.Serializable;


public class UserOffice implements Serializable {
	
	private Integer officeId; 
	private String officeDesc;
	
	public Integer getOfficeId() {
		return officeId;
	}
	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}
	public String getOfficeDesc() {
		return officeDesc;
	}
	public void setOfficeDesc(String officeDesc) {
		this.officeDesc = officeDesc;
	}
	
	
	
}
