package com.rtw.autopricing.ftp.indux.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;
import com.rtw.autopricing.util.CategoryTicket;

public interface PosVenueCategoryDAO {
	public Integer deleteByVenueCategoryId(Integer venueCategoryId,Integer brokerId) throws Exception;	
	public List<PosVenueCategory> getAllVenueCategorysByVenueId(Integer VenueId,Integer brokerId) throws Exception;
	public Integer save(CategoryTicket catTicket,Integer brokerId,AutopricingProduct product) throws Exception;
	public Integer getVenueCategoryId(CategoryTicket catTicket,Integer brokerId) throws Exception;
}
