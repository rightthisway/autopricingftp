package com.rtw.autopricing.ftp.indux.dao.services;

import java.sql.SQLException;
import java.util.List;

import com.rtw.autopricing.ftp.indux.data.POSTicketGroup;
import com.rtw.autopricing.ftp.indux.data.PosTicket;

public interface PosTicketDAO {
	
	public List<Integer> getAllDistinctTicketGroupIdByPurchaseOrderId(Integer brokerId,Integer purchaseOrderId) throws Exception;
	public void updateAllTicketsPrice(Integer brokerId,List<POSTicketGroup> pOSTicketGroups) throws Exception;
	public void updateTicketsPriceZeroByTicketGroupId(Integer brokerId,List<POSTicketGroup> pOSTicketGroups) throws Exception;
	public Integer getDistinctPurchaseOrderIdByicketgRoupId(Integer brokerId,Integer ticketGroupId) throws Exception;
	public List<PosTicket> getAllTicketsByTicketGroupId(Integer brokerId,Integer ticketGroupId) throws Exception;
	public boolean saveAll(Integer brokerId,List<PosTicket> pOSTickets) throws Exception;
	
}
