package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import com.rtw.autopricing.ftp.indux.data.ExchangeEvent;
import com.rtw.autopricing.util.connection.Connections;
//import com.rtw.autopricing.util.connection.Connections;

public class ExchangeEventDAO {
//	private Connections connections = new Connections();
	
	public ExchangeEvent getExchangeEventById(Integer brokerId,Integer exchangeEventId) throws Exception{
		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT * FROM exchange_event where Id=?";
		
		ResultSet resultSet =null;
		ExchangeEvent exchangeEvent = new ExchangeEvent();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, exchangeEventId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				exchangeEvent.setId(resultSet.getInt("Id"));
				exchangeEvent.setEventName(resultSet.getString("Event"));
				exchangeEvent.setEventDate(dbDateFormat.parse(resultSet.getString("EventDate")));
				exchangeEvent.setEventDateStr(dbDateFormat.format(dbDateFormat.parse(resultSet.getString("EventDate"))));
				exchangeEvent.setVenueId(resultSet.getInt("VId"));
				exchangeEvent.setVenueName(resultSet.getString("Venue"));
				exchangeEvent.setState(resultSet.getString("State"));
				exchangeEvent.setCity(resultSet.getString("City"));
				exchangeEvent.setParentId(resultSet.getInt("parentID"));
				exchangeEvent.setGrandChildId(resultSet.getInt("grandchildID"));
			}
			return exchangeEvent;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}
	
	
}
