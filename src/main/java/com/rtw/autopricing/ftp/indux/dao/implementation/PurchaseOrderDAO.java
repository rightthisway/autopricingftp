package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.indux.data.Broker;
import com.rtw.autopricing.ftp.indux.data.PurchaseOrder;
import com.rtw.autopricing.util.BrokerUtils;
import com.rtw.autopricing.util.connection.Connections;

public class PurchaseOrderDAO implements com.rtw.autopricing.ftp.indux.dao.services.PurchaseOrderDAO {
	
	public static final int JOE_KNOWS_TICKET = 7784;
	public static final int RESERVE_ONE_TICKET = 7790;
	
	public boolean deleteById(Integer brokerId,Integer purchaseOrderId) throws Exception{
		Integer shippingTrackingId = 0;
		
		ResultSet rsShippingTracking = null;
		Statement shippingTrackingStatement = null;
		Statement deleteShippingTrackingAddressStatement = null;
		Statement deletePurchaseOrderPaymentStatement = null;
		Statement deletePurchaseOrderStatement = null;
		Statement deleteShippingTrackingStatement = null;
		Connection connection = Connections.getInduxConnection(brokerId);
		try{
			String getShppingTrackingSql = " SELECT shipping_tracking_id FROM purchase_order WHERE purchase_order_id = " + purchaseOrderId ;
			shippingTrackingStatement = connection.createStatement();
			rsShippingTracking =  shippingTrackingStatement.executeQuery(getShppingTrackingSql);
			while(rsShippingTracking.next()){
				shippingTrackingId =  rsShippingTracking.getInt("shipping_tracking_id");
				break;
			}
			connection.setAutoCommit(false);
			String deleteShippingTrackingAddressSql =" DELETE FROM shipping_tracking_address WHERE shipping_tracking_id = "+ shippingTrackingId ;
			deleteShippingTrackingAddressStatement = connection.createStatement();
			deleteShippingTrackingAddressStatement.execute(deleteShippingTrackingAddressSql);
			
			String deletePurchaseOrderPaymentSql =" DELETE from purchase_order_payment where purchase_order_id = " + purchaseOrderId;
			deletePurchaseOrderPaymentStatement = connection.createStatement();
			deletePurchaseOrderPaymentStatement.execute(deletePurchaseOrderPaymentSql);
			
			String deletePurchaseOrderSql =" DELETE from purchase_order where purchase_order_id = " + purchaseOrderId;
			deletePurchaseOrderStatement = connection.createStatement();
			deletePurchaseOrderStatement.execute(deletePurchaseOrderSql);
			
			String deleteShippingTrackingSql =" DELETE from shipping_tracking WHERE shipping_tracking_id = "+ shippingTrackingId ;
			deleteShippingTrackingStatement = connection.createStatement();
			deleteShippingTrackingStatement.execute(deleteShippingTrackingSql);
			connection.commit();
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}
		//return false;
	}
	
	public Integer creatrePurchaseOrder(Integer brokerId) throws Exception{
		 Broker broker =BrokerUtils.getBrokerByPOSBrokerId(brokerId);
		 PurchaseOrder purchaseOrder = new PurchaseOrder();
		 purchaseOrder.setExternalPONumber("n/a");
//		 Date now = new Date();
//		 purchaseOrder.setBillDueDate(now);
		 purchaseOrder.setTotalShipping(0d);
		 purchaseOrder.setTotalExpenses(0d);
		 purchaseOrder.setTotalDue(0d);
		 purchaseOrder.setTotalTaxes(0d);
		 purchaseOrder.setPoTotal(0d);
		 purchaseOrder.setBalanceDue(0d);
		 purchaseOrder.setNotes("");
		 purchaseOrder.setPaymentTermId(7);  // Need to varify before production push..
		 purchaseOrder.setClientBrokerId(broker.getClientBrokerId());  //Larry Last
		 purchaseOrder.setBuyerBrokerId(broker.getBuyerBrokerId());
		 purchaseOrder.setClientBrokerEmployeeId(broker.getClientBrokerEmployeeId());
//		 purchaseOrder.setCreateDate(now);
		 purchaseOrder.setSystemUserId(broker.getSystemUserId());
		 purchaseOrder.setShippingTrackingId(-1);
		 purchaseOrder.setDropShipped(0);	
		 purchaseOrder.setDisplayedNotes("");
		 purchaseOrder.setIsEmailed(false);
		 purchaseOrder.setReprint(0);
		 purchaseOrder.setCurrencyId(1);
		 purchaseOrder.setDataCached(false);
		 purchaseOrder.setOnHandNotes("");
		 purchaseOrder.setTmOrderId("");
		 purchaseOrder.setTransOfficeId(broker.getSsAccountOfficeId());
		 purchaseOrder.setUserOfficeId(1);
		 Integer purchaserOrderId = InduxDAORegistry.getPurchaseOrderDAO().save(brokerId,purchaseOrder);
		 return purchaserOrderId;
	 }
 
	public int save(Integer brokerId,PurchaseOrder pOSPurchaseOrder) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId);
		
		ResultSet rsPruchaseOrder = null;
		ResultSet rsShippingTracking = null;
		Statement insertPurchaseOrderStatement = null;
		Statement insertPurchaseOrderPaymentStatement = null;
		Statement insertShippingTrackingStatement = null;
		Statement insertShippingTrackingAddressStatement = null;
		try{
			connection.setAutoCommit(false);
			
			insertPurchaseOrderStatement = connection.createStatement();
			insertPurchaseOrderPaymentStatement = connection.createStatement();
			insertShippingTrackingStatement = connection.createStatement();
			insertShippingTrackingAddressStatement = connection.createStatement();
			
			// Adding row in Shipping Tracking ..
			Broker broker =BrokerUtils.getBrokerByPOSBrokerId(brokerId);
			String shippingTrackingSql ="INSERT INTO shipping_tracking (shipping_tracking_number,shipped_on_date,arrived_on_date,estimated_arrival_date,notes " + 
            " ,shipping_account_type_id,shipping_account_delivery_method_id  " + 
			" ,shipping_tracking_status_id,shipping_tracking_cost,will_call_pickup_name,shipment_signed_for " + 
			" ,isreturned,marked_for_call,called_awaiting_shipment,isdrop,save_path,ftp_location )  " + 
			" values ('',GETDATE(),GETDATE(),GETDATE(),'',4,	22,1,0.00,'','-',0,0,0,0,'\\\\TN1\\FEDEX_SAVEDLABELS\\771605209080.png','http://fedex.ticketnetwork.com/mercury_982438_771582866479.png')";
			insertShippingTrackingStatement.executeUpdate(shippingTrackingSql, Statement.RETURN_GENERATED_KEYS);
			rsShippingTracking = insertShippingTrackingStatement.getGeneratedKeys();
			Integer shippingTrackingId =0;
			Integer purchaseOrderId =-1;
			if(rsShippingTracking.next()){
				
				shippingTrackingId = rsShippingTracking.getInt(1);
				pOSPurchaseOrder.setShippingTrackingId(shippingTrackingId);
				
				// Adding row in Shipping Tracking Address..
				
				String recipentName = broker.getName();
				String phoneNo = broker.getPhoneNo();
				String addressId ="1";
				if(brokerId==4949){
					addressId = "11502";
				}
				
				String shippingTrackingAddressSql =" INSERT INTO shipping_tracking_address(shipping_tracking_id,address_id,recipient_name,recipient_phone_number) " + 
				" values(" + shippingTrackingId + "," + addressId + ",'" + recipentName + "','"+ phoneNo + "')";
				insertShippingTrackingAddressStatement.execute(shippingTrackingAddressSql);

				// Adding row in Purchase ORder..
				
				String purchaseOrdersql = " INSERT INTO purchase_order (external_po_number,bill_due_date,total_shipping,total_expenses,total_due,total_taxes,po_total,balance_due, " + 
				" notes,payment_terms_id,shipping_tracking_id ,client_broker_id,system_user_id,client_broker_employee_id,buyer_broker_id,create_date,drop_shipped,displayed_notes,reprint,isemailed " +
				" ,currency_id,data_cached,on_hand_notes,tran_office_id ,user_office_id,tm_order_id) VALUES ('n/a',GETDATE(),0,0,0,0,0,0," +
				"''," + pOSPurchaseOrder.getPaymentTermId()  + "," + pOSPurchaseOrder.getShippingTrackingId() + "," +
				pOSPurchaseOrder.getClientBrokerId() + "," + pOSPurchaseOrder.getSystemUserId() + "," + pOSPurchaseOrder.getClientBrokerEmployeeId() 
				+ "," + pOSPurchaseOrder.getBuyerBrokerId() + ",GETDATE(),0,'',0,0,1,0,'',"+broker.getSsAccountOfficeId()+",1,'')";
				
				insertPurchaseOrderStatement.executeUpdate(purchaseOrdersql, Statement.RETURN_GENERATED_KEYS);
				rsPruchaseOrder = insertPurchaseOrderStatement.getGeneratedKeys();
								
				if(rsPruchaseOrder.next()){
					// Adding row in Purchase Order Payment..
					
					purchaseOrderId = rsPruchaseOrder.getInt(1);
					String purchaseOrderPaymentSql ="INSERT INTO purchase_order_payment" +
					" (amt,account_id,credit_card_number,authorization_number,purchase_order_id,payment_type_id " +
					" ,check_number,notes,system_user_id,create_date,update_date,isvoid,payment_credited,tran_office_id,user_office_id) " + 
					" VALUES( " +
					"0,2,'n/a','n/a'," + purchaseOrderId +",2,'n/a','initial payment'," + pOSPurchaseOrder.getSystemUserId()+",GETDATE(),GETDATE(),0 ,0,"+broker.getSsAccountOfficeId()+",1) ";
					insertPurchaseOrderPaymentStatement.execute(purchaseOrderPaymentSql);
				}
			}

			connection.commit();
			return purchaseOrderId ;
		}catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(rsPruchaseOrder !=null){
				rsPruchaseOrder.close();
			}
			if(rsShippingTracking!=null){
				rsShippingTracking.close();
			}
			if(insertPurchaseOrderStatement!=null){
				insertPurchaseOrderStatement.close();
			}
			if(insertPurchaseOrderPaymentStatement!=null){
				insertPurchaseOrderPaymentStatement.close();
			}
			if(insertShippingTrackingStatement!=null){
				insertShippingTrackingStatement.close();
			}
			if(insertShippingTrackingAddressStatement!=null){
				insertShippingTrackingAddressStatement.close();
			}
		}
		//return -1;
	}
	public List<Integer> getAllUnMappedPurchaseOrderIds(Broker broker) throws Exception {
		
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId()); 
		String sql = "select  distinct purchase_order_id as poId from  purchase_order where tran_office_id ="+broker.getSsAccountOfficeId()+" and purchase_order_id not in( " +
				" select purchase_order_id from  ticket t where t.purchase_order_id is not null) " +
				" and create_date<DATEADD(hh, -1, GETDATE()) " +
				" order by purchase_order_id ";
					 
		ResultSet resultSet =null;
		List<Integer> purchaseOrderIds = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				purchaseOrderIds.add(resultSet.getInt("poId"));
			}
			return purchaseOrderIds;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		//return purchaseOrderIds;
	}
}
