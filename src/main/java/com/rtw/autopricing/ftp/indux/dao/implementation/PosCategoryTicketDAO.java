package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.indux.data.PosCategoryTicket;
import com.rtw.autopricing.util.connection.Connections;


public class PosCategoryTicketDAO  implements com.rtw.autopricing.ftp.indux.dao.services.PosCategoryTicketDAO {
	
	
	public Integer save(PosCategoryTicket categoryTicket,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "INSERT INTO category_ticket (category_ticket_group_id,position,actual_price,processed ) " +
				"VALUES ("+categoryTicket.getCategoryTicketGroupId()+","+categoryTicket.getPosition()+",-1,0)";
		ResultSet rs= null;
		Statement insertStatement =null;
		try {
			connection.setAutoCommit(false);
			insertStatement = connection.createStatement();
			insertStatement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
			rs = insertStatement.getGeneratedKeys();
			Integer id =null;
			if(rs.next()){
				id = rs.getInt(1);
			}
			connection.commit();
			
			return id ;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}
		//return -1;
	}
	
	public boolean saveAll(List<PosCategoryTicket> categoryTickets,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId);
		connection.setAutoCommit(false);
		
		ResultSet rs= null;
		Statement insertStatement =null;
		insertStatement = connection.createStatement();
		try {
			for(PosCategoryTicket categoryTicket:categoryTickets){	
				String sql = "INSERT INTO category_ticket (category_ticket_group_id,position,actual_price,processed ) " +
				"VALUES ("+categoryTicket.getCategoryTicketGroupId()+","+categoryTicket.getPosition()+",-1,0)";
				insertStatement.addBatch(sql);
			}
			insertStatement.executeBatch();
			connection.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}	
	}
	
	public Integer deleteByCategoryTicketGroup(Integer categoryTicketGrouId,Integer brokerId) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "DELETE FROM category_ticket where category_ticket_group_id=? and invoice_id is null";
		try {
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			deleteStatement.setInt(1, categoryTicketGrouId);
			deleteStatement.executeUpdate();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			//return 0;
			throw e;
		}finally{

		}
		return 1;
	}
	

	
	public List<Integer> getCategoryTicketIds(Integer categoryTicketGroupId,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId);  
		String sql = "SELECT category_ticket_id FROM category_ticket where category_ticket_group_id=?";
		ResultSet resultSet =null;
		List<Integer> categoryTicketIds = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, categoryTicketGroupId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				categoryTicketIds.add(resultSet.getInt("category_ticket_id"));
			}
			return categoryTicketIds;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{

		}
		//return null;
	}
	
	
}
