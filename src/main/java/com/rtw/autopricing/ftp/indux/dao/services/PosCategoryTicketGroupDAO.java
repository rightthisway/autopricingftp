package com.rtw.autopricing.ftp.indux.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.ManualCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.Broker;
import com.rtw.autopricing.util.CategoryTicket;



public interface PosCategoryTicketGroupDAO {
  public Integer save(AutopricingProduct product,CategoryTicket catTicket,Integer brokerId) throws Exception;
  
  public Integer updateCategoryTicketGroup(CategoryTicket catTicket,Integer brokerId) throws Exception;
  public Integer updateByCategoryTicketGroups(List<CategoryTicket> catTixList,Integer brokerId) throws Exception;
  public Integer deleteByCategoryTicketGroup(Integer categoryTicketGrouId,Integer brokerId) throws Exception; 
//  public Integer deleteCategoryTicketGroups(Integer categoryTicketGrouId,Integer brokerId) throws Exception;
  public List<Integer> getSoldCategoryTicketGroupIds(String fromDateStr,String toDateStr,String internalNotesStr,Integer brokerId) throws Exception;
  //public Integer getAllActiveTicketsCountinPOS() throws Exception ;
  public Integer getAllActiveTicketsCountinPOS(Broker broker) throws Exception;
  public Integer getAllActiveInduxTicketsCountinPOS(Broker broker) throws Exception;
  public void deleteTicketGroupsByTNCategoryTicketGroupId(Integer brokerId,Integer tnCategoryTicketGroupId)throws Exception ;
  public List<ManualCategoryTicket> getAllBroadCastedManualCategoryTickets(Integer brokerId) throws Exception;
  public List<Object[]> getBrokerCategoryTicketCountProductWise(Integer brokerId, String internalNotesCombined)throws Exception;
  public List<Object[]> getBrokerSSAccountTicketCountProductWise(Broker broker, String internalNotesCombined)throws Exception;
  public boolean unBroadcastCategoryTicketsByExchangeEventIds(String eventIds, Integer brokerId) throws Exception;
  public Integer getLastestListingsCountByBroker(Broker broker) throws Exception;
  			  
}
