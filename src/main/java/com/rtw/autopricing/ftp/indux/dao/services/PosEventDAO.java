package com.rtw.autopricing.ftp.indux.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.indux.data.PosEvent;

public interface PosEventDAO {
	
	
//	public PosEvent getEventbyId(Integer brokerId,Integer eventId) throws Exception;
	public List<PosEvent> getAllActivePOSEventsByPOSBrokerId(Integer brokerId) throws Exception;
	public List<PosEvent> getAllActivePOSLocalEventsByPOSBrokerId(Integer brokerId) throws Exception;
	public List<PosEvent> getAllActiveExchangeEventWithoutLocalEvent(Integer brokerId) throws Exception;
	public boolean updateLocalEvent(Integer brokerId,Integer ExchangeEventId,Integer systemUserId) throws Exception;
	public Boolean checkPOSConnection(Integer brokerId) throws Exception;
}
