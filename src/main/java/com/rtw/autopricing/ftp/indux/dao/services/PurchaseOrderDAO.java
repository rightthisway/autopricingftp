package com.rtw.autopricing.ftp.indux.dao.services;

import java.sql.SQLException;
import java.util.List;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.indux.data.Broker;
import com.rtw.autopricing.ftp.indux.data.PurchaseOrder;

public interface PurchaseOrderDAO {
	
	public static final int JOE_KNOWS_TICKET = 7784;
	public static final int RESERVE_ONE_TICKET = 7790;
	
	public boolean deleteById(Integer brokerId,Integer purchaseOrderId) throws Exception;
	public Integer creatrePurchaseOrder(Integer brokerId) throws Exception;
	public int save(Integer brokerId,PurchaseOrder pOSPurchaseOrder) throws Exception;	
	public List<Integer> getAllUnMappedPurchaseOrderIds(Broker broker) throws Exception;
}
