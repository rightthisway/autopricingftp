package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.ManualCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.Broker;
import com.rtw.autopricing.ftp.indux.data.PosCategoryTicket;
import com.rtw.autopricing.util.CategoryTicket;
import com.rtw.autopricing.util.connection.Connections;


public class PosCategoryTicketGroupDAO implements com.rtw.autopricing.ftp.indux.dao.services.PosCategoryTicketGroupDAO {
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	
  public Integer save(AutopricingProduct product,CategoryTicket catTicket,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId);  
		String expectedArrivalDateStr ="";
		if(catTicket.getExpectedArrivalDate()!=null){
			expectedArrivalDateStr =  df.format(catTicket.getExpectedArrivalDate());
		}
		String notes = "";
		if(catTicket.getQuantity() > 4) {
			//notes = "If more than four (4) tickets are purchased from this group, your tickets may be split.  All tickets are guaranteed to be within the same section.";
			notes = product.getNotes();
		}
		
		int broadcast=1;
		Integer ticketgroupCodeId=null;
		if(product.getName().equals("SGLastFiveRow")) {
			broadcast=0;
			if(brokerId==156) {//RTW-2
			ticketgroupCodeId=41;//SGK
			}
		}
		
		String sql = "INSERT INTO category_ticket_group (event_id,venue_category_id,ticket_count,wholesale_price,retail_price,expected_arrival_date" +
				",face_price,cost,internal_notes,tax_exempt,update_datetime,broadcast ,create_date,office_id," +
				" unbroadcast_days_before_event,shipping_method_special_id," +
				" show_near_term_option_id,tg_note_grandfathered,auto_process_web_requests,notes,venue_configuration_zone_id,split_rule_id,ticket_group_code_id) " +
				" VALUES ("+catTicket.getPosEventId()+","+catTicket.getPosVenueCategoryId()+","+catTicket.getQuantity()+"," +
				""+catTicket.getTnPrice()+","+catTicket.getTnPrice()+",'"+expectedArrivalDateStr+"'," +
				"0.00,0.00,'"+product.getInternalNotes()+"',0,GETDATE(),"+broadcast+",GETDATE(),1," +
				"0,"+catTicket.getShippingMethodSpecialId()+"," +
				""+catTicket.getNearTermOptionId()+",0,1,'"+notes+"',"+catTicket.getVenueConfigurationZoneId()+",1,"+ticketgroupCodeId+")";
		//"+new Date(ticket.getExpectedArrivalDate().getTime())+"
		ResultSet rs= null;
		Statement insertStatement =null;
		try {
			connection.setAutoCommit(false);
			insertStatement = connection.createStatement();
			insertStatement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
			rs = insertStatement.getGeneratedKeys();
			Integer id =null;
			if(rs.next()){
				id = rs.getInt(1);
			}
			connection.commit();
			List<PosCategoryTicket> catTixs = new ArrayList<PosCategoryTicket>();
			for(int i =1;i<=catTicket.getQuantity();i++){
				PosCategoryTicket tix = new PosCategoryTicket();
				tix.setCategoryTicketGroupId(id.intValue());
				tix.setPosition(i);
				catTixs.add(tix);
			}
			if(!InduxDAORegistry.getPosCategoryTicketDAO().saveAll(catTixs, brokerId)){
				throw new Exception("Error while adding category tickets..");
			}
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}
		//return -1;
	}
  
  public Integer updateCategoryTicketGroup(CategoryTicket catTicket,Integer brokerId) throws Exception{
		
	  Connection connection = Connections.getInduxConnection(brokerId);  
		String sql = "UPDATE category_ticket_group set wholesale_price=?,retail_price=?," +
				" shipping_method_special_id = ?,show_near_term_option_id = ?,expected_arrival_date=?" +
				" where category_ticket_group_id=? and category_ticket_group_id not in(" +
				" select category_ticket_group_id from category_ticket where category_ticket_group_id=? and invoice_id is not null)";
		try {
			PreparedStatement updateStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			updateStatement.setDouble(1, catTicket.getTnPrice());
			updateStatement.setDouble(2, catTicket.getTnPrice());
			updateStatement.setInt(3, catTicket.getShippingMethodSpecialId());
			updateStatement.setInt(4, catTicket.getNearTermOptionId());
			updateStatement.setDate(5, new Date(catTicket.getExpectedArrivalDate().getTime()));
			updateStatement.setLong(6, catTicket.getTnCategoryTicketGroupId());
			updateStatement.setLong(7, catTicket.getTnCategoryTicketGroupId());
			updateStatement.executeUpdate();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			//return 0;
			throw e;
		}finally{
		}
		return 1;
	}
  
  public Integer updateByCategoryTicketGroups(List<CategoryTicket> catTixList,Integer brokerId) throws Exception{
		
	  Connection connection = Connections.getInduxConnection(brokerId);   
		String sql = "UPDATE category_ticket_group set wholesale_price=?,retail_price=?," +
				" shipping_method_special_id = ?,show_near_term_option_id = ?,expected_arrival_date=?" +
				" where category_ticket_group_id=? and category_ticket_group_id not in(" +
				" select category_ticket_group_id from category_ticket where category_ticket_group_id=? and invoice_id is not null)";
		
		int[] updateTicketGroupResult ;
		try {
			PreparedStatement updateStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			int u=0;
			for(CategoryTicket catTicket: catTixList){
				u++;
				updateStatement.setDouble(1, catTicket.getTnPrice());
				updateStatement.setDouble(2, catTicket.getTnPrice());
				updateStatement.setInt(3, catTicket.getShippingMethodSpecialId());
				updateStatement.setInt(4, catTicket.getNearTermOptionId());
				updateStatement.setDate(5, new Date(catTicket.getExpectedArrivalDate().getTime()));
				updateStatement.setLong(6, catTicket.getTnCategoryTicketGroupId());
				updateStatement.setLong(7, catTicket.getTnCategoryTicketGroupId());
				
				updateStatement.addBatch();
			}
			int result=0;
			if(u>0){
				updateTicketGroupResult = updateStatement.executeBatch();
				result+= updateTicketGroupResult.length;
			}
			
			connection.commit();
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
		}
		//return 0;
	}
  
  public Integer deleteByCategoryTicketGroup(Integer categoryTicketGrouId,Integer brokerId) throws Exception{
	  Connection connection = Connections.getInduxConnection(brokerId);  
		String sql = "DELETE FROM category_ticket_group where category_ticket_group_id=? and category_ticket_group_id not in(" +
				" select category_ticket_group_id from category_ticket where category_ticket_group_id=? and invoice_id is not null) ";
		try {
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			deleteStatement.setInt(1, categoryTicketGrouId);
			deleteStatement.setInt(2, categoryTicketGrouId);
			deleteStatement.executeUpdate();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			//return 0;
			throw e;
		}finally{

		}
		return 1;
	}
  
	 /* public Integer deleteCategoryTicketGroups(Integer categoryTicketGrouId,Integer brokerId) throws Exception{
		  Connection connection = Connections.getInduxConnection(brokerId);  
			
			String catTicketFillHistorySql = "DELETE FROM category_ticket_fill_history where category_ticket_id in ( SELECT category_ticket_id " +
					" FROM category_ticket where category_ticket_group_id=? and invoice_id is null)";
			
			String catTicketSql = "DELETE FROM category_ticket where category_ticket_group_id=? and invoice_id is null";
			
			String cateTicketGroupSql = "DELETE FROM category_ticket_group where category_ticket_group_id=? and category_ticket_group_id not in(" +
					" select category_ticket_group_id from category_ticket where category_ticket_group_id=? and invoice_id is not null) ";
			try {
				connection.setAutoCommit(false);
				PreparedStatement catTicketFillHistoryDeleteStatement = connection.prepareStatement(catTicketFillHistorySql);
				PreparedStatement catTicketDeleteStatement = connection.prepareStatement(catTicketSql);
				PreparedStatement cateTicketGroupDeleteStatement = connection.prepareStatement(cateTicketGroupSql);
				
				catTicketFillHistoryDeleteStatement.setInt(1, categoryTicketGrouId);
				catTicketFillHistoryDeleteStatement.executeUpdate();
				
				catTicketDeleteStatement.setInt(1, categoryTicketGrouId);
				catTicketDeleteStatement.executeUpdate();
				
				cateTicketGroupDeleteStatement.setInt(1, categoryTicketGrouId);
				cateTicketGroupDeleteStatement.setInt(2, categoryTicketGrouId);
				cateTicketGroupDeleteStatement.executeUpdate();
			
				connection.commit();
			} catch (Exception e) {
				e.printStackTrace();
				return 0;
			}finally{
	
			}
			return 1;
		}*/
	
	public List<Integer> getSoldCategoryTicketGroupIds(String fromDateStr,String toDateStr,String internalNotesStr,Integer brokerId) throws Exception {
		
		Connection connection = Connections.getInduxConnection(brokerId);   
		String sql = "select distinct ct.category_ticket_group_id from category_ticket_group ctg " +
				" inner join category_ticket ct on ct.category_ticket_group_id=ctg.category_ticket_group_id " +
				" inner join invoice i on i.invoice_id=ct.invoice_id " +
				" where ctg.internal_notes in("+internalNotesStr+") and i.create_date>='"+fromDateStr+"' and i.create_date<='"+toDateStr+"' ";
				
		ResultSet resultSet =null;
		List<Integer> catTixIdList = new ArrayList<Integer>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				catTixIdList.add(resultSet.getInt("category_ticket_group_id"));
			}
			return catTixIdList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		//return catTixIdList;
	}
	
	/*public Integer getAllActiveTicketsCountinPOS() throws Exception {
		
		Integer totTixCount = 0;
		try {
			
			Collection<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveBrokers();
			 for (Broker broker : brokers) {
				 totTixCount = totTixCount + getAllActiveTicketsCountinPOS(broker.getId());
			 }
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return totTixCount;
	}*/
	
	public Integer getAllActiveTicketsCountinPOS(Broker broker) throws Exception{
		
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId());   
		String sql = "select count(category_ticket_group_id) as tixCount from category_ticket_group " +
				" where retail_price > 0 and broadcast=1 and category_ticket_group_id not in(select category_ticket_group_id from category_ticket where invoice_id is not null) " +
				" union " +
				" select count(ticket_group_id) as tixCount from ticket_group " +
				" where retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" and internal_notes in('LarryLast NoStub')" +
				" and ticket_group_id not in(select ticket_group_id from ticket where invoice_id is not null)" +
				" and ticket_group_id in(select distinct ticket_group_id from ticket_group_distribution_method)";
				
				
		ResultSet resultSet =null;
		Integer tixCount = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				tixCount = tixCount + resultSet.getInt("tixCount");
			}
			return tixCount;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		
	}
	
public Integer getAllActiveInduxTicketsCountinPOS(Broker broker) throws Exception{
		
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId());   
		String sql = "select count(category_ticket_group_id) as tixCount from category_ticket_group " +
				" where retail_price > 0 and broadcast=1 " +
				"and category_ticket_group_id not in(select category_ticket_group_id from " +
				"category_ticket where invoice_id is not null) " +
				" union " +
				" select count(ticket_group_id) as tixCount from ticket_group " +
				" where retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" " +
				" and ticket_group_id not in(select ticket_group_id from ticket where invoice_id is not null)" +
				" and ticket_group_id in(select distinct ticket_group_id from ticket_group_distribution_method)";
				
				
		ResultSet resultSet =null;
		Integer tixCount = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				tixCount = tixCount + resultSet.getInt("tixCount");
			}
			return tixCount;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		
	}

	public List<Object[]> getBrokerCategoryTicketCountProductWise(Integer brokerId, String internalNotesCombined)throws Exception{
		List<Object[]> list = new ArrayList<Object[]>();
		String sql = "select internal_notes,count(category_ticket_group_id) from category_ticket_group " +
					 "where retail_price > 0 and broadcast=1 and internal_notes in ("+ internalNotesCombined +") " +
					 "and category_ticket_group_id not in(select category_ticket_group_id from category_ticket where invoice_id is not null) " +
					 "group by internal_notes " +
					 "union "+
					 "select 'MANUAL',sum(r.count) from (select internal_notes,count(category_ticket_group_id) as 'count' from category_ticket_group  " +
					 "where retail_price > 0 and broadcast=1 and " +
					 "internal_notes not in ("+ internalNotesCombined +") " +
					 "and category_ticket_group_id not in(select category_ticket_group_id from category_ticket where invoice_id is not null) group by internal_notes) as r";
		ResultSet resultSet =null;
		try{
			Connection connection = Connections.getInduxConnection(brokerId);
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				Object[] obj = new Object[2];
				obj[0] = resultSet.getObject(1);
				obj[1] = resultSet.getObject(2);
				list.add(obj);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return list;
	}
	
	public List<Object[]> getBrokerSSAccountTicketCountProductWise(Broker broker, String internalNotesCombined)throws Exception{
		List<Object[]> list = new ArrayList<Object[]>();
		String sql = "select internal_notes,count(ticket_group_id) from ticket_group " +
					 "where retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" and (internal_notes in ("+ internalNotesCombined +") ) " +
					 "and retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" and ticket_group_id not in(select ticket_group_id from ticket where invoice_id is not null) " +
					 "and ticket_group_id in(select distinct ticket_group_id from ticket_group_distribution_method) group by internal_notes " +
					 "union "+
					 "select 'MANUAL',sum(r.count) from (select internal_notes,count(ticket_group_id) as 'count' from ticket_group " +
					 "where retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" and internal_notes not in ("+ internalNotesCombined +") " +
					 "and retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" and ticket_group_id not in(select ticket_group_id from ticket where invoice_id is not null) " +
					 "and ticket_group_id in(select distinct ticket_group_id from ticket_group_distribution_method) group by internal_notes) as r";
		ResultSet resultSet =null;
		try{
			Connection connection = Connections.getInduxConnection(broker.getPosBrokerId());
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				Object[] obj = new Object[2];
				obj[0] = resultSet.getObject(1);
				obj[1] = resultSet.getObject(2);
				list.add(obj);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return list;
	}

	
	public void deleteTicketGroupsByTNCategoryTicketGroupId(Integer brokerId,Integer tnCategoryTicketGroupId)  throws Exception {
		Connection connection = Connections.getInduxConnection(brokerId);  
		
		String catTicketFillHistorySql = "DELETE FROM category_ticket_fill_history where category_ticket_id in ( SELECT category_ticket_id " +
				" FROM category_ticket where category_ticket_group_id=? and invoice_id is null)";
		
		String catTicketSql = "DELETE FROM category_ticket where category_ticket_group_id=? and invoice_id is null";
		
		String cateTicketGroupSql = "DELETE FROM category_ticket_group where category_ticket_group_id=? and category_ticket_group_id not in(" +
				" select category_ticket_group_id from category_ticket where category_ticket_group_id=? and invoice_id is not null) ";
		try {
			connection.setAutoCommit(false);
			PreparedStatement catTicketFillHistoryDeleteStatement = connection.prepareStatement(catTicketFillHistorySql);
			PreparedStatement catTicketDeleteStatement = connection.prepareStatement(catTicketSql);
			PreparedStatement cateTicketGroupDeleteStatement = connection.prepareStatement(cateTicketGroupSql);
			
			catTicketFillHistoryDeleteStatement.setLong(1, tnCategoryTicketGroupId);
			catTicketFillHistoryDeleteStatement.executeUpdate();
			
			catTicketDeleteStatement.setLong(1, tnCategoryTicketGroupId);
			catTicketDeleteStatement.executeUpdate();
			
			cateTicketGroupDeleteStatement.setLong(1, tnCategoryTicketGroupId);
			cateTicketGroupDeleteStatement.setLong(2, tnCategoryTicketGroupId);
			cateTicketGroupDeleteStatement.executeUpdate();
		
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
//			return 0;
		}finally{

		}
//		return 1;
		
	}
	
	
	public List<ManualCategoryTicket> getAllBroadCastedManualCategoryTickets(Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId);   
		
		String sql = "select ctg.category_ticket_group_id,ctg.event_id,e.event_name as EventName,e.event_datetime,v.name as VenueName," +
				"ctg.ticket_count,vc.section_low as section,vc.row_low as row,ctg.retail_price,ctg.wholesale_price," +
				"ctg.shipping_method_special_id from category_ticket_group ctg  " +
				"inner join venue_category vc on vc.venue_category_id=ctg.venue_category_id " +
				"inner join event e on e.event_id=ctg.event_id " +
				"inner join venue v on v.venue_id=e.venue_id where ctg.retail_price > 0 and ctg.broadcast=1 " +
				"and ctg.internal_notes not in ('MINICATS','VIPMINICATS','ZMINICATS','ZLASTROW MINICATS','ZVIPMINICATS','SCZP','LASTROW MINICATS','AUTOCAT','ZP') " +
				"and category_ticket_group_id not in (select distinct category_ticket_group_id from category_ticket where invoice_id is not null) " +
				"and vc.section_low not like '%zone%' and vc.row_low not like '%zone%'";
				
		ResultSet resultSet =null;
		ManualCategoryTicket manualCategoryTicket = null;
		List<ManualCategoryTicket> categoryTickets = new ArrayList<ManualCategoryTicket>();
		DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				manualCategoryTicket =  new ManualCategoryTicket();
				manualCategoryTicket.setTnLocalEventId(resultSet.getInt("event_id"));
				manualCategoryTicket.setTnCategoryGroupId(resultSet.getInt("category_ticket_group_id"));
				manualCategoryTicket.setEventName(resultSet.getString("EventName"));
				manualCategoryTicket.setEventDate(dbDateTimeFormat.parse(resultSet.getString("event_datetime")));
				manualCategoryTicket.setVenueName(resultSet.getString("VenueName"));
				manualCategoryTicket.setQuantity(resultSet.getInt("ticket_count"));
				manualCategoryTicket.setSection(resultSet.getString("section"));
				manualCategoryTicket.setRow(resultSet.getString("row"));
				manualCategoryTicket.setSeatFrom("");
				manualCategoryTicket.setSeatThru("");
				manualCategoryTicket.setCost(resultSet.getDouble("retail_price"));
				manualCategoryTicket.setEdelivery(resultSet.getInt("shipping_method_special_id"));
				categoryTickets.add(manualCategoryTicket);
			}
			return categoryTickets;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		//return null;
	}
	
	public boolean unBroadcastCategoryTicketsByExchangeEventIds(String admitoneIds, Integer brokerId) throws Exception{
		
		  Connection connection = Connections.getInduxConnection(brokerId);  
			String sql = "UPDATE ctg set broadcast = 0" +
					" from category_ticket_group ctg " +
					" inner join event e on e.event_id=ctg.event_id" +
					" where exchange_event_id in (" + admitoneIds + ")";
			try {
				PreparedStatement updateStatement = connection.prepareStatement(sql);
				connection.setAutoCommit(false);
				updateStatement.executeUpdate();
				connection.commit();
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				//return 0;
				throw e;
			}finally{
			}
		}
	
public Integer getLastestListingsCountByBroker(Broker broker) throws Exception {
		
		Connection connection = Connections.getInduxConnection(broker.getPosBrokerId());   
		
		String sql = " select sum(g.tic_count) as total_count from (" +
				" select count(*) as tic_count from category_ticket_group with(nolock)" +
				" where category_ticket_group_id not in(select distinct category_ticket_group_id " +
				" from category_ticket with(nolock) where invoice_id is not null)" +
				" and  category_ticket_group_id in (select distinct category_ticket_group_id from category_ticket with(nolock) )" +
				" and retail_price> 0 and broadcast=1" +
				" union" +
				" select count(*) as tic_count from ticket_group with(nolock)" +
				" where ticket_group_id not in(select distinct ticket_group_id from ticket with(nolock) where invoice_id is not null)" +
				" and ticket_group_id in(select ticket_group_id from ticket_group_distribution_method with(nolock))" +
				" and office_id=" +broker.getSsAccountOfficeId()+ " and retail_price> 0 ) as g";
				
		ResultSet resultSet =null;
		Integer totalCount = 0;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				totalCount = resultSet.getInt("total_count");
			}
			return totalCount;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		//return null;
	}
}
