package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.indux.data.PosVenueConfigurationZone;
import com.rtw.autopricing.util.connection.Connections;

public class PosVenueConfigurationZoneDAO implements com.rtw.autopricing.ftp.indux.dao.services.PosVenueConfigurationZoneDAO {

	
 public List<PosVenueConfigurationZone> getAllVenueConfigZone(Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "select * from venue_configuration_zone ";
		ResultSet resultSet =null;
		List<PosVenueConfigurationZone> venueConfigZoneList = new ArrayList<PosVenueConfigurationZone>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){ 
				PosVenueConfigurationZone venueConfigZone = new PosVenueConfigurationZone();
				venueConfigZone.setVenueConfigurationId(resultSet.getInt("venue_configuration_id"));
				venueConfigZone.setVenueConfigurationZoneId(resultSet.getInt("venue_configuration_zone_id"));
				venueConfigZone.setZone(resultSet.getString("zone"));
				
				venueConfigZoneList.add(venueConfigZone);
			}
			return venueConfigZoneList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		//return venueConfigZoneList;
	}
	
	
	
}
