package com.rtw.autopricing.ftp.indux.data;

import java.io.Serializable;
import java.util.Date;


public class PurchaseOrder implements Serializable {
	
	private Integer purchaseOrderId;
	private String externalPONumber;
	private Date billDueDate;
	private Double totalShipping;
	private Double totalExpenses;
	private Double totalDue;
	private Double totalTaxes;
	private Double poTotal;
	private Double balanceDue;
	private String notes;
	private Integer paymentTermId;
	private Integer shippingTrackingId;
	private Integer clientBrokerId;
	private Integer systemUserId;
	private Integer clientBrokerEmployeeId;
	private Integer buyerBrokerId;
	private Date createDate;
	private Integer dropShipped;
	private String displayedNotes;
	private Boolean isEmailed;
	private Integer currencyId;
	private Boolean dataCached;
	private String onHandNotes;
	private Integer transOfficeId;	
	private Integer userOfficeId;
	private String tmOrderId;
	private Integer reprint;
	
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Double getTotalShipping() {
		return totalShipping;
	}
	public void setTotalShipping(Double totalShipping) {
		this.totalShipping = totalShipping;
	}
	public Double getTotalExpenses() {
		return totalExpenses;
	}
	public void setTotalExpenses(Double totalExpenses) {
		this.totalExpenses = totalExpenses;
	}
	public Double getTotalTaxes() {
		return totalTaxes;
	}
	public void setTotalTaxes(Double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}
	public Double getPoTotal() {
		return poTotal;
	}
	public void setPoTotal(Double poTotal) {
		this.poTotal = poTotal;
	}
	public Double getBalanceDue() {
		return balanceDue;
	}
	public void setBalanceDue(Double balanceDue) {
		this.balanceDue = balanceDue;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDisplayedNotes() {
		return displayedNotes;
	}
	public void setDisplayedNotes(String displayedNotes) {
		this.displayedNotes = displayedNotes;
	}
	public String getOnHandNotes() {
		return onHandNotes;
	}
	public void setOnHandNotes(String onHandNotes) {
		this.onHandNotes = onHandNotes;
	}
	public Double getTotalDue() {
		return totalDue;
	}
	public void setTotalDue(Double totalDue) {
		this.totalDue = totalDue;
	}
	public Date getBillDueDate() {
		return billDueDate;
	}
	public void setBillDueDate(Date billDueDate) {
		this.billDueDate = billDueDate;
	}
	public Integer getPaymentTermId() {
		return paymentTermId;
	}
	public void setPaymentTermId(Integer paymentTermId) {
		this.paymentTermId = paymentTermId;
	}
	public Integer getShippingTrackingId() {
		return shippingTrackingId;
	}
	public void setShippingTrackingId(Integer shippingTrackingId) {
		this.shippingTrackingId = shippingTrackingId;
	}
	public Integer getClientBrokerId() {
		return clientBrokerId;
	}
	public void setClientBrokerId(Integer clientBrokerId) {
		this.clientBrokerId = clientBrokerId;
	}
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	public Integer getClientBrokerEmployeeId() {
		return clientBrokerEmployeeId;
	}
	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId) {
		this.clientBrokerEmployeeId = clientBrokerEmployeeId;
	}
	public Integer getBuyerBrokerId() {
		return buyerBrokerId;
	}
	public void setBuyerBrokerId(Integer buyerBrokerId) {
		this.buyerBrokerId = buyerBrokerId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Integer getDropShipped() {
		return dropShipped;
	}
	public void setDropShipped(Integer dropShipped) {
		this.dropShipped = dropShipped;
	}
	public Boolean getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}
	public Integer getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}
	public Boolean getDataCached() {
		return dataCached;
	}
	public void setDataCached(Boolean dataCached) {
		this.dataCached = dataCached;
	}
	public Integer getTransOfficeId() {
		return transOfficeId;
	}
	public void setTransOfficeId(Integer transOfficeId) {
		this.transOfficeId = transOfficeId;
	}
	public Integer getUserOfficeId() {
		return userOfficeId;
	}
	public void setUserOfficeId(Integer userOfficeId) {
		this.userOfficeId = userOfficeId;
	}
	public String getTmOrderId() {
		return tmOrderId;
	}
	public void setTmOrderId(String tmOrderId) {
		this.tmOrderId = tmOrderId;
	}
	public String getExternalPONumber() {
		return externalPONumber;
	}
	public void setExternalPONumber(String externalPONumber) {
		this.externalPONumber = externalPONumber;
	}
	public Integer getReprint() {
		return reprint;
	}
	public void setReprint(Integer reprint) {
		this.reprint = reprint;
	}
	
	
}
