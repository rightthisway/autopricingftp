package com.rtw.autopricing.ftp.indux.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ticket_group")
public class POSTicketGroup implements Serializable {

	private Integer ticketGroupId;
	private Integer ticketGroupExposureId;
	private Date arrivedOnDate;
	private Integer originalTicketCount;
	private Integer remainingTicketCount;
	private Double retailPrice;
	private Double facePrice;
	private Double cost;
	private Double wholesalePrice;
	private String row;
	private String section;
	private String notes;
	private Date createdDate;
	private Integer systemUserId;
	private Integer eventId;
	private Integer statusId;
	private Integer ticketGroupSeatingTypeId;
	private Integer ticketGroupTypeId;
	private Integer clientBrokerId;
	private Integer clientBrokerEmployeeId;
	private String internalNotes;
	private Date actualPurchaseDate;
	private Integer autoProcessWebRequests;
	private Boolean taxExempt;
	private Boolean ebayAuction;
	private String ebayAuctionNumber;
	private Date updateDateTime;
	private String notOnHandMessage;
	private Integer showTndNonTnd;
	private Integer officeId;
	private Boolean isDropSale;
	private Integer exchangeEventId;
	private Integer exchangeTicketGroupId;
	private Integer unbroadcastDaysBeforeEvent;
	private Integer ticketGroupStockTypeId;
	private Integer shippingMethodSpecialId;
	private Integer showNearTermOptionId;

	private Integer tgNoteGrandFathered;
	private Date priceUpdateDatetime;
	private Boolean eticketInstantDownload;
	private Boolean eticketStoredLocally;
	private Boolean overrideInstantDownloadPause;

	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}

	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	@Column(name="ticket_group_exposure_type_id")
	public Integer getTicketGroupExposureId() {
		return ticketGroupExposureId;
	}

	public void setTicketGroupExposureId(Integer ticketGroupExposureId) {
		this.ticketGroupExposureId = ticketGroupExposureId;
	}

	@Column(name="arrived_on_date")
	public Date getArrivedOnDate() {
		return arrivedOnDate;
	}

	public void setArrivedOnDate(Date arrivedOnDate) {
		this.arrivedOnDate = arrivedOnDate;
	}

	@Column(name="original_ticket_count")
	public Integer getOriginalTicketCount() {
		return originalTicketCount;
	}

	public void setOriginalTicketCount(Integer originalTicketCount) {
		this.originalTicketCount = originalTicketCount;
	}

	@Column(name="remaining_ticket_count")
	public Integer getRemainingTicketCount() {
		return remainingTicketCount;
	}

	public void setRemainingTicketCount(Integer remainingTicketCount) {
		this.remainingTicketCount = remainingTicketCount;
	}

	@Column(name="retail_price")
	public Double getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}

	@Column(name="face_price")
	public Double getFacePrice() {
		return facePrice;
	}

	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}

	@Column(name="cost")
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	@Column(name="wholesale_price")
	public Double getWholesalePrice() {
		return wholesalePrice;
	}

	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}

	@Column(name="row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Column(name="section")
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(name="notes")
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name="create_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="system_user_id")
	public Integer getSystemUserId() {
		return systemUserId;
	}

	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name="status_id")
	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	@Column(name="ticket_group_seating_type_id")
	public Integer getTicketGroupSeatingTypeId() {
		return ticketGroupSeatingTypeId;
	}

	public void setTicketGroupSeatingTypeId(Integer ticketGroupSeatingTypeId) {
		this.ticketGroupSeatingTypeId = ticketGroupSeatingTypeId;
	}

	@Column(name="ticket_group_type_id")
	public Integer getTicketGroupTypeId() {
		return ticketGroupTypeId;
	}

	public void setTicketGroupTypeId(Integer ticketGroupTypeId) {
		this.ticketGroupTypeId = ticketGroupTypeId;
	}

	@Column(name="client_broker_id")
	public Integer getClientBrokerId() {
		return clientBrokerId;
	}

	public void setClientBrokerId(Integer clientBrokerId) {
		this.clientBrokerId = clientBrokerId;
	}

	@Column(name="client_broker_employee_id")
	public Integer getClientBrokerEmployeeId() {
		return clientBrokerEmployeeId;
	}

	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId) {
		this.clientBrokerEmployeeId = clientBrokerEmployeeId;
	}

	@Column(name="internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	@Column(name="actual_purchase_date")
	public Date getActualPurchaseDate() {
		return actualPurchaseDate;
	}

	public void setActualPurchaseDate(Date actualPurchaseDate) {
		this.actualPurchaseDate = actualPurchaseDate;
	}

	@Column(name="auto_process_web_request")
	public Integer getAutoProcessWebRequests() {
		return autoProcessWebRequests;
	}

	public void setAutoProcessWebRequests(Integer autoProcessWebRequests) {
		this.autoProcessWebRequests = autoProcessWebRequests;
	}

	@Column(name="tax_exempt")
	public Boolean getTaxExempt() {
		return taxExempt;
	}

	public void setTaxExempt(Boolean taxExempt) {
		this.taxExempt = taxExempt;
	}

	@Column(name="ebay_autction")
	public Boolean getEbayAuction() {
		return ebayAuction;
	}

	public void setEbayAuction(Boolean ebayAuction) {
		this.ebayAuction = ebayAuction;
	}

	@Column(name="ebay_autction_number")
	public String getEbayAuctionNumber() {
		return ebayAuctionNumber;
	}

	public void setEbayAuctionNumber(String ebayAuctionNumber) {
		this.ebayAuctionNumber = ebayAuctionNumber;
	}

	@Column(name="update_datetime")
	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	@Column(name="not_on_hand_message")
	public String getNotOnHandMessage() {
		return notOnHandMessage;
	}

	public void setNotOnHandMessage(String notOnHandMessage) {
		this.notOnHandMessage = notOnHandMessage;
	}

	@Column(name="show_tnd_nontnd")
	public Integer getShowTndNonTnd() {
		return showTndNonTnd;
	}

	public void setShowTndNonTnd(Integer showTndNonTnd) {
		this.showTndNonTnd = showTndNonTnd;
	}

	@Column(name="office_id")
	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	@Column(name="isDropSale")
	public boolean isDropSale() {
		return isDropSale;
	}

	public void setDropSale(boolean isDropSale) {
		this.isDropSale = isDropSale;
	}

	@Column(name="exchange_event_id")
	public Integer getExchangeEventId() {
		return exchangeEventId;
	}

	public void setExchangeEventId(Integer exchangeEventId) {
		this.exchangeEventId = exchangeEventId;
	}

	@Column(name="exchage_ticket_group_id")
	public Integer getExchangeTicketGroupId() {
		return exchangeTicketGroupId;
	}

	public void setExchangeTicketGroupId(Integer exchangeTicketGroupId) {
		this.exchangeTicketGroupId = exchangeTicketGroupId;
	}

	@Column(name="unbroadcast_days_before_event")
	public Integer getUnbroadcastDaysBeforeEvent() {
		return unbroadcastDaysBeforeEvent;
	}

	public void setUnbroadcastDaysBeforeEvent(Integer unbroadcastDaysBeforeEvent) {
		this.unbroadcastDaysBeforeEvent = unbroadcastDaysBeforeEvent;
	}

	@Column(name="ticket_group_stock_type_id")
	public Integer getTicketGroupStockTypeId() {
		return ticketGroupStockTypeId;
	}

	public void setTicketGroupStockTypeId(Integer ticketGroupStockTypeId) {
		this.ticketGroupStockTypeId = ticketGroupStockTypeId;
	}

	@Column(name="shipping_method_special_id")
	public Integer getShippingMethodSpecialId() {
		return shippingMethodSpecialId;
	}

	public void setShippingMethodSpecialId(Integer shippingMethodSpecialId) {
		this.shippingMethodSpecialId = shippingMethodSpecialId;
	}

	@Column(name="show_near_term_option_id")
	public Integer getShowNearTermOptionId() {
		return showNearTermOptionId;
	}

	public void setShowNearTermOptionId(Integer showNearTermOptionId) {
		this.showNearTermOptionId = showNearTermOptionId;
	}

	@Column(name="tg_note_grandfathered")
	public Integer getTgNoteGrandFathered() {
		return tgNoteGrandFathered;
	}

	public void setTgNoteGrandFathered(Integer tgNoteGrandFathered) {
		this.tgNoteGrandFathered = tgNoteGrandFathered;
	}

	@Column(name="price_update_datetime")
	public Date getPriceUpdateDatetime() {
		return priceUpdateDatetime;
	}

	public void setPriceUpdateDatetime(Date priceUpdateDatetime) {
		this.priceUpdateDatetime = priceUpdateDatetime;
	}

	@Column(name="eticket_instant_download")
	public Boolean getEticketInstantDownload() {
		return eticketInstantDownload;
	}

	
	public void setEticketInstantDownload(Boolean eticketInstantDownload) {
		this.eticketInstantDownload = eticketInstantDownload;
	}

	@Column(name="eticket_stored_locally")
	public Boolean getEticketStoredLocally() {
		return eticketStoredLocally;
	}

	public void setEticketStoredLocally(Boolean eticketStoredLocally) {
		this.eticketStoredLocally = eticketStoredLocally;
	}

	@Column(name="override_instant_download_pause")
	public Boolean getOverrideInstantDownloadPause() {
		return overrideInstantDownloadPause;
	}

	public void setOverrideInstantDownloadPause(
			Boolean overrideInstantDownloadPause) {
		this.overrideInstantDownloadPause = overrideInstantDownloadPause;
	}

}
