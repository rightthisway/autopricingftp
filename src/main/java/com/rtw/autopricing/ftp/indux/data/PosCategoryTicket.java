package com.rtw.autopricing.ftp.indux.data;

import java.io.Serializable;


public class PosCategoryTicket implements Serializable {
	
	private Integer categoryTicketId; 
	private Integer categoryTicketGroupId;
	private Integer position;
	
	public Integer getCategoryTicketId() {
		return categoryTicketId;
	}
	public void setCategoryTicketId(Integer categoryTicketId) {
		this.categoryTicketId = categoryTicketId;
	}
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	} 
	
	

	
}
