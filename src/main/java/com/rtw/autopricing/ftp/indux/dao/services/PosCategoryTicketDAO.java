package com.rtw.autopricing.ftp.indux.dao.services;

import java.util.List;

import com.rtw.autopricing.ftp.indux.data.PosCategoryTicket;


public interface PosCategoryTicketDAO {
	public Integer save(PosCategoryTicket categoryTicket,Integer brokerId) throws Exception;
	public boolean saveAll(List<PosCategoryTicket> categoryTickets,Integer brokerId) throws Exception;
	public Integer deleteByCategoryTicketGroup(Integer categoryTicketGrouId,Integer brokerId) throws Exception;
	public List<Integer> getCategoryTicketIds(Integer categoryTicketGroupId,Integer brokerId) throws Exception;	
}
