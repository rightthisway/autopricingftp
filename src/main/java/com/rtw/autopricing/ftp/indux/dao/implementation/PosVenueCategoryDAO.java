package com.rtw.autopricing.ftp.indux.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtw.autopricing.ftp.dao.services.VenueCategoryDAO;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;
import com.rtw.autopricing.util.CategoryTicket;
import com.rtw.autopricing.util.connection.Connections;


public class PosVenueCategoryDAO implements com.rtw.autopricing.ftp.indux.dao.services.PosVenueCategoryDAO{
	
	public Integer deleteByVenueCategoryId(Integer venueCategoryId,Integer brokerId) throws Exception{
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "DELETE FROM venue_category where venue_category_id=? ";
		try {
			PreparedStatement deleteStatement = connection.prepareStatement(sql);
			connection.setAutoCommit(false);
			deleteStatement.setInt(1, venueCategoryId);
			deleteStatement.executeUpdate();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			//return 0;
			throw e;
		}finally{

		}
		return 1;
	}
	                              
	                              
	

	
 public List<PosVenueCategory> getAllVenueCategorysByVenueId(Integer venueId,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT venue_category_id,section_low,row_low,venue_configuration_zone_id FROM venue_category where venue_id=? and section_high=''" +
				" and row_high='' and text_desc='' and show_to_sales_staff=1 and default_ticket_group_notes='' " +
				" and ticket_group_stock_type_id=1 and ticket_group_type_id=1 and default_wholesale_price=0.00 and " +
				" default_retail_price=0.00 " +
				" and((seat_high='0' and seat_low='0') or (seat_high='' and seat_low=''))";
		ResultSet resultSet =null;
		List<PosVenueCategory> venuecatList = new ArrayList<PosVenueCategory>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, venueId);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				PosVenueCategory venuecategory = new PosVenueCategory();
				venuecategory.setId(resultSet.getInt("venue_category_id"));
				venuecategory.setSection(resultSet.getString("section_low"));
				venuecategory.setRow(resultSet.getString("row_low"));
				venuecategory.setVenueConfigZoneId(resultSet.getInt("venue_configuration_zone_id"));
				venuecatList.add(venuecategory);
			}
			return venuecatList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		//return venuecatList;
	}
	
	public Integer save(CategoryTicket catTicket,Integer brokerId,AutopricingProduct product) throws Exception{
		
		String row = "";
		if(product.getName().equalsIgnoreCase("AutoCats96")) {
			row = catTicket.getCatRow();
		} else if(product.getName().equalsIgnoreCase("MiniCats") || product.getName().equalsIgnoreCase("VIP MiniCats") ||
				product.getName().equalsIgnoreCase("PresaleAutoCat")) {
			row = catTicket.getRowRange();
		} else if(product.getName().equalsIgnoreCase("LastRow MiniCats") || product.getName().equalsIgnoreCase("VIPlastrowcats")
				|| product.getName().equalsIgnoreCase("SGLastFiveRow")) {
			row = catTicket.getLastRow();
		} else if(product.getName().equalsIgnoreCase("ZonesPricing")) {
			row = "";
		} else if(product.getName().equalsIgnoreCase("TNDZones") || product.getName().equalsIgnoreCase("TNDRealTix")) {
			row = catTicket.getLastRow();
		}
		
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "INSERT INTO venue_category (section_high,section_low,row_low,row_high,seat_low,seat_high,default_wholesale_price" +
				",default_retail_price,venue_id,text_desc,show_to_sales_staff,default_ticket_group_notes ,ticket_group_stock_type_id," +
				"ticket_group_type_id,venue_configuration_zone_id) VALUES (''," +
				"'"+catTicket.getSection()+"','"+row+"','','','',0.00,0.00, "+catTicket.getPosVenueId()+",'',1,'',1,1,"+catTicket.getVenueConfigurationZoneId()+")";
		ResultSet rs= null;
		Statement insertVenueCategoryStatement =null;
		try {
			connection.setAutoCommit(false);
			insertVenueCategoryStatement = connection.createStatement();
			insertVenueCategoryStatement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
			rs = insertVenueCategoryStatement.getGeneratedKeys();
			Integer venueCategoryId =null;
			if(rs.next()){
				venueCategoryId = rs.getInt(1);
			}
			connection.commit();
			return venueCategoryId ;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertVenueCategoryStatement!=null){
				insertVenueCategoryStatement.close();
			}
		}
		//return -1;
	}
	
	public Integer getVenueCategoryId(CategoryTicket catTicket,Integer brokerId) throws Exception{
		
		Connection connection = Connections.getInduxConnection(brokerId); 
		String sql = "SELECT venue_category_id FROM venue_category where section_low=? and row_low = ? ";
		ResultSet resultSet =null;
		Integer venueCatId = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, catTicket.getSection());
			statement.setString(2, catTicket.getRowRange());
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				venueCatId = resultSet.getInt("venue_category_id");
			}
			return venueCatId;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
		}
		//return null;
	}
	
	
}
