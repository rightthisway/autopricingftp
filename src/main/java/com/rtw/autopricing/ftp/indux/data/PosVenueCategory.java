package com.rtw.autopricing.ftp.indux.data;


import java.io.Serializable;
import java.util.Date;



public final class PosVenueCategory implements Serializable  {
	protected static final long serialVersionUID = -7402732543016575381L;
	
	
	protected Integer id;
	protected Integer venueId;
	protected String section;
	protected String row;
	protected Integer venueConfigZoneId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public Integer getVenueConfigZoneId() {
		return venueConfigZoneId;
	}

	public void setVenueConfigZoneId(Integer venueConfigZoneId) {
		this.venueConfigZoneId = venueConfigZoneId;
	}

	
	
}