package com.rtw.autopricing.ftp.indux.dao.services;

import com.rtw.autopricing.ftp.indux.data.UserOffice;

public interface UserOfficeDAO {
                     
	public UserOffice getUserOffice(Integer brokerId,String officeDesc) throws Exception;
}
