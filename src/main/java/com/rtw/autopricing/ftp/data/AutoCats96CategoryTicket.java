package com.rtw.autopricing.ftp.data;



import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.enums.TicketDeliveryType;
import com.rtw.autopricing.util.BrokerUtils;
import com.rtw.autopricing.util.CategoryTicket;


@Entity
@Table(name="autocats96_category_ticket")
public class AutoCats96CategoryTicket implements Serializable, CategoryTicket{


	private Integer id;
	private String section;
	private Integer quantity;
	private Date expectedArrivalDate;
	private Integer shippingMethodSpecialId;
	private Integer nearTermOptionId;
	private Integer eventId;
	private Integer tnExchangeEventId;
	private Integer tnCategoryTicketGroupId;
	private Category category;
	private Integer categoryId;
	private Integer ticketId;
	private String itemId;
	
	private Integer posEventId;
	private Integer posVenueCategoryId;
	private Integer posVenueId;
	private String expectedarrivalDateStr;

	
	private String browseRow;
	
	private long otherExchangeTicketId;
	private String status;
	private Double purPrice;
	
	private Double tnPrice;
	private Double vividPrice;
	private Double tickpickPrice;
	private Double scoreBigPrice;
	private Double fanxchangePrice;
	private Double ticketcityPrice;
	
	private Integer baseTicketOne;
	private Integer baseTicketTwo;
	private Integer baseTicketThree;
	private String priceHistory;
	private String rowRange;
	private String lastRow;
	private Double actualPrice;
	private Boolean isEdited=false;
	private Boolean isTnUpdate=false;
	
	private Date createdDate;
	private Date lastUpdated;
	private String ticketIdHistory;
	private String baseTicketOneHistory;
	private String baseTicketTwoHistory;
	private String baseTicketThreeHistory;
	
	private Double baseTicketOnePurPrice;
	private Double baseTicketTwoPurPrice;
	private Double baseTicketThreePurPrice;
	private Integer tnBrokerId;
	private String brokerName;
	private String reason;
	private String tmatZone;
	
	//for Ticket details page
	private String popPrice;
	private String popDate;
	
	private String internalNotes;
	private String catRow;
	private String alternateRow;
	private String normalizedSection;
	private String tmatCategory;
	private Integer productPriority;
	private Integer qtyPriority;
	private String tmatSection;
	private String productName;
	private Integer catsPriority;
	private Boolean isPresale;
	
	public AutoCats96CategoryTicket() {
		// TODO Auto-generated constructor stub
	}
	
	public AutoCats96CategoryTicket(CategoryTicket catTicket) {
		this.section = catTicket.getSection();
		this.quantity = catTicket.getQuantity();
		this.eventId = catTicket.getEventId();
//		this.posEventId = catTicket.getPosEventId();
		this.categoryId = catTicket.getCategoryId();
		this.ticketId = catTicket.getTicketId();
		this.itemId = catTicket.getItemId();
		this.lastRow = catTicket.getLastRow();
		this.baseTicketOne = catTicket.getBaseTicketOne();
		this.baseTicketTwo = catTicket.getBaseTicketTwo();
		this.baseTicketThree = catTicket.getBaseTicketThree();
		this.actualPrice = catTicket.getActualPrice();
		this.tnPrice = catTicket.getTnPrice();
		this.vividPrice = catTicket.getVividPrice();
		this.scoreBigPrice = catTicket.getScoreBigPrice();
		this.fanxchangePrice = catTicket.getFanxchangePrice();
		this.ticketcityPrice = catTicket.getTicketcityPrice();
		this.tickpickPrice = catTicket.getTickpickPrice();
		this.nearTermOptionId = catTicket.getNearTermOptionId();
		this.shippingMethodSpecialId = catTicket.getShippingMethodSpecialId();
		this.expectedArrivalDate = catTicket.getExpectedArrivalDate();
		this.status = "ACTIVE";
		this.baseTicketOnePurPrice = catTicket.getBaseTicketOnePurPrice();
		this.baseTicketTwoPurPrice = catTicket.getBaseTicketTwoPurPrice();
		this.baseTicketThreePurPrice = catTicket.getBaseTicketThreePurPrice();
		this.purPrice = catTicket.getPurPrice();
		this.tmatZone = catTicket.getTmatZone();
		
	}
	
	public AutoCats96CategoryTicket(AutoCats96CategoryTicket autocats96Tix) {
		this.section = autocats96Tix.getSection();
		this.quantity = autocats96Tix.getQuantity();
		this.eventId = autocats96Tix.getEventId();
//		this.posEventId = catTicket.getPosEventId();
		this.categoryId = autocats96Tix.getCategoryId();
		this.ticketId = autocats96Tix.getTicketId();
		this.itemId = autocats96Tix.getItemId();
		this.lastRow = autocats96Tix.getLastRow();
		this.baseTicketOne = autocats96Tix.getBaseTicketOne();
		this.baseTicketTwo = autocats96Tix.getBaseTicketTwo();
		this.baseTicketThree = autocats96Tix.getBaseTicketThree();
		this.actualPrice = autocats96Tix.getActualPrice();
		this.tnPrice = autocats96Tix.getTnPrice();
		this.vividPrice = autocats96Tix.getVividPrice();
		this.scoreBigPrice = autocats96Tix.getScoreBigPrice();
		this.fanxchangePrice = autocats96Tix.getFanxchangePrice();
		this.ticketcityPrice = autocats96Tix.getTicketcityPrice();
		this.tickpickPrice = autocats96Tix.getTickpickPrice();
		this.nearTermOptionId = autocats96Tix.getNearTermOptionId();
		this.shippingMethodSpecialId = autocats96Tix.getShippingMethodSpecialId();
		this.expectedArrivalDate = autocats96Tix.getExpectedArrivalDate();
		this.status = "ACTIVE";
		this.baseTicketOnePurPrice = autocats96Tix.getBaseTicketOnePurPrice();
		this.baseTicketTwoPurPrice = autocats96Tix.getBaseTicketTwoPurPrice();
		this.baseTicketThreePurPrice = autocats96Tix.getBaseTicketThreePurPrice();
		this.purPrice = autocats96Tix.getPurPrice();
		this.tmatZone = autocats96Tix.getTmatZone();
		this.actualPrice = autocats96Tix.getActualPrice();
		this.alternateRow = autocats96Tix.getAlternateRow();
		this.baseTicketOneHistory=autocats96Tix.getBaseTicketOneHistory();
		this.baseTicketThreeHistory=autocats96Tix.getBaseTicketThreeHistory();
		this.baseTicketTwoHistory=autocats96Tix.getBaseTicketTwoHistory();
		this.brokerName=autocats96Tix.getBrokerName();
		this.browseRow=autocats96Tix.getBrowseRow();
		this.category=autocats96Tix.getCategory();
		this.categoryId=autocats96Tix.getCategoryId();
		this.catRow=autocats96Tix.getCatRow();
		this.catsPriority=autocats96Tix.getCatsPriority();
		this.createdDate=autocats96Tix.getCreatedDate();
		this.expectedarrivalDateStr=autocats96Tix.getExpectedarrivalDateStr();
		this.internalNotes=autocats96Tix.getInternalNotes();
		//this.isEdited=autocats96Tix.getIsEdited();
		this.isPresale=autocats96Tix.getIsPresale();
		this.isTnUpdate=autocats96Tix.getIsTnUpdate();
		this.lastUpdated=autocats96Tix.getLastUpdated();
		this.nearTermOptionId=autocats96Tix.getNearTermOptionId();
		this.normalizedSection=autocats96Tix.getNormalizedSection();
		this.otherExchangeTicketId=autocats96Tix.getOtherExchangeTicketId();
		this.popDate=autocats96Tix.getPopDate();
		this.popPrice=autocats96Tix.getPopPrice();
		this.posEventId=autocats96Tix.getPosEventId();
		this.posVenueCategoryId=autocats96Tix.getPosVenueCategoryId();
		this.posVenueId=autocats96Tix.getPosVenueId();
		this.priceHistory=autocats96Tix.getPriceHistory();
		this.productName=autocats96Tix.getProductName();
		this.productPriority=autocats96Tix.getProductPriority();
		this.qtyPriority=autocats96Tix.getQtyPriority();
		this.quantity=autocats96Tix.getQuantity();
		this.reason=autocats96Tix.getReason();
		this.rowRange=autocats96Tix.getRowRange();
		this.ticketIdHistory=autocats96Tix.getTicketIdHistory();
		this.tmatCategory=autocats96Tix.getTmatCategory();
		this.tmatSection=autocats96Tix.getTmatSection();
		this.tnBrokerId=autocats96Tix.getTnBrokerId();
		this.tnExchangeEventId=autocats96Tix.getTnExchangeEventId();
		this.tnPrice=autocats96Tix.getTnPrice();
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="basetickettwo_history")
	public String getBaseTicketTwoHistory() {
		return baseTicketTwoHistory;
	}
	public void setBaseTicketTwoHistory(String baseTicketTwoHistory) {
		this.baseTicketTwoHistory = baseTicketTwoHistory;
	}
	
	
	@Column(name="baseticketthree_history")
	public String getBaseTicketThreeHistory() {
		return baseTicketThreeHistory;
	}
	public void setBaseTicketThreeHistory(String baseTicketThreeHistory) {
		this.baseTicketThreeHistory = baseTicketThreeHistory;
	}
	
	@Column(name="baseticketone_history")
	public String getBaseTicketOneHistory() {
		return baseTicketOneHistory;
	}
	public void setBaseTicketOneHistory(String baseTicketOneHistory) {
		this.baseTicketOneHistory = baseTicketOneHistory;
	}
	
	@Column(name="ticketid_history")
	public String getTicketIdHistory() {
		return ticketIdHistory;
	}
	public void setTicketIdHistory(String ticketIdHistory) {
		this.ticketIdHistory = ticketIdHistory;
	}
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	@Column(name="actual_price")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	@Column(name="expected_arrival_date", columnDefinition="DATE")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name="shipping_method_id")
	public Integer getShippingMethodSpecialId() {
		return shippingMethodSpecialId;
	}
	public void setShippingMethodSpecialId(Integer shippingMethodSpecialId) {
		this.shippingMethodSpecialId = shippingMethodSpecialId;
	}
	
	@Column(name="near_term_display_option_id")
	public Integer getNearTermOptionId() {
		return nearTermOptionId;
	}
	public void setNearTermOptionId(Integer nearTermOptionId) {
		this.nearTermOptionId = nearTermOptionId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="tn_exchange_event_id")
	public Integer getTnExchangeEventId() {
		return tnExchangeEventId;
	}
	public void setTnExchangeEventId(Integer tnExchangeEventId) {
		this.tnExchangeEventId = tnExchangeEventId;
	}
	
	@Column(name="tn_category_ticket_group_id")
	public Integer getTnCategoryTicketGroupId() {
		return tnCategoryTicketGroupId;
	}
	public void setTnCategoryTicketGroupId(Integer tnCategoryTicketGroupId) {
		this.tnCategoryTicketGroupId = tnCategoryTicketGroupId;
	}
	
	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="base_ticket2")
	public Integer getBaseTicketTwo() {
		return baseTicketTwo;
	}
	public void setBaseTicketTwo(Integer baseTicketTwo) {
		this.baseTicketTwo = baseTicketTwo;
	}
	
	
	@Column(name="base_ticket3")
	public Integer getBaseTicketThree() {
		return baseTicketThree;
	}
	public void setBaseTicketThree(Integer baseTicketThree) {
		this.baseTicketThree = baseTicketThree;
	}
	@Column(name="price_history")
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	

	
	
	@Column(name="tn_price")
	public Double getTnPrice() {
		return tnPrice;
	}
	public void setTnPrice(Double tnPrice) {
		this.tnPrice = tnPrice;
	}
	
	@Column(name="vivid_price")
	public Double getVividPrice() {
		return vividPrice;
	}
	public void setVividPrice(Double vividPrice) {
		this.vividPrice = vividPrice;
	}
	
	@Column(name="tickpick_price")
	public Double getTickpickPrice() {
		return tickpickPrice;
	}
	public void setTickpickPrice(Double tickpickPrice) {
		this.tickpickPrice = tickpickPrice;
	}
	
	@Column(name="scorebig_price")
	public Double getScoreBigPrice() {
		return scoreBigPrice;
	}
	public void setScoreBigPrice(Double scoreBigPrice) {
		this.scoreBigPrice = scoreBigPrice;
	}
	@Column(name="fanxchange_price")
	public Double getFanxchangePrice() {
		return fanxchangePrice;
	}

	public void setFanxchangePrice(Double fanxchangePrice) {
		this.fanxchangePrice = fanxchangePrice;
	}
	
	@Column(name="ticketcity_price")
	public Double getTicketcityPrice() {
		return ticketcityPrice;
	}

	public void setTicketcityPrice(Double ticketcityPrice) {
		this.ticketcityPrice = ticketcityPrice;
	}
	
	@Column(name="base_ticket1")
	public Integer getBaseTicketOne() {
		if(baseTicketOne==null){
			baseTicketOne=0;
		}
		return baseTicketOne;
	}
	public void setBaseTicketOne(Integer baseTicketOne) {
		this.baseTicketOne = baseTicketOne;
	}

	@Column(name="row_range")
	public String getRowRange() {
		if(rowRange==null){
			rowRange="";
		}
		return rowRange;
	}
	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}
	@Column(name="last_row")
	public String getLastRow() {
		return lastRow;
	}
	public void setLastRow(String lastRow) {
		this.lastRow = lastRow;
	}
	
	@Transient
	public String getExpectedArrivalDateStr() {
		if(expectedArrivalDate==null){
			return null;
		}
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm");
		return df.format(expectedArrivalDate);
	}
	
	@Transient
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	@Column(name="item_id")
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	@Transient
	public Integer getPosEventId() {
		return posEventId;
	}
	public void setPosEventId(Integer posEventId) {
		this.posEventId = posEventId;
	}
	
	@Transient
	public Integer getPosVenueCategoryId() {
		return posVenueCategoryId;
	}
	public void setPosVenueCategoryId(Integer posVenueCategoryId) {
		this.posVenueCategoryId = posVenueCategoryId;
	}
	
	@Transient
	public Integer getPosVenueId() {
		return posVenueId;
	}
	public void setPosVenueId(Integer posVenueId) {
		this.posVenueId = posVenueId;
	}
	
	@Transient
	public String getExpectedarrivalDateStr() {
		return expectedarrivalDateStr;
	}
	public void setExpectedarrivalDateStr(String expectedarrivalDateStr) {
		this.expectedarrivalDateStr = expectedarrivalDateStr;
	}

	@Transient
	public String getBrowseRow() {
		return browseRow;
	}
	public void setBrowseRow(String browseRow) {
		this.browseRow = browseRow;
	}
	@Transient
	public long getOtherExchangeTicketId() {
		return otherExchangeTicketId;
	}
	public void setOtherExchangeTicketId(long otherExchangeTicketId) {
		this.otherExchangeTicketId = otherExchangeTicketId;
	}

	@Transient
	public Double getPurPrice() {
		return purPrice;
	}
	public void setPurPrice(Double purPrice) {
		this.purPrice = purPrice;
	}
	
	@Transient
	public Boolean getIsEdited() {
		return isEdited;
	}
	public void setIsEdited(Boolean isEdited) {
		this.isEdited = isEdited;
	}
	
	@Transient
	public Double getBaseTicketOnePurPrice() {
		return baseTicketOnePurPrice;
	}
	
	public void setBaseTicketOnePurPrice(Double baseTicketOnePurPrice) {
		this.baseTicketOnePurPrice = baseTicketOnePurPrice;
	}
	
	@Transient
	public Double getBaseTicketTwoPurPrice() {
		return baseTicketTwoPurPrice;
	}
	public void setBaseTicketTwoPurPrice(Double baseTicketTwoPurPrice) {
		this.baseTicketTwoPurPrice = baseTicketTwoPurPrice;
	}
	
	@Transient
	public Double getBaseTicketThreePurPrice() {
		return baseTicketThreePurPrice;
	}
	public void setBaseTicketThreePurPrice(Double baseTicketThreePurPrice) {
		this.baseTicketThreePurPrice = baseTicketThreePurPrice;
	}
	
	@Column(name="broker_id")
	public Integer getTnBrokerId() {
		return tnBrokerId;
	}
	public void setTnBrokerId(Integer tnBrokerId) {
		this.tnBrokerId = tnBrokerId;
	}
	@Column(name="reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	@Column(name="tmat_zone")
	public String getTmatZone() {
		return tmatZone;
	}

	public void setTmatZone(String tmatZone) {
		this.tmatZone = tmatZone;
	}
	
	@Transient
	public Integer getPosPurchaseOrderId() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setPosPurchaseOrderId(Integer posPurchaseOrderId) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Transient
	public Integer getPosExchangeEventId() {
		return tnExchangeEventId;
	}
	
	public void setPosExchangeEventId(Integer posExchangeEventId) {
		this.tnExchangeEventId = posExchangeEventId;
		
	}
	
	@Transient
	public Integer getTnTicketGroupId() {
		// TODO Auto-generated method stub
		return tnCategoryTicketGroupId;
	}
	public void setTnTicketGroupId(Integer tnTicketGroupId) {
		this.tnCategoryTicketGroupId = tnTicketGroupId;
		
	}
	
	
	@Transient
	public Boolean getIsPosUpdate() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setIsPosUpdate(Boolean isPosUpdate) {
		// TODO Auto-generated method stub
		
	}
	
	@Transient
	public void createCategoryTicket(Ticket ticket) {
		this.section = ticket.getNormalizedSection().toUpperCase();
		this.eventId = ticket.getEventId();
		this.categoryId = ticket.getCategoryMapping().getCategoryId();
		this.tmatCategory=ticket.getCategory().getSymbol();
		this.ticketId = ticket.getId();
		this.itemId = ticket.getItemId();
		
		//Tamil 12/27/20178: for all events minus 5% percentage from cheapest TMAT ticket price
		//Tamil 10/11/2017 : for all events minus $1 dollar from cheapest TMAT ticket price
		//Tamil 12/25/2018 : for all events minus $15 dollar from cheapest TMAT ticket price
//		this.purPrice = ticket.getPurchasePrice() - 15;
		this.purPrice = Math.floor(ticket.getPurchasePrice()*0.95);
		
		this.status = "ACTIVE";
		this.normalizedSection=ticket.getNormalizedSection().toLowerCase();
		this.tmatZone = ticket.getCategory().getSymbol();
		
		if(this.internalNotes != null && this.internalNotes.equalsIgnoreCase("AUTOCAT")) {
			String sectionRange = ticket.getCategory().getNormalizedZone();
			String rowRange = ticket.getCategoryMapping().getRowRange();
			
			if(sectionRange != null) {
				sectionRange = sectionRange.replaceAll("'", "-");
				if(sectionRange.contains("%")) {
					String[] SectionRangeArray = sectionRange.split("%");
					sectionRange= SectionRangeArray[0];
					
					if(SectionRangeArray.length > 1) {
						rowRange= SectionRangeArray[1];
					}
				}
			}
			this.section=sectionRange;
			this.rowRange=rowRange;
			this.catRow=rowRange;
		}
		
		
		// this is pos shipping method special id
		// 1. for E-Ticket
		// 0. for Default Website Setting
		//System.out.println("TMAT ticket dilevery type "+ ticket.getTicketDeliveryType());
		if(ticket.getTicketDeliveryType() == null || ticket.getTicketDeliveryType().equals(TicketDeliveryType.MERCURY)) {
			this.shippingMethodSpecialId = 0;
		} else {
			this.shippingMethodSpecialId = 1;
		}

	}
	
	@Transient
	public String getPopPrice() {
		return popPrice;
	}
	public void setPopPrice(String popPrice) {
		this.popPrice = popPrice;
	}
	@Transient
	public String getPopDate() {
		return popDate;
	}
	public void setPopDate(String popDate) {
		this.popDate = popDate;
	}

	@Transient
	public Ticket getTicket() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTicket(Ticket ticket) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Double getZoneTicketPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setZoneTicketPrice(Double zoneTicketPrice) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getSectionRange() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setSectionRange(String sectionRange) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPriority(Integer priority) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getFloorCap() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setFloorCap(Double floorCap) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getVenueConfigurationZoneId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setVenueConfigurationZoneId(Integer venueConfigurationZoneId) {
		// TODO Auto-generated method stub
		
	}

	@Column(name="cat_row")
	public String getCatRow() {
		return catRow;
	}

	public void setCatRow(String catRow) {
		this.catRow = catRow;
	}

	@Transient
	public String getAlternateRow() {
		return alternateRow;
	}

	public void setAlternateRow(String alternateRow) {
		this.alternateRow = alternateRow;
	}

	@Transient
	public String getNormalizedSection() {
		return normalizedSection;
	}

	public void setNormalizedSection(String normalizedSection) {
		this.normalizedSection = normalizedSection;
	}
	
	@Transient
	public String getTmatCategory() {
		return tmatCategory;
	}

	public void setTmatCategory(String tmatCategory) {
		this.tmatCategory = tmatCategory;
	}

	@Transient
	public Integer getProductPriority() {
		return productPriority;
	}

	public void setProductPriority(Integer productPriority) {
		this.productPriority = productPriority;
	}

	@Transient
	public Integer getQtyPriority() {
		return qtyPriority;
	}

	public void setQtyPriority(Integer qtyPriority) {
		this.qtyPriority = qtyPriority;
	}

	@Transient
	public String getTmatSection() {
		return tmatSection;
	}

	public void setTmatSection(String tmatSection) {
		this.tmatSection = tmatSection;
	}

	
	@Column(name="internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	
	@Column(name="is_presale")
	public Boolean getIsPresale() {
		return isPresale;
	}

	public void setIsPresale(Boolean isPresale) {
		this.isPresale = isPresale;
	}

	@Transient
	public Boolean getIsTnUpdate() {
		return isTnUpdate;
	}

	public void setIsTnUpdate(Boolean isTnUpdate) {
		this.isTnUpdate = isTnUpdate;
	}

	@Column(name="product_name")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Transient
	public String getBrokerName() {
		
		if(tnBrokerId != null){
			Broker broker = BrokerUtils.getBrokerById(tnBrokerId);
			brokerName = broker.getName();
		}
		return brokerName;
	}

	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}

	@Transient
	public Integer getCatsPriority() {
		return catsPriority;
	}

	public void setCatsPriority(Integer catsPriority) {
		this.catsPriority = catsPriority;
	}

	@Transient
	public String getTicketDeliveryType() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTicketDeliveryType(String ticketDeliveryType) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getZoneTicketsTicketGroupId() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setZoneTicketsTicketGroupId(Integer zoneTicketsTicketGroupId) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getZoneTicketsEventId() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setZoneTicketsEventId(Integer zoneTicketsEventId) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Double getTaxAmount() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTaxAmount(Double taxAmount) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getTaxAmountHistory() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxAmountHistory(String taxAmountHistory) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Double getTaxPercentage() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxPercentage(Double taxPercentage) {
		// TODO Auto-generated method stub
	}
	@Transient
	public String getTaxPercentageHistory() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxPercentageHistory(String taxPercentageHistory) {
		// TODO Auto-generated method stub
	}
	@Transient
	public Double getTmatPrice() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTmatPrice(Double tmatPrice) {
		// TODO Auto-generated method stub
	}
	
	
	/*public static void main(String[] args) {
		
		Scanner  scr = new Scanner(System.in);
		while (true) {
			Double amt = scr.nextDouble();
			Double fnlAmt = Math.floor(amt*0.95);
			System.out.println("Amt : "+amt+" : "+fnlAmt);
		}
	}*/
}

