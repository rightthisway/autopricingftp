package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="ticket_count_alert")
public class TicketCountAlert implements Serializable {

	private Integer id;
	private String broker;
	private Integer ticketCount;
	private Integer minLimit;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="broker")
	public String getBroker() {
		return broker;
	}
	public void setBroker(String broker) {
		this.broker = broker;
	}
	
	@Column(name="ticket_count")
	public Integer getTicketCount() {
		return ticketCount;
	}
	public void setTicketCount(Integer ticketCount) {
		this.ticketCount = ticketCount;
	}
	
	@Column(name="min_limit")
	public Integer getMinLimit() {
		return minLimit;
	}
	public void setMinLimit(Integer minLimit) {
		this.minLimit = minLimit;
	}
}
