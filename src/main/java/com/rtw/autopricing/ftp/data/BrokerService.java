package com.rtw.autopricing.ftp.data;

public interface BrokerService {
	public Integer getBuyerBrokerId();
	public void setBuyerBrokerId(Integer buyerBrokerId);
	public Integer getClientBrokerEmployeeId();
	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId);
	public Integer getSystemUserId();
	public void setSystemUserId(Integer systemUserId);
	public Integer getClientBrokerId();
	public void setClientBrokerId(Integer clientBrokerId);
	public Boolean getIsTNUpdate();
	public void setIsTNUpdate(Boolean isTNUpdate);
	public Integer getPosBrokerId();
	public void setPosBrokerId(Integer posBrokerId);
	public Integer getMinutePOSAddLimit();
	public void setMinutePOSAddLimit(Integer minutePOSAddLimit);
	public Integer getMinutePOSUpdateLimit();
	public void setMinutePOSUpdateLimit(Integer minutePOSUpdateLimit);
	public Integer getMinutePOSDeleteLimit();
	public void setMinutePOSDeleteLimit(Integer minutePOSDeleteLimit);
}
