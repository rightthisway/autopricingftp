package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;

@Entity
@Table(name="exclude_venue_category_zones")
public class ExcludeVenueCategoryZones implements Serializable{

	private Integer id;
	private Integer venueCategoryId;	
	private Integer productId;
	private Integer brokerId;
	private String zone;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private AutopricingProduct product;
	private Broker broker;
	private VenueCategory venueCategory;
	private String status;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	@Column(name = "last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	@Column(name = "last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	@Transient
	public Broker getBroker() {
		
		if(brokerId == null) {
			return null;
		}
		if(broker == null) {
			broker = DAORegistry.getBrokerDAO().get(brokerId);
		}
		return broker;
	}

	@Transient
	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	@Transient
	public AutopricingProduct getProduct() {
		
		if(productId == null) {
			return null;
		}
		if(product == null) {
			product = DAORegistry.getAutopricingProductDAO().get(productId);
		}
		return product;
	}

	@Transient
	public void setProduct(AutopricingProduct product) {
		this.product = product;
	}
	
	@Column(name = "venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	
	@Column(name = "zone")
	public String getZone() {
		return zone;
	}
	
	public void setZone(String zone) {
		this.zone = zone;
	}
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Transient
	public VenueCategory getVenueCategory() {
		if(venueCategoryId == null){
		    return null;
	    }
	    if(venueCategory == null){
	    	venueCategory = DAORegistry.getVenueCategoryDAO().get(venueCategoryId);
	    }
		return venueCategory;
	}
	
	@Transient
	public void setVenueCategory(VenueCategory venueCategory) {
		this.venueCategory = venueCategory;
	}
	
}
