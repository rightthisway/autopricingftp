package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
@Entity
@Table(name="larrylast_exchange_event_audit")
public class LarryLastExchangeEventAudit  implements Serializable{

	private static final long serialVersionUID = 1L;
		
	private Integer id;
	private Integer eventId;
	private Event event;
	private String exposure;
	private Double rptFactor;
	private Double priceBreakup;
	private Double lowerMarkup;
	private Double upperMarkup;
	private Double lowerShippingFees;
	private Double upperShippingFees;
	private Integer ticketNetworkBrokerId;
	private Integer vividBrokerId;
	private Integer scoreBigBrokerId;
	private Integer zoneTicketBrokerId;
	private Broker zoneTicketBroker;
	private Broker ticketNetworkBroker;
	private Broker vividBroker;
	private Broker scoreBigBroker;
	private String status;
	private Integer shippingMethod;
	private Integer nearTermDisplayOption;
	private Boolean allowSectionRange;
	private Integer shippingDays;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private Boolean zone;
	private Boolean discountZone;
	private Double zoneDiscountPerc;
	private Integer fanxchangeBrokerId;
	private Broker fanxchangeBroker;
	private Integer ticketcityBrokerId;
	private Broker ticketcityBroker;
	private Integer seatGeekBrokerId;
	private Broker seatGeekBroker;
	private Boolean vividEnabled;
	private Boolean rotEnabled;
	private Boolean rtwEnabled;
	private Boolean tixcityEnabled;
	private Boolean zonedLastrowMinicatsEnabled;
	private Boolean larryLastEnabled;
	private Boolean preSaleSettingsUpdated;
	
	public LarryLastExchangeEventAudit() {
		// TODO Auto-generated constructor stub
	}
	public LarryLastExchangeEventAudit(LarryLastExchangeEvent larryLastExchangeEvent) {
		this.eventId = larryLastExchangeEvent.getEventId();
		this.exposure = larryLastExchangeEvent.getExposure();
		this.rptFactor = larryLastExchangeEvent.getRptFactor();
		this.priceBreakup = larryLastExchangeEvent.getPriceBreakup();
		this.lowerMarkup = larryLastExchangeEvent.getLowerMarkup();
		this.upperMarkup = larryLastExchangeEvent.getUpperMarkup();
		this.lowerShippingFees = larryLastExchangeEvent.getLowerShippingFees();
		this.upperShippingFees = larryLastExchangeEvent.getUpperShippingFees();
		this.ticketNetworkBrokerId= larryLastExchangeEvent.getTicketNetworkBrokerId();
		this.vividBrokerId = larryLastExchangeEvent.getVividBrokerId();
		this.scoreBigBrokerId =larryLastExchangeEvent.getScoreBigBrokerId();
		this.zoneTicketBrokerId =larryLastExchangeEvent.getZoneTicketBrokerId();
		this.fanxchangeBrokerId =larryLastExchangeEvent.getFanxchangeBrokerId();
		this.ticketcityBrokerId =larryLastExchangeEvent.getTicketcityBrokerId();
		this.seatGeekBrokerId =larryLastExchangeEvent.getSeatGeekBrokerId();
		this.status = larryLastExchangeEvent.getStatus();
		this.shippingMethod = larryLastExchangeEvent.getShippingMethod();
		this.nearTermDisplayOption = larryLastExchangeEvent.getNearTermDisplayOption();
		this.allowSectionRange = larryLastExchangeEvent.getAllowSectionRange();
		this.shippingDays = larryLastExchangeEvent.getShippingDays();
		this.lastUpdatedBy = larryLastExchangeEvent.getLastUpdatedBy();
		this.lastUpdatedDate = larryLastExchangeEvent.getLastUpdatedDate();
		this.zone = larryLastExchangeEvent.getZone();
		this.discountZone = larryLastExchangeEvent.getDiscountZone();
		this.vividEnabled = larryLastExchangeEvent.getVividEnabled();
		this.rotEnabled = larryLastExchangeEvent.getRotEnabled();
		this.rtwEnabled = larryLastExchangeEvent.getRtwEnabled();
		this.tixcityEnabled = larryLastExchangeEvent.getTixcityEnabled();
		this.zonedLastrowMinicatsEnabled=larryLastExchangeEvent.getZonedLastrowMinicatsEnabled();
		this.larryLastEnabled = larryLastExchangeEvent.getLarryLastEnabled();
		this.preSaleSettingsUpdated = larryLastExchangeEvent.getPreSaleSettingsUpdated();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)  
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Transient
	public Event getEvent() {
		if(event==null){
			if(eventId==null){
				return null;
			}
			event = DAORegistry.getEventDAO().get(eventId);
		}
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	@Column(name="exposure")
	public String getExposure() {
		return exposure;
	}
	public void setExposure(String exposure) {
		this.exposure = exposure;
	}
	
	@Column(name="rpt_factor")
	public Double getRptFactor() {
		return rptFactor;
	}
	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}
	
	@Column(name="price_breakup")
	public Double getPriceBreakup() {
		return priceBreakup;
	}
	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}
	
	@Column(name="lower_markup")
	public Double getLowerMarkup() {
		return lowerMarkup;
	}
	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}
	
	@Column(name="upper_markup")
	public Double getUpperMarkup() {
		return upperMarkup;
	}
	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}
	
	@Column(name="lower_shipping_fees")
	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}
	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}
	
	@Column(name="upper_shipping_fees")
	public Double getUpperShippingFees() {
		return upperShippingFees;
	}
	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}
	
	@Transient
	public Broker getTicketNetworkBroker() {
		if(ticketNetworkBroker==null){
			if(ticketNetworkBrokerId==null){
				return null;
			}
			ticketNetworkBroker = DAORegistry.getBrokerDAO().get(ticketNetworkBrokerId);
		}
		return ticketNetworkBroker;
	}
	
	public void setTicketNetworkBroker(Broker ticketNetworkBroker) {
		this.ticketNetworkBroker = ticketNetworkBroker;
	}
	
	@Column(name="tn_broker_id")
	public Integer getTicketNetworkBrokerId() {
		return ticketNetworkBrokerId;
	}
	public void setTicketNetworkBrokerId(Integer ticketNetworkBrokerId) {
		this.ticketNetworkBrokerId = ticketNetworkBrokerId;
	}
	

	@Transient
	public Broker getVividBroker() {
		if(vividBroker==null){
			if(vividBrokerId==null){
				return null;
			}
			vividBroker = DAORegistry.getBrokerDAO().get(vividBrokerId);
		}
		return vividBroker;
	}
	public void setVividBroker(Broker vividBroker) {
		this.vividBroker = vividBroker;
	}
	
	@Column(name="vivid_broker_id")
	public Integer getVividBrokerId() {
		return vividBrokerId;
	}
	public void setVividBrokerId(Integer vividBrokerId) {
		this.vividBrokerId = vividBrokerId;
	}
	
	@Transient	
	public Broker getScoreBigBroker() {
		if(scoreBigBroker==null){
			if(scoreBigBrokerId==null){
				return null;
			}
			scoreBigBroker = DAORegistry.getBrokerDAO().get(scoreBigBrokerId);
		}
		return scoreBigBroker;
	}
	public void setScoreBigBroker(Broker scoreBigBroker) {
		this.scoreBigBroker = scoreBigBroker;
	}
	
	@Column(name="scorebig_broker_id")
	public Integer getScoreBigBrokerId() {
		return scoreBigBrokerId;
	}
	public void setScoreBigBrokerId(Integer scoreBigBrokerId) {
		this.scoreBigBrokerId = scoreBigBrokerId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	@Column(name="near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}
	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	@Column(name="allow_section_range")
	public Boolean getAllowSectionRange() {
		return allowSectionRange;
	}
	public void setAllowSectionRange(Boolean allowSectionRange) {
		this.allowSectionRange = allowSectionRange;
	}
	
	@Column(name="shipping_days")
	public Integer getShippingDays() {
		return shippingDays;
	}
	public void setShippingDays(Integer shippingDays) {
		this.shippingDays = shippingDays;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name="last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	@Column(name="is_zone")
	public Boolean getZone() {
		if(zone == null){
			zone = false;
		}
		return zone;
	}
	public void setZone(Boolean isZone) {
		this.zone = isZone;
	}
	@Column(name="zoneticket_broker_id")
	public Integer getZoneTicketBrokerId() {
		return zoneTicketBrokerId;
	}
	public void setZoneTicketBrokerId(Integer zoneTicketBrokerId) {
		this.zoneTicketBrokerId = zoneTicketBrokerId;
	}
	
	@Transient
	public Broker getZoneTicketBroker() {

		if(zoneTicketBroker==null){
			if(zoneTicketBrokerId==null){
				return null;
			}
			zoneTicketBroker = DAORegistry.getBrokerDAO().get(zoneTicketBrokerId);
		}
		return zoneTicketBroker;
	
	}
	public void setZoneTicketBroker(Broker zoneTicketBroker) {
		this.zoneTicketBroker = zoneTicketBroker;
	}
	
	@Column(name="is_discount_zone")
	public Boolean getDiscountZone() {
		return discountZone;
	}
	public void setDiscountZone(Boolean discountZone) {
		this.discountZone = discountZone;
	}
	@Column(name="fanxchange_broker_id")
	public Integer getFanxchangeBrokerId() {
		return fanxchangeBrokerId;
	}
	public void setFanxchangeBrokerId(Integer fanxchangeBrokerId) {
		this.fanxchangeBrokerId = fanxchangeBrokerId;
	}
	
	@Transient
	public Broker getFanxchangeBroker() {

		if(fanxchangeBroker==null){
			if(fanxchangeBrokerId==null){
				return null;
			}
			fanxchangeBroker = DAORegistry.getBrokerDAO().get(fanxchangeBrokerId);
		}
		return fanxchangeBroker;
	}
	public void setFanxchangeBroker(Broker fanxchangeBroker) {
		this.fanxchangeBroker = fanxchangeBroker;
	}
	@Column(name="ticketcity_broker_id")
	public Integer getTicketcityBrokerId() {
		return ticketcityBrokerId;
	}
	public void setTicketcityBrokerId(Integer ticketcityBrokerId) {
		this.ticketcityBrokerId = ticketcityBrokerId;
	}
	
	@Column(name="vivid_enabled")
	public Boolean getVividEnabled() {
		return vividEnabled;
	}
	public void setVividEnabled(Boolean vividEnabled) {
		this.vividEnabled = vividEnabled;
	}
	
	@Column(name="rot_pos_enabled")
	public Boolean getRotEnabled() {
		return rotEnabled;
	}
	public void setRotEnabled(Boolean rotEnabled) {
		this.rotEnabled = rotEnabled;
	}
	
	@Column(name="rtw_pos_enabled")
	public Boolean getRtwEnabled() {
		return rtwEnabled;
	}
	public void setRtwEnabled(Boolean rtwEnabled) {
		this.rtwEnabled = rtwEnabled;
	}
	
	@Transient
	public Broker getTicketcityBroker() {
		if(ticketcityBroker==null){
			if(ticketcityBrokerId==null){
				return null;
			}
			ticketcityBroker = DAORegistry.getBrokerDAO().get(ticketcityBrokerId);
		}
		return ticketcityBroker;
	}
	public void setTicketcityBroker(Broker ticketcityBroker) {
		this.ticketcityBroker = ticketcityBroker;
	}
	@Column(name="seatgeek_broker_id")
	public Integer getSeatGeekBrokerId() {
		return seatGeekBrokerId;
	}
	public void setSeatGeekBrokerId(Integer seatGeekBrokerId) {
		this.seatGeekBrokerId = seatGeekBrokerId;
	}
	@Column(name="tixcity_pos_enabled")
	public Boolean getTixcityEnabled() {
		return tixcityEnabled;
	}
	public void setTixcityEnabled(Boolean tixcityEnabled) {
		this.tixcityEnabled = tixcityEnabled;
	}
	
	@Column(name="zoned_lastrowminicats_enabled")
	public Boolean getZonedLastrowMinicatsEnabled() {
		return zonedLastrowMinicatsEnabled;
	}
	public void setZonedLastrowMinicatsEnabled(Boolean zonedLastrowMinicatsEnabled) {
		this.zonedLastrowMinicatsEnabled = zonedLastrowMinicatsEnabled;
	}
	
	@Column(name="larrylast_enabled")
	public Boolean getLarryLastEnabled() {
		return larryLastEnabled;
	}
	public void setLarryLastEnabled(Boolean larryLastEnabled) {
		this.larryLastEnabled = larryLastEnabled;
	}
	
	@Column(name="presale_settings_updated")
	public Boolean getPreSaleSettingsUpdated() {
		return preSaleSettingsUpdated;
	}
	public void setPreSaleSettingsUpdated(Boolean preSaleSettingsUpdated) {
		this.preSaleSettingsUpdated = preSaleSettingsUpdated;
	}
	@Transient
	public Broker getSeatGeekBroker() {
		if(seatGeekBroker==null){
			if(seatGeekBrokerId==null){
				return null;
			}
			seatGeekBroker = DAORegistry.getBrokerDAO().get(seatGeekBrokerId);
		}
		return seatGeekBroker;
	}
	public void setSeatGeekBroker(Broker seatGeekBroker) {
		this.seatGeekBroker = seatGeekBroker;
	}
	
	@Transient
	public Double getZoneDiscountPerc() {
		return zoneDiscountPerc;
	}
	public void setZoneDiscountPerc(Double zoneDiscountPerc) {
		this.zoneDiscountPerc = zoneDiscountPerc;
	}
}
