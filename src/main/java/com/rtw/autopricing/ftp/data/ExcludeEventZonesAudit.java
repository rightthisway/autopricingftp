package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;

@Entity
@Table(name="exclude_event_zones_audit")
public class ExcludeEventZonesAudit implements Serializable{

	private Integer id;
	private Integer eventId;
	private Integer productId;
	private Integer brokerId;
	private String zone;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private AutopricingProduct product;
	private Broker broker;
	private Event event;
	private String action;
	
	public ExcludeEventZonesAudit(ExcludeEventZones excludeEventZone) {
		this.eventId=excludeEventZone.getEventId();
		this.brokerId = excludeEventZone.getBrokerId();
		this.productId = excludeEventZone.getProductId();
		this.zone = excludeEventZone.getZone();
		this.lastUpdatedBy = excludeEventZone.getLastUpdatedBy();
		this.lastUpdatedDate = excludeEventZone.getLastUpdatedDate();
	}
	public ExcludeEventZonesAudit() {
		
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	@Column(name = "last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	@Column(name = "last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	@Transient
	public Broker getBroker() {
		
		if(brokerId == null) {
			return null;
		}
		if(broker == null) {
			broker = DAORegistry.getBrokerDAO().get(brokerId);
		}
		return broker;
	}

	@Transient
	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	@Transient
	public AutopricingProduct getProduct() {
		
		if(productId == null) {
			return null;
		}
		if(product == null) {
			product = DAORegistry.getAutopricingProductDAO().get(productId);
		}
		return product;
	}

	@Transient
	public void setProduct(AutopricingProduct product) {
		this.product = product;
	}
	
	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name = "zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Transient
	public Event getEvent() {
		if(eventId==null){
		    return null;
	    }
	    if(event==null){
	    	event=DAORegistry.getEventDAO().get(eventId);
	    }
		return event;
	}
	
	@Transient
	public void setEvent(Event event) {
		this.event = event;
	}
	
	@Column(name = "action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}	
	
}
