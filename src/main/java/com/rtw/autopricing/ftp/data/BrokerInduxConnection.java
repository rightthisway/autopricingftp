package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

public class BrokerInduxConnection implements Serializable {

	private Integer brokerId;
	private String brokerName;
	private String connectionStatus;
	private Integer catTixCount;
	private String error;
	
	
	
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	public String getConnectionStatus() {
		return connectionStatus;
	}
	public void setConnectionStatus(String connectionStatus) {
		this.connectionStatus = connectionStatus;
	}
	public Integer getCatTixCount() {
		return catTixCount;
	}
	public void setCatTixCount(Integer catTixCount) {
		this.catTixCount = catTixCount;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	
	

}
