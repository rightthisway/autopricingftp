package com.rtw.autopricing.ftp.data;


import java.sql.Time;
import java.util.Date;


public class EventDTO {

	protected Integer id;
	protected String name;
	protected Date eventDate;
	protected Date eventTime;
	protected String eventStatus;
	protected Integer venueId;
	protected Integer admitoneId;
	protected Integer artistId;
	protected Integer venueCategoryId;
	protected Integer noOfCrawls;
	protected Integer grandChildCategoryId;
	protected Integer parentCategoryId;
	protected Integer posExchangeEventId;
	protected Boolean isZoneEvent;
	protected String venueCategoryName;
	protected String posChildCategoryName;
	protected String posParentCategoryName;
	protected String posGrandChildCategoryName;
	protected String state;
	protected String artistName;
	protected Boolean presaleEvent;
	protected String country;
	protected Date eventCreatedDate;
	
	protected Event event;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
	public String getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	public Integer getAdmitoneId() {
		return admitoneId;
	}
	public void setAdmitoneId(Integer admitoneId) {
		this.admitoneId = admitoneId;
	}
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	public Integer getNoOfCrawls() {
		return noOfCrawls;
	}
	public void setNoOfCrawls(Integer noOfCrawls) {
		this.noOfCrawls = noOfCrawls;
	}
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}
	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}
	public Event getEvent() {
		
		if(event == null) {
			event = new Event();
			event.setId(this.id);
			event.setName(this.name);
			event.setVenueCategoryId(this.venueCategoryId);
			
			if(this.eventDate !=null){
				event.setLocalDate(new Date(this.eventDate.getTime()));
			}
			if(this.eventTime !=null){
				event.setLocalTime(new Time(this.eventTime.getTime()));	
			}
		}
		return event;
	}
	public Boolean getIsZoneEvent() {
		return isZoneEvent;
	}
	public void setIsZoneEvent(Boolean isZoneEvent) {
		this.isZoneEvent = isZoneEvent;
	}
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	public Integer getPosExchangeEventId() {
		return posExchangeEventId;
	}
	public void setPosExchangeEventId(Integer posExchangeEventId) {
		this.posExchangeEventId = posExchangeEventId;
	}
	public String getVenueCategoryName() {
		return venueCategoryName;
	}
	public void setVenueCategoryName(String venueCategoryName) {
		this.venueCategoryName = venueCategoryName;
	}
	public String getPosChildCategoryName() {
		return posChildCategoryName;
	}
	public void setPosChildCategoryName(String posChildCategoryName) {
		this.posChildCategoryName = posChildCategoryName;
	}
	public String getPosParentCategoryName() {
		return posParentCategoryName;
	}
	public void setPosParentCategoryName(String posParentCategoryName) {
		this.posParentCategoryName = posParentCategoryName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public Boolean getPresaleEvent() {
		return presaleEvent;
	}
	public void setPresaleEvent(Boolean presaleEvent) {
		this.presaleEvent = presaleEvent;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPosGrandChildCategoryName() {
		return posGrandChildCategoryName;
	}
	public void setPosGrandChildCategoryName(String posGrandChildCategoryName) {
		this.posGrandChildCategoryName = posGrandChildCategoryName;
	}
	public Date getEventCreatedDate() {
		return eventCreatedDate;
	}
	public void setEventCreatedDate(Date eventCreatedDate) {
		this.eventCreatedDate = eventCreatedDate;
	}
	
	
	
}
