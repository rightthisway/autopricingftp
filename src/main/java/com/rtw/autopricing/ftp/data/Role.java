package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="roles")
public class Role implements Serializable{
	private static final long serialVersionUID = 8527900109963859003L;
	
	private Integer id;
	private String name;
	private String description;
	private Set<Privilege> privileges;
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="role_privileges",
        joinColumns=
            @JoinColumn(name="roles_id", referencedColumnName="id"),
        inverseJoinColumns=
            @JoinColumn(name="privileges_id", referencedColumnName="id")
        )
	public Set<Privilege> getPrivileges() {
		return privileges;
	}
		
	public void setPrivileges(Set<Privilege> privileges) {
		this.privileges = privileges;
	}
	
}
