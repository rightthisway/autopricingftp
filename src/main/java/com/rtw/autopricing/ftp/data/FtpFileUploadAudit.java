package com.rtw.autopricing.ftp.data;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.utils.FTPType;
import com.rtw.autopricing.ftp.utils.FTPVendor;

@Entity
@Table(name="file_upload_audit")
public class FtpFileUploadAudit implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer port;
	private String hostName;
	private String userName;
	private String password;
	private String fileSource;
	private String destination;
	private Date lastUploadTime;
	private String uploadType;
	private String status;
	private String failedReason;
	private Long fileSize;
	private File originalFile;
	private FTPType ftpType;
	
	
	public FtpFileUploadAudit(){
		
	}
	
	public FtpFileUploadAudit(String hostName,Integer port,String userName,String password,String fileSource,
			File originalFile,String destination,String uploadType,FTPType ftpType){
		this.hostName=hostName;
		this.port=port;
		this.userName=userName;
		this.password=password;
		this.fileSource=fileSource;
		this.originalFile=originalFile;
		this.destination=destination;
		this.uploadType=uploadType;
		this.ftpType=ftpType;
	}
	 
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="host")
	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	

	@Column(name="source")
	public String getFileSource() {
		return fileSource;
	}

	public void setFileSource(String fileSource) {
		this.fileSource = fileSource;
	}
	

	@Column(name="destination")
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	@Column(name="port")
	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	
	@Column(name="last_upload_time", columnDefinition="DATE")
	public Date getLastUploadTime() {
		return lastUploadTime;
	}

	public void setLastUploadTime(Date lastUploadTime) {
		this.lastUploadTime = lastUploadTime;
	}

	@Column(name="upload_type")
	public String getUploadType() {
		return uploadType;
	}

	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}

	@Column(name="file_size")
	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	
	@Transient
	public File getOriginalFile() {
		return originalFile;
	}

	public void setOriginalFile(File originalFile) {
		this.originalFile = originalFile;
	}

	@Transient
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name="failed_reason")
	public String getFailedReason() {
		return failedReason;
	}

	public void setFailedReason(String failedReason) {
		this.failedReason = failedReason;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ftp_type")
	public FTPType getFtpType() {
		return ftpType;
	}

	public void setFtpType(FTPType ftpType) {
		this.ftpType = ftpType;
	}
	
	
}
