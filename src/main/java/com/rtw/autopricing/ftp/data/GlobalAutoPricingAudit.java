package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="global_autopricing_audit_new")
public class GlobalAutoPricingAudit implements Serializable {
	 private Integer id;
	 private Double lowerMarkup;
	 private Double upperMarkup;
	 private Double lowerShippingFees;
	 private Double upperShippingFees;
	 private String exposure;
	 private Boolean allowSectionRange;
	 private Double rptFactor;
	 private Double priceBreakup;
	 private String eventTypes;
	 private String products;
	 private Integer shippingMethod;
	 private Integer nearTermDisplayOption;
	 private Integer shippingDays;
	 private String userName;
	 private Date createdDate;
	 private String action;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="lower_markup")
	public Double getLowerMarkup() {
		return lowerMarkup;
	}
	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}
	
	@Column(name="upper_markup")
	public Double getUpperMarkup() {
		return upperMarkup;
	}
	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}
	
	@Column(name="lower_shipping_fees")
	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}
	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}
	
	@Column(name="upper_shipping_fees")
	public Double getUpperShippingFees() {
		return upperShippingFees;
	}
	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}
	
	
	@Column(name="rpt_factor")
	public Double getRptFactor() {
		return rptFactor;
	}
	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}
	
	@Column(name="price_breakup")
	public Double getPriceBreakup() {
		return priceBreakup;
	}
	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}
	
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	@Column(name="event_types")
	public String getEventTypes() {
		return eventTypes;
	}
	public void setEventTypes(String eventTypes) {
		this.eventTypes = eventTypes;
	}
	
	@Column(name="shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	@Column(name="near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}

	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	@Column(name="exposure")
	public String getExposure() {
		return exposure;
	}
	public void setExposure(String exposure) {
		this.exposure = exposure;
	}
	@Column(name="shipping_days")
	public Integer getShippingDays() {
		return shippingDays;
	}
	public void setShippingDays(Integer shippingDays) {
		this.shippingDays = shippingDays;
	}
	@Column(name="allow_section_range")
	public Boolean getAllowSectionRange() {
		return allowSectionRange;
	}
	public void setAllowSectionRange(Boolean allowSectionRange) {
		this.allowSectionRange = allowSectionRange;
	}
	@Column(name="products")
	public String getProducts() {
		return products;
	}
	public void setProducts(String products) {
		this.products = products;
	}
	
}
