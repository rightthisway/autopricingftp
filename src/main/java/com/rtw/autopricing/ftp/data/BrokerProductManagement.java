package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Generated;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;

@Entity
@Table(name = "broker_product_management")
public class BrokerProductManagement implements Serializable {

	private Integer id;
	private Integer brokerId;
	//private Boolean isAutoCats;
	//private Boolean isVipAutoCats;
	private Boolean isMiniCats;
	private Boolean isVipMiniCats;
	private Boolean isLastRowMiniCats;
	private Boolean isZonedLastRowMiniCats;
	private Boolean isZonesPricing;
	private Boolean isLarryLast;
	
	private Broker broker;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	@Column(name="is_minicat")
	public Boolean getIsMiniCats() {
		return isMiniCats;
	}
	public void setIsMiniCats(Boolean isMiniCats) {
		this.isMiniCats = isMiniCats;
	}
	
	@Column(name="is_vip_minicat")
	public Boolean getIsVipMiniCats() {
		return isVipMiniCats;
	}
	public void setIsVipMiniCats(Boolean isVipMiniCats) {
		this.isVipMiniCats = isVipMiniCats;
	}
	
	@Column(name="is_lastrow_minicat")
	public Boolean getIsLastRowMiniCats() {
		return isLastRowMiniCats;
	}
	public void setIsLastRowMiniCats(Boolean isLastRowMiniCats) {
		this.isLastRowMiniCats = isLastRowMiniCats;
	}
	
	@Column(name="is_zoned_lastrow_minicat")
	public Boolean getIsZonedLastRowMiniCats() {
		return isZonedLastRowMiniCats;
	}
	public void setIsZonedLastRowMiniCats(Boolean isZonedLastRowMiniCats) {
		this.isZonedLastRowMiniCats = isZonedLastRowMiniCats;
	}
	
	@Column(name="is_zones_pricing")
	public Boolean getIsZonesPricing() {
		return isZonesPricing;
	}
	public void setIsZonesPricing(Boolean iszonesPricing) {
		this.isZonesPricing = iszonesPricing;
	}
	
	@Column(name="is_larrylast")
	public Boolean getIsLarryLast() {
		return isLarryLast;
	}
	public void setIsLarryLast(Boolean isLarryLast) {
		this.isLarryLast = isLarryLast;
	}
	@Transient
	public Broker getBroker() {
		
		if(brokerId == null) {
			return null;
		}
		if(broker == null) {
			broker = DAORegistry.getBrokerDAO().get(brokerId);	
		}
		return broker;
	}
	
}
