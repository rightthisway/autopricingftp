package com.rtw.autopricing.ftp.data;


import java.util.Date;

import com.rtw.autopricing.ftp.enums.TicketStatus;


public class DateTimePop {

	private String tPopPrice;
	private String tPopDate;
	private String tPopPurchasePrices;
	private String tPopTic;
	private String tPopTicDate;
	private String tPopTicSection;
	private String tPopTicRow;
	private String tPopTicItemId;
	private String tPopTicPur;
	private Double tPopTicOnilinePrice;
	private Integer tPopTicquantity;
	private String tPopTicSiteId;
	private Date tPopTicCreatedDate;
	private Date tPopTiclastUpdated;
	private TicketStatus tPopTicTicketStatus;
	private String tPopTicNormalizedSection;
	
	
	
	public String gettPopTicPur() {
		return tPopTicPur;
	}
	public void settPopTicPur(String tPopTicPur) {
		this.tPopTicPur = tPopTicPur;
	}
	public Double gettPopTicOnilinePrice() {
		return tPopTicOnilinePrice;
	}
	public void settPopTicOnilinePrice(Double tPopTicOnilinePrice) {
		this.tPopTicOnilinePrice = tPopTicOnilinePrice;
	}
	public Integer gettPopTicquantity() {
		return tPopTicquantity;
	}
	public void settPopTicquantity(Integer tPopTicquantity) {
		this.tPopTicquantity = tPopTicquantity;
	}
	public String gettPopTicSiteId() {
		return tPopTicSiteId;
	}
	public void settPopTicSiteId(String tPopTicSiteId) {
		this.tPopTicSiteId = tPopTicSiteId;
	}
	public Date gettPopTicCreatedDate() {
		return tPopTicCreatedDate;
	}
	public void settPopTicCreatedDate(Date tPopTicCreatedDate) {
		this.tPopTicCreatedDate = tPopTicCreatedDate;
	}
	public Date gettPopTiclastUpdated() {
		return tPopTiclastUpdated;
	}
	public void settPopTiclastUpdated(Date tPopTiclastUpdated) {
		this.tPopTiclastUpdated = tPopTiclastUpdated;
	}
	public TicketStatus gettPopTicTicketStatus() {
		return tPopTicTicketStatus;
	}
	public void settPopTicTicketStatus(TicketStatus tPopTicTicketStatus) {
		this.tPopTicTicketStatus = tPopTicTicketStatus;
	}
	public String gettPopTicNormalizedSection() {
		return tPopTicNormalizedSection;
	}
	public void settPopTicNormalizedSection(String tPopTicNormalizedSection) {
		this.tPopTicNormalizedSection = tPopTicNormalizedSection;
	}
	public String gettPopTicSection() {
		return tPopTicSection;
	}
	public void settPopTicSection(String tPopTicSection) {
		this.tPopTicSection = tPopTicSection;
	}
	public String gettPopTicRow() {
		return tPopTicRow;
	}
	public void settPopTicRow(String tPopTicRow) {
		this.tPopTicRow = tPopTicRow;
	}
	public String gettPopTicItemId() {
		return tPopTicItemId;
	}
	public void settPopTicItemId(String tPopTicItemId) {
		this.tPopTicItemId = tPopTicItemId;
	}
	public String gettPopPurchasePrices() {
		return tPopPurchasePrices;
	}
	public void settPopPurchasePrices(String tPopPurchasePrices) {
		this.tPopPurchasePrices = tPopPurchasePrices;
	}
	public String gettPopTic() {
		return tPopTic;
	}
	public void settPopTic(String tPopTic) {
		this.tPopTic = tPopTic;
	}
	public String gettPopTicDate() {
		return tPopTicDate;
	}
	public void settPopTicDate(String tPopTicDate) {
		this.tPopTicDate = tPopTicDate;
	}
	public String gettPopPrice() {
		return tPopPrice;
	}
	public void settPopPrice(String tPopPrice) {
		this.tPopPrice = tPopPrice;
	}
	public String gettPopDate() {
		return tPopDate;
	}
	public void settPopDate(String tPopDate) {
		this.tPopDate = tPopDate;
	}


}
