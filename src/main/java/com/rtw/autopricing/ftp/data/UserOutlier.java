package com.rtw.autopricing.ftp.data;



import java.io.Serializable;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.enums.AlertFor;
import com.rtw.autopricing.ftp.enums.UserOutlierStatus;
import com.rtw.autopricing.ftp.web.Constants;



@Entity
@Table(name="user_outlier")
public class UserOutlier implements Serializable {
	private Integer id;
	private Integer eventId;
	private String username;
	private String category;
	private UserOutlierStatus userOutlierStatus;
	private Date lastCheck;
	private String categoryGroupName;
	private String description;
	private String outlierTransType="BROWSE";
	private Integer mailInterval;
	private String siteIdsString;
	private Date creationDate;
	private Date lastEmailDate;
	private Float outlierPercentage;
	private AlertFor outlierFor=AlertFor.TMAT;
	private Event event;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	@Column(name="outlier_percentage")
	public Float getOutlierPercentage() {
		return outlierPercentage;
	}

	public void setOutlierPercentage(Float outlierPercentage) {
		this.outlierPercentage = outlierPercentage;
	}

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="outlier_trans_type")
	public String getOutlierTransType() {
		return outlierTransType;
	}

	public void setOutlierTransType(String outlierTransType) {
		this.outlierTransType = outlierTransType;
	}

	
	@Transient
	public Event getEvent() {
		if(event==null){
			event = DAORegistry.getEventDAO().get(eventId);
		}
		return event; 
	}

	@Transient
	public Artist getArtist() {
		return getEvent().getArtist();
	}

	
	@Column(name="user_outlier_status")
	@Enumerated(EnumType.ORDINAL)
	public UserOutlierStatus getUserOutlierStatus() {
		return userOutlierStatus;
	}

	public void setUserOutlierStatus(UserOutlierStatus userOutlierStatus) {
		this.userOutlierStatus = userOutlierStatus;
	}
	
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	@Transient
	public Boolean getCategoryNull() {
		return category == null;
	}
	
	@Transient
	public String getCategoryDescription() {
		if (category == null) {
			return "Uncategorized";
		} else if (category.isEmpty()) {
			return "All Categories";
		} else {
			return getCategory();
		}
	}

	@Transient
	public User getUser() {
		return DAORegistry.getUserDAO().getUserByUsername(username);
	}
	
	private static Pattern rangePattern = Pattern.compile("^(.*?):(.*?)$");

	
	
	@Column(name="category_group_name")
	public String getCategoryGroupName() {
		return categoryGroupName;
	}

	public void setCategoryGroupName(String categoryGroupName) {
		this.categoryGroupName = categoryGroupName;
	}

	// last time the alertScheduler run the alert.
	// (used to know which ticket we should check (based on the last_updated)
	@Column(name="last_check")
	public Date getLastCheck() {
		return lastCheck;
	}

	public void setLastCheck(Date lastCheck) {
		this.lastCheck = lastCheck;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="mail_interval")
	public Integer getMailInterval() {
		return mailInterval;
	}

	public void setMailInterval(Integer mailInterval) {
		this.mailInterval = mailInterval;
	}

	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="last_email_date")	
	public Date getLastEmailDate() {
		return lastEmailDate;
	}

	public void setLastEmailDate(Date lastEmailDate) {
		this.lastEmailDate = lastEmailDate;
	}

	@Transient
	public String getMarketMakerUsername() {
		User marketMaker = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(eventId);
		if (marketMaker != null) {
			return marketMaker.getUsername();
		} else {
			return "mdesantis";
		}
	}

	@Transient
	public String[] getSiteIds() {
		if (siteIdsString == null) {
			return Constants.getInstance().getSiteIds();
		}
		return siteIdsString.split(",");
	}

	@Transient
	public String getSitesDescription() {
		String[] siteIds = getSiteIds();
		boolean firstElement = true;
		String str = "";
		for(String siteId: siteIds) {
			if (!firstElement) {
				str += ",";
			} else {
				firstElement = false;
			}
			str += siteId;
		}
		return str;
	}

	@Transient
	public void setSiteIds(String[] siteIds) {
		String str = "";
		for(String siteId: siteIds) {
			if (!str.isEmpty()) {
				str += ",";
			}
			str += siteId;
		}
		siteIdsString = str;
	}

	@Column(name="site_ids")
	public String getSiteIdsString() {
		return siteIdsString;
	}

	public void setSiteIdsString(String siteIdsString) {
		this.siteIdsString = siteIdsString;
	}

	public String toString() {
		return "UserAlert: "
			+ ", categoryGroup=" + categoryGroupName
			+ ", category=" + category
			+ ", description=" + description;
	}

	public AlertFor getOutlierFor() {
		return outlierFor;
	}

	public void setOutlierFor(AlertFor outlierFor) {
		this.outlierFor = outlierFor;
	}
	
}