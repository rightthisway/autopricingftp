package com.rtw.autopricing.ftp.data;



import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.enums.AlertFor;
import com.rtw.autopricing.ftp.enums.UserAlertStatus;
import com.rtw.autopricing.ftp.enums.UserAlertType;
import com.rtw.autopricing.ftp.web.Constants;



@Entity
@Table(name="user_alert")
public class UserAlert implements Serializable {
	private Integer id;
	private Integer eventId;
	private String username;
	private String priceRange;
	private String sectionRange;
	private String rowRange;
	private String quantities;
	private String category;
	private UserAlertStatus userAlertStatus;
	private Date lastCheck;
	private String categoryGroupName;
	private UserAlertType userAlertType;
	private String description;
	private String alertTransType="BROWSE";
	
	private AlertFor alertFor;
	private String accountName;
	private String orderedBy;
	private String orderedByEmail;
	private String phoneNumber;
	private String altPhone;
	private String billingName;
	private String srNo;
	private Integer mailInterval;
	private String siteIdsString;
	
	private Date creationDate;
	
	private Date lastEmailDate;
	private String categoryGrpName;
	private Event event;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	@Column(name="alert_trans_type")
	public String getAlertTransType() {
		return alertTransType;
	}

	public void setAlertTransType(String alertTransType) {
		this.alertTransType = alertTransType;
	}

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Transient
	public Event getEvent() {
		
		if (event == null) {
			event = DAORegistry.getEventDAO().get(eventId);
		}
		return event;
	}

	@Transient
	public Artist getArtist() {
		return getEvent().getArtist();
	}

	@Column(name="price_range")
	public String getPriceRange() {
		return priceRange;
	}

	public void setPriceRange(String priceRange) {
		this.priceRange = priceRange;
	}	

	@Transient
	public Double getPriceLow() {
		String priceLowString = getRangeLow(priceRange);
		if (priceLowString != null && !priceLowString.isEmpty()) {
			return Double.parseDouble(priceLowString);
		}
		return null;
	}

	@Transient
	public Double getPriceHigh() {
		String priceHighString = getRangeHigh(priceRange);
		if (priceHighString != null && !priceHighString.isEmpty()) {
			return Double.parseDouble(priceHighString);
		}
		return null;
	}
	
	@Transient
	public Double getAdjustedPriceLow() {
		if (alertFor.equals(AlertFor.TMAT)) {
			return getPriceLow();
		}
		Double priceLow = getPriceLow();
		if (priceLow != null) {
			return priceLow * 0.8;
		}
		return null;
	}

	@Transient
	public Double getAdjustedPriceHigh() {
		if (alertFor.equals(AlertFor.TMAT)) {
			return getPriceHigh();
		}
		Double priceHigh = getPriceHigh();
		if (priceHigh != null) {
			return priceHigh * 0.8;
		}
		return null;
	}

	@Transient
	public String getPriceRangeDescription() {
		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
		if (priceRange == null || priceRange.isEmpty() || priceRange.equals(":")) {
			return "All Prices";
		} else if(getPriceLow() == null) {
			return "<= " + currencyFormat.format(getPriceHigh());
		} else if (getPriceHigh() == null) {
			return ">= " + currencyFormat.format(getPriceLow());			
		} else {
			if (getPriceLow().equals(getPriceHigh())) {
				return currencyFormat.format(getPriceLow());				
			} else {
			//	return currencyFormat.format(getPriceLow()) + " - " + currencyFormat.format(getPriceHigh());
				return currencyFormat.format(getPriceHigh());
			}
		}
	}	

	@Transient
	public String getAdjustedPriceRangeDescription() {
		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
		if (priceRange == null || priceRange.isEmpty() || priceRange.equals(":")) {
			return "All Prices";
		} else if(getAdjustedPriceLow() == null) {
			return "<= " + currencyFormat.format(getAdjustedPriceHigh());
		} else if (getAdjustedPriceHigh() == null) {
			return ">= " + currencyFormat.format(getAdjustedPriceLow());			
		} else {
			if (getAdjustedPriceLow().equals(getAdjustedPriceHigh())) {
				return currencyFormat.format(getAdjustedPriceLow());				
			} else {
				return currencyFormat.format(getAdjustedPriceLow()) + " - " + currencyFormat.format(getAdjustedPriceHigh());
			}
		}
	}	

	@Column(name="section_range")
	public String getSectionRange() {
		return sectionRange;
	}

	public void setSectionRange(String sectionRange) {
		this.sectionRange = sectionRange;
	}

	@Transient
	public Boolean getSectionRangeNull() {
		return sectionRange == null;
	}

	@Transient
	public String getSectionLow() {
		String sectionLowString = getRangeLow(sectionRange);
		if (sectionLowString != null && !sectionLowString.isEmpty()) {
			return sectionLowString;
		}
		return null;
	}

	@Transient
	public String getSectionHigh() {
		String sectionHighString = getRangeHigh(sectionRange);
		if (sectionHighString != null && !sectionHighString.isEmpty()) {
			return sectionHighString;
		}
		return null;
	}

	@Transient
	public String getSectionRangeDescription() {
		if (sectionRange == null) {
			return "Null/Empty Sections";
		} else if (sectionRange.isEmpty() || sectionRange.equals(":")) {
			return "All Sections";
		} else if(getSectionLow() == null) {
			return "<= " + getSectionHigh();
		} else if (getSectionHigh() == null) {
			return ">= " + getSectionLow();			
		} else {
			if (getSectionLow().equals(getSectionHigh())) {				
				return getSectionLow();
			} else {			
				return getSectionLow() + " - " + getSectionHigh();
			}
		}
	}

	@Column(name="row_range")
	public String getRowRange() {
		return rowRange;
	}

	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}

	@Transient
	public String getRowLow() {
		String rowLowString = getRangeLow(rowRange);
		if (rowLowString != null && !rowLowString.isEmpty()) {
			return rowLowString;
		}
		return null;
	}

	@Transient
	public String getRowHigh() {
		String rowHighString = getRangeHigh(rowRange);
		if (rowHighString != null && !rowHighString.isEmpty()) {
			return rowHighString;
		}
		return null;
	}

	@Transient
	public Boolean getRowRangeNull() {
		return rowRange == null;
	}
	
	@Transient
	public String getRowRangeDescription() {
		if (rowRange == null) {
			return "Null/Empty Rows";
		} else if (rowRange.isEmpty() || rowRange.equals(":")) {
			return "All Rows";
		} else if(getRowLow() == null) {
			return "<= " + getRowHigh();
		} else if (getRowHigh() == null) {
			return ">= " + getRowLow();			
		} else {
			if (getRowLow().equals(getRowHigh())) {				
				return getRowLow();
			} else {
				return getRowLow() + " - " + getRowHigh();
			}
		}
	}

	@Column(name="quantities")
	public String getQuantities() {
		return quantities;
	}

	public void setQuantities(String quantities) {
		this.quantities = quantities;
	}

	@Transient
	public String getQuantitiesDescription() {
		if (quantities == null || quantities.isEmpty()) {
			return "All Quantities";
		} else {
			return quantities;
		}
	}

	@Column(name="user_alert_status")
	@Enumerated(EnumType.ORDINAL)
	public UserAlertStatus getUserAlertStatus() {
		return userAlertStatus;
	}
	
	public void setUserAlertStatus(UserAlertStatus userAlertStatus) {
		this.userAlertStatus = userAlertStatus;
	}
	
	@Column(name="category")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	@Transient
	public Boolean getCategoryNull() {
		return category == null;
	}
	
	@Transient
	public String getCategoryDescription() {
		if (category == null) {
			return "Uncategorized";
		} else if (category.isEmpty()) {
			return "All Categories";
		} else {
			return getCategory();
		}
	}

	@Transient
	public User getUser() {
		return DAORegistry.getUserDAO().getUserByUsername(username);
	}
	
	private static Pattern rangePattern = Pattern.compile("^(.*?):(.*?)$");

	@Transient
	private String getRangeLow(String range) {
		if (range == null) {
			return null;
		}
		
		Matcher rangeMatcher = rangePattern.matcher(range);
		if (!rangeMatcher.find()) {
			return range;
		}
		String value = rangeMatcher.group(1).trim();
		if (value.isEmpty()) {
			return null;
		}
		return value;
	}	

	@Transient
	private String getRangeHigh(String range) {
		if (range == null) {
			return null;
		}
		
		Matcher rangeMatcher = rangePattern.matcher(range);
		if (!rangeMatcher.find()) {
			return range;
		}
		String value = rangeMatcher.group(2).trim();
		if (value.isEmpty()) {
			return null;
		}
		return value;
	}
	
	@Column(name="category_group_name")
	public String getCategoryGroupName() {
		return categoryGroupName;
	}

	public void setCategoryGroupName(String categoryGroupName) {
		this.categoryGroupName = categoryGroupName;
	}

	// last time the alertScheduler run the alert.
	// (used to know which ticket we should check (based on the last_updated)
	@Column(name="last_check")
	public Date getLastCheck() {
		return lastCheck;
	}

	public void setLastCheck(Date lastCheck) {
		this.lastCheck = lastCheck;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="user_alert_type")
	public UserAlertType getUserAlertType() {
		return userAlertType;
	}

	public void setUserAlertType(UserAlertType userAlertType) {
		this.userAlertType = userAlertType;
	}

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="alert_for")	
	public AlertFor getAlertFor() {
		return alertFor;
	}

	public void setAlertFor(AlertFor alertFor) {
		this.alertFor = alertFor;
	}

	@Column(name="account_name")
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Column(name="ordered_by")
	public String getOrderedBy() {
		return orderedBy;
	}

	public void setOrderedBy(String orderedBy) {
		this.orderedBy = orderedBy;
	}
	
	@Column(name="ordered_by_email")
	public String getOrderedByEmail() {
		return orderedByEmail;
	}

	public void setOrderedByEmail(String orderedByEmail) {
		this.orderedByEmail = orderedByEmail;
	}	

	@Column(name="phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name="alt_phone")
	public String getAltPhone() {
		return altPhone;
	}

	public void setAltPhone(String altPhone) {
		this.altPhone = altPhone;
	}

	@Column(name="billing_name")
	public String getBillingName() {
		return billingName;
	}

	public void setBillingName(String billingName) {
		this.billingName = billingName;
	}

	@Column(name="sr_no")
	public String getSrNo() {
		return srNo;
	}

	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}
	
	
	@Column(name="interval")
	public Integer getMailInterval() {
		return mailInterval;
	}

	public void setMailInterval(Integer mailInterval) {
		this.mailInterval = mailInterval;
	}

	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="last_email_date")	
	public Date getLastEmailDate() {
		return lastEmailDate;
	}

	public void setLastEmailDate(Date lastEmailDate) {
		this.lastEmailDate = lastEmailDate;
	}
	
	public String getCategoryGrpName() {
		Event event =null;
		if(null != eventId){
			event = DAORegistry.getEventDAO().get(eventId);
		}
		if(null != event && null != event.getVenueCategory()){
			return event.getVenueCategory().getCategoryGroup();
		}
		return categoryGrpName;
	}

	public void setCategoryGrpName(String categoryGrpName) {
		this.categoryGrpName = categoryGrpName;
	}

	@Transient
	public String getMarketMakerUsername() {
		User marketMaker = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(eventId);
		if (marketMaker != null) {
			return marketMaker.getUsername();
		} else {
			return "mdesantis";
		}
	}

	@Transient
	public String[] getSiteIds() {
		if (siteIdsString == null) {
			return Constants.getInstance().getSiteIds();
		}
		return siteIdsString.split(",");
	}

	@Transient
	public String getSitesDescription() {
		String[] siteIds = getSiteIds();
		boolean firstElement = true;
		String str = "";
		for(String siteId: siteIds) {
			if (!firstElement) {
				str += ",";
			} else {
				firstElement = false;
			}
			str += siteId;
		}
		return str;
	}

	@Transient
	public void setSiteIds(String[] siteIds) {
		String str = "";
		for(String siteId: siteIds) {
			if (!str.isEmpty()) {
				str += ",";
			}
			str += siteId;
		}
		siteIdsString = str;
	}

	@Column(name="site_ids")
	public String getSiteIdsString() {
		return siteIdsString;
	}

	public void setSiteIdsString(String siteIdsString) {
		this.siteIdsString = siteIdsString;
	}

	public String toString() {
		return "UserAlert: "
			+ ", categoryGroup=" + categoryGroupName
			+ ", category=" + category
			+ ", section=" + sectionRange
			+ ", row=" + rowRange
			+ ", alertType=" + userAlertType
			+ ", description=" + description;
	}
	
}
