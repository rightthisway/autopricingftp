package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ftp_user_file_information")
public class FtpUserFileInformation implements Serializable{
    
	private int id;
	private String fileSource;
	private String fileName;
	private String filePath;
	private FtpUserAccount ftpUserAccount;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(name="source")
	public String getFileSource() {
		return fileSource;
	}
	public void setFileSource(String fileSource) {
		this.fileSource = fileSource;
	}
	
	@Column(name="file_name")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@ManyToOne
	@JoinColumn(name="account_no")
	public FtpUserAccount getFtpUserAccount() {
		return ftpUserAccount;
	}
	public void setFtpUserAccount(FtpUserAccount ftpUserAccount) {
		this.ftpUserAccount = ftpUserAccount;
	}
	
	@Column(name="file_path")
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
	
}
