package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.rtw.autopricing.ftp.enums.ArtistStatus;

import java.util.List;


@Entity
@Table(name="artist")
public class Artist implements Serializable{
	
	private Integer id;
	private String name;
	private ArtistStatus artistStatus;
	private Integer grandChildTourCategoryId;
	private GrandChildCategory grandChildCategory;
	
	public Artist() {
	}
	
	public Artist(String name,Integer grandChildTourCategoryId,ArtistStatus artistStatus) {
		this.name = name;
		this.grandChildTourCategoryId = grandChildTourCategoryId;
		this.artistStatus = ArtistStatus.ACTIVE;
	}
	
	
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	/*@Column(name="stubhub_id")
	public Integer getStubhubId() {
		return stubhubId;
	}

	public void setStubhubId(Integer stubhubId) {
		this.stubhubId = stubhubId;
	}*/

	@Column(name="artist_status")
	@Enumerated(EnumType.ORDINAL)
	public ArtistStatus getArtistStatus() {
		return artistStatus;
	}

	public void setArtistStatus(ArtistStatus artistStatus) {
		this.artistStatus = artistStatus;
	}

	
	/*@Transient
	public Collection<Tour> getTours() {
		return DAORegistry.getTourDAO().getAllToursByArtist(id);
	}
	
	@Transient
	public int getTourCount() {
		return DAORegistry.getTourDAO().getTourCount(id);
	}*/

	/*@Transient
	public GrandChildCategory getGrandChildTourCategory() {
		if(grandChildCategory!=null){
			return grandChildCategory;
		}
		if(grandChildTourCategoryId!=null){
			grandChildCategory = DAORegistry.getGrandChildTourCategoryDAO().get(grandChildTourCategoryId);
		}
		return grandChildCategory;
		
	}*/

	public void setGrandChildTourCategory(GrandChildCategory grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}

	@Column(name="grand_child_category_id")
	public Integer getGrandChildTourCategoryId() {
		return grandChildTourCategoryId;
	}

	public void setGrandChildTourCategoryId(Integer grandChildTourCategoryId) {
		this.grandChildTourCategoryId = grandChildTourCategoryId;
	}
	
	
	
	/*List<Event> events ;
	@Transient
	public List<Event> getEvents(){
		if(events==null || events.isEmpty()){
			events = (List<Event>)DAORegistry.getEventDAO().getAllActiveEventsByArtistId(this.getId());
		}
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}*/
}
