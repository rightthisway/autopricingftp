package com.rtw.autopricing.ftp.data;



import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="auto_category_ticket")
public class AutoCatsCategoryTicket implements Serializable{

	private Integer id;
	private String section;
	private String rowRange;
	private String lastRow;
	private String quantity;
	private Double actualPrice;
	private Double tnPrice;
	private Double vividPrice;
	private Double tickpickPrice;
	private Double scoreBigPrice;
	
	private Date expectedArrivalDate;
	private Integer shippingMethodId;
	private Integer nearTermDisplayOptionId;
	private Integer eventId;
	//private Integer tnExchangeEventId;
	private Long tnCategoryTicketGroupId;
	private Integer categoryId;
	private Integer ticketId;
	private String status;
	private Date lastUpdated;
	private Date createdDate;
//	private Date eventDate;
//	private Time eventTime;
//	private String eventName;
//	private String venueName;
	
	private Integer baseTicketOne;
	private Integer baseTicketTwo;
	private Integer baseTicketThree;
	private String priceHistory; 
	private String ticketIdHistory;
	private String baseTicketOneHistory;
	private String baseTicketTwoHistory;
	private String baseTicketThreeHistory;
	private String popPrice;
	private String popDate;
	
	
	
	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	
	@Column(name="quantity")
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	@Column(name="price")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	@Column(name="expected_arrival_date", columnDefinition="DATE")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name="ticketid_history")
	public String getTicketIdHistory() {
		return ticketIdHistory;
	}
	public void setTicketIdHistory(String ticketIdHistory) {
		this.ticketIdHistory = ticketIdHistory;
	}
	
	
	@Column(name="baseticketone_history")
	public String getBaseTicketOneHistory() {
		return baseTicketOneHistory;
	}
	public void setBaseTicketOneHistory(String baseTicketOneHistory) {
		this.baseTicketOneHistory = baseTicketOneHistory;
	}
	
	@Column(name="basetickettwo_history")
	public String getBaseTicketTwoHistory() {
		return baseTicketTwoHistory;
	}
	public void setBaseTicketTwoHistory(String baseTicketTwoHistory) {
		this.baseTicketTwoHistory = baseTicketTwoHistory;
	}
	
	
	@Column(name="baseticketthree_history")
	public String getBaseTicketThreeHistory() {
		return baseTicketThreeHistory;
	}
	public void setBaseTicketThreeHistory(String baseTicketThreeHistory) {
		this.baseTicketThreeHistory = baseTicketThreeHistory;
	}
	
	
	@Column(name="shipping_method_id")
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}
	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	
	@Column(name="near_term_display_option_id")
	public Integer getNearTermDisplayOptionId() {
		return nearTermDisplayOptionId;
	}
	public void setNearTermDisplayOptionId(Integer nearTermDisplayOptionId) {
		this.nearTermDisplayOptionId = nearTermDisplayOptionId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
/*	@Column(name="tn_exchange_event_id")
	public Integer getTnExchangeEventId() {
		return tnExchangeEventId;
	}
	public void setTnExchangeEventId(Integer tnExchangeEventId) {
		this.tnExchangeEventId = tnExchangeEventId;
	}*/
	
	@Column(name="tn_category_ticket_group_id")
	public Long getTnCategoryTicketGroupId() {
		return tnCategoryTicketGroupId;
	}
	public void setTnCategoryTicketGroupId(Long tnCategoryTicketGroupId) {
		this.tnCategoryTicketGroupId = tnCategoryTicketGroupId;
	}
	
	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	/*@Column(name="event_date",columnDefinition="DATE")
	public Date getEventDate() {
		return eventDate;
	}
	
	public void setEventDate(Date date) {
		this.eventDate = date;
	}*/

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/*@Column(name="event_time")
	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}*/
	
	@Column(name="last_update_date")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="base_ticket2")
	public Integer getBaseTicketTwo() {
		return baseTicketTwo;
	}
	public void setBaseTicketTwo(Integer baseTicketTwo) {
		this.baseTicketTwo = baseTicketTwo;
	}
	
	@Column(name="base_ticket3")
	public Integer getBaseTicketThree() {
		return baseTicketThree;
	}
	public void setBaseTicketThree(Integer baseTicketThree) {
		this.baseTicketThree = baseTicketThree;
	}
	
	@Column(name="price_history")
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	

	
	
	@Column(name="tn_price")
	public Double getTnPrice() {
		return tnPrice;
	}
	public void setTnPrice(Double tnPrice) {
		this.tnPrice = tnPrice;
	}
	
	@Column(name="vividseat_price")
	public Double getVividPrice() {
		return vividPrice;
	}
	public void setVividPrice(Double vividPrice) {
		this.vividPrice = vividPrice;
	}
	
	@Column(name="tickpick_price")
	public Double getTickpickPrice() {
		return tickpickPrice;
	}
	public void setTickpickPrice(Double tickpickPrice) {
		this.tickpickPrice = tickpickPrice;
	}
	
	@Column(name="scorebig_price")
	public Double getScoreBigPrice() {
		return scoreBigPrice;
	}
	public void setScoreBigPrice(Double scoreBigPrice) {
		this.scoreBigPrice = scoreBigPrice;
	}
	
	@Column(name="base_ticket1")
	public Integer getBaseTicketOne() {
		if(baseTicketOne==null){
			baseTicketOne=0;
		}
		return baseTicketOne;
	}
	public void setBaseTicketOne(Integer baseTicketOne) {
		this.baseTicketOne = baseTicketOne;
	}
	@Transient
	public String getPopPrice() {
		return popPrice;
	}
	public void setPopPrice(String popPrice) {
		this.popPrice = popPrice;
	}
	@Transient
	public String getPopDate() {
		return popDate;
	}
	public void setPopDate(String popDate) {
		this.popDate = popDate;
	}
	@Column(name="row_range")
	public String getRowRange() {
		return rowRange;
	}
	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}
	@Column(name="last_row")
	public String getLastRow() {
		return lastRow;
	}
	public void setLastRow(String lastRow) {
		this.lastRow = lastRow;
	}		
}

