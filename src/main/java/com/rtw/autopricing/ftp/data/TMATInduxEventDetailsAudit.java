package com.rtw.autopricing.ftp.data;


import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tmat_indux_event_details_audit")
public class TMATInduxEventDetailsAudit implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected Integer id;
	protected Integer eventId;
	protected Integer posExchangeEventId;
	protected String posParentCategoryName;	
	protected String posChildCategoryName;
	protected String posGrandChildCategoryName;
	protected Boolean isZoneEvent;
	protected Date lastUpdate;

	
	public TMATInduxEventDetailsAudit() {
		// TODO Auto-generated constructor stub
	}
	
	public TMATInduxEventDetailsAudit(TMATInduxEventDetails tmatInduxEventDetails) {
		this.eventId = tmatInduxEventDetails.getEventId();
		this.posExchangeEventId = tmatInduxEventDetails.getPosExchangeEventId();
		this.posParentCategoryName = tmatInduxEventDetails.getPosParentCategoryName();
		this.posChildCategoryName = tmatInduxEventDetails.getPosChildCategoryName();
		this.posGrandChildCategoryName = tmatInduxEventDetails.getPosGrandChildCategoryName();
		this.isZoneEvent = tmatInduxEventDetails.getIsZoneEvent();
		this.lastUpdate = tmatInduxEventDetails.getLastUpdate();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)  
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="pos_exchange_event_id")
	public Integer getPosExchangeEventId() {
		return posExchangeEventId;
	}
	public void setPosExchangeEventId(Integer posExchangeEventId) {
		this.posExchangeEventId = posExchangeEventId;
	}
	
	@Column(name="pos_parent_category")
	public String getPosParentCategoryName() {
		return posParentCategoryName;
	}
	public void setPosParentCategoryName(String posParentCategoryName) {
		this.posParentCategoryName = posParentCategoryName;
	}
	
	@Column(name="pos_child_category")
	public String getPosChildCategoryName() {
		return posChildCategoryName;
	}
	public void setPosChildCategoryName(String posChildCategoryName) {
		this.posChildCategoryName = posChildCategoryName;
	}
	
	@Column(name="pos_grandchild_category")
	public String getPosGrandChildCategoryName() {
		return posGrandChildCategoryName;
	}
	public void setPosGrandChildCategoryName(String posGrandChildCategoryName) {
		this.posGrandChildCategoryName = posGrandChildCategoryName;
	}
	
	@Column(name="is_zone_event")
	public Boolean getIsZoneEvent() {
		return isZoneEvent;
	}
	public void setIsZoneEvent(Boolean isZoneEvent) {
		this.isZoneEvent = isZoneEvent;
	}
	
	@Column(name="last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
