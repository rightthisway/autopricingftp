package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="category_mapping_new")
public class CategoryMapping implements Serializable {
	private Integer id;
	private Integer categoryId;
//	private Integer eventId;
	private String startSection;
	private String endSection;
	private String startRow;
	private String endRow;
	private String startSeat;
	private String endSeat;
	private String rowRange;
	private String lastRow;
	private String larrySection;
	private String alternateRow; // Not used as of now 
	private String vipLastRow;
//	private String categoryGroupName;
//	private Boolean useForZoneMap;
	
	private String miniLastRow;
	private Set<String> miniLastEligibleRows;
	private List<String> rowList;
	
	// Mehul : added for VipLastRowCats product
	private String miniVipLastRow;
	private Set<String> miniVipLastEligibleRows;
	
	public static final String MINUS_ESCAPE_MATCH_STRING = "%[mM][iI][nN][uU][sS]%";
	public static final String OR_MATCH_STRING = " [oO][rR] ";
	public static final String AND_MATCH_STRING = " [aA][nN][dD] ";
	
	public CategoryMapping(Integer categoryId, String startSection, String endSection, String startRow, String endRow) {
		this.categoryId = categoryId;
		setStartSection(startSection);
		setEndSection(endSection);
		setStartRow(startRow);
		setEndRow(endRow);
//		setCategoryGroupName(DAORegistry.getCategoryDAO().get(categoryId).getGroupName());
	}
	
	public CategoryMapping(Integer categoryId, String startSection, String endSection, String startRow, String endRow, String startSeat, String endSeat) {
		this.categoryId = categoryId;
		setStartSection(startSection);
		setEndSection(endSection);
		setStartRow(startRow);
		setEndRow(endRow);
		setStartSeat(startSeat);
		setEndSeat(endSeat);
//		setCategoryGroupName(DAORegistry.getCategoryDAO().get(categoryId).getGroupName());
	}
	
	public CategoryMapping(CategoryMapping catMapping) {
		setId(catMapping.getId());
		setCategoryId(catMapping.getCategoryId());
		setStartSection(catMapping.getStartSection());
		setEndSection(catMapping.getEndSection());
		setStartRow(catMapping.getStartRow());
		setEndRow(catMapping.getEndRow());
		setStartSeat(catMapping.getStartSeat());
		setEndSeat(catMapping.getEndSeat());
		setLastRow(catMapping.getLastRow());
		setVipLastRow(catMapping.getVipLastRow());
		setRowRange(catMapping.getRowRange());
		//setAlternateRow(catMapping.getAlternateRow());
		setMiniLastRow(catMapping.getMiniLastRow());
		setMiniLastEligibleRows(catMapping.getMiniLastEligibleRows());
	//	setCategory(catMapping.getCategory());
//		setCategoryGroupName(DAORegistry.getCategoryDAO().get(categoryId).getGroupName());
	}
	
	public CategoryMapping() {}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="start_section")
	public String getStartSection() {
		return startSection;
	}
	
	public void setStartSection(String startSection) {
		this.startSection = startSection.replaceAll(MINUS_ESCAPE_MATCH_STRING, "-");
	}
	
	@Column(name="end_section")
	public String getEndSection() {
		return endSection;
	}
	
	public void setEndSection(String endSection) {
		this.endSection = endSection.replaceAll(MINUS_ESCAPE_MATCH_STRING, "-");
	}
	
	@Column(name="start_row")
	public String getStartRow() {
		return startRow;
	}
	
	public void setStartRow(String startRow) {
		this.startRow = startRow.replaceAll(MINUS_ESCAPE_MATCH_STRING, "-");
	}
	
	@Column(name="end_row")
	public String getEndRow() {
		return endRow.replaceAll(MINUS_ESCAPE_MATCH_STRING, "-");
	}
	
	public void setEndRow(String endRow) {
		this.endRow = endRow;
	}

	@Column(name="start_seat")
	public String getStartSeat() {
		return startSeat;
	}

	public void setStartSeat(String startSeat) {
		this.startSeat = startSeat;
	}

	@Column(name="end_seat")
	public String getEndSeat() {
		return endSeat;
	}

	public void setEndSeat(String endSeat) {
		this.endSeat = endSeat;
	}

	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name="row_range")
	public String getRowRange() {
		return rowRange;
	}

	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}

	@Column(name="last_row")
	public String getLastRow() {
		return lastRow;
	}

	
	public void setLastRow(String lastRow) {
		this.lastRow = lastRow;
	}

	@Column(name="alternate_row")
	public String getAlternateRow() {
		return alternateRow;
	}

	public void setAlternateRow(String alternateRow) {
		this.alternateRow = alternateRow;
	}
	
	@Column(name = "larry_section")
	public String getLarrySection() {
		return larrySection;
	}

	public void setLarrySection(String larrySection) {
		this.larrySection = larrySection;
	}

	@Transient
	public String getMiniLastRow() {
		return miniLastRow;
	}

	public void setMiniLastRow(String miniLastRow) {
		this.miniLastRow = miniLastRow;
	}

	@Transient
	public Set<String> getMiniLastEligibleRows() {
		return miniLastEligibleRows;
	}

	public void setMiniLastEligibleRows(Set<String> miniLastEligibleRows) {
		this.miniLastEligibleRows = miniLastEligibleRows;
	}

	@Transient
	public List<String> getRowList() {
		return rowList;
	}

	public void setRowList(List<String> rowList) {
		this.rowList = rowList;
	}
	
	@Column(name="vip_last_row")
	public String getVipLastRow() {
		return vipLastRow;
	}

	public void setVipLastRow(String vipLastRow) {
		this.vipLastRow = vipLastRow;
	}

	@Transient
	public String getMiniVipLastRow() {
		return miniVipLastRow;
	}

	public void setMiniVipLastRow(String miniVipLastRow) {
		this.miniVipLastRow = miniVipLastRow;
	}

	@Transient
	public Set<String> getMiniVipLastEligibleRows() {
		return miniVipLastEligibleRows;
	}

	public void setMiniVipLastEligibleRows(Set<String> miniVipLastEligibleRows) {
		this.miniVipLastEligibleRows = miniVipLastEligibleRows;
	}
	
	
}