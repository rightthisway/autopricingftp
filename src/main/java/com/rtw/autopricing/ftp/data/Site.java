package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

/**
 * Class to store merchant data.
 * For now, it is only storing rounding policy
 */

public class Site implements Serializable {
	public final static String EBAY = "ebay";
	public final static String EVENT_INVENTORY = "eventinventory";
	public final static String EI_MARKETPLACE = "eimarketplace";
	public final static String AOP ="admitoneeventinventory";
	public final static String EIBOX = "eibox";
	public final static String FAN_SNAP = "fansnap";
	public final static String GET_ME_IN = "getmein";
	public final static String RAZOR_GATOR = "razorgator";
	public final static String SEATWAVE = "seatwave";
	public final static String SEATWAVE_FEED = "seatwavefeed";
	public final static String STUB_HUB = "stubhub";
	public final static String TICKET_MASTER = "ticketmaster";
	public final static String TICKET_NETWORK = "ticketnetwork";
	public final static String TICKET_NETWORK_DIRECT = "ticketnetworkdirect";
	public final static String TICKETS_NOW = "ticketsnow";
	public final static String TICKET_SOLUTIONS = "ticketsolutions";
	public final static String VIAGOGO = "viagogo";
	public final static String WS_TICKETS = "wstickets";
	public final static String STUBHUB_FEED = "stubhubfeed";
	public final static String TICKET_EVOLUTION = "ticketevolution";
	public final static String VIVIDSEAT = "vividseat";
	public final static String SEATGEEK = "seatgeek";
	public final static String FLASH_SEATS = "flashseats";
	public final static String TICK_PICK = "tickpick";
	public final static String TICKET_CITY = "ticketcity";
	public final static String TICKET_LIQUIDATOR= "ticketliquidator";
	
	
	private String id;
	//private PriceRounding priceRounding;
	private Integer maxConcurrentConnections;
	
	public Site() {
	}
	
	public Site(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/*public PriceRounding getPriceRounding() {
		return priceRounding;
	}

	public void setPriceRounding(PriceRounding priceRounding) {
		this.priceRounding = priceRounding;
	}*/

	public Integer getMaxConcurrentConnections() {
		return maxConcurrentConnections;
	}

	public void setMaxConcurrentConnections(Integer maxConcurrentConnections) {
		this.maxConcurrentConnections = maxConcurrentConnections;
	}
	
}
