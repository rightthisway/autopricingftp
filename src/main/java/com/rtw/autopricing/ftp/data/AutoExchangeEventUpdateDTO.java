package com.rtw.autopricing.ftp.data;


import java.sql.Time;
import java.util.Date;


public class AutoExchangeEventUpdateDTO {

	protected Integer eventId;
	protected Integer posExchangeEventId;
	protected String posParentCategoryName;	
	protected String posChildCategoryName;
	protected String posGrandChildCategoryName;
	protected Boolean isZoneEvent;

	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public Integer getPosExchangeEventId() {
		return posExchangeEventId;
	}
	public void setPosExchangeEventId(Integer posExchangeEventId) {
		this.posExchangeEventId = posExchangeEventId;
	}
	public String getPosParentCategoryName() {
		return posParentCategoryName;
	}
	public void setPosParentCategoryName(String posParentCategoryName) {
		this.posParentCategoryName = posParentCategoryName;
	}
	public String getPosChildCategoryName() {
		return posChildCategoryName;
	}
	public void setPosChildCategoryName(String posChildCategoryName) {
		this.posChildCategoryName = posChildCategoryName;
	}
	public String getPosGrandChildCategoryName() {
		return posGrandChildCategoryName;
	}
	public void setPosGrandChildCategoryName(String posGrandChildCategoryName) {
		this.posGrandChildCategoryName = posGrandChildCategoryName;
	}
	public Boolean getIsZoneEvent() {
		return isZoneEvent;
	}
	public void setIsZoneEvent(Boolean isZoneEvent) {
		this.isZoneEvent = isZoneEvent;
	}
	

}
