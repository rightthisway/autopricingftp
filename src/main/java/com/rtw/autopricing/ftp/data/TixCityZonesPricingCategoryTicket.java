package com.rtw.autopricing.ftp.data;



import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.enums.TicketDeliveryType;
import com.rtw.autopricing.util.CategoryTicket;

@Entity
@Table(name="tixcity_zones_pricing_category_ticket")
public class TixCityZonesPricingCategoryTicket implements CategoryTicket,Serializable{


	private Integer id;
	private String section;
	private Integer quantity;
	private Date expectedArrivalDate;
	private Integer shippingMethodSpecialId;
	private Integer nearTermOptionId;
	private Integer eventId;
	private Integer tnExchangeEventId;
	private Integer tnCategoryTicketGroupId;
	private Category category;
	private Integer categoryId;
	private Integer ticketId;
	private String itemId;
	
	private Integer posEventId;
	private Integer posVenueCategoryId;
	private Integer posVenueId;
	private String expectedarrivalDateStr;

	
	private String status;
	private Double purPrice;
	
	private Double tnPrice;
	private Double vividPrice;
	private Double tickpickPrice;
	private Double scoreBigPrice;
	private Double fanxchangePrice;
	private Double ticketcityPrice;
	
	private Integer baseTicketOne;
	private Integer baseTicketTwo;
	private Integer baseTicketThree;
	private String priceHistory;
	private String rowRange;
	private String lastRow;
	private Double actualPrice;
	private Boolean isEdited=false;
	
	private Date createdDate;
	private Date lastUpdated;
	private String ticketIdHistory;
	private String baseTicketOneHistory;
	private String baseTicketTwoHistory;
	private String baseTicketThreeHistory;
	
	private Double baseTicketOnePurPrice;
	private Double baseTicketTwoPurPrice;
	private Double baseTicketThreePurPrice;
	private Integer tnBrokerId;
	private String reason;
	private String tmatZone;
	
	private Integer venueConfigurationZoneId;
	private Double floorCap;
	private Ticket ticket;
	
	//for Ticket details page
	private String popPrice;
	private String popDate;
	
	public TixCityZonesPricingCategoryTicket() {
		// TODO Auto-generated constructor stub
	}
	
	public TixCityZonesPricingCategoryTicket(CategoryTicket catTicket) {
		this.section = catTicket.getSection();
		this.quantity = catTicket.getQuantity();
		this.eventId = catTicket.getEventId();
		this.posEventId = catTicket.getPosEventId();
		this.categoryId = catTicket.getCategoryId();
		this.ticketId = catTicket.getTicketId();
		this.itemId = catTicket.getItemId();
		this.baseTicketOne = catTicket.getBaseTicketOne();
		this.baseTicketTwo = catTicket.getBaseTicketTwo();
		this.baseTicketThree = catTicket.getBaseTicketThree();
		this.actualPrice = catTicket.getActualPrice();
		this.tnPrice = catTicket.getTnPrice();
		this.vividPrice = catTicket.getVividPrice();
		this.scoreBigPrice = catTicket.getScoreBigPrice();
		this.fanxchangePrice = catTicket.getFanxchangePrice();
		this.ticketcityPrice = catTicket.getTicketcityPrice();
		this.tickpickPrice = catTicket.getTickpickPrice();
		this.nearTermOptionId = catTicket.getNearTermOptionId();
		this.shippingMethodSpecialId = catTicket.getShippingMethodSpecialId();
		this.expectedArrivalDate = catTicket.getExpectedArrivalDate();
		this.status = "ACTIVE";
		this.baseTicketOnePurPrice = catTicket.getBaseTicketOnePurPrice();
		this.baseTicketTwoPurPrice = catTicket.getBaseTicketTwoPurPrice();
		this.baseTicketThreePurPrice = catTicket.getBaseTicketThreePurPrice();
		this.purPrice = catTicket.getPurPrice();
		this.tmatZone = catTicket.getTmatZone();
		
		this.tnCategoryTicketGroupId = catTicket.getTnCategoryTicketGroupId();
		this.tnExchangeEventId = catTicket.getTnExchangeEventId();
		this.tnBrokerId = catTicket.getTnBrokerId();
		this.priceHistory = catTicket.getPriceHistory();
		this.baseTicketOneHistory= catTicket.getBaseTicketOneHistory();
		this.baseTicketThreeHistory = catTicket.getBaseTicketThreeHistory();
		this.baseTicketThreePurPrice = catTicket.getBaseTicketThreePurPrice();
		this.baseTicketTwoHistory = catTicket.getBaseTicketTwoHistory();
		this.expectedArrivalDate = catTicket.getExpectedArrivalDate();
		this.posEventId = catTicket.getPosEventId();
		this.posVenueCategoryId = catTicket.getPosVenueCategoryId();
		this.venueConfigurationZoneId = catTicket.getVenueConfigurationZoneId();
		this.category = catTicket.getCategory();
		this.ticket = catTicket.getTicket();
		this.floorCap = catTicket.getFloorCap();
		
	}
	/*public TixCityZonesPricingCategoryTicket(String fcZone,Double fcPrice,Event event) {
		
		this.section = fcZone;
		this.quantity = null;
		this.eventId = event.getId();
//		this.posEventId = catTicket.getPosEventId();
		this.categoryId = 0;
		this.ticketId = 0;
		this.itemId = "0";
		this.baseTicketOne = 0;
		this.baseTicketTwo = 0;
		this.baseTicketThree = 0;
		this.actualPrice = fcPrice;
		this.tnPrice = fcPrice;
		this.vividPrice = fcPrice;
		this.scoreBigPrice = fcPrice;
		this.fanxchangePrice = fcPrice;
		this.tickpickPrice = fcPrice;
		//this.nearTermOptionId = catTicket.getNearTermOptionId();
		//this.shippingMethodSpecialId = catTicket.getShippingMethodSpecialId();
		//this.expectedArrivalDate = catTicket.getExpectedArrivalDate();
		this.status = "ACTIVE";
		this.baseTicketOnePurPrice = 0.0;
		this.baseTicketTwoPurPrice = 0.0;
		this.baseTicketThreePurPrice = 0.0;
		this.purPrice = 0.0;
		this.tmatZone =fcZone;
		
		//this.tnBrokerId = catTicket.getTnBrokerId();
		this.priceHistory = null;
		this.baseTicketOneHistory= null;
		this.baseTicketThreeHistory = null;
		this.baseTicketTwoHistory = null;
		this.expectedArrivalDate = null;
		//this.posEventId = catTicket.getPosEventId();
		//this.posVenueCategoryId = catTicket.getPosVenueCategoryId();
		//this.venueConfigurationZoneId = catTicket.getVenueConfigurationZoneId();
		this.category = null;
		this.ticket = null;
		this.floorCap = null;
		
	
	}*/
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="basetickettwo_history")
	public String getBaseTicketTwoHistory() {
		return baseTicketTwoHistory;
	}
	public void setBaseTicketTwoHistory(String baseTicketTwoHistory) {
		this.baseTicketTwoHistory = baseTicketTwoHistory;
	}
	
	
	@Column(name="baseticketthree_history")
	public String getBaseTicketThreeHistory() {
		return baseTicketThreeHistory;
	}
	public void setBaseTicketThreeHistory(String baseTicketThreeHistory) {
		this.baseTicketThreeHistory = baseTicketThreeHistory;
	}
	
	@Column(name="baseticketone_history")
	public String getBaseTicketOneHistory() {
		return baseTicketOneHistory;
	}
	public void setBaseTicketOneHistory(String baseTicketOneHistory) {
		this.baseTicketOneHistory = baseTicketOneHistory;
	}
	
	@Column(name="ticketid_history")
	public String getTicketIdHistory() {
		return ticketIdHistory;
	}
	public void setTicketIdHistory(String ticketIdHistory) {
		this.ticketIdHistory = ticketIdHistory;
	}
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	@Column(name="actual_price")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	@Column(name="expected_arrival_date", columnDefinition="DATE")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name="shipping_method_id")
	public Integer getShippingMethodSpecialId() {
		return shippingMethodSpecialId;
	}
	public void setShippingMethodSpecialId(Integer shippingMethodSpecialId) {
		this.shippingMethodSpecialId = shippingMethodSpecialId;
	}
	
	@Column(name="near_term_display_option_id")
	public Integer getNearTermOptionId() {
		return nearTermOptionId;
	}
	public void setNearTermOptionId(Integer nearTermOptionId) {
		this.nearTermOptionId = nearTermOptionId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="tn_exchange_event_id")
	public Integer getTnExchangeEventId() {
		return tnExchangeEventId;
	}
	public void setTnExchangeEventId(Integer tnExchangeEventId) {
		this.tnExchangeEventId = tnExchangeEventId;
	}
	
	@Column(name="tn_category_ticket_group_id")
	public Integer getTnCategoryTicketGroupId() {
		return tnCategoryTicketGroupId;
	}
	public void setTnCategoryTicketGroupId(Integer tnCategoryTicketGroupId) {
		this.tnCategoryTicketGroupId = tnCategoryTicketGroupId;
	}
	
	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="base_ticket2")
	public Integer getBaseTicketTwo() {
		return baseTicketTwo;
	}
	public void setBaseTicketTwo(Integer baseTicketTwo) {
		this.baseTicketTwo = baseTicketTwo;
	}
	
	
	@Column(name="base_ticket3")
	public Integer getBaseTicketThree() {
		return baseTicketThree;
	}
	public void setBaseTicketThree(Integer baseTicketThree) {
		this.baseTicketThree = baseTicketThree;
	}
	@Column(name="price_history")
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	

	
	
	@Column(name="tn_price")
	public Double getTnPrice() {
		return tnPrice;
	}
	public void setTnPrice(Double tnPrice) {
		this.tnPrice = tnPrice;
	}
	
	@Transient
	public Double getVividPrice() {
		return vividPrice;
	}
	public void setVividPrice(Double vividPrice) {
		this.vividPrice = vividPrice;
	}
	
	@Transient
	public Double getTickpickPrice() {
		return tickpickPrice;
	}
	public void setTickpickPrice(Double tickpickPrice) {
		this.tickpickPrice = tickpickPrice;
	}
	
	@Transient
	public Double getScoreBigPrice() {
		return scoreBigPrice;
	}
	public void setScoreBigPrice(Double scoreBigPrice) {
		this.scoreBigPrice = scoreBigPrice;
	}
	@Transient
	public Double getFanxchangePrice() {
		return fanxchangePrice;
	}

	public void setFanxchangePrice(Double fanxchangePrice) {
		this.fanxchangePrice = fanxchangePrice;
	}
	@Column(name="ticketcity_price")
	public Double getTicketcityPrice() {
		return ticketcityPrice;
	}

	public void setTicketcityPrice(Double ticketcityPrice) {
		this.ticketcityPrice = ticketcityPrice;
	}
	
	@Column(name="base_ticket1")
	public Integer getBaseTicketOne() {
		if(baseTicketOne==null){
			baseTicketOne=0;
		}
		return baseTicketOne;
	}
	public void setBaseTicketOne(Integer baseTicketOne) {
		this.baseTicketOne = baseTicketOne;
	}

	@Transient
	public String getRowRange() {
		if(rowRange==null){
			rowRange="";
		}
		return rowRange;
	}
	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}
	@Transient
	public String getLastRow() {
		return lastRow;
	}
	public void setLastRow(String lastRow) {
		this.lastRow = lastRow;
	}
	
	@Transient
	public String getExpectedArrivalDateStr() {
		if(expectedArrivalDate==null){
			return null;
		}
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm");
		return df.format(expectedArrivalDate);
	}
	
	@Transient
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	@Column(name="item_id")
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	@Transient
	public Integer getPosEventId() {
		return posEventId;
	}
	public void setPosEventId(Integer posEventId) {
		this.posEventId = posEventId;
	}
	
	@Transient
	public Integer getPosVenueCategoryId() {
		return posVenueCategoryId;
	}
	public void setPosVenueCategoryId(Integer posVenueCategoryId) {
		this.posVenueCategoryId = posVenueCategoryId;
	}
	
	@Transient
	public Integer getPosVenueId() {
		return posVenueId;
	}
	public void setPosVenueId(Integer posVenueId) {
		this.posVenueId = posVenueId;
	}
	
	@Transient
	public String getExpectedarrivalDateStr() {
		return expectedarrivalDateStr;
	}
	public void setExpectedarrivalDateStr(String expectedarrivalDateStr) {
		this.expectedarrivalDateStr = expectedarrivalDateStr;
	}

	@Transient
	public Double getPurPrice() {
		return purPrice;
	}
	public void setPurPrice(Double purPrice) {
		this.purPrice = purPrice;
	}
	
	@Transient
	public Boolean getIsEdited() {
		return isEdited;
	}
	public void setIsEdited(Boolean isEdited) {
		this.isEdited = isEdited;
	}
	
	@Transient
	public Double getBaseTicketOnePurPrice() {
		return baseTicketOnePurPrice;
	}
	
	public void setBaseTicketOnePurPrice(Double baseTicketOnePurPrice) {
		this.baseTicketOnePurPrice = baseTicketOnePurPrice;
	}
	
	@Transient
	public Double getBaseTicketTwoPurPrice() {
		return baseTicketTwoPurPrice;
	}
	public void setBaseTicketTwoPurPrice(Double baseTicketTwoPurPrice) {
		this.baseTicketTwoPurPrice = baseTicketTwoPurPrice;
	}
	
	@Transient
	public Double getBaseTicketThreePurPrice() {
		return baseTicketThreePurPrice;
	}
	public void setBaseTicketThreePurPrice(Double baseTicketThreePurPrice) {
		this.baseTicketThreePurPrice = baseTicketThreePurPrice;
	}
	
	@Column(name="tn_broker_id")
	public Integer getTnBrokerId() {
		return tnBrokerId;
	}
	public void setTnBrokerId(Integer tnBrokerId) {
		this.tnBrokerId = tnBrokerId;
	}
	
	@Column(name="reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	@Column(name="tmat_zone")
	public String getTmatZone() {
		return tmatZone;
	}

	public void setTmatZone(String tmatZone) {
		this.tmatZone = tmatZone;
	}
	
	@Column(name="floor_cap")
	public Double getFloorCap() {
		return floorCap;
	}

	public void setFloorCap(Double floorCap) {
		this.floorCap = floorCap;
	}

	@Transient
	public Integer getPosPurchaseOrderId() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setPosPurchaseOrderId(Integer posPurchaseOrderId) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Transient
	public Integer getPosExchangeEventId() {
		return tnExchangeEventId;
	}
	
	public void setPosExchangeEventId(Integer posExchangeEventId) {
		this.tnExchangeEventId = posExchangeEventId;
		
	}
	
	@Transient
	public Integer getTnTicketGroupId() {
		// TODO Auto-generated method stub
		return tnCategoryTicketGroupId;
	}
	public void setTnTicketGroupId(Integer tnTicketGroupId) {
		this.tnCategoryTicketGroupId = tnTicketGroupId;
		
	}
	
	
	@Transient
	public Boolean getIsPosUpdate() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setIsPosUpdate(Boolean isPosUpdate) {
		// TODO Auto-generated method stub
		
	}
	
	@Transient
	public void createCategoryTicket(Ticket ticket) {
		this.section = "ZONE "+ticket.getCategory().getSymbol().toUpperCase();
		//this.section = ticket.getNormalizedSection().toUpperCase();
		this.eventId = ticket.getEventId();
		this.ticketId = ticket.getId();
		this.itemId = ticket.getItemId();
		this.purPrice = ticket.getPurchasePrice();	
		this.status = "ACTIVE";
		this.ticket=ticket;
		this.tmatZone = ticket.getCategory().getSymbol();
	
		if(null != ticket.getCategoryMapping()) {
			this.categoryId = ticket.getCategoryMapping().getCategoryId();
		}
		
		
		// this is pos shipping method special id
		// 1. for E-Ticket
		// 0. for Default Website Setting
		//System.out.println("TMAT delievery type :"+ ticket.getTicketDeliveryType());
		if(ticket.getTicketDeliveryType() == null || ticket.getTicketDeliveryType().equals(TicketDeliveryType.MERCURY)) {
			this.shippingMethodSpecialId = 0;
		} else {
			this.shippingMethodSpecialId = 1;
		}

	}
	
	@Transient
	public String getPopPrice() {
		return popPrice;
	}
	public void setPopPrice(String popPrice) {
		this.popPrice = popPrice;
	}
	@Transient
	public String getPopDate() {
		return popDate;
	}
	public void setPopDate(String popDate) {
		this.popDate = popDate;
	}
	@Transient
	public Integer getVenueConfigurationZoneId() {
		return venueConfigurationZoneId;
	}

	public void setVenueConfigurationZoneId(Integer venueConfigurationZoneId) {
		this.venueConfigurationZoneId = venueConfigurationZoneId;
	}

	@Transient
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	@Transient
	public Double getZoneTicketPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setZoneTicketPrice(Double zoneTicketPrice) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getSectionRange() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setSectionRange(String sectionRange) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPriority(Integer priority) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public String getTicketDeliveryType() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTicketDeliveryType(String ticketDeliveryType) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getProductPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setProductPriority(Integer productPriority) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getQtyPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setQtyPriority(Integer qtyPriority) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getTmatCategory() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTmatCategory(String tmatCategory) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getNormalizedSection() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setNormalizedSection(String normalizedSection) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getAlternateRow() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setAlternateRow(String alternateRow) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getCatRow() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCatRow(String catRow) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getInternalNotes() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setInternalNotes(String internalNotes) {
		// TODO Auto-generated method stub
		
	}
	
	@Transient
	public Integer getCatsPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCatsPriority(Integer catsPriority) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getZoneTicketsTicketGroupId() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setZoneTicketsTicketGroupId(Integer zoneTicketsTicketGroupId) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getZoneTicketsEventId() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setZoneTicketsEventId(Integer zoneTicketsEventId) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getIsTnUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setIsTnUpdate(Boolean isTnUpdate) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getIsPresale() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setIsPresale(Boolean isPresale) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getTaxAmount() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTaxAmount(Double taxAmount) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public String getTaxAmountHistory() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxAmountHistory(String taxAmountHistory) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getTaxPercentage() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxPercentage(Double taxPercentage) {
		// TODO Auto-generated method stub
	}
	@Transient
	public String getTaxPercentageHistory() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxPercentageHistory(String taxPercentageHistory) {
		// TODO Auto-generated method stub
	}
	@Transient
	public String getTmatSection() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTmatSection(String tmatSection) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getTmatPrice() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTmatPrice(Double tmatPrice) {
		// TODO Auto-generated method stub
	}
}
