package com.rtw.autopricing.ftp.data;



import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.enums.TicketDeliveryType;
import com.rtw.autopricing.util.CategoryTicket;


@Entity
@Table(name="seatgeek_upload_cats")
public class SeatGeekUploadCats implements Serializable{


	private Integer id;
	private Integer catTicketId;
	private Integer eventId;
	private String section;
	private String row;
	private String seatFrom;
	private String seatThru;
	private Integer quantity;
	private Double price;
	private String notes;
	private Date inHandDate;
	private String eDelivery;
	private String instant;
	private String edit;
	private Date createdDate;
	private Date lastUpdated;
	private String status;
	private String priceHistory;
	private String eventName;
	private String venueName;
	private Date eventDate;
	private Date eventTime;
	private String tmatZone;
	private Integer shippingMethodId;
	private Integer venueCategoryId;
	private String internalNotes;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="cat_ticket_id")
	public Integer getCatTicketId() {
		return catTicketId;
	}
	public void setCatTicketId(Integer catTicketId) {
		this.catTicketId = catTicketId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name="seat_from")
	public String getSeatFrom() {
		return seatFrom;
	}
	public void setSeatFrom(String seatFrom) {
		this.seatFrom = seatFrom;
	}
	
	@Column(name="seat_thru")
	public String getSeatThru() {
		return seatThru;
	}
	public void setSeatThru(String seatThru) {
		this.seatThru = seatThru;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Column(name="notes")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Transient//@Column(name="inhand_date")
	public Date getInHandDate() {
		return inHandDate;
	}
	@Transient
	public void setInHandDate(Date inHandDate) {
		this.inHandDate = inHandDate;
	}
	
	@Column(name="e_delivery")
	public String geteDelivery() {
		return eDelivery;
	}
	public void seteDelivery(String eDelivery) {
		this.eDelivery = eDelivery;
	}
	
	@Column(name="instant")
	public String getInstant() {
		return instant;
	}
	public void setInstant(String instant) {
		this.instant = instant;
	}
	
	@Column(name="edit")
	public String getEdit() {
		return edit;
	}
	public void setEdit(String edit) {
		this.edit = edit;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="price_history")
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	
	@Transient//@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	@Transient
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Transient//@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	@Transient
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Transient//@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	@Transient
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Transient//@Column(name="event_time")
	public Date getEventTime() {
		return eventTime;
	}
	@Transient
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
	@Column(name="shipping_method_id")
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}
	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	
	@Column(name="internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	@Transient
	public String getTmatZone() {
		return tmatZone;
	}
	@Transient
	public void setTmatZone(String tmatZone) {
		this.tmatZone = tmatZone;
	}
	@Transient
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	@Transient
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	
	
}

