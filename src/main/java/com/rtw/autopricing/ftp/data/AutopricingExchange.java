package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;

@Entity
@Table(name = "autopricing_exchange")
public class AutopricingExchange implements Serializable {

	private Integer id;
	private String name;
	private String status;
	private Double additionalMarkup;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "additional_markup")
	public Double getAdditionalMarkup() {
		return additionalMarkup;
	}

	public void setAdditionalMarkup(Double additionalMarkup) {
		this.additionalMarkup = additionalMarkup;
	}
	

}
