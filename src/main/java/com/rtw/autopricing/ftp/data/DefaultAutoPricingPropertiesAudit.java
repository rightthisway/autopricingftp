package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;

@Entity
@Table(name="default_autopricing_properties_audit_new")
public class DefaultAutoPricingPropertiesAudit implements Serializable {
	private Integer id;
	private Integer productId;
	private Integer parentCategoryId;
	private String exposure;
	private Integer shippingMethod;
	private Integer nearTermDisplayOption;
	private Double rptFactor;
	private Double priceBreakup;
	private Double upperMarkup;
	private Double lowerMarkup;
	private Double lowerShippingFees;
	private Double upperShippingFees;
	private Integer sectionCountTicket;
	private Integer shippingDays; 
	private Date lastUpdated;
	private String lastUpdatedBy;
	private String action;
	private Integer ticketNetworkBrokerId;
	private Integer vividBrokerId;
	private Integer scoreBigBrokerId;
	 private Integer zoneTicketBrokerId;
	 private Broker zoneTicketBroker;
	private Broker ticketNetworkBroker;
	private Broker vividBroker;
	private Broker scoreBigBroker;
	private Integer fanxchangeBrokerId;
	private Broker fanxchangeBroker;
	private Integer ticketcityBrokerId;
	 private Broker ticketcityBroker;
	 private Integer seatGeekBrokerId;
	private Broker seatGeekBroker;
	private Double taxPercentage;
	private Integer excludeHoursBeforeEvent;
	 
	private AutopricingProduct product;
	private ParentCategory parentCategory;
	private Boolean vividSeatEnabled;
	private Boolean rotEnabled;
	private Boolean rtwEnabled;
	private Boolean minicatsEnabled;
	private Boolean lastrowMinicatsEnabled;
	private Boolean vipMinicatsEnabled;
	private Boolean presaleAutocatsEnabled;
	private Boolean vipLastRowMinicatsEnabled;
	private Boolean tixcityEnabled;
	private Boolean zonedLastrowMinicatsEnabled;
	private Boolean larryLastEnabled;
	
	public  DefaultAutoPricingPropertiesAudit(DefaultAutoPricingProperties defaultAPProperties) {
		this.productId=defaultAPProperties.getProductId();
		this.parentCategoryId=defaultAPProperties.getParentCategoryId();
		this.exposure=defaultAPProperties.getExposure();
		this.shippingMethod=defaultAPProperties.getShippingMethod();
		this.nearTermDisplayOption=defaultAPProperties.getNearTermDisplayOption();
		this.rptFactor=defaultAPProperties.getRptFactor();
		this.priceBreakup=defaultAPProperties.getPriceBreakup();
		this.upperMarkup=defaultAPProperties.getUpperMarkup();
		this.lowerMarkup=defaultAPProperties.getLowerMarkup();
		this.lowerShippingFees=defaultAPProperties.getLowerShippingFees();
		this.upperShippingFees=defaultAPProperties.getUpperShippingFees();
		this.sectionCountTicket=defaultAPProperties.getSectionCountTicket();
		this.shippingDays=defaultAPProperties.getShippingDays();
		this.lastUpdated=defaultAPProperties.getLastUpdated();
		this.lastUpdatedBy=defaultAPProperties.getLastUpdatedBy();
		this.lastUpdatedBy=defaultAPProperties.getLastUpdatedBy();
		this.ticketNetworkBrokerId=defaultAPProperties.getTicketNetworkBrokerId();
		this.vividBrokerId=defaultAPProperties.getVividBrokerId();
		this.scoreBigBrokerId=defaultAPProperties.getScoreBigBrokerId();
		this.fanxchangeBrokerId=defaultAPProperties.getFanxchangeBrokerId();
		this.ticketcityBrokerId=defaultAPProperties.getTicketcityBrokerId();
		this.seatGeekBrokerId =defaultAPProperties.getSeatGeekBrokerId();
		this.zoneTicketBrokerId=defaultAPProperties.getZoneTicketBrokerId();
		this.vividSeatEnabled= defaultAPProperties.getVividSeatEnabled();
		this.rotEnabled= defaultAPProperties.getRotEnabled();
		this.rtwEnabled= defaultAPProperties.getRtwEnabled();
		this.minicatsEnabled= defaultAPProperties.getMinicatsEnabled();
		this.lastrowMinicatsEnabled= defaultAPProperties.getLastrowMinicatsEnabled();
		this.vipMinicatsEnabled= defaultAPProperties.getVipMinicatsEnabled();
		this.presaleAutocatsEnabled= defaultAPProperties.getPresaleAutocatsEnabled();
		this.taxPercentage = defaultAPProperties.getTaxPercentage();
		this.excludeHoursBeforeEvent=defaultAPProperties.getExcludeHoursBeforeEvent();
		this.vipLastRowMinicatsEnabled=defaultAPProperties.getVipLastRowMinicatsEnabled();
		this.tixcityEnabled = defaultAPProperties.getTixcityEnabled();
		this.zonedLastrowMinicatsEnabled=defaultAPProperties.getZonedLastrowMinicatsEnabled();
		this.larryLastEnabled=defaultAPProperties.getLarryLastEnabled();
	}
	
	public  DefaultAutoPricingPropertiesAudit() {
		
	}

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Column(name = "parent_category_id")
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	
	@Column(name = "exposure")
	public String getExposure() {
		return exposure;
	}
	public void setExposure(String exposure) {
		this.exposure = exposure;
	}
	
	@Column(name = "shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	@Column(name = "near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}
	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	@Column(name = "rpt_factor")
	public Double getRptFactor() {
		return rptFactor;
	}
	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}
	
	@Column(name = "price_breakup")
	public Double getPriceBreakup() {
		return priceBreakup;
	}
	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}
	
	@Column(name = "upper_markup")
	public Double getUpperMarkup() {
		return upperMarkup;
	}
	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}
	
	@Column(name = "lower_markup")
	public Double getLowerMarkup() {
		return lowerMarkup;
	}
	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}
	
	@Column(name = "upper_shipping_fees")
	public Double getUpperShippingFees() {
		return upperShippingFees;
	}
	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}
	
	@Column(name = "lower_shipping_fees")
	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}
	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}
	
	@Column(name = "section_ticket_count")
	public Integer getSectionCountTicket() {
		return sectionCountTicket;
	}
	public void setSectionCountTicket(Integer sectionCountTicket) {
		this.sectionCountTicket = sectionCountTicket;
	}
	
	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name = "last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name = "shipping_days")
	public Integer getShippingDays() {
		return shippingDays;
	}
	public void setShippingDays(Integer shippingDays) {
		this.shippingDays = shippingDays;
	}
	@Column(name = "action")
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	@Transient
	public Broker getTicketNetworkBroker() {
		if(ticketNetworkBroker==null){
			if(ticketNetworkBrokerId==null){
				return null;
			}
			ticketNetworkBroker = DAORegistry.getBrokerDAO().get(ticketNetworkBrokerId);
		}
		return ticketNetworkBroker;
	}
	
	public void setTicketNetworkBroker(Broker ticketNetworkBroker) {
		this.ticketNetworkBroker = ticketNetworkBroker;
	}
	
	@Column(name="tn_broker_id")
	public Integer getTicketNetworkBrokerId() {
		return ticketNetworkBrokerId;
	}
	public void setTicketNetworkBrokerId(Integer ticketNetworkBrokerId) {
		this.ticketNetworkBrokerId = ticketNetworkBrokerId;
	}
	

	@Transient
	public Broker getVividBroker() {
		if(vividBroker==null){
			if(vividBrokerId==null){
				return null;
			}
			vividBroker = DAORegistry.getBrokerDAO().get(vividBrokerId);
		}
		return vividBroker;
	}
	public void setVividBroker(Broker vividBroker) {
		this.vividBroker = vividBroker;
	}
	
	@Column(name="vivid_broker_id")
	public Integer getVividBrokerId() {
		return vividBrokerId;
	}
	public void setVividBrokerId(Integer vividBrokerId) {
		this.vividBrokerId = vividBrokerId;
	}
	
	@Transient	
	public Broker getScoreBigBroker() {
		if(scoreBigBroker==null){
			if(scoreBigBrokerId==null){
				return null;
			}
			scoreBigBroker = DAORegistry.getBrokerDAO().get(scoreBigBrokerId);
		}
		return scoreBigBroker;
	}
	public void setScoreBigBroker(Broker scoreBigBroker) {
		this.scoreBigBroker = scoreBigBroker;
	}
	
	@Column(name="scorebig_broker_id")
	public Integer getScoreBigBrokerId() {
		return scoreBigBrokerId;
	}
	public void setScoreBigBrokerId(Integer scoreBigBrokerId) {
		this.scoreBigBrokerId = scoreBigBrokerId;
	}
	@Column(name="fanxchange_broker_id")
	public Integer getFanxchangeBrokerId() {
		return fanxchangeBrokerId;
	}
	public void setFanxchangeBrokerId(Integer fanxchangeBrokerId) {
		this.fanxchangeBrokerId = fanxchangeBrokerId;
	}
	
	@Transient
	public Broker getFanxchangeBroker() {

		if(fanxchangeBroker==null){
			if(fanxchangeBrokerId==null){
				return null;
			}
			fanxchangeBroker = DAORegistry.getBrokerDAO().get(fanxchangeBrokerId);
		}
		return fanxchangeBroker;
	}
	public void setFanxchangeBroker(Broker fanxchangeBroker) {
		this.fanxchangeBroker = fanxchangeBroker;
	}

	@Transient
	public AutopricingProduct getProduct() {
		
		if(productId == null) {
			return null;
		}
		if(product == null) {
			product = DAORegistry.getAutopricingProductDAO().get(productId);
		}
		return product;
	}

	
	public void setProduct(AutopricingProduct product) {
		this.product = product;
	}
	
	@Transient
	public ParentCategory getParentCategory() {
		
		if(parentCategoryId == null) {
			return null;
		}
		if(parentCategory == null) {
			parentCategory = DAORegistry.getParentCategoryDAO().get(parentCategoryId);
		}
		return parentCategory;
	}
	
	
	public void setParentCategory(ParentCategory parentCategory) {
		this.parentCategory = parentCategory;
	}
	
	@Column(name="zoneticket_broker_id")
	public Integer getZoneTicketBrokerId() {
		return zoneTicketBrokerId;
	}
	public void setZoneTicketBrokerId(Integer zoneTicketBrokerId) {
		this.zoneTicketBrokerId = zoneTicketBrokerId;
	}
	
	@Transient
	public Broker getZoneTicketBroker() {

		if(zoneTicketBroker==null){
			if(zoneTicketBrokerId==null){
				return null;
			}
			zoneTicketBroker = DAORegistry.getBrokerDAO().get(zoneTicketBrokerId);
		}
		return zoneTicketBroker;
	
	}
	public void setZoneTicketBroker(Broker zoneTicketBroker) {
		this.zoneTicketBroker = zoneTicketBroker;
	}
	@Column(name="ticketcity_broker_id")
	public Integer getTicketcityBrokerId() {
		return ticketcityBrokerId;
	}
	public void setTicketcityBrokerId(Integer ticketcityBrokerId) {
		this.ticketcityBrokerId = ticketcityBrokerId;
	}
	
	@Transient
	public Broker getTicketcityBroker() {
		if(ticketcityBroker==null){
			if(ticketcityBrokerId==null){
				return null;
			}
			ticketcityBroker = DAORegistry.getBrokerDAO().get(ticketcityBrokerId);
		}
		return ticketcityBroker;
	}
	public void setTicketcityBroker(Broker ticketcityBroker) {
		this.ticketcityBroker = ticketcityBroker;
	}
	
	@Column(name="rot_enabled")
	public Boolean getRotEnabled() {
		return rotEnabled;
	}
	public void setRotEnabled(Boolean rotEnabled) {
		this.rotEnabled = rotEnabled;
	}
	
	@Column(name="rtw_enabled")
	public Boolean getRtwEnabled() {
		return rtwEnabled;
	}
	public void setRtwEnabled(Boolean rtwEnabled) {
		this.rtwEnabled = rtwEnabled;
	}

	@Column(name="minicats_enabled")
	public Boolean getMinicatsEnabled() {
		return minicatsEnabled;
	}
	public void setMinicatsEnabled(Boolean minicatsEnabled) {
		this.minicatsEnabled = minicatsEnabled;
	}
	
	@Column(name="lastrowminicats_enabled")
	public Boolean getLastrowMinicatsEnabled() {
		return lastrowMinicatsEnabled;
	}
	public void setLastrowMinicatsEnabled(Boolean lastrowMinicatsEnabled) {
		this.lastrowMinicatsEnabled = lastrowMinicatsEnabled;
	}
	
	@Column(name="vipminicats_enabled")
	public Boolean getVipMinicatsEnabled() {
		return vipMinicatsEnabled;
	}
	public void setVipMinicatsEnabled(Boolean vipMinicatsEnabled) {
		this.vipMinicatsEnabled = vipMinicatsEnabled;
	}
	@Column(name="presale_autocats_enabled")
	public Boolean getPresaleAutocatsEnabled() {
		return presaleAutocatsEnabled;
	}
	public void setPresaleAutocatsEnabled(Boolean presaleAutocatsEnabled) {
		this.presaleAutocatsEnabled = presaleAutocatsEnabled;
	}
	
	@Column(name="vivid_enabled")
	public Boolean getVividSeatEnabled() {
		return vividSeatEnabled;
	}
	public void setVividSeatEnabled(Boolean vividSeatEnabled) {
		this.vividSeatEnabled = vividSeatEnabled;
	}
	@Column(name="seatgeek_broker_id")
	public Integer getSeatGeekBrokerId() {
		return seatGeekBrokerId;
	}
	@Column(name="tax_percentage")
	public Double getTaxPercentage() {
		return taxPercentage;
	}
	public void setTaxPercentage(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}
	public void setSeatGeekBrokerId(Integer seatGeekBrokerId) {
		this.seatGeekBrokerId = seatGeekBrokerId;
	}
	@Column(name="exclude_hours_before_event")
	public Integer getExcludeHoursBeforeEvent() {
		return excludeHoursBeforeEvent;
	}
	public void setExcludeHoursBeforeEvent(Integer excludeHoursBeforeEvent) {
		this.excludeHoursBeforeEvent = excludeHoursBeforeEvent;
	}
	@Column(name="vip_lastrowminicats_enabled")
	public Boolean getVipLastRowMinicatsEnabled() {
		return vipLastRowMinicatsEnabled;
	}

	public void setVipLastRowMinicatsEnabled(Boolean vipLastRowMinicatsEnabled) {
		this.vipLastRowMinicatsEnabled = vipLastRowMinicatsEnabled;
	}
	@Column(name="tixcity_enabled")
	public Boolean getTixcityEnabled() {
		return tixcityEnabled;
	}
	public void setTixcityEnabled(Boolean tixcityEnabled) {
		this.tixcityEnabled = tixcityEnabled;
	}

	@Transient
	public Broker getSeatGeekBroker() {
		if(seatGeekBroker==null){
			if(seatGeekBrokerId==null){
				return null;
			}
			seatGeekBroker = DAORegistry.getBrokerDAO().get(seatGeekBrokerId);
		}
		return seatGeekBroker;
	}
	public void setSeatGeekBroker(Broker seatGeekBroker) {
		this.seatGeekBroker = seatGeekBroker;
	}
	@Column(name="zoned_lastrowminicats_enabled")
	public Boolean getZonedLastrowMinicatsEnabled() {
		return zonedLastrowMinicatsEnabled;
	}
	public void setZonedLastrowMinicatsEnabled(Boolean zonedLastrowMinicatsEnabled) {
		this.zonedLastrowMinicatsEnabled = zonedLastrowMinicatsEnabled;
	}
	
	@Column(name="larrylast_enabled")
	public Boolean getLarryLastEnabled() {
		return larryLastEnabled;
	}
	public void setLarryLastEnabled(Boolean larryLastEnabled) {
		this.larryLastEnabled = larryLastEnabled;
	}
}

