package com.rtw.autopricing.ftp.data;

import java.util.Date;

public class TicketListingCrawl {
	/*
	crawl id
	*/
	
	private Integer id;
	
	/**
	 * Date when the crawl ends.
	 */
	private Date endCrawl;
	
	/**
	 * Event Id.
	 */
	private Integer eventId;
	
	/**
	 * Site Id.
	 */
	private String siteId;
	
	/**
	 * Enabled.
	 */	
	private Boolean enabled;
	/**
	 * crawl frequency in second
	 * by default, set it to every hour.
	 */
	private Integer crawlFrequency;
	
	/**
	 * whether frequency is set automatically or not
	 */	
	private Boolean automaticCrawlFrequency;
	

	public Date getEndCrawl() {
		return endCrawl;
	}

	public void setEndCrawl(Date endCrawl) {
		this.endCrawl = endCrawl;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getCrawlFrequency() {
		return crawlFrequency;
	}

	public void setCrawlFrequency(Integer crawlFrequency) {
		this.crawlFrequency = crawlFrequency;
	}

	public Boolean getAutomaticCrawlFrequency() {
		return automaticCrawlFrequency;
	}

	public void setAutomaticCrawlFrequency(Boolean automaticCrawlFrequency) {
		this.automaticCrawlFrequency = automaticCrawlFrequency;
	}
	
	
}
