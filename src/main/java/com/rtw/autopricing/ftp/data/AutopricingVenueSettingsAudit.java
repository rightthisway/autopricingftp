package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;

@Entity
@Table(name="autopricing_venue_settings_audit")
public class AutopricingVenueSettingsAudit implements Serializable {
	 private Integer id;
	 private Integer productId;
	 private Integer venueId;
	 private Integer shippingMethod;
	 private Integer nearTermDisplayOption;
	 private Date lastUpdatedDate;
	 private String lastUpdatedBy;
	 private String action;
	 private Integer shippingDays;
	 private Double markup;
	 private Boolean existingEventUpdate;
	 
	 private AutopricingProduct product;
	 private Venue venue;
	 
	public AutopricingVenueSettingsAudit(){
		
	}
	 public AutopricingVenueSettingsAudit(AutopricingVenueSettings autopricingVenueSettings) {
			this.venueId=autopricingVenueSettings.getVenueId();
			this.productId=autopricingVenueSettings.getProductId();
			this.shippingMethod=autopricingVenueSettings.getShippingMethod();
			this.nearTermDisplayOption=autopricingVenueSettings.getNearTermDisplayOption();
			this.lastUpdatedBy=autopricingVenueSettings.getLastUpdatedBy();
			this.lastUpdatedDate=autopricingVenueSettings.getLastUpdatedDate();
			this.shippingDays=autopricingVenueSettings.getShippingDays();
			this.markup=autopricingVenueSettings.getMarkup();
			//this.status=autopricingVenueSettings.getStatus();
		}
	 
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Column(name = "shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	@Column(name = "near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}
	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	@Column(name = "last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name = "last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name = "venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name = "action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Column(name = "shipping_days")
	public Integer getShippingDays() {
		return shippingDays;
	}
	public void setShippingDays(Integer shippingDays) {
		this.shippingDays = shippingDays;
	}
	@Column(name = "markup")
	public Double getMarkup() {
		return markup;
	}
	public void setMarkup(Double markup) {
		this.markup = markup;
	}
	@Column(name = "existing_event_update")
	public Boolean getExistingEventUpdate() {
		return existingEventUpdate;
	}
	public void setExistingEventUpdate(Boolean existingEventUpdate) {
		this.existingEventUpdate = existingEventUpdate;
	}
	@Transient
	public AutopricingProduct getProduct() {
		
		if(productId == null) {
			return null;
		}
		if(product == null) {
			product = DAORegistry.getAutopricingProductDAO().get(productId);
		}
		return product;
	}
	
	public void setProduct(AutopricingProduct product) {
		this.product = product;
	}
	
	@Transient
	public Venue getVenue(){ 
	    if(venueId==null){
		    return null;
	    }
	    if(venue==null){
		    venue=DAORegistry.getVenueDAO().get(venueId);
	    }
	return venue;
	}
	@Transient
	public void setVenue(Venue venue) {
		this.venue = venue;
	}	
	
	
	
	
}

