package com.rtw.autopricing.ftp.data;


import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.OptimisticLockType;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.enums.TicketDeliveryType;
import com.rtw.autopricing.ftp.enums.TicketStatus;
import com.rtw.autopricing.ftp.enums.TicketType;
import com.rtw.autopricing.util.TicketUtil;

@MappedSuperclass
@Entity
@Table(name="ticket")
@org.hibernate.annotations.Entity(
	dynamicUpdate = true,optimisticLock=OptimisticLockType.NONE)
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@Transactional(isolation=Isolation.READ_UNCOMMITTED)
public class Ticket  implements Serializable {
	private Integer id;
	/* the identifier that uniquely defines a ticket on the ticket site */
	private String siteId;
	private String seller;
	private String itemId;

	private Integer eventId;
	
	private Integer quantity;
	private Integer remainingQuantity;
	private Integer soldQuantity;
	private Integer lotSize;
	
	private String section;
	private String normalizedSection;
	private String row;
	private String seat;

	private Double currentPrice;
	//private Double adjustedCurrentPrice;
	private Double buyItNowPrice;
	//private Double shortPrice;
	
	private Date insertionDate;
	private Date endDate;	
	private Date lastUpdate;
	
	private Integer ticketListingCrawlId;
	
	private Integer priceUpdateCount;
	
	private TicketStatus ticketStatus;
	
	private TicketDeliveryType ticketDeliveryType;
	private Event event;
	private boolean uncategorized = false;
	private TicketType ticketType;
	private boolean isSetAdmitOne;
	private Boolean inHand;
	private boolean isSetAdmitOneNonTM;
	private String priceHistory; 

	private Integer dupRefTicketId; // reference ticket which replaces me (if I'm a dupe) 
	private String eventName;
	private String venue;
	private Date eventDate;
	private Date eventTime;
	private Integer categoryId;
	private Integer categoryMappingId;
	private String filename;
	private Integer catId;
	private String venueCategoryGroup;
	private Double purchasePrice=0.0;
	private String priceByQuantity;
	private boolean isZoneCategoryticket = false;
	
	private Category category;
	private CategoryMapping categoryMapping;
	
	public Ticket() {
	}

	public Ticket(Ticket baseTicket) {
		setDupRefTicketId(null);
		//setAdjustedCurrentPrice(baseTicket.getAdjustedCurrentPrice());
		setBuyItNowPrice(baseTicket.getBuyItNowPrice());
		setCurrentPrice(baseTicket.getAdjustedCurrentPrice());
		setEndDate(baseTicket.getEndDate());
		setEventId(baseTicket.getEventId());
		setId(baseTicket.getId());
		setInsertionDate(baseTicket.getInsertionDate());
		setItemId(baseTicket.getItemId());
		setLastUpdate(baseTicket.getLastUpdate());
		setLotSize(baseTicket.getLotSize());
//		setNormalizedRow(baseTicket.getNormalizedRow());
		setNormalizedSection(baseTicket.getNormalizedSection());
//		setNormalizedSeat(baseTicket.getNormalizedSeat());
		setPriceUpdateCount(baseTicket.getPriceUpdateCount());
		setQuantity(baseTicket.getQuantity());
		setRemainingQuantity(baseTicket.getRemainingQuantity());
		setRow(baseTicket.getRow());
		setSection(baseTicket.getSection());
		setSeller(baseTicket.getSeller());
		//setShortPrice(baseTicket.getShortPrice());
		setSiteId(baseTicket.getSiteId());
		setSoldQuantity(baseTicket.getSoldQuantity());
		setSeat(baseTicket.getNormalizedSeat());
		setTicketDeliveryType(baseTicket.getTicketDeliveryType());
		setTicketListingCrawlId(baseTicket.getTicketListingCrawlId());
		setTicketStatus(baseTicket.getTicketStatus());
		setTicketType(baseTicket.getTicketType());
		setInHand(baseTicket.isInHand());
		setCategory(baseTicket.getCategory());
		setCategoryMapping(baseTicket.getCategoryMapping());
		//setTitle(baseTicket.getTitle());
	}

		
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = (id == null) ? null : (!id.equals(this.id)) ? id : this.id;
	}

	@Column(name="event_id")
	@Index(name="eventIdIndex")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = (eventId == null) ? null : (!eventId.equals(this.eventId)) ? eventId : this.eventId;
	}
	
	@Transient
	public Event getEvent() {
		if (event == null) {
			event = DAORegistry.getEventDAO().get(eventId);
		}
		return event; 
	}

	@Column(name="site_id")
	@Index(name="siteIdIndex")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = (siteId == null) ? null : (!siteId.equals(this.siteId)) ? siteId : this.siteId;
	}

	@Column(name="item_id")
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = (itemId == null) ? null : (!itemId.equals(this.itemId)) ? itemId : this.itemId;
	}

	@Column(name="section")
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = (section == null) ? null : (!section.equals(this.section)) ? section : this.section;
	}

	@Column(name="row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = (row == null) ? null : (!row.equals(this.row)) ? row : this.row;
	}

	@Column(name="current_price")
	public Double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = (currentPrice == null) ? null : (!(currentPrice == this.currentPrice)) ? currentPrice : this.currentPrice;
	}

	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = (quantity == null) ? null : (!quantity.equals(this.quantity)) ? quantity : this.quantity;
	}

	@Column(name="buy_it_now_price")
	public Double getBuyItNowPrice() {
		return buyItNowPrice;
	}

	public void setBuyItNowPrice(Double buyItNowPrice) {
		this.buyItNowPrice = (buyItNowPrice == null) ? null : (!buyItNowPrice.equals(this.buyItNowPrice)) ? buyItNowPrice : this.buyItNowPrice;
	}

	@Column(name="end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = (endDate == null) ? null : (!endDate.equals(this.endDate)) ? endDate : this.endDate;
	}

	@Column(name="last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = (lastUpdate == null) ? null : (!lastUpdate.equals(this.lastUpdate)) ? lastUpdate : this.lastUpdate;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="ticket_status")
	@Index(name="ticketStatusIndex")	
	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = (ticketStatus == null) ? null : (!ticketStatus.equals(this.ticketStatus)) ? ticketStatus : this.ticketStatus;
	}

	@Column(name="sold_quantity")
	public Integer getSoldQuantity() {
		return soldQuantity;
	}

	public void setSoldQuantity(Integer soldQuantity) {		
		this.soldQuantity = (soldQuantity == null) ? null : (!soldQuantity.equals(this.soldQuantity)) ? soldQuantity : this.soldQuantity;
	}
	
	@Column(name="seller")
	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = (seller == null) ? null : (!seller.equals(this.seller)) ? seller : this.seller;
	}
	
	@Column(name="seat")
	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = (seat == null) ? null : (!seat.equals(this.seat)) ? seat : this.seat;
	}

	@Column(name="crawl_id")
	@Index(name="crawlIdIndex")
	public Integer getTicketListingCrawlId() {
		return ticketListingCrawlId;
	}
	
		
	/*@Transient
	public Integer getCategoryId(VenueCategory venueCategory, String groupName,Map<Integer,Category> catMap,Map<Integer,List<CategoryMapping>> catMappingMap) {
		if(category != null && category.getGroupName().equals(groupName)){
			return category.getId();
		} else {
			category = Categorizer.computeCategory(venueCategory,this.getNormalizedSection(),this.getNormalizedRow(),this.getNormalizedSeat(),catMap,catMappingMap);
			if(category==null){
				uncategorized = true;				
			}
		}
		if(category == null){
			return null;
		}
		return category.getId();
	}*/
	
	/*public void setCategory(Category category) {
		this.category = category;
		if (category == null) {
			uncategorized = true;
		}
	}*/

	public void setTicketListingCrawlId(Integer ticketListingCrawlId) {
		this.ticketListingCrawlId = ticketListingCrawlId;
	}

	@Column(name="insertion_date")
	public Date getInsertionDate() {
		return insertionDate;
	}

	public void setInsertionDate(Date insertionDate) {
		this.insertionDate = (insertionDate == null) ? null : (!insertionDate.equals(this.insertionDate)) ? insertionDate : this.insertionDate;
	}

	@Column(name="lot_size")
	public Integer getLotSize() {
		return lotSize;
	}

	public void setLotSize(Integer lotSize) {
		this.lotSize = (lotSize == null) ? null : (!lotSize.equals(this.lotSize)) ? lotSize : this.lotSize;
	}

	@Column(name="price_update_count")
	public Integer getPriceUpdateCount() {
		return priceUpdateCount;
	}

	public void setPriceUpdateCount(Integer priceUpdateCount) {
		this.priceUpdateCount = (priceUpdateCount == null) ? null : (!priceUpdateCount.equals(this.priceUpdateCount)) ? priceUpdateCount : this.priceUpdateCount;
	}

	@Column(name="normalized_section")
	public String getNormalizedSection() {
		return normalizedSection;
	}

	public void setNormalizedSection(String normalizedSection) {
		this.normalizedSection = ((normalizedSection == null) ? null : (!normalizedSection.equals(this.normalizedSection)) ? normalizedSection : this.normalizedSection);
	}
	
	@Transient
	public String getNormalizedRow() {
		return row;
	}

	@Transient
	public String getNormalizedSeat() {
		return seat;
	}

	@Transient
	public Double getAdjustedCurrentPrice() {
		return TicketUtil.computeAdjustedPrice(this);
		//return 0.0;
	}

//	public void setAdjustedCurrentPrice(Double adjustedCurrentPrice) {
//		this.adjustedCurrentPrice = (adjustedCurrentPrice == null) ? null : (!adjustedCurrentPrice.equals(this.adjustedCurrentPrice)) ? adjustedCurrentPrice : this.adjustedCurrentPrice;
//	}

	@Column(name="remaining_quantity")
	@Index(name="remainingQuantityIndex")
	public Integer getRemainingQuantity() {
		return remainingQuantity;
	}

	public void setRemainingQuantity(Integer remainingQuantity) {
		this.remainingQuantity = (remainingQuantity == null) ? null : (!remainingQuantity.equals(this.remainingQuantity)) ? remainingQuantity : this.remainingQuantity;
	}

	@Transient
	public Long getNumSecondsBeforeEndDate() {
		if (endDate == null) {
			return null;
		}
		
		return endDate.getTime() - (new Date()).getTime();
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ticket_type")
	public TicketType getTicketType() {
		return ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = (ticketType == null) ? null : (!ticketType.equals(this.ticketType)) ? ticketType : this.ticketType;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ticket_delivery_type")
	public TicketDeliveryType getTicketDeliveryType() {
		return ticketDeliveryType;
	}

	public void setTicketDeliveryType(TicketDeliveryType ticketDeliveryType) {
		this.ticketDeliveryType = ticketDeliveryType;
	}

	/*@Transient
	public Integer getValuationFactor() {
		if(valuationFactor == null){
			valuationFactor = ValuationFactorUtil.getValuationFactor(eventId, getNormalizedSection(), getNormalizedRow());
		}
		return valuationFactor;
	}*/

	@Transient
	public Category getCategory(VenueCategory venueCategory, String groupName,Map<Integer,Category> catMap,Map<Integer,List<CategoryMapping>> catMappingMap) {
		if (uncategorized) {
			return null;
		}

		if(category == null || !category.getGroupName().equals(groupName)){
			this.getCategoryId(venueCategory,groupName,catMap,catMappingMap);
		}
		
		return category;
	}
		
	
	@Transient
	public String getTicketKey() {
		return siteId + "-" + itemId;
	}

	@Transient
	public boolean isAdmitOneTicket() {
		return isSetAdmitOne;
	}
	
	public void setAdmitOneTicket(boolean value) {
		this.isSetAdmitOne = value;
	}

	/*public String toString() {
		return "[Ticket: id=" + id
			+ " siteId=" + siteId
			+ " section=" + section
			+ " row=" + row
			+ " seat=" + seat
			+ " aprice=" + getAdjustedCurrentPrice()
			+ " price=" + getCurrentPrice()
			+ "]";
	}*/

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Ticket)) {
			return false;
		}
		Ticket other = (Ticket) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	@Transient
	public void setUncategorized(boolean uncategorized){
		this.uncategorized=uncategorized;
	}
	@Transient
	public boolean isAdmitOneNonTMTicket() {
		return isSetAdmitOneNonTM;
	}
	
	public void setAdmitOneNonTMTicket(boolean admitOneNonTMTicket) {
		this.isSetAdmitOneNonTM = admitOneNonTMTicket;
	}

	@Column(name="price_history")
	public String getPriceHistory() {
		return priceHistory;
	}

	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}

	@Column(name="in_hand")
	public Boolean isInHand() {
		if(inHand==null){
			return false;
		}
		return inHand;
	}
	
	public void setInHand(Boolean inHand) {
		if(inHand== null){
			inHand= false;
		}
		this.inHand = inHand;
	}

	
	public Ticket(String siteId, String itemId) {
		setSiteId(siteId);
		setItemId(itemId);
	}
	@Transient
	public String toString() {
		return super.toString();
	}

	@Transient
	public Integer getDupRefTicketId() {
		return dupRefTicketId;
	}
	@Transient
	public void setDupRefTicketId(Integer dupRefTicketId) {
		this.dupRefTicketId = dupRefTicketId;
	}
	
	@Transient
	public String getEventName() {
		return eventName;
	}
	@Transient
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Transient
	public String getVenue() {
		return venue;
	}
	@Transient
	public void setVenue(String venue) {
		this.venue = venue;
	}

	@Transient
	public Date getEventDate() {
		return eventDate;
	}
	@Transient
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	@Transient
	public Date getEventTime() {
		return eventTime;
	}
	@Transient
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	@Transient
	public Integer getCategoryId(VenueCategory venueCategory,String groupName,Map<Integer,Category>catMap,Map<Integer,List<CategoryMapping>> catMappingMap) {
		/*if(category != null && category.getGroupName().equals(groupName)){
			return category.getId();
		} else {
			category = Categorizer.computeCategory(venueCategory,this.getNormalizedSection(),this.getRow(),this.getSeat(),catMap,catMappingMap);
			if(category == null) {
				uncategorized = true;				
			}
		}
		if(category == null){
			return null;
		}*/
		return category.getId();
	}
	
	
	@Transient
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	@Transient
	public Integer getCatId() {
		return catId ;
	}
	
	@Transient
	public void setCatId(Integer catId) {
		this.catId = catId;
	}
	
	@Transient
	public Integer getCategoryMappingId() {
		return categoryMappingId;
	}
	@Transient
	public void setCategoryMappingId(Integer categoryMappingId) {
		this.categoryMappingId = categoryMappingId;
	}
	
	
	@Transient
	public String getFilename() {
		return filename;
	}
	@Transient
	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Transient
	public Double getPurchasePrice() {		
		return purchasePrice;
	}
	@Transient
	public void setPurchasePrice(Double purchasePrice) {		
		this.purchasePrice = purchasePrice;
	}
	
	@Transient
	public Category getCategory() {
		return category;
	}
	@Transient
	public void setCategory(Category category) {
		this.category = category;
		if (category == null) {
			uncategorized = true;
		}
	}
	
	@Transient
	public String getVenueCategoryGroup() {
		return venueCategoryGroup;
	}
	@Transient
	public void setVenueCategoryGroup(String venueCategoryGroup) {
		this.venueCategoryGroup = venueCategoryGroup;
	}

	@Transient
	public String getPriceByQuantity() {
		return priceByQuantity;
	}
	@Transient
	public void setPriceByQuantity(String priceByQuantity) {
		this.priceByQuantity = priceByQuantity;
	}

	@Transient
	public CategoryMapping getCategoryMapping() {
		return categoryMapping;
	}
	@Transient
	public void setCategoryMapping(CategoryMapping categoryMapping) {
		this.categoryMapping = categoryMapping;
	}

	@Transient
	public boolean isZoneCategoryticket() {
		return isZoneCategoryticket;
	}

	public void setZoneCategoryticket(boolean isZoneCategoryticket) {
		this.isZoneCategoryticket = isZoneCategoryticket;
	}
	
/*	public void setCategory(Category category) {
		this.category = category;
		if (category == null) {
			uncategorized = true;
		}
	}
	*/
	
}
