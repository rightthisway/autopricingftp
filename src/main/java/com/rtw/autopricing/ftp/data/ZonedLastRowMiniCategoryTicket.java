package com.rtw.autopricing.ftp.data;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.enums.TicketDeliveryType;
import com.rtw.autopricing.util.CategoryTicket;

@Entity
@Table(name="zone_lastrow_minicat_category_ticket")
public class ZonedLastRowMiniCategoryTicket implements Serializable,CategoryTicket {

	private Integer id;
	private String section;
	private Integer quantity;
	private Integer eventId;
	private Integer posExchangeEventId;
//	private Integer posEventId;
	private Integer tnTicketGroupId;
	private Integer posPurchaseOrderId;
	private Category category;
	private Integer categoryId;
	private Integer ticketId;
	private String itemId;

	
//	private String browseRow;
	
	private String status;
//	private int tmatQty;
	private Double purPrice;
	
	
	private Integer baseTicketOne;
	private Integer baseTicketTwo;
	private Integer baseTicketThree;
	private String priceHistory;
	private String ticketIdHistory;
	private String baseTicketOneHistory;
	private String baseTicketTwoHistory;
	private String baseTicketThreeHistory;
	
	private Double baseTicketOnePurPrice;
	private Double baseTicketTwoPurPrice;
	private Double baseTicketThreePurPrice;
	
	private String lastRow;
	private Double actualPrice;
	private Double tnPrice;
	private Double vividPrice;
	private Double scoreBigPrice;
	private Double fanxchangePrice;
	private Double ticketcityPrice;
	private Double tickpickPrice;
	private Date createdDate;
	private Date lastUpdated;
	private String reason;
	private String tmatZone;
	private Integer nearTermOptionId;
	private Integer shippingMethodSpecialId;
	private Date expectedArrivalDate;
	private Boolean isEdited = false;;
	private Boolean isPosUpdate;
	private Boolean isTnUpdate=false;
	private Integer tnBrokerId;
	
	//for Ticket details page
	private String popPrice;
	private String popDate;
	
	/*public ZonedLastRowMiniCategoryTicket(Ticket ticket) {
		this.section = ticket.getNormalizedSection().toUpperCase();
		this.eventId = ticket.getEventId();
		this.categoryId = ticket.getCategoryMapping().getCategoryId();
		this.ticketId = ticket.getId();
		this.itemId = ticket.getItemId();
		//this.lastRow = ticket.getCategoryMapping().getLastRow().toUpperCase();
		this.purPrice = ticket.getPurchasePrice();
		this.status ="ACTIVE";
		
	}*/
	
	public ZonedLastRowMiniCategoryTicket(CategoryTicket catTicket) {
		this.section = catTicket.getSection();
		this.quantity = catTicket.getQuantity();
		this.eventId = catTicket.getEventId();
		this.posExchangeEventId = catTicket.getPosExchangeEventId();
		this.categoryId = catTicket.getCategoryId();
		this.ticketId = catTicket.getTicketId();
		this.itemId = catTicket.getItemId();
		this.lastRow = catTicket.getLastRow();
		this.baseTicketOne = catTicket.getBaseTicketOne();
		this.baseTicketTwo = catTicket.getBaseTicketTwo();
		this.baseTicketThree = catTicket.getBaseTicketThree();
		this.actualPrice = catTicket.getActualPrice();
		this.tnPrice = catTicket.getTnPrice();
		this.vividPrice = catTicket.getVividPrice();
		this.scoreBigPrice = catTicket.getScoreBigPrice();
		this.fanxchangePrice = catTicket.getFanxchangePrice();
		this.ticketcityPrice = catTicket.getTicketcityPrice();
		this.tickpickPrice = catTicket.getTickpickPrice();
		this.nearTermOptionId = catTicket.getNearTermOptionId();
		this.shippingMethodSpecialId = catTicket.getShippingMethodSpecialId();
		this.expectedArrivalDate = catTicket.getExpectedArrivalDate();
		this.status = "ACTIVE";
		this.baseTicketOnePurPrice = catTicket.getBaseTicketOnePurPrice();
		this.baseTicketTwoPurPrice = catTicket.getBaseTicketTwoPurPrice();
		this.baseTicketThreePurPrice = catTicket.getBaseTicketThreePurPrice();
		this.purPrice = catTicket.getPurPrice();
		this.tmatZone = catTicket.getTmatZone();
		
	}
	
	
	public ZonedLastRowMiniCategoryTicket() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name = "quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	/*public Integer getPosExchangeEventId() {
		return posExchangeEventId;
	}
	public void setPosExchangeEventId(Integer posExchangeEventId) {
		this.posExchangeEventId = posExchangeEventId;
	}
	public Integer getPosEventId() {
		return posEventId;
	}
	public void setPosEventId(Integer posEventId) {
		this.posEventId = posEventId;
	}
	public Integer getPosTicketGroupId() {
		return posTicketGroupId;
	}
	public void setPosTicketGroupId(Integer posTicketGroupId) {
		this.posTicketGroupId = posTicketGroupId;
	}
	*/
	
	@Column(name = "pos_purchase_order_id")
	public Integer getPosPurchaseOrderId() {
		return posPurchaseOrderId;
	}
	public void setPosPurchaseOrderId(Integer posPurchaseOrderId) {
		this.posPurchaseOrderId = posPurchaseOrderId;
	}
	
	@Transient
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	@Column(name = "category_id")
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	@Column(name = "ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name = "item_id")
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "tn_price")
	public Double getTnPrice() {
		return tnPrice;
	}
	public void setTnPrice(Double tnPrice) {
		this.tnPrice = tnPrice;
	}
	
	@Column(name = "base_ticket1")
	public Integer getBaseTicketOne() {
		return baseTicketOne;
	}
	public void setBaseTicketOne(Integer baseTicketOne) {
		this.baseTicketOne = baseTicketOne;
	}
	
	@Column(name = "base_ticket2")
	public Integer getBaseTicketTwo() {
		return baseTicketTwo;
	}
	public void setBaseTicketTwo(Integer baseTicketTwo) {
		this.baseTicketTwo = baseTicketTwo;
	}
	
	@Column(name = "price_history")
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	
	@Column(name = "last_row")
	public String getLastRow() {
		return lastRow;
	}
	public void setLastRow(String lastRow) {
		this.lastRow = lastRow;
	}
	
	@Column(name = "actual_price")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name = "near_term_display_option_id")
	public Integer getNearTermOptionId() {
		return nearTermOptionId;
	}

	public void setNearTermOptionId(Integer nearTermOptionId) {
		this.nearTermOptionId = nearTermOptionId;
	}

	@Column(name = "shipping_method_id")
	public Integer getShippingMethodSpecialId() {
		return shippingMethodSpecialId;
	}

	public void setShippingMethodSpecialId(Integer shippingMethodSpecialId) {
		this.shippingMethodSpecialId = shippingMethodSpecialId;
	}

	@Column(name = "tn_exchange_event_id")
	public Integer getPosExchangeEventId() {
		return posExchangeEventId;
	}


	public void setPosExchangeEventId(Integer posExchangeEventId) {
		this.posExchangeEventId = posExchangeEventId;
	}

	@Column(name = "tn_ticket_group_id")
	public Integer getTnTicketGroupId() {
		return tnTicketGroupId;
	}


	public void setTnTicketGroupId(Integer tnTicketGroupId) {
		this.tnTicketGroupId = tnTicketGroupId;
	}

	@Column(name = "vivid_price")
	public Double getVividPrice() {
		return vividPrice;
	}


	public void setVividPrice(Double vividPrice) {
		this.vividPrice = vividPrice;
	}

	@Column(name = "scorebig_price")
	public Double getScoreBigPrice() {
		return scoreBigPrice;
	}


	public void setScoreBigPrice(Double scoreBigPrice) {
		this.scoreBigPrice = scoreBigPrice;
	}
	@Column(name="fanxchange_price")
	public Double getFanxchangePrice() {
		return fanxchangePrice;
	}

	public void setFanxchangePrice(Double fanxchangePrice) {
		this.fanxchangePrice = fanxchangePrice;
	}
	@Column(name="ticketcity_price")
	public Double getTicketcityPrice() {
		return ticketcityPrice;
	}

	public void setTicketcityPrice(Double ticketcityPrice) {
		this.ticketcityPrice = ticketcityPrice;
	}

	@Column(name = "tickpick_price")
	public Double getTickpickPrice() {
		return tickpickPrice;
	}


	public void setTickpickPrice(Double tickpickPrice) {
		this.tickpickPrice = tickpickPrice;
	}

	
	@Transient
	public Double getPurPrice() {
		return purPrice;
	}


	public void setPurPrice(Double purPrice) {
		this.purPrice = purPrice;
	}

	@Column(name = "expected_arrival_date")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Transient
	public Boolean getIsEdited() {
		return isEdited;
	}


	public void setIsEdited(Boolean isEdited) {
		this.isEdited = isEdited;
	}


	
	@Transient
	public Boolean getIsPosUpdate() {
		return isPosUpdate;
	}

	public void setIsPosUpdate(Boolean isPosUpdate) {
		this.isPosUpdate = isPosUpdate;
	}

	@Column(name = "base_ticket3")
	public Integer getBaseTicketThree() {
		return baseTicketThree;
	}

	public void setBaseTicketThree(Integer baseTicketThree) {
		this.baseTicketThree = baseTicketThree;
	}

	@Column(name = "ticketid_history")
	public String getTicketIdHistory() {
		return ticketIdHistory;
	}

	public void setTicketIdHistory(String ticketIdHistory) {
		this.ticketIdHistory = ticketIdHistory;
	}

	@Column(name = "baseticketone_history")
	public String getBaseTicketOneHistory() {
		return baseTicketOneHistory;
	}

	public void setBaseTicketOneHistory(String baseTicketOneHistory) {
		this.baseTicketOneHistory = baseTicketOneHistory;
	}

	@Column(name = "basetickettwo_history")
	public String getBaseTicketTwoHistory() {
		return baseTicketTwoHistory;
	}

	public void setBaseTicketTwoHistory(String baseTicketTwoHistory) {
		this.baseTicketTwoHistory = baseTicketTwoHistory;
	}

	@Column(name = "baseticketthree_history")
	public String getBaseTicketThreeHistory() {
		return baseTicketThreeHistory;
	}

	public void setBaseTicketThreeHistory(String baseTicketThreeHistory) {
		this.baseTicketThreeHistory = baseTicketThreeHistory;
	}

	@Transient
	public Double getBaseTicketOnePurPrice() {
		return baseTicketOnePurPrice;
	}

	public void setBaseTicketOnePurPrice(Double baseTicketOnePurPrice) {
		this.baseTicketOnePurPrice = baseTicketOnePurPrice;
	}

	@Transient
	public Double getBaseTicketTwoPurPrice() {
		return baseTicketTwoPurPrice;
	}

	public void setBaseTicketTwoPurPrice(Double baseTicketTwoPurPrice) {
		this.baseTicketTwoPurPrice = baseTicketTwoPurPrice;
	}

	@Transient
	public Double getBaseTicketThreePurPrice() {
		return baseTicketThreePurPrice;
	}

	public void setBaseTicketThreePurPrice(Double baseTicketThreePurPrice) {
		this.baseTicketThreePurPrice = baseTicketThreePurPrice;
	}

	public void createCategoryTicket(Ticket ticket) {
		this.section = ticket.getNormalizedSection().toUpperCase();
		this.eventId = ticket.getEventId();
		this.categoryId = ticket.getCategoryMapping().getCategoryId();
		this.ticketId = ticket.getId();
		this.itemId = ticket.getItemId();
		this.lastRow = ticket.getCategoryMapping().getLastRow().toUpperCase();
		
		//Tamil 10/11/2017 : for all events minus $1 dollar from cheapest TMAT ticket price
		this.purPrice = ticket.getPurchasePrice() - 2;	
		
		this.status = "ACTIVE";
		this.tmatZone = ticket.getCategory().getSymbol();
		
		
		// this is pos shipping method special id
		// 1. for E-Ticket
		// 0. for Default Website Setting
		//System.out.println("TMAT ticket delivery type "+ ticket.getTicketDeliveryType());
		if(ticket.getTicketDeliveryType() == null || ticket.getTicketDeliveryType().equals(TicketDeliveryType.MERCURY)) {
			this.shippingMethodSpecialId = 0;
		} else {
			this.shippingMethodSpecialId = 1;
		}

	}

	@Column(name="tn_broker_id")
	public Integer getTnBrokerId() {
		return tnBrokerId;
	}

	public void setTnBrokerId(Integer tnBrokerId) {
		this.tnBrokerId = tnBrokerId;
	}
	
	@Column(name="reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	@Column(name="tmat_zone")
	public String getTmatZone() {
		return tmatZone;
	}

	public void setTmatZone(String tmatZone) {
		this.tmatZone = tmatZone;
	}

	@Transient
	public String getRowRange() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setRowRange(String rowRange) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getPosEventId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPosEventId(Integer posEventId) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getPosVenueId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPosVenueCategoryId(Integer posVenueCategoryId) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getPosVenueCategoryId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPosVenueId(Integer posVenueId) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getTnCategoryTicketGroupId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTnCategoryTicketGroupId(Integer tnCategoryTicketGroupId) {
		// TODO Auto-generated method stub
		
	}
	
	@Transient
	public String getPopPrice() {
		return popPrice;
	}
	public void setPopPrice(String popPrice) {
		this.popPrice = popPrice;
	}
	@Transient
	public String getPopDate() {
		return popDate;
	}
	public void setPopDate(String popDate) {
		this.popDate = popDate;
	}
	
	@Transient
	public Ticket getTicket() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTicket(Ticket ticket) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Double getZoneTicketPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setZoneTicketPrice(Double zoneTicketPrice) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getSectionRange() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setSectionRange(String sectionRange) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPriority(Integer priority) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getFloorCap() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setFloorCap(Double floorCap) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getVenueConfigurationZoneId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setVenueConfigurationZoneId(Integer venueConfigurationZoneId) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getTnExchangeEventId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTnExchangeEventId(Integer tnExchangeEventId) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public String getTicketDeliveryType() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTicketDeliveryType(String ticketDeliveryType) {
		// TODO Auto-generated method stub
		
	}
	
	@Transient
	public String getCatRow() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCatRow(String catRow) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getInternalNotes() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setInternalNotes(String internalNotes) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getCatsPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCatsPriority(Integer catsPriority) {
		// TODO Auto-generated method stub
		
	}
	
	@Transient
	public Integer getProductPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setProductPriority(Integer productPriority) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getQtyPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setQtyPriority(Integer qtyPriority) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getTmatCategory() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTmatCategory(String tmatCategory) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getNormalizedSection() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setNormalizedSection(String normalizedSection) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public String getAlternateRow() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setAlternateRow(String alternateRow) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getZoneTicketsTicketGroupId() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setZoneTicketsTicketGroupId(Integer zoneTicketsTicketGroupId) {
		// TODO Auto-generated method stub
		
	}

	@Transient
	public Integer getZoneTicketsEventId() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setZoneTicketsEventId(Integer zoneTicketsEventId) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getIsTnUpdate() {
		return isTnUpdate;
	}

	public void setIsTnUpdate(Boolean isTnUpdate) {
		this.isTnUpdate = isTnUpdate;
	}
	@Transient
	public Boolean getIsPresale() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setIsPresale(Boolean isPresale) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getTaxAmount() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTaxAmount(Double taxAmount) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public String getTaxAmountHistory() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxAmountHistory(String taxAmountHistory) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getTaxPercentage() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxPercentage(Double taxPercentage) {
		// TODO Auto-generated method stub
	}
	@Transient
	public String getTaxPercentageHistory() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxPercentageHistory(String taxPercentageHistory) {
		// TODO Auto-generated method stub
	}
	@Transient
	public String getTmatSection() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTmatSection(String tmatSection) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getTmatPrice() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTmatPrice(Double tmatPrice) {
		// TODO Auto-generated method stub
	}
}
