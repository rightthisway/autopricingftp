package com.rtw.autopricing.ftp.data;



import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.enums.TourType;



@Entity
@Table(name="tour_category")
public class TourCategory implements Serializable{

	private Integer id;
	private TourType tourType;
	private String name;
	

	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Transient
	public TourType getTourType() {
		if(tourType!=null){
			return tourType;
		}else{
			if(name == null || name.equalsIgnoreCase("other")){
				tourType = TourType.OTHER;
			}else if(name.equalsIgnoreCase("sports")){
				tourType = TourType.SPORT;
			}else if(name == null || name.equalsIgnoreCase("concerts")){
				tourType = TourType.CONCERT;
			}else if(name == null || name.equalsIgnoreCase("theater")){
				tourType = TourType.THEATER;
			}
		}
		return tourType;
	}
	public void setTourType(TourType tourType) {
		this.tourType = tourType;
	}
	
}

