package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="category_new")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Category implements Serializable {
	public static final Integer UNCATEGORIZED_ID = -3;
	public static final Integer CATEGORIZED_ID = -2;
	public static final Integer ALL_ID = -1;
	
	private Integer id;
	private String symbol;
	private Integer venueCategoryId;
	private String description;
	private String equalCats;
	private String groupName;
	private String normalizedZone;
	private Integer seatQuantity;
	private Integer categoryCapacity;
	//private String larrySection;
	
	public Category(Integer venueCategoryId, String symbol, String description, String equalCats, String groupName) {
		this.venueCategoryId = venueCategoryId;
		this.symbol = symbol;
		this.description = description;
		this.equalCats = equalCats;
		this.groupName = groupName;
	}
	
	public Category() {}

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="symbol")
	public String getSymbol() {
		return symbol;
	}
	
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	

	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="equal_cats")
	public String getEqualCats() {
		return equalCats;
	}

	public void setEqualCats(String equalCats) {
		this.equalCats = equalCats;
	}
	
	@Column(name="group_name")
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Column(name="seat_quantity")
	public Integer getSeatQuantity() {
		return seatQuantity;
	}

	public void setSeatQuantity(Integer seatQuantity) {
		this.seatQuantity = seatQuantity;
	}

	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}

	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	
	@Column(name="normalized_zone")
	public String getNormalizedZone() {
		return normalizedZone;
	}

	public void setNormalizedZone(String normalizedZone) {
		this.normalizedZone = normalizedZone;
	}
	
	@Transient
	public boolean hasDefinedEquivalency() {
		if(equalCats != null) {
			if (equalCats.equals("*")) {
				return false;
			}
		}
		return true;
	}

	@Column(name="category_capacity")
	public Integer getCategoryCapacity() {
		return categoryCapacity;
	}

	public void setCategoryCapacity(Integer categoryCapacity) {
		this.categoryCapacity = categoryCapacity;
	}

	/*@Column(name = "larry_section")
	public String getLarrySection() {
		return larrySection;
	}

	public void setLarrySection(String larrySection) {
		this.larrySection = larrySection;
	}
*/

}
