package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Generated;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.util.BrokerUtils;

@Entity
@Table(name = "broker")
public class Broker implements Serializable,com.rtw.autopricing.ftp.indux.data.Broker {

	private Integer id;
	private String name;
	private String emailId;
	private String phoneNo;
	private String status;
	private Boolean isAutopricingBroker;
	private String emailSuffix;
	private Integer buyerBrokerId = 1;
	private Integer clientBrokerEmployeeId = 1;
	private Integer systemUserId = 6;
	private Integer clientBrokerId = 1;
	
	private Integer posBrokerId ;
	
	private Boolean isTNUpdate = null;
//	private Integer minutePOSAddLimit = 100;
	private Integer minutePOSUpdateLimit;// = 100;
	private Integer minuteUpdateCounter = 0;
	private Date minuteUpdateTimeStamp;
	
	private Integer ssAccountOfficeId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "email_id")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Column(name = "phone_no")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "is_autopricing_broker")
	public Boolean getisAutopricingBroker() {
		return isAutopricingBroker;
	}
	public void setisAutopricingBroker(Boolean isAutopricingBroker) {
		this.isAutopricingBroker = isAutopricingBroker;
	}

	@Column(name = "email_suffix")
	public String getEmailSuffix() {
		return emailSuffix;
	}

	public void setEmailSuffix(String emailSuffix) {
		this.emailSuffix = emailSuffix;
	}

	@Column(name = "buyer_broker_id")
	public Integer getBuyerBrokerId() {
		return buyerBrokerId;
	}
	public void setBuyerBrokerId(Integer buyerBrokerId) {
		this.buyerBrokerId = buyerBrokerId;
	}
	
	@Column(name = "client_broker_employee_id")
	public Integer getClientBrokerEmployeeId() {
		return clientBrokerEmployeeId;
	}
	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId) {
		this.clientBrokerEmployeeId = clientBrokerEmployeeId;
	}
	
	@Column(name = "system_user_id")
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	
	@Column(name = "client_broker_id")
	public Integer getClientBrokerId() {
		return clientBrokerId;
	}
	public void setClientBrokerId(Integer clientBrokerId) {
		this.clientBrokerId = clientBrokerId;
	}
	
	@Column(name = "pos_broker_id")
	public Integer getPosBrokerId() {
		return posBrokerId;
	}
	public void setPosBrokerId(Integer posBrokerId) {
		this.posBrokerId = posBrokerId;
	}
	
	/*public Integer getMinutePOSAddLimit() {
		return minutePOSAddLimit;
	}
	public void setMinutePOSAddLimit(Integer minutePOSAddLimit) {
		this.minutePOSAddLimit = minutePOSAddLimit;
	}*/
	@Column(name = "pos_update_per_minute_limit")
	public Integer getMinutePOSUpdateLimit() {
		return minutePOSUpdateLimit;
	}
	public void setMinutePOSUpdateLimit(Integer minutePOSUpdateLimit) {
		this.minutePOSUpdateLimit = minutePOSUpdateLimit;
	}
	
	@Column(name = "ss_account_office_id")
	public Integer getSsAccountOfficeId() {
		return ssAccountOfficeId;
	}

	public void setSsAccountOfficeId(Integer ssAccountOfficeId) {
		this.ssAccountOfficeId = ssAccountOfficeId;
	}
	
	
	/*public Integer getMinutePOSDeleteLimit() {
		return minutePOSDeleteLimit;
	}
	public void setMinutePOSDeleteLimit(Integer minutePOSDeleteLimit) {
		this.minutePOSDeleteLimit = minutePOSDeleteLimit;
	}*/

	@Transient
	public Integer getMinuteUpdateCounter() {
		if(minuteUpdateCounter==null){
			minuteUpdateCounter = 0;
		}
		return minuteUpdateCounter;
	}
	public void setMinuteUpdateCounter(Integer minuteUpdateCounter) {
		this.minuteUpdateCounter = minuteUpdateCounter; 
	}

	public void addMinuteUpdateCounter() {
		this.minuteUpdateCounter ++; 
	}

	@Transient
	public Date getMinuteUpdateTimeStamp() {
		if(minuteUpdateTimeStamp==null){
			minuteUpdateTimeStamp= new Date();
		}
		return minuteUpdateTimeStamp;
	}

	public void setMinuteUpdateTimeStamp(Date minuteUpdateTimeStamp) {
		this.minuteUpdateTimeStamp = minuteUpdateTimeStamp;
	}

	
	
	
}
