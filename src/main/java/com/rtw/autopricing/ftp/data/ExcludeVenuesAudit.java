package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Venue;

@Entity
@Table(name="exclude_venues_product_audit")
public class ExcludeVenuesAudit implements Serializable{

	private Integer id;
	private Integer venueId;
	private Integer productId;
	private Integer brokerId;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String action;
	private Venue venue;
	private AutopricingProduct product;
	private Broker broker;
	
	
	public ExcludeVenuesAudit(){
		
	}
	public ExcludeVenuesAudit(ExcludeVenues excludeVenue) {
		this.venueId=excludeVenue.getVenueId();
		this.productId=excludeVenue.getProductId();
		this.brokerId=excludeVenue.getBrokerId();
		this.lastUpdatedBy=excludeVenue.getLastUpdatedBy();
		this.lastUpdatedDate=excludeVenue.getLastUpdatedDate();
				// TODO Auto-generated constructor stub
	}
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	@Transient
	public Broker getBroker() {
		
		if(brokerId == null) {
			return null;
		}
		if(broker == null) {
			broker = DAORegistry.getBrokerDAO().get(brokerId);
		}
		return broker;
	}

	@Transient
	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	@Transient
	public AutopricingProduct getProduct() {
		
		if(productId == null) {
			return null;
		}
		if(product == null) {
			product = DAORegistry.getAutopricingProductDAO().get(productId);
		}
		return product;
	}

	@Transient
	public void setProduct(AutopricingProduct product) {
		this.product = product;
	}
	@Transient
	public Venue getVenue(){ 
	    if(venueId==null){
		    return null;
	    }
	    if(venue==null){
		    venue=DAORegistry.getVenueDAO().get(venueId);
	    }
	return venue;
	}
	@Transient
	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	@Column(name = "last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	@Column(name = "last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	@Column(name = "action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	
	
	
	
}
