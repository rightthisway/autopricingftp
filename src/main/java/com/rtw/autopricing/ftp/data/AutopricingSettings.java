package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Generated;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;

@Entity
@Table(name = "autopricing_global_settings")
public class AutopricingSettings implements Serializable {

	private Integer id;
	private Integer brokerId;
	private Integer productId;
	private Integer exchangeId;
	private Integer excludeEventDays;
	private Boolean isEnabled;
	private String lastUpdatedBy;
	private Date lastUpdated;
	
	private Broker broker;
	private AutopricingProduct product;
	private AutopricingExchange exchange;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Column(name = "exchange_id")
	public Integer getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(Integer exchangeId) {
		this.exchangeId = exchangeId;
	}

	@Column(name = "is_enabled")
	public Boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	@Column(name = "last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name = "exclude_event_days")
	public Integer getExcludeEventDays() {
		return excludeEventDays;
	}

	public void setExcludeEventDays(Integer excludeEventDays) {
		this.excludeEventDays = excludeEventDays;
	}

	@Transient
	public Broker getBroker() {
		
		if(brokerId == null) {
			return null;
		}
		if(broker == null) {
			broker = DAORegistry.getBrokerDAO().get(brokerId);
		}
		return broker;
	}

	@Transient
	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	@Transient
	public AutopricingProduct getProduct() {
		
		if(productId == null) {
			return null;
		}
		if(product == null) {
			product = DAORegistry.getAutopricingProductDAO().get(productId);
		}
		return product;
	}

	@Transient
	public void setProduct(AutopricingProduct product) {
		this.product = product;
	}

	@Transient
	public AutopricingExchange getExchange() {
		if(exchangeId == null) {
			return null;
		}
		if(exchange == null) {
			exchange = DAORegistry.getAutopricingExchangeDAO().get(exchangeId);
		}
		return exchange;
	}

	@Transient
	public void setExchange(AutopricingExchange Exchange) {
		this.exchange = Exchange;
	}

	

}
