package com.rtw.autopricing.ftp.data;

import java.io.File;
import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.enums.CreationType;
import com.rtw.autopricing.ftp.enums.EventStatus;
import com.rtw.autopricing.ftp.enums.TourType;


@Entity
@Table(name="event")
public class Event implements Serializable{
	private Venue venue;
//	private Collection<Category> category;
	private String zoneCategoryGroupName=null;
	private String formatedDate;
	private String formatedAdmitoneId;
	private String catGroupName;
//	private String secondaryCategoryGroup;
	private Collection<String> enabledCrawlSites;
	private Collection<String> disabledCrawlSites;
	private Boolean autoCorrect;
	private Boolean ignoreTBDTime;
	private VenueCategory venueCategory;
	
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	protected static DateFormat timeFormat = new SimpleDateFormat("HH:mm zzz");
	
	protected static final long serialVersionUID = -7402732543016575381L;
	protected Integer id;
//	protected Integer tourId;
	private Date localDate;
	private Time localTime;
	private Integer venueId;
	private Integer venueCategoryId;
	private String timeZoneId;
	private String name;
	private boolean timeTbd = false;
	private boolean dateTbd = false;
	private EventStatus eventStatus;
	private Integer stubhubId;
	private Integer seatwaveId;
	private java.util.Date creationDate;
	private java.util.Date lastUpdate;
	private CreationType creationType;
	private Integer admitoneId;
	private TourType eventType;
//	protected Tour tour;
	private Artist artist;
	private Integer artistId;
	
//	private Boolean zoneEvent;
	private String eventOverridden;
	
	private EventTicketStat eventTicketStat;
	private String updatedBy;
	private Integer brokerId;
	private String brokerStatus;
	private Integer parentCategoryId;
	private String childCategoryName;
	private String posChildCategoryName;
	private Boolean venueMap; 
	private Boolean presaleEvent;
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	@Column(name="event_type")
	@Enumerated(EnumType.STRING)
	public TourType getEventType() {
		if (eventType != null) {
			return eventType;
		}
		return TourType.OTHER;
	}

	public void setEventType(TourType eventType) {
		if (eventType == null) {
			eventType = TourType.OTHER;
		}
		this.eventType = eventType;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	// store date in UTC
	@Column(name="event_date", columnDefinition="DATE")
	public Date getLocalDate() {
		return localDate;
	}
	
	public void setLocalDate(Date date) {
		this.localDate = date;
	}

	@Transient
	public java.util.Date getLocalDateTime() {
		if (localDate != null){
			java.util.Date dateTime = new java.util.Date(localDate.getTime());
			if(localTime != null){
				dateTime.setHours(localTime.getHours());
				dateTime.setMinutes(localTime.getMinutes());
				dateTime.setSeconds(localTime.getSeconds());
			}
			return dateTime;
		}else{
			return null;
		}
	}


	@Column(name="timezone")
	public String getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(String timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Transient
	public TimeZone getTimeZone() {
		if (timeZoneId == null) {
			return TimeZone.getTimeZone("America/New_York");
		}
		return TimeZone.getTimeZone(timeZoneId);
	}
	
	@Transient
	public String getTimeZoneSymbol() {
		return getTimeZone().getDisplayName(false, TimeZone.SHORT);
	}
	
	// return date in UTC
	@Transient
	public java.util.Date getDate() {
		if(localDate != null){
			Calendar calendar = Calendar.getInstance(getTimeZone());
			calendar.setTime(localDate);
			return calendar.getTime();
		}else{
			return null;
		}
	}

	// return time in UTC
	@Transient
	public Time getTime() {
		if (localTime == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance(getTimeZone());
		calendar.setTime(localTime);
		return new Time(calendar.getTime().getTime());
	}

	@Column(name="event_time")
	public Time getLocalTime() {
		return localTime;
	}

	public void setLocalTime(Time localTime) {
		this.localTime = localTime;
	}

	@Transient
	public boolean getTimeTbd() {
		return timeTbd;
	}

	public void setTimeTbd(boolean timeTbd) {
		this.timeTbd = timeTbd;
	}
	
	@Transient
	public boolean getDateTbd() {
		return dateTbd;
	}

	public void setDateTbd(boolean dateTbd) {
		this.dateTbd = dateTbd;
	}
	
	@Column(name="stubhub_id")
	public Integer getStubhubId() {
		return stubhubId;
	}

	public void setStubhubId(Integer stubhubId) {
		this.stubhubId = stubhubId;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="event_status")
	public EventStatus getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	
	@Transient
	public String getFormattedEventDate() {
		// do this because dateFormat.format is not thread safe
		synchronized(Event.class) {
			return ((getDate() == null)?"TBD":(dateFormat.format(getDate()))) + " " + ((getTime() == null)?"TBD":(timeFormat.format(getTime())));
		}
	}
	
	
	
	@Column(name="creation_date")
	public java.util.Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(java.util.Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="last_update")
	public java.util.Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(java.util.Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Column(name="creation_type")
	@Enumerated(EnumType.STRING)
	public CreationType getCreationType() {
		return creationType;
	}

	public void setCreationType(CreationType creationType) {
		this.creationType = creationType;
	}
	
	@Column(name="admitone_id")
	public Integer getAdmitoneId() {
		return admitoneId;
	}

	@Column(name="seatwave_id")
	public Integer getSeatwaveId() {
		return seatwaveId;
	}

	public void setSeatwaveId(Integer seatwaveId) {
		this.seatwaveId = seatwaveId;
	}

	public void setAdmitoneId(Integer admitoneId) {
		this.admitoneId = admitoneId;
	}
	
	/*@Column(name="zone_event")
	public Boolean getZoneEvent() {
		return zoneEvent;
	}

	public void setZoneEvent(Boolean zoneEvent) {
		this.zoneEvent = zoneEvent;
	}*/

	@Column(name="event_over_riden")
	public String getEventOverridden() {
		return eventOverridden;
	}

	public void setEventOverridden(String eventOverridden) {
		this.eventOverridden = eventOverridden;
	}

	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}

	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	@Transient
	public EventTicketStat getEventTicketStat() {
		return eventTicketStat;
	}
	
	public void setEventTicketStat(EventTicketStat eventTicketStat) {
		this.eventTicketStat = eventTicketStat;
	}

	/*@Transient
	public Tour getTour() {
		if (tour == null) {
			tour = DAORegistry.getTourDAO().get(tourId);
		}
		return tour;
	}*/

	public static class EventTicketStat {
		private int ticketListingCount;
		private int ticketCount;
		
		public EventTicketStat(int ticketListingCount, int ticketCount) {
			this.ticketListingCount = ticketListingCount;
			this.ticketCount = ticketCount;
		}

		public int getTicketListingCount() {
			return ticketListingCount;
		}

		public int getTicketCount() {
			return ticketCount;
		}				
	}
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(name = "broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	@Column(name = "broker_status")
	public String getBrokerStatus() {
		return brokerStatus;
	}

	public void setBrokerStatus(String brokerStatus) {
		this.brokerStatus = brokerStatus;
	}

	@Transient
	public Artist getArtist() {
		if(artist!=null){
			return artist;
		}
		if(artistId !=null)
			artist = DAORegistry.getArtistDAO().get(artistId);
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	@Column(name = "artist_id")
	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="auto_correct")
	public Boolean getAutoCorrect() {
		if(autoCorrect==null){
			return false;
		}
		return autoCorrect;
	}

	public void setAutoCorrect(Boolean autoCorrect) {
		this.autoCorrect = autoCorrect;
	}

	@Column(name="ignore_tbd_time")
	public Boolean getIgnoreTBDTime() {
		if(ignoreTBDTime==null){
			return false;
		}
		return ignoreTBDTime;
	}

	public void setIgnoreTBDTime(Boolean ignoreTBDTime) {
		this.ignoreTBDTime = ignoreTBDTime;
	}
	
	@Column(name="presale_event")
	public Boolean getPresaleEvent() {
		if(presaleEvent==null){
			return false;
		}
		return presaleEvent;
	}

	public void setPresaleEvent(Boolean presaleEvent) {
		this.presaleEvent = presaleEvent;
	}

	@Transient
	public Venue getVenue() {
		if (venueId == null) {
			return null;
		}
		
		if (venue == null) {
			//DAOCall method - for fetching venue by venue id
			venue = DAORegistry.getVenueDAO().get(venueId); 
		}
		return venue;
	}
	
	@Transient
	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	
	
	

	
		/**
	 * Get a well formatted venue description.
	 * @return
	 */
	@Transient
	public String getFormattedVenueDescription() {
		Venue venue = getVenue();
		if (venue == null) {
			return "n/a";
		}
		
		String[] tokens = {
			venue.getBuilding(),
			venue.getCity(),
			venue.getState(),
			venue.getCountry()				
		};
		
		String v = null;
		for (String token: tokens) {
			if (token == null || token.equals("null")) {
				continue;
			}
			
			if (v == null) {
				v = token;
			} else {
				v += ", " + token;
			}
		}
		return v;
	}

	
	@Transient  //commented as we don't use it ager zones b2b. 
	public String getZoneCategoryGroupName() {
		return zoneCategoryGroupName;
	}

	public void setZoneCategoryGroupName(String zoneCategoryGroupName) {
		this.zoneCategoryGroupName = zoneCategoryGroupName;
	}
	@Transient
	public String getFormatedDate() {
		// TODO Auto-generated method stub	
		if(getDate() == null){
			formatedDate = "TBD";	
		}else{
		formatedDate= dateFormat.format(getDate());
		}
		if(getTime()!=null){
			formatedDate = formatedDate+" "+timeFormat.format(getTime());
		}else{
			formatedDate = formatedDate+" TBD";
		}
		
		return formatedDate;
	}
	
	@Transient
	public String getFormatedAdmitoneId() {
		if(getAdmitoneId()==null){
			formatedAdmitoneId = "False";
		}else{
			formatedAdmitoneId = "True";
		}
		return formatedAdmitoneId;
	}		
	
	@Transient
	public String getCatGroupName() {
		catGroupName = "";
		if(this.getVenueCategory()!=null){
			catGroupName = this.getVenueCategory().getCategoryGroup();
		}
		if(catGroupName.isEmpty()){
			return "No Map";
		}else{
		File file = new File("C:/TMATIMAGESFINAL/SvgMaps/" + getVenueId() + "_" +catGroupName+ ".gif");		
		if (!file.exists()) {
			return "No Map";
			}
		}
		return catGroupName;
	}
	
	@Transient
	public String toString() {
		String thisEvent = "Event[EventId:" + id 
		+ ", EventName: " + name 
		+ ", EventDate: " + localDate 
		+ ", Venue: " + this.getFormattedVenueDescription() + "]";
		
		return thisEvent;
	}
	
	@Transient
	public Collection<String> getEnabledCrawlSites() {
		return enabledCrawlSites;
	}

	public void setEnabledCrawlSites(Collection<String> enabledCrawlSites) {
		this.enabledCrawlSites = enabledCrawlSites;
	}

	@Transient
	public Collection<String> getDisabledCrawlSites() {
		return disabledCrawlSites;
	}

	public void setDisabledCrawlSites(Collection<String> disabledCrawlSites) {
		this.disabledCrawlSites = disabledCrawlSites;
	}

	@Transient
	public VenueCategory getVenueCategory() {
		if(this.venueCategoryId!=null && this.venueCategory==null){
			venueCategory = DAORegistry.getVenueCategoryDAO().get(venueCategoryId); 
		}
		return venueCategory;
	}

	public void setVenueCategory(VenueCategory venueCategory) {
		this.venueCategory = venueCategory;
	}

	@Transient
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	/*@Transient
	public String getChildCategoryName() {
		return childCategoryName;
	}

	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}*/

	@Transient
	public String getPosChildCategoryName() {
		return posChildCategoryName;
	}

	public void setPosChildCategoryName(String posChildCategoryName) {
		this.posChildCategoryName = posChildCategoryName;
	}

	@Transient
	public Boolean getVenueMap() {
		return venueMap;
	}

	public void setVenueMap(Boolean venueMap) {
		this.venueMap = venueMap;
	}
	
		
	
	
}