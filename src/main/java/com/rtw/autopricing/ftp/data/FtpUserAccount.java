package com.rtw.autopricing.ftp.data;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.utils.FTPType;

@Entity
@Table(name="ftp_user_account_details")
public class FtpUserAccount implements Serializable{
	
	private static final long serialVersionUID = 1L;	
	private Integer accountNo;
	private String hostName;
	private String portNumber;
	private String userName;
	private String password;
//	private String fileSource;
	private String destination;
	private Date uploadStartTime;
	private Date lastUploadTime;
	private Date nextUploadTime;
	private Integer frequency;
	private String status;
	private Integer minute;
	private Integer hour;
	private String formatedDate;
//	private String filePath;
	//private String fileName;
	private String changeOption;
	private File jspFile;
	List<FtpUserFileInformation> ftpUserFileInformation;
	private FTPType ftpType;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="account_no")
	public Integer getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(Integer accountNo) {
		this.accountNo = accountNo;
	}

	@Column(name="host")
	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name="password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

/*	@Column(name="source")
	public String getFileSource() {
		return fileSource;
	}

	public void setFileSource(String fileSource) {
		this.fileSource = fileSource;
	}*/
	

	@Column(name="destination")
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	@Column(name="upload_start_time", columnDefinition="DATE")
	public Date getUploadStartTime() {
		return uploadStartTime;
	}

	public void setUploadStartTime(Date uploadStartTime) {
		this.uploadStartTime = uploadStartTime;
	}
	
	@Column(name="next_upload_time", columnDefinition="DATE")
	public Date getNextUploadTime() {
		return nextUploadTime;
	}

	public void setNextUploadTime(Date nextUploadTime) {
		this.nextUploadTime = nextUploadTime;
	}
	
	@Column(name="last_upload_time", columnDefinition="DATE")
	public Date getLastUploadTime() {
		return lastUploadTime;
	}

	public void setLastUploadTime(Date lastUploadTime) {
		this.lastUploadTime = lastUploadTime;
	}

	@Column(name="frequency")
	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Transient
	public Integer getMinute() {
		
		if(uploadStartTime != null){
			DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			String newDateString = dbDateTimeFormat.format(uploadStartTime);
			String[] dateArray = newDateString.split(" ");
			String[] timeArray = dateArray[1].split(":");
			minute= Integer.parseInt(timeArray[1]) ;
		}
		
		return minute;
	}

	public void setMinute(Integer minute) {
		this.minute = minute;
	}

	@Transient
	public Integer getHour() {
		if(uploadStartTime != null){
			DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			String newDateString = dbDateTimeFormat.format(uploadStartTime);
			String[] dateArray = newDateString.split(" ");
			String[] timeArray = dateArray[1].split(":");
			hour= Integer.parseInt(timeArray[0]) ;
		}
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	@Transient
	public String getFormatedDate() {
		if(uploadStartTime != null){
			DateFormat uiDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			formatedDate = uiDateFormat.format(uploadStartTime);
		}
		return formatedDate;
	}

	public void setFormatedDate(String formatedDate) {
		this.formatedDate = formatedDate;
	}

	
/*	@Column(name="file_path")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}*/
	/*
	@Column(name="file_name")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
*/
	@Transient
	public File getJspFile() {
		return jspFile;
	}

	public void setJspFile(File jspFile) {
		this.jspFile = jspFile;
	}

	@Transient
	public String getChangeOption() {
		return changeOption;
	}

	public void setChangeOption(String changeOption) {
		this.changeOption = changeOption;
	}

	@OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="ftpUserAccount")
	public List<FtpUserFileInformation> getFtpUserFileInformation() {
		return ftpUserFileInformation;
	}

	public void setFtpUserFileInformation(
			List<FtpUserFileInformation> ftpUserFileInformation) {
		this.ftpUserFileInformation = ftpUserFileInformation;
	}

	@Column(name="ftp_port")
	public String getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(String portNumber) {
		this.portNumber = portNumber;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ftp_type")
	public FTPType getFtpType() {
		return ftpType;
	}

	public void setFtpType(FTPType ftpType) {
		this.ftpType = ftpType;
	}
	
	
		
}
