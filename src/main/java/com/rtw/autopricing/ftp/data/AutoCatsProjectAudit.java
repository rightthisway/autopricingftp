package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name="autocat_projects_audit")
public class AutoCatsProjectAudit implements Serializable{
  private Integer id;
  private Broker broker;
  private AutopricingProduct product;
  private Integer count;
  private Date lastRunTime;
  private String processType;
  

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name = "id")
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
@OneToOne
@JoinColumn(name = "broker_id")
public Broker getBroker() {
	return broker;
}
public void setBroker(Broker broker) {
	this.broker = broker;
}
@OneToOne
@JoinColumn(name = "product_id")
public AutopricingProduct getProduct() {
	return product;
}
public void setProduct(AutopricingProduct product) {
	this.product = product;
}
@Column(name = "count")
public Integer getCount() {
	return count;
}
public void setCount(Integer count) {
	this.count = count;
}
@Column(name = "last_run_time")
public Date getLastRunTime() {
	return lastRunTime;
}
public void setLastRunTime(Date lastRunTime) {
	this.lastRunTime = lastRunTime;
}

@Column(name = "process_type")
public String getProcessType() {
	return processType;
}
public void setProcessType(String processType) {
	this.processType = processType;
}
  
}
