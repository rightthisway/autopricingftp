package com.rtw.autopricing.ftp.data;



import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="market_maker_event_tracking")
public class MarketMakerEventTracking implements Serializable {
	private Integer id;
	private String marketMakerUsername;
	private String creator;
	private Integer eventId;
	private Date creationDate;
	private Date endDate;
	private boolean enabled;
	
	public MarketMakerEventTracking() {
		this.enabled = false;
		this.creationDate = new Date();
	}

	public MarketMakerEventTracking(String creator,
			String marketMakerUsername,
			Integer eventId) {
		this(creator, marketMakerUsername, eventId, new Date(), null, true);
	}

	public MarketMakerEventTracking(String creator,
			String marketMakerUsername,
			Integer eventId,
			Date creationDate,
			Date endDate,
			boolean enabled) {
		super();
		this.creator = creator;
		this.marketMakerUsername = marketMakerUsername;
		this.eventId = eventId;
		this.creationDate = creationDate;
		this.endDate = endDate;
		this.enabled = enabled;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="market_maker_username")
	public String getMarketMakerUsername() {
		return marketMakerUsername;
	}
	
	public void setMarketMakerUsername(String marketMakerUsername) {
		this.marketMakerUsername = marketMakerUsername;
	}
	
	@Column(name="creator")
	public String getCreator() {
		return creator;
	}
	
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="end_date")
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@Column(name="enabled")
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}		
}
