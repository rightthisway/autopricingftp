package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;


@Entity
@Table(name="crownjewel_events")
public class CrownJewelEvents  implements Serializable,ExchangeEvent {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer eventId;
	private Event event;
	private String exposure;
	private Double rptFactor;
	private Double priceBreakup;
	private Double lowerMarkup;
	private Double upperMarkup;
	private Double lowerShippingFees;
	private Double upperShippingFees;
	private String status;
	private Integer shippingMethod;
	private Integer nearTermDisplayOption;
	private Boolean allowSectionRange;
	private Integer shippingDays;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String cjParentType;
	private Integer zoneTicketBrokerId;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)  
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Transient
	public Event getEvent() {
		if(event==null){
			if(eventId==null){
				return null;
			}
			try {
				event = DAORegistry.getEventDAO().get(eventId);
			}catch (Exception e) {
				
			}
		}
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	@Column(name="exposure")
	public String getExposure() {
		return exposure;
	}
	public void setExposure(String exposure) {
		this.exposure = exposure;
	}
	
	@Column(name="rpt_factor")
	public Double getRptFactor() {
		return rptFactor;
	}
	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}
	
	@Column(name="price_breakup")
	public Double getPriceBreakup() {
		return priceBreakup;
	}
	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}
	
	@Column(name="lower_markup")
	public Double getLowerMarkup() {
		return lowerMarkup;
	}
	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}
	
	@Column(name="upper_markup")
	public Double getUpperMarkup() {
		return upperMarkup;
	}
	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}
	
	@Column(name="lower_shipping_fees")
	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}
	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}
	
	@Column(name="upper_shipping_fees")
	public Double getUpperShippingFees() {
		return upperShippingFees;
	}
	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	@Column(name="near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}
	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	@Column(name="allow_section_range")
	public Boolean getAllowSectionRange() {
		return allowSectionRange;
	}
	public void setAllowSectionRange(Boolean allowSectionRange) {
		this.allowSectionRange = allowSectionRange;
	}
	
	@Column(name="shipping_days")
	public Integer getShippingDays() {
		return shippingDays;
	}
	public void setShippingDays(Integer shippingDays) {
		this.shippingDays = shippingDays;
	}

	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name="last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="parent_type")
	public String getCjParentType() {
		return cjParentType;
	}
	public void setCjParentType(String cjParentType) {
		this.cjParentType = cjParentType;
	}
	
	@Column(name="zoneticket_broker_id")
	public Integer getZoneTicketBrokerId() {
		return zoneTicketBrokerId;
	}
	public void setZoneTicketBrokerId(Integer zoneTicketBrokerId) {
		this.zoneTicketBrokerId = zoneTicketBrokerId;
	}

	@Transient
	public Integer getTicketNetworkBrokerId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setTicketNetworkBrokerId(Integer ticketNetworkBroker) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getVividBrokerId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setVividBrokerId(Integer vividBrokerId) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getScoreBigBrokerId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setScoreBigBrokerId(Integer scoreBigBrokerId) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Broker getTicketNetworkBroker() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setTicketNetworkBroker(Broker ticketNetworkBroker) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Broker getVividBroker() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setVividBroker(Broker vividBroker) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Broker getScoreBigBroker() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setScoreBigBroker(Broker scoreBigBroker) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getZone() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setZone(Boolean isZone) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Broker getZoneTicketBroker() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setZoneTicketBroker(Broker zoneTicketBroker) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getDiscountZone() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public Boolean getVividEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setVividEnabled(Boolean vividEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public void setDiscountZone(Boolean discountZone) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getZoneDiscountPerc() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setZoneDiscountPerc(Double zoneDiscountPerc) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getRotEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setRotEnabled(Boolean rotEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getRtwEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setRtwEnabled(Boolean rtwEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getMinicatsEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setMinicatsEnabled(Boolean minicatsEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getLastrowMinicatsEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setLastrowMinicatsEnabled(Boolean lastrowMinicatsEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getVipMinicatsEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setVipMinicatsEnabled(Boolean vipMinicatsEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getFanxchangeBrokerId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setFanxchangeBrokerId(Integer fanxchangeBrokerId) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Broker getFanxchangeBroker() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setFanxchangeBroker(Broker fanxchangeBroker) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public String getAssignedProduct() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setAssignedProduct(String assignedProduct) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getSectionTicketCount() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setSectionTicketCount(Integer sectionTicketCount) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getPresaleAutocatsEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setPresaleAutocatsEnabled(Boolean presaleAutocatsEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getIsPresale() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setIsPresale(Boolean isPresale) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getTicketcityBrokerId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setTicketcityBrokerId(Integer ticketcityBrokerId) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Broker getTicketcityBroker() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setTicketcityBroker(Broker ticketcityBroker) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getVipLastrowMinicatsEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setVipLastrowMinicatsEnabled(Boolean vipLastrowMinicatsEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getSeatGeekBrokerId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setSeatGeekBrokerId(Integer seatGeekBrokerId) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Broker getSeatGeekBroker() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public void setSeatGeekBroker(Broker seatGeekBroker) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Double getTaxPercentage() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTaxPercentage(Double taxPercentage) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Integer getExcludeHoursBeforeEvent() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setExcludeHoursBeforeEvent(Integer excludeHoursBeforeEvent) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getTixcityEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setTixcityEnabled(Boolean tixcityEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getZonedLastrowMinicatsEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setZonedLastrowMinicatsEnabled(
			Boolean zonedLastrowMinicatsEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getLarryLastEnabled() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setLarryLastEnabled(Boolean larryLastEnabled) {
		// TODO Auto-generated method stub
		
	}
	@Transient
	public Boolean getPreSaleSettingsUpdated() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setPreSaleSettingsUpdated(Boolean preSaleSettingsUpdated) {
		// TODO Auto-generated method stub
		
	}
	
}

