package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="child_tour_category")
public class ChildCategory implements Serializable{
	private Integer id;
	private String name;
	private ParentCategory parentCategory;
	
	
	public ChildCategory(){
		
	}
	/*public ChildCategory(ChildTourCategoryCommand childTourCategoryCommand){
		this.id=childTourCategoryCommand.getId();
		this.name=childTourCategoryCommand.getName();
		this.tourCategory=DAORegistry.getTourCategoryDAO().get(childTourCategoryCommand.getTourCategory());
	}*/
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToOne
	@JoinColumn(name="tour_category_id")
	public ParentCategory getTourCategory() {
		return parentCategory;
	}
	public void setTourCategory(ParentCategory parentCategory) {
		this.parentCategory = parentCategory;
	}

}
