package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="admitone_users_broker_details")
public class UserBrokerDetails implements Serializable{
	
private Integer Id;
private User user;
private Integer brokerId;

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name = "id")
public Integer getId() {
	return Id;
}
public void setId(Integer id) {
	Id = id;
}
@ManyToOne
@JoinColumn(name="user_id")
public User getUser() {
	return user;
}
public void setUser(User user) {
	this.user = user;
}
@Column(name="broker_id")
public Integer getBrokerId() {
	return brokerId;
}
public void setBrokerId(Integer brokerId) {
	this.brokerId = brokerId;
}
}
