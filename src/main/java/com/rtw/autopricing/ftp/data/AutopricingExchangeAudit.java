package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="autopricing_exchange_audit")
public class AutopricingExchangeAudit implements Serializable {
	 private Integer id;
	 private Integer exchangeId;
	 private String action;
	 private String lastUpdatedBy;
	 private Date lastUpdated;
	 private String exchangeName;
	 private Double additionalMarkup;
	
	 
	 public AutopricingExchangeAudit(AutopricingExchange autopricingExchange) {
		 this.exchangeId = autopricingExchange.getId();
		 this.exchangeName = autopricingExchange.getName();
		 this.additionalMarkup = autopricingExchange.getAdditionalMarkup();
	 }
 
	 public AutopricingExchangeAudit() {
		 
	 }
	 
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="exchange_id")
	public Integer getExchangeId() {
		return exchangeId;
	}
	public void setExchangeId(Integer exchangeId) {
		this.exchangeId = exchangeId;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="additional_markup")
	public Double getAdditionalMarkup() {
		return additionalMarkup;
	}
	public void setAdditionalMarkup(Double additionalMarkup) {
		this.additionalMarkup = additionalMarkup;
	}
	
	@Column(name="exchange_name")
	public String getExchangeName() {
		return exchangeName;
	}
	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}
	
	
	}
