package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Generated;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.util.BrokerUtils;

@Entity
@Table(name = "broker_audit")
public class BrokerAudit implements Serializable {

	private Integer id;
	private Integer brokerId;
	private String name;
	private String emailId;
	private String phoneNo;
	private String status;
	private Boolean isAutopricingBroker;
	private String emailSuffix;
	private Integer buyerBrokerId;
	private Integer clientBrokerEmployeeId;
	private Integer systemUserId;
	private Integer clientBrokerId;
	private Integer posBrokerId;
	private String lastUpdatedBy;
	private Date lastUpdated;
	private String action;
	
	public BrokerAudit(Broker broker) {
		this.brokerId = broker.getId();
		this.name = broker.getName();
		this.emailId = broker.getEmailId();
		this.emailSuffix = broker.getEmailSuffix();
		this.phoneNo = broker.getPhoneNo();
		this.isAutopricingBroker = broker.getisAutopricingBroker();
		this.buyerBrokerId = broker.getBuyerBrokerId();
		this.clientBrokerEmployeeId = broker.getClientBrokerEmployeeId();
		this.clientBrokerId = broker.getClientBrokerId();
		this.posBrokerId = broker.getPosBrokerId();
		this.systemUserId = broker.getSystemUserId();
	}
	public BrokerAudit() {
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "email_id")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Column(name = "phone_no")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "email_suffix")
	public String getEmailSuffix() {
		return emailSuffix;
	}

	public void setEmailSuffix(String emailSuffix) {
		this.emailSuffix = emailSuffix;
	}

	@Column(name = "buyer_broker_id")
	public Integer getBuyerBrokerId() {
		return buyerBrokerId;
	}
	public void setBuyerBrokerId(Integer buyerBrokerId) {
		this.buyerBrokerId = buyerBrokerId;
	}
	
	@Column(name = "client_broker_employee_id")
	public Integer getClientBrokerEmployeeId() {
		return clientBrokerEmployeeId;
	}
	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId) {
		this.clientBrokerEmployeeId = clientBrokerEmployeeId;
	}
	
	@Column(name = "system_user_id")
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	
	@Column(name = "client_broker_id")
	public Integer getClientBrokerId() {
		return clientBrokerId;
	}
	public void setClientBrokerId(Integer clientBrokerId) {
		this.clientBrokerId = clientBrokerId;
	}
	
	@Column(name = "pos_broker_id")
	public Integer getPosBrokerId() {
		return posBrokerId;
	}
	public void setPosBrokerId(Integer posBrokerId) {
		this.posBrokerId = posBrokerId;
	}

	@Column(name = "is_autopricing_broker")
	public Boolean getIsAutopricingBroker() {
		return isAutopricingBroker;
	}

	public void setIsAutopricingBroker(Boolean isAutopricingBroker) {
		this.isAutopricingBroker = isAutopricingBroker;
	}

	@Column(name = "last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Column(name = "action")
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
}
