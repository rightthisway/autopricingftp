package com.rtw.autopricing.ftp.data;



import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


@Entity
@Table(name="expired_tickets")
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public final class HarvestTicket extends Ticket {
}
