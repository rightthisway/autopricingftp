package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.rtw.autopricing.ftp.enums.SeatGeekWSTrackingStatus;

@Entity
@Table(name="seatgeek_ws_tracking")
public class SeatGeekWSTracking  implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String url;
	private String message;
	private String action;
	private String error;
	private Date createdDate;
	private SeatGeekWSTrackingStatus status;
	private String orderId;
	private Integer catTicketId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)  
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column(name="message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Column(name="error")
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public SeatGeekWSTrackingStatus getStatus() {
		return status;
	}
	public void setStatus(SeatGeekWSTrackingStatus status) {
		this.status = status;
	}
	
	@Column(name="order_id")
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	@Column(name="cat_ticket_id")
	public Integer getCatTicketId() {
		return catTicketId;
	}
	public void setCatTicketId(Integer catTicketId) {
		this.catTicketId = catTicketId;
	}
	
}

