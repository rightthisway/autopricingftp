package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unbroadcasted_events")
public class UnbroadcastedEvents implements Serializable {
	
	private Integer id;
	private String unbroadcastedDate;
	private Date lastUpdated;
	private String lastUpdatedBy;
	private String admitoneIds;
	private String eventIds;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="unbroadcasted_date")
	public String getUnbroadcastedDate() {
		return unbroadcastedDate;
	}
	public void setUnbroadcastedDate(String unbroadcastedDate) {
		this.unbroadcastedDate = unbroadcastedDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name="admitone_ids")
	public String getAdmitoneIds() {
		return admitoneIds;
	}
	public void setAdmitoneIds(String admitoneIds) {
		this.admitoneIds = admitoneIds;
	}
	@Column(name="event_ids")
	public String getEventIds() {
		return eventIds;
	}
	public void setEventIds(String eventIds) {
		this.eventIds = eventIds;
	}
		
}
