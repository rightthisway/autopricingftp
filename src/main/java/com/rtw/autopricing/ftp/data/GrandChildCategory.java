package com.rtw.autopricing.ftp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="grand_child_tour_category")
public class GrandChildCategory implements Serializable{
	private Integer id;
	private String name;
	private ChildCategory childCategory;
	
	public GrandChildCategory(){
		
	}
	/*public GrandChildCategory(GrandChildTourCategoryCommand grandChildTourCategoryCommand){
		this.id=grandChildTourCategoryCommand.getId();
		this.name=grandChildTourCategoryCommand.getName();
		this.childCategory=DAORegistry.getChildTourCategoryDAO().get(grandChildTourCategoryCommand.getChildTourCategory());
	}*/
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToOne
	@JoinColumn(name="child_category_id")
	public ChildCategory getChildTourCategory() {
		return childCategory;
	}
	public void setChildTourCategory(ChildCategory childCategory) {
		this.childCategory = childCategory;
	}

}
