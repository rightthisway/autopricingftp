package com.rtw.autopricing.ftp.data;



import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="brokers_manual_category_tickets")
public class ManualCategoryTicket implements Serializable{

	private Integer id;
	private Integer tnLocalEventId;
	private Date eventDate;
	private String eventName;
	private String venueName;
	private Integer tnCategoryGroupId;
	private Integer quantity;
	private String section;
	private String row;
	private String seatFrom;
	private String seatThru;
	private Integer edelivery;
	private Integer brokerId;
	private Date inHandDate;
	private Double cost;
	private String status;
	private Date lastUpdated;
	private Date createdDate;
	private Boolean isEdit;
	private String priceHistory;
	
	@Id 
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="tn_category_group_id")
	public Integer getTnCategoryGroupId() {
		return tnCategoryGroupId;
	}
	public void setTnCategoryGroupId(Integer tnCategoryGroupId) {
		this.tnCategoryGroupId = tnCategoryGroupId;
	}
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name="seat_from")
	public String getSeatFrom() {
		return seatFrom;
	}
	public void setSeatFrom(String seatFrom) {
		this.seatFrom = seatFrom;
	}
	
	@Column(name="seat_thru")
	public String getSeatThru() {
		return seatThru;
	}
	public void setSeatThru(String seatThru) {
		this.seatThru = seatThru;
	}
	
	@Column(name="edelivery")
	public Integer getEdelivery() {
		return edelivery;
	}
	public void setEdelivery(Integer edelivery) {
		this.edelivery = edelivery;
	}
	
	@Column(name="in_hand_date")
	public Date getInHandDate() {
		return inHandDate;
	}
	public void setInHandDate(Date inHandDate) {
		this.inHandDate = inHandDate;
	}
	
	@Column(name="cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="tn_local_event_id")
	public Integer getTnLocalEventId() {
		return tnLocalEventId;
	}
	public void setTnLocalEventId(Integer tnLocalEventId) {
		this.tnLocalEventId = tnLocalEventId;
	}
	
	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	@Transient
	public Boolean getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name="price_history")
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	
	
	
	
	
}

