package com.rtw.autopricing.ftp.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zone_ticket_locked_ticket_event_details")
public class ZoneTicketsLockedTicketEventDetails implements Serializable {
	
	Integer id;
	Date createdDate;
	Integer eventId;
	
	public ZoneTicketsLockedTicketEventDetails() {
		
	}
	
	public ZoneTicketsLockedTicketEventDetails(Integer eventId,Date createdDate) {
		this.eventId=eventId;
		this.createdDate=createdDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
}
