package com.rtw.autopricing.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Broker;


public class AutoCats96Utils {
	
	private static List<Integer> applicableBrokerIds = new ArrayList<Integer>();
	private static List<Integer> filledBrokerIds = new ArrayList<Integer>();
	private static Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
	

	public static Map<Integer, Broker> getAllAutoPricingBrokersForAutocats96(){
		Collection<Broker> activeBrokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		for (Broker broker : activeBrokers) {
			if(broker.getId() == 1 || broker.getId() == 4 || broker.getId() == 6){// || broker.getId() == 7 
				continue;
			}
			brokerMap.put(broker.getId(), broker);
		}
		return new HashMap<Integer, Broker>(brokerMap);
	}
	
	public static Integer getNextAvailableBrokerIdOld(Integer lastProcessedBrokerId){
		
		int totalBrokerSize = applicableBrokerIds.size();
		int indexPosition = applicableBrokerIds.indexOf(lastProcessedBrokerId);
		
		if(indexPosition == totalBrokerSize-1){
			indexPosition = 0;
		}else{
			indexPosition = indexPosition +1;
		}
		
		Integer nextBrokerId = applicableBrokerIds.get(indexPosition);
		
		if(filledBrokerIds.contains(nextBrokerId)){
			if(filledBrokerIds.containsAll(applicableBrokerIds)) {
				return null;
			}
			getNextAvailableBrokerIdOld(nextBrokerId);
			nextBrokerId = applicableBrokerIds.get(indexPosition);
		}
		return nextBrokerId;
	}
	
	public static Integer getNextAvailableBrokerId(Integer lastProcessedBrokerId,List<Integer> availableTnBrokerIds){
		
		if(null == availableTnBrokerIds || availableTnBrokerIds.isEmpty()){
			return null;
		}
		
		if(null == lastProcessedBrokerId){
			return availableTnBrokerIds.get(0);
		}
		
		int totalBrokerSize = availableTnBrokerIds.size();
		int indexPosition = availableTnBrokerIds.indexOf(lastProcessedBrokerId);
		if(indexPosition == totalBrokerSize-1){
			indexPosition = 0;
		}else{
			indexPosition = indexPosition +1;
		}
		Integer nextBrokerId = availableTnBrokerIds.get(indexPosition);
		return nextBrokerId;
	}
	
	
	public static List<Integer> getEventActiveBrokers(List<Integer> tnbrokerIds){
		applicableBrokerIds.clear();
		for (Integer brokerId : tnbrokerIds) {
			applicableBrokerIds.add(brokerId);
		}
		return applicableBrokerIds;
	}
	
	public static List<Integer> addFullFilledBrokerIds(Integer fullFilledBrokerId){
		filledBrokerIds.add(fullFilledBrokerId);
		return filledBrokerIds;
	}
	
	public static void clearFullFilledBrokerIds(){
		filledBrokerIds.clear();
	}
	
	public static List<Integer> getAutoCatsActiveBrokerIds(){
		 List<Integer> autoCats96BrokerIds = new ArrayList<Integer>();
		 //autoCats96BrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID); /*Tixcity*/
		 autoCats96BrokerIds.add(5); /*RTW*/
		 //autoCats96BrokerIds.add(2); /*Joe Knows Tickets*/
		 //autoCats96BrokerIds.add(10);//RTW-2 /*Reserve One Tickets*/
		 
		 return autoCats96BrokerIds;
	 }
	
	public static List<Integer> getBrokerListbyParentCategory(Integer parentCategoryId){
		 if(parentCategoryId == 1 || parentCategoryId == 2){
			 List<Integer> autoCats96SportsConcertsBrokerIds = new ArrayList<Integer>();
			 //autoCats96SportsConcertsBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID); /*Tixcity*/
			 autoCats96SportsConcertsBrokerIds.add(5); /*RTW*/
			 //autoCats96SportsConcertsBrokerIds.add(2); /*Joe Knows Tickets*/
			 //autoCats96SportsConcertsBrokerIds.add(10); //RTW-2 /*Reserve One Tickets*/
			 return autoCats96SportsConcertsBrokerIds;
		 }else {
			 List<Integer> autoCats96TheaterOtherBrokerIds = new ArrayList<Integer>();
			 //autoCats96TheaterOtherBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID); /*Tixcity*/
			 autoCats96TheaterOtherBrokerIds.add(5); /*RTW*/
			 //autoCats96TheaterOtherBrokerIds.add(10);//RTW-2 /*Reserve One Tickets*/
			 //autoCats96TheaterOtherBrokerIds.add(2); /*Joe Knows Tickets*/
			 return autoCats96TheaterOtherBrokerIds;
		 }
	 }
	
	public static List<String> getAutoCats96InternalNotes(){
		List<String> autoCats96ProductInternalNotes = new ArrayList<String>();
		autoCats96ProductInternalNotes.add("MINICATS"); /*MINICATS*/
		autoCats96ProductInternalNotes.add("LASTROW MINICATS"); /*LASTROW MINICATS*/
		autoCats96ProductInternalNotes.add("VIPMINICATS"); /*VIPMINICATS*/
		autoCats96ProductInternalNotes.add("AUTOCAT"); /*PRESALECATS*/
		autoCats96ProductInternalNotes.add("VIPLR"); /* VIP last Row - Added by Mehul*/
		return autoCats96ProductInternalNotes;
	 }
	
	
	
}
