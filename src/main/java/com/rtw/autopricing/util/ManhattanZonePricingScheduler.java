package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZones;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ManhattanZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.data.ZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;

public class ManhattanZonePricingScheduler extends QuartzJobBean implements StatefulJob {

	private static Logger log = LoggerFactory.getLogger(ManhattanZonePricingScheduler.class);
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 public static Double ROT_DEFAULT_NO_FLOORCAP_MARKUP = 20.0;
	
	 public static void processManhattanZonesPricingTickets() throws Exception {
		 Calendar cal =  Calendar.getInstance();
			Date now = cal.getTime();
			cal.add(Calendar.MINUTE,15);
			setNextRunTime(cal.getTime());
			if(isStopped() || isRunning()){
				return ;
			}
			setRunning(true);
			
			
			Date lastRunTime = getLastUpdateTime();
			Long minute = 0l;
			if(lastRunTime!=null){
				minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
			}else{
				minute = 2880l; // 2 Days in minute
			}
			AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MHZonePricing");
			if(autopricingProduct==null){
				autopricingProduct = new AutopricingProduct();
				autopricingProduct.setName("MHZonePricing");
				autopricingProduct.setStatus("ACTIVE");
				autopricingProduct.setStopped(false);
			}
			if(autopricingProduct.getStopped()){
				setStopped(true);
				setRunning(false);
				return;
			}
			
			setLastUpdateTime(now);
			autopricingProduct.setLastRunTime(now);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			DateFormat timeDf = new SimpleDateFormat("HH:mm:ss");
			
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			String toAddress = resourceBundle.getString("emailNotificationTo");
			String ccAddress= resourceBundle.getString("emailNotificationCCTo");
			
			AutoPricingError error= null;
			List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
			boolean isErrorOccured = false;
//			Date jobStartTime = new Date();
			int tInsert=0,tUpdate=0,tremovet=0,tposCount=0;
			int eInsert=0,eUpdate=0,eremovet=0,eposCount=0;
			
			try{
				 Map<Integer, Integer> brokerAddCountMap = new HashMap<Integer, Integer>();
				 Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
				 Map<Integer, Integer> brokerUpdateCountMap = new HashMap<Integer, Integer>();
				 
				 Double tnExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork").getAdditionalMarkup();
				 Double vividExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats").getAdditionalMarkup();
				 Double scorebigMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Scorebig").getAdditionalMarkup();
				 Double fanxchangeMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("FanXchange").getAdditionalMarkup();
				 Double ticketcityMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity").getAdditionalMarkup();
				 Double tickPickExAddMarkup = 0.0;
				 Double scoreBigExAddMarkup = 0.0;
					
				 Boolean isTnEvent = false;
				 
				 Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
				 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
				 List<PosEvent> posEvents = new ArrayList<PosEvent>();
				 Map<Integer, List<PosEvent>> posEventsAndBrokerMap = new HashMap<Integer, List<PosEvent>> ();
				 Collection<Event> eventList = null;
				 Collection<ManhattanZonePricingExchangeEvent> manhattanZonePricingExchangeEvents = null;
				 
				 try {
					 //Get all events that is updated in tmat since last run time. 
					 manhattanZonePricingExchangeEvents = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAllManhattanZonePricingExchangeEventsEligibleForUpdate(minute,autopricingProduct);
					 System.out.println("MZP exchange Event Size : "+manhattanZonePricingExchangeEvents.size());
//					 log.info("MZP exchange Pricing Event Size : "+ManhattanZonePricingExchangeEvents.size());
					 
					 eventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentId();
					 for(Event event:eventList) {
						eventMap.put(event.getId(), event);
						eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
					 }
					 System.out.println("MZP : TMAT event size : "+eventList.size());
//					 log.info("ZP : TMAT event size : "+eventList.size());
					 
				 } catch (Exception e) {
					 isErrorOccured = true;
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Loading Events.");
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Event Loading.");
					 error.setEventId(0);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("MZP 1 : Error while Loading Events.");
					 System.err.println("MZP 1 : Error while Loading Events.");
					 e.printStackTrace();
				 }
				 Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
				 Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
				 
				 try {
					 List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductId(autopricingProduct.getId());
					 for (ExcludeEventZones excludeEventZone : excludeEventZones) {
						 Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
						 if(eventZones == null) {
							 eventZones = new HashSet<String>();
						 }
						 eventZones.add(excludeEventZone.getBrokerId()+"_"+excludeEventZone.getZone().toUpperCase());
						 excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
					}
					 List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductId(autopricingProduct.getId());
					 for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
						 Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
						 if(venueCategoryZones == null) {
							 venueCategoryZones = new HashSet<String>();
						 }
						 venueCategoryZones.add(excludeVenueCategoryZone.getBrokerId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
						 excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
					}
				 } catch (Exception e) {
					 isErrorOccured = true;
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Loading Exclude Zones.");
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Exclude Zones Loading.");
					 error.setEventId(0);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("MINI 1 : Error while Loading Exclude Zones..");
					 System.err.println("MINI 1 : Error while Loading Exclude Zones..");
					 e.printStackTrace();
				 }
				 
				 List<ZonesPricingFloorCap> zonesPricingFloorCaps = DAORegistry.getQueryManagerDAO().getAllExchangeEventFloorCap();
				 Map<String, Map<String, Double>> exchangeEventFloorCapMap = new HashMap<String, Map<String,Double>>();
				 Map<String, Map<String, String>> exchangeEventFloorCapSectionMap = new HashMap<String, Map<String,String>>();
				 for (ZonesPricingFloorCap zonesPricingFloorCap : zonesPricingFloorCaps) {
					 if(zonesPricingFloorCap.getBrokerId() == null) {
						continue; 
					 }
					 Map<String, Double> zoneFloorCapMap = exchangeEventFloorCapMap.get(zonesPricingFloorCap.getBrokerId()+":"+zonesPricingFloorCap.getTnExchangeEventId());
					 Map<String, String> zoneFloorCapSectionMap = exchangeEventFloorCapSectionMap.get(zonesPricingFloorCap.getBrokerId()+":"+zonesPricingFloorCap.getTnExchangeEventId());
					 
					 if(null != zoneFloorCapMap && !zoneFloorCapMap.isEmpty()){
							zoneFloorCapMap.put(zonesPricingFloorCap.getSection().replaceAll("\\s+", " ").trim().toLowerCase(), zonesPricingFloorCap.getFloorCap());
							zoneFloorCapSectionMap.put(zonesPricingFloorCap.getSection().replaceAll("\\s+", " ").trim().toLowerCase(), zonesPricingFloorCap.getSection());
					}else {
						zoneFloorCapMap = new HashMap<String, Double>();
						zoneFloorCapSectionMap = new HashMap<String, String>();
						zoneFloorCapMap.put(zonesPricingFloorCap.getSection().replaceAll("\\s+", " ").trim().toLowerCase(), zonesPricingFloorCap.getFloorCap());
						zoneFloorCapSectionMap.put(zonesPricingFloorCap.getSection().replaceAll("\\s+", " ").trim().toLowerCase(), zonesPricingFloorCap.getSection());
					}
					exchangeEventFloorCapMap.put(zonesPricingFloorCap.getBrokerId()+":"+zonesPricingFloorCap.getTnExchangeEventId(), zoneFloorCapMap);
					exchangeEventFloorCapSectionMap.put(zonesPricingFloorCap.getBrokerId()+":"+zonesPricingFloorCap.getTnExchangeEventId(), zoneFloorCapSectionMap);
				 }
				
				 Map<Integer, Map<String, Integer>> brokersVenueConfigZoneMap = new HashMap<Integer, Map<String, Integer>>();
				 
				 int eventSize = manhattanZonePricingExchangeEvents.size();
				 Integer i=0;
				 Map<Integer,DefaultAutoPricingProperties> defaultAutoPricingsMap = new HashMap<Integer, DefaultAutoPricingProperties>();
				 List<DefaultAutoPricingProperties> defaultAutoPricingsFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getDefaultAutoPricingPropertiesByProductId(autopricingProduct.getId());
				 for (DefaultAutoPricingProperties defaultAutopriicngSettings : defaultAutoPricingsFromDB) {
					 defaultAutoPricingsMap.put(defaultAutopriicngSettings.getParentCategoryId(),defaultAutopriicngSettings);
				 }
				 
				 Integer minimamExcludeEventHours = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
				 
				 for (ExchangeEvent exEvent : manhattanZonePricingExchangeEvents) {
					 
					 /*exEvent.setLowerMarkup(0.00);
					 exEvent.setUpperMarkup(0.00);
					 exEvent.setLowerShippingFees(0.00);
					 exEvent.setUpperShippingFees(0.00);
					 exEvent.setExposure("1-OXP");*/
					 
					 
					 if(isStopped()){
						 break;
					 }
					 isTnEvent = false;
					 
					 i++;
					 Integer eventId = exEvent.getEventId();
//					 eventId = 1000071847; 
					 Event event = eventMap.get(eventId);
					 if(event==null ){
						 System.out.println(i+".TMAT Event Not Exisit :" + eventId);
//						 log.info(i+".TMAT Event Not Exisit :" + eventId);
						 continue;
					 }
					
					 
					 eInsert=0;eUpdate=0;eremovet=0;eposCount=0;
					 
					 Integer exBrokerId = null;
					 //if((exEvent.getZone()==null || !exEvent.getZone()) &&  exEvent.getTicketNetworkBrokerId()!=null && exEvent.getTicketNetworkBrokerId()!=0){
					 if(exEvent.getTicketNetworkBrokerId()!=null && exEvent.getTicketNetworkBrokerId()!=0) {
						 isTnEvent = true;
						 exBrokerId = exEvent.getTicketNetworkBrokerId();
					 }else if(exEvent.getVividBrokerId()!=null && exEvent.getVividBrokerId()!=0){
						 exBrokerId = exEvent.getVividBrokerId();
					 }else if(exEvent.getScoreBigBrokerId()!=null && exEvent.getScoreBigBrokerId()!=0){
						 exBrokerId = exEvent.getScoreBigBrokerId();
					 } else if(exEvent.getFanxchangeBrokerId()!=null && exEvent.getFanxchangeBrokerId()!=0){
						 exBrokerId = exEvent.getFanxchangeBrokerId();
					 } else if(exEvent.getSeatGeekBrokerId()!=null && exEvent.getSeatGeekBrokerId()!=0){
						 exBrokerId = exEvent.getSeatGeekBrokerId();
					 } else {
						 exBrokerId = exEvent.getTicketcityBrokerId();
					 }
					 Date currentTime = new Date();
					 Broker broker = null;
					 AutopricingSettings autopricingSettings = null;
					 AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
					 if(isTnEvent){
						 broker = BrokerUtils.getBrokerById(exBrokerId);
						 if(broker!=null){
							 autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
						 }
					 }
					 boolean isUpdateTN=true;
						if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
							isUpdateTN = false;
						}
						
							scoreBigExAddMarkup = scorebigMarkup;
					
					//Remove listings if event is assigned to non Manhattan Broker.					
							if(exEvent.getTicketNetworkBrokerId() == null || exEvent.getZone() == null || !exEvent.getZone()
									|| (exEvent.getTicketNetworkBrokerId() != null && !exEvent.getTicketNetworkBrokerId().equals(4))) { 
								
								 try { // Remove all tickets of event if event is not zone , event is not active or there is restriction to update POS.
									  
									  List<ManhattanZonesPricingCategoryTicket> miniTickets = DAORegistry.getManhattanZonesPricingCategoryTicketDAO().getAllManhattanZonesPricingCategoryTicketsByEventId(eventId);
									  
									 for(ManhattanZonesPricingCategoryTicket cat:miniTickets){
										 
										 cat.setReason("Event is a non Zone Event");
										 if(broker != null && isUpdateTN) {	
											if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
												cat.setStatus("DELETED");
												cat.setLastUpdated(currentTime);
												continue;
											}
											try {
												if(Counter.isMinuteDeleteAllow(broker)) {
//													System.out.println("LRM delete begins..1..."+new Date()+"...count.."+eremovet);
													InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
													broker.addMinuteUpdateCounter();
													eremovet++;
													cat.setStatus("DELETED");
													cat.setLastUpdated(currentTime);
													cat.setReason("Event is a non Zone Event and Deleted from TN");
												}
											} catch (Exception e) {
												cat.setReason("Event is a non Zone Event and Exception in TN");
												e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
													error.setProcess("POS Remove Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("MZP 2 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
													System.err.println("MZP 2 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
											}
										 }
									}
									 DAORegistry.getManhattanZonesPricingCategoryTicketDAO().updateAll(miniTickets);
									
								} catch (Exception e) {
									isErrorOccured = true;
									error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									 error.setMessage("Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
									 error.setExample(""+e.fillInStackTrace());
									 error.setProcess("Deleting Event Listings.");
									 error.setEventId(eventId);
									 error.setTimeStamp(new Date());
									 errorList.add(error);
									 log.error("MZP 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
									 System.err.println("MZP 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
									 e.printStackTrace();
								}
								
								continue;
							 }
							
							Map<Integer,PosEvent> posEventMap = new HashMap<Integer, PosEvent>();
							if(broker != null && isUpdateTN && isTnEvent) {
							 //if(isUpdatePOS) {
								 posEvents = posEventsAndBrokerMap.get(exBrokerId);
								if(posEvents == null || posEvents.isEmpty()){
									posEvents = BrokerUtils.getPOSEventByBrokerId(exBrokerId);
									posEventsAndBrokerMap.put(exBrokerId, posEvents);
								}
								 if(posEvents!=null){
									 for(PosEvent posEvent:posEvents){
										 posEventMap.put(posEvent.getExchangeEventId(), posEvent);
									 }
								
								 }
							 }
							if(posEventMap.isEmpty()) {
								isTnEvent = false;
							}
							
							PosEvent posEvent = posEventMap.get(event.getAdmitoneId());
							//broadway child category id for manhattan pos is 70 and it is hard coded here.
							
							if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || posEvent.getExchangeEventId() == 0 ||
									posEvent.getChildCategoryId() != 70){
								 // Remove all tickets of event if event is not in POS or not an exchange event in POS.
								if(broker != null && isUpdateTN && isTnEvent) {
									try {
										List<ManhattanZonesPricingCategoryTicket> miniTickets = DAORegistry.getManhattanZonesPricingCategoryTicketDAO().getAllTNManhattanZonesPricingCategoryTicketsByEventId(event.getId());
//										System.out.println("DELETING " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
//										log.info("DELETING " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());

										for(ManhattanZonesPricingCategoryTicket cat:miniTickets){
											
											cat.setReason("Event not Exist in TN");
											if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
												continue;
											}
											try {
												if(Counter.isMinuteDeleteAllow(broker)) {
//													System.out.println("LRM delete begins..2..."+new Date()+"...count.."+eremovet);
													InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
													broker.addMinuteUpdateCounter();
													eremovet++;
													cat.setStatus("DELETED");
													cat.setLastUpdated(currentTime);
													cat.setReason("Event not Exist in TN and Deleted from TN");
												}
											} catch (Exception e) {
												cat.setReason("Event not Exist in TN and Exception in TN");
												e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
													error.setProcess("POS Remove Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("MZP 4 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
													System.err.println("MZP 4 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
											}
										}
										DAORegistry.getManhattanZonesPricingCategoryTicketDAO().updateAll(miniTickets);

									} catch (Exception e) {
										isErrorOccured = true;
										error = new AutoPricingError();
										 error.setProductId(autopricingProduct.getId());
										 error.setMessage("Error while Deleting Event Listings for Event not Found in POS."+eventId);
										 error.setExample(""+e.fillInStackTrace());
										 error.setProcess("Deleting Event Listings.");
										 error.setEventId(eventId);
										 error.setTimeStamp(new Date());
										 errorList.add(error);
										 log.error("MZP 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
										 System.err.println("MZP 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
										 e.printStackTrace();
									}
								
								}
								isTnEvent = false;
//								continue;
							}
							
							System.out.println("ManhattanZonesPricing Event:" + i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());
//							log.info(i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());

							Date date = null;
							if(event.getLocalDate()!=null){
								if(event.getLocalTime()!=null) {
									date = dateTimeFormat.parse(df.format(event.getLocalDate()) + " " + timeDf.format(event.getLocalTime()));									
								} else {
									date = dateTimeFormat.parse(df.format(event.getLocalDate()) + " 00:00:00");
								}
							}
							//now = df.parse(df.format(now));
//							System.out.println("============" + autopricingSettings);
							Integer excludingEventHours = 6; // event exclude hours hard coded
							/*if(autopricingSettings!=null){
								excludingEventHours = autopricingSettings.getExcludeEventDays();
							}
							if(excludingEventHours==null){
								excludingEventHours = minimamExcludeEventHours ;
							}*/
							
							if(date!=null && ((date.getTime()-now.getTime()) <= excludingEventHours * 60 * 60 * 1000)) {//6 hours events
								 // Remove all tickets of event if event is within 6 hours..
								try {
									List<ManhattanZonesPricingCategoryTicket> miniTickets = DAORegistry.getManhattanZonesPricingCategoryTicketDAO().getAllManhattanZonesPricingCategoryTicketsByEventId(event.getId());
//									System.out.println("DELETING " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
//									log.info("DELETING " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
									
									if(broker != null && isUpdateTN && isTnEvent) {
										
										for(ManhattanZonesPricingCategoryTicket cat:miniTickets){
											cat.setStatus("DELETED");
											cat.setLastUpdated(currentTime);
											cat.setReason("Event within exclude eventdays");
											
											if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
												continue;
											}
											try {
												if(Counter.isMinuteDeleteAllow(broker)) {
//													System.out.println("LRM delete begins..3..."+new Date()+"...count.."+eremovet);
													InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
													broker.addMinuteUpdateCounter();
													eremovet++;
													cat.setReason("Event within exclude eventdays and Deleted from TN");
												}
											} catch (Exception e) {
												cat.setReason("Event within exclude eventdays and Exception in TN");
												e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
													error.setProcess("POS Remove Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("MZP 6 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
													System.err.println("MZP 6 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
											}
										}
									 }
									 DAORegistry.getManhattanZonesPricingCategoryTicketDAO().updateAll(miniTickets);
									
								} catch (Exception e) {
									isErrorOccured = true;
									error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									 error.setMessage("Error while Deleting Event Listings for Event within Few Days."+eventId);
									 error.setExample(""+e.fillInStackTrace());
									 error.setProcess("Deleting Event Listings.");
									 error.setEventId(eventId);
									 error.setTimeStamp(new Date());
									 errorList.add(error);
									 log.error("MZP 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
									 System.err.println("MZP 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
									 e.printStackTrace();
								}
								continue;
							}
							
							Map<String, Integer> venueConfigZoneMap = brokersVenueConfigZoneMap.get(broker.getId());
							try { 
								if(venueConfigZoneMap == null || venueConfigZoneMap.isEmpty()) {
									venueConfigZoneMap = new HashMap<String, Integer>();
									if(isTnEvent && isUpdateTN){
										venueConfigZoneMap = BrokerUtils.getPOSVenueConfigurationMap(broker);
									}
									
									brokersVenueConfigZoneMap.put(broker.getId(), venueConfigZoneMap);
								}
							} catch (Exception e) {
								isErrorOccured = true;
								error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Loading VenueConfiguration. brokerId." + broker.getId());
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("Error while Loading VenueConfiguration.");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("MZP 77 : Error while Loading VenueConfiguration. brokerId." + broker.getId()+" : eventId : "+eventId);
								 System.err.println("MZP 77 : Error while Loading VenueConfiguration. brokerId." + broker.getId()+" : eventId : "+eventId);
								 e.printStackTrace();
								 
							}
							Set<String> excludeZones = excludeEventZoneMap.get(eventId);
							if(excludeZones == null) {
								excludeZones = excludeVenueCategoryZoneMap.get(event.getVenueCategoryId());
							}
							
							// If event is valid event add , update or delete event tickets based on latest tmat tickets.
							Map<String,ManhattanZonesPricingCategoryTicket> catTixMap = new HashMap<String, ManhattanZonesPricingCategoryTicket>();
							Map<String,ManhattanZonesPricingCategoryTicket> catTixFromDB = new HashMap<String, ManhattanZonesPricingCategoryTicket>();
							Set<String> zoneswithTickets = new HashSet<String>(); 
							List<ManhattanZonesPricingCategoryTicket> updatedBrokerTicketList = new ArrayList<ManhattanZonesPricingCategoryTicket>();
							try {
								int defaultExpectedArrivialDatePriorDays = exEvent.getShippingDays();
								Integer sectionMinEtry = 1;
								DefaultAutoPricingProperties defaultAutopricing = defaultAutoPricingsMap.get(event.getParentCategoryId());
								if(defaultAutopricing != null) {
									sectionMinEtry = defaultAutopricing.getSectionCountTicket();
								}
								
								Map<String, Double> zonesPricingMap = null;
								if(broker != null) {
									zonesPricingMap = exchangeEventFloorCapMap.get(broker.getId()+":"+event.getAdmitoneId());
								}
								
								 List<CategoryTicket> catTixList = CategoryGroupManager.computeCategoryTickets(event, exEvent, defaultExpectedArrivialDatePriorDays, sectionMinEtry, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeMarkup,ticketcityMarkup,autopricingProduct,null);
								 for (CategoryTicket catTixObj : catTixList) { // Add generated tickets in map
									ManhattanZonesPricingCategoryTicket catTix = (ManhattanZonesPricingCategoryTicket) catTixObj;
									 if((catTix.getQuantity() <= 3 && catTix.getTnPrice() < 75 ) ||
											 (catTix.getQuantity() > 3 && catTix.getTnPrice() < 50)){
										 continue;
									 }
									if(catTix.getTnPrice()>=5000){
										 continue;
									 }
									if(isTnEvent && isUpdateTN){
										catTix.setPosEventId(posEvent.getId());
										catTix.setPosExchangeEventId(posEvent.getExchangeEventId());
										
										//if(!broker.getName().equalsIgnoreCase("Right This Way")) {
										
										String venueConfigZoneKey = posEvent.getVenueConfigurationId()+":"+catTixObj.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
										Integer configZoneId =venueConfigZoneMap.get(venueConfigZoneKey);
										
										if(null == configZoneId || configZoneId == -1  || configZoneId == 0 ){
											System.out.println("Venue Configuration Zone is not found.. Zone:"+catTixObj.getSection());
											continue;
										}
										catTixObj.setVenueConfigurationZoneId(configZoneId);
									}
									
									if(broker != null) {
										//Map<String, Double> zonesPricingMap = exchangeEventFloorCapMap.get(broker.getId()+":"+event.getAdmitoneId());
											
										Double floorCap = 0.0;
										if(null != zonesPricingMap && !zonesPricingMap.isEmpty()){
											floorCap = zonesPricingMap.get(catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase());
											if(null == floorCap ){
												floorCap = 0.0;
											}
										}
										//System.out.println("FC : "+floorCap+"---"+catTix.getSection()+"---"+catTix.getQuantity()+"------"+catTix.getActualPrice());
										
										if(floorCap != 0 && floorCap != 0.00){
											if(catTix.getPurPrice() > floorCap){
												double price =  Math.floor(catTix.getPurPrice()-1);
												catTix.setTnPrice(price);
												catTix.setActualPrice(price);
												
											}else if (catTix.getPurPrice() < floorCap){
												catTix.setTnPrice(floorCap);
												catTix.setActualPrice(floorCap);
											}else if (catTix.getPurPrice()== floorCap){
												
											} else {
												System.out.println("Skipped Due to FC : "+catTix.getSection()+"---"+catTix.getSection()+"---"+catTix.getQuantity()+"------"+catTix.getActualPrice());
												//log.info("Skipped Due to FC : "+catTix.getSection()+"---"+catTix.getSection()+"---"+catTix.getQuantity()+"------"+catTix.getActualPrice());
												continue;
											}
										} /*else {
											//if event do not have floor cap then we have to add markup only
											if(broker.getName().equalsIgnoreCase("Reserve One Tickets")) {
												double price = Math.ceil(catTix.getPurPrice() * (1+(ROT_DEFAULT_NO_FLOORCAP_MARKUP/100)));
												catTix.setTnPrice(price);
												catTix.setActualPrice(price);
											} else if(broker.getName().equalsIgnoreCase("Joe Knows Tickets")) {
												continue;
											}
										}*/
										zoneswithTickets.add(catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase());
										catTix.setFloorCap(floorCap);
									}
									
									//update user assigned shipping method
									/*if(exEvent.getShippingMethod() != null) {
										catTix.setShippingMethodSpecialId(exEvent.getShippingMethod());
									}*/
									
									//Tamil : hotcoding shippingMethod as Local Pickup Near venue, near term display option s  Show Only Near Term display option
									catTix.setShippingMethodSpecialId(3);
									catTix.setNearTermOptionId(2);
									
									String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
									catTixMap.put(key, catTix);
								 }
								
								 if(broker != null ) {
									 //Map<String, Double> zonesPricingMap = exchangeEventFloorCapMap.get(broker.getId()+":"+event.getAdmitoneId());
									 Map<String, String> zoneFloorCapSectionMap = exchangeEventFloorCapSectionMap.get(broker.getId()+":"+event.getAdmitoneId());
										Double floorCap = 0.0;
										if(null != zonesPricingMap && !zonesPricingMap.isEmpty()){

											for (String fcZone : zonesPricingMap.keySet()) {
												floorCap = zonesPricingMap.get(fcZone);
												if(floorCap == null || floorCap <= 0 || floorCap >= 5000 || zoneswithTickets.contains(fcZone)){
													continue;
												}
												Date expectedArrivalDate = null;
												if(event.getLocalDate()!=null) {
													Calendar calOne = Calendar.getInstance();
													calOne.setTime(event.getLocalDate());

													//Tamil : HardCoding expected arrival date as eventdate
													//calOne.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
													expectedArrivalDate = calOne.getTime();
												}
												String section = zoneFloorCapSectionMap.get(fcZone);
												
												ManhattanZonesPricingCategoryTicket catTicket = new ManhattanZonesPricingCategoryTicket(section, floorCap, event);
												catTicket.setTnExchangeEventId(event.getAdmitoneId());
												//catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
												//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
												catTicket.setPosEventId(posEvent.getId());
												catTicket.setPosExchangeEventId(posEvent.getExchangeEventId());
												catTicket.setExpectedArrivalDate(expectedArrivalDate);
												//catTicket.setQuantity(8);
												
												//Tamil : if zone is premium we have to process only 2 qty tickets else 4 qty tickets
												if(section.contains("premium")) {
													catTicket.setQuantity(2);
												} else {
													catTicket.setQuantity(4);
												}
												 if((catTicket.getQuantity() <= 3 && catTicket.getTnPrice() < 75 ) ||
														 (catTicket.getQuantity() > 3 && catTicket.getTnPrice() < 50)){
													 continue;
												 }
												catTicket.setPosEventId(posEvent.getId());
												catTicket.setPosExchangeEventId(posEvent.getExchangeEventId());
												
												//if(!broker.getName().equalsIgnoreCase("Right This Way")) {
												if(isTnEvent && isUpdateTN) {
													//Map<String, Integer> venueConfigZoneMap = brokersVenueConfigZoneMap.get(broker.getId());
													String venueConfigZoneKey = posEvent.getVenueConfigurationId()+":"+catTicket.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
													Integer configZoneId =venueConfigZoneMap.get(venueConfigZoneKey);
													
													if(null == configZoneId || configZoneId == -1  || configZoneId == 0 ){
														System.out.println("Venue Configuration Zone is not found.. Zone:"+catTicket.getSection());
														continue;
													}
													catTicket.setVenueConfigurationZoneId(configZoneId);
												}
												
												//update user assigned shipping method
												/*if(exEvent.getShippingMethod() != null) {
													catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
												}*/
												
												//Tamil : hotcoding shippingMethod as Local Pickup Near venue, near term display option s  Show Only Near Term display option
												catTicket.setShippingMethodSpecialId(3);
												catTicket.setNearTermOptionId(2);
												
												String key = catTicket.getQuantity()+":"+ catTicket.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
												catTixMap.put(key, catTicket);
											}
										}
								 }
								 
								 
								 List<ManhattanZonesPricingCategoryTicket> miniTickets = DAORegistry.getManhattanZonesPricingCategoryTicketDAO().getAllManhattanZonesPricingCategoryTicketsByEventId(event.getId());
								 for (ManhattanZonesPricingCategoryTicket dbCatTix : miniTickets) {// Add Existing tickets in map.]
									 if(isTnEvent && isUpdateTN){
										 dbCatTix.setPosVenueId(posEvent.getVenueId());
										 dbCatTix.setPosEventId(posEvent.getId());
										 dbCatTix.setPosExchangeEventId(posEvent.getExchangeEventId());
										 
										 if(dbCatTix.getTnExchangeEventId() != null && !dbCatTix.getTnExchangeEventId().equals(event.getAdmitoneId())) {
											 dbCatTix.setStatus("DELETED");
											 dbCatTix.setLastUpdated(currentTime);
											 dbCatTix.setReason("POS Event Id Mismatching");
											 updatedBrokerTicketList.add(dbCatTix);
											 continue;
										 }
										 
										 if(dbCatTix.getTnCategoryTicketGroupId() != null) {
											 if(excludeZones != null && broker != null && dbCatTix.getTmatZone() != null) {
												 String exZoneKey = broker.getId()+"_"+dbCatTix.getTmatZone().toUpperCase(); 
												 if(excludeZones.contains(exZoneKey)) {
													 dbCatTix.setStatus("DELETED");
													 dbCatTix.setLastUpdated(currentTime);
													 dbCatTix.setReason("Excluded Zone ticket");
													 updatedBrokerTicketList.add(dbCatTix);
													 continue;
												 }
											 }
											 /*if(dbCatTix.getQuantity()==2 && dbCatTix.getTnPrice()<50){
												 dbCatTix.setStatus("DELETED");
												 dbCatTix.setReason("Price below $50 and Deleted from TN");
												 updatedBrokerTicketList.add(dbCatTix);
												 continue;
											 }*/
											 if(dbCatTix.getTnBrokerId()!=null) {
												 if(!dbCatTix.getTnBrokerId().equals(exEvent.getTicketNetworkBrokerId())){
													 dbCatTix.setStatus("DELETED");
													 dbCatTix.setReason("Ticket TN Broker mismatching with event TN broker");
													 updatedBrokerTicketList.add(dbCatTix);
													 continue;
												 }
											 } else {
												 dbCatTix.setTnBrokerId(exEvent.getTicketNetworkBrokerId());
											 }
										 }
									 }
									 String key = dbCatTix.getQuantity()+":"+ dbCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
									 catTixFromDB.put(key, dbCatTix);
								 }
								 
							} catch (Exception e) {
								isErrorOccured = true;
								error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Computing Category Tickets."+eventId);
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("Category Ticket Computation.");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("MZP 8 : Error while Computing Category Tickets."+eventId);
								 System.err.println("MZP 8 : Error while Computing Category Tickets."+eventId);
								 e.printStackTrace();
								 continue;
							}
							List<ManhattanZonesPricingCategoryTicket> tnNewCatTix = new ArrayList<ManhattanZonesPricingCategoryTicket>();
							List<ManhattanZonesPricingCategoryTicket> tnUpdateCatTix = new ArrayList<ManhattanZonesPricingCategoryTicket>();
							List<ManhattanZonesPricingCategoryTicket> tnRemoveCatTix = new ArrayList<ManhattanZonesPricingCategoryTicket>();
							Map<String,ManhattanZonesPricingCategoryTicket> exFinalCatTixMap = new HashMap<String, ManhattanZonesPricingCategoryTicket>();
							
							try {
								 String priceHistory="";
								 String ticketIdHistory= "";
								 String baseTicketOneHistory= "";
								 String baseTicketTwoHistory="";
								 String baseTicketThreeHistory="";
								 
								String ticketHDateStr = dateTimeFormat.format(new Date());
								String priceHDateStr = dateTimeFormat.format(new Date());
								List<String> keys = new ArrayList<String>(catTixFromDB.keySet());
								Integer baseTicket1,baseTicket2,baseTicket3;
								Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
								//Integer purchaseOrderId = null;
								//boolean existingPurchaseOrder = false;
								
								for(String key:keys) {
									ManhattanZonesPricingCategoryTicket dbMan =catTixFromDB.remove(key);
									ManhattanZonesPricingCategoryTicket man = catTixMap.remove(key);
									isPriceChangeFlag = false;
									
									if(man != null) { // if generated tickets is already existing..
										if(!dbMan.getActualPrice().equals(man.getActualPrice())) {
											isPriceChangeFlag = true;
											priceHistory = priceHDateStr+"-"+man.getActualPrice();
											if(dbMan.getPriceHistory() != null) {
												priceHistory = dbMan.getPriceHistory() +","+priceHistory;
												if(priceHistory.length()>=500) {
													priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
												}
											}
											dbMan.setPriceHistory(priceHistory);
											dbMan.setIsEdited(true);
											
										} else if(//!dbTix.getVividPrice().equals(tix.getVividPrice()) ||
//													 !dbTix.getTickpickPrice().equals(tix.getTickpickPrice()) ||
												//!dbTix.getScoreBigPrice().equals(tix.getScoreBigPrice()) || 
												//!dbTix.getFanxchangePrice().equals(tix.getFanxchangePrice()) ||
												//!dbTix.getTicketcityPrice().equals(tix.getTicketcityPrice()) || 
												!dbMan.getTicketId().equals(man.getTicketId())) { 
											
											dbMan.setIsEdited(true);
											
										} else {
											baseTicket1 = 0;
											baseTicket2 = 0;
											baseTicket3 = 0;
											
											if(dbMan.getBaseTicketOne() != null) {
												baseTicket1 = dbMan.getBaseTicketOne();
											}
											if(dbMan.getBaseTicketTwo() != null) {
												baseTicket2 = dbMan.getBaseTicketTwo();
											}
											if(dbMan.getBaseTicketThree() != null) {
												baseTicket3 = dbMan.getBaseTicketThree();
											}
											
											if(!man.getBaseTicketOne().equals(baseTicket1) || !man.getBaseTicketTwo().equals(baseTicket2) || !man.getBaseTicketThree().equals(baseTicket3)) {
												dbMan.setIsEdited(true);
												
											}
										}
										
										//add the pricehistory of basetickets prices here for all tickets
										
										tempTicketIdFlag = false;
										if(isPriceChangeFlag || !dbMan.getTicketId().equals(man.getTicketId())) {
											ticketIdHistory = ticketHDateStr +"/"+ man.getTicketId() +"/"+ man.getPurPrice();
											
											if(dbMan.getTicketIdHistory() != null) {
												ticketIdHistory = dbMan.getTicketIdHistory()+","+ ticketIdHistory;
												if(ticketIdHistory.length()>= 500) {
													ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
												}
											}
											dbMan.setTicketIdHistory(ticketIdHistory);
											dbMan.setIsEdited(true);
										} 
										
										// For Base Ticket 1
										tempTicketIdFlag = false;
										if (dbMan.getBaseTicketOne() != null && dbMan.getBaseTicketOne() != 0) {
											if(isPriceChangeFlag || !dbMan.getBaseTicketOne().equals(man.getBaseTicketOne())) {
												tempTicketIdFlag = true;
											}
										} else if(man.getBaseTicketOne() != null && man.getBaseTicketOne() != 0) {
											tempTicketIdFlag = true;
										}
										
										if(tempTicketIdFlag) {
											baseTicketOneHistory = ticketHDateStr +"/"+ man.getBaseTicketOne() +"/"+ man.getBaseTicketOnePurPrice();
											
											if(dbMan.getBaseTicketOneHistory() != null) {
												baseTicketOneHistory = dbMan.getBaseTicketOneHistory()+","+ baseTicketOneHistory;
																		
												if(baseTicketOneHistory.length()>= 500) {
													baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
												}
											}
											dbMan.setBaseTicketOneHistory(baseTicketOneHistory);
											dbMan.setIsEdited(true);
										}
										
										// For Base Ticket 2
										tempTicketIdFlag = false;
										if (dbMan.getBaseTicketTwo() != null && dbMan.getBaseTicketTwo() != 0) {
											if(isPriceChangeFlag || !dbMan.getBaseTicketTwo().equals(man.getBaseTicketTwo())){
												tempTicketIdFlag = true;
											}
										} else if(man.getBaseTicketTwo() != null && man.getBaseTicketTwo() != 0) {
											tempTicketIdFlag = true;
										}
										
										if(tempTicketIdFlag) {
											baseTicketTwoHistory = ticketHDateStr +"/"+ man.getBaseTicketTwo() +"/"+ man.getBaseTicketTwoPurPrice();
											if(dbMan.getBaseTicketTwoHistory() != null) {
												baseTicketTwoHistory = dbMan.getBaseTicketTwoHistory()+","+ baseTicketTwoHistory;
																		
												if(baseTicketTwoHistory.length()>= 500) {
													baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
												}
											}
											dbMan.setBaseTicketTwoHistory(baseTicketTwoHistory);
											dbMan.setIsEdited(true);
										}
										// For Base Ticket 3
										tempTicketIdFlag = false;
										if (dbMan.getBaseTicketThree() != null && dbMan.getBaseTicketThree() != 0) {
											if(isPriceChangeFlag || !dbMan.getBaseTicketThree().equals(man.getBaseTicketThree()))  {
												tempTicketIdFlag = true;
											}
										} else if(man.getBaseTicketThree() != null && man.getBaseTicketThree() != 0) {
											tempTicketIdFlag = true;
										}
										
										if(tempTicketIdFlag) {
											baseTicketThreeHistory = ticketHDateStr +"/"+ man.getBaseTicketThree() +"/"+ man.getBaseTicketThreePurPrice();
											
											if(dbMan.getBaseTicketThreeHistory() != null) {
												baseTicketThreeHistory = dbMan.getBaseTicketThreeHistory()+","+ baseTicketThreeHistory;
																		
												if(baseTicketThreeHistory.length()>= 500) {
													baseTicketThreeHistory = baseTicketThreeHistory.substring(baseTicketThreeHistory.indexOf(",")+1);	
												}
											}
											dbMan.setBaseTicketThreeHistory(baseTicketThreeHistory);
											dbMan.setIsEdited(true);
										}
										
										if(broker != null && isUpdateTN && isTnEvent) {
											if(dbMan.getTnCategoryTicketGroupId() == null || dbMan.getTnCategoryTicketGroupId() == 0) { // Add tickets as new tickets if it is not exist in POS (i.e. tn group id is not present)
												//dbMan.setShippingMethodSpecialId(man.getShippingMethodSpecialId());
												dbMan.setNearTermOptionId(man.getNearTermOptionId());
												dbMan.setExpectedArrivalDate(dbMan.getExpectedArrivalDate());
												dbMan.setIsEdited(true);
												
												tnNewCatTix.add(dbMan);
												
											} else if(!dbMan.getTnPrice().equals(man.getTnPrice()) || // Else change it in pos if price or other details has been changed.
													!dbMan.getShippingMethodSpecialId().equals(man.getShippingMethodSpecialId()) ||
													!dbMan.getNearTermOptionId().equals(man.getNearTermOptionId()) ||
													dbMan.getExpectedArrivalDate().compareTo(man.getExpectedArrivalDate()) != 0) {
												
												dbMan.setTnPrice(man.getTnPrice());
												//dbMan.setShippingMethodSpecialId(man.getShippingMethodSpecialId());
												dbMan.setNearTermOptionId(man.getNearTermOptionId());
												dbMan.setExpectedArrivalDate(dbMan.getExpectedArrivalDate());
												
//													dbTix.setIsPosUpdate(true);
												dbMan.setIsEdited(true);
												
												tnUpdateCatTix.add(dbMan);
											}
										}
										
										if(!dbMan.getShippingMethodSpecialId().equals(man.getShippingMethodSpecialId())){
											dbMan.setShippingMethodSpecialId(man.getShippingMethodSpecialId());
											dbMan.setIsEdited(true);
										}
										
										dbMan.setBaseTicketOne(man.getBaseTicketOne());
										dbMan.setBaseTicketTwo(man.getBaseTicketTwo());
										dbMan.setBaseTicketThree(man.getBaseTicketThree());
										
										dbMan.setCategoryId(man.getCategoryId());
										dbMan.setTicketId(man.getTicketId());
										dbMan.setItemId(man.getItemId());
										dbMan.setVenueConfigurationZoneId(man.getVenueConfigurationZoneId());
										dbMan.setFloorCap(man.getFloorCap());
										
										dbMan.setActualPrice(man.getActualPrice());
										dbMan.setVividPrice(man.getVividPrice());
//											dbTix.setTickpickPrice(tix.getTickpickPrice());
										dbMan.setScoreBigPrice(man.getScoreBigPrice());
										dbMan.setFanxchangePrice(man.getFanxchangePrice());
										dbMan.setTicketcityPrice(man.getTicketcityPrice());
										dbMan.setTmatZone(man.getTmatZone());
										dbMan.setTnExchangeEventId(man.getTnExchangeEventId());
										
										if(dbMan.getIsEdited()){
											dbMan.setLastUpdated(currentTime);
											exFinalCatTixMap.put(key, dbMan);
										}
											
									} else { // else delete existing ticket as it does not exist in newly generated tickets..
//											dbTix.setStatus(DE)
										tnRemoveCatTix.add(dbMan);
										
										dbMan.setStatus("DELETED");
										dbMan.setLastUpdated(currentTime);
										dbMan.setReason("Ticket not exist in TMAT");
										exFinalCatTixMap.put(key, dbMan);
									}
								}
								
								
							
								for(String key:catTixMap.keySet()) {  // Add remaining newly generated tickets to db.
									 ManhattanZonesPricingCategoryTicket newTix = catTixMap.get(key);
									 priceHistory = priceHDateStr+"-"+newTix.getActualPrice();
									newTix.setPriceHistory(priceHistory);
									
									ticketIdHistory = ticketHDateStr +"/"+ newTix.getTicketId() +"/"+ newTix.getPurPrice();
									newTix.setTicketIdHistory(ticketIdHistory);
									
									if (newTix.getBaseTicketOne() != 0) {
										baseTicketOneHistory = ticketHDateStr +"/"+ newTix.getBaseTicketOne() +"/"+ newTix.getBaseTicketOnePurPrice();
										newTix.setBaseTicketOneHistory(baseTicketOneHistory);
									}
									
									// For Base Ticket 2
									if (newTix.getBaseTicketTwo() != 0) {
										baseTicketTwoHistory = ticketHDateStr +"/"+ newTix.getBaseTicketTwo() +"/"+ newTix.getBaseTicketTwoPurPrice();
										newTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
									}
									// For Base Ticket 3
									if (newTix.getBaseTicketThree() != 0) {
										baseTicketThreeHistory = ticketHDateStr +"/"+ newTix.getBaseTicketThree() +"/"+ newTix.getBaseTicketThreePurPrice();
										newTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
									}
									
									newTix.setLastUpdated(currentTime);
									newTix.setCreatedDate(currentTime);
									exFinalCatTixMap.put(key, newTix);
									
									if(broker != null && isUpdateTN && isTnEvent) {
										tnNewCatTix.add(newTix);
									}
								}
								 
								 if(broker != null && isUpdateTN && isTnEvent) {
									 
									 int addCount=0;
									 //boolean isCreateListingEnabled = Counter.isCreateListingEnabled(broker);
									 if(!tnNewCatTix.isEmpty()) {// && isCreateListingEnabled
										 Map<String, Integer> posVenueCatIdMap = new HashMap<String, Integer>();
										 try {
											 List<PosVenueCategory> posVenueCatList = InduxDAORegistry.getPosVenueCategoryDAO().getAllVenueCategorysByVenueId(posEvent.getVenueId(),broker.getPosBrokerId());
											 for (PosVenueCategory posVenueCategory : posVenueCatList) {
														
												 //Tamil : select only venue configuration zone id mapped venue configurations for zones pricing listings
												 if(posVenueCategory.getVenueConfigZoneId() != null && posVenueCategory.getVenueConfigZoneId() > 0) {
													 String key = posVenueCategory.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
													 posVenueCatIdMap.put(key, posVenueCategory.getId());	
												 }
											 }
											 //System.out.println("venue cat size..."+posVenueCatIdMap.size());
										 } catch (Exception e) {
											 e.printStackTrace();
											 isErrorOccured = true;
											 error = new AutoPricingError();
											 error.setProductId(autopricingProduct.getId());
											 error.setExample(""+e.fillInStackTrace());
											 error.setMessage("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ": While loading POS VenueCategors :ErrorMsg:"  );
											 error.setProcess("Loading POS VenueCategors.");
											 error.setEventId(event.getId());
											 error.setTimeStamp(new Date());
											 errorList.add(error);
											 System.err.println("MZP 9 : Error From While Loading POS VenueCategors.."+eventId);
										 }
										 for (ManhattanZonesPricingCategoryTicket tix : tnNewCatTix) {
											 /*if(tix.getQuantity() == 2 && tix.getTnPrice()<50){
												 continue;
											 }*/
											 if(excludeZones != null && broker != null && tix.getTmatZone() != null) {
												 String exZoneKey = broker.getId()+"_"+tix.getTmatZone().toUpperCase(); 
												 if(excludeZones.contains(exZoneKey)) {
													 continue;
												 }
											 }
												 
											tix.setPosEventId(posEvent.getId());
											tix.setPosVenueId(posEvent.getVenueId());
											tix.setPosExchangeEventId(posEvent.getExchangeEventId());
											
											Integer posVenueCategoryId = null;
											String venueCategorykey = tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
											posVenueCategoryId = posVenueCatIdMap.get(venueCategorykey);
											if(null == posVenueCategoryId || posVenueCategoryId == -1) {
												posVenueCategoryId = InduxDAORegistry.getPosVenueCategoryDAO().save(tix,broker.getPosBrokerId(),autopricingProduct);
												posVenueCatIdMap.put(venueCategorykey, posVenueCategoryId);
//													System.out.println("new venue cat id.."+posVenueCategoryId);
											}
											if(null == posVenueCategoryId || posVenueCategoryId == -1) {
												isErrorOccured = true;
												error = new AutoPricingError();
												 error.setProductId(autopricingProduct.getId());
												error.setMessage("Error While Creating VenueCategory");
												error.setExample("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
												error.setProcess("ADD Venuecategory.");
												error.setEventId(event.getId());
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("MZP 10 :  Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
												System.err.println("MZP 10 :  Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
												continue;
											}
											tix.setPosVenueCategoryId(posVenueCategoryId);
											 
											 tix.setTnBrokerId(exEvent.getTicketNetworkBrokerId());
											 if(Counter.isMinuteAddAllow(broker)){
	//											 System.out.println("LRM ADD begins....."+new Date()+"...count.."+eInsert);
												try {
													Integer tnCategoryTicketGroupId = InduxDAORegistry.getPosCategoryTicketGroupDAO().save(autopricingProduct,tix,broker.getPosBrokerId());
													//System.out.println("tnCategoryTicketGroupId "+tnCategoryTicketGroupId);
													if(tnCategoryTicketGroupId!=-1l){
														tix.setTnCategoryTicketGroupId(tnCategoryTicketGroupId);
														broker.addMinuteUpdateCounter();
														addCount++;
														eInsert++;
													}
												 } catch(Exception e) {
													 isErrorOccured = true;
													 error = new AutoPricingError();
													 error.setProductId(autopricingProduct.getId());
													 error.setMessage("Error while creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
													 error.setExample(""+e.fillInStackTrace());
													 error.setProcess("Ticket Group Creation..");
													 error.setEventId(eventId);
													 error.setTimeStamp(new Date());
													 errorList.add(error);
													 log.error("TZP 11 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
													 System.err.println("TZP 11 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
													 e.printStackTrace();
												 }
											 }
										 }
									 }
									 
									 for(ManhattanZonesPricingCategoryTicket cat:tnUpdateCatTix){
												
										 try {
											 
											 if(Counter.isMinuteUpdateAllow(broker)) {
//												System.out.println("LRM Update begins....."+new Date()+"...count.."+eUpdate);
												 InduxDAORegistry.getPosCategoryTicketGroupDAO().updateCategoryTicketGroup(cat,broker.getPosBrokerId());
												 broker.addMinuteUpdateCounter();
												 eUpdate++;
											 }
										} catch (Exception e) {
											e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Updating Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
												error.setProcess("POS Update Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("MZP 12 : Error While POS Update:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
												System.err.println("MZP 12 :  Error From POS Update :"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
										}
									 }
									 
									 for(ManhattanZonesPricingCategoryTicket cat:tnRemoveCatTix){
												
										 try {
											 if(cat.getTnCategoryTicketGroupId()!=null){	
												 if(Counter.isMinuteDeleteAllow(broker)) {
//													 System.out.println("LRM delete begins..4..."+new Date()+"...count.."+eremovet);
												
													 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
													 broker.addMinuteUpdateCounter();
													 eremovet++;
													 cat.setReason("Ticket not exist in TMAT and Deleted from TN");
												}
											 }
										 } catch (Exception e) {
											 cat.setReason("Ticket not exist in TMAT and Exception in TN");
											 e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
												error.setProcess("POS Remove Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("MZP 13 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
												System.err.println("MZP 13 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
										 }
									 }
									 for(ManhattanZonesPricingCategoryTicket cat:updatedBrokerTicketList){
											
										 try {
											 if(cat.getTnCategoryTicketGroupId()!=null){	
												 if(Counter.isMinuteDeleteAllow(broker)) {
//													 System.out.println("LRM delete begins..4..."+new Date()+"...count.."+eremovet);
												
													 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
													 broker.addMinuteUpdateCounter();
													 eremovet++;
													 cat.setReason(cat.getReason()+" and Deleted from TN");
												}
											 }
										 } catch (Exception e) {
											 cat.setReason(cat.getReason()+" and Exception in TN");
											 e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
												error.setProcess("POS Remove Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("MZP 13 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
												System.err.println("MZP 13 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
										 }
									 }
									 String key = null;
									 ManhattanZonesPricingCategoryTicket catTix = null;
									 for (ManhattanZonesPricingCategoryTicket tnCatTix :tnNewCatTix) {
										key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
										catTix = exFinalCatTixMap.get(key);
										 if(catTix != null) {
											 catTix.setCreatedDate(currentTime);
											 catTix.setTnCategoryTicketGroupId(tnCatTix.getTnCategoryTicketGroupId());
//											 catTix.setPosPurchaseOrderId(tnCatTix.getPosPurchaseOrderId());
											 catTix.setTnPrice(tnCatTix.getTnPrice());
											 catTix.setIsEdited(true);
											 catTix.setLastUpdated(currentTime);
										 }
										 
									 }
									 
									 for (ManhattanZonesPricingCategoryTicket tnCatTix : tnUpdateCatTix) {
										 key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
										 catTix = exFinalCatTixMap.get(key);
											
										 if(catTix != null) {
											 catTix.setTnPrice(tnCatTix.getTnPrice());
											 catTix.setIsEdited(true);
											 catTix.setLastUpdated(currentTime);
										 }
										 
									 }
									 
									 for (ManhattanZonesPricingCategoryTicket tnCatTix : tnRemoveCatTix) {
										 key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
										 catTix = exFinalCatTixMap.get(key);
											
										 if(catTix != null) {
											 catTix.setStatus("DELETED");
//											 tnCatTix.setTnPrice(0.0);
											 catTix.setIsEdited(true);
											 catTix.setLastUpdated(currentTime);
										 }
										 
									 }
								 }
								 DAORegistry.getManhattanZonesPricingCategoryTicketDAO().saveOrUpdateAll(updatedBrokerTicketList);
								 DAORegistry.getManhattanZonesPricingCategoryTicketDAO().saveOrUpdateAll(new ArrayList<ManhattanZonesPricingCategoryTicket>(exFinalCatTixMap.values()));
								 
								 tInsert += eInsert;
								 tUpdate += eUpdate;
								 tremovet += eremovet;
								 
								 if(broker!=null){
									 Integer addCount = brokerAddCountMap.get(broker.getId());
									 if(addCount==null){
										 addCount = 0;
									 }
									 addCount= addCount + eInsert;
									 brokerAddCountMap.put(broker.getId(),addCount);
									 
									 Integer deleteCount = brokerDeleteCountMap.get(broker.getId());
									 if(deleteCount==null){
										 deleteCount = 0;
									 }
									 deleteCount= deleteCount + eremovet;
									 brokerDeleteCountMap.put(broker.getId(),deleteCount);
									 
									 Integer updateCount = brokerUpdateCountMap.get(broker.getId());	 
									 if(updateCount==null){
										 updateCount = 0;
									 }
									 updateCount= updateCount + eUpdate;
									 brokerUpdateCountMap.put(broker.getId(),updateCount);
									
								 }
								 
								 
								  
								 //System.out.println(eventId+"...: Insert :"+insert+" Updat : "+update+" : tremovet :"+remove);
								 //log.info(eventId+"...: Insert :"+insert+" Updat : "+update+" : ZeroUpdate :"+zeroUpdate+" ntExist :"+notExisit);
								 System.out.println("ManhattanZonesPricing  : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
//								 log.info("ManhattanZonesPricing : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
								 
								 
							 } catch (Exception e) {
								 isErrorOccured = true;
								 error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Updating TMAT Larry Ticket."+eventId);
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("ManhattanZonesPricing Ticket Updation..");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("MZP 14 : Error while Updating TMAT Larry Ticket."+eventId);
								 System.err.println("MZP 14 : Error while Updating TMAT Larry Ticket."+eventId);
								 e.printStackTrace();
							}
				 }// end of exchange event for loop
				 
				 List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
					
					for(Integer brokerId:brokerAddCountMap.keySet()){
						int count = brokerAddCountMap.get(brokerId);
						if(count==0){
							continue;
						}
						AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
						Broker broker = BrokerUtils.getBrokerById(brokerId);
						addAudit.setProduct(autopricingProduct);
						addAudit.setBroker(broker);
						addAudit.setCount(count);
						addAudit.setProcessType("Insert");
						addAudit.setLastRunTime(getLastUpdateTime());
						autoCatsProjectAuditList.add(addAudit);
						
					}
					
					for(Integer brokerId:brokerUpdateCountMap.keySet()){
						int count = brokerUpdateCountMap.get(brokerId);
						if(count==0){
							continue;
						}
						AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
						Broker broker = BrokerUtils.getBrokerById(brokerId);
						addAudit.setProduct(autopricingProduct);
						addAudit.setBroker(broker);
						addAudit.setCount(count);
						addAudit.setProcessType("Update");
						addAudit.setLastRunTime(getLastUpdateTime());
						autoCatsProjectAuditList.add(addAudit);
						
					}
					
					for(Integer brokerId:brokerDeleteCountMap.keySet()){
						int count = brokerDeleteCountMap.get(brokerId);
						if(count==0){
							continue;
						}
						AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
						Broker broker = BrokerUtils.getBrokerById(brokerId);
						addAudit.setProduct(autopricingProduct);
						addAudit.setBroker(broker);
						addAudit.setCount(count);
						addAudit.setProcessType("Delete");
						addAudit.setLastRunTime(getLastUpdateTime());
						autoCatsProjectAuditList.add(addAudit);
						
					}
					
					DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);
					DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
					System.out.println("ManhattanZonesPricing Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
//					log.info("ManhattanZonesPricing Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
					running = false; 
			}catch(Exception e){
				 running = false;
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Common Properties.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Loading common Properties..");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("MZP 15 : Error while Loading Common Properties.");
				 System.err.println("MZP 15 : Error while Loading Common Properties.");
				 e.printStackTrace();
			 }
			
			String subject,fileName;
			 Map<String, Object> map;
				
				if(isErrorOccured){
					map = new HashMap<String, Object>();
					map.put("error", error);
					map.put("errors", errorList);
					try {
						log.info("CT error size......"+errorList.size());
						
						DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
						subject = "ManhattanZonesPricing Scheduler job failed :";
						fileName = "templates/autopricing-job-failure-message.txt";
						
						//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
						
						EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
						
					} catch (Exception e) {
						log.error("MZP 16 : Error while Inserting Error Listings in TMAT.");
						 System.err.println("MZP 16 : Error while Inserting Error Listings in TMAT.");
						e.printStackTrace();
					}
				}
				 
	 }
	 
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
//		if(!running){
//			running=true;
			System.out.println("MHZonePricing Job Called.." + new Date() + ": " + running);
			log.info(" MHZonePricing Job Called.." + new Date() + ": " + running);
			try{
				
				processManhattanZonesPricingTickets();
				System.out.println("Job finished @ " + new Date());
			}catch(Exception e){
				e.printStackTrace();
			}
//			running = false;
//		}
	}
	
	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		ManhattanZonePricingScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		ManhattanZonePricingScheduler.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MHZonePricing").getLastRunTime();
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		ManhattanZonePricingScheduler.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}


	public static void setNextRunTime(Date nextRunTime) {
		ManhattanZonePricingScheduler.nextRunTime = nextRunTime;
	}
}
