package com.rtw.autopricing.util;

import java.util.ArrayList;
import java.util.List;

public class StringUtil {
	
	public static String[] removeFromArray(String[] arr,String toBeRemove){
		List<String> result = new ArrayList<String>();
		for(String str:arr){
			if(!str.equals(toBeRemove)){
				result.add(str);
			}
		}
		String[] finalResult = new String[result.size()];
		int i=0;
		for(String res:result){
			finalResult[i]=res;
			i++;
		}
		return finalResult;
	}
	
	
	public static void main(String[] args) {
		String[] test ={"1","2","3","4","5"};
		System.out.println(removeFromArray(test, "3"));
	}

}
