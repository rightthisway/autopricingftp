package com.rtw.autopricing.util;

//import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.data.TNDRealCategoryTicket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;


public class TNDRealTixScheduler  extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(TNDRealTixScheduler.class);
	
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 
	 static Integer hourstoConsiderOrders = 24;
	 static	Double markupPercentage = 30.0;
	 static	Integer excludeEventDays = 7;
	 static	Integer shippingMethodId = 0;//Default Website Setting
	 static	Integer nearTermDisplayOptionId = 0;//Standard delivery until near-term
	 static Integer expectedArrivalDaysBeforeEvent = 2;
		
	 public static void processTNDRealTickets() throws Exception {
		Calendar cal =  Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.MINUTE,60);
		setNextRunTime(cal.getTime());
		if(isStopped()){//|| isRunning()
			setRunning(false);
			return ;
		}
		//setRunning(true);
		
		
		Date lastRunTime = getLastUpdateTime();
		Long minute = 0l;
		if(lastRunTime!=null){
			minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
		}else{
			minute = 2880l; // 2 Days in minute
		}
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNDRealTix");
		if(autopricingProduct==null){
			autopricingProduct = new AutopricingProduct();
			autopricingProduct.setName("TNDRealTix");
			autopricingProduct.setStatus("ACTIVE");
			autopricingProduct.setStopped(false);
		}
		if(autopricingProduct.getStopped()){
			setStopped(true);
			setRunning(false);
			return;
		}
		
		setLastUpdateTime(now);
		autopricingProduct.setLastRunTime(now);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
//		Date jobStartTime = new Date();
		int tInsert=0,tUpdate=0,tremovet=0,tposCount=0;
		int eInsert=0,eUpdate=0,eremovet=0;
		
		 try {
			 //Map<Integer, Integer> brokerAddCountMap = new HashMap<Integer, Integer>();
			// Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
			 //Map<Integer, Integer> brokerUpdateCountMap = new HashMap<Integer, Integer>();
			 
				
			// Boolean isTnEvent = false;
			 
			 //Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
			 List<PosEvent> posEvents = new ArrayList<PosEvent>();
			 Map<Integer, List<PosEvent>> posEventsAndBrokerMap = new HashMap<Integer, List<PosEvent>> ();
			 Collection<Event> eventList = null;
			// Collection<MiniExchangeEvent> miniExchangeEvents = null;
			
			 Broker broker = BrokerUtils.getBrokerById(5);//RTW
			 
			 try {
				 
				 eventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentId();
				 for(Event event:eventList) {
					//eventMap.put(event.getId(), event);
					eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
				 }
				 System.out.println("TND Real : TMAT event size : "+eventList.size());
//				 log.info("NFES : TMAT event size : "+eventList.size());
				 
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("TND Real 1 : Error while Loading Events.");
				 System.err.println("TND Real 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			 
				
			 try {
				 Map<Integer,Map<String,TNDRealCategoryTicket>> eventsTixMap = new HashMap<Integer, Map<String,TNDRealCategoryTicket>>();

				 List<TNDRealCategoryTicket> catsList = DAORegistry.getQueryManagerDAO().getAllTNDRealTixElibibleOrders(hourstoConsiderOrders,excludeEventDays);
				 for (TNDRealCategoryTicket catTix : catsList) {

					 if(catTix.getSection().contains("-") || catTix.getSection().toLowerCase().contains(" or ") ||
							 catTix.getLastRow().contains("-") || catTix.getLastRow().toLowerCase().contains(" or ") ||
							 catTix.getSection().toLowerCase().contains("package") || catTix.getLastRow().toLowerCase().contains("package") ||
							 catTix.getSection().toLowerCase().contains("parking") || catTix.getLastRow().toLowerCase().contains("parking") ||
							 catTix.getSection().toLowerCase().contains("pass") || catTix.getLastRow().toLowerCase().contains("pass") ||
							 catTix.getLastRow().toLowerCase().contains("*")|| catTix.getLastRow().toLowerCase().equals(".")) {
						 continue;
					 }
					 if(catTix.getTnPrice()<50 || catTix.getQuantity()< 2) {
						 continue;
					 }
					 
					 Double price = (double) Math.ceil(((catTix.getTnPrice()*(1+(markupPercentage)/100))) * 100 / 100);
					 catTix.setTnPrice(price);
					 catTix.setActualPrice(price);
					 
					 String lastRow = Categorizer.getTNDZonesLastRow(catTix.getLastRow());
					 if(lastRow == null) {
						 continue;
					 }
					 catTix.setLastRow(lastRow.toUpperCase());
					 
					 catTix.setQuantity(2);
					 catTix.setTnBrokerId(broker.getId());
					 catTix.setSection(catTix.getSection().replaceAll("'", "").toUpperCase());
					 
					 Map<String,TNDRealCategoryTicket> catsMap = eventsTixMap.get(catTix.getTnExchangeEventId());
					 if(catsMap == null) {
						 catsMap = new HashMap<String, TNDRealCategoryTicket>();
					 }
					 String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+catTix.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase();
					 catsMap.put(key, catTix);
					 eventsTixMap.put(catTix.getTnExchangeEventId(), catsMap);
				}
				 Map<Integer,Map<String,TNDRealCategoryTicket>> eventsTixMapFromDb = new HashMap<Integer, Map<String,TNDRealCategoryTicket>>();

				 List<TNDRealCategoryTicket> catsListFromDb = DAORegistry.getTndRealCategoryTicketDAO().getAllActiveTNDRealCategoryTickets();
				 for (TNDRealCategoryTicket catTix : catsListFromDb) {
					 Map<String,TNDRealCategoryTicket> catsMapFromDb = eventsTixMapFromDb.get(catTix.getTnExchangeEventId());
					 if(catsMapFromDb == null) {
						 catsMapFromDb = new HashMap<String, TNDRealCategoryTicket>();
					 }
					 String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+catTix.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase();
					 catsMapFromDb.put(key, catTix);
					 eventsTixMapFromDb.put(catTix.getTnExchangeEventId(), catsMapFromDb);
				}
				 
				 Map<Integer,PosEvent> posEventMap = new HashMap<Integer, PosEvent>();
				 posEvents = posEventsAndBrokerMap.get(broker.getId());
				 if(posEvents == null){
					 posEvents = BrokerUtils.getPOSEventByBrokerId(broker.getId());
					 posEventsAndBrokerMap.put(broker.getId(), posEvents);
				 }
				 if(posEvents!=null){
					 for(PosEvent posEvent:posEvents){
						 posEventMap.put(posEvent.getExchangeEventId(), posEvent);
					 }
				 }
				 //Unbroadcsted event ids from TN autopricing products
				 List<Integer> unbroadcastedEventIds = DAORegistry.getQueryManagerDAO().getUserUnBroadcastedEventIdsForTN();
				 if(unbroadcastedEventIds == null) {
					 unbroadcastedEventIds = new ArrayList<Integer>();
				 }
				 
				 List<Integer> tnEventIdList = new ArrayList<Integer>(eventsTixMap.keySet());
				 int totalSize = tnEventIdList.size();
				 int processed = 0;
				 for (Integer tnEventId : tnEventIdList) {
					
					 if(isStopped()){
						 break;
					 }
					 processed++;
					 System.out.println(totalSize+"/"+processed+" : TND Real : "+tnEventId+" : added : "+eInsert+" : removed : "+eremovet);
					 try {
						 Map<String,TNDRealCategoryTicket> catsMap = eventsTixMap.remove(tnEventId);
						 Map<String,TNDRealCategoryTicket> catsMapFromDb = eventsTixMapFromDb.remove(tnEventId);
						 
						 List<TNDRealCategoryTicket> deletedCatsList = new ArrayList<TNDRealCategoryTicket>();
						 List<TNDRealCategoryTicket> newCatsList = new ArrayList<TNDRealCategoryTicket>();
						 List<TNDRealCategoryTicket> tobeUpdatedList = new ArrayList<TNDRealCategoryTicket>();
						 
						 Event event = eventMapByExchangeEventId.get(tnEventId);
						 PosEvent posEvent = posEventMap.get(tnEventId);
						 if(posEvent == null || event == null || unbroadcastedEventIds.contains(event.getId())) {
							if(catsMapFromDb!= null && !catsMapFromDb.isEmpty()) {
								for (TNDRealCategoryTicket dbCatTix : catsMapFromDb.values()) {
									dbCatTix.setStatus("DELETED");
									dbCatTix.setLastUpdated(now);
									deletedCatsList.add(dbCatTix);
								}
								//To deleted code
							}
							System.out.println("TND Real : Event or POS Event is not found : "+tnEventId);
						 } else {
							 Map<Integer,TNDRealCategoryTicket> soldTndRealCatsMap = new HashMap<Integer, TNDRealCategoryTicket>();
							 List<TNDRealCategoryTicket> soldTndRealCatsList = DAORegistry.getTndRealCategoryTicketDAO().getAllSoldTNDRealCategoryTicketsByEventId(event.getId());
							 for (TNDRealCategoryTicket catTix : soldTndRealCatsList) {
								if(catTix.getTndOrderId() != null) {
									soldTndRealCatsMap.put(catTix.getTndOrderId(), catTix);
								}
							}
							 
							 if(catsMapFromDb != null) {
								 List<String> keyList = new ArrayList<String>(catsMapFromDb.keySet());
								 for (String key : keyList) {
									 TNDRealCategoryTicket catTix = catsMap.remove(key);
									 TNDRealCategoryTicket dbCatTix = catsMapFromDb.remove(key);
									 if(catTix != null) {
										 if(dbCatTix.getTnCategoryTicketGroupId() == null || dbCatTix.getTnCategoryTicketGroupId()<=0) {
											 dbCatTix.setLastUpdated(now);
											 newCatsList.add(dbCatTix);
										 } else if(dbCatTix.getTnPrice()!=catTix.getTnPrice()) {
											 dbCatTix.setTnPrice(catTix.getTnPrice());
											 dbCatTix.setActualPrice(catTix.getActualPrice());
											 dbCatTix.setLastUpdated(now);
											 tobeUpdatedList.add(dbCatTix);
										 }
									 } else {
										 dbCatTix.setStatus("DELETED");
										 dbCatTix.setLastUpdated(now);
										 deletedCatsList.add(dbCatTix);
									 }
								 }
							 }
							 for (TNDRealCategoryTicket newCatTix : catsMap.values()) {
								 
								 TNDRealCategoryTicket soldCatTix = soldTndRealCatsMap.get(newCatTix.getTndOrderId());
								 if(soldCatTix != null) {
									 continue;
								 }
								 newCatTix.setPosEventId(posEvent.getId());
								 newCatTix.setPosExchangeEventId(posEvent.getExchangeEventId());
								 newCatTix.setEventId(event.getId());
								 newCatTix.setCreatedDate(now);
								 newCatTix.setLastUpdated(now);
								 newCatTix.setStatus("ACTIVE");
								 newCatsList.add(newCatTix);
							} 
							//To deleted and save code code
						 }
						 
						 if(!newCatsList.isEmpty()) {// && isCreateListingEnabled
	
								Calendar expetedDateCal = Calendar.getInstance();
								expetedDateCal.setTime(event.getLocalDate());
								//expetedDateCal.set(Calendar.HOUR_OF_DAY,0);
								//expetedDateCal.set(Calendar.MINUTE,0);
								//expetedDateCal.set(Calendar.SECOND,0);
								expetedDateCal.add(Calendar.DAY_OF_MONTH, - expectedArrivalDaysBeforeEvent);
								Date expectedArrivalDate = expetedDateCal.getTime();
								
							 
							 Map<String, Integer> posVenueCatIdMap = new HashMap<String, Integer>();
							 try {
								 List<PosVenueCategory> posVenueCatList = InduxDAORegistry.getPosVenueCategoryDAO().getAllVenueCategorysByVenueId(posEvent.getVenueId(),broker.getPosBrokerId());
								 for (PosVenueCategory posVenueCategory : posVenueCatList) {
									
									//Tamil : skip all venue configuration zone id mapped venue configurations for non zones pricing listings
										if(posVenueCategory.getVenueConfigZoneId() != null && posVenueCategory.getVenueConfigZoneId() > 0) {
											continue;
										}
										String key = posVenueCategory.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+posVenueCategory.getRow().replaceAll("\\s+", " ").trim().toLowerCase();
										posVenueCatIdMap.put(key, posVenueCategory.getId());
								}
								//System.out.println("venue cat size..."+posVenueCatIdMap.size());
							} catch (Exception e) {
								e.printStackTrace();
								isErrorOccured = true;
								error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								error.setExample(""+e.fillInStackTrace());
								error.setMessage("Event:" + tnEventId);
								error.setProcess("Loading POS VenueCategors.");
								error.setEventId(tnEventId);
								error.setTimeStamp(new Date());
								errorList.add(error);
								System.err.println("TND Real 9 : NF Error From While Loading POS VenueCategors.."+tnEventId);
							}
	
							int addCount = 0;
							for (TNDRealCategoryTicket tix : newCatsList) {
								/* if(excludeZones != null && broker != null && tix.getTmatZone() != null) {
									 String exZoneKey = broker.getId()+"_"+tix.getTmatZone().toUpperCase(); 
									 if(excludeZones.contains(exZoneKey)) {
										 continue;
									 }
								 }*/
								tix.setExpectedArrivalDate(expectedArrivalDate);
								tix.setShippingMethodSpecialId(shippingMethodId);
								tix.setNearTermOptionId(nearTermDisplayOptionId);
								tix.setInternalNotes(autopricingProduct.getInternalNotes());
								
								 tix.setPosVenueId(posEvent.getVenueId());
								 tix.setPosEventId(posEvent.getId());	
								 Integer posVenueCategoryId = posVenueCatIdMap.get(tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tix.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase());
								 if(null == posVenueCategoryId || posVenueCategoryId == -1) {
									posVenueCategoryId = InduxDAORegistry.getPosVenueCategoryDAO().save(tix,broker.getPosBrokerId(),autopricingProduct);
									String posVenueCategorykey = tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tix.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase();
									posVenueCatIdMap.put(posVenueCategorykey, posVenueCategoryId);
									//System.out.println("new venue cat id.."+posVenueCategoryId);
								}
								if(null == posVenueCategoryId || posVenueCategoryId == -1) {
									//newTix.setReason("Venue Category not exist in TN");
									isErrorOccured = true;
									error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									error.setMessage("Error While Creating VenueCategory");
									//error.setExample("Event:" + tnEventId + ":-:" + event.getName() +":"+ event.getLocalDate() + ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
									error.setProcess("ADD Venuecategory.");
									error.setEventId(tnEventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("TND Real 11 :Error From While Loading POS VenueCategors.."+tnEventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
									System.err.println("TND Real 11 : Error From While Loading POS VenueCategors.."+tnEventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
									
									continue;
								}
								tix.setPosVenueCategoryId(posVenueCategoryId);
								tix.setPosExchangeEventId(posEvent.getExchangeEventId());
								tix.setTnBrokerId(broker.getId());
								
								if(Counter.isMinuteAddAllow(broker)){
	//								 System.out.println("TNDReal ADD begins....."+new Date()+"...count.."+eInsert);
									try {
										Integer tnCategoryTicketGroupId = InduxDAORegistry.getPosCategoryTicketGroupDAO().save(autopricingProduct, tix,broker.getPosBrokerId());
										if(tnCategoryTicketGroupId!=-1l){
											tix.setTnCategoryTicketGroupId(tnCategoryTicketGroupId);
											broker.addMinuteUpdateCounter();
											addCount++;
											eInsert++;
										}
									 } catch(Exception e) {
										 //tix.setReason("Exception while creating ticket in TN");
										 isErrorOccured = true;
										 error = new AutoPricingError();
										 error.setProductId(autopricingProduct.getId());
										 error.setMessage("Error while creating Ticket Group. "+"Event:" + tnEventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
										 error.setExample(""+e.fillInStackTrace());
										 error.setProcess("Ticket Group Creation..");
										 error.setEventId(tnEventId);
										 error.setTimeStamp(new Date());
										 errorList.add(error);
										 log.error("TND Real 12 : Error while Creating Ticket Group. "+"Event:" + tnEventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
										 System.out.println("TND Real 12 : Error while Creating Ticket Group. "+"Event:" + tnEventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
										 e.printStackTrace();
									 }
								 }
							 }
						 }
						 for(TNDRealCategoryTicket tix:tobeUpdatedList){
							 try {
								 if(Counter.isMinuteUpdateAllow(broker)) {
//										System.out.println("MINI Update begins....."+new Date()+"...count.."+eUpdate);
										InduxDAORegistry.getPosCategoryTicketGroupDAO().updateCategoryTicketGroup(tix,broker.getPosBrokerId());
										broker.addMinuteUpdateCounter();
										eUpdate++;
								 }
							} catch (Exception e) {
								 //tix.setReason("Exception while creating ticket in TN");
								 isErrorOccured = true;
								 error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Updating Ticket Group. "+"Event:" + tnEventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("Ticket Group Creation..");
								 error.setEventId(tnEventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("TND Real 12 : Error while Updating Ticket Group. "+"Event:" + tnEventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
								 System.out.println("TND Real 12 : Error while Updating Ticket Group. "+"Event:" + tnEventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
								 e.printStackTrace();
							 }
						 }
						 for(TNDRealCategoryTicket cat:deletedCatsList){
									
							 try {
								 if(cat.getTnCategoryTicketGroupId()!=null){	
									 if(Counter.isMinuteDeleteAllow(broker)) {
	//									 System.out.println("MINI delete begins..4..."+new Date()+"...count.."+eremovet);
									
										 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
										 broker.addMinuteUpdateCounter();
										 cat.setReason("Ticket not exist in TMAT and Deleted from TN");
										 eremovet++;
									}
								 }
							 } catch (Exception e) {
								 cat.setReason("Ticket not exist in TMAT and Exception in TN");
								 e.printStackTrace();
									isErrorOccured = true;
									error = new AutoPricingError();
									error.setProductId(autopricingProduct.getId());
									error.setMessage("While Remove Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
									error.setProcess("POS Remove Category.");
									error.setExample(""+e.fillInStackTrace());
									error.setEventId(tnEventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("TND Real 14 : Error While Remove Category:"+"Event:" + tnEventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
									System.out.println("TND Real 14 : Error From POS API : Remove Category:"+"Event:" + tnEventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
							 }
						 }
						 DAORegistry.getTndRealCategoryTicketDAO().saveOrUpdateAll(newCatsList);
						 DAORegistry.getTndRealCategoryTicketDAO().updateAll(deletedCatsList);
						 DAORegistry.getTndRealCategoryTicketDAO().updateAll(tobeUpdatedList);
						 
					
					 } catch (Exception e) {
						 isErrorOccured = true;
						 error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while Processing Event.");
						 error.setExample(""+e.fillInStackTrace());
						 error.setProcess("Event Processing.");
						 error.setEventId(tnEventId);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("TND Real 1 : Error while Processing Events.");
						 System.err.println("TND Real 1 : Error while Processing Events.");
						 e.printStackTrace();
					 }
				 }
				 
				 if(eventsTixMapFromDb!= null && !eventsTixMapFromDb.isEmpty()) {
					 for (Integer tnEventId : eventsTixMapFromDb.keySet()) {
						 Map<String,TNDRealCategoryTicket> catsMap = eventsTixMapFromDb.get(tnEventId); 
						
						 if(catsMap!= null && !catsMap.isEmpty()) {
							 List<TNDRealCategoryTicket> deletedCatsList = new ArrayList<TNDRealCategoryTicket>(catsMap.values());
							 for(TNDRealCategoryTicket cat:deletedCatsList){
								 try {
									 cat.setStatus("DELETED");
									 cat.setLastUpdated(now);
									 
									 if(cat.getTnCategoryTicketGroupId()!=null){	
										 if(Counter.isMinuteDeleteAllow(broker)) {
		//									 System.out.println("MINI delete begins..4..."+new Date()+"...count.."+eremovet);
										
											 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
											 broker.addMinuteUpdateCounter();
											 cat.setReason("Ticket not exist in TMAT and Deleted from TN");
											 eremovet++;
										}
									 }
								 } catch (Exception e) {
									 cat.setReason("Ticket not exist in TMAT and Exception in TN");
									 e.printStackTrace();
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("While Remove Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(tnEventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("TND Real 14 : Error While Remove Category:"+"Event:" + tnEventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
										System.out.println("TND Real 14 : Error From POS API : Remove Category:"+"Event:" + tnEventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
								 }
							 }
							 DAORegistry.getTndRealCategoryTicketDAO().updateAll(deletedCatsList);
						 }
					 }
				 }
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("TND Real 1 : Error while Loading Events.");
				 System.err.println("TND Real 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			 
			/*List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
			
			for(Integer brokerId:brokerAddCountMap.keySet()){
				int count = brokerAddCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Insert");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerUpdateCountMap.keySet()){
				int count = brokerUpdateCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Update");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerDeleteCountMap.keySet()){
				int count = brokerDeleteCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Delete");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);*/
			DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
			System.out.println("TND Real Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
//			log.info("TND Real Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
			//running = false; 
		 }catch(Exception e){
			// running = false;
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Loading Common Properties.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Loading common Properties..");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("TND Real 16 :Error while Loading Common Properties.");
			 System.out.println("TND Real 16 : Error while Loading Common Properties.");
			 e.printStackTrace();
		 }
		 
		 String subject,fileName;
		 Map<String, Object> map;
			
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {
					log.info("CT error size......"+errorList.size());
					
					DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
					subject = "TNDRealTix Scheduler job failed :";
					fileName = "templates/autopricing-job-failure-message.txt";
					
					//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
					
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
				} catch (Exception e) {
					log.error("TND Real 17 : Error while Inserting Error Listings in TMAT.");
					 System.out.println("TND Real 17 :Error while Inserting Error Listings in TMAT.");
					e.printStackTrace();
				}
			}
	 }
	 
	 public static void processTNDRealTixCleaner() throws Exception {
			
		 AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNDRealTix");
		 AutoPricingError error= null;
		 List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		 boolean isErrorOccured = false;
				 
		 try {
			 Broker broker = BrokerUtils.getBrokerById(5);//RTW
			 
			 try {
				 Integer result = DAORegistry.getTndRealCategoryTicketDAO().deleteAllTNDRealCategoryTicketswithinMinimumExcludeEventDays(hourstoConsiderOrders,excludeEventDays);
							 
				 System.out.println("Total minicats Listings within miniumum exclude event days ("+excludeEventDays+") to be removed : "+result);
//				 log.info("Total minicats Listings within miniumum exclude event days ("+minimumExcludeEventDays+") to be removed : "+result);
						 
			 } catch (Exception e) {
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting Records within miniumum exclude event days ("+excludeEventDays+")");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Deleting Records in TMAT.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("TNDC 1 : Error while Deleting Records within miniumum exclude event days ("+excludeEventDays+") in TMAT.");
				 System.err.println("TNDC 1 : Error while Deleting Records within miniumum exclude event days ("+excludeEventDays+") in TMAT.");
				 e.printStackTrace();
			 }
			 
			 try {//delete all zoneticket listings which are not exist in exchange event for zonetickets exchange 
				 Integer result = DAORegistry.getTndRealCategoryTicketDAO().deleteAllTNDRealCategoryTicketsofUnbroadcastedEvents();
					 
				 System.out.println("Total Listings Not Exist in Exchange Event for any Exchange to be removed : "+result);
//						 log.info("Total Listings Not Exist in Exchange Event for any Exchange to be removed : "+result);
				 
			 } catch (Exception e) {
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Deleting Records in TMAT.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("TNDC 2 : Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
				 System.err.println("TNDC 2 : Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
				 e.printStackTrace();
			 }
			 
			 try {
					//Deleting unmapped MiniCats listings with POS
					 Integer result = DAORegistry.getTndRealCategoryTicketDAO().deleteAllMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(broker,autopricingProduct);
						System.out.println("Unmapped Larry tn tickets with POS for BROKER " + broker.getId() + " to be removed :"+result);
//								log.info("Unmapped Larry tn tickets with POS for BROKER " + brokerId + " to be removed :"+result);
						
					}catch (Exception e) {
						e.printStackTrace();
						error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						error.setMessage("Error while removeing unmapped larry listings with POS in TMAT for broker : "+broker.getId());
						error.setExample(""+e.fillInStackTrace());
						error.setProcess("Remove Category.");
						error.setEventId(0);
						error.setTimeStamp(new Date());
						errorList.add(error);
						log.error("TNDC 5 : Error while removeing unmapped larry listings with POS in TMAT for broker : "+broker.getId());
						System.err.println("TNDC 5 : Error while removeing unmapped larry listings with POS in TMAT for broker : "+broker.getId());
					}
					
					//Deleting unmapped MiniCats listings in POS with larryLast category
				 try {
					 List<Integer> nonExistTciketGroupIds = DAORegistry.getTndRealCategoryTicketDAO().getAllTicketNetworkGroupIdsNotExistInTNDRealCategoryTicketByBrokerId(broker,autopricingProduct);
					 System.out.println("TN ticket groups Not Exist in Mini for BROKER " + broker.getId() + " to be removed :"+nonExistTciketGroupIds.size());
//							 log.info("TN ticket groups Not Exist in Mini for BROKER " + brokerId + " to be removed :"+nonExistTciketGroupIds.size());
					 
					 for(Integer categoryTicketGroupId : nonExistTciketGroupIds){
							
						try {
							if(Counter.isMinuteDeleteAllow(broker)) {
//										System.out.println("FE API delete begins....."+new Date()+"...count.."+removet);
								
								InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),categoryTicketGroupId);
								broker.addMinuteUpdateCounter();
								//removet++;
							}
						} catch (Exception e) {
								isErrorOccured = true;
								error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								error.setMessage("ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
								error.setProcess("POS Remove Category.");
								error.setExample(""+e.fillInStackTrace());
								error.setEventId(0);
								error.setTimeStamp(new Date());
								errorList.add(error);
								log.error("TNDC 6 : Error While Remove TN ticket groups Not Exist in larrylast ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
								System.err.println("TNDC 6 : Error While Remove TN ticket groups Not Exist in larrylast ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
						}
					 }
				 } catch (Exception e) {
					 
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Deleting mismatching Records in POS for broker : "+broker.getId());
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Deleting Records in POS.");
					 error.setEventId(0);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("TNDC 7 : Error while Deleting mismatching Records in POS for broker : "+broker.getId());
					 System.err.println("TNDC 7 : Error while Deleting mismatching Records in POS for broker : "+broker.getId());
					 e.printStackTrace();
				 }
			 
		 } catch (Exception e) {
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Cleaning Listings.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Cleaning Listings.");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("TND Real 1 : Error while Cleaning Listings.");
			 System.err.println("TND Real 1 : Error while Cleaning Listings.");
			 e.printStackTrace();
		 }
				 
			 
		 String subject,fileName;
		 Map<String, Object> map;
			
		 if(isErrorOccured){
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			String toAddress = resourceBundle.getString("emailNotificationTo");
			String ccAddress= resourceBundle.getString("emailNotificationCCTo");
				
			map = new HashMap<String, Object>();
			map.put("error", error);
			map.put("errors", errorList);
			try {
				log.info("CT error size......"+errorList.size());
				
				DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
				subject = "TNDRealTix Cleaner job failed :";
				fileName = "templates/autopricing-job-failure-message.txt";
				
				//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
				
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
				
			} catch (Exception e) {
				log.error("TND Real Cleaner 17 : Error while Inserting Error Listings in TMAT.");
				 System.out.println("TND Real Cleaner 17 :Error while Inserting Error Listings in TMAT.");
				e.printStackTrace();
			}
			}
		 }
	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
			if(!running){
				running=true;
				Calendar cal = Calendar.getInstance();
				
				//Run hourly once
				if (cal.get(Calendar.MINUTE) < 10 ) {
					try {
						System.out.println("TNDRealTix Job Called.." + new Date() + ": " + running);
						
						processTNDRealTickets();
						
						System.out.println("TND Real Scheduler Job finished @ " + new Date());
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				try {
					System.out.println("TNDRealTix Cleaner Job Called.." + new Date() + ": " + running);
					
					processTNDRealTixCleaner();
					
					System.out.println("TND RealTix Cleaner Job finished @ " + new Date());
				}catch(Exception e){
					e.printStackTrace();
				}
				running = false;
			}
	}


	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		TNDRealTixScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		TNDRealTixScheduler.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNDRealTix").getLastRunTime();
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		TNDRealTixScheduler.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}


	public static void setNextRunTime(Date nextRunTime) {
		TNDRealTixScheduler.nextRunTime = nextRunTime;
	}

}