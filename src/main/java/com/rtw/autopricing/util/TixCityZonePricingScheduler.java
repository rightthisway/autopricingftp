package com.rtw.autopricing.util;

//import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZones;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.TixCityZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;


public class TixCityZonePricingScheduler  extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(TixCityZonePricingScheduler.class);
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 public static Double ROT_DEFAULT_NO_FLOORCAP_MARKUP = 20.0;
	 
	 
	 public static void processTixCityZonesPricingTickets() throws Exception {
		Calendar cal =  Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.MINUTE,15);
		setNextRunTime(cal.getTime());
		if(isStopped() || isRunning()){
			return ;
		}
		setRunning(true);
		
		
		Date lastRunTime = getLastUpdateTime();
		Long minute = 0l;
		if(lastRunTime!=null){
			minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
		}else{
			minute = 2880l; // 2 Days in minute
		}
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RTWZonesPricing");
		if(autopricingProduct==null){
			autopricingProduct = new AutopricingProduct();
			autopricingProduct.setName("RTWZonesPricing");
			autopricingProduct.setStatus("ACTIVE");
			autopricingProduct.setStopped(false);
		}
		if(autopricingProduct.getStopped()){
			setStopped(true);
			setRunning(false);
			return;
		}
		
		setLastUpdateTime(now);
		autopricingProduct.setLastRunTime(now);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
//		Date jobStartTime = new Date();
		int tInsert=0,tUpdate=0,tremovet=0,tposCount=0;
		int eInsert=0,eUpdate=0,eremovet=0,eposCount=0;
		
		 try{
			 Map<Integer, Integer> brokerAddCountMap = new HashMap<Integer, Integer>();
			 Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
			 Map<Integer, Integer> brokerUpdateCountMap = new HashMap<Integer, Integer>();
			 
			 Double tnExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork").getAdditionalMarkup();
			 Double vividExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats").getAdditionalMarkup();
			 Double scorebigMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Scorebig").getAdditionalMarkup();
			 Double fanxchangeMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("FanXchange").getAdditionalMarkup();
			 Double ticketcityMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity").getAdditionalMarkup();
			 Double tickPickExAddMarkup = 0.0;
			 Double scoreBigExAddMarkup = 0.0;
				
			 Boolean isTnEvent = false;
			 
			 Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
			 List<PosEvent> posEvents = new ArrayList<PosEvent>();
			 Map<Integer, List<PosEvent>> posEventsAndBrokerMap = new HashMap<Integer, List<PosEvent>> ();
			 Collection<Event> eventList = null;
			 Collection<TixCityZonePricingExchangeEvent> tixCityZonePricingExchangeEvents = null;
			 
			 try {
				 //Get all events that is updated in tmat since last run time. 
				 tixCityZonePricingExchangeEvents = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsEligibleForUpdate(minute,autopricingProduct);
				 System.out.println("TZP exchange Event Size : "+tixCityZonePricingExchangeEvents.size());
//				 log.info("TZP exchange Pricing Event Size : "+TixCityZonePricingExchangeEvents.size());
				 
				 eventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentId();
				 for(Event event:eventList) {
					eventMap.put(event.getId(), event);
					eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
				 }
				 System.out.println("TZP : TMAT event size : "+eventList.size());
//				 log.info("ZP : TMAT event size : "+eventList.size());
				 
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("TZP 1 : Error while Loading Events.");
				 System.err.println("TZP 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			 Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			 Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			 try {
				 List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductId(autopricingProduct.getId());
				 for (ExcludeEventZones excludeEventZone : excludeEventZones) {
					 Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
					 if(eventZones == null) {
						 eventZones = new HashSet<String>();
					 }
					 eventZones.add(excludeEventZone.getBrokerId()+"_"+excludeEventZone.getZone().toUpperCase());
					 excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
				}
				 List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductId(autopricingProduct.getId());
				 for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
					 Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
					 if(venueCategoryZones == null) {
						 venueCategoryZones = new HashSet<String>();
					 }
					 venueCategoryZones.add(excludeVenueCategoryZone.getBrokerId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
					 excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
				}
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Exclude Zones.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Exclude Zones Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("MINI 1 : Error while Loading Exclude Zones..");
				 System.err.println("MINI 1 : Error while Loading Exclude Zones..");
				 e.printStackTrace();
			 }
			 
			 /*List<ZonesPricingFloorCap> zonesPricingFloorCaps = DAORegistry.getQueryManagerDAO().getAllExchangeEventFloorCap();
			 Map<Integer, Map<String, Double>> exchangeEventFloorCapMap = new HashMap<Integer, Map<String,Double>>();
			 Map<Integer, Map<String, String>> exchangeEventFloorCapSectionMap = new HashMap<Integer, Map<String,String>>();
			 for (ZonesPricingFloorCap zonesPricingFloorCap : zonesPricingFloorCaps) {
				 Map<String, Double> zoneFloorCapMap = exchangeEventFloorCapMap.get(zonesPricingFloorCap.getTnExchangeEventId());
				 Map<String, String> zoneFloorCapSectionMap = exchangeEventFloorCapSectionMap.get(zonesPricingFloorCap.getTnExchangeEventId());
				 
				 if(null != zoneFloorCapMap && !zoneFloorCapMap.isEmpty()){
						zoneFloorCapMap.put(zonesPricingFloorCap.getSection().replaceAll("\\s+", " ").trim().toLowerCase(), zonesPricingFloorCap.getFloorCap());
						zoneFloorCapSectionMap.put(zonesPricingFloorCap.getSection().replaceAll("\\s+", " ").trim().toLowerCase(), zonesPricingFloorCap.getSection());
				}else {
					zoneFloorCapMap = new HashMap<String, Double>();
					zoneFloorCapSectionMap = new HashMap<String, String>();
					zoneFloorCapMap.put(zonesPricingFloorCap.getSection().replaceAll("\\s+", " ").trim().toLowerCase(), zonesPricingFloorCap.getFloorCap());
					zoneFloorCapSectionMap.put(zonesPricingFloorCap.getSection().replaceAll("\\s+", " ").trim().toLowerCase(), zonesPricingFloorCap.getSection());
				}
				exchangeEventFloorCapMap.put(zonesPricingFloorCap.getTnExchangeEventId(), zoneFloorCapMap);
				exchangeEventFloorCapSectionMap.put(zonesPricingFloorCap.getTnExchangeEventId(), zoneFloorCapSectionMap);
			}*/
			
			 Map<Integer, Map<String, Integer>> brokersVenueConfigZoneMap = new HashMap<Integer, Map<String, Integer>>();
				
				/*for(Integer brokerId : BrokerUtils.getAllBrokersId()){
					Broker broker = BrokerUtils.getBrokerById(brokerId);
					
					List<PosVenueConfigurationZone> venuConfigZones = InduxDAORegistry.getPosVenueConfigurationZoneDAO().getAllVenueConfigZone(broker.getPosBrokerId());
					Map<String, Integer> venueConfigZoneMap = new HashMap<String, Integer>();
					for (PosVenueConfigurationZone configZoneObj : venuConfigZones) {
						String key = configZoneObj.getVenueConfigurationId()+":"+configZoneObj.getZone().replaceAll("\\s+", " ").trim().toLowerCase();
						venueConfigZoneMap.put(key, configZoneObj.getVenueConfigurationZoneId());
					}
					
					brokersVenueConfigZoneMap.put(brokerId, venueConfigZoneMap);
				}*/
				
			 int eventSize = tixCityZonePricingExchangeEvents.size();
			 Integer i=0;
			 Map<Integer,DefaultAutoPricingProperties> defaultAutoPricingsMap = new HashMap<Integer, DefaultAutoPricingProperties>();
			 List<DefaultAutoPricingProperties> defaultAutoPricingsFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getDefaultAutoPricingPropertiesByProductId(autopricingProduct.getId());
			 for (DefaultAutoPricingProperties defaultAutopriicngSettings : defaultAutoPricingsFromDB) {
				 defaultAutoPricingsMap.put(defaultAutopriicngSettings.getParentCategoryId(),defaultAutopriicngSettings);
			 }
			 
			 Integer minimamExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
			 
//			 AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(brokerId, productId, exch) 
			 for (ExchangeEvent exEvent : tixCityZonePricingExchangeEvents) {
				 
				 /*exEvent.setLowerMarkup(0.00);
				 exEvent.setUpperMarkup(0.00);
				 exEvent.setLowerShippingFees(0.00);
				 exEvent.setUpperShippingFees(0.00);
				 exEvent.setExposure("1-OXP");*/
				 
				 
				 if(isStopped()){
					 break;
				 }
				 isTnEvent = false;
				 
				 i++;
				 Integer eventId = exEvent.getEventId();
//				 eventId = 1000071847; 
				 Event event = eventMap.get(eventId);
				 if(event==null ){
					 System.out.println(i+".TMAT Event Not Exisit :" + eventId);
//					 log.info(i+".TMAT Event Not Exisit :" + eventId);
					 continue;
				 }
				 eInsert=0;eUpdate=0;eremovet=0;eposCount=0;
				 
				 Integer exBrokerId = null;
				 //if((exEvent.getZone()==null || !exEvent.getZone()) &&  exEvent.getTicketNetworkBrokerId()!=null && exEvent.getTicketNetworkBrokerId()!=0){
				 if(exEvent.getTicketNetworkBrokerId()!=null && exEvent.getTicketNetworkBrokerId()!=0) {
					 isTnEvent = true;
					 exBrokerId = exEvent.getTicketNetworkBrokerId();
				 }else if(exEvent.getVividBrokerId()!=null && exEvent.getVividBrokerId()!=0){
					 exBrokerId = exEvent.getVividBrokerId();
				 }else if(exEvent.getScoreBigBrokerId()!=null && exEvent.getScoreBigBrokerId()!=0){
					 exBrokerId = exEvent.getScoreBigBrokerId();
				 }else if(exEvent.getFanxchangeBrokerId()!=null && exEvent.getFanxchangeBrokerId()!=0){
					 exBrokerId = exEvent.getFanxchangeBrokerId();
				 } else if(exEvent.getSeatGeekBrokerId()!=null && exEvent.getSeatGeekBrokerId()!=0){
					 exBrokerId = exEvent.getSeatGeekBrokerId();
				 } else {
					 exBrokerId = exEvent.getTicketcityBrokerId();
				 }
				 Date currentTime = new Date();
				 Broker broker = null;
				 AutopricingSettings autopricingSettings = null;
				 AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
				 if(isTnEvent){
					 broker = BrokerUtils.getBrokerById(exBrokerId);
					 if(broker!=null){
						 autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
					 }
				 }
				 
				 boolean isUpdateTN=true;
				if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
					isUpdateTN = false;
				}
				
				
				//need to fix it..				
//				if(exEvent.getScoreBigBrokerId() != null && exEvent.getScoreBigBrokerId().equals(2)) {
//					scoreBigExAddMarkup = 100.0;
//				} else {
					scoreBigExAddMarkup = scorebigMarkup;
//				}

//Remove listings if event is assigned to non Right this way Broker.					
				if(exEvent.getTicketNetworkBrokerId() == null || exEvent.getZone() == null || !exEvent.getZone()
						|| (exEvent.getTicketNetworkBrokerId() != null && !exEvent.getTicketNetworkBrokerId().equals(5))) { 
					
					 try { // Remove all tickets of event if event is not zone , event is not active or there is restriction to update POS.
						  
						  List<TixCityZonesPricingCategoryTicket> miniTickets = DAORegistry.getTixCityZonesPricingCategoryTicketDAO().getAllTixCityZonesPricingCategoryTicketsByEventId(eventId);
						  
						 for(TixCityZonesPricingCategoryTicket cat:miniTickets){
							 
							 cat.setReason("Event is a non Zone Event");
							 if(broker != null && isUpdateTN) {	
								if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
									cat.setStatus("DELETED");
									cat.setLastUpdated(currentTime);
									continue;
								}
								try {
									if(Counter.isMinuteDeleteAllow(broker)) {
//										System.out.println("LRM delete begins..1..."+new Date()+"...count.."+eremovet);
										InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
										broker.addMinuteUpdateCounter();
										eremovet++;
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										cat.setReason("Event is a non Zone Event and Deleted from TN");
									}
								} catch (Exception e) {
									cat.setReason("Event is a non Zone Event and Exception in TN");
									e.printStackTrace();
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(eventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("TZP 2 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
										System.err.println("TZP 2 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
								}
							 }
						}
						 DAORegistry.getTixCityZonesPricingCategoryTicketDAO().updateAll(miniTickets);
						
					} catch (Exception e) {
						isErrorOccured = true;
						error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
						 error.setExample(""+e.fillInStackTrace());
						 error.setProcess("Deleting Event Listings.");
						 error.setEventId(eventId);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("TZP 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
						 System.err.println("TZP 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
						 e.printStackTrace();
					}
					
					continue;
				 }
				 

				Map<Integer,PosEvent> posEventMap = new HashMap<Integer, PosEvent>();
				if(broker != null && isUpdateTN && isTnEvent) {
				 //if(isUpdatePOS) {
					 posEvents = posEventsAndBrokerMap.get(exBrokerId);
					if(posEvents == null || posEvents.isEmpty()){
						posEvents = BrokerUtils.getPOSEventByBrokerId(exBrokerId);
						posEventsAndBrokerMap.put(exBrokerId, posEvents);
					}
					 if(posEvents!=null){
						 for(PosEvent posEvent:posEvents){
							 posEventMap.put(posEvent.getExchangeEventId(), posEvent);
						 }
					
					 }
				 }
				if(posEventMap.isEmpty()) {
					isTnEvent = false;
				}
				
				PosEvent posEvent = posEventMap.get(event.getAdmitoneId());
				if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || posEvent.getExchangeEventId() == 0){
					 // Remove all tickets of event if event is not in POS or not an exchange event in POS.
					if(broker != null && isUpdateTN && isTnEvent) {
						try {
							List<TixCityZonesPricingCategoryTicket> miniTickets = DAORegistry.getTixCityZonesPricingCategoryTicketDAO().getAllTNTixCityZonesPricingCategoryTicketsByEventId(event.getId());
//							System.out.println("DELETING " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
//							log.info("DELETING " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());

							for(TixCityZonesPricingCategoryTicket cat:miniTickets){
								
								cat.setReason("Event not Exist in TN");
								if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
									continue;
								}
								try {
									if(Counter.isMinuteDeleteAllow(broker)) {
//										System.out.println("LRM delete begins..2..."+new Date()+"...count.."+eremovet);
										InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
										broker.addMinuteUpdateCounter();
										eremovet++;
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										cat.setReason("Event not Exist in TN and Deleted from TN");
									}
								} catch (Exception e) {
									cat.setReason("Event not Exist in TN and Exception in TN");
									e.printStackTrace();
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(eventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("TZP 4 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
										System.err.println("TZP 4 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
								}
							}
							DAORegistry.getTixCityZonesPricingCategoryTicketDAO().updateAll(miniTickets);

						} catch (Exception e) {
							isErrorOccured = true;
							error = new AutoPricingError();
							 error.setProductId(autopricingProduct.getId());
							 error.setMessage("Error while Deleting Event Listings for Event not Found in POS."+eventId);
							 error.setExample(""+e.fillInStackTrace());
							 error.setProcess("Deleting Event Listings.");
							 error.setEventId(eventId);
							 error.setTimeStamp(new Date());
							 errorList.add(error);
							 log.error("TZP 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
							 System.err.println("TZP 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
							 e.printStackTrace();
						}
					
					}
					isTnEvent = false;
//					continue;
				}
				 
				System.out.println("RTWZonesPricing Event:" + i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());
//				log.info(i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());

				Date date = null;
				if(event.getLocalDate()!=null){
					date = df.parse(df.format(event.getLocalDate()));	
				}
				//now = df.parse(df.format(now));
//				System.out.println("============" + autopricingSettings);
				Integer excludingEventDays = null;
				if(autopricingSettings!=null){
					excludingEventDays = autopricingSettings.getExcludeEventDays();
				}
				if(excludingEventDays==null){
					excludingEventDays = minimamExcludeEventDays ;
				}
				if(date!=null && ((date.getTime()-now.getTime()) <= excludingEventDays* 24 * 60 * 60 * 1000)) {//2 days events
					 // Remove all tickets of event if event is within 2 days..
					try {
						List<TixCityZonesPricingCategoryTicket> miniTickets = DAORegistry.getTixCityZonesPricingCategoryTicketDAO().getAllTixCityZonesPricingCategoryTicketsByEventId(event.getId());
//						System.out.println("DELETING " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
//						log.info("DELETING " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
						
						if(broker != null && isUpdateTN && isTnEvent) {
							
							for(TixCityZonesPricingCategoryTicket cat:miniTickets){
								cat.setStatus("DELETED");
								cat.setLastUpdated(currentTime);
								cat.setReason("Event within exclude eventdays");
								
								if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
									continue;
								}
								try {
									if(Counter.isMinuteDeleteAllow(broker)) {
//										System.out.println("LRM delete begins..3..."+new Date()+"...count.."+eremovet);
										InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
										broker.addMinuteUpdateCounter();
										eremovet++;
										cat.setReason("Event within exclude eventdays and Deleted from TN");
									}
								} catch (Exception e) {
									cat.setReason("Event within exclude eventdays and Exception in TN");
									e.printStackTrace();
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(eventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("TZP 6 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
										System.err.println("TZP 6 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
								}
							}
						 }
						 DAORegistry.getTixCityZonesPricingCategoryTicketDAO().updateAll(miniTickets);
						
					} catch (Exception e) {
						isErrorOccured = true;
						error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while Deleting Event Listings for Event within Few Days."+eventId);
						 error.setExample(""+e.fillInStackTrace());
						 error.setProcess("Deleting Event Listings.");
						 error.setEventId(eventId);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("TZP 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
						 System.err.println("TZP 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
						 e.printStackTrace();
					}
					continue;
				}
				
				Map<String, Integer> venueConfigZoneMap = brokersVenueConfigZoneMap.get(broker.getId());
				try { 
					if(venueConfigZoneMap == null || venueConfigZoneMap.isEmpty()) {
						venueConfigZoneMap = new HashMap<String, Integer>();
						if(isTnEvent && isUpdateTN){
							venueConfigZoneMap = BrokerUtils.getPOSVenueConfigurationMap(broker);
						}
						
						brokersVenueConfigZoneMap.put(broker.getId(), venueConfigZoneMap);
					}
				} catch (Exception e) {
					isErrorOccured = true;
					error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Loading VenueConfiguration. brokerId." + broker.getId());
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Error while Loading VenueConfiguration.");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("TZP 77 : Error while Loading VenueConfiguration. brokerId." + broker.getId()+" : eventId : "+eventId);
					 System.err.println("TZP 77 : Error while Loading VenueConfiguration. brokerId." + broker.getId()+" : eventId : "+eventId);
					 e.printStackTrace();
					 
				}
				Set<String> excludeZones = excludeEventZoneMap.get(eventId);
				if(excludeZones == null) {
					excludeZones = excludeVenueCategoryZoneMap.get(event.getVenueCategoryId());
				}
				
				// If event is valid event add , update or delete event tickets based on latest tmat tickets.
				Map<String,TixCityZonesPricingCategoryTicket> catTixMap = new HashMap<String, TixCityZonesPricingCategoryTicket>();
				Map<String,TixCityZonesPricingCategoryTicket> catTixFromDB = new HashMap<String, TixCityZonesPricingCategoryTicket>();
				Set<String> zoneswithTickets = new HashSet<String>(); 
				List<TixCityZonesPricingCategoryTicket> updatedBrokerTicketList = new ArrayList<TixCityZonesPricingCategoryTicket>();
				try {
					int defaultExpectedArrivialDatePriorDays = exEvent.getShippingDays();
					Integer sectionMinEtry = 1;
					DefaultAutoPricingProperties defaultAutopricing = defaultAutoPricingsMap.get(event.getParentCategoryId());
					if(defaultAutopricing != null) {
						sectionMinEtry = defaultAutopricing.getSectionCountTicket();
					}
					 List<CategoryTicket> catTixList = CategoryGroupManager.computeCategoryTickets(event, exEvent, defaultExpectedArrivialDatePriorDays, sectionMinEtry, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeMarkup,ticketcityMarkup,autopricingProduct,null);
					 for (CategoryTicket catTixObj : catTixList) { // Add generated tickets in map
						TixCityZonesPricingCategoryTicket catTix = (TixCityZonesPricingCategoryTicket) catTixObj;
						
						//Tamil : RTW Category ticket tn price should be greater than $75 as per business.
						//Tamil : Category ticket tn price should be less than $5000 as per business.
						if(catTix.getTnPrice() <= 75 || catTix.getTnPrice()>=5000){
							 continue;
						 }
						
						if(isTnEvent && isUpdateTN){
							catTix.setPosEventId(posEvent.getId());
							catTix.setPosExchangeEventId(posEvent.getExchangeEventId());
							
							//if(!broker.getName().equalsIgnoreCase("Right This Way")) {
							
							String venueConfigZoneKey = posEvent.getVenueConfigurationId()+":"+catTixObj.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
							Integer configZoneId =venueConfigZoneMap.get(venueConfigZoneKey);
							
							if(null == configZoneId || configZoneId == -1  || configZoneId == 0 ){
								System.out.println("Venue Configuration Zone is not found.. Zone:"+catTixObj.getSection());
								continue;
							}
							catTixObj.setVenueConfigurationZoneId(configZoneId);
						}
						
						//broadway child category id for manhattan pos is 70 and it is hard coded here.if event is broad way then we should not proccess ZONE A and ZONE PREMIUM category tickets.
						/*if(posEvent != null && posEvent.getChildCategoryId().equals(70)){
							if(catTix.getSection().equalsIgnoreCase("zone premium") || catTix.getSection().equals("zone a")) {
								continue;
							}
						}*/
						
						/*if(!broker.getName().equalsIgnoreCase("Right This Way")) {
							//if(broker.getName().equals("Reserve One Tickets")) {
								Map<String, Double> zonesPricingMap = exchangeEventFloorCapMap.get(event.getAdmitoneId());
								
								Double floorCap = 0.0;
								if(null != zonesPricingMap && !zonesPricingMap.isEmpty()){
									floorCap = zonesPricingMap.get(catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase());
									if(null == floorCap ){
										floorCap = 0.0;
									}
								}
								//System.out.println("FC : "+floorCap+"---"+catTix.getSection()+"---"+catTix.getQuantity()+"------"+catTix.getActualPrice());
								
								if(floorCap != 0 && floorCap != 0.00){
									if(catTix.getPurPrice() > floorCap){
										double price =  Math.floor(catTix.getPurPrice()-1);
										catTix.setTnPrice(price);
										catTix.setActualPrice(price);
										
									}else if (catTix.getPurPrice() < floorCap){
										catTix.setTnPrice(floorCap);
										catTix.setActualPrice(floorCap);
									}else if (catTix.getPurPrice()== floorCap){
										
									} else {
										System.out.println("Skipped Due to FC : "+catTix.getSection()+"---"+catTix.getSection()+"---"+catTix.getQuantity()+"------"+catTix.getActualPrice());
										//log.info("Skipped Due to FC : "+catTix.getSection()+"---"+catTix.getSection()+"---"+catTix.getQuantity()+"------"+catTix.getActualPrice());
										continue;
									}
								} else {
									//if event do not have floor cap then we have to add markup only
									if(broker.getName().equalsIgnoreCase("Reserve One Tickets")) {
										double price = Math.ceil(catTix.getPurPrice() * (1+(ROT_DEFAULT_NO_FLOORCAP_MARKUP/100)));
										catTix.setTnPrice(price);
										catTix.setActualPrice(price);
									} else if(broker.getName().equalsIgnoreCase("Joe Knows Tickets")) {
										continue;
									}
								}
								zoneswithTickets.add(catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase());
								catTix.setFloorCap(floorCap);
							//}
						}*/
						
						//update user assigned shipping method
						if(exEvent.getShippingMethod() != null) {
							catTix.setShippingMethodSpecialId(exEvent.getShippingMethod());
						}
						
						String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
						catTixMap.put(key, catTix);
					 }
					/* if(broker.getName().equalsIgnoreCase("Joe Knows Tickets")) {
						 Map<String, Double> zonesPricingMap = exchangeEventFloorCapMap.get(event.getAdmitoneId());
						 Map<String, String> zoneFloorCapSectionMap = exchangeEventFloorCapSectionMap.get(event.getAdmitoneId());
							Double floorCap = 0.0;
							if(null != zonesPricingMap && !zonesPricingMap.isEmpty()){

								for (String fcZone : zonesPricingMap.keySet()) {
									floorCap = zonesPricingMap.get(fcZone);
									if(floorCap == null || floorCap <= 0 || floorCap >= 5000 || zoneswithTickets.contains(fcZone)){
										continue;
									}
									Date expectedArrivalDate = null;
									if(event.getLocalDate()!=null) {
										Calendar calOne = Calendar.getInstance();
										calOne.setTime(event.getLocalDate());
										calOne.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
										expectedArrivalDate = calOne.getTime();
									}
									String section = zoneFloorCapSectionMap.get(fcZone);
									
									TixCityZonesPricingCategoryTicket catTicket = new TixCityZonesPricingCategoryTicket(section, floorCap, event);
									catTicket.setTnExchangeEventId(event.getAdmitoneId());
									catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
									dbTix.setExpectedArrivalDate(tix.getExpectedArrivalDate());
									//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
									catTicket.setPosEventId(posEvent.getId());
									catTicket.setPosExchangeEventId(posEvent.getExchangeEventId());
									catTicket.setExpectedArrivalDate(expectedArrivalDate);
									catTicket.setQuantity(8);
									
									catTicket.setPosEventId(posEvent.getId());
									catTicket.setPosExchangeEventId(posEvent.getExchangeEventId());
									
									//if(!broker.getName().equalsIgnoreCase("Right This Way")) {
									if(isTnEvent && isUpdateTN){
										//Map<String, Integer> venueConfigZoneMap = brokersVenueConfigZoneMap.get(broker.getId());
										String venueConfigZoneKey = posEvent.getVenueConfigurationId()+":"+catTicket.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
										Integer configZoneId =venueConfigZoneMap.get(venueConfigZoneKey);
										
										if(null == configZoneId || configZoneId == -1  || configZoneId == 0 ){
											System.out.println("Venue Configuration Zone is not found.. Zone:"+catTicket.getSection());
											continue;
										}
										catTicket.setVenueConfigurationZoneId(configZoneId);
									}
									
									//update user assigned shipping method
									if(exEvent.getShippingMethod() != null) {
										catTix.setShippingMethodSpecialId(exEvent.getShippingMethod());
									}
									String key = catTicket.getQuantity()+":"+ catTicket.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
									catTixMap.put(key, catTicket);
								}
							}
					 }*/
					 
					 
					 List<TixCityZonesPricingCategoryTicket> miniTickets = DAORegistry.getTixCityZonesPricingCategoryTicketDAO().getAllTixCityZonesPricingCategoryTicketsByEventId(event.getId());
					 for (TixCityZonesPricingCategoryTicket dbCatTix : miniTickets) {// Add Existing tickets in map.]
						 if(isTnEvent && isUpdateTN){
							 dbCatTix.setPosVenueId(posEvent.getVenueId());
							 dbCatTix.setPosEventId(posEvent.getId());
							 dbCatTix.setPosExchangeEventId(posEvent.getExchangeEventId());
							 
							 if(dbCatTix.getTnExchangeEventId() != null && !dbCatTix.getTnExchangeEventId().equals(event.getAdmitoneId())) {
								 dbCatTix.setStatus("DELETED");
								 dbCatTix.setLastUpdated(currentTime);
								 dbCatTix.setReason("POS Event Id Mismatching");
								 updatedBrokerTicketList.add(dbCatTix);
								 continue;
							 }
							 
							 if(dbCatTix.getTnCategoryTicketGroupId() != null) {
								 if(excludeZones != null && broker != null && dbCatTix.getTmatZone() != null) {
									 String exZoneKey = broker.getId()+"_"+dbCatTix.getTmatZone().toUpperCase(); 
									 if(excludeZones.contains(exZoneKey)) {
										 dbCatTix.setStatus("DELETED");
										 dbCatTix.setLastUpdated(currentTime);
										 dbCatTix.setReason("Excluded Zone ticket");
										 updatedBrokerTicketList.add(dbCatTix);
										 continue;
									 }
								 }
								 /*if(dbCatTix.getQuantity()==2 && dbCatTix.getTnPrice()<50){
									 dbCatTix.setStatus("DELETED");
									 dbCatTix.setReason("Price below $50 and Deleted from TN");
									 updatedBrokerTicketList.add(dbCatTix);
									 continue;
								 }*/
								 if(dbCatTix.getTnBrokerId()!=null) {
									 if(!dbCatTix.getTnBrokerId().equals(exEvent.getTicketNetworkBrokerId())){
										 dbCatTix.setStatus("DELETED");
										 dbCatTix.setReason("Ticket TN Broker mismatching with event TN broker");
										 updatedBrokerTicketList.add(dbCatTix);
										 continue;
									 }
								 } else {
									 dbCatTix.setTnBrokerId(exEvent.getTicketNetworkBrokerId());
								 }
							 }
						 }
						 String key = dbCatTix.getQuantity()+":"+ dbCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
						 catTixFromDB.put(key, dbCatTix);
					 }
					 
				} catch (Exception e) {
					isErrorOccured = true;
					error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Computing Category Tickets."+eventId);
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Category Ticket Computation.");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("TZP 8 : Error while Computing Category Tickets."+eventId);
					 System.err.println("TZP 8 : Error while Computing Category Tickets."+eventId);
					 e.printStackTrace();
					 continue;
				}
				List<TixCityZonesPricingCategoryTicket> tnNewCatTix = new ArrayList<TixCityZonesPricingCategoryTicket>();
				List<TixCityZonesPricingCategoryTicket> tnUpdateCatTix = new ArrayList<TixCityZonesPricingCategoryTicket>();
				List<TixCityZonesPricingCategoryTicket> tnRemoveCatTix = new ArrayList<TixCityZonesPricingCategoryTicket>();
				Map<String,TixCityZonesPricingCategoryTicket> exFinalCatTixMap = new HashMap<String, TixCityZonesPricingCategoryTicket>();
				 try {
					 String priceHistory="";
					 String ticketIdHistory= "";
					 String baseTicketOneHistory= "";
					 String baseTicketTwoHistory="";
					 String baseTicketThreeHistory="";
					 
					 String ticketHDateStr = dateTimeFormat.format(new Date());
					String priceHDateStr = dateTimeFormat.format(new Date());
					List<String> keys = new ArrayList<String>(catTixFromDB.keySet());
					Integer baseTicket1,baseTicket2,baseTicket3;
					Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
					//Integer purchaseOrderId = null;
					//boolean existingPurchaseOrder = false;
					
					for(String key:keys) {
						TixCityZonesPricingCategoryTicket dbTix =catTixFromDB.remove(key);
						TixCityZonesPricingCategoryTicket tix = catTixMap.remove(key);
						isPriceChangeFlag = false;
						
						if(tix != null) { // if generated tickets is already existing..
							if(!dbTix.getActualPrice().equals(tix.getActualPrice())) {
								isPriceChangeFlag = true;
								priceHistory = priceHDateStr+"-"+tix.getActualPrice();
								if(dbTix.getPriceHistory() != null) {
									priceHistory = dbTix.getPriceHistory() +","+priceHistory;
									if(priceHistory.length()>=500) {
										priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
									}
								}
								dbTix.setPriceHistory(priceHistory);
								dbTix.setIsEdited(true);
								
							} else if(//!dbTix.getVividPrice().equals(tix.getVividPrice()) ||
//										 !dbTix.getTickpickPrice().equals(tix.getTickpickPrice()) ||
									//!dbTix.getScoreBigPrice().equals(tix.getScoreBigPrice()) || 
									//!dbTix.getFanxchangePrice().equals(tix.getFanxchangePrice()) ||
									//!dbTix.getTicketcityPrice().equals(tix.getTicketcityPrice()) || 
									!dbTix.getTicketId().equals(tix.getTicketId())) { 
								
								dbTix.setIsEdited(true);
								
							} else {
								baseTicket1 = 0;
								baseTicket2 = 0;
								baseTicket3 = 0;
								
								if(dbTix.getBaseTicketOne() != null) {
									baseTicket1 = dbTix.getBaseTicketOne();
								}
								if(dbTix.getBaseTicketTwo() != null) {
									baseTicket2 = dbTix.getBaseTicketTwo();
								}
								if(dbTix.getBaseTicketThree() != null) {
									baseTicket3 = dbTix.getBaseTicketThree();
								}
								
								if(!tix.getBaseTicketOne().equals(baseTicket1) || !tix.getBaseTicketTwo().equals(baseTicket2) || !tix.getBaseTicketThree().equals(baseTicket3)) {
									dbTix.setIsEdited(true);
									
								}
							}
							
							//add the pricehistory of basetickets prices here for all tickets
							
							tempTicketIdFlag = false;
							if(isPriceChangeFlag || !dbTix.getTicketId().equals(tix.getTicketId())) {
								ticketIdHistory = ticketHDateStr +"/"+ tix.getTicketId() +"/"+ tix.getPurPrice();
								
								if(dbTix.getTicketIdHistory() != null) {
									ticketIdHistory = dbTix.getTicketIdHistory()+","+ ticketIdHistory;
									if(ticketIdHistory.length()>= 500) {
										ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
									}
								}
								dbTix.setTicketIdHistory(ticketIdHistory);
								dbTix.setIsEdited(true);
							} 
							
							// For Base Ticket 1
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketOne() != null && dbTix.getBaseTicketOne() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketOne().equals(tix.getBaseTicketOne())) {
									tempTicketIdFlag = true;
								}
							} else if(tix.getBaseTicketOne() != null && tix.getBaseTicketOne() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketOneHistory = ticketHDateStr +"/"+ tix.getBaseTicketOne() +"/"+ tix.getBaseTicketOnePurPrice();
								
								if(dbTix.getBaseTicketOneHistory() != null) {
									baseTicketOneHistory = dbTix.getBaseTicketOneHistory()+","+ baseTicketOneHistory;
															
									if(baseTicketOneHistory.length()>= 500) {
										baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketOneHistory(baseTicketOneHistory);
								dbTix.setIsEdited(true);
							}
							
							// For Base Ticket 2
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketTwo() != null && dbTix.getBaseTicketTwo() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketTwo().equals(tix.getBaseTicketTwo())){
									tempTicketIdFlag = true;
								}
							} else if(tix.getBaseTicketTwo() != null && tix.getBaseTicketTwo() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketTwoHistory = ticketHDateStr +"/"+ tix.getBaseTicketTwo() +"/"+ tix.getBaseTicketTwoPurPrice();
								if(dbTix.getBaseTicketTwoHistory() != null) {
									baseTicketTwoHistory = dbTix.getBaseTicketTwoHistory()+","+ baseTicketTwoHistory;
															
									if(baseTicketTwoHistory.length()>= 500) {
										baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
								dbTix.setIsEdited(true);
							}
							// For Base Ticket 3
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketThree() != null && dbTix.getBaseTicketThree() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketThree().equals(tix.getBaseTicketThree()))  {
									tempTicketIdFlag = true;
								}
							} else if(tix.getBaseTicketThree() != null && tix.getBaseTicketThree() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketThreeHistory = ticketHDateStr +"/"+ tix.getBaseTicketThree() +"/"+ tix.getBaseTicketThreePurPrice();
								
								if(dbTix.getBaseTicketThreeHistory() != null) {
									baseTicketThreeHistory = dbTix.getBaseTicketThreeHistory()+","+ baseTicketThreeHistory;
															
									if(baseTicketThreeHistory.length()>= 500) {
										baseTicketThreeHistory = baseTicketThreeHistory.substring(baseTicketThreeHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
								dbTix.setIsEdited(true);
							}
							
							if(broker != null && isUpdateTN && isTnEvent) {
								if(dbTix.getTnCategoryTicketGroupId() == null || dbTix.getTnCategoryTicketGroupId() == 0) { // Add tickets as new tickets if it is not exist in POS (i.e. tn group id is not present)
									//dbTix.setShippingMethodSpecialId(tix.getShippingMethodSpecialId());
									dbTix.setNearTermOptionId(tix.getNearTermOptionId());
									dbTix.setExpectedArrivalDate(tix.getExpectedArrivalDate());
									dbTix.setIsEdited(true);
									
									tnNewCatTix.add(dbTix);
									
								} else if(!dbTix.getTnPrice().equals(tix.getTnPrice()) || // Else change it in pos if price or other details has been changed.
										!dbTix.getShippingMethodSpecialId().equals(tix.getShippingMethodSpecialId()) ||
										!dbTix.getNearTermOptionId().equals(tix.getNearTermOptionId()) ||
										dbTix.getExpectedArrivalDate().compareTo(tix.getExpectedArrivalDate()) != 0) {
									
									dbTix.setTnPrice(tix.getTnPrice());
									//dbTix.setShippingMethodSpecialId(tix.getShippingMethodSpecialId());
									dbTix.setNearTermOptionId(tix.getNearTermOptionId());
									dbTix.setExpectedArrivalDate(tix.getExpectedArrivalDate());
									
//										dbTix.setIsPosUpdate(true);
									dbTix.setIsEdited(true);
									
									tnUpdateCatTix.add(dbTix);
								}
							}
							
							if(!dbTix.getShippingMethodSpecialId().equals(tix.getShippingMethodSpecialId())){
								dbTix.setShippingMethodSpecialId(tix.getShippingMethodSpecialId());
								dbTix.setIsEdited(true);
							}
							
							dbTix.setBaseTicketOne(tix.getBaseTicketOne());
							dbTix.setBaseTicketTwo(tix.getBaseTicketTwo());
							dbTix.setBaseTicketThree(tix.getBaseTicketThree());
							
							dbTix.setCategoryId(tix.getCategoryId());
							dbTix.setTicketId(tix.getTicketId());
							dbTix.setItemId(tix.getItemId());
							dbTix.setVenueConfigurationZoneId(tix.getVenueConfigurationZoneId());
							dbTix.setFloorCap(tix.getFloorCap());
							
							dbTix.setActualPrice(tix.getActualPrice());
							dbTix.setVividPrice(tix.getVividPrice());
//								dbTix.setTickpickPrice(tix.getTickpickPrice());
							dbTix.setScoreBigPrice(tix.getScoreBigPrice());
							dbTix.setFanxchangePrice(tix.getFanxchangePrice());
							dbTix.setTicketcityPrice(tix.getTicketcityPrice());
							dbTix.setTmatZone(tix.getTmatZone());
							dbTix.setTnExchangeEventId(tix.getTnExchangeEventId());
							
							if(dbTix.getIsEdited()){
								dbTix.setLastUpdated(currentTime);
								exFinalCatTixMap.put(key, dbTix);
							}
								
						} else { // else delete existing ticket as it does not exist in newly generated tickets..
//								dbTix.setStatus(DE)
							tnRemoveCatTix.add(dbTix);
							
							dbTix.setStatus("DELETED");
							dbTix.setLastUpdated(currentTime);
							dbTix.setReason("Ticket not exist in TMAT");
							exFinalCatTixMap.put(key, dbTix);
						}
					}
					
					
				
					for(String key:catTixMap.keySet()) {  // Add remaining newly generated tickets to db.
						 TixCityZonesPricingCategoryTicket newTix = catTixMap.get(key);
						 priceHistory = priceHDateStr+"-"+newTix.getActualPrice();
						newTix.setPriceHistory(priceHistory);
						
						ticketIdHistory = ticketHDateStr +"/"+ newTix.getTicketId() +"/"+ newTix.getPurPrice();
						newTix.setTicketIdHistory(ticketIdHistory);
						
						if (newTix.getBaseTicketOne() != 0) {
							baseTicketOneHistory = ticketHDateStr +"/"+ newTix.getBaseTicketOne() +"/"+ newTix.getBaseTicketOnePurPrice();
							newTix.setBaseTicketOneHistory(baseTicketOneHistory);
						}
						
						// For Base Ticket 2
						if (newTix.getBaseTicketTwo() != 0) {
							baseTicketTwoHistory = ticketHDateStr +"/"+ newTix.getBaseTicketTwo() +"/"+ newTix.getBaseTicketTwoPurPrice();
							newTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
						}
						// For Base Ticket 3
						if (newTix.getBaseTicketThree() != 0) {
							baseTicketThreeHistory = ticketHDateStr +"/"+ newTix.getBaseTicketThree() +"/"+ newTix.getBaseTicketThreePurPrice();
							newTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
						}
						
						newTix.setLastUpdated(currentTime);
						newTix.setCreatedDate(currentTime);
						exFinalCatTixMap.put(key, newTix);
						
						if(broker != null && isUpdateTN && isTnEvent) {
							tnNewCatTix.add(newTix);
						}
					}
					 
					 if(broker != null && isUpdateTN && isTnEvent) {
						 
						 int addCount=0;
						// boolean isCreateListingEnabled = Counter.isCreateListingEnabled(broker);
						// if(isCreateListingEnabled) {
							 if(!tnNewCatTix.isEmpty()) {
								 Map<String, Integer> posVenueCatIdMap = new HashMap<String, Integer>();
								 try {
									 List<PosVenueCategory> posVenueCatList = InduxDAORegistry.getPosVenueCategoryDAO().getAllVenueCategorysByVenueId(posEvent.getVenueId(),broker.getPosBrokerId());
									 for (PosVenueCategory posVenueCategory : posVenueCatList) {
									
										 //Tamil : select only venue configuration zone id mapped venue configurations for zones pricing listings
										 if(posVenueCategory.getVenueConfigZoneId() != null && posVenueCategory.getVenueConfigZoneId() > 0) {
											 String key = posVenueCategory.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
											 posVenueCatIdMap.put(key, posVenueCategory.getId());	
										 }
									 }
									//System.out.println("venue cat size..."+posVenueCatIdMap.size());
								 } catch (Exception e) {
									 e.printStackTrace();
									 isErrorOccured = true;
									 error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									 error.setExample(""+e.fillInStackTrace());
									 error.setMessage("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ": While loading POS VenueCategors :ErrorMsg:"  );
									 error.setProcess("Loading POS VenueCategors.");
									 error.setEventId(event.getId());
									 error.setTimeStamp(new Date());
									 errorList.add(error);
									 System.err.println("TZP 9 : Error From While Loading POS VenueCategors.."+eventId);
								 }
								 
								 for (TixCityZonesPricingCategoryTicket tix : tnNewCatTix) {
									 /*if(tix.getQuantity() == 2 && tix.getTnPrice()<50){
										 continue;
									 }*/
									 if(excludeZones != null && broker != null && tix.getTmatZone() != null) {
										 String exZoneKey = broker.getId()+"_"+tix.getTmatZone().toUpperCase(); 
										 if(excludeZones.contains(exZoneKey)) {
											 continue;
										 }
									 }
									 
									 tix.setPosEventId(posEvent.getId());
									 tix.setPosVenueId(posEvent.getVenueId());
									 tix.setPosExchangeEventId(posEvent.getExchangeEventId());
									 
									 Integer posVenueCategoryId = null;
									 String venueCategorykey = tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
									 posVenueCategoryId = posVenueCatIdMap.get(venueCategorykey);
									 if(null == posVenueCategoryId || posVenueCategoryId == -1) {
										 posVenueCategoryId = InduxDAORegistry.getPosVenueCategoryDAO().save(tix,broker.getPosBrokerId(),autopricingProduct);
										 posVenueCatIdMap.put(venueCategorykey, posVenueCategoryId);
	//									System.out.println("new venue cat id.."+posVenueCategoryId);
									 }
									 if(null == posVenueCategoryId || posVenueCategoryId == -1) {
										 isErrorOccured = true;
										 error = new AutoPricingError();
										 error.setProductId(autopricingProduct.getId());
										 error.setMessage("Error While Creating VenueCategory");
										 error.setExample("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
										 error.setProcess("ADD Venuecategory.");
										 error.setEventId(event.getId());
										 error.setTimeStamp(new Date());
										 errorList.add(error);
										 log.error("TZP 10 :  Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
										 System.err.println("TZP 10 :  Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
										 continue;
									 }
									 tix.setPosVenueCategoryId(posVenueCategoryId);
								 
									 tix.setTnBrokerId(exEvent.getTicketNetworkBrokerId());
									 if(Counter.isMinuteAddAllow(broker)){
		//								 System.out.println("LRM ADD begins....."+new Date()+"...count.."+eInsert);
										try {
											Integer tnCategoryTicketGroupId = InduxDAORegistry.getPosCategoryTicketGroupDAO().save(autopricingProduct,tix,broker.getPosBrokerId());
											if(tnCategoryTicketGroupId!=-1l){
												tix.setTnCategoryTicketGroupId(tnCategoryTicketGroupId);
												broker.addMinuteUpdateCounter();
												addCount++;
												eInsert++;
											}
										 } catch(Exception e) {
											 isErrorOccured = true;
											 error = new AutoPricingError();
											 error.setProductId(autopricingProduct.getId());
											 error.setMessage("Error while creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
											 error.setExample(""+e.fillInStackTrace());
											 error.setProcess("Ticket Group Creation..");
											 error.setEventId(eventId);
											 error.setTimeStamp(new Date());
											 errorList.add(error);
											 log.error("TZP 11 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
											 System.err.println("TZP 11 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
											 e.printStackTrace();
										 }
									 }
								 }
							 }
						 //}
						 
						 for(TixCityZonesPricingCategoryTicket cat:tnUpdateCatTix){
									
							 try {
								 
								if(Counter.isMinuteUpdateAllow(broker)) {
//									System.out.println("LRM Update begins....."+new Date()+"...count.."+eUpdate);
									InduxDAORegistry.getPosCategoryTicketGroupDAO().updateCategoryTicketGroup(cat,broker.getPosBrokerId());
									broker.addMinuteUpdateCounter();
									eUpdate++;
								 }
							} catch (Exception e) {
								e.printStackTrace();
									isErrorOccured = true;
									error = new AutoPricingError();
									error.setProductId(autopricingProduct.getId());
									error.setMessage("While Updating Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
									error.setExample(""+e.fillInStackTrace());
									error.setProcess("POS Update Category.");
									error.setEventId(eventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("TZP 12 : Error While POS Update:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
									System.err.println("TZP 12 :  Error From POS Update :"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" );
							}
						 }
						 
						 for(TixCityZonesPricingCategoryTicket cat:tnRemoveCatTix){
									
							 try {
								 if(cat.getTnCategoryTicketGroupId()!=null){	
									 if(Counter.isMinuteDeleteAllow(broker)) {
//										 System.out.println("LRM delete begins..4..."+new Date()+"...count.."+eremovet);
									
										 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
										 broker.addMinuteUpdateCounter();
										 eremovet++;
										 cat.setReason("Ticket not exist in TMAT and Deleted from TN");
									}
								 }
							 } catch (Exception e) {
								 cat.setReason("Ticket not exist in TMAT and Exception in TN");
								 e.printStackTrace();
									isErrorOccured = true;
									error = new AutoPricingError();
									error.setProductId(autopricingProduct.getId());
									error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
									error.setProcess("POS Remove Category.");
									error.setExample(""+e.fillInStackTrace());
									error.setEventId(eventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("TZP 13 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
									System.err.println("TZP 13 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
							 }
						 }
						 for(TixCityZonesPricingCategoryTicket cat:updatedBrokerTicketList){
								
							 try {
								 if(cat.getTnCategoryTicketGroupId()!=null){	
									 if(Counter.isMinuteDeleteAllow(broker)) {
//										 System.out.println("LRM delete begins..4..."+new Date()+"...count.."+eremovet);
									
										 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
										 broker.addMinuteUpdateCounter();
										 eremovet++;
										 cat.setReason(cat.getReason()+" and Deleted from TN");
									}
								 }
							 } catch (Exception e) {
								 cat.setReason(cat.getReason()+" and Exception in TN");
								 e.printStackTrace();
									isErrorOccured = true;
									error = new AutoPricingError();
									error.setProductId(autopricingProduct.getId());
									error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
									error.setProcess("POS Remove Category.");
									error.setExample(""+e.fillInStackTrace());
									error.setEventId(eventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("TZP 13 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
									System.err.println("TZP 13 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:"  );
							 }
						 }
						 String key = null;
						 TixCityZonesPricingCategoryTicket catTix = null;
						 for (TixCityZonesPricingCategoryTicket tnCatTix :tnNewCatTix) {
							key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
							catTix = exFinalCatTixMap.get(key);
							 if(catTix != null) {
								 catTix.setCreatedDate(currentTime);
								 catTix.setTnCategoryTicketGroupId(tnCatTix.getTnCategoryTicketGroupId());
//								 catTix.setPosPurchaseOrderId(tnCatTix.getPosPurchaseOrderId());
								 catTix.setTnPrice(tnCatTix.getTnPrice());
								 catTix.setIsEdited(true);
								 catTix.setLastUpdated(currentTime);
							 }
							 
						 }
						 
						 for (TixCityZonesPricingCategoryTicket tnCatTix : tnUpdateCatTix) {
							 key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
							 catTix = exFinalCatTixMap.get(key);
								
							 if(catTix != null) {
								 catTix.setTnPrice(tnCatTix.getTnPrice());
								 catTix.setIsEdited(true);
								 catTix.setLastUpdated(currentTime);
							 }
							 
						 }
						 
						 for (TixCityZonesPricingCategoryTicket tnCatTix : tnRemoveCatTix) {
							 key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
							 catTix = exFinalCatTixMap.get(key);
								
							 if(catTix != null) {
								 catTix.setStatus("DELETED");
//								 tnCatTix.setTnPrice(0.0);
								 catTix.setIsEdited(true);
								 catTix.setLastUpdated(currentTime);
							 }
							 
						 }
					 }
					 DAORegistry.getTixCityZonesPricingCategoryTicketDAO().saveOrUpdateAll(updatedBrokerTicketList);
					 DAORegistry.getTixCityZonesPricingCategoryTicketDAO().saveOrUpdateAll(new ArrayList<TixCityZonesPricingCategoryTicket>(exFinalCatTixMap.values()));
					 
					 tInsert += eInsert;
					 tUpdate += eUpdate;
					 tremovet += eremovet;
					 
					 if(broker!=null){
						 Integer addCount = brokerAddCountMap.get(broker.getId());
						 if(addCount==null){
							 addCount = 0;
						 }
						 addCount= addCount + eInsert;
						 brokerAddCountMap.put(broker.getId(),addCount);
						 
						 Integer deleteCount = brokerDeleteCountMap.get(broker.getId());
						 if(deleteCount==null){
							 deleteCount = 0;
						 }
						 deleteCount= deleteCount + eremovet;
						 brokerDeleteCountMap.put(broker.getId(),deleteCount);
						 
						 Integer updateCount = brokerUpdateCountMap.get(broker.getId());	 
						 if(updateCount==null){
							 updateCount = 0;
						 }
						 updateCount= updateCount + eUpdate;
						 brokerUpdateCountMap.put(broker.getId(),updateCount);
						
					 }
					 
					 
					  
					 //System.out.println(eventId+"...: Insert :"+insert+" Updat : "+update+" : tremovet :"+remove);
					 //log.info(eventId+"...: Insert :"+insert+" Updat : "+update+" : ZeroUpdate :"+zeroUpdate+" ntExist :"+notExisit);
					 System.out.println("RTWZonesPricing  : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
//					 log.info("RTWZonesPricing : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
					 
					 
				 } catch (Exception e) {
					 isErrorOccured = true;
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Updating TMAT TixCity Zone Pricing Ticket."+eventId);
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("RTWZonesPricing Ticket Updation..");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("TZP 14 : Error while Updating TMAT TixCity ZonePricing Ticket."+eventId);
					 System.err.println("TZP 14 : Error while Updating TMAT TixCity ZonePricing Ticket."+eventId);
					 e.printStackTrace();
				}
			}
			
			List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
			
			for(Integer brokerId:brokerAddCountMap.keySet()){
				int count = brokerAddCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Insert");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerUpdateCountMap.keySet()){
				int count = brokerUpdateCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Update");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerDeleteCountMap.keySet()){
				int count = brokerDeleteCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Delete");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);
			DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
			System.out.println("RTWZonesPricing Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
//			log.info("RTWZonesPricing Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
			running = false; 
		 }catch(Exception e){
			 running = false;
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Loading Common Properties.");
//			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Loading common Properties..");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("TZP 15 : Error while Loading Common Properties.");
			 System.err.println("TZP 15 : Error while Loading Common Properties.");
			 e.printStackTrace();
		 }

		 String subject,fileName;
		 Map<String, Object> map;
			
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {
					log.info("CT error size......"+errorList.size());
					
					DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
					subject = "RTWZonesPricing Scheduler job failed :";
					fileName = "templates/autopricing-job-failure-message.txt";
					
					//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
					
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
				} catch (Exception e) {
					log.error("TZP 16 : Error while Inserting Error Listings in TMAT.");
					 System.err.println("TZP 16 : Error while Inserting Error Listings in TMAT.");
					e.printStackTrace();
				}
			}
	 }
	 
	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
//			if(!running){
//				running=true;
				System.out.println("RTWZonesPricing Job Called.." + new Date() + ": " + running);
				log.info(" RTWZonesPricing Job Called.." + new Date() + ": " + running);
				try{
					
					processTixCityZonesPricingTickets();
					System.out.println("Job finished @ " + new Date());
				}catch(Exception e){
					e.printStackTrace();
				}
//				running = false;
//			}
	}


	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		TixCityZonePricingScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		TixCityZonePricingScheduler.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RTWZonesPricing").getLastRunTime();
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		TixCityZonePricingScheduler.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}


	public static void setNextRunTime(Date nextRunTime) {
		TixCityZonePricingScheduler.nextRunTime = nextRunTime;
	}

}