package com.rtw.autopricing.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;


public class Counter {
	
	
	private static Integer TICKET_COUNT_MAX_LIMIT = 700000;//499900
	public static Map<Integer,Integer> brokerTixCoutMap = new HashMap<Integer, Integer>();
	public static Map<Integer,Date> brokerTimeStampMap = new HashMap<Integer, Date>();

	public static boolean  isMinuteAllow(Broker broker)throws Exception {
		if(broker.getMinutePOSUpdateLimit()>broker.getMinuteUpdateCounter()){
			return true;
		}else{
			Long waitTime = 60*1000 - (new Date().getTime()- broker.getMinuteUpdateTimeStamp().getTime());
			if(waitTime>0){
				Thread.sleep(waitTime);
			}
			broker.setMinuteUpdateTimeStamp(new Date());
			broker.setMinuteUpdateCounter(0);
		}
		return true;
	}
	public static boolean  isMinuteAddAllow(Broker broker)throws Exception {
		if(isCreateListingEnabledNew(broker)) {
			addBrokerTixCount(broker.getId());
			return isMinuteAllow(broker);
		}
		return false;
	}

	public static boolean  isMinuteUpdateAllow(Broker broker) throws Exception {
		return isMinuteAllow(broker);
	}
	public static boolean  isMinuteDeleteAllow(Broker broker) throws Exception {
		deleteBrokerTixCount(broker.getId());
		return isMinuteAllow(broker);
	}
	
	/*public static boolean isCreateListingEnabled(Broker broker) throws Exception {
		return true;
	}*/
	
	public static boolean isCreateListingEnabledNew(Broker broker) throws Exception {
		
		Integer count = brokerTixCoutMap.get(broker.getId());
		Date countDate = brokerTimeStampMap.get(broker.getId());
		if(count == null || countDate == null || (new Date().getTime()- countDate.getTime())>= 5*60*1000) {
			count = getLatestListingsCountFromDb(broker);
			countDate = new Date();
			brokerTixCoutMap.put(broker.getId(), count);	
			brokerTimeStampMap.put(broker.getId(), countDate);
			
			if(count>= TICKET_COUNT_MAX_LIMIT) {
				System.out.println("Count Reached MAx Limit : "+count+" Broker : "+broker.getName()+" : "+new Date());
			}
		}
		if(count>= TICKET_COUNT_MAX_LIMIT) {
			return false;
		}
		return true;
	}
	
	public static Integer getLatestListingsCountFromDb(Broker broker) throws Exception {
		
		Integer ticketCount = 0;
		try {
			ticketCount = InduxDAORegistry.getPosCategoryTicketGroupDAO().getLastestListingsCountByBroker(broker);
		} catch(Exception e) {
			e.printStackTrace();
		}
		if(ticketCount == 0) {
			ticketCount = 0;
		}
		return ticketCount;
	}
	
	public static Integer getBrokerTixCount(Integer brokerId) {
		Integer count = brokerTixCoutMap.get(brokerId);
		if(count == null) {
			count =0;
		}
		return count;
	}
	
	public static void addBrokerTixCount(Integer brokerId) {
		Integer count = brokerTixCoutMap.get(brokerId);
		if(count == null) {
			brokerTixCoutMap.put(brokerId,1);
		} else {
			brokerTixCoutMap.put(brokerId,brokerTixCoutMap.get(brokerId)+1);
		}
	}
	public static void deleteBrokerTixCount(Integer brokerId) {
		Integer count = brokerTixCoutMap.get(brokerId);
		if(count == null) {
			brokerTixCoutMap.put(brokerId,0);
		} else {
			brokerTixCoutMap.put(brokerId,brokerTixCoutMap.get(brokerId)-1);
		}
	}
}
