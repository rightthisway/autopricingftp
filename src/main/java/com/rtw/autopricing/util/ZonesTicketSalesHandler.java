package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.autopricing.util.common.SharedProperty;
import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.ftp.data.SoldCategoryTicket;
import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.data.TicketListingCrawl;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;
import com.rtw.autopricing.ftp.utils.mail.MailIdProperty;
import com.rtw.autopricing.ftp.zonetickets.dao.implementaion.ZoneTicketDAORegistry;


public class ZonesTicketSalesHandler extends QuartzJobBean implements StatefulJob {
	static boolean running=false;
	static Logger logger = LoggerFactory.getLogger(ZonesTicketSalesHandler.class);
	private static SharedProperty sharedProperty;
	private static Date lastrunTime = null;
	private static Property jobProperty = null;
	private static MailIdProperty mailIdProperty;
	
	public static void updateZonesSoldCategoryTickets(String fromDateStr,String toDateStr) throws Exception {
	   List<SoldCategoryTicket> soldCatTicketList = new ArrayList<SoldCategoryTicket>();
	   SoldCategoryTicket soldCategoryTicket = null;
	   try{
			List<Integer> soldCatTixIdList = ZoneTicketDAORegistry.getZoneTicketsTicketGroupDAO().getZoneTicketsSoldTicketGroupIds(fromDateStr, toDateStr);
			String eventIds="";
			if(soldCatTixIdList != null && !soldCatTixIdList.isEmpty()) {
				AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RewardTheFan Listings");
				List<ZoneTicketsProcessorCategoryTicket> ticketList = DAORegistry.getZoneTicketsProcessorCategoryTicketDAO().getAllZoneTicektsProcessorCategoryTicketsByIds(soldCatTixIdList);
				for (ZoneTicketsProcessorCategoryTicket categoryTicket : ticketList) {
					categoryTicket.setStatus("SOLD");
					categoryTicket.setLastUpdated(new Date());
					
					soldCategoryTicket = new SoldCategoryTicket();
					soldCategoryTicket.setProductId(autopricingProduct.getId());
					soldCategoryTicket.setEventId(categoryTicket.getEventId());
					soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
					soldCatTicketList.add(soldCategoryTicket);
					eventIds += categoryTicket.getEventId() + ",";
				}
				
				DAORegistry.getSoldCategoryTicketDAO().saveAll(soldCatTicketList);
				DAORegistry.getZoneTicketsProcessorCategoryTicketDAO().updateAll(ticketList);
				
				for(ZoneTicketsProcessorCategoryTicket categoryTicket : ticketList) {
					forceCrawlEventAndSendMail(autopricingProduct, categoryTicket);
				}
				
				/*if(!eventIds.isEmpty() && !eventIds.trim().equals(",")){
					eventIds = eventIds.substring(0,eventIds.length()-1);
					String url = sharedProperty.getBrowseUrl()+"WSForceEvents";		
					HttpClient hc = new DefaultHttpClient();
					HttpPost hp = new HttpPost(url);
					NameValuePair nameValuePair = new BasicNameValuePair("eventIds", eventIds);
					NameValuePair nameValuePair1 = new BasicNameValuePair("postBackUrl", sharedProperty.getAutoPricingUrl()+"ForceCrawledEventResult");
					NameValuePair nameValuePair2 = new BasicNameValuePair("priority", "high");
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(nameValuePair);
					parameters.add(nameValuePair1);
					parameters.add(nameValuePair2);
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
					hp.setEntity(entity);
					HttpResponse res = hc.execute(hp);
					String content = EntityUtils.toString(res.getEntity());
					System.out.println(content);
				}*/
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				jobProperty.setValue(dateFormat.format(lastrunTime));
				DAORegistry.getPropertyDAO().update(jobProperty);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
			
	}
	
	// to force crawl an event and send fullfillment details for zonetickets order.
	public static boolean forceCrawlEventAndSendMail(AutopricingProduct autopricingProduct, ZoneTicketsProcessorCategoryTicket categoryTicket){
		try{
			Date now = new Date();
			String url = sharedProperty.getBrowseUrl()+"WSForceEvents";		
			HttpClient hc = new DefaultHttpClient();
			HttpPost hp = new HttpPost(url);
			NameValuePair nameValuePair = new BasicNameValuePair("eventIds", "" + categoryTicket.getEventId());
			NameValuePair nameValuePair1 = new BasicNameValuePair("postBackUrl", sharedProperty.getAutoPricingUrl()+"ForceCrawledEventResult");
			NameValuePair nameValuePair2 = new BasicNameValuePair("priority", "high");
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			parameters.add(nameValuePair);
			parameters.add(nameValuePair1);
			parameters.add(nameValuePair2);
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
			hp.setEntity(entity);
			HttpResponse res = hc.execute(hp);
			String content = EntityUtils.toString(res.getEntity());
			
			Collection<TicketListingCrawl> crawls = DAORegistry.getQueryManagerDAO().getActiveCrawlsByEventId(""+ categoryTicket.getEventId());
			if(crawls != null){
				boolean flag = checkCrawls(crawls, now);
				while(!flag){
					flag = checkCrawls(crawls, now);
					if(flag == false){
						Thread.sleep(5000);
						crawls = DAORegistry.getQueryManagerDAO().getActiveCrawlsByEventId(""+ categoryTicket.getEventId());
					}
					// to avoid infinite loop we will exit after 2 mins.
					if(new Date().getTime() - now.getTime() >= 2*60*1000){
						flag = true;
					}
				}
			
			System.out.println("Crawled success for eventId = "+ categoryTicket.getEventId());
			Event event = DAORegistry.getEventDAO().get(categoryTicket.getEventId());
			if(event != null){
				event.setParentCategoryId(DAORegistry.getEventDAO().getParentCategoryIdByEventId(event.getId()));
				
				ExchangeEvent exchangeEvent = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getZoneTicketsProcessorExchangeEventsByEventId(event.getId());
				List<Ticket> tickets = new ArrayList<Ticket>();
				List<Ticket> eTickets = new ArrayList<Ticket>();
				List<Ticket> regTickets = new ArrayList<Ticket>();
				if(exchangeEvent!=null){
					exchangeEvent.setExposure("1-OXP");
					double minPrice = 0;
					double eMinPrice = 0;
					double regMinPrice = 0;
					List<CategoryTicket> catTixList = CategoryGroupManager.computeCategoryTickets(event, exchangeEvent, 1, 1, 0d, 0d, 0d, 0d, 0d,0d, autopricingProduct,null);
					//System.out.println("category tickets "+ catTixList.size());
					Ticket t = null;
					int count=0;
					int ticketCount=0;
					int eTicketCount = 0;
					int regTicketCount = 0;
					int numberOfETickets = 2;
					int numberOfRegTickets = 3;
					Map<Integer,Boolean> map = new HashMap<Integer, Boolean>();
					for(CategoryTicket catTix:catTixList){
	//					System.out.println(catTix.getActualPrice() + ":" +  catTix.getSection()+":"+(ticketDetail.getSection()) + ":" + catTix.getQuantity() + ":" + ticketDetail.getQuantity());
						if(map.containsKey(catTix.getTicket().getId())){
							continue;
						}
						t = catTix.getTicket();
						map.put(t.getId(), true);
						
						if(t.getTicketDeliveryType()==null){
							ticketCount = numberOfRegTickets;
							count = regTicketCount;
							minPrice = regMinPrice;
						}else{
							ticketCount = numberOfETickets;
							count = eTicketCount;
							minPrice = eMinPrice;
						}
						if((count<= ticketCount ||minPrice > catTix.getActualPrice()) && (catTix.getSection().equalsIgnoreCase(categoryTicket.getSection()) && t.getQuantity()>= categoryTicket.getQuantity())){
	//						t = catTix.getTicket();
							if(count==ticketCount){
								Ticket toBeRemoved = null;
								double firstPrice =0d;
								if(t.getTicketDeliveryType()==null){
									for(Ticket ticket:regTickets){
										if(toBeRemoved==null){
											toBeRemoved = ticket;
											firstPrice =  ticket.getPurchasePrice();
										}else if(firstPrice<ticket.getPurchasePrice() && ticket.getQuantity()>ticket.getQuantity()){
											toBeRemoved = ticket;
											firstPrice = ticket.getPurchasePrice();
										}
									}
									regTickets.remove(toBeRemoved);
									regTicketCount--;
								}else {
									for(Ticket ticket:eTickets){
										if(toBeRemoved==null){
											toBeRemoved = ticket;
											firstPrice =  ticket.getPurchasePrice();
										}else if(firstPrice<ticket.getPurchasePrice() && ticket.getQuantity()>ticket.getQuantity()){
											toBeRemoved = ticket;
											firstPrice = ticket.getPurchasePrice();
										}
									}
									eTicketCount--;
									eTickets.remove(toBeRemoved);
								}
							}
							
							if(t.getTicketDeliveryType()==null){
								regTickets.add(t);
								regTicketCount++;
								regMinPrice = -1d;
								for(Ticket ticket:regTickets){
									if(regMinPrice== -1d || regMinPrice>ticket.getPurchasePrice()){
										regMinPrice = ticket.getPurchasePrice();
									}
								}
							}else{
								eTickets.add(t);
								eTicketCount++;
								eMinPrice = -1d;
								for(Ticket ticket:eTickets){
									if(eMinPrice== -1d || eMinPrice>ticket.getPurchasePrice()){
										eMinPrice = ticket.getPurchasePrice();
									}
								}
							}
						}
					}
					if(regTickets!=null){
						tickets.addAll(regTickets);
					}
					if(regTickets!=null){
						tickets.addAll(eTickets);
					}
					
					//System.out.println("Tickets size "+tickets.size());
					Map<String, Object> orderMap = new HashMap<String, Object>();
					if(tickets != null && tickets.size() != 0){
						Collections.sort(tickets, new Comparator<Ticket>(){
							public int compare(Ticket t1, Ticket t2) {
								return t1.getPurchasePrice().compareTo(t2.getPurchasePrice());
							}
						});
						Ticket ticket = tickets.get(0);
						if(ticket.getSection() == null){
							ticket.setSection("");
						}
						if(ticket.getRow() == null){
							ticket.setRow("");
						}
						orderMap.put("ticket", ticket);
						orderMap.put("venue", DAORegistry.getVenueDAO().get(event.getVenueId()));
						/*SimpleDateFormat formatDate = new SimpleDateFormat("MM-dd-yyyy");
						SimpleDateFormat formatTime = new SimpleDateFormat("hh:mm a");
						if(event.getLocalDate() != null){
							orderMap.put("date", formatDate.format(event.getLocalDate()));
						}else{
							orderMap.put("date", "TBD");
						}
						if(event.getLocalTime() != null){
							orderMap.put("time", formatTime.format(event.getLocalTime()));
						}else{
							orderMap.put("time", "TBD");
						}*/
						orderMap.put("dateTime", event.getLocalDateTime());
						orderMap.put("event", event);
						orderMap.put("displayError",  "none;");
						orderMap.put("displayDetails",  "block;");
					}else{
						// not getting suitable ticket to fullfill this order
						orderMap.put("displayError",  "block;");
						orderMap.put("displayDetails",  "none;");
					}
					String subject = "RewardTheFan Order Fullfillment Details";
					String fileName = "templates/mail-zonetickets-order-fullfillment.txt";
					String toAddress = mailIdProperty.getZoneticketsOrderFullFillMailTo(), ccAddress = "";
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, orderMap);	
					System.out.println("Email sent for zone tickets ticket group id = "+ categoryTicket.getZoneTicketsTicketGroupId());
				}else{
					System.out.println("No exchange event found for eventId = "+ categoryTicket.getEventId() + " and TicketId = "+ categoryTicket.getZoneTicketsTicketGroupId());
				}
				
			}
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean checkCrawls(Collection<TicketListingCrawl> crawls, Date now){
		try{
			int count = 0;
			for(TicketListingCrawl crawl : crawls){
				if(crawl.getEndCrawl() != null && (now.getTime() - crawl.getEndCrawl().getTime()) <= 15*60*1000){
					count++;
				}
			}
			//System.out.println("count = "+ count);
			if(count == crawls.size()){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public static void getTodaySalesFromZones() {
		
		if(isRunning()){
			return ;
		}
		setRunning(true);
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress =resourceBundle.getString("emailNotificationTo");
		String ccAddress=resourceBundle.getString("emailNotificationCCTo");
		
		logger.info("Sales Collection Job Started @ " + new Date());
		System.out.println("Sales Collection Job Started @ " + new Date());
			
			try {
				
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//Date today = lastrunTime;
				Date now = new Date();
				String fromDateStr = "";
				jobProperty = DAORegistry.getPropertyDAO().getPropertyByName("autoweb.zonetickets.sales.handler.last.runtime");
				fromDateStr = jobProperty.getValue();
				if(fromDateStr == null || fromDateStr.equals("")){
					fromDateStr = df.format(now)+" 00:00:00";
				}
				/*if(today == null){
					today = new Date();
					fromDateStr = df.format(today)+" 00:00:00";
				}else{
					fromDateStr = dateFormat.format(today);
				}*/
				
				String toDateStr = dateFormat.format(now);//+" 23:59:59";
				lastrunTime = now;
				List<SoldCategoryTicket> soldCatTicketList = new ArrayList<SoldCategoryTicket>();
				/*System.out.println("From date "+ fromDateStr);
				System.out.println("To date "+ toDateStr);*/
				try {
					updateZonesSoldCategoryTickets(fromDateStr, toDateStr);
				} catch (Exception e) {
					e.printStackTrace();
					isErrorOccured = true;
					error = new AutoPricingError();
					error.setMessage("Error While getting Todays Sales for Zone tickets");
					error.setExample("Error While getting Todays Sales for Zone tickets");
					error.setProcess("Get Todays Sales");
					error.setEventId(0);
					error.setTimeStamp(new Date());
					errorList.add(error);
					logger.error("Error While getting Todays Sales for Zone tickets");
					System.out.println("Error While getting Todays Sales for Zone tickets");
				}
					
				logger.error("Last Hour Sales count:"+ soldCatTicketList.size() +":@:"+new Date());
				System.out.println("Last Hour Sales count:"+ soldCatTicketList.size() +":@:"+new Date());
				running=false;
			} catch (Exception e) {
				running=false;
				e.printStackTrace();
				isErrorOccured = true;
				error = new AutoPricingError();
				error.setMessage("Error in CategoryTicket SalesHandler");
				error.setExample("Error in CategoryTicket SalesHandler");
				error.setProcess("Get Todays Sales");
				error.setEventId(0);
				error.setTimeStamp(new Date());
				errorList.add(error);
				logger.error("Error in CategoryTicket SalesHandler");
				System.out.println("Error in CategoryTicket SalesHandler");
			}
			String subject,fileName;
			Map<String, Object> map;
			
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {

					DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
					
					subject = "Autopricing Sales Handler Job failed.";
					fileName = "templates/autopricing-job-failure-message.txt";
					
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("Sales Collection Job finished @ " + new Date());
			logger.info("Sales Collection Job finished @ " + new Date());
	
	}

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		System.out.println("SALES Collection Job Called.." + new Date() + ": " + running);
		logger.info("SALES Collection Job Called.." + new Date() + ": " + running);
		try{
			
			getTodaySalesFromZones();
			System.out.println("Sales Scheduler Job finished @ " + new Date());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		ZonesTicketSalesHandler.running = running;
	}

	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public static void setSharedProperty(SharedProperty sharedProperty) {
		ZonesTicketSalesHandler.sharedProperty = sharedProperty;
	}

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public static void setMailIdProperty(MailIdProperty mailIdProperty) {
		ZonesTicketSalesHandler.mailIdProperty = mailIdProperty;
	}

	
	
}