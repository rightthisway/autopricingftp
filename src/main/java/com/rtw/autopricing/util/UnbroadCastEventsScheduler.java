package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEvent;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEventAudit;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEventAudit;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEventAudit;
import com.rtw.autopricing.ftp.data.SGLastFiveRowCategoryTicket;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEvent;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEventAudit;
import com.rtw.autopricing.ftp.data.SeatGeekUploadCats;
import com.rtw.autopricing.ftp.data.SeatGeekWSTracking;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.UnbroadcastedEvents;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEventAudit;
import com.rtw.autopricing.ftp.enums.SeatGeekWSTrackingStatus;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.zonetickets.dao.implementaion.ZoneTicketDAORegistry;

public class UnbroadCastEventsScheduler extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(UnbroadCastEventsScheduler.class);
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	 
	 public void unbroadCastEvents(){
		 if(isStopped() || isRunning()){
				return ;
		 }
		 setRunning(true);
		 
		 try{
			 //int rtwBrokerId = 5;
			 Date now = new Date();
			 // substracting 15 minutes from current time.
			 Date before15MinsFromNow = new Date(now.getTime() - 30*60*1000);
			 System.out.println("Now.."+now);
			 System.out.println("before 30 mins.."+before15MinsFromNow);
			 
			 Broker rtwBroker = BrokerUtils.getBrokerById(5);//RTW
			 //Broker rtwTwoBroker = BrokerUtils.getBrokerById(10);//RTW-2
			 //Broker tixcityBroker = BrokerUtils.getBrokerById(AutoExchangeEventLoader.TIXCITY_BROKER_ID);//Tixcity
			 List<Broker> brokerList = new ArrayList<Broker>();
			 brokerList.add(rtwBroker);
			 //brokerList.add(rtwTwoBroker);
			 //brokerList.add(tixcityBroker);
			 
			 Set<Integer> admitoneIdsSet = new HashSet<Integer>();
			 Set<Integer> eventIdsSet = new HashSet<Integer>();
			 String admitoneIds = "";
			 String eventIds = "";
			 String tempAdmitOneIds = "";
			 
			 //Get unbroadcast event ids from RTW and RTW POS
			 for (Broker broker : brokerList) {
				 List<Integer> exchangeEventIds = DAORegistry.getQueryManagerDAO().getExchangeEventIdsToUnbroadcastEventsByBroker(broker,now, before15MinsFromNow);
				// String eventIds = "";
				 if(exchangeEventIds != null && exchangeEventIds.size() >0){
					 for(Integer admitoneId : exchangeEventIds){
						 Event event = DAORegistry.getEventDAO().getEventByAdmitoneId(admitoneId);
						 if(event != null) {
							 if(eventIdsSet.add(event.getId())) {
								 eventIds += event.getId() + ",";	 
							 }
						 } else {
							 if(admitoneIdsSet.add(admitoneId)) {
								 admitoneIds += admitoneId + ",";
							 }
						 }
						 tempAdmitOneIds += admitoneId + ",";
					 }
				 }
			}
			//Get unbroadcast event ids from ZoneTickets(Rewardthefan)
			 List<Integer> rtfEventIds = ZoneTicketDAORegistry.getZoneTicketsTicketGroupDAO().getEventIdsToUnbroadcastEventsFromRewardTheFan(now, before15MinsFromNow);
			 if(rtfEventIds != null && rtfEventIds.size() >0){
				 for(Integer eventId : rtfEventIds){
					 if(eventIdsSet.add(eventId)) {
						 eventIds += eventId + ",";	 
					 }
					 Event event = DAORegistry.getEventDAO().get(eventId);
					 if(event != null && event.getAdmitoneId() != null) {
						 tempAdmitOneIds += event.getAdmitoneId() + ",";
					 }
				 }
			 }
			 
			//Get unbroadcast event ids from RTW and RTW POS
			 /*List<Integer> seatGeekEventIds = ZoneTicketDAORegistry.getZoneTicketsTicketGroupDAO().getEventIdsToUnbroadcastEventsFromSeatGeek(now, before15MinsFromNow);
			 if(seatGeekEventIds != null && seatGeekEventIds.size() >0){
				 for(Integer eventId : seatGeekEventIds){
					 if(eventIdsSet.add(eventId)) {
						 eventIds += eventId + ",";	 
					 }
					 Event event = DAORegistry.getEventDAO().get(eventId);
					 if(event != null && event.getAdmitoneId() != null) {
						 tempAdmitOneIds += event.getAdmitoneId() + ",";
					 }
				 }
			 }*/
			 

			 UnbroadcastedEvents unbroadcastedEvents = DAORegistry.getUnbroadcastedEventsDAO().
				getUnbrodacastedEventsByDate(dbDateFormat.format(now));
			 String admitoneIdsForMail = "";
			 if( !admitoneIds.equals("")){
				 List<String> existingAdmitoneIdsList = new ArrayList<String>();
				 if(unbroadcastedEvents != null){
					 if(unbroadcastedEvents.getAdmitoneIds() != null){
						 for(String admitoneId : unbroadcastedEvents.getAdmitoneIds().split(",")){
							 existingAdmitoneIdsList.add(admitoneId);
						 }
					 }
				 }
				 admitoneIds = admitoneIds.substring(0, admitoneIds.length() - 1);
				 if(existingAdmitoneIdsList != null && existingAdmitoneIdsList.size() > 0){
					 for(String str : admitoneIds.split(",")){
						 if(!existingAdmitoneIdsList.contains(str)){
							 admitoneIdsForMail += str + ",";
						 }
					 }
				 }else{
					 admitoneIdsForMail = admitoneIds + ",";
				 }
			 }
			 String eventIdsForMil = "";
			 if(!eventIds.equals("")){
				 List<String> existingEventIdsList = new ArrayList<String>();
				 if(unbroadcastedEvents != null){
					 if(unbroadcastedEvents.getEventIds() != null){
						 for(String eventId : unbroadcastedEvents.getEventIds().split(",")){
							 existingEventIdsList.add(eventId);
						 }
					 }
				 }
				 eventIds = eventIds.substring(0, eventIds.length() - 1);
				 if(existingEventIdsList != null && existingEventIdsList.size() > 0){
					 for(String str : eventIds.split(",")){
						 if(!existingEventIdsList.contains(str)){
							 eventIdsForMil += str + ",";
						 }
					 }
				 }else{
					 eventIdsForMil = eventIds + ",";
				 }
			 
				 
				 // eventIds = eventIds.substring(0, eventIds.length() - 1);
			
			 
			 // need to unbroadcast all cat tickets for these event ids
			 //Broker rtwBroker = DAORegistry.getBrokerDAO().get(5);//RTW
			// Broker rtw2Broker = DAORegistry.getBrokerDAO().get(10);//RTW-2
			 
			 //InduxDAORegistry.getPosTicketGroupDAO().unbroadcastTicketGroupsByEventIds(broker.getPosBrokerId(), eventIds);
			 
			if(!tempAdmitOneIds.equals("")) {
				tempAdmitOneIds = tempAdmitOneIds.substring(0, tempAdmitOneIds.length() - 1);
				for (Broker broker : brokerList) {
					InduxDAORegistry.getPosCategoryTicketGroupDAO().unBroadcastCategoryTicketsByExchangeEventIds(tempAdmitOneIds, broker.getPosBrokerId());
					InduxDAORegistry.getPosTicketGroupDAO().unbroadcastTicketGroupsByExchangeEventIds(broker,tempAdmitOneIds);
				}
			}
			 
			 
			 System.out.println("Events unbroadcasted for eventIds = "+ tempAdmitOneIds + " at " + new Date());
			 System.out.println("Admitone Ids.."+tempAdmitOneIds);
			 
			 //Unbroadcasting from rewardthefan
			 //deleting from rewardthefan existing listings
			 for (String eventIdStr : eventIds.split(",")) {
				 
				 /*Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr));
				 if(event != null && event.getName().toLowerCase().contains("paul simon")) {
					 continue;
				 }*/
				 ZoneTicketDAORegistry.getZoneTicketsTicketGroupDAO().deleteCategoryTicketGroupsByEventId(Integer.parseInt(eventIdStr));
				 //removeSeatGeekAPIListingsByEventId(Integer.parseInt(eventIdStr));
			 }
			 //unbroadcasting rewardthefan events in autopricing
			 Collection<ZoneTicketsProcessorExchangeEvent> zoneTicketsProcessorExchangeEvent = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsByEventIds(eventIds);
			 Collection<ZoneTicketsProcessorExchangeEventAudit> zoneTicketsProcessorExchangeEventAuditList = new ArrayList<ZoneTicketsProcessorExchangeEventAudit>();
			 for(ZoneTicketsProcessorExchangeEvent event : zoneTicketsProcessorExchangeEvent){
				 if(event.getStatus().equals("ACTIVE")){
					 /*if(event.getEvent() != null && event.getEvent().getName().toLowerCase().contains("paul simon")) {
						 continue;
					 }*/
					 ZoneTicketsProcessorExchangeEventAudit audit = new ZoneTicketsProcessorExchangeEventAudit(event);
					 zoneTicketsProcessorExchangeEventAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 // Need to exclude events from autopricing products
			 
			 //Collection<MiniExchangeEvent> miniExchangeEvents = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsByAdmitoneIds(admitoneIds);
			 Collection<MiniExchangeEvent> miniExchangeEvents = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsByEventIds(eventIds);
			 Collection<MiniExchangeEventAudit> miniExchangeEventsAuditList = new ArrayList<MiniExchangeEventAudit>();
			 for(MiniExchangeEvent event : miniExchangeEvents){
				 if(event.getStatus().equals("ACTIVE") ){//&& event.getTicketNetworkBrokerId() == rtwBrokerId
					 MiniExchangeEventAudit audit = new MiniExchangeEventAudit(event);
					 miniExchangeEventsAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<VipMiniExchangeEvent> vipMiniExchangeEvents = DAORegistry.getVipMiniExchangeEventDAO().getAllVipMiniExchangeEventsByEventIds(eventIds);
			 Collection<VipMiniExchangeEventAudit> vipMiniExchangeEventAuditList = new ArrayList<VipMiniExchangeEventAudit>();
			 for(VipMiniExchangeEvent event : vipMiniExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){//&& event.getTicketNetworkBrokerId() == rtwBrokerId
					 VipMiniExchangeEventAudit audit = new VipMiniExchangeEventAudit(event);
					 vipMiniExchangeEventAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<LastRowMiniExchangeEvent> lastRowMiniExchangeEvents = DAORegistry.getLastRowMiniExchangeEventDAO().getAllLastRowMiniExchangeEventsByEventIds(eventIds);
			 Collection<LastRowMiniExchangeEventAudit> lastRowMiniExchangeEventAuditList = new ArrayList<LastRowMiniExchangeEventAudit>();
			 for(LastRowMiniExchangeEvent event : lastRowMiniExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){//&& event.getTicketNetworkBrokerId() == rtwBrokerId
					 LastRowMiniExchangeEventAudit audit = new LastRowMiniExchangeEventAudit(event);
					 lastRowMiniExchangeEventAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<LarryLastExchangeEvent> larryLastExchangeEvents = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsByEventIds(eventIds);
			 Collection<LarryLastExchangeEventAudit> larryLastExchangeEventAuditList = new ArrayList<LarryLastExchangeEventAudit>();
			 for(LarryLastExchangeEvent event : larryLastExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){
					 LarryLastExchangeEventAudit audit = new LarryLastExchangeEventAudit(event);
					 larryLastExchangeEventAuditList.add(audit);
					 //event.setRtwEnabled(false);
					 //event.setRotEnabled(false);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<ZoneLastRowMiniExchangeEvent> zoneLastRowMiniExchangeEvents = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsByEventIds(eventIds);
			 Collection<ZoneLastRowMiniExchangeEventAudit> zoneLastRowMiniExchangeEventAuditList = new ArrayList<ZoneLastRowMiniExchangeEventAudit>();
			 for(ZoneLastRowMiniExchangeEvent event : zoneLastRowMiniExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){
					 ZoneLastRowMiniExchangeEventAudit audit = new ZoneLastRowMiniExchangeEventAudit(event);
					 zoneLastRowMiniExchangeEventAuditList.add(audit);
					 //event.setRtwEnabled(false);
					 //event.setRotEnabled(false);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 // rtw zone pricing
			 Collection<TixCityZonePricingExchangeEvent> tixCityZonePricingExchangeEvents = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsByEventIds(eventIds);
			 Collection<TixCityZonePricingExchangeEventAudit> tixCityZonePricingExchangeEventAuditList = new ArrayList<TixCityZonePricingExchangeEventAudit>();
			 for(TixCityZonePricingExchangeEvent event : tixCityZonePricingExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){
					 TixCityZonePricingExchangeEventAudit audit = new TixCityZonePricingExchangeEventAudit(event);
					 tixCityZonePricingExchangeEventAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<ZonePricingExchangeEvent> zonePricingExchangeEvents = DAORegistry.getZonePricingExchangeEventDAO().getAllZonePricingExchangeEventsByEventIds(eventIds);
			 Collection<ZonePricingExchangeEventAudit> zonePricingExchangeEventAuditList = new ArrayList<ZonePricingExchangeEventAudit>();
			 for(ZonePricingExchangeEvent event : zonePricingExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){
					 ZonePricingExchangeEventAudit audit = new ZonePricingExchangeEventAudit(event);
					 zonePricingExchangeEventAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<ManhattanZonePricingExchangeEvent> manhattanZonePricingExchangeEvents = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAllManhattanZonePricingExchangeEventsByEventIds(eventIds);
			 Collection<ManhattanZonePricingExchangeEventAudit> manhattanZonePricingExchangeEventAuditList = new ArrayList<ManhattanZonePricingExchangeEventAudit>();
			 for(ManhattanZonePricingExchangeEvent event : manhattanZonePricingExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){
					 ManhattanZonePricingExchangeEventAudit audit = new ManhattanZonePricingExchangeEventAudit(event);
					 manhattanZonePricingExchangeEventAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<AutoCats96ExchangeEvent> autoCats96ExchangeEvents = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByEventIds(eventIds);
			 Collection<AutoCats96ExchangeEventAudit> autoCats96ExchangeEventAuditList = new ArrayList<AutoCats96ExchangeEventAudit>();
			 System.out.println("event size "+ autoCats96ExchangeEvents.size());
			 for(AutoCats96ExchangeEvent event : autoCats96ExchangeEvents){
				 
				 if(event.getStatus().equals("ACTIVE")){
					 AutoCats96ExchangeEventAudit audit = new AutoCats96ExchangeEventAudit(event);
					 autoCats96ExchangeEventAuditList.add(audit);
					 //event.setRtwEnabled(false);
					 //event.setRotEnabled(false);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<PresaleAutoCatExchangeEvent> presaleAutoCatExchangeEvents = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsByEventIds(eventIds);
			 Collection<PresaleAutoCatExchangeEventAudit> presaleAutoCatExchangeEventAuditList = new ArrayList<PresaleAutoCatExchangeEventAudit>();
			 System.out.println("event size "+ presaleAutoCatExchangeEvents.size());
			 for(PresaleAutoCatExchangeEvent event : presaleAutoCatExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){
					 PresaleAutoCatExchangeEventAudit audit = new PresaleAutoCatExchangeEventAudit(event);
					 presaleAutoCatExchangeEventAuditList.add(audit);
					 //event.setRtwEnabled(false);
					 //event.setRotEnabled(false);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<VipLastRowMiniExchangeEvent> vipLastRowMiniExchangeEvents = DAORegistry.getVipLastRowMiniExchangeEventDAO().getAllVipLastRowMiniExchangeEventsByEventIds(eventIds);
			 Collection<VipLastRowMiniExchangeEventAudit> vipLastRowMiniExchangeEventAuditList = new ArrayList<VipLastRowMiniExchangeEventAudit>();
			 for(VipLastRowMiniExchangeEvent event : vipLastRowMiniExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){//&& event.getTicketNetworkBrokerId() == rtwBrokerId
					 VipLastRowMiniExchangeEventAudit audit = new VipLastRowMiniExchangeEventAudit(event);
					 vipLastRowMiniExchangeEventAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 Collection<SGLastFiveRowExchangeEvent> sgLastFiveRowExchangeEvents = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAllSGLastFiveRowExchangeEventsByEventIds(eventIds);
			 Collection<SGLastFiveRowExchangeEventAudit> sgLastFiveRowExchangeEventAuditList = new ArrayList<SGLastFiveRowExchangeEventAudit>();
			 for(SGLastFiveRowExchangeEvent event : sgLastFiveRowExchangeEvents){
				 if(event.getStatus().equals("ACTIVE")){//&& event.getTicketNetworkBrokerId() == rtwBrokerId
					 SGLastFiveRowExchangeEventAudit audit = new SGLastFiveRowExchangeEventAudit(event);
					 sgLastFiveRowExchangeEventAuditList.add(audit);
					 //event.setTicketNetworkBrokerId(null);
					 event.setLastUpdatedDate(new Date());
					 event.setStatus("DELETED");
					 event.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 }
			 }
			 
			 DAORegistry.getMiniExchangeEventDAO().updateAll(miniExchangeEvents);
			 DAORegistry.getVipMiniExchangeEventDAO().updateAll(vipMiniExchangeEvents);
			 DAORegistry.getLastRowMiniExchangeEventDAO().updateAll(lastRowMiniExchangeEvents);
			 DAORegistry.getLarryLastExchangeEventDAO().updateAll(larryLastExchangeEvents);
			 DAORegistry.getZoneLastRowMiniExchangeEventDAO().updateAll(zoneLastRowMiniExchangeEvents);
			 DAORegistry.getTixCityZonePricingExchangeEventDAO().updateAll(tixCityZonePricingExchangeEvents);
			 DAORegistry.getZoneTicketsProcessorExchangeEventDAO().updateAll(zoneTicketsProcessorExchangeEvent);
			 DAORegistry.getAutoCats96ExchangeEventDAO().updateAll(autoCats96ExchangeEvents);
			 DAORegistry.getPresaleAutoCatExchangeEventDAO().updateAll(presaleAutoCatExchangeEvents);
			 DAORegistry.getVipLastRowMiniExchangeEventDAO().updateAll(vipLastRowMiniExchangeEvents);
			 DAORegistry.getSgLastFiveRowExchangeEventDAO().updateAll(sgLastFiveRowExchangeEvents);
			 DAORegistry.getZonePricingExchangeEventDAO().updateAll(zonePricingExchangeEvents);
			 DAORegistry.getManhattanZonePricingExchangeEventDAO().updateAll(manhattanZonePricingExchangeEvents);
			 
			 DAORegistry.getMiniExchangeEventAuditDAO().saveAll(miniExchangeEventsAuditList);
			 DAORegistry.getVipMiniExchangeEventAuditDAO().saveAll(vipMiniExchangeEventAuditList);
			 DAORegistry.getLastRowMiniExchangeEventAuditDAO().saveAll(lastRowMiniExchangeEventAuditList);
			 DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(larryLastExchangeEventAuditList);
			 DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().saveAll(zoneLastRowMiniExchangeEventAuditList);
			 DAORegistry.getTixCityZonePricingExchangeEventAuditDAO().saveAll(tixCityZonePricingExchangeEventAuditList);
			 DAORegistry.getZoneTicketsProcessorExchangeEventAuditDAO().saveAll(zoneTicketsProcessorExchangeEventAuditList);
			 DAORegistry.getAutoCats96ExchangeEventAuditDAO().saveAll(autoCats96ExchangeEventAuditList);
			 DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().saveAll(presaleAutoCatExchangeEventAuditList);
			 DAORegistry.getVipLastRowMiniExchangeEventAuditDAO().saveAll(vipLastRowMiniExchangeEventAuditList);
			 DAORegistry.getSgLastFiveRowExchangeEventAuditDAO().saveAll(sgLastFiveRowExchangeEventAuditList);
			 DAORegistry.getZonePricingExchangeEventAuditDAO().saveAll(zonePricingExchangeEventAuditList);
			 DAORegistry.getManhattanZonePricingExchangeEventAuditDAO().saveAll(manhattanZonePricingExchangeEventAuditList);
			 
			 // we will store admit one ids which are unbroadcasted for today.
			 //Date todayDate = new Date(dbDateFormat.format(now));
			 
			 //Collection<Event> toberemovedEvents = DAORegistry.getEventDAO().getAllEventsByAdmitoneIds(admitoneIds);
			 
			 if(unbroadcastedEvents == null){
				 unbroadcastedEvents = new UnbroadcastedEvents();
				 unbroadcastedEvents.setLastUpdated(now);
				 unbroadcastedEvents.setLastUpdatedBy("UNBROADCAST EVENT JOB");
				 unbroadcastedEvents.setUnbroadcastedDate(dbDateFormat.format(now));
			 }else{
				 unbroadcastedEvents.setLastUpdated(now);
				 unbroadcastedEvents.setLastUpdatedBy("UNBROADCAST EVENT JOB");
			 }
			 if(unbroadcastedEvents.getEventIds() == null || unbroadcastedEvents.getEventIds().equals("")) {
				 unbroadcastedEvents.setEventIds(eventIds);
			 } else {
				 unbroadcastedEvents.setEventIds(unbroadcastedEvents.getEventIds() + "," +eventIds);
			 }
			 if(unbroadcastedEvents.getAdmitoneIds() == null || unbroadcastedEvents.getAdmitoneIds().equals("")) {
				 unbroadcastedEvents.setAdmitoneIds(admitoneIds);
			 } else {
				 unbroadcastedEvents.setAdmitoneIds(unbroadcastedEvents.getAdmitoneIds() + "," +admitoneIds);
			 }
			 DAORegistry.getUnbroadcastedEventsDAO().saveOrUpdate(unbroadcastedEvents);
			 // will send an email with event list
			 	if(eventIdsForMil != null && !eventIdsForMil.isEmpty()){
			 		eventIdsForMil = eventIdsForMil.substring(0, eventIdsForMil.length() - 1);
			 		//Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByAdmitoneIds(admitoneIdsForMail);
			 		Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByeventIds(eventIdsForMil);
					 Map<String, Object> map = new HashMap<String, Object>();
					 map.put("events", events);
					 String subject = "Unbroadcasted Events due to multiple sales";
					 String fileName = "templates/unbroadcasted-events-details.txt";
					 String toAddress ="leor@rewardthefan.com;ezippo@rightthisway.com;jreardon@rightthisway.com;bmanusama@rightthisway.com";
					 String bccAddress = "AODev@rightthisway.com";
					 //String toAddress ="tselvan@rightthisway.com";
					 //String bccAddress = null;
					 EmailManager.sendEmail(toAddress, null, bccAddress, subject, fileName, map);
			 	}
		 }
	 }catch(Exception e){
		 System.out.println("Unbroadcast events as per invoice date time job failed.." + new Date() );
			log.info("Unbroadcast events as per invoice date time job failed.." + new Date());
		 e.printStackTrace();
		 try {
			 ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			 String toAddress = resourceBundle.getString("emailNotificationCCTo");
			 String ccAddress= resourceBundle.getString("emailAODev");
				
			 Map<String, Object> map = new HashMap<String, Object>();
			 String subject = "Unbroadcasted Events Job Failed";
			 String fileName = "templates/unbroadcasted-events-details.txt";
			 EmailManager.sendEmail(toAddress, ccAddress, null, subject, fileName, map);
		
		 } catch(Exception ex) {
			 ex.printStackTrace();
		 }
	 }
		 
	 }
	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
			System.out.println("Unbroadcast events as per invoice date time job started.." + new Date() + ": " + running);
			log.info("Unbroadcast events as per invoice date time job started.." + new Date() + ": " + running);
			try{
				unbroadCastEvents();
				System.out.println("Unbroadcast events as per invoice date time job finished @ " + new Date());
			}catch(Exception e){
				 System.out.println("Unbroadcast events as per invoice date time job failed.." + new Date() );
					log.info("Unbroadcast events as per invoice date time job failed.." + new Date());
				 e.printStackTrace();
				 try {
					 ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
					 String toAddress = resourceBundle.getString("emailNotificationCCTo");
					 String ccAddress= resourceBundle.getString("emailAODev");
						
					 Map<String, Object> map = new HashMap<String, Object>();
					 String subject = "Unbroadcasted Events Job Failed";
					 String fileName = "templates/unbroadcasted-events-details.txt";
					 EmailManager.sendEmail(toAddress, ccAddress, null, subject, fileName, map);
				
				 } catch(Exception ex) {
					 ex.printStackTrace();
				 }
			 }
			setRunning(false);
	}
	 
 public static String removeSeatGeekAPIListingsByEventId(Integer eventId) throws Exception {
		 
		 String error = null;
		 try {
			 if(!SeatGeekWSUtil.isSeatGeekAPIEnabled()) {
				 System.out.println("UNBROADCAST EVENT JOB SeatGeek API access is in disabled mode. eventId : "+eventId+" Date : "+new Date());
				 log.error(" UNBROADCAST EVENT JOB SeatGeek API access is in disabled mode. eventId : "+eventId+" Date : "+new Date());
			 } else {
				 List<SeatGeekUploadCats> uploadCats = DAORegistry.getSeatGeekUploadCatsDAO().getAllActiveSeatGeekListingsByEventId(eventId);
				 List<SeatGeekUploadCats> tobeUpdateList = new ArrayList<SeatGeekUploadCats>();
				 List<SeatGeekWSTracking> trackingList = new ArrayList<SeatGeekWSTracking>();
				 for (SeatGeekUploadCats sgUploadcat : uploadCats) {
					 String message = SeatGeekWSUtil.deleteTicket(sgUploadcat.getCatTicketId());
					 if(message == null || !message.equals("200")) {
						 SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
						 sgWsTrackingOne.setAction("Deleting Ticket");
						 sgWsTrackingOne.setCatTicketId(sgUploadcat.getCatTicketId());
						 sgWsTrackingOne.setCreatedDate(new Date());
						 sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
						 sgWsTrackingOne.setMessage("Error while deleting ticket in UNBROADCAST EVENT JOB");
						 sgWsTrackingOne.setError(message);
						 //DAORegistry.getSeatGeekWSTrackingDAO().saveOrUpdate(sgWsTrackingOne);
						 trackingList.add(sgWsTrackingOne);
							
						 continue;
					 }
					 sgUploadcat.setStatus("DELETED");
					 sgUploadcat.setLastUpdated(new Date());
					 tobeUpdateList.add(sgUploadcat);
				 }
				 DAORegistry.getSeatGeekUploadCatsDAO().saveOrUpdateAll(tobeUpdateList);
				 DAORegistry.getSeatGeekWSTrackingDAO().saveAll(trackingList);
			 }
		 } catch(Exception e) {
			 e.printStackTrace();
		 }
		 return error;
	 }
	 
	 public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		UnbroadCastEventsScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		UnbroadCastEventsScheduler.running = running;
	}

}
