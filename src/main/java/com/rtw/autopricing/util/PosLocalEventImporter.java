package com.rtw.autopricing.util;

//import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;



public class PosLocalEventImporter extends QuartzJobBean implements StatefulJob {
	
	private static Logger log = LoggerFactory.getLogger(PosLocalEventImporter.class);
	
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
	 
	 public static void importLocalEvents() throws Exception{ 
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		 
		Collection<Broker> brokers =  DAORegistry.getBrokerDAO().getAllActiveBrokers();
		Map<Integer, Broker> brokerMap  = new HashMap<Integer, Broker>();
		
		List<Integer> admitoneIds = new ArrayList<Integer>();
		try {
			admitoneIds =DAORegistry.getEventDAO().getAllEventsAdmitoneIdFromUSandCA();	
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		 
		 Map<Integer,Boolean> eventMap = new HashMap<Integer,Boolean>();
		 
		 for (Integer admitoneId : admitoneIds) {
			 eventMap.put(admitoneId, Boolean.TRUE);
		 }
		 
		 /*Collection<Integer> manhaattanExEvents = DAORegistry.getQueryManagerDAO().getAllActiveManhattanExEvents();
		 Map<Integer,Boolean> manhattanExEventMap = new HashMap<Integer,Boolean>();
		 
		 for (Integer exEventId : manhaattanExEvents) {
			 manhattanExEventMap.put(exEventId, Boolean.TRUE);
		 }*/
		 
		 /*Collection<Integer> tixcityNewExEvents = DAORegistry.getQueryManagerDAO().getAllActiveTixCityNewExEvents();
		 Map<Integer,Boolean> tixcityNewExEventMap = new HashMap<Integer,Boolean>();
		 
		 for (Integer exEventId : tixcityNewExEvents) {
			 tixcityNewExEventMap.put(exEventId, Boolean.TRUE);
		 }*/
		 
		for (Broker broker : brokers) {
		  if( broker.getId() == 1 ){
			 continue;
		  }
		  brokerMap.put(broker.getId(), broker);
		}
		
		String subject,fileName;
		Map<String, Object> map;
		StringBuilder statusMessage = new StringBuilder();
		 
		for (Integer brokerId : brokerMap.keySet()) {
			Broker broker = brokerMap.get(brokerId);
			int success =0,failure=0,error=0;
			try{
				
				List<PosEvent> posEvents = InduxDAORegistry.getPosEventDAO().getAllActiveExchangeEventWithoutLocalEvent(broker.getPosBrokerId());
				 if(null != posEvents && posEvents.size() >0 && eventMap.size() > 0){
					 boolean successFlag=false;
					 for (PosEvent posEvent : posEvents) {
						 try {
						 if(null != eventMap.get(posEvent.getExchangeEventId()) && eventMap.get(posEvent.getExchangeEventId())){
							 
							 /*if(brokerId.equals(4)){//Manhattan
								 if( null == manhattanExEventMap.get(posEvent.getExchangeEventId()) || 
										 !manhattanExEventMap.get(posEvent.getExchangeEventId())){
									 continue;
								 }
							 }*//*else if(brokerId.equals(7)){//Tix City New
								 if( null == tixcityNewExEventMap.get(posEvent.getExchangeEventId()) || 
										 !tixcityNewExEventMap.get(posEvent.getExchangeEventId())){
									 continue;
								 }
							 }*/
							 successFlag = InduxDAORegistry.getPosEventDAO().updateLocalEvent(broker.getPosBrokerId(), posEvent.getExchangeEventId(), broker.getSystemUserId());
							 if(successFlag){
								 success++;
							 }else{
								 failure++;
							 }
						 }
						 } catch(Exception e) {
							 error++;
							 e.printStackTrace();
						 }
					}
				 }
				 statusMessage.append("Broker Name :"+broker.getName()+" , Success :"+success+" , Failure :"+failure+" , Error : "+error);
				 statusMessage.append(" \r\n ");
			}catch(Exception e){
				map = new HashMap<String, Object>();
				map.put("brokerName", broker.getName());
				map.put("example", ""+e.fillInStackTrace());
				try {
					subject = broker.getName()+": POS Event Import Scheduler job failed :";
					fileName = "templates/localevent-import-job-failure-message.txt";
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
				} catch (Exception e1) {
					log.error("POSLOCALEVENTIMPORTER 17 : Error while Importing Local Event By Exchange Event Id.");
					System.out.println("POSLOCALEVENTIMPORTER 17 : Error while Importing Local Event By Exchange Event Id.");
					e1.printStackTrace();
				}
				e.printStackTrace();
				continue;
			}
			
		}
		
		map = new HashMap<String, Object>();
		System.out.println(statusMessage.toString());
		map.put("statusMessage",statusMessage.toString());
		try {
			subject ="POS Local Event Importer Status";
			fileName = "templates/localevent-import-success-message.txt";
			//EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
		} catch (Exception e1) {
			log.error("POSLOCALEVENTIMPORTER 18 : Error while sending Local Event Importer Status Mail.");
			System.out.println("POSLOCALEVENTIMPORTER 18 : Error while sending Local Event Importer Status Mail.");
			e1.printStackTrace();
		}
		
	 }
	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
				System.out.println("ManualCats Job Called.." + new Date());
				log.info("ManualCats Job Called.." + new Date());
				try{
					
					importLocalEvents();
					System.out.println("ManualCats Scheduler Job finished @ " + new Date());
				}catch(Exception e){
					e.printStackTrace();
				}
	}


}