package com.rtw.autopricing.util;

//import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.data.TNDZonesCategoryTicket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;


public class TNDZonesScheduler  extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(TNDZonesScheduler.class);
	
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 
	 
	 public static void processTNDZonesTickets() throws Exception {
		Calendar cal =  Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.MINUTE,15);
		setNextRunTime(cal.getTime());
		if(isStopped() || isRunning()){
			return ;
		}
		setRunning(true);
		
		
		Date lastRunTime = getLastUpdateTime();
		Long minute = 0l;
		if(lastRunTime!=null){
			minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
		}else{
			minute = 2880l; // 2 Days in minute
		}
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNDZones");
		if(autopricingProduct==null){
			autopricingProduct = new AutopricingProduct();
			autopricingProduct.setName("TNDZones");
			autopricingProduct.setStatus("ACTIVE");
			autopricingProduct.setStopped(false);
		}
		if(autopricingProduct.getStopped()){
			setStopped(true);
			setRunning(false);
			return;
		}
		
		setLastUpdateTime(now);
		autopricingProduct.setLastRunTime(now);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
//		Date jobStartTime = new Date();
		int tInsert=0,tUpdate=0,tremovet=0,tposCount=0;
		int eInsert=0,eUpdate=0,eremovet=0;
		
		Integer hourstoConsiderOrders = 48;
		Double markupPercentage = 30.0;
		Integer excludeEventDays = 7;
		Integer shippingMethodId = 0;//Default Website Setting
		Integer nearTermDisplayOptionId = 0;//Standard delivery until near-term
		Integer expectedArrivalDaysBeforeEvent = 2;
		 try {
			 //Map<Integer, Integer> brokerAddCountMap = new HashMap<Integer, Integer>();
			// Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
			 //Map<Integer, Integer> brokerUpdateCountMap = new HashMap<Integer, Integer>();
			 
				
			// Boolean isTnEvent = false;
			 
			 //Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
			 List<PosEvent> posEvents = new ArrayList<PosEvent>();
			 Map<Integer, List<PosEvent>> posEventsAndBrokerMap = new HashMap<Integer, List<PosEvent>> ();
			 Collection<Event> eventList = null;
			// Collection<MiniExchangeEvent> miniExchangeEvents = null;
			
			 Broker broker = BrokerUtils.getBrokerById(5);//RTW
			 
			 try {
				 
				 eventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentId();
				 for(Event event:eventList) {
					//eventMap.put(event.getId(), event);
					eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
				 }
				 System.out.println("TND Zones : TMAT event size : "+eventList.size());
//				 log.info("NFES : TMAT event size : "+eventList.size());
				 
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("TND Zones 1 : Error while Loading Events.");
				 System.err.println("TND Zones 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			 
			 //Map<String, Integer> venueConfigZoneMap = BrokerUtils.getPOSVenueConfigurationMap(broker);
				
			 try {
				 List<Integer> tndOrderEventIdsList = new ArrayList<Integer>(); 
				 Map<Integer,Map<String,TNDZonesCategoryTicket>> eventsTixMap = new HashMap<Integer, Map<String,TNDZonesCategoryTicket>>();
				 List<TNDZonesCategoryTicket> catsListFromOrders = DAORegistry.getQueryManagerDAO().getAllTNDZonesElibibleOrders(hourstoConsiderOrders,excludeEventDays);
				 for (TNDZonesCategoryTicket catTix : catsListFromOrders) {
					 
					 if(!tndOrderEventIdsList.contains(catTix.getTnExchangeEventId())) {
						 tndOrderEventIdsList.add(catTix.getTnExchangeEventId());
					 }
					 if(catTix.getSection().contains("-") || catTix.getSection().toLowerCase().contains(" or ") ||
							// catTix.getLastRow().contains("-") || catTix.getLastRow().toLowerCase().contains(" or ") ||
							 catTix.getSection().toLowerCase().contains("package") || //catTix.getLastRow().toLowerCase().contains("package") ||
							 catTix.getSection().toLowerCase().contains("parking") || //|| catTix.getLastRow().toLowerCase().contains("parking") 
							  catTix.getSection().toLowerCase().contains("pass") 
							 ) {
						 continue;
					 }
					 /*Double price = catTix.getTnPrice()-1;
					 catTix.setTnPrice(price);
					 catTix.setActualPrice(price);
					 if(catTix.getTnPrice()<50) {
						 continue;
					 }
					 catTix.setLastRow("");
					 catTix.setQuantity(2);
					 catTix.setTnBrokerId(broker.getId());
					 catTix.setSection(catTix.getSection().toUpperCase());*/
					 
					 Map<String,TNDZonesCategoryTicket> catsMap = eventsTixMap.get(catTix.getTnExchangeEventId());
					 if(catsMap == null) {
						 catsMap = new HashMap<String, TNDZonesCategoryTicket>();
					 }
					 String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
					 catsMap.put(key, catTix);
					 eventsTixMap.put(catTix.getTnExchangeEventId(), catsMap);
				}
				 Map<Integer,Map<String,TNDZonesCategoryTicket>> eventsTixMapFromDb = new HashMap<Integer, Map<String,TNDZonesCategoryTicket>>();

				 List<TNDZonesCategoryTicket> catsListFromDbOrders = DAORegistry.getTndZonesCategoryTicketDAO().getAllActiveTndZonesCategoryTickets();
				 for (TNDZonesCategoryTicket catTix : catsListFromDbOrders) {
					 Map<String,TNDZonesCategoryTicket> catsMapFromDb = eventsTixMapFromDb.get(catTix.getTnExchangeEventId());
					 if(catsMapFromDb == null) {
						 catsMapFromDb = new HashMap<String, TNDZonesCategoryTicket>();
					 }
					 String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
					 catsMapFromDb.put(key, catTix);
					 eventsTixMapFromDb.put(catTix.getTnExchangeEventId(), catsMapFromDb);
				}
				 
				 Map<Integer,PosEvent> posEventMap = new HashMap<Integer, PosEvent>();
				 posEvents = posEventsAndBrokerMap.get(broker.getId());
				 if(posEvents == null){
					 posEvents = BrokerUtils.getPOSEventByBrokerId(broker.getId());
					 posEventsAndBrokerMap.put(broker.getId(), posEvents);
				 }
				 if(posEvents!=null){
					 for(PosEvent posEvent:posEvents){
						 posEventMap.put(posEvent.getExchangeEventId(), posEvent);
					 }
				 }
				//Unbroadcsted event ids from TN autopricing products
				 List<Integer> unbroadcastedEventIds = DAORegistry.getQueryManagerDAO().getUserUnBroadcastedEventIdsForTN();
				 if(unbroadcastedEventIds == null) {
					 unbroadcastedEventIds = new ArrayList<Integer>();
				 }
				 
				// List<Integer> tnEventIdList = new ArrayList<Integer>(eventsTixMap.keySet());
				 int totalSize = tndOrderEventIdsList.size();
				 int processed = 0;
				 for (Integer tnEventId : tndOrderEventIdsList) {
					
					 if(isStopped()){
						 break;
					 }
					 processed++;
					 System.out.println(totalSize+"/"+processed+" : TND Zones : "+tnEventId+" : added : "+eInsert+" : removed : "+eremovet);
					
					 Map<String,TNDZonesCategoryTicket> catsMap = eventsTixMap.remove(tnEventId);
					 //Map<String,TNDZonesCategoryTicket> catsMapFromDb = eventsTixMapFromDb.remove(tnEventId);
						 
						// List<TNDZonesCategoryTicket> deletedCatsList = new ArrayList<TNDZonesCategoryTicket>();
						// List<TNDZonesCategoryTicket> newCatsList = new ArrayList<TNDZonesCategoryTicket>();
						 
						 Event event = eventMapByExchangeEventId.get(tnEventId);
						 PosEvent posEvent = posEventMap.get(tnEventId);
						 
						 String reason = null;
						 Boolean deleteDataFlag = false;
						 if(posEvent == null || event == null) {
							 deleteDataFlag = true;
							 reason = "Event is Missing in TMAT or POS";
						 } else if(catsMap == null || catsMap.isEmpty()) {
							 deleteDataFlag = true;
							 reason = "Tnd Orders Skipped";
						 } else if(unbroadcastedEventIds.contains(event.getId())) {
							 deleteDataFlag = true;
							 reason = "Event Unbroadcasted";
						 }
						 if(deleteDataFlag) {
							 
							 List<TNDZonesCategoryTicket> miniTickets = DAORegistry.getTndZonesCategoryTicketDAO().getAllTNDZonesCategoryTicketsByTnExchangeEventId(tnEventId);
							 for(TNDZonesCategoryTicket cat:miniTickets){
								 	
								 cat.setReason(reason);
								if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
									continue;
								}
								try {
									if(Counter.isMinuteDeleteAllow(broker)) {
//										System.out.println("MINI delete begins..1..."+new Date()+"...count.."+eremovet);
										InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
										broker.addMinuteUpdateCounter();
										eremovet++;
										cat.setStatus("DELETED");
										cat.setLastUpdated(now);
										cat.setReason(reason+" and Deleted from TN");
									}
								} catch (Exception e) {
										cat.setReason(reason+" and Exception in TN");
										isErrorOccured = true;
										error = new AutoPricingError();
										 error.setProductId(autopricingProduct.getId());
										error.setMessage("While Remove Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(tnEventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("MINI 2 : Error While Remove Category:"+"Event:" + tnEventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
										System.err.println("MINI 2 : CT Error From POS API : Remove Category:"+"Event:" + tnEventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
								}
							}
							 DAORegistry.getTndZonesCategoryTicketDAO().updateAll(miniTickets);
							 
							System.out.println("TND Zones : Event or POS Event is not found : "+tnEventId);
							continue;
						 }
						 
						 
						 
						Integer eventId =event.getId();
						List<TNDZonesCategoryTicket> updatedBrokerTicketList = new ArrayList<TNDZonesCategoryTicket>();
							 
							 /*Map<Integer,TNDZonesCategoryTicket> soldTndZonesCatsMap = new HashMap<Integer, TNDZonesCategoryTicket>();
							 List<TNDZonesCategoryTicket> soldTndZonesCatsList = DAORegistry.getTndZonesCategoryTicketDAO().getAllSoldTNDZonesCategoryTicketsByEventId(event.getId());
							 for (TNDZonesCategoryTicket catTix : soldTndZonesCatsList) {
								if(catTix.getTndOrderId() != null) {
									soldTndZonesCatsMap.put(catTix.getTndOrderId(), catTix);
								}
							}*/
							 
							List<TNDZonesCategoryTicket> tndOrders = new ArrayList<TNDZonesCategoryTicket>(catsMap.values());
							 Map<String,TNDZonesCategoryTicket> catTixMap = new HashMap<String, TNDZonesCategoryTicket>();
							 List<TNDZonesCategoryTicket> catsList = CategoryGroupManager.computeCategoryTicketsForTNDZones(event, expectedArrivalDaysBeforeEvent,markupPercentage, autopricingProduct,tndOrders);
							 for (TNDZonesCategoryTicket catTix : catsList) {
								 if(catTix.getTnPrice() < 50) {
									 continue;
								 }
								 catTix.setShippingMethodSpecialId(shippingMethodId);
								 catTix.setNearTermOptionId(nearTermDisplayOptionId);
								 catTix.setTnBrokerId(broker.getId());
								 String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+catTix.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase();
								 catTixMap.put(key, catTix);	
							 }
							 Map<String,TNDZonesCategoryTicket> catTixFromDB = new HashMap<String, TNDZonesCategoryTicket>();
							 List<TNDZonesCategoryTicket> catsListFromDbList = DAORegistry.getTndZonesCategoryTicketDAO().getAllTNDZonesCategoryTicketsByEventId(event.getId());
							 for (TNDZonesCategoryTicket dbCatTix : catsListFromDbList) {
								
								 dbCatTix.setPosVenueId(posEvent.getVenueId());
								 dbCatTix.setPosEventId(posEvent.getId());
								 dbCatTix.setPosExchangeEventId(posEvent.getExchangeEventId());
								 
								 if(dbCatTix.getTnExchangeEventId() != null && !dbCatTix.getTnExchangeEventId().equals(event.getAdmitoneId())) {
									 dbCatTix.setStatus("DELETED");
									 dbCatTix.setLastUpdated(now);
									 dbCatTix.setReason("POS Event Id Mismatching");
									 updatedBrokerTicketList.add(dbCatTix);
									 continue;
								 }
								 String key = dbCatTix.getQuantity()+":"+ dbCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+dbCatTix.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase();
								 catTixFromDB.put(key, dbCatTix);	
							 }
							 
							 List<TNDZonesCategoryTicket> tnNewCatTix = new ArrayList<TNDZonesCategoryTicket>();
							 List<TNDZonesCategoryTicket> tnUpdateCatTix = new ArrayList<TNDZonesCategoryTicket>();
							 List<TNDZonesCategoryTicket> tnRemoveCatTix = new ArrayList<TNDZonesCategoryTicket>();
							 Map<String,TNDZonesCategoryTicket> exFinalCatTixMap = new HashMap<String, TNDZonesCategoryTicket>();
							 try {
									 String priceHistory="";
									 String ticketIdHistory= "";
									 String baseTicketOneHistory= "";
									 String baseTicketTwoHistory="";
									 String baseTicketThreeHistory="";
									 
									 String ticketHDateStr = dateTimeFormat.format(new Date());
									String priceHDateStr = dateTimeFormat.format(new Date());
									List<String> keys = new ArrayList<String>(catTixFromDB.keySet());
									Integer baseTicket1,baseTicket2,baseTicket3;
									Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
									//Integer purchaseOrderId = null;
									//boolean existingPurchaseOrder = false;
									
									for(String key:keys) {
										TNDZonesCategoryTicket dbTix =catTixFromDB.remove(key);
										TNDZonesCategoryTicket tix = catTixMap.remove(key);
										isPriceChangeFlag = false;

										if(tix != null) { // if generated tickets is already existing..
											if(!dbTix.getActualPrice().equals(tix.getActualPrice())) {
												isPriceChangeFlag = true;
												priceHistory = priceHDateStr+"-"+tix.getActualPrice();
												if(dbTix.getPriceHistory() != null) {
													priceHistory = dbTix.getPriceHistory() +","+priceHistory;
													if(priceHistory.length()>=500) {
														priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
													}
												}
												dbTix.setPriceHistory(priceHistory);
												dbTix.setIsEdited(true);
												
											} else if(!dbTix.getTicketId().equals(tix.getTicketId())) { 
												
												dbTix.setIsEdited(true);
												
											} else {
												baseTicket1 = 0;
												baseTicket2 = 0;
												baseTicket3 = 0;
												
												if(dbTix.getBaseTicketOne() != null) {
													baseTicket1 = dbTix.getBaseTicketOne();
												}
												if(dbTix.getBaseTicketTwo() != null) {
													baseTicket2 = dbTix.getBaseTicketTwo();
												}
												if(dbTix.getBaseTicketThree() != null) {
													baseTicket3 = dbTix.getBaseTicketThree();
												}
												
												if(!tix.getBaseTicketOne().equals(baseTicket1) || !tix.getBaseTicketTwo().equals(baseTicket2) || !tix.getBaseTicketThree().equals(baseTicket3)) {
													dbTix.setIsEdited(true);
													
												}
											}
											
											//add the pricehistory of basetickets prices here for all tickets
											
											tempTicketIdFlag = false;
											if(isPriceChangeFlag || !dbTix.getTicketId().equals(tix.getTicketId())) {
												ticketIdHistory = ticketHDateStr +"/"+ tix.getTicketId() +"/"+ tix.getPurPrice();
												
												if(dbTix.getTicketIdHistory() != null) {
													ticketIdHistory = dbTix.getTicketIdHistory()+","+ ticketIdHistory;
													if(ticketIdHistory.length()>= 500) {
														ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
													}
												}
												dbTix.setTicketIdHistory(ticketIdHistory);
												dbTix.setIsEdited(true);
											} 
											
											// For Base Ticket 1
											tempTicketIdFlag = false;
											if (dbTix.getBaseTicketOne() != null && dbTix.getBaseTicketOne() != 0) {
												if(isPriceChangeFlag || !dbTix.getBaseTicketOne().equals(tix.getBaseTicketOne())) {
													tempTicketIdFlag = true;
												}
											} else if(tix.getBaseTicketOne() != null && tix.getBaseTicketOne() != 0) {
												tempTicketIdFlag = true;
											}
											
											if(tempTicketIdFlag) {
												baseTicketOneHistory = ticketHDateStr +"/"+ tix.getBaseTicketOne() +"/"+ tix.getBaseTicketOnePurPrice();
												
												if(dbTix.getBaseTicketOneHistory() != null) {
													baseTicketOneHistory = dbTix.getBaseTicketOneHistory()+","+ baseTicketOneHistory;
																			
													if(baseTicketOneHistory.length()>= 500) {
														baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
													}
												}
												dbTix.setBaseTicketOneHistory(baseTicketOneHistory);
												dbTix.setIsEdited(true);
											}
											
											// For Base Ticket 2
											tempTicketIdFlag = false;
											if (dbTix.getBaseTicketTwo() != null && dbTix.getBaseTicketTwo() != 0) {
												if(isPriceChangeFlag || !dbTix.getBaseTicketTwo().equals(tix.getBaseTicketTwo())){
													tempTicketIdFlag = true;
												}
											} else if(tix.getBaseTicketTwo() != null && tix.getBaseTicketTwo() != 0) {
												tempTicketIdFlag = true;
											}
											
											if(tempTicketIdFlag) {
												baseTicketTwoHistory = ticketHDateStr +"/"+ tix.getBaseTicketTwo() +"/"+ tix.getBaseTicketTwoPurPrice();
												if(dbTix.getBaseTicketTwoHistory() != null) {
													baseTicketTwoHistory = dbTix.getBaseTicketTwoHistory()+","+ baseTicketTwoHistory;
																			
													if(baseTicketTwoHistory.length()>= 500) {
														baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
													}
												}
												dbTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
												dbTix.setIsEdited(true);
											}
											// For Base Ticket 3
											tempTicketIdFlag = false;
											if (dbTix.getBaseTicketThree() != null && dbTix.getBaseTicketThree() != 0) {
												if(isPriceChangeFlag || !dbTix.getBaseTicketThree().equals(tix.getBaseTicketThree()))  {
													tempTicketIdFlag = true;
												}
											} else if(tix.getBaseTicketThree() != null && tix.getBaseTicketThree() != 0) {
												tempTicketIdFlag = true;
											}
											
											if(tempTicketIdFlag) {
												baseTicketThreeHistory = ticketHDateStr +"/"+ tix.getBaseTicketThree() +"/"+ tix.getBaseTicketThreePurPrice();
												
												if(dbTix.getBaseTicketThreeHistory() != null) {
													baseTicketThreeHistory = dbTix.getBaseTicketThreeHistory()+","+ baseTicketThreeHistory;
																			
													if(baseTicketThreeHistory.length()>= 500) {
														baseTicketThreeHistory = baseTicketThreeHistory.substring(baseTicketThreeHistory.indexOf(",")+1);	
													}
												}
												dbTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
												dbTix.setIsEdited(true);
											}
											
											if(broker != null ) {
												if(dbTix.getTnCategoryTicketGroupId() == null || dbTix.getTnCategoryTicketGroupId() == 0) { // Add tickets as new tickets if it is not exist in POS (i.e. tn group id is not present)
													
													dbTix.setNearTermOptionId(tix.getNearTermOptionId());
													dbTix.setExpectedArrivalDate(tix.getExpectedArrivalDate());
													dbTix.setIsEdited(true);
													
													tnNewCatTix.add(dbTix);
													
												} else if(!dbTix.getTnPrice().equals(tix.getTnPrice()) || // Else change it in pos if price or other details has been changed.
														!dbTix.getShippingMethodSpecialId().equals(tix.getShippingMethodSpecialId()) ||
														!dbTix.getNearTermOptionId().equals(tix.getNearTermOptionId()) ||
														dbTix.getExpectedArrivalDate().compareTo(tix.getExpectedArrivalDate()) != 0) {
													
													dbTix.setTnPrice(tix.getTnPrice());
													//dbTix.setShippingMethodSpecialId(tix.getShippingMethodSpecialId());
													dbTix.setNearTermOptionId(tix.getNearTermOptionId());
													dbTix.setExpectedArrivalDate(tix.getExpectedArrivalDate());
													
//														dbTix.setIsPosUpdate(true);
													dbTix.setIsEdited(true);
													
													tnUpdateCatTix.add(dbTix);
												}
											}
											if(!dbTix.getShippingMethodSpecialId().equals(tix.getShippingMethodSpecialId())){
												dbTix.setShippingMethodSpecialId(tix.getShippingMethodSpecialId());
												dbTix.setIsEdited(true);
											}
											
											dbTix.setBaseTicketOne(tix.getBaseTicketOne());
											dbTix.setBaseTicketTwo(tix.getBaseTicketTwo());
											dbTix.setBaseTicketThree(tix.getBaseTicketThree());
											
											dbTix.setCategoryId(tix.getCategoryId());
											dbTix.setTicketId(tix.getTicketId());
											dbTix.setItemId(tix.getItemId());
											
											dbTix.setActualPrice(tix.getActualPrice());
											dbTix.setTmatZone(tix.getTmatZone());
											dbTix.setTnExchangeEventId(tix.getTnExchangeEventId());
											
											if(dbTix.getIsEdited()){
												dbTix.setLastUpdated(now);
												exFinalCatTixMap.put(key, dbTix);
											}
												
										} else { // else delete existing ticket as it does not exist in newly generated tickets..
//												dbTix.setStatus(DE)
											tnRemoveCatTix.add(dbTix);
											
											dbTix.setStatus("DELETED");
											dbTix.setLastUpdated(now);
											dbTix.setReason("Ticket not exist in TMAT");
											exFinalCatTixMap.put(key, dbTix);
										}
									}
									
									
								
									for(String key:catTixMap.keySet()) {  // Add remaining newly generated tickets to db.
										TNDZonesCategoryTicket newTix = catTixMap.get(key);
										 
										 priceHistory = priceHDateStr+"-"+newTix.getActualPrice();
										newTix.setPriceHistory(priceHistory);
										
										ticketIdHistory = ticketHDateStr +"/"+ newTix.getTicketId() +"/"+ newTix.getPurPrice();
										newTix.setTicketIdHistory(ticketIdHistory);
										
										if (newTix.getBaseTicketOne() != 0) {
											baseTicketOneHistory = ticketHDateStr +"/"+ newTix.getBaseTicketOne() +"/"+ newTix.getBaseTicketOnePurPrice();
											newTix.setBaseTicketOneHistory(baseTicketOneHistory);
										}
										
										// For Base Ticket 2
										if (newTix.getBaseTicketTwo() != 0) {
											baseTicketTwoHistory = ticketHDateStr +"/"+ newTix.getBaseTicketTwo() +"/"+ newTix.getBaseTicketTwoPurPrice();
											newTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
										}
										// For Base Ticket 3
										if (newTix.getBaseTicketThree() != 0) {
											baseTicketThreeHistory = ticketHDateStr +"/"+ newTix.getBaseTicketThree() +"/"+ newTix.getBaseTicketThreePurPrice();
											newTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
										}
										
										newTix.setLastUpdated(now);
										newTix.setCreatedDate(now);
										exFinalCatTixMap.put(key, newTix);
										
										if(broker != null) {
											tnNewCatTix.add(newTix);
										}
									}
									
									 if(broker != null) {
										 
										 int addCount=0;
										 //boolean isCreateListingEnabled = Counter.isCreateListingEnabled(broker);
										 if(!tnNewCatTix.isEmpty()) {// && isCreateListingEnabled
											 Map<String, Integer> posVenueCatIdMap = new HashMap<String, Integer>();
											 try {
												 List<PosVenueCategory> posVenueCatList = InduxDAORegistry.getPosVenueCategoryDAO().getAllVenueCategorysByVenueId(posEvent.getVenueId(),broker.getPosBrokerId());
												 for (PosVenueCategory posVenueCategory : posVenueCatList) {
													
													//Tamil : skip all venue configuration zone id mapped venue configurations for non zones pricing listings
													if(posVenueCategory.getVenueConfigZoneId() != null && posVenueCategory.getVenueConfigZoneId() > 0) {
														continue;
													}
													String key = posVenueCategory.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+posVenueCategory.getRow().replaceAll("\\s+", " ").trim().toLowerCase();
													posVenueCatIdMap.put(key, posVenueCategory.getId());
												}
												//System.out.println("venue cat size..."+posVenueCatIdMap.size());
											} catch (Exception e) {
												e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												 error.setProductId(autopricingProduct.getId());
												error.setExample(""+e.fillInStackTrace());
												error.setMessage("Event:" + event.getId());
												error.setProcess("Loading POS VenueCategors.");
												error.setEventId(event.getId());
												error.setTimeStamp(new Date());
												errorList.add(error);
												System.err.println("MINI 9 : NF Error From While Loading POS VenueCategors.."+eventId);
											}

											for (TNDZonesCategoryTicket tix : tnNewCatTix) {
												/* if(excludeZones != null && broker != null && tix.getTmatZone() != null) {
													 String exZoneKey = broker.getId()+"_"+tix.getTmatZone().toUpperCase(); 
													 if(excludeZones.contains(exZoneKey)) {
														 continue;
													 }
												 }*/
												 tix.setPosVenueId(posEvent.getVenueId());
												 tix.setPosEventId(posEvent.getId());	
												 Integer posVenueCategoryId = posVenueCatIdMap.get(tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase());
												 if(null == posVenueCategoryId || posVenueCategoryId == -1) {
													posVenueCategoryId = InduxDAORegistry.getPosVenueCategoryDAO().save(tix,broker.getPosBrokerId(),autopricingProduct);
													String posVenueCategorykey = tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase();
													posVenueCatIdMap.put(posVenueCategorykey, posVenueCategoryId);
													//System.out.println("new venue cat id.."+posVenueCategoryId);
												}
												if(null == posVenueCategoryId || posVenueCategoryId == -1) {
													//newTix.setReason("Venue Category not exist in TN");
													isErrorOccured = true;
													error = new AutoPricingError();
													 error.setProductId(autopricingProduct.getId());
													error.setMessage("Error While Creating VenueCategory");
													error.setExample("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
													error.setProcess("ADD Venuecategory.");
													error.setEventId(event.getId());
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("MINI 11 :Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
													System.err.println("MINI 11 : Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
													
													continue;
												}
												tix.setPosVenueCategoryId(posVenueCategoryId);
												tix.setPosExchangeEventId(posEvent.getExchangeEventId());
												tix.setTnBrokerId(broker.getId());
												
												if(Counter.isMinuteAddAllow(broker)){
					//								 System.out.println("Minicats ADD begins....."+new Date()+"...count.."+eInsert);
													try {
														Integer tnCategoryTicketGroupId = InduxDAORegistry.getPosCategoryTicketGroupDAO().save(autopricingProduct, tix,broker.getPosBrokerId());
														if(tnCategoryTicketGroupId!=-1l){
															tix.setTnCategoryTicketGroupId(tnCategoryTicketGroupId);
															broker.addMinuteUpdateCounter();
															addCount++;
															eInsert++;
														}
													 } catch(Exception e) {
														 //tix.setReason("Exception while creating ticket in TN");
														 isErrorOccured = true;
														 error = new AutoPricingError();
														 error.setProductId(autopricingProduct.getId());
														 error.setMessage("Error while creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
														 error.setExample(""+e.fillInStackTrace());
														 error.setProcess("Ticket Group Creation..");
														 error.setEventId(eventId);
														 error.setTimeStamp(new Date());
														 errorList.add(error);
														 log.error("MINI 12 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
														 System.out.println("MINI 12 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
														 e.printStackTrace();
													 }
												 }
											 }
										 }
										 
										 for(TNDZonesCategoryTicket cat:tnUpdateCatTix){
											 try {
												 //if(isTnEvent && isUpdateTN){ 
												 	if(Counter.isMinuteUpdateAllow(broker)) {
//														System.out.println("MINI Update begins....."+new Date()+"...count.."+eUpdate);
														InduxDAORegistry.getPosCategoryTicketGroupDAO().updateCategoryTicketGroup(cat,broker.getPosBrokerId());
														broker.addMinuteUpdateCounter();
														eUpdate++;
													}
												// }
											} catch (Exception e) {
													//cat.setReason("Exception while updating ticket in TN");
													e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Updating Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
													error.setProcess("POS Update Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("MINI 13 : Error While POS Update:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
													System.out.println("MINI 13 : Error From POS Update :"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											}
										 }
										 
										 for(TNDZonesCategoryTicket cat:tnRemoveCatTix){
													
											 try {
												 if(cat.getTnCategoryTicketGroupId()!=null){	
													 if(Counter.isMinuteDeleteAllow(broker)) {
//														 System.out.println("MINI delete begins..4..."+new Date()+"...count.."+eremovet);
													
														 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
														 broker.addMinuteUpdateCounter();
														 cat.setReason("Ticket not exist in TMAT and Deleted from TN");
														 eremovet++;
													}
												 }
											 } catch (Exception e) {
												 cat.setReason("Ticket not exist in TMAT and Exception in TN");
												 e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Remove Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
													error.setProcess("POS Remove Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("MINI 14 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
													System.out.println("MINI 14 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											 }
										 }
										 //Delete excluded zone tickets and broker id changed tickets from ticketnetwork
										 for(TNDZonesCategoryTicket cat : updatedBrokerTicketList){
												
											 try {
												 if(cat.getTnCategoryTicketGroupId()!=null){	
													 if(Counter.isMinuteDeleteAllow(broker)) {
													
														 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
														 broker.addMinuteUpdateCounter();
														 cat.setReason(cat.getReason()+" and Deleted from TN");
														 eremovet++;
													}
												 }
											 } catch (Exception e) {
												 cat.setReason(cat.getReason()+" and Exception in TN");
												 e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Remove Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
													error.setProcess("POS Remove Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("MINI 14 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
													System.out.println("MINI 14 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											 }
										 }
										 String key = null;
										 TNDZonesCategoryTicket catTix = null;
										 for (TNDZonesCategoryTicket tnCatTix :tnNewCatTix) {
											key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tnCatTix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase();
											catTix = exFinalCatTixMap.get(key);
											 if(catTix != null) {
												 catTix.setCreatedDate(now);
												 catTix.setTnCategoryTicketGroupId(tnCatTix.getTnCategoryTicketGroupId());
//												 catTix.setPosPurchaseOrderId(tnCatTix.getPosPurchaseOrderId());
												 catTix.setTnPrice(tnCatTix.getTnPrice());
												 catTix.setIsEdited(true);
												 catTix.setLastUpdated(now);
											 }
											 
										 }
										 
										 for (TNDZonesCategoryTicket tnCatTix : tnUpdateCatTix) {
											 key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tnCatTix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase();
											 catTix = exFinalCatTixMap.get(key);
												
											 if(catTix != null) {
												 catTix.setTnPrice(tnCatTix.getTnPrice());
												 catTix.setIsEdited(true);
												 catTix.setLastUpdated(now);
											 }
											 
										 }
										 
										 for (TNDZonesCategoryTicket tnCatTix : tnRemoveCatTix) {
											 key = tnCatTix.getQuantity()+":"+ tnCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tnCatTix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase();
											 catTix = exFinalCatTixMap.get(key);
												
											 if(catTix != null) {
												 catTix.setStatus("DELETED");
//												 tnCatTix.setTnPrice(0.0);
												 catTix.setIsEdited(true);
												 catTix.setLastUpdated(now);
											 }
											 
										 }
									 }
									 DAORegistry.getTndZonesCategoryTicketDAO().saveOrUpdateAll(updatedBrokerTicketList);
									 DAORegistry.getTndZonesCategoryTicketDAO().saveOrUpdateAll(new ArrayList<TNDZonesCategoryTicket>(exFinalCatTixMap.values()));
									
					 } catch (Exception e) {
						 isErrorOccured = true;
						 error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while Processing Event.");
						// error.setExample(""+e.fillInStackTrace());
						 error.setProcess("Event Processing.");
						 error.setEventId(tnEventId);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("TND Zones 1 : Error while Processing Events.");
						 System.err.println("TND Zones 1 : Error while Processing Events.");
						 e.printStackTrace();
					 }
				 }
				 
				/* if(eventsTixMapFromDb!= null && !eventsTixMapFromDb.isEmpty()) {
					 for (Integer tnEventId : eventsTixMapFromDb.keySet()) {
						 Map<String,TNDZonesCategoryTicket> catsMap = eventsTixMapFromDb.get(tnEventId); 
						
						 if(catsMap!= null && !catsMap.isEmpty()) {
							 List<TNDZonesCategoryTicket> deletedCatsList = new ArrayList<TNDZonesCategoryTicket>(catsMap.values());
							 for(TNDZonesCategoryTicket cat:deletedCatsList){
								 try {
									 cat.setStatus("DELETED");
									 cat.setLastUpdated(now);
									 
									 if(cat.getTnCategoryTicketGroupId()!=null){	
										 if(Counter.isMinuteDeleteAllow(broker)) {
		//									 System.out.println("MINI delete begins..4..."+new Date()+"...count.."+eremovet);
										
											 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
											 broker.addMinuteUpdateCounter();
											 cat.setReason("Ticket not exist in TMAT and Deleted from TN");
											 eremovet++;
										}
									 }
								 } catch (Exception e) {
									 cat.setReason("Ticket not exist in TMAT and Exception in TN");
									 e.printStackTrace();
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("While Remove Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(tnEventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("TND Zones 14 : Error While Remove Category:"+"Event:" + tnEventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
										System.out.println("TND Zones 14 : Error From POS API : Remove Category:"+"Event:" + tnEventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
								 }
							 }
							 DAORegistry.getTndZonesCategoryTicketDAO().updateAll(deletedCatsList);
						 }
					 }
				 }*/
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("TND Zones 1 : Error while Loading Events.");
				 System.err.println("TND Zones 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			 
			 try {
				 try {
					 Integer result = DAORegistry.getTndZonesCategoryTicketDAO().deleteAllTNDZonesCategoryTicketswithinMinimumExcludeEventDays(hourstoConsiderOrders,excludeEventDays);
						 
					 System.out.println("Total minicats Listings within miniumum exclude event days ("+excludeEventDays+") to be removed : "+result);
//					 log.info("Total minicats Listings within miniumum exclude event days ("+minimumExcludeEventDays+") to be removed : "+result);
					 
				 } catch (Exception e) {
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Deleting Records within miniumum exclude event days ("+excludeEventDays+")");
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Deleting Records in TMAT.");
					 error.setEventId(0);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("TNDC 1 : Error while Deleting Records within miniumum exclude event days ("+excludeEventDays+") in TMAT.");
					 System.err.println("TNDC 1 : Error while Deleting Records within miniumum exclude event days ("+excludeEventDays+") in TMAT.");
					 e.printStackTrace();
				 }
				 
				 try {
						//Deleting unmapped MiniCats listings with POS
						 Integer result = DAORegistry.getTndZonesCategoryTicketDAO().deleteAllMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(broker,autopricingProduct);
							System.out.println("Unmapped Larry tn tickets with POS for BROKER " + broker.getId() + " to be removed :"+result);
//							log.info("Unmapped Larry tn tickets with POS for BROKER " + brokerId + " to be removed :"+result);
							
						}catch (Exception e) {
							e.printStackTrace();
							error = new AutoPricingError();
							 error.setProductId(autopricingProduct.getId());
							error.setMessage("Error while removeing unmapped larry listings with POS in TMAT for broker : "+broker.getId());
							error.setExample(""+e.fillInStackTrace());
							error.setProcess("Remove Category.");
							error.setEventId(0);
							error.setTimeStamp(new Date());
							errorList.add(error);
							log.error("TNDC 5 : Error while removeing unmapped larry listings with POS in TMAT for broker : "+broker.getId());
							System.err.println("TNDC 5 : Error while removeing unmapped larry listings with POS in TMAT for broker : "+broker.getId());
						}
						
						//Deleting unmapped MiniCats listings in POS with larryLast category
					 try {
						 List<Integer> nonExistTciketGroupIds = DAORegistry.getTndZonesCategoryTicketDAO().getAllTicketNetworkGroupIdsNotExistInTNDZonesCategoryTicketByBrokerId(broker,autopricingProduct);
						 System.out.println("TN ticket groups Not Exist in Mini for BROKER " + broker.getId() + " to be removed :"+nonExistTciketGroupIds.size());
//						 log.info("TN ticket groups Not Exist in Mini for BROKER " + brokerId + " to be removed :"+nonExistTciketGroupIds.size());
						 
						 for(Integer categoryTicketGroupId : nonExistTciketGroupIds){
								
							try {
								if(Counter.isMinuteDeleteAllow(broker)) {
//									System.out.println("FE API delete begins....."+new Date()+"...count.."+removet);
									
									InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),categoryTicketGroupId);
									broker.addMinuteUpdateCounter();
									//removet++;
								}
							} catch (Exception e) {
									isErrorOccured = true;
									error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									error.setMessage("ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
									error.setProcess("POS Remove Category.");
									error.setExample(""+e.fillInStackTrace());
									error.setEventId(0);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("TNDC 6 : Error While Remove TN ticket groups Not Exist in larrylast ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
									System.err.println("TNDC 6 : Error While Remove TN ticket groups Not Exist in larrylast ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
							}
						 }
					 } catch (Exception e) {
						 
						 error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while Deleting mismatching Records in POS for broker : "+broker.getId());
						 error.setExample(""+e.fillInStackTrace());
						 error.setProcess("Deleting Records in POS.");
						 error.setEventId(0);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("TNDC 7 : Error while Deleting mismatching Records in POS for broker : "+broker.getId());
						 System.err.println("TNDC 7 : Error while Deleting mismatching Records in POS for broker : "+broker.getId());
						 e.printStackTrace();
					 }
				 
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Cleaning Listings.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Cleaning Listings.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("TND Zones 1 : Error while Cleaning Listings.");
				 System.err.println("TND Zones 1 : Error while Cleaning Listings.");
				 e.printStackTrace();
			 }
			 
			/*List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
			
			for(Integer brokerId:brokerAddCountMap.keySet()){
				int count = brokerAddCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Insert");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerUpdateCountMap.keySet()){
				int count = brokerUpdateCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Update");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerDeleteCountMap.keySet()){
				int count = brokerDeleteCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Delete");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);*/
			DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
			System.out.println("TND Zones Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
//			log.info("TND Zones Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
			running = false; 
		 }catch(Exception e){
			 running = false;
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Loading Common Properties.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Loading common Properties..");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("TND Zones 16 :Error while Loading Common Properties.");
			 System.out.println("TND Zones 16 : Error while Loading Common Properties.");
			 e.printStackTrace();
		 }
		 
		 String subject,fileName;
		 Map<String, Object> map;
			
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {
					log.info("CT error size......"+errorList.size());
					
					DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
					subject = "TNDZones Scheduler job failed :";
					fileName = "templates/autopricing-job-failure-message.txt";
					
					//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
					
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
				} catch (Exception e) {
					log.error("TND Zones 17 : Error while Inserting Error Listings in TMAT.");
					 System.out.println("TND Zones 17 :Error while Inserting Error Listings in TMAT.");
					e.printStackTrace();
				}
			}
	 }
	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
//			if(!running){
//				running=true;
				System.out.println("TNDZones Job Called.." + new Date() + ": " + running);
				log.info("TNDZones Job Called.." + new Date() + ": " + running);
				try{
					
					processTNDZonesTickets();
					System.out.println("TND Zones Scheduler Job finished @ " + new Date());
				}catch(Exception e){
					e.printStackTrace();
				}
//				running = false;
//			}
	}


	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		TNDZonesScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		TNDZonesScheduler.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNDZones").getLastRunTime();
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		TNDZonesScheduler.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}


	public static void setNextRunTime(Date nextRunTime) {
		TNDZonesScheduler.nextRunTime = nextRunTime;
	}

}