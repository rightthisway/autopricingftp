package com.rtw.autopricing.util.connection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Connections {
	private static Logger logger = LoggerFactory.getLogger(Connections.class);
	
	private	static Connection tmatConnection;
	private	static Connection zoneTicketsConnection;
	private	static Connection rotConnection;
	private	static Connection mtConnection;
	private	static Connection rtwConnection;
	private	static Connection rtwTwoConnection;
	private	static Connection tixcityConnection;
	private	static Connection tixcityTwoConnection;
	private	static Connection ticketEmpireConnection;
		
	public static Connection getTmatConnection() throws Exception{
		if(tmatConnection!=null){
			try {
				if(!tmatConnection.isClosed()){
					return tmatConnection;
				}
			} catch (SQLException e) {
				System.out.println("..............CONNECTION FOUND CLOSE........");
			}
		}
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tmat.db.url");
		String username =resourceBundle.getString("tmat.db.user.name");
		String password =resourceBundle.getString("tmat.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			tmatConnection = DriverManager.getConnection(url, username, password);
			return tmatConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	
	public static ResultSet getResultSet(Statement statement,String sql){
		try{
			return statement.executeQuery(sql);
		}catch(SQLException sqlEx){
			return null;
		}
	}
	

	public static CallableStatement getCallableStatement(Connection connnection,String sql){
		try{
			return connnection.prepareCall(sql);
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}
		
	}
	
	public  static Connection getZoneTicketsConnection() throws Exception{
		try {
			if(zoneTicketsConnection!=null && !zoneTicketsConnection.isClosed()){
				return zoneTicketsConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("zoneTicketsConnection Exception...."+new Date());
			logger.error("zoneTicketsConnection Exception...."+new Date());
		}
		//System.out.println("TN1 New Conection....");
		logger.info("zoneTicketsConnection new Conection....");
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("zone.tickets.db.url");
		String username =resourceBundle.getString("zone.tickets.db.user.name");
		String password =resourceBundle.getString("zone.tickets.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			zoneTicketsConnection = DriverManager.getConnection(url, username, password);
			return zoneTicketsConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	
	public static Connection getInduxConnection(Integer brokerId) throws Exception {
		if (brokerId == 8087){
			return getRTWInduxConnection();
		}/*else if (brokerId == 11499){
			return getTixcityTwoInduxConnection(false);
		}*//*else if (brokerId == 166){
			return getTixcityInduxConnection(false);
		}*/else if (brokerId == 156){
			return getRtwTwoInduxConnection();
		}else if (brokerId == 7790){
			return getROTInduxConnection();
		}else if (brokerId == 178){
			return getManhattenTicketInduxConnection();
		}else if(brokerId == 4949){
			return getTicketEmpireInduxConnection();
		}
		System.err.println("Returning null connection for broker Id:" + brokerId);
		logger.error("Returning null connection for broker Id:" + brokerId);
		return null;
	}
	
	private  static Connection getROTInduxConnection() throws Exception {
		try {
			if(rotConnection!=null && !rotConnection.isClosed()){
				return rotConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ROT Conection Exception....");
			logger.error("ROT Conection Exception....");
		}
		//System.out.println("TN1 New Conection....");
		logger.info("ROT new Conection....");
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("rot.indux.db.url");
		String username =resourceBundle.getString("rot.indux.db.user.name");
		String password =resourceBundle.getString("rot.indux.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rotConnection = DriverManager.getConnection(url, username, password);
			return rotConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	
	private  static Connection getRTWInduxConnection() throws Exception {
		try {
			if(rtwConnection!=null && !rtwConnection.isClosed()){
				return rtwConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("RTW Conection Exception....");
			logger.error("RTW Conection Exception....");
		}
		//System.out.println("TN1 New Conection....");
		logger.info("RTW new Conection....");
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("rtw.indux.db.url");
		String username =resourceBundle.getString("rtw.indux.db.user.name");
		String password =resourceBundle.getString("rtw.indux.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtwConnection = DriverManager.getConnection(url, username, password);
			return rtwConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	private  static Connection getRtwTwoInduxConnection() throws Exception {
		try {
			if(rtwTwoConnection!=null && !rtwTwoConnection.isClosed()){
				return rtwTwoConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("RTW-2 Conection Exception....");
			logger.error("RTW-2 Conection Exception....");
		}
		//System.out.println("TN1 New Conection....");
		logger.info("RTW-2 new Conection....");
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("rtw.two.indux.db.url");
		String username =resourceBundle.getString("rtw.two.indux.db.user.name");
		String password =resourceBundle.getString("rtw.two.indux.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtwTwoConnection = DriverManager.getConnection(url, username, password);
			return rtwTwoConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	public  static Connection getTixcityInduxConnection(boolean forceToReCreate) throws Exception {
		try {

			if(!forceToReCreate && tixcityConnection!=null && !tixcityConnection.isClosed()){
				return tixcityConnection;
			}
			if(forceToReCreate && tixcityConnection!=null && !tixcityConnection.isClosed()) {
				tixcityConnection.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Tixcity Conection Exception....");
			logger.error("Tixcity Conection Exception....");
		}
		//System.out.println("TN1 New Conection....");
		Date date = new Date();
		logger.info("Tixcity new Conection.start..."+new Date());
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tixcity.indux.db.url");
		String username =resourceBundle.getString("tixcity.indux.db.user.name");
		String password =resourceBundle.getString("tixcity.indux.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			tixcityConnection = DriverManager.getConnection(url, username, password);
			
			long time = new Date().getTime()-date.getTime();
			logger.info("Tixcity new Conection.end..."+time+" : "+new Date());
			
			return tixcityConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			logger.info("Tixcity new Conection Exception.1.."+new Date());
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			logger.info("Tixcity new Conection Exception.2.."+new Date());
			throw ex;
		}
	}
	public  static Connection getTixcityTwoInduxConnection(boolean forceToReCreate) throws Exception {
		try {

			if(!forceToReCreate && tixcityTwoConnection!=null && !tixcityTwoConnection.isClosed()){
				return tixcityTwoConnection;
			}
			if(forceToReCreate && tixcityTwoConnection!=null && !tixcityTwoConnection.isClosed()) {
				tixcityTwoConnection.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Tixcity Two Conection Exception....");
			logger.error("Tixcity Two Conection Exception....");
		}
		//System.out.println("TN1 New Conection....");
		Date date = new Date();
		logger.info("Tixcity Two new Conection.start..."+new Date());
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tixcity.two.indux.db.url");
		String username =resourceBundle.getString("tixcity.two.indux.db.user.name");
		String password =resourceBundle.getString("tixcity.two.indux.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			tixcityTwoConnection = DriverManager.getConnection(url, username, password);
			
			long time = new Date().getTime()-date.getTime();
			logger.info("Tixcity Two new Conection.end..."+time+" : "+new Date());
			
			return tixcityTwoConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			logger.info("Tixcity Two new Conection Exception.1.."+new Date());
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			logger.info("Tixcity Two new Conection Exception.2.."+new Date());
			throw ex;
		}
	}
	/*private  static Connection getTixCityNewInduxConnection() throws Exception {
		try {
			if(tixcityNewConnection!=null && !tixcityNewConnection.isClosed()){
				return tixcityNewConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("TixCityNew Conection Exception....");
			logger.error("TixCityNew Conection Exception....");
		}
		//System.out.println("TN1 New Conection....");
		logger.info("TixCityNew new Conection....");
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tixcitynew.indux.db.url");
		String username =resourceBundle.getString("tixcitynew.indux.db.user.name");
		String password =resourceBundle.getString("tixcitynew.indux.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			tixcityNewConnection = DriverManager.getConnection(url, username, password);
			return tixcityNewConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}*/
	
	private  static Connection getTicketEmpireInduxConnection() throws Exception {
		try {
			if(ticketEmpireConnection!=null && !ticketEmpireConnection.isClosed()){
				return ticketEmpireConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Ticket Empire Conection Exception....");
			logger.error("Ticket Empire Conection Exception....");
		}
		//System.out.println("TN1 New Conection....");
		logger.info("Ticket Empire new Conection....");
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("ticket.empire.indux.db.url");
		String username =resourceBundle.getString("ticket.empire.indux.db.user.name");
		String password =resourceBundle.getString("ticket.empire.indux.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			ticketEmpireConnection = DriverManager.getConnection(url, username, password);
			return ticketEmpireConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	
	
	private  static Connection getManhattenTicketInduxConnection() throws Exception {
		try {
			if(mtConnection!=null && !mtConnection.isClosed()){
				return mtConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Manhattan Conection Exception....");
			logger.error("Manhattan Conection Exception....");
		}
		//System.out.println("TN1 New Conection....");
		logger.info("Manhattan new Conection...."+new Date());
		
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("manhattan.indux.db.url");
		String username =resourceBundle.getString("manhattan.indux.db.user.name");
		String password =resourceBundle.getString("manhattan.indux.db.password");
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			mtConnection = DriverManager.getConnection(url, username, password);
			return mtConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}

}
