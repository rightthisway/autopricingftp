package com.rtw.autopricing.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.util.mail.EmailUtil;

public class AutoPricingTicketnetworkSSAccountFileGenerator extends QuartzJobBean implements StatefulJob{

	private static String folderPath;
	public static final int DEFAULT_TIMEOUT = 30000;
	private static String mailToList;
	
	public static final String getFolderPath() {
		return folderPath;
	}

	public final void setFolderPath(String folderPath) {
		AutoPricingTicketnetworkSSAccountFileGenerator.folderPath = folderPath;
	}

	public static final String getMailToList() {
		return mailToList;
	}

	public final void setMailToList(String mailToList) {
		AutoPricingTicketnetworkSSAccountFileGenerator.mailToList = mailToList;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		
		generateAutoPricingFiles();
		
	}

	public static String getTNSpecialCSV() {
		String message = "";
		String project = "";

		try {
//			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
//				project = "ROT";
//			}
		
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getTNSpeacialCsv();
			
			StringBuilder fileStrBuilder = new StringBuilder();
			fileStrBuilder.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,TicketGroupTypeID,TicketGroupStockTypeID,ShippingMethodSpecialID,ShowNearTermOptionID,InHandDate,TicketGroupObstructionDescriptionID");
			fileStrBuilder.append("\n");
			
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					fileStrBuilder.append(data[0]+",");
					
					if(data[1] != null){
						fileStrBuilder.append(((String) data[1]).replaceAll(",", " ")+",");
					}else{
						fileStrBuilder.append(",");
					}
					
					if(data[2] != null){
						fileStrBuilder.append(((String) data[2]).replaceAll(",", " ")+",");
					}else{
						fileStrBuilder.append(",");
					}
					
					if(data[3] != null){
						fileStrBuilder.append(data[3]+",");
					}else{
						fileStrBuilder.append(",");
					}
					
					if(data[4] != null){
						fileStrBuilder.append(data[4]+",");
					}else{
						fileStrBuilder.append(",");
					}
					
					if(data[5] != null){
						fileStrBuilder.append(data[5]+",");
					}else{
						fileStrBuilder.append(",");
					}
					
					if(data[6] != null){
						fileStrBuilder.append(((String) data[6]).replaceAll("[,.]", " ")+",");
					}else{
						fileStrBuilder.append(",");
					}
					
					if(data[7] != null){
						fileStrBuilder.append(((String) data[7]).replaceAll("[,.]", " ")+",");
					}else{
						fileStrBuilder.append(",");
					}					
					
					fileStrBuilder.append(data[8]+",");
					fileStrBuilder.append(data[9]+",");
					
					if(data[10] != null){
						fileStrBuilder.append(((String) data[10]).replaceAll(",", " ")+",");
					}else{
						fileStrBuilder.append(",");
					}
					
					if(data[11] != null){
						fileStrBuilder.append(data[11]+",");
					}else{
						fileStrBuilder.append(",");
					}
					
					if(data[12] != null){
						fileStrBuilder.append(data[12]+",");
					}else{
						fileStrBuilder.append(",");
					}
					if(data[13] != null){
						fileStrBuilder.append(data[13]+",");
					}else{
						fileStrBuilder.append(",");
					}
					if(data[14] != null){
						fileStrBuilder.append(data[14]+",");
					}else{
						fileStrBuilder.append(",");
					}
					if(data[15] != null){
						fileStrBuilder.append(data[15]+",");
					}else{
						fileStrBuilder.append(",");
					}
					if(data[16] != null){
						fileStrBuilder.append(data[16]+",");
					}else{
						fileStrBuilder.append(",");
					}
					if(data[17] != null){
						fileStrBuilder.append(data[17]+",");
					}else{
						fileStrBuilder.append(",");
					}
					if(data[18] != null){
						fileStrBuilder.append(data[18]);
					}else{
						fileStrBuilder.append(",");
					}
					
					fileStrBuilder.append("\n");
					
				}
			}
		
			File file = new File("\\\\Admit1\\Shared\\Autopricing\\TNSPECIAL\\"+"tnspecial.csv");
			 
			 if(file.exists()){
				 SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				 generateBackupFile(file, "\\\\Admit1\\Shared\\Autopricing\\TNSPECIAL\\backup\\tnspecial" + df.format(new Date())+".csv");
			 }
			 
			if (!file.exists())
				file.createNewFile();
			
			FileWriter vividSeatWriter = new FileWriter(file);				
			vividSeatWriter.append(fileStrBuilder.toString());
			vividSeatWriter.flush();
			vividSeatWriter.close();
			
			message = project+"tnspecial.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = project+"tnspecial.csv" + " - Not generated due to exception";
			return message;
		}
			
	}
	
	private static void generateBackupFile(File sourceFile, String backupFilePath) throws Exception {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(sourceFile);
	        os = new FileOutputStream(new File(backupFilePath));
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    }
	    finally {
	        is.close();
	        os.close();
	    }
	}
	
	public void generateAutoPricingFiles(){

//		String rotMessage = "Below TN Special autopricing files located at S:\\Autopricing\\ROT are generated." + "\n";
//		String tixCityMessage = "Below TIXCITY autopricing files located at S:\\Autopricing\\TIXCITY are generated." + "\t\n";

//		Broker jktBroker = DAORegistry.getBrokerDAO().getBrokerByName("Joe Knows Tickets");
//		Broker rotBroker = DAORegistry.getBrokerDAO().getBrokerByName("Reserve One Tickets");
		
		// BROKER ID 1 = MZTIX , 2 = JoeKnowTicket(JKT) , 3 = ReverseOneTickets(ROT) , 4 = Manhattan
		
		try {
			
//			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNSpecial");
//			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats");
			getTNSpecialCSV();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String messageText = "";//jktMessage + "\n \n \n" + rotMessage;
		String subject = "Autopricing file generation job completed successfully.";
		
		if(messageText.contains("Not generated due to exception")) {
			subject = "Autopricing file generation job failed.";
		}

		 try {
//			sendMailNow(subject, messageText);
			//mailManager.sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth, username, password, emailFrom, emailFrom, toAddress, subject, null, null, mimeType);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void sendMailNow(String subject , String content) throws Exception	{
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailAODev");
		
		
		String smtpHost = DAORegistry.getPropertyDAO().getPropertyByName("smtp.host").getValue().trim();
		int smtpPort = Integer.parseInt(DAORegistry.getPropertyDAO().getPropertyByName("smtp.port").getValue().trim());
		String smtpSecureConnection = DAORegistry.getPropertyDAO().getPropertyByName("smtp.secure.connection").getValue();
		Boolean smtpAuth = Boolean.parseBoolean(DAORegistry.getPropertyDAO().getPropertyByName("smtp.auth").getValue());
		
		String username = null;
		Property smtpUserNameProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.username");
		if (smtpUserNameProperty != null) {
			username = smtpUserNameProperty.getValue();
		}
		
		String fromName =null;
		Property fromNameProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.from");
		if (fromNameProperty != null) {
			fromName = fromNameProperty.getValue();
		}
		String password = null;
		Property smtpUserPasswordProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.password");
		if (smtpUserPasswordProperty != null) {
			password = smtpUserPasswordProperty.getValue();
			if (password.isEmpty()) {
				password = null;
			}
		}
		String emailFrom = DAORegistry.getPropertyDAO().getPropertyByName("smtp.from").getValue();
	
		EmailUtil.sendMailNow(smtpHost, smtpPort, "smtp", smtpSecureConnection, smtpAuth, username, password, fromName, emailFrom, "amit.raut@rightthisway.com", null, null, subject , content, "text/plain");
	}
	
}
