package com.rtw.autopricing.util;

import java.io.StringWriter;
import java.util.Map;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.util.mail.EmailUtil;


public class EmailManager {
	
	public static void sendEmail(String toAddress,String ccAddress,String subject,String fileName,Map<String, Object> map) {

		try {
			
			String smtpHost = DAORegistry.getPropertyDAO().getPropertyByName("smtp.host").getValue().trim();
			int smtpPort = Integer.parseInt(DAORegistry.getPropertyDAO().getPropertyByName("smtp.port").getValue().trim());
			String smtpSecureConnection = DAORegistry.getPropertyDAO().getPropertyByName("smtp.secure.connection").getValue();
			Boolean smtpAuth = Boolean.parseBoolean(DAORegistry.getPropertyDAO().getPropertyByName("smtp.auth").getValue());
			
			String username = null;
			Property smtpUserNameProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.username");
			if (smtpUserNameProperty != null) {
				username = smtpUserNameProperty.getValue();
			}
			
			String fromName =null;
			Property fromNameProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.from");
			if (fromNameProperty != null) {
				fromName = fromNameProperty.getValue();
			}
			String password = null;
			Property smtpUserPasswordProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.password");
			if (smtpUserPasswordProperty != null) {
				password = smtpUserPasswordProperty.getValue();
				if (password.isEmpty()) {
					password = null;
				}
			}
			String emailFrom = DAORegistry.getPropertyDAO().getPropertyByName("smtp.from").getValue();
			
			
			VelocityEngine velocityEngine = new VelocityEngine();
		    ExtendedProperties velocityEngineProperties = new ExtendedProperties();
	        velocityEngineProperties.setProperty(VelocityEngine.RESOURCE_LOADER, "classpath");
		    
	        velocityEngineProperties.setProperty("classpath." + VelocityEngine.RESOURCE_LOADER + ".class",
	        			ClasspathResourceLoader.class.getName());
		    velocityEngine.setExtendedProperties(velocityEngineProperties);
		    
		    StringWriter stringWriter = new StringWriter();	    
		    VelocityContext velocityContext = new VelocityContext();
		    velocityContext.put("map", map);
		    velocityEngine.mergeTemplate(fileName, "iso-8859-1", velocityContext, stringWriter);
		    stringWriter.close();
		    
		    // remove extra whitespaces
		    String content = stringWriter.toString().replaceAll("\\s+", " ").trim();
//			System.out.println(text);
			EmailUtil.sendMailNow(smtpHost, smtpPort, "smtp", smtpSecureConnection, smtpAuth, username, password, fromName, emailFrom, toAddress, ccAddress, null, subject , content, "text/html");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void sendEmail(String toAddress,String ccAddress,String bccAddress,String subject,String fileName,Map<String, Object> map) {

		try {
			
			String smtpHost = DAORegistry.getPropertyDAO().getPropertyByName("smtp.host").getValue().trim();
			int smtpPort = Integer.parseInt(DAORegistry.getPropertyDAO().getPropertyByName("smtp.port").getValue().trim());
			String smtpSecureConnection = DAORegistry.getPropertyDAO().getPropertyByName("smtp.secure.connection").getValue();
			Boolean smtpAuth = Boolean.parseBoolean(DAORegistry.getPropertyDAO().getPropertyByName("smtp.auth").getValue());
			
			String username = null;
			Property smtpUserNameProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.username");
			if (smtpUserNameProperty != null) {
				username = smtpUserNameProperty.getValue();
			}
			
			String fromName =null;
			Property fromNameProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.from");
			if (fromNameProperty != null) {
				fromName = fromNameProperty.getValue();
			}
			String password = null;
			Property smtpUserPasswordProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.password");
			if (smtpUserPasswordProperty != null) {
				password = smtpUserPasswordProperty.getValue();
				if (password.isEmpty()) {
					password = null;
				}
			}
			String emailFrom = DAORegistry.getPropertyDAO().getPropertyByName("smtp.from").getValue();
			
			
			VelocityEngine velocityEngine = new VelocityEngine();
		    ExtendedProperties velocityEngineProperties = new ExtendedProperties();
	        velocityEngineProperties.setProperty(VelocityEngine.RESOURCE_LOADER, "classpath");
		    
	        velocityEngineProperties.setProperty("classpath." + VelocityEngine.RESOURCE_LOADER + ".class",
	        			ClasspathResourceLoader.class.getName());
		    velocityEngine.setExtendedProperties(velocityEngineProperties);
		    
		    StringWriter stringWriter = new StringWriter();	    
		    VelocityContext velocityContext = new VelocityContext();
		    velocityContext.put("map", map);
		    velocityEngine.mergeTemplate(fileName, "iso-8859-1", velocityContext, stringWriter);
		    stringWriter.close();
		    
		    // remove extra whitespaces
		    String content = stringWriter.toString().replaceAll("\\s+", " ").trim();
//			System.out.println(text);
			EmailUtil.sendMailNow(smtpHost, smtpPort, "smtp", smtpSecureConnection, smtpAuth, username, password, fromName, emailFrom, toAddress, ccAddress, bccAddress, subject , content, "text/html");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
