package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Category;
import com.rtw.autopricing.ftp.data.CategoryMapping;
import com.rtw.autopricing.ftp.data.DefaultPurchasePrice;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.LarryLastCategoryTicket;
import com.rtw.autopricing.ftp.data.ManagePurchasePrice;
import com.rtw.autopricing.ftp.data.Ticket;


public class LarryLastCategoryGroupManager {
	public static Logger logger = LoggerFactory.getLogger(LarryLastCategoryGroupManager.class);
	static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	static DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	public static int PURCHASE_PRICE_THRESHHOLD = 10;
	public static int MAX_PUR_PRICE_THRESHOLD_PERCENTAGE = 200;
	
	
	public static Collection<Ticket> getTMATActiveTickets(Event event,AutopricingProduct product,Boolean isAllowSectionRange) throws Exception {
		
		List<Ticket> newFinalTicketList = new ArrayList<Ticket>();
		Collection<Ticket> finalTicketList = new ArrayList<Ticket>();
		Collection<Category> categoryList = null;
		
		Collection<Ticket> ticketList = CategoryGroupManager.getTMATActiveTicketsFromDB(event, product);
		
		try {
			
			categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
			ticketList = TicketUtil.removeDuplicateTickets(ticketList);
			ticketList = TicketUtil.removeAdmitoneTickets(ticketList, event, event.getAdmitoneId(),isAllowSectionRange);
			ticketList = TicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
			
			//Purchase tour = DAORegistry.getPurchaseDAO().getPurchaseByEventId(event.getId());
			Collection<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAll();
			Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
			for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
				defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
			}
			Collection<ManagePurchasePrice> managePurchasePricelist = DAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());//tour.getTmatPurchaseId());
			Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
			for(ManagePurchasePrice tourPrice:managePurchasePricelist){
				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
			}
			
			for (Ticket ticket : ticketList) {
				TicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);
				if(ticket.getPurchasePrice() < PURCHASE_PRICE_THRESHHOLD) {
					continue;
				}
				//Tamil : Added to skip tickets with quantity 2 and purchase price below 100 in auto pricing
				if(ticket.getQuantity().equals(2) && ticket.getPurchasePrice()< 100) {
					continue;
				}
				finalTicketList.add(ticket);
			}
			//Removing Fake Tickets by Maximum Purchase Price Threshold
			finalTicketList = TicketUtil.removeFakeTicketsByCategoryandSection(finalTicketList, MAX_PUR_PRICE_THRESHOLD_PERCENTAGE);
			
			
			for (Ticket ticket : finalTicketList) {
				//Tamil : for Broadway events process premium zone tickets only in all products
				/*if(event.getChildCategoryName().equalsIgnoreCase("Broadway") && 
					(ticket.getCategory() != null && ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().equalsIgnoreCase("premium"))) {
					continue;
				}*/
				newFinalTicketList.add(ticket);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + e.fillInStackTrace());
			System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
			throw e;
		}
		return newFinalTicketList;
	}
	
	public static List<CategoryTicket> computeCategoryTickets(Event event,ExchangeEvent exEvent,Collection<Ticket> finalTicketList,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,AutopricingProduct product) throws Exception {
		List<CategoryTicket> catTixList = new ArrayList<CategoryTicket>();
		//Map<Integer, List<AutoCats96CategoryTicket>> qtyCatTixMap = new HashMap<Integer, List<AutoCats96CategoryTicket>>();
		try{
			
			Integer minEligibleSections = Integer.parseInt(exEvent.getExposure().split("-")[0]);
			if(minEligibleSections > 2){
				minEligibleSections = 2;
			}
			sectionEligibleMinEntries = minEligibleSections;
			
			if(null != exEvent.getLarryLastEnabled() && exEvent.getLarryLastEnabled()){
				getLarryLastCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList,product);
			}
			if(null != exEvent.getZonedLastrowMinicatsEnabled() && exEvent.getZonedLastrowMinicatsEnabled()){
				getZoneLastRowMiniCatCategoryTickets(event, exEvent, defaultExpectedArrivialDatePriorDays, sectionEligibleMinEntries, tnExAddMarkup,
						vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup, fanxchangeAddMarkup, ticketcityAddMarkup, finalTicketList, catTixList, product);
			}
				
			//System.out.println("TOTAL CATEGORY TICKETS : "+catTixList.size());
			 
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + e.fillInStackTrace());
			System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
			throw e;
		}
		return catTixList;
	}
	
	public static List<CategoryTicket> getLarryLastCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,
			Collection<Ticket> finalTicketList, List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception{
		try{
			
			Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
			Set<Ticket> sectionTickets = null;
			for (Ticket ticket : finalTicketList) {
				CategoryMapping catmapping = ticket.getCategoryMapping();
				if(catmapping != null && catmapping.getLarrySection() != null && catmapping.getLarrySection().length() > 0 &&
						 catmapping.getLastRow() != null && catmapping.getLastRow().length() > 0) {
					String section = catmapping.getLarrySection().replaceAll("\\s+", " ").trim().toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>(); 
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
				}
			}
			//Map<String,Set<String>> larrySectionMap = DAORegistry.getQueryManagerDAO().getAllLarrySectionAndLastRowByVenueCategoryId(event.getVenueCategoryId());
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLarryLastProductsLarrySectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				int quantity;
				//Set<String> lastRows = larrySectionMap.get(key.toUpperCase());
				//if(lastRows == null || lastRows.isEmpty()) {
					//continue;
				//}
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				//String rowRange = categoryMapping.getRowRange();
				//String lastRow = categoryMapping.getLastRow();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				for (int i = 1; i <= 9; i++) {
					if(i ==2 || i== 4 ) {//|| i == 6
						
						//Tamil : for Hamilton events Process only 2 and 4 qty listings for SS account as business requirement
						if(i == 6 && event.getName().equalsIgnoreCase("Hamilton")) {
							continue;
						}
						
						quantity = i;
						LarryLastCategoryTicket catTicket = new LarryLastCategoryTicket();		
						catTicket.setInternalNotes(AutoExchangeEventLoader.LARRYLAST_INTERNAL_NOTES);
						
						catTicket = (LarryLastCategoryTicket)CategoryGroupManager.computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							
							//If LarrySection ends with SPECIAL then we have to create duplicate ticket with Sections like LEFT and RIGHT
							String sectionPrefix = null,section1 = null,section2=null;
							if(catTicket.getSection() != null && catTicket.getSection().toUpperCase().contains("SPECIAL")) {
								sectionPrefix = catTicket.getSection().replaceAll("\\s+", " ").toUpperCase().replaceAll("SPECIAL", "");
								section1 = sectionPrefix.trim() + " LEFT";
								section2 = sectionPrefix.trim() + " RIGHT";
							} else {
								section1 = catTicket.getSection();
							}
							catTicket.setSection(section1);
							catTicket.setLastRow(miniLastRow);
							catTicket.setProductPriority(2);
							
							catTixList.add(catTicket);
							
							if(section2 != null) {
								LarryLastCategoryTicket newCatTicket = new LarryLastCategoryTicket(catTicket);
								newCatTicket.setSection(section2);
								newCatTicket.setLastRow(miniLastRow);
								catTixList.add(newCatTicket);
							}
							// Need to work for other products..
							//If more than one different last row exist in CSV for single larrySection(CSV - G Column) we have to create create duplicate listings with all last row values.
							/*for (String lastRow : lastRows) {
								if(!catTicket.getLastRow().equalsIgnoreCase(lastRow)) {
									LarryLastCategoryTicket newCatTicket = new LarryLastCategoryTicket(catTicket);
									newCatTicket.setLastRow(lastRow);
									newCatTicket.setSection(section1);
									catTixList.add(newCatTicket);
									
									if(section2 != null) {
										newCatTicket = new LarryLastCategoryTicket(newCatTicket);
										newCatTicket.setSection(section2);
										catTixList.add(newCatTicket);
									}	
								}
							}*/
						}
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			//System.out.println(e.fillInStackTrace());
			//return null;
			throw e;
		}
		
		return catTixList;
	}
	
	public static List<CategoryTicket> getZoneLastRowMiniCatCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,
			Collection<Ticket> finalTicketList,List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		try{
			Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
			Set<Ticket> sectionTickets = null;
			for (Ticket ticket : finalTicketList) {
				String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
				sectionTickets = sectionTicketMap.get(section);
				
				if(null == sectionTickets) {
					sectionTickets = new HashSet<Ticket>();
					sectionTicketMap.put(section, sectionTickets);
				}
				sectionTickets.add(ticket);
			}
			
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			String eventExposure = exEvent.getExposure();
			Integer eventSectionEligibleMinEntries = sectionEligibleMinEntries;
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				String rowRange = categoryMapping.getRowRange();
				//String lastRow = categoryMapping.getLastRow();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				if(rowRange == null) {
					rowRange = "";
				}
				
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				//for (Ticket tix : sectionTickets) {
						//System.out.println("tix id..."+tix.getId()+"...section..."+tix.getNormalizedSection()+"...purprice.."+tix.getPurchasePrice()+"..qty.."+tix.getQuantity()+"...row ..."+tix.getNormalizedRow());
				//}
				//System.out.println("section..."+key+"..rowrange.."+rowRange+"..lastrow.."+lastRow+"..minilastrow.."+miniLastRow);
				
				Map<Integer,CategoryTicket> catsMap = new HashMap<Integer, CategoryTicket>();
				for (int i = 1; i <= 9; i++) {
					if(i ==2 || i== 4 ) { //|| i == 6
						int quantity = i;
						
						CategoryTicket catTicket = new LarryLastCategoryTicket();
						catTicket.setInternalNotes(AutoExchangeEventLoader.ZONED_LASTROW_INTERNAL_NOTES);
						
						//Tamil : for Hamilton events Process only 2 and 4 qty listings for SS account as business requirement
						if(i == 6 && event.getName().equalsIgnoreCase("Hamilton")) {
							continue;
						}
						exEvent.setExposure(eventExposure);
						sectionEligibleMinEntries=eventSectionEligibleMinEntries;
						
						catTicket = CategoryGroupManager.computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							catTicket.setProductPriority(1);
							catTicket.setLastRow(miniLastRow);
							catTicket.setRowRange(rowRange);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							
							//catTixList.add(catTicket);
							catsMap.put(quantity, catTicket);
						}
					}
				}
				/*
				 * Compute tickets for lastfive rows from lastrow of each sections
				 * 6qty for last row
				 * 4 qty for last 2nd row
				 * 2 quantity for last 3rd,4th and 5th rows
				 * if ther is no 4 or qty then consider 2 qty listing for all five rows
				 * for last 2nd row computation remove last row listings and check min price from last2nd row if no tix then use last row tix
				 * for lease row comutations use exposure as 1-OXP and mintixCount to 1
				 * 
				 * if lastrow is like GA or BOX ,etc then consider TMAT tickets with row as lastrow only.
				 * consider cats listings row as csv lastorw value.
				 * create listings for qty 2 and 4 with same lastrow value.
				 * if minlastorw list is null then consider that row is like GA or BOX type row
				*/
				if (!catsMap.isEmpty()) {

					int size = 2;
					Map<Integer,String> minLastRowMap = Categorizer.getMiniLastRow(miniLastRow,size);
					
					if(!minLastRowMap.isEmpty()) {
						
						int subtractRowCount = 0;
						String tempRow = null;
						int quantity = 4;
						Set<Ticket> qtyTempSectionTickets = new HashSet<Ticket>(sectionTickets);
						for(int i=0;i<2;i++) {
							CategoryTicket catTicket = null;
							
								if(tempRow != null) {
									eligibleRowList.remove(tempRow.toLowerCase());
									
									
									Set<Ticket> qtySectionTickets = new HashSet<Ticket>();
									for (Ticket ticket : qtyTempSectionTickets) {
										if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
											qtySectionTickets.add(ticket);
										} 
									}
									exEvent.setExposure("1-OXP");
									sectionEligibleMinEntries=1;
									
									qtyTempSectionTickets = new HashSet<Ticket>(qtySectionTickets);
									catTicket = new LarryLastCategoryTicket();
									catTicket.setInternalNotes(AutoExchangeEventLoader.ZONED_LASTROW_INTERNAL_NOTES);
									
									catTicket = CategoryGroupManager.computecategoryTicket(qtyTempSectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
											tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
									
									if(catTicket != null) {
										catTicket.setProductPriority(1);
										catTicket.setLastRow(miniLastRow);
										catTicket.setRowRange(rowRange);
										catTicket.setPosExchangeEventId(event.getAdmitoneId());
										//catTicket.setExpectedArrivalDate(expectedArrivalDate);
										catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
										//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
										
										//catTixList.add(catTicket);
										catsMap.put(quantity, catTicket);
									}
								} 
								
								
								if(catTicket == null) {
									while(true) {
										catTicket = catsMap.get(quantity);
										if(quantity==6) {
											quantity=4;
										} else if(quantity == 4) {
											quantity =2;
										} else if(quantity == 2) {
											break;
										}
										if(catTicket != null) {
											catTicket = new LarryLastCategoryTicket(catTicket);
											break;
										}
									}
								} else {
									if(quantity==6) {
										quantity=4;
									} else if(quantity == 4) {
										quantity =2;
									}
								}
								
								tempRow = minLastRowMap.get(subtractRowCount);
								if(catTicket != null && tempRow != null) {
									catTicket.setLastRow(tempRow.toUpperCase());
									catTixList.add(catTicket);
									subtractRowCount++;
								} else {
									tempRow = null;
								}
						}
					} else {
						CategoryTicket catTicket = catsMap.get(4);
						if(catTicket != null) {
							catTixList.add(catTicket);
						}
						catTicket = catsMap.get(2);
						if(catTicket != null) {
							catTixList.add(catTicket);
						}
					}
				
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
}
