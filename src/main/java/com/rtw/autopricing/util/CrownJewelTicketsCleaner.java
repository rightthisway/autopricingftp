package com.rtw.autopricing.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.zonetickets.dao.implementaion.ZoneTicketDAORegistry;


public class CrownJewelTicketsCleaner extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(CrownJewelTicketsCleaner.class);
	private static Boolean running = false;
	
	public void cleanAllPOS(){
		
		if(isRunning()){
			return ;
		}
		setRunning(true);
		
		 ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		 String toAddress = resourceBundle.getString("emailNotificationTo");
		 String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		 AutoPricingError error= null;
		 List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		 boolean isErrorOccured = false;
		 
		 AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("CrownJewel");
		 AutopricingExchange zoneTicketExchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("RewardTheFan");
		 Broker broker = DAORegistry.getBrokerDAO().getBrokerByName("MZTix");
		 
		 if(autopricingProduct.getStopped()){
			 setRunning(false);
			 //setStopped(true);
			 System.out.println("CrownJewel Ticket Cleaner job skiped.");
			 log.info("CrownJewel Ticket Cleaner job skiped.");
				
			 return;
		 }
		  
		 AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(),autopricingProduct.getId(), zoneTicketExchange.getId());
		 Integer minimumExcludeEventDays = 2;
		 try {
			 if(autopricingSettings != null) {
				 minimumExcludeEventDays = autopricingSettings.getExcludeEventDays();				 
			 } 
			 //minimumExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
			 Integer result = DAORegistry.getCrownJewelCategoryTicketDAO().deleteAllCrownJewelCategoryTicketswithinMinimumExcludeEventDays(minimumExcludeEventDays);
				 
			 System.out.println("Total CrownJewel Listings within miniumum ExcludeEventDays event days ("+minimumExcludeEventDays+") to be removed : "+result);
			 
		 } catch (Exception e) {
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+")");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Deleting Records in TMAT.");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("CrownJewel 1 : Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+") in TMAT.");
			 System.err.println("CrownJewel 1 : Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+") in TMAT.");
			 e.printStackTrace();
		 }
		 
		 try {//delete all CrownJewel listings which are not exist in exchange event for CrownJewels exchange 
			 Integer result = DAORegistry.getCrownJewelCategoryTicketDAO().deleteAllCrownJewelCategoryTicketsnotExistinCrownJewelEventforZoneTicketExchange();
				 
			 System.out.println("Total Listings Not Exist in Exchange Event for any Exchange to be removed : "+result);
//			 log.info("Total Listings Not Exist in Exchange Event for any Exchange to be removed : "+result);
			 
		 } catch (Exception e) {
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Deleting Records in TMAT.");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("CrownJewel 2 : Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
			 System.err.println("CrownJewel 2 : Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
			 e.printStackTrace();
		 }
		 
		 try {
			 //Deleting unmapped CrownJewelsprocessor listings with CrownJewels ticket group
			 Integer result = DAORegistry.getCrownJewelCategoryTicketDAO().deleteAllCrownJewelCategoryTicketGroupIdsNotExistInZoneTicketsCJCategoryTicket();
			 System.out.println("Unmapped CrownJewel tickets with CrownJewels ticket group  to be removed :"+result);
					
		 }catch (Exception e) {
			 e.printStackTrace();
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while removeing Unmapped CrownJewelProcessor tickets with CrownJewels ticket grop");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Remove ticket group listings in CrownJewels.");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("CrownJewel 5 : Error while removeing Unmapped CrownJewel tickets with CrownJewels ticket grop");
			 System.err.println("CrownJewel 5 : Error while removeing Unmapped CrownJewel tickets with CrownJewels ticket grop");
		 }
				
		 //Deleting unmapped CrownJewels processor listings in zone ticket group
		 try {
			 List<Integer> nonExistTciketGroupIds = DAORegistry.getCrownJewelCategoryTicketDAO().getAllCrownJewelTicketsTicketGroupIdsNotExistInTmat();
			 System.out.println("Zone ticket groups Not Exist in tmat zone tickets processor to be removed :"+nonExistTciketGroupIds.size());
			 
			 ZoneTicketDAORegistry.getZtCrownJewelCatTicketGroupDAO().deleteByZoneTicketGroupIds(nonExistTciketGroupIds);
					
		 } catch (Exception e) {
			 
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Deleting mismatching Records in zone tickets ticket group");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Remove ticket group listings in CrownJewels.");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("CrownJewel 7 : Error while Deleting mismatching Records in zone tickets ticket group");
			 System.err.println("CrownJewel 7 : Error while Deleting mismatching Records in zone tickets ticket group");
			 e.printStackTrace();
		 }
		 
		
		running = false;
		
		String subject,fileName;
		 Map<String, Object> map;
		
		if(isErrorOccured){
			map = new HashMap<String, Object>();
			map.put("error", error);
			map.put("errors", errorList);
			try {
//				log.info("CT error size......"+errorList.size());
				
				DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
				subject = "CrownJewels Ticket Cleaner job failed :";
				fileName = "templates/autopricing-job-failure-message.txt";
				
				//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
				
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
				
			} catch (Exception e) {
				log.error("CrownJewel 8 : Error while Inserting Error Listings in TMAT.");
				 System.err.println("CrownJewel 8 : Error while Inserting Error Listings in TMAT.");
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		cleanAllPOS();
	}
	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		CrownJewelTicketsCleaner.running = running;
	}
}
