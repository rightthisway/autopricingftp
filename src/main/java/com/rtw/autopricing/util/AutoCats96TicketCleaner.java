package com.rtw.autopricing.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;


public class AutoCats96TicketCleaner extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(AutoCats96TicketCleaner.class);
	private static Boolean running = false;
	
	public void cleanAllPOS(){
		
		if(isRunning()){
			return ;
		}
		setRunning(true);
		
		 Date now = new Date(); 
		 int tUpdate=0,tremovet=0;
		 int update=0,removet=0;
		 ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		 String toAddress = resourceBundle.getString("emailNotificationTo");
		 String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		 AutoPricingError error= null;
		 List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		 boolean isErrorOccured = false;
		 
		 Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
		 
		 AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("AutoCats96");
		 AutopricingExchange tnExchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
		 
		 if(autopricingProduct.getStopped()){
			 setRunning(false);
			 //setStopped(true);
			 System.out.println("AutoCats96 Ticket Cleaner job skiped.");
			 log.info("AutoCats96 Ticket Cleaner job skiped.");
				
			 return;
		 }
		 
		 Map<Integer,AutopricingSettings> autopricingSettingsMap = new HashMap<Integer, AutopricingSettings>();
		 List<AutopricingSettings> autopricingSettingsList = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettingsByProductIdandExchangeId(autopricingProduct.getId(), tnExchange.getId());
		 for (AutopricingSettings autopricingSettings : autopricingSettingsList) {
			 autopricingSettingsMap.put(autopricingSettings.getBrokerId(), autopricingSettings);
		 }
		 
		 Integer minimumExcludeEventDays = 0;
		 try {
			 minimumExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
			 Integer result = DAORegistry.getAutoCats96CategoryTicketDAO().deleteAllAutoCats96CategoryTicketswithinMinimumExcludeEventDays(minimumExcludeEventDays);
				 
			 System.out.println("Total AutoCats96 Listings within miniumum exclude event days ("+minimumExcludeEventDays+") to be removed : "+result);
			 
		 } catch (Exception e) {
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+")");
			 error.setProcess("Deleting Records in TMAT.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("AC96 1 : Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+") in TMAT.");
			 System.err.println("AC96 1 : Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+") in TMAT.");
			 e.printStackTrace();
		 }
			 
		List<Integer> autoCats96ActiveBrokerIds = AutoCats96Utils.getAutoCatsActiveBrokerIds();
		for(Integer brokerId : autoCats96ActiveBrokerIds){
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			try {
				Integer result = DAORegistry.getAutoCats96CategoryTicketDAO().deleteAllAutoCats96CategoryTicketsnotExistinExchangeEventByBroker(brokerId);
				System.out.println("Total onsale Listings Not Exist in Exchange Event for Broker: "+broker.getName()+": to be removed : "+result);
				
				//Tamil : presale evnet added in autocats96_exchange_Evnet_table itself and processed from lastrow products
				//result = DAORegistry.getAutoCats96CategoryTicketDAO().deleteAllPresaleAutoCatsCategoryTicketsnotExistinExchangeEventByBroker(brokerId);
				//System.out.println("Total presale Listings Not Exist in Exchange Event for Broker: "+broker.getName()+": to be removed : "+result);
				 
			 } catch (Exception e) {
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting Records not exist in exchange Event for Broker "+broker.getName()+" in TMAT.");
				 error.setProcess("Deleting Records in TMAT.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("AC96 2 : Error while Deleting Records not existin exchange Event for Broker: "+broker.getName()+": in TMAT.");
				 System.err.println("AC96 2 : Error while Deleting Records not existin exchange Event for Broker: "+broker.getName()+": in TMAT.");
				 e.printStackTrace();
			 }
		}
		
		List<String> autoCats96InternalNotes = AutoCats96Utils.getAutoCats96InternalNotes();
		
		for(String internalNotes : autoCats96InternalNotes){

			try {
				Integer result = DAORegistry.getAutoCats96CategoryTicketDAO().deleteAllAutoCats96CategoryTicketsInWhichProductDisabled(internalNotes);
				System.out.println("Total onsale Listings of: "+internalNotes+": to be removed : "+result);
				
				if(internalNotes.equals("MINICATS") || internalNotes.equals("AUTOCAT")) {
					//Tamil : presale evnet added in autocats96_exchange_Evnet_table itself and processed from lastrow products
					//result = DAORegistry.getAutoCats96CategoryTicketDAO().deleteAllPresaleAutoCatsCategoryTicketsInWhichProductDisabled(internalNotes);
					//System.out.println("Total presale Listings of: "+internalNotes+": to be removed : "+result);
				}
				 
			 } catch (Exception e) {
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting Records disable "+internalNotes+" tickets in TMAT.");
				 error.setProcess("Deleting Records in TMAT.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("AC96 2 : Error while Deleting Records disable "+internalNotes+" tickets in TMAT.");
				 System.err.println("AC96 2 : Error while Deleting Records disable "+internalNotes+" tickets in TMAT.");
				 e.printStackTrace();
			 }
		}
		 
		List<Integer> brokerIdList = BrokerUtils.getAllBrokersId();
		for(Integer brokerId : brokerIdList){
			update=0;removet=0;
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			
			AutopricingSettings tnautopricingSettings = autopricingSettingsMap.get(broker.getId());
			if(tnautopricingSettings == null || !tnautopricingSettings.getIsEnabled()) {
				System.out.println("1 AutoCats96 Ticket Cleaner job skiped for BROKER " + brokerId);
				log.info("1 AutoCats96 Ticket Cleaner job skiped for BROKER " + brokerId);
				continue;
			}
			
			try {
				 if(tnautopricingSettings.getExcludeEventDays()> minimumExcludeEventDays) {
					 
					 Integer result = DAORegistry.getAutoCats96CategoryTicketDAO().deleteAllTnAutoCats96CategoryTicketswithinExcludeEventDaysByBroker(brokerId,tnautopricingSettings.getExcludeEventDays());
					 System.out.println("Total TN Listings Not Exist in Exchange Event for BROKER " + brokerId + " to be removed : "+result);
				 }
				 
			 } catch (Exception e) {
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting TN Listings Not Exist in Exchange Event  for broker : "+brokerId);
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Deleting Records in TMAT.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("AC96 3 : Error while Deleting TN Listings Not Exist in Exchange Event  for broker : "+brokerId);
				 System.err.println("AC96 3 : Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId); 
				 e.printStackTrace();
			 }
			 
			 /*try {
				 Integer result = DAORegistry.getAutoCats96CategoryTicketDAO().deleteAllTnAutoCats96CategoryTicketsnotExistinExchangeEventForTNByBroker(brokerId);
				 
				 System.out.println("Total TN Listings Not Exist in Exchange Event for BROKER " + brokerId + " to be removed : "+result);
				 
			 } catch (Exception e) {
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Deleting Records in TMAT.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("AC96 4 : Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);
				 System.err.println("AC96 4 : Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);
				 e.printStackTrace();
			 }*/
			 
			 try {
				//Deleting unmapped AutoCats96 listings with POS
				 Integer result = DAORegistry.getAutoCats96CategoryTicketDAO().deleteAllAutoCats96TnCategoryTicketGroupIdsNotExistInPOSByBrokerId(broker,autopricingProduct);
				 System.out.println("Unmapped AutoCats96 tn tickets with POS for BROKER " + brokerId + " to be removed :"+result);
					
				}catch (Exception e) {
					e.printStackTrace();
					error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					error.setMessage("Error while removeing unmapped AutoCats96 listings with POS in TMAT for broker : "+brokerId);
					 error.setExample(""+e.fillInStackTrace());
					error.setProcess("Remove Category.");
					error.setEventId(0);
					error.setTimeStamp(new Date());
					errorList.add(error);
					log.error("AC96 5 : Error while removeing unmapped AutoCats96 listings with POS in TMAT for broker : "+brokerId);
					System.err.println("AC96 5 : Error while removeing unmapped AutoCats96 listings with POS in TMAT for broker : "+brokerId);
				}
				
				//Deleting unmapped MiniCats listings in POS with autoCats96 category
			 try {
				 List<Integer> nonExistTciketGroupIds = DAORegistry.getAutoCats96CategoryTicketDAO().getAllTicketNetworkGroupIdsNotExistInAutoCats96CategoryTicketByBrokerId(broker,autopricingProduct);
				 System.out.println("TN ticket groups Not Exist in AutoCats96 for BROKER " + brokerId + " to be removed :"+nonExistTciketGroupIds.size());
//				 log.info("TN ticket groups Not Exist in Mini for BROKER " + brokerId + " to be removed :"+nonExistTciketGroupIds.size());
				 
				 for(Integer categoryTicketGroupId : nonExistTciketGroupIds){
						
					try {
						if(Counter.isMinuteDeleteAllow(broker)) {
//							System.out.println("FE API delete begins....."+new Date()+"...count.."+removet);
							
							InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),categoryTicketGroupId);
							broker.addMinuteUpdateCounter();
							removet++;
						}
					} catch (Exception e) {
							isErrorOccured = true;
							error = new AutoPricingError();
							error.setProductId(autopricingProduct.getId());
							error.setMessage("ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
							error.setProcess("POS Remove Category.");
							 error.setExample(""+e.fillInStackTrace());
							error.setEventId(0);
							error.setTimeStamp(new Date());
							errorList.add(error);
							log.error("AC96 6 : Error While Remove TN ticket groups Not Exist in autoCats96 ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
							System.err.println("AC96 6 : Error While Remove TN ticket groups Not Exist in autoCats96 ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
					}
				 }
			 } catch (Exception e) {
				 
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting mismatching Records in POS for broker : "+brokerId);
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Deleting Records in POS.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("AC96 7 : Error while Deleting mismatching Records in POS for broker : "+brokerId);
				 System.err.println("AC96 7 : Error while Deleting mismatching Records in POS for broker : "+brokerId);
				 e.printStackTrace();
			 }
			 tUpdate = tUpdate + update;
			 tremovet= tremovet + removet;
			 
			 Integer deleteCount = brokerDeleteCountMap.get(broker.getId());
			 if(deleteCount==null){
				 deleteCount = 0;
			 }
			 deleteCount= deleteCount + removet;
			 brokerDeleteCountMap.put(broker.getId(),deleteCount);
		}
		running = false;
		
		List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
		
		for(Integer brokerId:brokerDeleteCountMap.keySet()){
			int count = brokerDeleteCountMap.get(brokerId);
			if(count==0){
				continue;
			}
			AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			addAudit.setProduct(autopricingProduct);
			addAudit.setBroker(broker);
			addAudit.setCount(count);
			addAudit.setProcessType("Delete");
			addAudit.setLastRunTime(now);
			autoCatsProjectAuditList.add(addAudit);
			
		}
		DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);

		String subject,fileName;
		 Map<String, Object> map;
		
		if(isErrorOccured){
			map = new HashMap<String, Object>();
			map.put("error", error);
			map.put("errors", errorList);
			try {
				
				DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
				subject = "AutoCats96 Ticket Cleaner job failed :";
				fileName = "templates/autopricing-job-failure-message.txt";
				
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
				
			} catch (Exception e) {
				log.error("MC 8 : Error while Inserting Error Listings in TMAT.");
				 System.err.println("MC 8 : Error while Inserting Error Listings in TMAT.");
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		cleanAllPOS();
	}
	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		AutoCats96TicketCleaner.running = running;
	}
}
