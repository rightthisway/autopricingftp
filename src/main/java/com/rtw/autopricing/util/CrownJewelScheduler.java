package com.rtw.autopricing.util;

//import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.CrownJewelCategoryTicket;
import com.rtw.autopricing.ftp.data.CrownJewelEvents;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneTicketsLockedTicketEventDetails;
import com.rtw.autopricing.ftp.zonetickets.dao.implementaion.ZoneTicketDAORegistry;
import com.rtw.autopricing.ftp.zonetickets.data.ZoneTicketsEvent;


public class CrownJewelScheduler  extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(CrownJewelScheduler.class);
	
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 
	 
	 public static void processCrownJewelsProcessorTickets() throws Exception{
		Calendar cal =  Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.MINUTE,5);
		setNextRunTime(cal.getTime());
		if(isStopped() || isRunning()){
			return ;
		}
		setRunning(true);
		
		
		Date lastRunTime = getLastUpdateTime();
		Long minute = 0l;
		if(lastRunTime!=null){
			minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
		}else{
			minute = 2880l; // 2 Days in minute
		}
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("CrownJewel");
		if(autopricingProduct==null){
			autopricingProduct = new AutopricingProduct();
			autopricingProduct.setName("CrownJewel");
			autopricingProduct.setStatus("ACTIVE");
			autopricingProduct.setStopped(false);
		}
		if(autopricingProduct.getStopped()){
			setStopped(true);
			setRunning(false);
			return;
		}
		
		setLastUpdateTime(now);
		autopricingProduct.setLastRunTime(now);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
//		Date jobStartTime = new Date();
		int tInsert=0,tUpdate=0,tremovet=0,tposCount=0;
		
		 try{
			 Double tnExAddMarkup = 0.0;
			 Double vividExAddMarkup = 0.0;
			 Double scorebigMarkup = 0.0;
			 Double fanxchangeMarkup = 0.0;
			 Double ticketcityMarkup = 0.0;
			 Double tickPickExAddMarkup = 0.0;
			 Double scoreBigExAddMarkup = 0.0;
				
			 Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
			 Collection<Event> eventList = null;
			 Collection<CrownJewelEvents> zoneTicketsExchangeEvents = null;
			 
			 try {
				 //Get all events that is updated in tmat since last run time. 
				 zoneTicketsExchangeEvents = DAORegistry.getCrownJewelEventsDAO().getAllCrownJewelEventsEligibleForUpdate(minute,autopricingProduct);
				 System.out.println("CrownJewel Event Size : "+zoneTicketsExchangeEvents.size());
				 
				 eventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentId();
				 for(Event event:eventList) {
					eventMap.put(event.getId(), event);
					eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
				 }
				 System.out.println("CrownJewel : TMAT event size : "+eventList.size());
				 
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("CrownJewel 1 : Error while Loading Events.");
				 System.err.println("CrownJewel 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			 Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			 Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			 /*try {
				 List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductId(autopricingProduct.getId());
				 for (ExcludeEventZones excludeEventZone : excludeEventZones) {
					 Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
					 if(eventZones == null) {
						 eventZones = new HashSet<String>();
					 }
					 eventZones.add(excludeEventZone.getBrokerId()+"_"+excludeEventZone.getZone().toUpperCase());
					 excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
				}
				 List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductId(autopricingProduct.getId());
				 for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
					 Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
					 if(venueCategoryZones == null) {
						 venueCategoryZones = new HashSet<String>();
					 }
					 venueCategoryZones.add(excludeVenueCategoryZone.getBrokerId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
					 excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
				}
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Exclude Zones.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Exclude Zones Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("MINI 1 : Error while Loading Exclude Zones..");
				 System.err.println("MINI 1 : Error while Loading Exclude Zones..");
				 e.printStackTrace();
			 }*/
			 
			 List<ZoneTicketsEvent> zoneTicketEvents = new ArrayList<ZoneTicketsEvent>();
			 Map<Integer,ZoneTicketsEvent> zoneTicketEventMap = new HashMap<Integer, ZoneTicketsEvent>();
			
			 int eventSize = zoneTicketsExchangeEvents.size();
			 Integer i=0;
			 Integer minimamExcludeEventDays = 0;//DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
			 
			 Broker broker = DAORegistry.getBrokerDAO().getBrokerByName("MZTix");
			 AutopricingExchange zoneTicketExchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("RewardTheFan");
			 AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), zoneTicketExchange.getId()); 

			 boolean isZoneEvent = true;
			 boolean isUpdateZone = true;
			 if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
				 isUpdateZone = false;
			 }
			 List<ZoneTicketsLockedTicketEventDetails> ztLockedEventDetails = new ArrayList<ZoneTicketsLockedTicketEventDetails>();
			Set<Integer> zoneEventIdsWithLockedTickets = null;
			Date lockedEventIdLoadedTime = new Date(); 
			
			for (ExchangeEvent exEvent : zoneTicketsExchangeEvents) {
				 
				 if(isStopped()){
					 break;
				 }
				 i++;
				 Integer eventId = exEvent.getEventId();
//				 eventId = 1000117766; 
				 Event event = eventMap.get(eventId);
				 if(event==null ){
					 System.out.println(i+".TMAT Event Not Exisit :" + eventId);
//					 log.info(i+".TMAT Event Not Exisit :" + eventId);
					 continue;
				 }
				 
				 Date currentTime = new Date();
				 
				System.out.println("CrownJewel Even:" + i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());
//				log.info(i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());
					
				/*File file = new File("C:\\TMATIMAGESFINAL\\" + event.getVenueId() + "_" +event.getZoneCategoryGroupName()+ ".gif");	
					
				if (!file.exists() || exEvent.getZoneTicketBrokerId() == null) {
					 try { // Remove all tickets of event if event don't have map.
						 List<CrownJewelCategoryTicket> miniTickets = DAORegistry.getCrownJewelCategoryTicketDAO().getAllCrownJewelCategoryTicketsByEventId(eventId);

						 try {
							 for (CrownJewelCategoryTicket cat : miniTickets) {
								 cat.setStatus("DELETED");
								 cat.setLastUpdated(currentTime);
								 cat.setReason("Event MAP not Found in TMAT");
							 }
							 
							 if(isUpdateZone && isZoneEvent){
								 ZoneTicketDAORegistry.getZtCrownJewelCatTicketGroupDAO().deleteByCategoryTicketGroups(miniTickets);
							 }
							 
						 } catch (Exception e) {
							 isErrorOccured = true;
							 error = new AutoPricingError();
							 error.setProductId(autopricingProduct.getId());
							 error.setMessage("Event : "+eventId);
							 error.setExample("" + e.fillInStackTrace());
							 error.setProcess("Error while deleting zone listings of Event without Map.");
							 error.setEventId(eventId);
							 error.setTimeStamp(new Date());
							 errorList.add(error);
							 log.error("MINI 2 : Error While Remove Category:"+"Event:" + eventId + "):Msg:" + e.fillInStackTrace());
							 System.err.println("MINI 2 : Error While Remove Category:"+"Event:" + eventId + "):Msg:" + e.fillInStackTrace());
						 }
						 DAORegistry.getCrownJewelCategoryTicketDAO().updateAll(miniTickets);
						 
						 System.out.println("Deleting Event Listings for MAP not Found in TMAT : " + event.getId() + ", Tix: " + miniTickets.size());
						
					} catch (Exception e) {
						isErrorOccured = true;
						error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while Deleting Event Listings for MAP not Found in TMAT."+eventId);
						 error.setExample(""+e.fillInStackTrace());
						 error.setProcess("Deleting Event Listings.");
						 error.setEventId(eventId);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("CrownJewel 3 : Error while Deleting Event Listings for MAP not Found in TMAT."+eventId);
						 System.err.println("CrownJewel 3 : Error while Deleting Event Listings for MAP not Found in TMAT."+eventId);
						 e.printStackTrace();
					}
					
					continue;
				 }*/
				
				if(isUpdateZone && isZoneEvent) {
					if(zoneTicketEventMap == null || zoneTicketEventMap.isEmpty()) {
						zoneTicketEvents = ZoneTicketDAORegistry.getZoneTicketsEventDAO().getAllActiveEvents();
						
						if(zoneTicketEvents != null) {
							for(ZoneTicketsEvent zoneTicketEvent : zoneTicketEvents) {
								zoneTicketEventMap.put(zoneTicketEvent.getId(), zoneTicketEvent);
							}
						}
					}
				}
				
				ZoneTicketsEvent zoneTicketsEvent = null;
				if(isUpdateZone && isZoneEvent) {
					zoneTicketsEvent = zoneTicketEventMap.get(event.getId());
					
					if(zoneTicketsEvent == null) {
						
						try { // Remove all tickets of event if event not found in zone tickets database.
							 List<CrownJewelCategoryTicket> miniTickets = DAORegistry.getCrownJewelCategoryTicketDAO().getAllCrownJewelCategoryTicketsByEventId(eventId);

							 try {
								 for (CrownJewelCategoryTicket cat : miniTickets) {
									 cat.setStatus("DELETED");
									 cat.setLastUpdated(currentTime);
									 cat.setReason("Event not Exist in ZoneTickets");
								 }
								 if(isUpdateZone && isZoneEvent){
									 ZoneTicketDAORegistry.getZtCrownJewelCatTicketGroupDAO().deleteByCategoryTicketGroups(miniTickets);
									 
									 //isErrorOccured = true;
									 error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									 error.setMessage("Event not found in zone tickets database : "+eventId);
									 error.setExample("");
									 error.setProcess("Event Listings are removed due to event not found in zones.");
									 error.setEventId(eventId);
									 error.setTimeStamp(new Date());
									 errorList.add(error);
								 }
								 
							 } catch (Exception e) {
								 isErrorOccured = true;
								 error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Event : "+eventId);
								 error.setExample("" + e.fillInStackTrace());
								 error.setProcess("Error while deleting zone listings of Event not Found in Zone Tickets.");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("MINI 2 : Error While Remove Category:"+"Event:" + eventId + "):Msg:" + e.fillInStackTrace());
								 System.err.println("MINI 2 : Error While Remove Category:"+"Event:" + eventId + "):Msg:" + e.fillInStackTrace());
							 }
							 DAORegistry.getCrownJewelCategoryTicketDAO().updateAll(miniTickets);
							 
							 System.out.println("Deleting Event Listings for Event not Found in Zone Tickets : " + event.getId() + ", Tix: " + miniTickets.size());
							
						} catch (Exception e) {
							isErrorOccured = true;
							error = new AutoPricingError();
							 error.setProductId(autopricingProduct.getId());
							 error.setMessage("Error while Deleting Event Listings for Event not Found in Zone Tickets."+eventId);
							 error.setExample(""+e.fillInStackTrace());
							 error.setProcess("Deleting Event Listings.");
							 error.setEventId(eventId);
							 error.setTimeStamp(new Date());
							 errorList.add(error);
							 log.error("CrownJewel 3 : Error while Deleting Event Listings for Event not Found in Zone Tickets."+eventId);
							 System.err.println("CrownJewel 3 : Error while Deleting Event Listings for Event not Found in Zone Tickets."+eventId);
							 e.printStackTrace();
						}
						//isZoneEvent = false;
						continue;
						
					}
				}
				
				Date date = null;
				if(event.getLocalDate()!=null){
					date = df.parse(df.format(event.getLocalDate()));	
				}
				now = df.parse(df.format(now));
				Integer excludingEventDays = null;
				if(autopricingSettings!=null){
					excludingEventDays = autopricingSettings.getExcludeEventDays();
				}
				if(excludingEventDays==null){
					excludingEventDays = minimamExcludeEventDays ;
				}
				
				/*if(isUpdateZone && isZoneEvent) {
					
					try {
						if(zoneEventIdsWithLockedTickets == null || (new Date().getTime()-lockedEventIdLoadedTime.getTime() > 1 * 60 * 1000)) {
							zoneEventIdsWithLockedTickets = ZoneTicketDAORegistry.getZtCrownJewelCatTicketGroupDAO().getAllActiveLockedTicketEventIds();
							lockedEventIdLoadedTime = new Date();
						}
						if(zoneEventIdsWithLockedTickets.contains(eventId)) {
							ztLockedEventDetails.add(new ZoneTicketsLockedTicketEventDetails(eventId,new Date()));
						}
						
					} catch(Exception e) {
						isErrorOccured = true;
						error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while getting active locked ticket eventIds.");
						 error.setExample(""+e.fillInStackTrace());
						 error.setProcess("Get locked ticket event ids.");
						 error.setEventId(eventId);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("CrownJewel 7 : Error while getting active locked ticket eventIds."+ new Date());
						 System.err.println("CrownJewel 7 : Error while getting active locked ticket eventIds."+ new Date());
						 e.printStackTrace();
					}
					
				}*/
				Set<String> excludeZones = excludeEventZoneMap.get(eventId);
				if(excludeZones == null) {
					excludeZones = excludeVenueCategoryZoneMap.get(event.getVenueCategoryId());
				}
				
				// If event is valid event add , update or delete event tickets based on latest tmat tickets.
				Map<String,CrownJewelCategoryTicket> catTixMap = new HashMap<String, CrownJewelCategoryTicket>();
				Map<String,CrownJewelCategoryTicket> catTixFromDB = new HashMap<String, CrownJewelCategoryTicket>();
				List<CrownJewelCategoryTicket> removeTixList = new ArrayList<CrownJewelCategoryTicket>();
				try {
					int defaultExpectedArrivialDatePriorDays = exEvent.getShippingDays();
					Integer sectionMinEtry = 2;
					/*DefaultAutoPricingProperties defaultAutopricing = defaultAutoPricingsMap.get(event.getParentCategoryId());
					if(defaultAutopricing != null) {
						sectionMinEtry = defaultAutopricing.getSectionCountTicket();
					}*/
					 List<CategoryTicket> catTixList = CategoryGroupManager.computeCategoryTickets(event, exEvent, defaultExpectedArrivialDatePriorDays, sectionMinEtry, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeMarkup,ticketcityMarkup,autopricingProduct,null);
					 for (CategoryTicket catTixObj : catTixList) { // Add generated tickets in map
						CrownJewelCategoryTicket catTix = (CrownJewelCategoryTicket) catTixObj;
						if(isZoneEvent){
							catTix.setZoneTicketsEventId(zoneTicketsEvent.getId());
						}
						String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
						catTixMap.put(key, catTix);
					 }
					 
					 List<CrownJewelCategoryTicket> miniTickets = DAORegistry.getCrownJewelCategoryTicketDAO().getAllCrownJewelCategoryTicketsByEventId(event.getId());
					 for (CrownJewelCategoryTicket dbCatTix : miniTickets) {// Add Existing tickets in map.]
						 if(isZoneEvent){
							 dbCatTix.setZoneTicketsEventId(zoneTicketsEvent.getId());
							 
							 //if(dbCatTix.getZoneTicketsTicketGroupId() != null) {
								 if(excludeZones != null && broker != null && dbCatTix.getTmatZone() != null) {
									 String exZoneKey = broker.getId()+"_"+dbCatTix.getTmatZone().toUpperCase(); 
									 if(excludeZones.contains(exZoneKey)) {
										 dbCatTix.setStatus("DELETED");
										 dbCatTix.setLastUpdated(currentTime);
										 dbCatTix.setReason("Excluded Zone ticket");
										 removeTixList.add(dbCatTix);
										 continue;
									 }
								 }
							// }
						 }
						 String key = dbCatTix.getQuantity()+":"+ dbCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase();
						 catTixFromDB.put(key, dbCatTix);
					 }
					 
				} catch (Exception e) {
					isErrorOccured = true;
					error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Computing Category Tickets."+eventId);
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Category Ticket Computation.");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("CrownJewel 8 :Error while Computing Category Tickets."+eventId);
					 System.err.println("CrownJewel 8 : Error while Computing Category Tickets."+eventId);
					 e.printStackTrace();
					 
					 continue;
				}
				List<CrownJewelCategoryTicket> newTixList = new ArrayList<CrownJewelCategoryTicket>();
				//Map<String,CrownJewelCategoryTicket> exFinalCatTixMap = new HashMap<String, CrownJewelCategoryTicket>();
				 try {
					 String priceHistory="";
					 String ticketIdHistory= "";
					 String baseTicketOneHistory= "";
					 String baseTicketTwoHistory="";
					 String baseTicketThreeHistory="";
					 
					 String ticketHDateStr = dateTimeFormat.format(new Date());
					String priceHDateStr = dateTimeFormat.format(new Date());
					List<String> keys = new ArrayList<String>(catTixFromDB.keySet());
					Integer baseTicket1,baseTicket2,baseTicket3;
					Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
					
					for(String key:keys) {
						CrownJewelCategoryTicket dbTix =catTixFromDB.remove(key);
						CrownJewelCategoryTicket tix = catTixMap.remove(key);
						isPriceChangeFlag = false;

						if(tix != null) { // if generated tickets is already existing..
							if(!dbTix.getActualPrice().equals(tix.getActualPrice())) {
								isPriceChangeFlag = true;
								priceHistory = priceHDateStr+"-"+tix.getActualPrice();
								if(dbTix.getPriceHistory() != null) {
									priceHistory = dbTix.getPriceHistory() +","+priceHistory;
									if(priceHistory.length()>=500) {
										priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
									}
								}
								dbTix.setPriceHistory(priceHistory);
								dbTix.setIsEdited(true);
								
							} else if(!dbTix.getZoneTicketPrice().equals(tix.getZoneTicketPrice()) ||
									//!dbTix.getVividPrice().equals(tix.getVividPrice()) ||
//										 !dbTix.getTickpickPrice().equals(tix.getTickpickPrice()) ||
									//!dbTix.getScoreBigPrice().equals(tix.getScoreBigPrice()) ||
									//!dbTix.getFanxchangePrice().equals(tix.getFanxchangePrice()) ||
									//!dbTix.getTicketcityPrice().equals(tix.getTicketcityPrice()) ||
									!dbTix.getSectionRange().equals(tix.getSectionRange()) ||
									!dbTix.getRowRange().equals(tix.getRowRange()) ||
									!dbTix.getTicketId().equals(tix.getTicketId()) ||
									!dbTix.getTicketDeliveryType().equals(tix.getTicketDeliveryType())) { 
								
								dbTix.setIsEdited(true);
								
							} else {
								baseTicket1 = 0;
								baseTicket2 = 0;
								baseTicket3 = 0;
								
								if(dbTix.getBaseTicketOne() != null) {
									baseTicket1 = dbTix.getBaseTicketOne();
								}
								if(dbTix.getBaseTicketTwo() != null) {
									baseTicket2 = dbTix.getBaseTicketTwo();
								}
								if(dbTix.getBaseTicketThree() != null) {
									baseTicket3 = dbTix.getBaseTicketThree();
								}
								
								if(!tix.getBaseTicketOne().equals(baseTicket1) || !tix.getBaseTicketTwo().equals(baseTicket2) || !tix.getBaseTicketThree().equals(baseTicket3)) {
									dbTix.setIsEdited(true);
									
								}
							}
							
							//add the pricehistory of basetickets prices here for all tickets
							
							tempTicketIdFlag = false;
							if(isPriceChangeFlag || !dbTix.getTicketId().equals(tix.getTicketId())) {
								ticketIdHistory = ticketHDateStr +"/"+ tix.getTicketId() +"/"+ tix.getPurPrice();
								
								if(dbTix.getTicketIdHistory() != null) {
									ticketIdHistory = dbTix.getTicketIdHistory()+","+ ticketIdHistory;
									if(ticketIdHistory.length()>= 500) {
										ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
									}
								}
								dbTix.setTicketIdHistory(ticketIdHistory);
								dbTix.setIsEdited(true);
							} 
							
							// For Base Ticket 1
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketOne() != null && dbTix.getBaseTicketOne() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketOne().equals(tix.getBaseTicketOne())) {
									tempTicketIdFlag = true;
								}
							} else if(tix.getBaseTicketOne() != null && tix.getBaseTicketOne() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketOneHistory = ticketHDateStr +"/"+ tix.getBaseTicketOne() +"/"+ tix.getBaseTicketOnePurPrice();
								
								if(dbTix.getBaseTicketOneHistory() != null) {
									baseTicketOneHistory = dbTix.getBaseTicketOneHistory()+","+ baseTicketOneHistory;
															
									if(baseTicketOneHistory.length()>= 500) {
										baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketOneHistory(baseTicketOneHistory);
								dbTix.setIsEdited(true);
							}
							
							// For Base Ticket 2
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketTwo() != null && dbTix.getBaseTicketTwo() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketTwo().equals(tix.getBaseTicketTwo())){
									tempTicketIdFlag = true;
								}
							} else if(tix.getBaseTicketTwo() != null && tix.getBaseTicketTwo() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketTwoHistory = ticketHDateStr +"/"+ tix.getBaseTicketTwo() +"/"+ tix.getBaseTicketTwoPurPrice();
								if(dbTix.getBaseTicketTwoHistory() != null) {
									baseTicketTwoHistory = dbTix.getBaseTicketTwoHistory()+","+ baseTicketTwoHistory;
															
									if(baseTicketTwoHistory.length()>= 500) {
										baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
								dbTix.setIsEdited(true);
							}
							// For Base Ticket 3
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketThree() != null && dbTix.getBaseTicketThree() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketThree().equals(tix.getBaseTicketThree()))  {
									tempTicketIdFlag = true;
								}
							} else if(tix.getBaseTicketThree() != null && tix.getBaseTicketThree() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketThreeHistory = ticketHDateStr +"/"+ tix.getBaseTicketThree() +"/"+ tix.getBaseTicketThreePurPrice();
								
								if(dbTix.getBaseTicketThreeHistory() != null) {
									baseTicketThreeHistory = dbTix.getBaseTicketThreeHistory()+","+ baseTicketThreeHistory;
															
									if(baseTicketThreeHistory.length()>= 500) {
										baseTicketThreeHistory = baseTicketThreeHistory.substring(baseTicketThreeHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
								dbTix.setIsEdited(true);
							}
							
							if(isUpdateZone && isZoneEvent) {
								if(dbTix.getZoneTicketsTicketGroupId() == null ) { // Add tickets as new tickets if it is not exist in POS (i.e. tn group id is not present)
									dbTix.setIsEdited(true);
								} else if(!dbTix.getZoneTicketPrice().equals(tix.getZoneTicketPrice())) { // Else change it in pos if price or other details has been changed.
									dbTix.setZoneTicketPrice(tix.getZoneTicketPrice());
									dbTix.setIsEdited(true);
								}
							}
							
							if(dbTix.getIsEdited()) {
								dbTix.setBaseTicketOne(tix.getBaseTicketOne());
								dbTix.setBaseTicketTwo(tix.getBaseTicketTwo());
								dbTix.setBaseTicketThree(tix.getBaseTicketThree());
								
								dbTix.setCategoryId(tix.getCategoryId());
								dbTix.setTicketId(tix.getTicketId());
								dbTix.setItemId(tix.getItemId());
								
								dbTix.setRowRange(tix.getRowRange());
								dbTix.setSectionRange(tix.getSectionRange());
								
								dbTix.setActualPrice(tix.getActualPrice());
								//dbTix.setZoneTicketPrice(tix.getZoneTicketPrice());
								dbTix.setVividPrice(tix.getVividPrice());
//									dbTix.setTickpickPrice(tix.getTickpickPrice());
								dbTix.setScoreBigPrice(tix.getScoreBigPrice());
								dbTix.setFanxchangePrice(tix.getFanxchangePrice());
								dbTix.setTicketcityPrice(tix.getTicketcityPrice());
								
								dbTix.setTicketDeliveryType(tix.getTicketDeliveryType());
								dbTix.setLastUpdated(currentTime);
								dbTix.setTmatZone(tix.getTmatZone());
								dbTix.setTnExchangeEventId(tix.getTnExchangeEventId());
								//exFinalCatTixMap.put(key, dbTix);
								
								newTixList.add(dbTix);
							}
								
						} else { // else delete existing ticket as it does not exist in newly generated tickets..
							
							dbTix.setStatus("DELETED");
							dbTix.setLastUpdated(currentTime);
							dbTix.setReason("Ticket not exist in TMAT");
							//exFinalCatTixMap.put(key, dbTix);
							removeTixList.add(dbTix);
						}
					}
				
					for(String key:catTixMap.keySet()) {  // Add remaining newly generated tickets to db.
						CrownJewelCategoryTicket newTix = catTixMap.get(key);
						 
						 if(excludeZones != null && broker != null && newTix.getTmatZone() != null) {
							 String exZoneKey = broker.getId()+"_"+newTix.getTmatZone().toUpperCase(); 
							 if(excludeZones.contains(exZoneKey)) {
								 continue;
							 }
						 }
						priceHistory = priceHDateStr+"-"+newTix.getActualPrice();
						newTix.setPriceHistory(priceHistory);
						
						ticketIdHistory = ticketHDateStr +"/"+ newTix.getTicketId() +"/"+ newTix.getPurPrice();
						newTix.setTicketIdHistory(ticketIdHistory);
						
						if (newTix.getBaseTicketOne() != 0) {
							baseTicketOneHistory = ticketHDateStr +"/"+ newTix.getBaseTicketOne() +"/"+ newTix.getBaseTicketOnePurPrice();
							newTix.setBaseTicketOneHistory(baseTicketOneHistory);
						}
						
						// For Base Ticket 2
						if (newTix.getBaseTicketTwo() != 0) {
							baseTicketTwoHistory = ticketHDateStr +"/"+ newTix.getBaseTicketTwo() +"/"+ newTix.getBaseTicketTwoPurPrice();
							newTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
						}
						// For Base Ticket 3
						if (newTix.getBaseTicketThree() != 0) {
							baseTicketThreeHistory = ticketHDateStr +"/"+ newTix.getBaseTicketThree() +"/"+ newTix.getBaseTicketThreePurPrice();
							newTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
						}
						
						newTix.setLastUpdated(currentTime);
						newTix.setCreatedDate(currentTime);
						
						//exFinalCatTixMap.put(key, newTix);
						newTixList.add(newTix);
					}
					
					if(isUpdateZone && isZoneEvent) {
						for (CrownJewelCategoryTicket zoneTicketProcessorTicket : newTixList) {
							
							try {
								if(zoneTicketProcessorTicket.getZoneTicketsTicketGroupId() == null) {
									Integer zoneTicektGroupId = ZoneTicketDAORegistry.getZtCrownJewelCatTicketGroupDAO().save(zoneTicketProcessorTicket);
									zoneTicketProcessorTicket.setZoneTicketsTicketGroupId(zoneTicektGroupId);
									tInsert++;
								} else {
									ZoneTicketDAORegistry.getZtCrownJewelCatTicketGroupDAO().updateCategoryTicketGroup(zoneTicketProcessorTicket);
									tUpdate++;
								}
							} catch(Exception e) {
								e.printStackTrace();
								isErrorOccured = true;
								error = new AutoPricingError();
								error.setProductId(autopricingProduct.getId());
								error.setMessage("Zone tickets Update zoneTicketGroupId:"+zoneTicketProcessorTicket.getZoneTicketsTicketGroupId()); 
								error.setExample("" + e.fillInStackTrace());
								error.setProcess("Error While zones Update.");
								error.setEventId(eventId);
								error.setTimeStamp(new Date());
								errorList.add(error);
								log.error("MINI 13 : Error While zones Update :"+"Event:" + eventId+ ":" +"Id:(" + zoneTicketProcessorTicket.getQuantity() +":" + zoneTicketProcessorTicket.getSection() + "):Msg:" + e.fillInStackTrace());
								System.out.println("MINI 13 : Error While zones Update :"+"Event:" + eventId+ ":" +"Id:(" + zoneTicketProcessorTicket.getQuantity() +":" + zoneTicketProcessorTicket.getSection() + "):Msg:" + e.fillInStackTrace());
							}
						}
						
						try {
							ZoneTicketDAORegistry.getZtCrownJewelCatTicketGroupDAO().deleteByCategoryTicketGroups(removeTixList);
							
						} catch(Exception e) {
							e.printStackTrace();
							isErrorOccured = true;
							error = new AutoPricingError();
							 error.setProductId(autopricingProduct.getId());
							 error.setMessage("While deleting zoneticketgroups event :"+eventId);
							error.setExample("" + e.fillInStackTrace());
							error.setProcess("Error While zones delete.");
							error.setEventId(eventId);
							error.setTimeStamp(new Date());
							errorList.add(error);
							log.error("MINI 13 : Error While zones Update :"+"Event:" + eventId + " :Msg:" + e.fillInStackTrace());
							System.out.println("MINI 13 : Error While zones Update :"+"Event:" + eventId + " :Msg:" + e.fillInStackTrace());
						}
					}
					 
					 DAORegistry.getCrownJewelCategoryTicketDAO().saveOrUpdateAll(newTixList);
					 DAORegistry.getCrownJewelCategoryTicketDAO().updateAll(removeTixList);
				
					 System.out.println("CrownJewel  : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
					 
					 
				 } catch (Exception e) {
					 isErrorOccured = true;
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Updating TMAT Larry Ticket."+eventId);
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Larry Ticket Updation..");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("CrownJewel 15 : Error while Updating TMAT Larry Ticket."+eventId);
					 System.out.println("CrownJewel 15 : Error while Updating TMAT Larry Ticket."+eventId);
					 e.printStackTrace();
				}
			}
			DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
			System.out.println("CrownJewel Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
			 
			running = false; 
		 }catch(Exception e){
			 running = false;
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Loading Common Properties.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Loading common Properties..");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("CrownJewel 16 :Error while Loading Common Properties.");
			 System.out.println("CrownJewel 16 : Error while Loading Common Properties.");
			 e.printStackTrace();
		 }
		 
		 String subject,fileName;
		 Map<String, Object> map;
			
		 DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
		 log.info("CT error size......"+errorList.size());
		 
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {
					
					subject = "CrownJewel Scheduler job failed :";
					fileName = "templates/autopricing-job-failure-message.txt";
					
					//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
					
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
				} catch (Exception e) {
					log.error("CrownJewel 17 : Error while Inserting Error Listings in TMAT.");
					 System.out.println("CrownJewel 17 :Error while Inserting Error Listings in TMAT.");
					e.printStackTrace();
				}
			}
	 }
	 
	 	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
//			if(!running){
//				running=true;
				System.out.println("CrownJeweltsProcessor Job Called.." + new Date() + ": " + running);
				log.info("CrownJewel Job Called.." + new Date() + ": " + running);
				try{
					
					processCrownJewelsProcessorTickets();
					System.out.println("CrownJewel Scheduler Job finished @ " + new Date());
				}catch(Exception e){
					e.printStackTrace();
				}
//				running = false;
//			}
	}


	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		CrownJewelScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		CrownJewelScheduler.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("CrownJewel").getLastRunTime();
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		CrownJewelScheduler.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}


	public static void setNextRunTime(Date nextRunTime) {
		CrownJewelScheduler.nextRunTime = nextRunTime;
	}

}