package com.rtw.autopricing.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;


public class SeatGeekWSUtil {
	public static String seatGeekBaseURL;// = "https://sellerdirect-api.seatgeek.com/rightthisway/";
	public static String seatGeekApplicationKey;// = "application_key=<application_key>";
	public static String seatGeekToken;//="m61Tv5IebugzqukzhkEu";
	public static Integer seatGeekApiAccessEnabled;// = 1;
	//public static Integer seatGeekCustomerId;// = 1;
	
	
	public static String getSeatGeekBaseURL() {
		return seatGeekBaseURL;
	}

	public final void setSeatGeekBaseURL(String seatGeekBaseURL) {
		SeatGeekWSUtil.seatGeekBaseURL = seatGeekBaseURL;
	}

	public static String getSeatGeekApplicationKey() {
		return seatGeekApplicationKey;
	}

	public final void setSeatGeekApplicationKey(String seatGeekApplicationKey) {
		SeatGeekWSUtil.seatGeekApplicationKey = seatGeekApplicationKey;
	}

	public static String getSeatGeekToken() {
		return seatGeekToken;
	}

	public final void setSeatGeekToken(String seatGeekToken) {
		SeatGeekWSUtil.seatGeekToken = seatGeekToken;
	}

//	public static Integer getSeatGeekCustomerId() {
//		return seatGeekCustomerId;
//	}

//	public final void setSeatGeekCustomerId(Integer seatGeekCustomerId) {
//		SeatGeekWSUtil.seatGeekCustomerId = seatGeekCustomerId;
//	}
	
	public static Integer getSeatGeekApiAccessEnabled() {
		return seatGeekApiAccessEnabled;
	}

	public final void setSeatGeekApiAccessEnabled(Integer seatGeekApiAccessEnabled) {
		SeatGeekWSUtil.seatGeekApiAccessEnabled = seatGeekApiAccessEnabled;
	}

	public static boolean isSeatGeekAPIEnabled() throws Exception {
		if(seatGeekApiAccessEnabled != null && seatGeekApiAccessEnabled.equals(1)) {
			return true;
		}
		return false;
	}
	
	
	public static String deleteTicket(Integer ticketId) throws Exception {
		try{
			if(!isSeatGeekAPIEnabled()) {
				return "SeatGeek API access is in disabled mode.";
			}
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			//System.out.println(url);
			String url=seatGeekBaseURL+"listings?token="+seatGeekToken+"&listing_id="+ticketId;
			HttpDelete delete = new HttpDelete(url);
			delete.addHeader("Content-Type", "application/json");
			System.out.println(url);
			HttpResponse response = httpClient.execute(delete);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			String message = null;
			//{"meta":{"status":200}}
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			} else if(jsonObject.has("meta")) {
				JSONObject metaJSONObj = jsonObject.getJSONObject("meta");
				message = metaJSONObj.getString("status");
			}
			System.out.println("message : "+message);
			return 	message;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
		
}
