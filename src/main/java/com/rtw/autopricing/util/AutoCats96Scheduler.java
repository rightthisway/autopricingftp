package com.rtw.autopricing.util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEvent;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZones;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;


public class AutoCats96Scheduler  extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(AutoCats96Scheduler.class);
	
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 
	 
	
	 public static void processAutoCats96Tickets() throws Exception{
			Calendar cal =  Calendar.getInstance();
			Date now = cal.getTime();
			cal.add(Calendar.MINUTE,15);
			setNextRunTime(cal.getTime());
			if(isStopped() || isRunning()){
				return ;
			}
			setRunning(true);
			
			
			Date lastRunTime = getLastUpdateTime();
			Long minute = 0l;
			if(lastRunTime!=null){
				minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
			}else{
				minute = 2880l; // 2 Days in minute
				Calendar cal1 = Calendar.getInstance();
				cal1.add(Calendar.MINUTE, -2880);
				lastRunTime = cal1.getTime();
			}
			AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("AutoCats96");
			if(autopricingProduct==null){
				autopricingProduct = new AutopricingProduct();
				autopricingProduct.setName("AutoCats96");
				autopricingProduct.setStatus("ACTIVE");
				autopricingProduct.setStopped(false);
			}
			if(autopricingProduct.getStopped()){
				setStopped(true);
				setRunning(false);
				return;
			}
			
			
			setLastUpdateTime(now);
			autopricingProduct.setLastRunTime(now);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			String toAddress = resourceBundle.getString("emailNotificationTo");
			String ccAddress= resourceBundle.getString("emailNotificationCCTo");
			
			AutoPricingError error= null;
			List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
			boolean isErrorOccured = false;
//			Date jobStartTime = new Date();
			int tInsert=0,tUpdate=0,tremovet=0;
			int eInsert=0,eUpdate=0,eremovet=0;
			
			 try{
				 Map<Integer, Integer> brokerAddCountMap = new HashMap<Integer, Integer>();
				 Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
				 Map<Integer, Integer> brokerUpdateCountMap = new HashMap<Integer, Integer>();
				 
				 AutopricingProduct presaleAutopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("PresaleAutoCat");
				 
				 Double tnExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork").getAdditionalMarkup();
				 Double vividExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats").getAdditionalMarkup();
				 Double scorebigMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Scorebig").getAdditionalMarkup();
				 Double fanxchangeMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("FanXchange").getAdditionalMarkup();
				 Double ticketcityMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity").getAdditionalMarkup();
				 Double tickPickExAddMarkup = 0.0;
				 Double scoreBigExAddMarkup = 0.0;
				 
				 scoreBigExAddMarkup = scorebigMarkup; 
				 
				 Boolean isTnEvent = false;
				 
				 Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
				 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
				 List<PosEvent> posEvents = new ArrayList<PosEvent>();
				 Map<Integer, List<PosEvent>> posEventsAndBrokerMap = new HashMap<Integer, List<PosEvent>> ();
				 
				 Map<Integer, Map<Integer, PosEvent>> brokerPosEventMap = new HashMap<Integer, Map<Integer,PosEvent>>();
				 
				 Collection<Event> eventList = null;
				 Collection<ExchangeEvent> miniExchangeEvents = new ArrayList<ExchangeEvent>();
				// Map<Integer, Boolean> tnCrawlOnlyEventMap = new HashMap<Integer, Boolean>();				 
				 
				 try {
					 Map<Integer, ExchangeEvent> presaleExEventMap = new HashMap<Integer, ExchangeEvent>();
					
					//Tamil : To consider all presale events every 1 hour once
					 boolean isAllPresaleEvents = false;
					 Calendar calendar = Calendar.getInstance();
					 if(calendar.get(Calendar.MINUTE) < 15) {
						 isAllPresaleEvents = true;
						 System.out.println("Autocats : all presale events flag......"+new Date());
					 }
					 
					 //Get all events that is updated in tmat since last run time.
					 //TN not allow section or row with ranges so we are processing only lastrow products. lastrow not applicable for presale events
					/* Collection<PresaleAutoCatExchangeEvent> presaleExchangeEvents = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsEligibleForUpdate(minute,presaleAutopricingProduct,isAllPresaleEvents);
					 for (PresaleAutoCatExchangeEvent presaleExEvent : presaleExchangeEvents) {
						 presaleExEventMap.put(presaleExEvent.getEventId(), presaleExEvent);
						 miniExchangeEvents.add(presaleExEvent);
					 }*/
					 
					 //Property eventDeleteFlagProperty = DAORegistry.getPropertyDAO().getPropertyByName("autocats96.event.delete.flag");
					 //Boolean eventDeleteFlagOn = Boolean.valueOf(eventDeleteFlagProperty.getValue());
					 
					 Collection<AutoCats96ExchangeEvent> autocats96ExchangeEvent = null;
					/// if(eventDeleteFlagOn) {
						 //Get all events that is updated in tmat since last run time. 
						// autocats96ExchangeEvent = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsEligibleForUpdateTemp(minute,autopricingProduct);						 
					 //} else {
						 //Get all events that is updated in tmat since last run time. 
						 autocats96ExchangeEvent = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsEligibleForUpdate(minute,autopricingProduct);
					 //}
					 for (AutoCats96ExchangeEvent autocats96ExEvent : autocats96ExchangeEvent) {
						 if(presaleExEventMap.remove(autocats96ExEvent.getEventId()) == null) {
							 miniExchangeEvents.add(autocats96ExEvent);
						 }
					 }

					 System.out.println("AUTOCATS Event Size : "+miniExchangeEvents.size());
					 
					 eventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentId();
					 for(Event event:eventList) {
						eventMap.put(event.getId(), event);
						eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
					 }
					 System.out.println("AUTOCATS96 : TMAT event size : "+eventList.size());
					 
					/* List<Integer> eventIdswithTnCrawlsOnly = DAORegistry.getQueryManagerDAO().getAllEventsIdswithTnCrawlsOnly();
					 for (Integer eventId : eventIdswithTnCrawlsOnly) {
						 tnCrawlOnlyEventMap.put(eventId, Boolean.TRUE);
					 }
					 System.out.println("AUTOCATS96 : TN Crawl only event size : "+eventIdswithTnCrawlsOnly.size());*/
					 
				 } catch (Exception e) {
					 isErrorOccured = true;
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Loading Events.");
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Event Loading.");
					 error.setEventId(0);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("AUTOCATS96 1 : Error while Loading Events.");
					 System.err.println("AUTOCATS96 1 : Error while Loading Events.");
					 e.printStackTrace();
				 }
				 
				 Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
				 Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
				 Map<Integer,Set<String>> presaleExcludeEventZoneMap = new HashMap<Integer,Set<String>>();
				 Map<Integer,Set<String>> presaleExcludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
				 
				 try {
					 List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductId(autopricingProduct.getId());
					 for (ExcludeEventZones excludeEventZone : excludeEventZones) {
						 Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
						 if(eventZones == null) {
							 eventZones = new HashSet<String>();
						 }
						 eventZones.add(excludeEventZone.getBrokerId()+"_"+excludeEventZone.getZone().toUpperCase());
						 excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
					}
					 List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductId(autopricingProduct.getId());
					 for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
						 Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
						 if(venueCategoryZones == null) {
							 venueCategoryZones = new HashSet<String>();
						 }
						 venueCategoryZones.add(excludeVenueCategoryZone.getBrokerId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
						 excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
					 }
					 excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductId(presaleAutopricingProduct.getId());
					 for (ExcludeEventZones excludeEventZone : excludeEventZones) {
						 Set<String> eventZones = presaleExcludeEventZoneMap.get(excludeEventZone.getEventId());
						 if(eventZones == null) {
							 eventZones = new HashSet<String>();
						 }
						 eventZones.add(excludeEventZone.getBrokerId()+"_"+excludeEventZone.getZone().toUpperCase());
						 presaleExcludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
					 }
					 excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductId(presaleAutopricingProduct.getId());
					 for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
						 Set<String> venueCategoryZones = presaleExcludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
						 if(venueCategoryZones == null) {
							 venueCategoryZones = new HashSet<String>();
						 }
						 venueCategoryZones.add(excludeVenueCategoryZone.getBrokerId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
						 presaleExcludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
					 }
				 } catch (Exception e) {
					 isErrorOccured = true;
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Loading Exclude Zones.");
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Exclude Zones Loading.");
					 error.setEventId(0);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("MINI 1 : Error while Loading Exclude Zones..");
					 System.err.println("MINI 1 : Error while Loading Exclude Zones..");
					 e.printStackTrace();
				 }
				
				 int eventSize = miniExchangeEvents.size();
				 Integer i=0;
				 Map<Integer,DefaultAutoPricingProperties> defaultAutoPricingsMap = new HashMap<Integer, DefaultAutoPricingProperties>();
				 List<DefaultAutoPricingProperties> defaultAutoPricingsFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getDefaultAutoPricingPropertiesByProductId(autopricingProduct.getId());
				 for (DefaultAutoPricingProperties defaultAutopriicngSettings : defaultAutoPricingsFromDB) {
					 defaultAutoPricingsMap.put(defaultAutopriicngSettings.getParentCategoryId(),defaultAutopriicngSettings);
				 }
				 Integer minimamExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
				 
				 //Get presale auto pricing products default auto pricing settings
				 Map<Integer,DefaultAutoPricingProperties> presaleDefaultAutoPricingsMap = new HashMap<Integer, DefaultAutoPricingProperties>();
				 defaultAutoPricingsFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getDefaultAutoPricingPropertiesByProductId(presaleAutopricingProduct.getId());
				 for (DefaultAutoPricingProperties defaultAutopriicngSettings : defaultAutoPricingsFromDB) {
					 presaleDefaultAutoPricingsMap.put(defaultAutopriicngSettings.getParentCategoryId(),defaultAutopriicngSettings);
				 }
				 //Integer presaleMinimamExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(presaleAutopricingProduct.getId());
				 
				 Map<Integer, Broker> brokerMap = AutoCats96Utils.getAllAutoPricingBrokersForAutocats96();
				 
				 
				 for (Integer brokerId : brokerMap.keySet()) {
					 
					 Map<Integer,PosEvent> posEventMap = brokerPosEventMap.get(brokerId);
					 
					 if(null == posEventMap || posEventMap.isEmpty())
					 {
						 posEvents = posEventsAndBrokerMap.get(brokerId);
						if(posEvents == null){
							posEvents = BrokerUtils.getPOSEventByBrokerId(brokerId);
							posEventsAndBrokerMap.put(brokerId, posEvents);
						}
						
						posEventMap = new HashMap<Integer, PosEvent>();
						if(posEvents!=null){
							 for(PosEvent posEvent:posEvents){
								 posEventMap.put(posEvent.getExchangeEventId(), posEvent);
							 }
						 }
					 }
					 brokerPosEventMap.put(brokerId, posEventMap);
				 }
				 
				 /* If you want to delete existing listings and to recreate it. Begins- Ulaganathan  */
				 Property eventDeleteFlagProperty = DAORegistry.getPropertyDAO().getPropertyByName("autocats96.event.delete.flag");
				 Boolean eventDeleteFlagOn = Boolean.valueOf(eventDeleteFlagProperty.getValue());
				 /* If you want to delete existing listings and to recreate it. Ends- Ulaganathan  */
				 
				 
				 
				 for (ExchangeEvent exEvent : miniExchangeEvents) {
					 
					 System.out.print(" : "+exEvent.getEventId()+" :Start : "+new Date());
					 boolean isZoneEvent =false;
					 List<Integer> tnBrokerIds = new ArrayList<Integer>();
					 List<Integer> activeBrokerIds = new ArrayList<Integer>();
					 List<Integer> tobeDeletedBrokerIds = new ArrayList<Integer>();
					 
					 List<String> activeProducts = new ArrayList<String>();
					 List<String> tobeDeletedProducts = new ArrayList<String>();
					 int brokerMaxTixCount = 32,eventMaxTixCount=96;
					 if(isStopped()){
						 break;
					 }
					 isTnEvent = false;
					 
					 i++;
					 Integer eventId = exEvent.getEventId();
					 Event event = eventMap.get(eventId);
					 if(event==null ){
						 System.out.println(i+".TMAT Event Not Exisit :" + eventId);
						 continue;
					 }
					 eInsert=0;eUpdate=0;eremovet=0;
					 Date start = new Date();
					 long prePros=0,tixComp=0,postPros=0,tixUpdate=0,dbUpdate=0;
					 
					 /* If you want to delete existing listings and to recreate it. Begins -By Ulaganathan  */
					 if(eventDeleteFlagOn){
						 
						 for (Integer brokerId : brokerMap.keySet()) {
							 Date currentTime = new Date();
							 Broker broker = null;
							 try{
								 broker = BrokerUtils.getBrokerById(brokerId); 
							 }catch(Exception e){
								 e.printStackTrace();
								 System.out.println("EXCEPTION ON BROKER ID=====>"+brokerId);
							 }
							 
							 List<AutoCats96CategoryTicket> miniTickets = DAORegistry.getAutoCats96CategoryTicketDAO().getAllTNAutoCats96CategoryTicketsByEventIdByBrokerID(eventId,brokerId);
								
							 if(null == miniTickets || miniTickets.isEmpty()){
								 continue;
							 }
							 System.out.println("FORCE EVENT DELETE: Broker Name: "+broker.getName()+" " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
							 
							 for(AutoCats96CategoryTicket cat:miniTickets){
								 	cat.setReason("Forced to Delete Existing Tickets");
									if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) { 
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										continue;
									}
									try {
										if(Counter.isMinuteDeleteAllow(broker)) {
											InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
											//broker.addMinuteUpdateCounter();
											cat.setStatus("DELETED");
											cat.setLastUpdated(currentTime);
											cat.setReason("Forced to Delete Existing Tickets and Deleted from TN");
											eremovet++;
										}
									} catch (Exception e) {
										cat.setReason("Forced to Delete Existing Tickets and Exception from TN");
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(eventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("AUTOCATS96 2 FORCE EVENT DELETE: Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
										System.err.println("AUTOCATS96 2 FORCE EVENT DELETE : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
									}
								}
								DAORegistry.getAutoCats96CategoryTicketDAO().updateAll(miniTickets);
						 }
					 }
					 /* If you want to delete existing listings and to recreate it. Ends - By Ulaganathan  */
					 
					 tnBrokerIds = AutoCats96Utils.getBrokerListbyParentCategory(event.getParentCategoryId());
					 
					 //RTW-2
					 /*if(null != exEvent.getRotEnabled() && exEvent.getRotEnabled()){
						 activeBrokerIds.add(10);
					 }else{
						 tobeDeletedBrokerIds.add(10);
					 }*/
					 
					 //Tamil : 08/27/2018 changed listings from Tixcity-2 to RTW
					 /*if(null != exEvent.getTixcityEnabled() && exEvent.getTixcityEnabled()){
						 activeBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID);
					 }else{
						 tobeDeletedBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID);
					 }*/
					 
					 //RTW
					 if(null != exEvent.getRtwEnabled() && exEvent.getRtwEnabled()){
						 activeBrokerIds.add(5);
					 }else{
						 tobeDeletedBrokerIds.add(5);
					 }
					 
					 //Process Lastrow lisitngs only. TN not allowing sections or row with ranges
					 /*if(null != exEvent.getMinicatsEnabled() && exEvent.getMinicatsEnabled()){
						 activeProducts.add("MINICATS");
					 }else{
						 tobeDeletedProducts.add("MINICATS");
					 }*/
					 
					 if(exEvent.getIsPresale()) {
						/* if(null != exEvent.getPresaleAutocatsEnabled() && exEvent.getPresaleAutocatsEnabled()){
							 activeProducts.add("AUTOCAT");
						 }else {
							 tobeDeletedProducts.add("AUTOCAT");
						 }*/
					 } else {
						 if(null != exEvent.getLastrowMinicatsEnabled() && exEvent.getLastrowMinicatsEnabled()){
							 activeProducts.add("LASTROW MINICATS");
						 }else{
							 tobeDeletedProducts.add("LASTROW MINICATS");
						 }
						 
						/* if(null != exEvent.getVipMinicatsEnabled() && exEvent.getVipMinicatsEnabled()){
							 activeProducts.add("VIPMINICATS");
						 }else{
							 tobeDeletedProducts.add("VIPMINICATS");
						 }
						 */
						//Tamil : stop VIP Lastrow for TN as per Amit on 09/19/2017
						/* if(null != exEvent.getVipLastrowMinicatsEnabled() && exEvent.getVipLastrowMinicatsEnabled()){
							 activeProducts.add("VIPLR");
						 }else{
							 tobeDeletedProducts.add("VIPLR");
						 }*/
					 }
					 
					 System.out.println("AUTOCATS96 Even:" + i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());
					 
					 boolean noExistingTickets = true;
					 
					 List<AutoCats96CategoryTicket> autoCats96Tickets = DAORegistry.getAutoCats96CategoryTicketDAO().getAllTNAutoCats96CategoryTicketsByEventId(eventId);
					 
						System.out.print(" : "+event.getId()+" :dbcats 1 : "+new Date()); 
						 //If There is an existing tickets for this event then only we will try to delete based on some criteria. - Done By Ulaganathan
						 
						 Map<Integer, List<AutoCats96CategoryTicket>> brokerTicketsMap = new HashMap<Integer, List<AutoCats96CategoryTicket>>();
						 Map<String, List<AutoCats96CategoryTicket>> brokerProductTicketsMap = new HashMap<String, List<AutoCats96CategoryTicket>>();
						 
						 for (AutoCats96CategoryTicket catTicket : autoCats96Tickets) {
							 
							 List<AutoCats96CategoryTicket> tempTickets = brokerTicketsMap.get(catTicket.getTnBrokerId());
							 List<AutoCats96CategoryTicket> tempTickets1 = brokerProductTicketsMap.get(catTicket.getTnBrokerId()+":"+catTicket.getInternalNotes());
							 
							 if(null != tempTickets && !tempTickets.isEmpty()){
								 brokerTicketsMap.get(catTicket.getTnBrokerId()).add(catTicket);
							 }else{
								 tempTickets= new ArrayList<AutoCats96CategoryTicket>();
								 tempTickets.add(catTicket);
								 brokerTicketsMap.put(catTicket.getTnBrokerId(),tempTickets);
							 }
							 
							 if(null != tempTickets1 && !tempTickets1.isEmpty()){
								 brokerProductTicketsMap.get(catTicket.getTnBrokerId()+":"+catTicket.getInternalNotes()).add(catTicket);
							 }else{
								 tempTickets1= new ArrayList<AutoCats96CategoryTicket>();
								 tempTickets1.add(catTicket);
								 brokerProductTicketsMap.put(catTicket.getTnBrokerId()+":"+catTicket.getInternalNotes(),tempTickets1);
							 }
						 }
						 
						 Map<Integer, Broker> enabledBrokersMap = new HashMap<Integer, Broker>();
						 Map<Integer, AutopricingSettings> enabledBrokersAutoSettingsMap = new HashMap<Integer, AutopricingSettings>();
						 for (Integer brokerId : tnBrokerIds) {
							 Broker broker = null;
							 AutopricingSettings autopricingSettings = null;
							 AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
							 isTnEvent = true;
							 if(isTnEvent){
								 broker = BrokerUtils.getBrokerById(brokerId);
								 if(broker!=null){
									 autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
								 }
							 }
							 if(autopricingSettings != null && autopricingSettings.getIsEnabled()) {
								 enabledBrokersMap.put(brokerId, broker);
							 }/*else{
								 enabledBrokersMap.put(brokerId, broker);
							 }*/
							 enabledBrokersAutoSettingsMap.put(brokerId, autopricingSettings);
						}
						 
						 System.out.print(" : "+event.getId()+" :setings 1 : "+new Date());
						 
						 for (Integer deleteBrokerId : tobeDeletedBrokerIds) {
							 Date currentTime = new Date();
							 Broker broker =enabledBrokersMap.get(deleteBrokerId);
							 
							 if(broker != null && null != broker.getId()) {
								 
								 List<AutoCats96CategoryTicket> miniTickets = brokerTicketsMap.remove(deleteBrokerId);
								
								 if(null == miniTickets || miniTickets.isEmpty()){
									 continue;
								 }
								 System.out.println("DELETING: BROKER IS DISABLED: Broker Name: "+broker.getName()+" " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
								 for(AutoCats96CategoryTicket cat:miniTickets){
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										cat.setReason("Broker is Disabled for this Event");
										
										if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) { 
											continue;
										}
										try {
											if(Counter.isMinuteDeleteAllow(broker)) {
												InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
												broker.addMinuteUpdateCounter();
												eremovet++;
												cat.setReason("Broker is Disabled for this Event and Deleted from TN");
											}
										} catch (Exception e) {
												cat.setReason("Broker is Disabled for this Event and Exception from TN");
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
												error.setProcess("POS Remove Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("AUTOCATS96 2 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
												System.err.println("AUTOCATS96 2 : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
										}
									}
									DAORegistry.getAutoCats96CategoryTicketDAO().updateAll(miniTickets);
							 }
						}
						 System.out.print(" : "+event.getId()+" :delete brk ids 1 : "+new Date());
						 
						 for (Integer activeBrokerId : activeBrokerIds) {
							
							 isTnEvent = true;
							 Date currentTime = new Date();
							 Broker broker = enabledBrokersMap.get(activeBrokerId);
							 boolean isUpdateTN = true;
							 
							 if(broker == null ) {
								 isUpdateTN = false;
							 }
							 
							 if(exEvent.getZone() == null || exEvent.getZone()) {
								 
								 isZoneEvent = true;
								 
								 try { // Remove all tickets of event if event is not zone , event is not active or there is restriction to update POS.
									  if(broker != null && isUpdateTN ) {
										 List<AutoCats96CategoryTicket> miniTickets = brokerTicketsMap.remove(activeBrokerId);
										 if(null == miniTickets || miniTickets.isEmpty()){
											continue;
										 }
										 System.out.println("DELETING: EX EVENT IS ZONE EVENT " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
										 for(AutoCats96CategoryTicket cat:miniTickets){
											cat.setStatus("DELETED");
											cat.setLastUpdated(currentTime);
											cat.setReason("Event is a Zone Event");
											if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) { 
												continue;
											}
											try {
												if(Counter.isMinuteDeleteAllow(broker)) {
													InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
													broker.addMinuteUpdateCounter();
													eremovet++;
													cat.setReason("Event is a Zone Event and Deleted from TN");
												}
											} catch (Exception e) {
													cat.setReason("Event is a Zone Event and Exception in TN");
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
													error.setProcess("POS Remove Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("AUTOCATS96 2 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
													System.err.println("AUTOCATS96 2 : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											}
										}
										 DAORegistry.getAutoCats96CategoryTicketDAO().updateAll(miniTickets);
									 }
								} catch (Exception e) {
									isErrorOccured = true;
									error = new AutoPricingError();
									error.setProductId(autopricingProduct.getId());
									error.setMessage("Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
									error.setExample(""+e.fillInStackTrace());
									error.setProcess("Deleting Event Listings.");
									error.setEventId(eventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("AUTOCATS96 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
									System.err.println("AUTOCATS96 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
									e.printStackTrace();
								}
								isTnEvent = false;
							 }
							 
							 Map<Integer,PosEvent> posEventMap = null;
							 
							 if(broker != null && isUpdateTN && isTnEvent) {
								 posEventMap = brokerPosEventMap.get(activeBrokerId); 
							 }
							 
							 PosEvent posEvent = null;
							 
							 if(null ==posEventMap || posEventMap.isEmpty()) {
								isTnEvent = false;
							 }else{
								 posEvent = posEventMap.get(event.getAdmitoneId());
							 }
							 
							 if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || 
									 posEvent.getExchangeEventId() == 0){
								 
								 // Remove all tickets of the event if event is not in POS or not an exchange event in POS.
								if(broker != null && isUpdateTN && isTnEvent) {
									try {
										List<AutoCats96CategoryTicket> miniTickets = brokerTicketsMap.remove(activeBrokerId);
										if(null == miniTickets || miniTickets.isEmpty()){
											continue;
										}
										
										System.out.println("DELETING: NO POS EVENT " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());

										for(AutoCats96CategoryTicket cat:miniTickets){
											cat.setStatus("DELETED");
											cat.setLastUpdated(currentTime);
											cat.setReason("Event not Exist in TN");
											if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
												continue;
											}
											try {
												if(Counter.isMinuteDeleteAllow(broker)) {
													InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
													broker.addMinuteUpdateCounter();
													eremovet++;
													cat.setReason("Event not Exist in TN and Deleted from TN");
												}
											} catch (Exception e) {
												cat.setReason("Event not Exist in TN and Exception in TN");
												e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
												error.setProcess("POS Remove Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("AUTOCATS96 4 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
												System.err.println("AUTOCATS96 4 : 104.CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											}
										}
										DAORegistry.getAutoCats96CategoryTicketDAO().updateAll(miniTickets);

									} catch (Exception e) {
										isErrorOccured = true;
										error = new AutoPricingError();
										 error.setProductId(autopricingProduct.getId());
										 error.setMessage("Error while Deleting Event Listings for Event not Found in POS."+eventId);
										 error.setExample(""+e.fillInStackTrace());
										 error.setProcess("Deleting Event Listings.");
										 error.setEventId(eventId);
										 error.setTimeStamp(new Date());
										 errorList.add(error);
										 log.error("AUTOCATS96 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
										 System.err.println("AUTOCATS96 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
										 e.printStackTrace();
									}
									}
									isTnEvent = false;
								}
								
								
								Date date = null;
								if(event.getLocalDate()!=null){
									date = df.parse(df.format(event.getLocalDate()));	
								}
								now = df.parse(df.format(now));
								Integer excludingEventDays = null;
								AutopricingSettings autopricingSettings =enabledBrokersAutoSettingsMap.get(activeBrokerId);
								
								if(autopricingSettings!=null){
									excludingEventDays = autopricingSettings.getExcludeEventDays();
								}
								if(excludingEventDays==null){
									excludingEventDays = minimamExcludeEventDays ;
								}
								
								if(date!=null && ((date.getTime()-now.getTime()) <= excludingEventDays* 24 * 60 * 60 * 1000)) {//2 days events
									 // Remove all tickets of event if event is within 2 days..
									
									tobeDeletedBrokerIds.add(activeBrokerId);
									
									try {
										
										if(broker != null && isUpdateTN && isTnEvent) {
											List<AutoCats96CategoryTicket> miniTickets = brokerTicketsMap.remove(activeBrokerId);
											if(null == miniTickets || miniTickets.isEmpty()){
												 continue;
											 }
											System.out.println("DELETING: EVENT WITHIN TWO DAYS " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
												
											for(AutoCats96CategoryTicket cat:miniTickets){
												cat.setStatus("DELETED");
												cat.setLastUpdated(currentTime);
												cat.setReason("Event within exclude eventdays");
												
												if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {
													continue;
												}
												try {
													if(Counter.isMinuteDeleteAllow(broker)) {
														System.out.println("AUTOCATS96 delete begins..3..."+new Date()+"...count.."+eremovet);
														InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
														broker.addMinuteUpdateCounter();
														eremovet++;
														cat.setReason("Event within exclude eventdays and Deleted from TN");
													}
												} catch (Exception e) {
													cat.setReason("Event within exclude eventdays and Exception in TN");
													e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
													error.setProcess("POS Remove Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("AUTOCATS96 6 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
													System.err.println("AUTOCATS96 6 : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
												}
											}
											 DAORegistry.getAutoCats96CategoryTicketDAO().updateAll(miniTickets);
										 }
									} catch (Exception e) {
										 isErrorOccured = true;
										 error = new AutoPricingError();
										 error.setProductId(autopricingProduct.getId());
										 error.setMessage("Error while Deleting Event Listings for Event within Few Days."+eventId);
										 error.setExample(""+e.fillInStackTrace());
										 error.setProcess("Deleting Event Listings.");
										 error.setEventId(eventId);
										 error.setTimeStamp(new Date());
										 errorList.add(error);
										 log.error("AUTOCATS96 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
										 System.err.println("AUTOCATS96 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
										 e.printStackTrace();
									}
									continue;
								}
								
							for (String productName : tobeDeletedProducts) {
								
								try {
									List<AutoCats96CategoryTicket> tempTickets = brokerTicketsMap.remove(activeBrokerId);
									if(null == tempTickets || tempTickets.isEmpty()){
										 continue;
									}
									List<AutoCats96CategoryTicket> miniTickets = brokerProductTicketsMap.remove(activeBrokerId+":"+productName);
									
									if(null == miniTickets || miniTickets.isEmpty()){
										 continue;
									}
									System.out.println("DELETING:DISABLED PRODUCT TIXS " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
									boolean tnFlag = false;
									if(broker != null && isUpdateTN && isTnEvent) {
										tnFlag = true;
									}
									for(AutoCats96CategoryTicket cat:miniTickets){
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										cat.setReason("Product is Disabled for this Event");
										if(tnFlag){
											
											if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {
												continue;
											}
											try {
												if(Counter.isMinuteDeleteAllow(broker)) {
													System.out.println("AUTOCATS96 delete begins..3..."+new Date()+"...count.."+eremovet);
													InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
													broker.addMinuteUpdateCounter();
													eremovet++;
													cat.setReason("Product is Disabled for this Event and Deleted from TN");
												}
											} catch (Exception e) {
												cat.setReason("Product is Disabled for this Event and Exception from TN");
												e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
												error.setProcess("POS Remove Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("AUTOCATS96 6 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
												System.err.println("AUTOCATS96 6 : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											}
										}
									}
									DAORegistry.getAutoCats96CategoryTicketDAO().updateAll(miniTickets);
									
								} catch (Exception e) {
									 isErrorOccured = true;
									 error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									 error.setMessage("Error while Deleting Event Listings for Event within Few Days."+eventId);
									 error.setExample(""+e.fillInStackTrace());
									 error.setProcess("Deleting Event Listings.");
									 error.setEventId(eventId);
									 error.setTimeStamp(new Date());
									 errorList.add(error);
									 log.error("AUTOCATS96 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
									 System.err.println("AUTOCATS96 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
									 e.printStackTrace();
								}
							}	
						}
					
						 System.out.print(" : "+event.getId()+" :active brk ids 1 : "+new Date());
						 
						 if(exEvent.getZone() == null || exEvent.getZone()) {
							 isZoneEvent = true;
						 }
					
					 /*It will skip the ticket computation part for zone events . Please remove this code if you want to enable 
					  * ticket computation for vivid broker - Done by Ulaganathan */
					 
					 if(isZoneEvent){
						 //Skip the ticket computation process if the event is zone event.
						 continue;
					 }
					 
					try {
						Set<String> excludeZones = null;
						int defaultExpectedArrivialDatePriorDays = exEvent.getShippingDays();
						int sectionMinEtry = 1;
						
						if(exEvent.getIsPresale()) {
							DefaultAutoPricingProperties defaultAutopricing = presaleDefaultAutoPricingsMap.get(event.getParentCategoryId());
							if(defaultAutopricing != null) {
								sectionMinEtry = defaultAutopricing.getSectionCountTicket();
							}	
							
							excludeZones = presaleExcludeEventZoneMap.get(eventId);
							if(excludeZones == null) {
								excludeZones = presaleExcludeVenueCategoryZoneMap.get(event.getVenueCategoryId());
							}
						} else {
							DefaultAutoPricingProperties defaultAutopricing = defaultAutoPricingsMap.get(event.getParentCategoryId());
							if(defaultAutopricing != null) {
								sectionMinEtry = defaultAutopricing.getSectionCountTicket();
							}
							
							excludeZones = excludeEventZoneMap.get(eventId);
							if(excludeZones == null) {
								excludeZones = excludeVenueCategoryZoneMap.get(event.getVenueCategoryId());
							}
						}
						
						/*int minimumSectionTixCout = sectionMinEtry;
						boolean isAllowSectionRange = exEvent.getAllowSectionRange();
						
						ExchangeEvent presaleExEvent = null;
						if(event.getZoneCategoryGroupName().equalsIgnoreCase("PRESALE")) {
							presaleExEvent = presaleExEventMap.get(event.getId());
							minimumSectionTixCout = 1;
							isAllowSectionRange = true;
							
							if(presaleExEvent != null) {
								isAllowSectionRange = presaleExEvent.getAllowSectionRange();
							}
						}*/
						
						//Tamil 04/27/2018 Remove Presale Logic 
						//Tamil 10/11/2017 : if event has TN crawl only or if event is presale event then consider markup shipping as 0 and exposure as 1-OXP
						//Tamil 10/18/2017 Removed TN crawl only rule 
						//tnCrawlOnlyEventMap.get(event.getId()) != null ||
						/*if(event.getPresaleEvent() != null && event.getPresaleEvent()) {//event.getZoneCategoryGroupName() != null &&  event.getZoneCategoryGroupName().equalsIgnoreCase("PRESALE")
							exEvent.setExposure("1-OXP");
							exEvent.setLowerMarkup(0.0);
							exEvent.setUpperMarkup(0.0);
							exEvent.setLowerShippingFees(0.0);
							exEvent.setUpperShippingFees(0.0);
							sectionMinEtry = 1;
						}*/
						
						prePros = new Date().getTime()-(start.getTime());
						String productName = "AutoCats96";
						Collection<Ticket> finalTicketList = AutoCats96CategoryGroupManager.getTMATActiveTickets(event,autopricingProduct,exEvent.getAllowSectionRange()); 
						
						System.out.print(" : "+event.getId()+" :TMAT Act Tix 1 : "+new Date());
						
						for (Integer deletedBrokerId : tobeDeletedBrokerIds) {
							tnBrokerIds.remove(deletedBrokerId);
						}
						
						if(null != tnBrokerIds && tnBrokerIds.size() >0 ){
							eventMaxTixCount = brokerMaxTixCount * tnBrokerIds.size();
						}else{
							continue;
						}
						
						Map<Integer, Integer> brokerTixCountMap = new HashMap<Integer, Integer>();
						eventMaxTixCount = 0;
						
						for (Integer brokerId : tnBrokerIds) {
							
							if(brokerId.equals(AutoExchangeEventLoader.TIXCITY_BROKER_ID)){//Tixcity
								brokerTixCountMap.put(brokerId, 0);
								eventMaxTixCount=eventMaxTixCount+0;
							} else if(brokerId.equals(5)){//RTW
								brokerTixCountMap.put(brokerId, 96);//96
								eventMaxTixCount=eventMaxTixCount+96;//96
							} else if(brokerId.equals(10)){//RTW-2
								brokerTixCountMap.put(brokerId, 0);
								eventMaxTixCount=eventMaxTixCount+0;
							}else{
								brokerTixCountMap.put(brokerId, 0);
								eventMaxTixCount=eventMaxTixCount+0;
							}
							
						}
						
						//DAORegistry.getAutoCats96ExchangeEventDAO().updateAutoCats96ExchangeEventById(exEvent.getId(), productName, sectionTixCount);
						
						/*Map<Integer, List<AutoCats96CategoryTicket>> categoryTicketQtyMap= AutoCats96CategoryGroupManager.computeCategoryTickets(event, exEvent, 
								finalTicketList,defaultExpectedArrivialDatePriorDays, sectionMinEtry, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, 
								 scoreBigExAddMarkup,fanxchangeMarkup,autopricingProduct,isAbove96Cats);*/
						
						List<AutoCats96CategoryTicket> categoryTickets = AutoCats96CategoryGroupManager.computeCategoryTickets(event, exEvent, 
								finalTicketList,defaultExpectedArrivialDatePriorDays, sectionMinEtry, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, 
								 scoreBigExAddMarkup,fanxchangeMarkup,ticketcityMarkup,autopricingProduct);
						
						tixComp = new Date().getTime()-(start.getTime()+prePros);
						
						Map<String,AutoCats96CategoryTicket> catTixMap = new HashMap<String, AutoCats96CategoryTicket>();	
						for (AutoCats96CategoryTicket catTix : categoryTickets) { // Add generated tickets in map
							 
							//Tamil : changed tickets minimam price from $50 to $70
							//if((catTix.getQuantity() <= 3 && catTix.getTnPrice() < 75 ) ||
									 //(catTix.getQuantity() > 3 && catTix.getTnPrice() < 50)){
							if(catTix.getTnPrice() < 70 ) {
								 continue;
							 }
							 
							 if(catTix.getTnPrice()>=5000){
								 continue;
							 }
							 if(catTix.getCatRow() == null || catTix.getCatRow().contains("-") || catTix.getCatRow().toLowerCase().contains(" or ") ||
							 catTix.getSection() == null || catTix.getSection().contains("-") || catTix.getSection().toLowerCase().contains(" or ")) {
								 continue;
							 }
							 
							 catTix.setIsPresale(exEvent.getIsPresale());
							 
							 //update user assigned shipping method
							 if(exEvent.getShippingMethod() != null) {
								 catTix.setShippingMethodSpecialId(exEvent.getShippingMethod());
							 }
							 
							 String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+catTix.getCatRow().replaceAll("\\s+", " ").trim().toLowerCase();
							 
							 AutoCats96CategoryTicket existingCatTix =catTixMap.get(key);  
							 if(existingCatTix != null) {
								 //if(!existingCatTix.getActualPrice().equals(catTix.getActualPrice())) {
									 if(existingCatTix.getActualPrice() > catTix.getActualPrice()) {
										 catTixMap.put(key, catTix);
										 continue;
									 }
								//}
							 }
							 catTixMap.put(key, catTix);
						 }
						System.out.print(" : "+event.getId()+" :Comp Cat Tix 1 : "+new Date());
						
						List<AutoCats96CategoryTicket> excludeZoneTickets = new ArrayList<AutoCats96CategoryTicket>();
						Collection<AutoCats96CategoryTicket> catTixFromDB = DAORegistry.getAutoCats96CategoryTicketDAO().getAllTNAutoCats96CategoryTicketsByEventId(eventId);
						Map<String, AutoCats96CategoryTicket> catTixFromDBMap = new HashMap<String, AutoCats96CategoryTicket>();
						for (AutoCats96CategoryTicket dbCatTix : catTixFromDB) {
							
							if(dbCatTix.getTnExchangeEventId() != null && !dbCatTix.getTnExchangeEventId().equals(event.getAdmitoneId())) {
								 dbCatTix.setStatus("DELETED");
								 dbCatTix.setLastUpdated(new Date());
								 dbCatTix.setReason("POS Event Id Mismatching");
								 excludeZoneTickets.add(dbCatTix);
								 continue;
							 }
							
							if(dbCatTix.getTnCategoryTicketGroupId() != null) {
								 if(excludeZones != null && dbCatTix.getTnBrokerId() != null && dbCatTix.getTmatZone() != null) {
									 String exZoneKey = dbCatTix.getTnBrokerId()+"_"+dbCatTix.getTmatZone().toUpperCase(); 
									 if(excludeZones.contains(exZoneKey)) {
										 dbCatTix.setStatus("DELETED");
										 dbCatTix.setLastUpdated(new Date());
										 dbCatTix.setReason("Excluded Zone ticket");
										 excludeZoneTickets.add(dbCatTix);
										 continue;
									 }
								 }
							}
							String key = dbCatTix.getQuantity()+":"+ dbCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+dbCatTix.getCatRow().replaceAll("\\s+", " ").trim().toLowerCase();
							catTixFromDBMap.put(key, dbCatTix);	
						}
						System.out.print(" : "+event.getId()+" :TMAT DB Tix 1 : "+new Date());
						
						Map<Integer, List<AutoCats96CategoryTicket>> brokersTicketMap = AutoCats96CategoryTicketUtil.prioritizingCategoryTicketsByBroker(catTixFromDBMap, 
								catTixMap, brokerTixCountMap,eventMaxTixCount,tnBrokerIds,excludeZones);
						System.out.print(" : "+event.getId()+" :comp Prior Tix 1 : "+new Date());
						
						AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
						Map<Integer, AutopricingSettings> brokerAutoPricingSettingMap = new HashMap<Integer, AutopricingSettings>(); 
						Broker broker = null;
						AutopricingSettings autopricingSettings = null;
						
						for (Integer finalBrokerId  : tnBrokerIds) {
							broker = BrokerUtils.getBrokerById(finalBrokerId);
							if(broker!=null){
								autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
							}
							brokerAutoPricingSettingMap.put(finalBrokerId, autopricingSettings);
						}
						
						List<AutoCats96CategoryTicket> toBeSavedOrUpdatedTixs = new ArrayList<AutoCats96CategoryTicket>();
						
						for (AutoCats96CategoryTicket tix : excludeZoneTickets) {
							toBeSavedOrUpdatedTixs.add(tix);
							try {
								broker = BrokerUtils.getBrokerById(tix.getTnBrokerId());
								autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
								boolean isUpdateTN=true;
								if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
									isUpdateTN = false;
								}
								if(isUpdateTN){
									if(tix.getTnCategoryTicketGroupId()!=null){	
										 if(Counter.isMinuteDeleteAllow(broker)) {
											 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),tix.getTnCategoryTicketGroupId());
											 broker.addMinuteUpdateCounter();
											 eremovet++;
											 tix.setReason(tix.getReason()+" and Deleted from TN");
										}
									 }
								}
							 } catch (Exception e) {
								 tix.setReason(tix.getReason()+" and Exception in TN");
								e.printStackTrace();
								isErrorOccured = true;
								error = new AutoPricingError();
								error.setProductId(autopricingProduct.getId());
								error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+tix.getTnCategoryTicketGroupId());
								error.setProcess("POS Remove Category.");
								error.setExample(""+e.fillInStackTrace());
								error.setEventId(eventId);
								error.setTimeStamp(new Date());
								errorList.add(error);
								log.error("AUTOCATS96 14 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getCatRow() + "):Msg:" + e.fillInStackTrace());
								System.out.println("AUTOCATS96 14 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getCatRow() + "):Msg:" + e.fillInStackTrace());
							 }
						}
						postPros = new Date().getTime()-(start.getTime()+prePros+tixComp);
						System.out.print(" : "+event.getId()+" :Exclude Zone Tix 1 : "+new Date());
						
						for (Integer finalBrokerId : brokersTicketMap.keySet()) {
							List<AutoCats96CategoryTicket> tickets =brokersTicketMap.get(finalBrokerId);
							if(null ==tickets || tickets.isEmpty()){
								continue;
							}
							
							
							broker = null;
							autopricingSettings = null;
							if(finalBrokerId == 0 ){
								
								for (AutoCats96CategoryTicket tix : tickets) {
									tix.setStatus("DELETED");
									tix.setLastUpdated(new Date());
									eremovet++;
									tix.setReason("Ticket not exist in TMAT");
									
									toBeSavedOrUpdatedTixs.add(tix);
									
									try {
										broker = BrokerUtils.getBrokerById(tix.getTnBrokerId());
										autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
										boolean isUpdateTN=true;
										if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
											isUpdateTN = false;
										}
										if(isUpdateTN){
											if(tix.getTnCategoryTicketGroupId()!=null){	
												 if(Counter.isMinuteDeleteAllow(broker)) {
													 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),tix.getTnCategoryTicketGroupId());
													 broker.addMinuteUpdateCounter();
													 eremovet++;
													 
													 tix.setReason("Ticket not exist in TMAT and Deleted from TN");
												}
											 }
										}
									 } catch (Exception e) {
										 tix.setReason("Ticket not exist in TMAT and Exception in TN");
										e.printStackTrace();
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+tix.getTnCategoryTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(eventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("AUTOCATS96 14 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getCatRow() + "):Msg:" + e.fillInStackTrace());
										System.out.println("AUTOCATS96 14 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getCatRow() + "):Msg:" + e.fillInStackTrace());
									 }
								}
								//DAORegistry.getAutoCats96CategoryTicketDAO().updateAll(tickets);
								continue;
							}
							
							isTnEvent = true;
							broker = null;
							autopricingSettings = null;
							broker = BrokerUtils.getBrokerById(finalBrokerId);
							if(null != broker){
								autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
							}
							
							boolean isUpdateTN=true,isBrokerEnabled=false;
							if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
								isUpdateTN = false;
							}
							
							if(activeBrokerIds.contains(finalBrokerId)){
								isBrokerEnabled=true;
							}else{
								//continue;
							}
							
							Map<Integer,PosEvent> posEventMap = brokerPosEventMap.get(finalBrokerId);
							PosEvent posEvent = null;
							if(null == posEventMap || posEventMap.isEmpty()){
								isTnEvent = false;
							}else{
								posEvent = posEventMap.get(event.getAdmitoneId());
							}
							
							if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || 
									 posEvent.getExchangeEventId() == 0){
								isTnEvent = false;
							}
							
							Map<String, Integer> posVenueCatIdMap = new HashMap<String, Integer>();
							
							for (AutoCats96CategoryTicket tix : tickets) {
								
								tix.setTnBrokerId(finalBrokerId);
								tix.setProductName(productName);
								tix.setLastUpdated(new Date());
								if(null != tix.getId() && tix.getTnCategoryTicketGroupId() != null){
									
									if(null != tix.getIsTnUpdate() && tix.getIsTnUpdate()){
										
										if(isTnEvent && isUpdateTN && isBrokerEnabled && !isZoneEvent){ 
											
										 	 if(tix.getTnCategoryTicketGroupId()!=null && Counter.isMinuteUpdateAllow(broker)) {
												InduxDAORegistry.getPosCategoryTicketGroupDAO().updateCategoryTicketGroup(tix,broker.getPosBrokerId());
												broker.addMinuteUpdateCounter();
												eUpdate++;
											}
										 }
										
									}
									if(null != tix.getIsEdited() && tix.getIsEdited()){
										toBeSavedOrUpdatedTixs.add(tix);
									}
								}else {
									//boolean isCreateListingEnabled = Counter.isCreateListingEnabled(broker);
									if(isTnEvent  && isUpdateTN && isBrokerEnabled && !isZoneEvent){// && isCreateListingEnabled
										
										if(Counter.isMinuteAddAllow(broker)){
											if(posVenueCatIdMap.isEmpty()){
												try {
													List<PosVenueCategory> posVenueCatList = InduxDAORegistry.getPosVenueCategoryDAO().getAllVenueCategorysByVenueId(posEvent.getVenueId(),broker.getPosBrokerId());
													for (PosVenueCategory posVenueCategory : posVenueCatList) {
														
														//Tamil : skip all venue configuration zone id mapped venue configurations for non zones pricing listings
														if(posVenueCategory.getVenueConfigZoneId() != null && posVenueCategory.getVenueConfigZoneId() > 0) {
															continue;
														}
														String key = posVenueCategory.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+posVenueCategory.getRow().replaceAll("\\s+", " ").trim().toLowerCase();
														posVenueCatIdMap.put(key, posVenueCategory.getId());
													}
												} catch (Exception e) {
													e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setExample(""+e.fillInStackTrace());
													error.setMessage("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ": While loading POS VenueCategors :ErrorMsg:" + e.fillInStackTrace());
													error.setProcess("Loading POS VenueCategors.");
													error.setEventId(event.getId());
													error.setTimeStamp(new Date());
													errorList.add(error);
													System.err.println("AUTOCATS96 9 : NF Error From While Loading POS VenueCategors.."+eventId);
												}
											}
											
											 tix.setPosVenueId(posEvent.getVenueId());
											 tix.setPosEventId(posEvent.getId());	
											 Integer posVenueCategoryId = posVenueCatIdMap.get(tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tix.getCatRow().replaceAll("\\s+", " ").trim().toLowerCase());
											 if(null == posVenueCategoryId || posVenueCategoryId == -1) {
												 tix.setRowRange(tix.getCatRow());
												posVenueCategoryId = InduxDAORegistry.getPosVenueCategoryDAO().save(tix,broker.getPosBrokerId(),autopricingProduct);
												String posVenueCategorykey = tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tix.getCatRow().replaceAll("\\s+", " ").trim().toLowerCase();
												posVenueCatIdMap.put(posVenueCategorykey, posVenueCategoryId);
											}
											if(null == posVenueCategoryId || posVenueCategoryId == -1) {
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("Error While Creating VenueCategory");
												error.setExample("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getCatRow() +")");
												error.setProcess("ADD Venuecategory.");
												error.setEventId(event.getId());
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("AUTOCATS96 11 :Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getCatRow() +")");
												System.err.println("AUTOCATS96 11 : Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getCatRow() +")");
												continue;
											}
											tix.setPosVenueCategoryId(posVenueCategoryId);
											tix.setPosExchangeEventId(posEvent.getExchangeEventId());
											
											try {
												autopricingProduct.setInternalNotes(tix.getInternalNotes());
												
												Integer tnCategoryTicketGroupId = InduxDAORegistry.getPosCategoryTicketGroupDAO().save(autopricingProduct, tix,broker.getPosBrokerId());
												if(tnCategoryTicketGroupId!=-1l){
													tix.setTnCategoryTicketGroupId(tnCategoryTicketGroupId);
													tix.setIsEdited(true);
													broker.addMinuteUpdateCounter();
													eInsert++;
												}
											 } catch(Exception e) {
												 isErrorOccured = true;
												 error = new AutoPricingError();
												 error.setProductId(autopricingProduct.getId());
												 error.setMessage("Error while creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
												 error.setProcess("Ticket Group Creation..");
												 error.setExample(""+e.fillInStackTrace());
												 error.setEventId(eventId);
												 error.setTimeStamp(new Date());
												 errorList.add(error);
												 log.error("AUTOCATS96 12 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
												 System.out.println("AUTOCATS96 12 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
												 e.printStackTrace();
											 }
										}
									 }
									if(tix.getId() == null) {
										tix.setCreatedDate(new Date());
									}
									
									if(null != tix.getIsEdited() && tix.getIsEdited()){
										toBeSavedOrUpdatedTixs.add(tix);
									}
								}
								
								if(broker!=null){
									 Integer addCount = brokerAddCountMap.get(broker.getId());
									 if(addCount==null){
										 addCount = 0;
									 }
									 addCount= addCount + eInsert;
									 brokerAddCountMap.put(broker.getId(),addCount);
									 
									 Integer deleteCount = brokerDeleteCountMap.get(broker.getId());
									 if(deleteCount==null){
										 deleteCount = 0;
									 }
									 deleteCount= deleteCount + eremovet;
									 brokerDeleteCountMap.put(broker.getId(),deleteCount);
									 
									 Integer updateCount = brokerUpdateCountMap.get(broker.getId());	 
									 if(updateCount==null){
										 updateCount = 0;
									 }
									 updateCount= updateCount + eUpdate;
									 brokerUpdateCountMap.put(broker.getId(),updateCount);
								}
							}
						}
						tixUpdate = new Date().getTime()-(start.getTime()+prePros+tixComp+postPros);
						
						System.out.print(" : "+event.getId()+" :Brok Tix Map Proc 1 : "+new Date());
						
						DAORegistry.getAutoCats96CategoryTicketDAO().saveOrUpdateAll(toBeSavedOrUpdatedTixs);
						System.out.print(" : "+event.getId()+" :TMATTix Update : "+new Date());
						
						dbUpdate = new Date().getTime()-(start.getTime()+prePros+tixComp+postPros+tixUpdate);
					} catch (Exception e) {
						 isErrorOccured = true;
						 error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while Computing Category Tickets."+eventId);
						 error.setProcess("Category Ticket Computation.");
						 error.setExample(""+e.fillInStackTrace());
						 error.setEventId(eventId);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("AUTOCATS96 8 :Error while Computing Category Tickets."+eventId);
						 System.err.println("AUTOCATS96 8 : Error while Computing Category Tickets."+eventId);
						 e.printStackTrace();
					}
					tInsert += eInsert;
					tUpdate += eUpdate;
					tremovet += eremovet;
					 
					System.out.println("AUTOCATS96  : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet);
					long finalTim = new Date().getTime()-start.getTime(); 
					System.out.println("AUTOCATS96 Time fnl:"+finalTim+"  : Pre :"+prePros+" comp : "+tixComp+" : post :"+postPros+" :tixup: "+tixUpdate+" :dbup: "+dbUpdate+" : "+new Date());
				}
				
				List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
				
				for(Integer brokerId:brokerAddCountMap.keySet()){
					int count = brokerAddCountMap.get(brokerId);
					if(count==0){
						continue;
					}
					AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
					Broker broker = BrokerUtils.getBrokerById(brokerId);
					addAudit.setProduct(autopricingProduct);
					addAudit.setBroker(broker);
					addAudit.setCount(count);
					addAudit.setProcessType("Insert");
					addAudit.setLastRunTime(lastRunTime);
					autoCatsProjectAuditList.add(addAudit);
				}
				
				for(Integer brokerId:brokerUpdateCountMap.keySet()){
					int count = brokerUpdateCountMap.get(brokerId);
					if(count==0){
						continue;
					}
					AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
					Broker broker = BrokerUtils.getBrokerById(brokerId);
					addAudit.setProduct(autopricingProduct);
					addAudit.setBroker(broker);
					addAudit.setCount(count);
					addAudit.setProcessType("Update");
					addAudit.setLastRunTime(lastRunTime);
					autoCatsProjectAuditList.add(addAudit);
					
				}
				
				for(Integer brokerId:brokerDeleteCountMap.keySet()){
					int count = brokerDeleteCountMap.get(brokerId);
					if(count==0){
						continue;
					}
					AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
					Broker broker = BrokerUtils.getBrokerById(brokerId);
					addAudit.setProduct(autopricingProduct);
					addAudit.setBroker(broker);
					addAudit.setCount(count);
					addAudit.setProcessType("Delete");
					addAudit.setLastRunTime(lastRunTime);
					autoCatsProjectAuditList.add(addAudit);
					
				}
				DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);
				autopricingProduct.setInternalNotes("MINICATS,VIPMINICATS,LASTROW MINICATS,AUTOCAT");
				DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
				System.out.println("AUTOCATS96 Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet);
				
				eventDeleteFlagProperty = DAORegistry.getPropertyDAO().getPropertyByName("autocats96.event.delete.flag");
				Boolean isFlagOn = Boolean.valueOf(eventDeleteFlagProperty.getValue());
				if(isFlagOn){
					eventDeleteFlagProperty.setValue("false");
					DAORegistry.getPropertyDAO().update(eventDeleteFlagProperty);
				}
				
				running = false; 
			 }catch(Exception e){
				 running = false;
				 isErrorOccured = true;
				 e.printStackTrace();
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Common Properties.");
				 error.setProcess("Loading common Properties..");
				 error.setExample(""+e.fillInStackTrace());
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("AUTOCATS96 16 :Error while Loading Common Properties.");
				 System.out.println("AUTOCATS96 16 : Error while Loading Common Properties.");
				 
			 }
			 
			 String subject,fileName;
			 Map<String, Object> map;
				
				if(isErrorOccured){
					map = new HashMap<String, Object>();
					map.put("error", error);
					map.put("errors", errorList);
					try {
						log.info("CT error size......"+errorList.size());
						DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
						subject = "AutoCats96 Scheduler job failed :";
						fileName = "templates/autopricing-job-failure-message.txt";
						EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
						
					} catch (Exception e) {
						log.error("AUTOCATS96 17 : Error while Inserting Error Listings in TMAT.");
						 System.out.println("AUTOCATS96 17 :Error while Inserting Error Listings in TMAT.");
						e.printStackTrace();
					}
				}
		 }
	 
	 
	 @Override
	protected void executeInternal(JobExecutionContext context)throws JobExecutionException {
		System.out.println("AUTOCATS96 Job Called.." + new Date() + ": " + running);
		log.info("AUTOCATS96 Job Called.." + new Date() + ": " + running);
		try{
			processAutoCats96Tickets();
			System.out.println("AUTOCATS96 Scheduler Job finished @ " + new Date());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}

	public static void setStopped(Boolean stopped) {
		AutoCats96Scheduler.stopped = stopped;
	}

	public static Boolean isRunning() {
		return running;
	}

	public static void setRunning(Boolean running) {
		AutoCats96Scheduler.running = running;
	}

	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("AutoCats96").getLastRunTime();
		}
		return lastUpdateTime;
	}

	public static void setLastUpdateTime(Date lastUpdateTime) {
		AutoCats96Scheduler.lastUpdateTime = lastUpdateTime;
	}

	public static Date getNextRunTime() {
		return nextRunTime;
	}

	public static void setNextRunTime(Date nextRunTime) {
		AutoCats96Scheduler.nextRunTime = nextRunTime;
	}

}