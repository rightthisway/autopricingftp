package com.rtw.autopricing.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEvent;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEventAudit;
import com.rtw.autopricing.ftp.data.AutoExchangeEventUpdateDTO;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEventAudit;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEventAudit;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEventAudit;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEvent;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEventAudit;
import com.rtw.autopricing.ftp.data.TMATInduxEventDetails;
import com.rtw.autopricing.ftp.data.TMATInduxEventDetailsAudit;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEvent;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEventAudit;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEventAudit;



public class AutoExchangeEventUpdater  {
	
	private static Logger log = LoggerFactory.getLogger(AutoExchangeEventUpdater.class);
	/*private boolean running;
	@Override 
	public void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		
		try {
			if(!running){
				running = true;
				
				System.out.println("Autopricing Event Updater Job Started....."+new Date());
				
				autoExchangeEventUpdateProcess();
				
				running = false;
				System.out.println("Autopricing Event Updater Job Completed....."+new Date());
			}
		} catch(Exception e) {
			running = false;
			e.printStackTrace();
			
			try {
				ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
				String toAddress = resourceBundle.getString("emailNotificationTo");
				String ccAddress= resourceBundle.getString("emailNotificationCCTo");
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("example", ""+e.fillInStackTrace());
				
				String subject = "Auto Exchange event updater job failed :";
				String fileName = "templates/auto-exchange-event-loader-job-failure-message.txt";
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
			} catch (Exception e1) {
				log.error("Error while sending email in auto exchagne event updater job.");
				e1.printStackTrace();
			}
			
		}
	}*/
	
	public static void autoExchangeEventUpdateProcess() throws Exception {
		
		//System.out.println("running...in ExchangeEventLoader");
			
			try{
				List<AutoExchangeEventUpdateDTO> autoExchangeEventUpdateDTOs = DAORegistry.getQueryManagerDAO().getAutoExchangeEventUpdateList();
				
				if(autoExchangeEventUpdateDTOs != null && !autoExchangeEventUpdateDTOs.isEmpty()){
					System.out.println("Auto Exchange event updater event size........."+autoExchangeEventUpdateDTOs.size());
					
					List<PresaleAutoCatExchangeEvent> presaleAutoCatExEvents = new ArrayList<PresaleAutoCatExchangeEvent>();
					List<PresaleAutoCatExchangeEventAudit> presaleAutoCatExEventAudits = new ArrayList<PresaleAutoCatExchangeEventAudit>();
					
					List<MiniExchangeEvent> miniExEvents = new ArrayList<MiniExchangeEvent>();
					List<MiniExchangeEventAudit> miniExEventAudits = new ArrayList<MiniExchangeEventAudit>();
					
					List<SGLastFiveRowExchangeEvent> sgLastFiveRowExEvents = new ArrayList<SGLastFiveRowExchangeEvent>();
					List<SGLastFiveRowExchangeEventAudit> sgLastFiveRowExEventAudits = new ArrayList<SGLastFiveRowExchangeEventAudit>();

					List<VipMiniExchangeEvent> vipMiniExEvents = new ArrayList<VipMiniExchangeEvent>();
					List<VipMiniExchangeEventAudit> vipMiniExEventAudits = new ArrayList<VipMiniExchangeEventAudit>();
					
					List<LastRowMiniExchangeEvent> lastRowMiniExEvents = new ArrayList<LastRowMiniExchangeEvent>();
					List<LastRowMiniExchangeEventAudit> lastRowMiniExEventAudits = new ArrayList<LastRowMiniExchangeEventAudit>();
					
					List<LarryLastExchangeEvent> larryLastExEvents = new ArrayList<LarryLastExchangeEvent>();
					List<LarryLastExchangeEventAudit> larryLastExEventAudits = new ArrayList<LarryLastExchangeEventAudit>();
					
					List<ZoneLastRowMiniExchangeEvent> zoneLastRowMiniExEvents = new ArrayList<ZoneLastRowMiniExchangeEvent>();
					List<ZoneLastRowMiniExchangeEventAudit> zoneLastRowMiniExEventAudits = new ArrayList<ZoneLastRowMiniExchangeEventAudit>();
					
					List<ZonePricingExchangeEvent> zonePricingExEvents = new ArrayList<ZonePricingExchangeEvent>();
					List<ZonePricingExchangeEventAudit> zonePricingExEventAudits = new ArrayList<ZonePricingExchangeEventAudit>();
					
					List<TixCityZonePricingExchangeEvent> tixCityZonePricingExEvents = new ArrayList<TixCityZonePricingExchangeEvent>();
					List<TixCityZonePricingExchangeEventAudit> tixCityZonePricingExEventAudits = new ArrayList<TixCityZonePricingExchangeEventAudit>();
					
					List<ManhattanZonePricingExchangeEvent> manhattanZonePricingExEvents = new ArrayList<ManhattanZonePricingExchangeEvent>();
					List<ManhattanZonePricingExchangeEventAudit> manhattanZonePricingExEventAudits = new ArrayList<ManhattanZonePricingExchangeEventAudit>();
					
					List<ZoneTicketsProcessorExchangeEvent> zoneTicketsProcessorExEvents = new ArrayList<ZoneTicketsProcessorExchangeEvent>();
					List<ZoneTicketsProcessorExchangeEventAudit> zoneTicketsProcessorExEventAudits = new ArrayList<ZoneTicketsProcessorExchangeEventAudit>();
					
					List<PresaleZoneTicketsExchangeEvent> presaleZoneTicketsExEvents = new ArrayList<PresaleZoneTicketsExchangeEvent>();
					List<PresaleZoneTicketsExchangeEventAudit> presaleZoneTicketsExEventAudits = new ArrayList<PresaleZoneTicketsExchangeEventAudit>();
					
					List<AutoCats96ExchangeEvent> autoCats96ExEvents = new ArrayList<AutoCats96ExchangeEvent>();
					List<AutoCats96ExchangeEventAudit> autoCats96ExEventAudits = new ArrayList<AutoCats96ExchangeEventAudit>();
					
					List<TicketNetworkSSAccountExchangeEvent> tnSpecialExEvents = new ArrayList<TicketNetworkSSAccountExchangeEvent>();
					List<TicketNetworkSSAccountExchangeEventAudit> tnSpecialExEventAudits = new ArrayList<TicketNetworkSSAccountExchangeEventAudit>();
					
					List<VipLastRowMiniExchangeEvent> vipLastRowMiniExEvents = new ArrayList<VipLastRowMiniExchangeEvent>();
					List<VipLastRowMiniExchangeEventAudit> vipLastRowMiniExEventAudits = new ArrayList<VipLastRowMiniExchangeEventAudit>();
					
					List<TMATInduxEventDetails> tmatInduxEventDetails = new ArrayList<TMATInduxEventDetails>();
					List<TMATInduxEventDetailsAudit> tmatInduxEventDetailAudits = new ArrayList<TMATInduxEventDetailsAudit>();
					
					String username = "AUTO";//SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
					
					for(AutoExchangeEventUpdateDTO autoExchangeEventUpdateDTO : autoExchangeEventUpdateDTOs){
						
						
						TMATInduxEventDetails tmatInduxEventDetail = DAORegistry.getTmatInduxEventDetailsDAO().getTMATInduxEventDetailsByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(tmatInduxEventDetail != null ) {
							TMATInduxEventDetailsAudit tmatInduxEventDetailAudit = new TMATInduxEventDetailsAudit(tmatInduxEventDetail);
							tmatInduxEventDetailAudits.add(tmatInduxEventDetailAudit);
								
							tmatInduxEventDetail.setPosExchangeEventId(autoExchangeEventUpdateDTO.getPosExchangeEventId());
							tmatInduxEventDetail.setPosParentCategoryName(autoExchangeEventUpdateDTO.getPosParentCategoryName());
							tmatInduxEventDetail.setPosChildCategoryName(autoExchangeEventUpdateDTO.getPosChildCategoryName());
							tmatInduxEventDetail.setPosGrandChildCategoryName(autoExchangeEventUpdateDTO.getPosGrandChildCategoryName());
							tmatInduxEventDetail.setIsZoneEvent(autoExchangeEventUpdateDTO.getIsZoneEvent());
							tmatInduxEventDetail.setLastUpdate(now);
							
							tmatInduxEventDetails.add(tmatInduxEventDetail);
						}
						
						ZoneTicketsProcessorExchangeEvent zoneTicketsProcessorExEvent = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getZoneTicketsProcessorExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(zoneTicketsProcessorExEvent != null ) {
							ZoneTicketsProcessorExchangeEventAudit zoneTicketsProcessorExEventAudit = new ZoneTicketsProcessorExchangeEventAudit(zoneTicketsProcessorExEvent);
							zoneTicketsProcessorExEventAudit.setStatus("DELETED");
							zoneTicketsProcessorExEventAudit.setLastUpdatedDate(now);
							zoneTicketsProcessorExEventAudit.setLastUpdatedBy(username);
							zoneTicketsProcessorExEventAudits.add(zoneTicketsProcessorExEventAudit);
								
							zoneTicketsProcessorExEvents.add(zoneTicketsProcessorExEvent);
						}
					
						PresaleZoneTicketsExchangeEvent presaleZoneTicketsExEvent = DAORegistry.getPresaleZoneTicketsExchangeEventDAO().getPresaleZoneTicketsProcessorExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(presaleZoneTicketsExEvent != null ) {
							PresaleZoneTicketsExchangeEventAudit presaleZoneTicketsExEventAudit = new PresaleZoneTicketsExchangeEventAudit(presaleZoneTicketsExEvent);
							presaleZoneTicketsExEventAudit.setStatus("DELETED");
							presaleZoneTicketsExEventAudit.setLastUpdatedDate(now);
							presaleZoneTicketsExEventAudit.setLastUpdatedBy(username);
							presaleZoneTicketsExEventAudits.add(presaleZoneTicketsExEventAudit);
								
							presaleZoneTicketsExEvents.add(presaleZoneTicketsExEvent);
						}
						
						AutoCats96ExchangeEvent autoCats96ExEvent = DAORegistry.getAutoCats96ExchangeEventDAO().getAutoCats96ExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(autoCats96ExEvent != null ) {
							AutoCats96ExchangeEventAudit autoCats96ExEventAudit = new AutoCats96ExchangeEventAudit(autoCats96ExEvent);
							autoCats96ExEventAudit.setStatus("DELETED");
							autoCats96ExEventAudit.setLastUpdatedDate(now);
							autoCats96ExEventAudit.setLastUpdatedBy(username);
							autoCats96ExEventAudits.add(autoCats96ExEventAudit);
								
							autoCats96ExEvents.add(autoCats96ExEvent);
						}
						
						PresaleAutoCatExchangeEvent presaleAutoCatExEvent = DAORegistry.getPresaleAutoCatExchangeEventDAO().getPresaleAutoCatExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(presaleAutoCatExEvent != null ) {
							PresaleAutoCatExchangeEventAudit presaleAutoCatExEventAudit = new PresaleAutoCatExchangeEventAudit(presaleAutoCatExEvent);
							presaleAutoCatExEventAudit.setStatus("DELETED");
							presaleAutoCatExEventAudit.setLastUpdatedDate(now);
							presaleAutoCatExEventAudit.setLastUpdatedBy(username);
							presaleAutoCatExEventAudits.add(presaleAutoCatExEventAudit);
								
							presaleAutoCatExEvents.add(presaleAutoCatExEvent);
						}
						
						MiniExchangeEvent miniExEvent = DAORegistry.getMiniExchangeEventDAO().getMiniExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(miniExEvent != null ) {
							MiniExchangeEventAudit miniExEventAudit = new MiniExchangeEventAudit(miniExEvent);
							miniExEventAudit.setStatus("DELETED");
							miniExEventAudit.setLastUpdatedDate(now);
							miniExEventAudit.setLastUpdatedBy(username);
							miniExEventAudits.add(miniExEventAudit);
								
							miniExEvents.add(miniExEvent);
						}
						
						SGLastFiveRowExchangeEvent sgLastFiveRowExEvent = DAORegistry.getSgLastFiveRowExchangeEventDAO().getSGLastFiveRowExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(sgLastFiveRowExEvent != null ) {
							SGLastFiveRowExchangeEventAudit sgLastFiveRowExEventAudit = new SGLastFiveRowExchangeEventAudit(sgLastFiveRowExEvent);
							sgLastFiveRowExEventAudit.setStatus("DELETED");
							sgLastFiveRowExEventAudit.setLastUpdatedDate(now);
							sgLastFiveRowExEventAudit.setLastUpdatedBy(username);
							sgLastFiveRowExEventAudits.add(sgLastFiveRowExEventAudit);
								
							sgLastFiveRowExEvents.add(sgLastFiveRowExEvent);
						}
						
						VipMiniExchangeEvent vipMiniExEvent = DAORegistry.getVipMiniExchangeEventDAO().getVipMiniExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(vipMiniExEvent != null ) {
							VipMiniExchangeEventAudit vipMiniExEventAudit = new VipMiniExchangeEventAudit(vipMiniExEvent);
							vipMiniExEventAudit.setStatus("DELETED");
							vipMiniExEventAudit.setLastUpdatedDate(now);
							vipMiniExEventAudit.setLastUpdatedBy(username);
							vipMiniExEventAudits.add(vipMiniExEventAudit);
								
							vipMiniExEvents.add(vipMiniExEvent);
						}
						
						LastRowMiniExchangeEvent lastRowMiniExEvent = DAORegistry.getLastRowMiniExchangeEventDAO().getLastRowMiniExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(lastRowMiniExEvent != null ) {
							LastRowMiniExchangeEventAudit lastRowMiniExEventAudit = new LastRowMiniExchangeEventAudit(lastRowMiniExEvent);
							lastRowMiniExEventAudit.setStatus("DELETED");
							lastRowMiniExEventAudit.setLastUpdatedDate(now);
							lastRowMiniExEventAudit.setLastUpdatedBy(username);
							lastRowMiniExEventAudits.add(lastRowMiniExEventAudit);
								
							lastRowMiniExEvents.add(lastRowMiniExEvent);
						}
						
						ZoneLastRowMiniExchangeEvent zoneLastRowMiniExEvent = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getZoneLastRowMiniExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(zoneLastRowMiniExEvent != null ) {
							ZoneLastRowMiniExchangeEventAudit zoneLastRowMiniExEventAudit = new ZoneLastRowMiniExchangeEventAudit(zoneLastRowMiniExEvent);
							zoneLastRowMiniExEventAudit.setStatus("DELETED");
							zoneLastRowMiniExEventAudit.setLastUpdatedDate(now);
							zoneLastRowMiniExEventAudit.setLastUpdatedBy(username);
							zoneLastRowMiniExEventAudits.add(zoneLastRowMiniExEventAudit);
								
							zoneLastRowMiniExEvents.add(zoneLastRowMiniExEvent);
						}
						LarryLastExchangeEvent larryLastExEvent = DAORegistry.getLarryLastExchangeEventDAO().getLarryLastExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(larryLastExEvent != null ) {
							LarryLastExchangeEventAudit larryLastExEventAudit = new LarryLastExchangeEventAudit(larryLastExEvent);
							larryLastExEventAudit.setStatus("DELETED");
							larryLastExEventAudit.setLastUpdatedDate(now);
							larryLastExEventAudit.setLastUpdatedBy(username);
							larryLastExEventAudits.add(larryLastExEventAudit);
								
							larryLastExEvents.add(larryLastExEvent);
						}
						
						ZonePricingExchangeEvent zonePricingExEvent = DAORegistry.getZonePricingExchangeEventDAO().getZonePricingExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(zonePricingExEvent != null ) {
							ZonePricingExchangeEventAudit zonePricingExEventAudit = new ZonePricingExchangeEventAudit(zonePricingExEvent);
							zonePricingExEventAudit.setStatus("DELETED");
							zonePricingExEventAudit.setLastUpdatedDate(now);
							zonePricingExEventAudit.setLastUpdatedBy(username);
							zonePricingExEventAudits.add(zonePricingExEventAudit);
								
							zonePricingExEvents.add(zonePricingExEvent);
						}
						
						TixCityZonePricingExchangeEvent tixCityZonePricingExEvent = DAORegistry.getTixCityZonePricingExchangeEventDAO().getTixCityZonePricingExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(tixCityZonePricingExEvent != null ) {
							TixCityZonePricingExchangeEventAudit tixCityZonePricingExEventAudit = new TixCityZonePricingExchangeEventAudit(tixCityZonePricingExEvent);
							tixCityZonePricingExEventAudit.setStatus("DELETED");
							tixCityZonePricingExEventAudit.setLastUpdatedDate(now);
							tixCityZonePricingExEventAudit.setLastUpdatedBy(username);
							tixCityZonePricingExEventAudits.add(tixCityZonePricingExEventAudit);
								
							tixCityZonePricingExEvents.add(tixCityZonePricingExEvent);
						}
						
						ManhattanZonePricingExchangeEvent manhattanZonePricingExEvent = DAORegistry.getManhattanZonePricingExchangeEventDAO().getManhattanZonePricingExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(manhattanZonePricingExEvent != null ) {
							ManhattanZonePricingExchangeEventAudit manhattanZonePricingExEventAudit = new ManhattanZonePricingExchangeEventAudit(manhattanZonePricingExEvent);
							manhattanZonePricingExEventAudit.setStatus("DELETED");
							manhattanZonePricingExEventAudit.setLastUpdatedDate(now);
							manhattanZonePricingExEventAudit.setLastUpdatedBy(username);
							manhattanZonePricingExEventAudits.add(manhattanZonePricingExEventAudit);
								
							manhattanZonePricingExEvents.add(manhattanZonePricingExEvent);
						}
						VipLastRowMiniExchangeEvent vipLastRowMiniExEvent = DAORegistry.getVipLastRowMiniExchangeEventDAO().getVipLastRowMiniExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(vipLastRowMiniExEvent != null ) {
							VipLastRowMiniExchangeEventAudit vipLastRowMiniExEventAudit = new VipLastRowMiniExchangeEventAudit(vipLastRowMiniExEvent);
							vipLastRowMiniExEventAudit.setStatus("DELETED");
							vipLastRowMiniExEventAudit.setLastUpdatedDate(now);
							vipLastRowMiniExEventAudit.setLastUpdatedBy(username);
							vipLastRowMiniExEventAudits.add(vipLastRowMiniExEventAudit);
								
							vipLastRowMiniExEvents.add(vipLastRowMiniExEvent);
						}
						TicketNetworkSSAccountExchangeEvent tnSpecialExEvent = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getTNSpecialExchangeEventByEventId(autoExchangeEventUpdateDTO.getEventId());
						if(tnSpecialExEvent != null ) {
							TicketNetworkSSAccountExchangeEventAudit tnSpecialExEventAudit = new TicketNetworkSSAccountExchangeEventAudit(tnSpecialExEvent);
							tnSpecialExEventAudit.setStatus("DELETED");
							tnSpecialExEventAudit.setLastUpdatedDate(now);
							tnSpecialExEventAudit.setLastUpdatedBy(username);
							tnSpecialExEventAudits.add(tnSpecialExEventAudit);
								
							tnSpecialExEvents.add(tnSpecialExEvent);
						}
					}
					
					DAORegistry.getMiniExchangeEventDAO().saveOrUpdateAll(miniExEvents);
					DAORegistry.getMiniExchangeEventAuditDAO().saveAll(miniExEventAudits);
					
					DAORegistry.getSgLastFiveRowExchangeEventDAO().saveOrUpdateAll(sgLastFiveRowExEvents);
					DAORegistry.getSgLastFiveRowExchangeEventAuditDAO().saveAll(sgLastFiveRowExEventAudits);
					
					DAORegistry.getVipMiniExchangeEventDAO().saveOrUpdateAll(vipMiniExEvents);
					DAORegistry.getVipMiniExchangeEventAuditDAO().saveAll(vipMiniExEventAudits);
					
					DAORegistry.getLastRowMiniExchangeEventDAO().saveOrUpdateAll(lastRowMiniExEvents);
					DAORegistry.getLastRowMiniExchangeEventAuditDAO().saveAll(lastRowMiniExEventAudits);
					
					DAORegistry.getZonePricingExchangeEventDAO().saveOrUpdateAll(zonePricingExEvents);
					DAORegistry.getZonePricingExchangeEventAuditDAO().saveAll(zonePricingExEventAudits);
					
					DAORegistry.getTixCityZonePricingExchangeEventDAO().saveOrUpdateAll(tixCityZonePricingExEvents);
					DAORegistry.getTixCityZonePricingExchangeEventAuditDAO().saveAll(tixCityZonePricingExEventAudits);
					
					DAORegistry.getManhattanZonePricingExchangeEventDAO().saveOrUpdateAll(manhattanZonePricingExEvents);
					DAORegistry.getManhattanZonePricingExchangeEventAuditDAO().saveAll(manhattanZonePricingExEventAudits);
					
					DAORegistry.getLarryLastExchangeEventDAO().saveOrUpdateAll(larryLastExEvents);
					DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(larryLastExEventAudits);
					
					DAORegistry.getZoneLastRowMiniExchangeEventDAO().saveOrUpdateAll(zoneLastRowMiniExEvents);
					DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().saveAll(zoneLastRowMiniExEventAudits);
					
					DAORegistry.getZoneTicketsProcessorExchangeEventDAO().saveOrUpdateAll(zoneTicketsProcessorExEvents);
					DAORegistry.getZoneTicketsProcessorExchangeEventAuditDAO().saveAll(zoneTicketsProcessorExEventAudits);
					
					DAORegistry.getPresaleZoneTicketsExchangeEventDAO().saveOrUpdateAll(presaleZoneTicketsExEvents);
					DAORegistry.getPresaleZoneTicketsExchangeEventAuditDAO().saveAll(presaleZoneTicketsExEventAudits);
					
					DAORegistry.getPresaleAutoCatExchangeEventDAO().saveOrUpdateAll(presaleAutoCatExEvents);
					DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().saveAll(presaleAutoCatExEventAudits);
					
					DAORegistry.getAutoCats96ExchangeEventDAO().saveOrUpdateAll(autoCats96ExEvents);
					DAORegistry.getAutoCats96ExchangeEventAuditDAO().saveAll(autoCats96ExEventAudits);
					
					DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().saveOrUpdateAll(tnSpecialExEvents);
					DAORegistry.getTicketNetworkSSAccountExchangeEventAuditDAO().saveAll(tnSpecialExEventAudits);
					
					DAORegistry.getVipLastRowMiniExchangeEventDAO().saveOrUpdateAll(vipLastRowMiniExEvents);
					DAORegistry.getVipLastRowMiniExchangeEventAuditDAO().saveAll(vipLastRowMiniExEventAudits);
					
					DAORegistry.getTmatInduxEventDetailsDAO().updateAll(tmatInduxEventDetails);
					DAORegistry.getTmatInduxEventDetailsAuditDAO().saveAll(tmatInduxEventDetailAudits);
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Error in AutoExchange Event Updater process.");
				throw e;
			}
	}
	
}