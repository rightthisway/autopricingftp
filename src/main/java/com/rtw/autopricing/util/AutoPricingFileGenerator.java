package com.rtw.autopricing.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZones;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.ftp.data.SeatGeekUploadCats;
import com.rtw.autopricing.util.mail.EmailUtil;

public class AutoPricingFileGenerator extends QuartzJobBean implements StatefulJob{

	private static String folderPath;
	public static final int DEFAULT_TIMEOUT = 30000;
	private static String mailToList;
	
	public static final String getFolderPath() {
		return folderPath;
	}

	public final void setFolderPath(String folderPath) {
		AutoPricingFileGenerator.folderPath = folderPath;
	}

	public static final String getMailToList() {
		return mailToList;
	}

	public final void setMailToList(String mailToList) {
		AutoPricingFileGenerator.mailToList = mailToList;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		
		generateAutoPricingFiles();
		
	}

	public static String miniCatsVividCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";

		try {
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			}
		
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getMiniCatsVividCsv(autopricingSettings);
			
			StringBuilder sbVividSeats = new StringBuilder();
			sbVividSeats.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,InHandDate,EDelivery");
			sbVividSeats.append("\n");
			
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && excludeZones.contains((data[17].toString()).toUpperCase())) {//tmatZone
							continue;
						}
					}
					
					sbVividSeats.append(data[0]+",");
					
					if(data[1] != null){
						sbVividSeats.append(((String) data[1]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[2] != null){
						sbVividSeats.append(((String) data[2]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[3] != null){
						sbVividSeats.append(data[3]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[4] != null){
						sbVividSeats.append(data[4]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[5] != null){
						sbVividSeats.append(data[5]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[6] != null){
						sbVividSeats.append(((String) data[6]).replaceAll("[,.]", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[7] != null){
						sbVividSeats.append(((String) data[7]).replaceAll("[,.]", " ")+",");
					}else{
						sbVividSeats.append(",");
					}					
					
					sbVividSeats.append(data[8]+",");
					sbVividSeats.append(data[9]+",");
					
					if(data[10] != null){
						sbVividSeats.append(((String) data[10]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[11] != null){
						sbVividSeats.append(data[11]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[12] != null){
						sbVividSeats.append(data[12]+",");
					}else{
						sbVividSeats.append(",");
					}
					if(data[13] != null){
						sbVividSeats.append(data[13]+",");
					}else{
						sbVividSeats.append(",");
					}
					if(data[14] != null){
						Integer ticketType = (Integer) data[14];
						if(ticketType == 1 || ticketType == 9)
							sbVividSeats.append(1);
						else
							sbVividSeats.append(0);
					}else{
						sbVividSeats.append(0);
					}				
					
					sbVividSeats.append("\n");
					
				}
			}
		
			File file = new File(folderPath+project+"\\"+project+"vividseatsminicats.csv");
			 
			//System.out.println("Path is -> "+folderPath+project+"\\"+project+"vividseatsminicats.csv");
			 if(file.exists()){
				 SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				 generateBackupFile(file, folderPath+project+"\\backup\\"+project+"vividseatsminicats-" + df.format(new Date())+".csv");
				
				 //file.renameTo(new File(folderPath+project+"\\backup\\"+project+"vividseatsminicats-" + df.format(new Date())+".csv"));
			 }
			 
			if (!file.exists())
				file.createNewFile();
			
			FileWriter vividSeatWriter = new FileWriter(file);				
			vividSeatWriter.append(sbVividSeats.toString());
			vividSeatWriter.flush();
			vividSeatWriter.close();
			
			message = project+"vividseatsminicats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = project+"vividseatsminicats.csv" + " - Not generated due to exception";
			return message;
		}
			
	}
	
	
	
	public static String miniCatscoreBigCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";
		String fileNamePath = "";
		try {
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
				fileNamePath = "ROTscorebigminicats";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
				fileNamePath = "scorebig\\Manhattanscorebigminicats";
			}
			
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getScoreBigCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();
			sbOtherExchange.append("Edit,EventName,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[14] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[14]);
						if(excludeZones == null && data[15] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[15]);
						}
					}
					if(excludeZones != null) {
						if(data[16] != null && excludeZones.contains((data[16].toString()).toUpperCase())) {//tmatZone
							continue;
						}
					}
					
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\"+fileNamePath+".csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\"+fileNamePath+"-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = fileNamePath+".csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = fileNamePath+".csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String miniCatFanXchangeCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";
		
		try {
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getFanXchangeCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();
			sbOtherExchange.append("Edit,EventName,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\fanxchange\\"+project+"fanxchangeminicats.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\fanxchange\\"+project+"fanxchangeminicats-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\fanxchange\\"+project+"fanxchangeminicats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\fanxchange\\"+project+"fanxchangeminicats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String miniCatsTicketCityCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";
		
		try {
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}
			
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getTicketCityCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();

			sbOtherExchange.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketID,Edelivery_ind,InHandDate,Instant,SPEC");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && excludeZones.contains((data[17].toString()).toUpperCase())) {//tmatZone
							continue;
						}
					}
					
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					if(data[13] != null){
						Integer ticketType = (Integer) data[13];
						if(ticketType == 1 || ticketType == 9)
							sbOtherExchange.append("Y,");
						else
							sbOtherExchange.append("N,");
					}else{
						sbOtherExchange.append("N,");
					}
					if(data[14] != null){//InhandDate
						sbOtherExchange.append(data[14]+",");
					}else{
						sbOtherExchange.append(",");
					}
					sbOtherExchange.append(",");
					sbOtherExchange.append(",");
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\ticketcity\\"+project+"ticketcityminicats.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\ticketcity\\"+project+"ticketcityminicats-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\ticketcity\\"+project+"ticketcityminicats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\ticketcity\\"+project+"ticketcityminicats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String seatgeekLastFiveRowCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "RTW";
		
		try {
			/*if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}*/
			
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getSeatGeekLastFiveRowCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();

			sbOtherExchange.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketID,InHandDate,Edelivery_ind,Instant");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && excludeZones.contains((data[17].toString()).toUpperCase())) {//tmatZone
							continue;
						}
					}
					
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					if(data[13] != null){//InhandDate
						sbOtherExchange.append(data[13]+",");
					}else{
						sbOtherExchange.append(",");
					}
					/*if(data[14] != null){//Edelivery_ind
						Integer ticketType = (Integer) data[14];
						if(ticketType == 1 || ticketType == 9)
							sbOtherExchange.append("Y,");
						else
							sbOtherExchange.append("N,");
					}else{
						sbOtherExchange.append("N,");
					}*/
					sbOtherExchange.append("Y,");//Edelivery_ind
					sbOtherExchange.append("N,");//Instant
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\seatgeek\\"+project+"seatgeeklastfiverowcats.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\seatgeek\\"+project+"seatgeeklastfiverowcats-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\seatgeek\\"+project+"seatgeeklastfiverowcats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\seatgeek\\"+project+"seatgeeklastfiverowcats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static List<SeatGeekUploadCats> getUpdatedSeatGeekUploadCatsData(AutopricingSettings autopricingSettings) throws Exception {
		
		
		try {
	
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			
			AutopricingProduct miniProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MiniCats");
			AutopricingExchange miniExchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("SeatGeek");
			AutopricingSettings miniApSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(autopricingSettings.getBrokerId(), miniProduct.getId(), miniExchange.getId());
			List<SeatGeekUploadCats> catsList = DAORegistry.getQueryManagerDAO().getSeatGeekLastFiveRowsWithMinicats(autopricingSettings,miniApSettings);
			
			//List<SeatGeekUploadCats> catsList = DAORegistry.getQueryManagerDAO().getSeatGeekLastFiveRows(autopricingSettings);
			Map<Integer,SeatGeekUploadCats> catTixMap = new HashMap<Integer, SeatGeekUploadCats>();
			boolean isFirstRecord = true;
			for (SeatGeekUploadCats sgUploadCats : catsList) {
				//Tamil : add $1 to first record to make minimam one recod should be updated in CSV.
				if(isFirstRecord) {
					sgUploadCats.setPrice(sgUploadCats.getPrice()+1);
					DAORegistry.getSgLastFiveRowCategoryTicketDAO().updatePriceById(sgUploadCats.getCatTicketId(),sgUploadCats.getPrice());
					isFirstRecord = false;
				}
				catTixMap.put(sgUploadCats.getCatTicketId(), sgUploadCats);
			}
			List<SeatGeekUploadCats> catsFromDBList = DAORegistry.getSeatGeekUploadCatsDAO().getAllActiveSeatGeekUploadCats();
			Map<Integer,SeatGeekUploadCats> catTixMapFromDB = new HashMap<Integer, SeatGeekUploadCats>();
			for (SeatGeekUploadCats sgCatsDB : catsFromDBList) {
				catTixMapFromDB.put(sgCatsDB.getCatTicketId(), sgCatsDB);
			}
			List<SeatGeekUploadCats> sgCatsList = new ArrayList<SeatGeekUploadCats>();
			//List<SeatGeekUploadCats> toBeDeleteList = new ArrayList<SeatGeekUploadCats>();
			List<Integer> keys = new ArrayList<Integer>(catTixMap.keySet());
			for (Integer key : keys) {
				SeatGeekUploadCats dbCats = catTixMapFromDB.remove(key);
				SeatGeekUploadCats cats = catTixMap.remove(key);
				
				//Tamil : skip excluded zone tickets
				Set<String> excludeZones = null;
				excludeZones = excludeEventZoneMap.get(cats.getEventId());
				if(excludeZones == null && cats.getVenueCategoryId() != null) {//venuecategoryid
						excludeZones = excludeVenueCategoryZoneMap.get(cats.getVenueCategoryId());
				}
				if(excludeZones != null) {
					if(cats.getTmatZone() != null && excludeZones.contains(cats.getTmatZone().toUpperCase())) {//tmatZone
						continue;
					}
				}
				if(dbCats != null) {
					if(!dbCats.getPrice().equals(cats.getPrice()) ||
							!dbCats.getQuantity().equals(cats.getQuantity()) ||
							!dbCats.getSection().equals(cats.getSection()) ||
							!dbCats.getRow().equals(cats.getRow()) ||
							//!dbCats.getInHandDate().equals(cats.getInHandDate()) ||
							//!dbCats.getVenueName().equals(cats.getVenueName()) ||
							//!dbCats.getEventDate().equals(cats.getEventDate()) ||
							!dbCats.getEventId().equals(cats.getEventId()) //||
							//!dbCats.getSeatFrom().equals(cats.getSeatFrom()) ||
							//!dbCats.getSeatThru().equals(cats.getSeatThru()) ||
							//!dbCats.getNotes().equals(cats.getNotes()) ||
							//!dbCats.getEdit().equals(cats.getEdit())
							) {
					/*} else {
						
						if(dbCats.getShippingMethodId() == null || cats.getShippingMethodId() == null ||
								!dbCats.getShippingMethodId().equals(cats.getShippingMethodId()) ||
								dbCats.getEventTime() == null || cats.getEventTime() == null ||
								!dbCats.getEventTime().equals(cats.getEventTime()) ||
								dbCats.getEventDate() == null || cats.getEventDate() == null ||
								!dbCats.getEventDate().equals(cats.getEventDate())) {
						}
						
					}*/
						
						dbCats.setPrice(cats.getPrice());
						dbCats.setQuantity(cats.getQuantity());
						dbCats.setSection(cats.getSection());
						dbCats.setRow(cats.getRow());
						dbCats.setShippingMethodId(cats.getShippingMethodId());
						//dbCats.setInHandDate(cats.getInHandDate());
						//dbCats.setVenueName(cats.getVenueName());
						//dbCats.setEventDate(cats.getEventDate());
						//dbCats.setEventTime(cats.getEventTime());
						dbCats.setEventId(cats.getEventId());
						dbCats.setInternalNotes(cats.getInternalNotes());
						//dbCats.setSeatFrom(cats.getSeatFrom());
						//dbCats.setSeatThru(cats.getSeatThru());
						//dbCats.setNotes(cats.getSeatThru());
						//dbCats.setEdit(cats.getSeatThru());
						
						dbCats.setLastUpdated(new Date());
						sgCatsList.add(dbCats);
					}
				} else {
					cats.setStatus("ACTIVE");
					cats.setCreatedDate(new Date());
					cats.setLastUpdated(new Date());
					sgCatsList.add(cats);
				}
			}
			
			for (SeatGeekUploadCats sgUploadCat : catTixMapFromDB.values()) {
				sgUploadCat.setStatus("DELETED");
				sgUploadCat.setLastUpdated(new Date());
				sgCatsList.add(sgUploadCat);
			}
			//DAORegistry.getSeatGeekUploadCatsDAO().deleteAll(new ArrayList<SeatGeekUploadCats>(catTixMapFromDB.values()));
			DAORegistry.getSeatGeekUploadCatsDAO().saveOrUpdateAll(sgCatsList);
			
			return sgCatsList;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		
	}
	
	public static String seatgeekLastFiveRowCSVFromUploadCats(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "RTW";
		
		try {
			
			getUpdatedSeatGeekUploadCatsData(autopricingSettings); 
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getSeatGeekUploadCatsCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();

			sbOtherExchange.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketID,InHandDate,Edelivery_ind,Instant");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					/*Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && excludeZones.contains((data[17].toString()).toUpperCase())) {//tmatZone
							continue;
						}
					}*/
					
					sbOtherExchange.append(data[0]+",");//Edit
					
					if(data[1] != null){//Event
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){//Venue
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){//EventDate
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){//EventTime
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){//quantity
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){//Section
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){//row
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");//seatFrom
					sbOtherExchange.append(data[9]+",");//seatThru
					
					if(data[10] != null){//notes
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){//cost
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){//ticketId
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					if(data[13] != null){//InhandDate
						sbOtherExchange.append(data[13]+",");
					}else{
						sbOtherExchange.append(",");
					}
					/*if(data[14] != null){//Edelivery_ind
						Integer ticketType = (Integer) data[14];
						if(ticketType == 1 || ticketType == 9)
							sbOtherExchange.append("Y,");
						else
							sbOtherExchange.append("N,");
					}else{
						sbOtherExchange.append("N,");
					}*/
					sbOtherExchange.append("Y,");//Edelivery_ind
					sbOtherExchange.append("N,");//Instant
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\seatgeek\\SGKCats.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				//generateBackupFile(file, folderPath+project+"\\backup\\seatgeek\\SGKCats-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\seatgeek\\SGKCats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\seatgeek\\SGKCats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String seatgeekLastFiveRowCSVfromTN(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "RTW";
		
		try {
			/*if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}*/
			
			/*Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}*/
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getSeatGeekLastFiveRowCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();

			sbOtherExchange.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketID,InHandDate,Edelivery_ind,Instant");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					/*Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && excludeZones.contains((data[17].toString()).toUpperCase())) {//tmatZone
							continue;
						}
					}*/
					
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					if(data[13] != null){//InhandDate
						sbOtherExchange.append(data[13]+",");
					}else{
						sbOtherExchange.append(",");
					}
					/*if(data[14] != null){//Edelivery_ind
						Integer ticketType = (Integer) data[14];
						if(ticketType == 1 || ticketType == 9)
							sbOtherExchange.append("Y,");
						else
							sbOtherExchange.append("N,");
					}else{
						sbOtherExchange.append("N,");
					}*/
					sbOtherExchange.append("Y,");//Edelivery_ind
					sbOtherExchange.append("N,");//Instant
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\seatgeek\\"+project+"tnseatgeeklastfiverowcats.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\seatgeek\\"+project+"tnseatgeeklastfiverowcats-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\seatgeek\\"+project+"tnseatgeeklastfiverowcats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\seatgeek\\"+project+"tnseatgeeklastfiverowcats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String miniCatsSportsTicketCityCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";
		
		try {
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getTicketCitySportsCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();

			sbOtherExchange.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketID,Edelivery_ind,InHandDate,Instant,SPEC");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && excludeZones.contains((data[17].toString()).toUpperCase())) {//tmatZone
							continue;
						}
					}
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					if(data[13] != null){
						Integer ticketType = (Integer) data[13];
						if(ticketType == 1 || ticketType == 9)
							sbOtherExchange.append("Y,");
						else
							sbOtherExchange.append("N,");
					}else{
						sbOtherExchange.append("N,");
					}
					if(data[14] != null){//InhandDate
						sbOtherExchange.append(data[14]+",");
					}else{
						sbOtherExchange.append(",");
					}
					sbOtherExchange.append(",");
					sbOtherExchange.append(",");
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\ticketcity\\"+project+"ticketcityminicatssports.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\ticketcity\\"+project+"ticketcityminicatssports-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\ticketcity\\"+project+"ticketcityminicatssports.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\ticketcity\\"+project+"ticketcityminicatssports.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String vipMiniCatscoreBigCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";
		try {
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getVipminicatsScoreBigCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();
			sbOtherExchange.append("Edit,EventName,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\scorebig\\"+project+"scorebigvipminicats.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\scorebig\\"+project+"scorebigvipminicats-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\scorebig\\"+project+"scorebigvipminicats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\scorebig\\"+project+"scorebigvipminicats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	public static String vipMiniCatFanXchangeCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";
		
		try {
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getVipMinicatsFanXchangeCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();
			sbOtherExchange.append("Edit,EventName,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\fanxchange\\"+project+"fanxchangevipminicats.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\fanxchange\\"+project+"fanxchangevipminicats-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\fanxchange\\"+project+"fanxchangevipminicats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\fanxchange\\"+project+"fanxchangevipminicats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String vipMiniCatsTicketCityCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";
		
		try {
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(autopricingSettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}
			
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductIdandBrokerId(autopricingSettings.getProductId(),autopricingSettings.getBrokerId());
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getVipMinicatsTicketCityCsv(autopricingSettings);
			StringBuilder sbOtherExchange = new StringBuilder();
			sbOtherExchange.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketID,Edelivery_ind,InHandDate,Instant,SPEC");
			sbOtherExchange.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && excludeZones.contains((data[17].toString()).toUpperCase())) {//tmatZone
							continue;
						}
					}
					
					sbOtherExchange.append(data[0]+",");
					
					if(data[1] != null){
						sbOtherExchange.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[2] != null){
						sbOtherExchange.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[3] != null){
						sbOtherExchange.append(data[3]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[4] != null){
						sbOtherExchange.append(data[4]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[5] != null){
						sbOtherExchange.append(data[5]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[6] != null){
						sbOtherExchange.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[7] != null){
							sbOtherExchange.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}					
					
					sbOtherExchange.append(data[8]+",");
					sbOtherExchange.append(data[9]+",");
					
					if(data[10] != null){
						sbOtherExchange.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[11] != null){
						sbOtherExchange.append(data[11]+",");
					}else{
						sbOtherExchange.append(",");
					}
					
					if(data[12] != null){
						sbOtherExchange.append(data[12]+",");
					}else{
						sbOtherExchange.append(",");
					}
					if(data[13] != null){
						Integer ticketType = (Integer) data[13];
						if(ticketType == 1 || ticketType == 9)
							sbOtherExchange.append("Y,");
						else
							sbOtherExchange.append("N,");
					}else{
						sbOtherExchange.append("N,");
					}
					if(data[14] != null){//InhandDate
						sbOtherExchange.append(data[14]+",");
					}else{
						sbOtherExchange.append(",");
					}
					sbOtherExchange.append(",");
					sbOtherExchange.append(",");
					
					sbOtherExchange.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\ticketcity\\"+project+"ticketcityvipminicats.csv");
			
			if(file.exists()){
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\ticketcity\\"+project+"ticketcityvipminicats-" + df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigminicats-" + df.format(new Date())+".csv"));
			}
			
		    if (!file.exists())
		    	file.createNewFile();
		    
			FileWriter otherExchangeWriter = new FileWriter(file);
			otherExchangeWriter.append(sbOtherExchange.toString());
			otherExchangeWriter.flush();
			otherExchangeWriter.close();
			
			message = "\\ticketcity\\"+project+"ticketcityvipminicats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\ticketcity\\"+project+"ticketcityvipminicats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	
	
	public static String larryAndAlLastRowMiniCatsVividSeatCSV(AutopricingSettings larrySettings,AutopricingSettings lastrowSettings,
			AutopricingSettings zoneLastrowSettings) {
		String message = "";
		String project = "";
		String fileName = "";
	
		try {
			if(larrySettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
				fileName = "vividseatslarrytheater";
			}
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			String productIdStr = larrySettings.getProductId()+","+lastrowSettings.getProductId()+","+zoneLastrowSettings.getProductId();

			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByBrokerIdandProductIds(larrySettings.getBrokerId(),productIdStr);
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getProductId()+"_"+excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByBrokerIdandProductIds(larrySettings.getBrokerId(),productIdStr);
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getProductId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getLarryAndAllLastRowVividSeatCsv(larrySettings,lastrowSettings,zoneLastrowSettings);
			StringBuilder sbVividSeats = new StringBuilder();
			sbVividSeats.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,InHandDate,EDelivery");
			sbVividSeats.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && data[18] != null) {//tmatZone(17),productId(18)
							String key = (data[18].toString())+"_"+(data[17].toString()).toUpperCase();
							if(excludeZones.contains(key)) {
								continue;								
							}
						}
					}
					
					sbVividSeats.append(data[0]+",");
					
					if(data[1] != null){
						sbVividSeats.append(((String) data[1]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[2] != null){
						sbVividSeats.append(((String) data[2]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[3] != null){
						sbVividSeats.append(data[3]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[4] != null){
						sbVividSeats.append(data[4]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[5] != null){
						sbVividSeats.append(data[5]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[6] != null){
						sbVividSeats.append(((String) data[6]).replaceAll("[,.]", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[7] != null){
						sbVividSeats.append(((String) data[7]).replaceAll("[,.]", " ")+",");
					}else{
						sbVividSeats.append(",");
					}					
					
					sbVividSeats.append(data[8]+",");
					sbVividSeats.append(data[9]+",");
					
					if(data[10] != null){
						sbVividSeats.append(((String) data[10]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[11] != null){
						sbVividSeats.append(data[11]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[12] != null){
						sbVividSeats.append(data[12]+",");
					}else{
						sbVividSeats.append(",");
					}
					if(data[13] != null){
						sbVividSeats.append(data[13]+",");
					}else{
						sbVividSeats.append(",");
					}				
					if(data[14] != null){
						Integer ticketType = (Integer) data[14];
						if(ticketType == 1 || ticketType == 9)
							sbVividSeats.append(1);
						else
							sbVividSeats.append(0);
					}else{
						sbVividSeats.append(0);
					}
					
					sbVividSeats.append("\n");
					
				}
			}
			File file = new File(folderPath+project+"\\"+project+fileName+".csv");
			
			 if(file.exists()){
				 SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				 generateBackupFile(file, folderPath+project+"\\backup\\"+project+fileName+df.format(new Date())+".csv");
				 
				 //file.renameTo(new File(folderPath+project+"\\backup\\"+project+fileName+df.format(new Date())+".csv"));
			 }
			 
			if (!file.exists())
				file.createNewFile();
			
			FileWriter vividSeatWriter = new FileWriter(file);				
			vividSeatWriter.append(sbVividSeats.toString());
			vividSeatWriter.flush();
			vividSeatWriter.close();
			
			message = project+fileName+".csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = project+fileName+".csv" + " - Not generated due to exception";
			return message;
		}
	}
	/*public static String lastRowMiniCatsVividCSV(Integer brokerId,String project) {
		String message = "";
		List<Object[]> list = DAORegistry.getQueryManagerDAO().getLarryAndAllLastRowVividSeatCsv(brokerId);
		StringBuilder sbVividSeats = new StringBuilder();
		sbVividSeats.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,InHandDate,EDelivery");
		sbVividSeats.append("\n");
		if(list != null && !list.isEmpty()){
			for(Object[] data : list){
				
				sbVividSeats.append(data[0]+",");
				
				if(data[1] != null){
					sbVividSeats.append(((String) data[1]).replaceAll(",", " ")+",");
				}else{
					sbVividSeats.append(",");
				}
				
				if(data[2] != null){
					sbVividSeats.append(((String) data[2]).replaceAll(",", " ")+",");
				}else{
					sbVividSeats.append(",");
				}
				
				if(data[3] != null){
					sbVividSeats.append(data[3]+",");
				}else{
					sbVividSeats.append(",");
				}
				
				if(data[4] != null){
					sbVividSeats.append(data[4]+",");
				}else{
					sbVividSeats.append(",");
				}
				
				if(data[5] != null){
					sbVividSeats.append(data[5]+",");
				}else{
					sbVividSeats.append(",");
				}
				
				if(data[6] != null){
					sbVividSeats.append(((String) data[6]).replaceAll("[,.]", " ")+",");
				}else{
					sbVividSeats.append(",");
				}
				
				if(data[7] != null){
					sbVividSeats.append(((String) data[7]).replaceAll("[,.]", " ")+",");
				}else{
					sbVividSeats.append(",");
				}					
				
				sbVividSeats.append(data[8]+",");
				sbVividSeats.append(data[9]+",");
				
				if(data[10] != null){
					sbVividSeats.append(((String) data[10]).replaceAll(",", " ")+",");
				}else{
					sbVividSeats.append(",");
				}
				
				if(data[11] != null){
					sbVividSeats.append(data[11]+",");
				}else{
					sbVividSeats.append(",");
				}
				
				if(data[12] != null){
					sbVividSeats.append(data[12]+",");
				}else{
					sbVividSeats.append(",");
				}
				if(data[13] != null){
					sbVividSeats.append(data[13]+",");
				}else{
					sbVividSeats.append(",");
				}
				if(data[14] != null){
					Integer ticketType = (Integer) data[14];
					if(ticketType == 1 || ticketType == 9)
						sbVividSeats.append(1);
					else
						sbVividSeats.append(0);
				}else{
					sbVividSeats.append(0);
				}
				
				sbVividSeats.append("\n");
				
			}
			try {
				 File file = new File(folderPath+project+"\\"+project+"vividseatslastrowmiinicats.csv");
				
				 if(file.exists()){
					 SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
					 generateBackupFile(file, folderPath+project+"\\backup\\"+project+"vividseatslastrowmiinicats"+df.format(new Date())+".csv");
					 
					 //file.renameTo(new File(folderPath+project+"\\backup\\"+project+"vividseatslastrowmiinicats"+df.format(new Date())+".csv"));
				 }
				 
				if (!file.exists())
					file.createNewFile();
				
				FileWriter vividSeatWriter = new FileWriter(file);				
				vividSeatWriter.append(sbVividSeats.toString());
				vividSeatWriter.flush();
				vividSeatWriter.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				message = project+"vividseatslastrowmiinicats.csv" + " - Not generated due to exception";
				return message;
			}
			message = project+"vividseatslastrowmiinicats.csv" + " - Generated fine";
			return message;
		}
		else {
			message = project+"vividseatslastrowmiinicats.csv" + " - Not generated because either empty or zero record";
			return message;
		}
	}*/
	
	public static String larryLastAndAllLastRowMiniCatsScoreBigCSV(AutopricingSettings larrySettings,AutopricingSettings lastrowSettings,
			AutopricingSettings zoneLastrowSettings){
		String message = "";
		String project = "";
		String fileNamePath = "";
		
		try {
			if(larrySettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
				fileNamePath = "ROTscorebigcats";
			} else if(larrySettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
				fileNamePath = "scorebig\\Manhattanscorebiglastrowminicats";
			}
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			String productIdStr = larrySettings.getProductId()+","+lastrowSettings.getProductId()+","+zoneLastrowSettings.getProductId();

			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByBrokerIdandProductIds(larrySettings.getBrokerId(),productIdStr);
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getProductId()+"_"+excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByBrokerIdandProductIds(larrySettings.getBrokerId(),productIdStr);
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getProductId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getLarryAndAllLastRowScorebigCsv(larrySettings,lastrowSettings,zoneLastrowSettings);
			StringBuilder sb = new StringBuilder();
			sb.append("Edit,EventName,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId");
			sb.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					String sections = (String) data[6];
					
					if(sections != null && !sections.isEmpty()){
						sections = sections.toLowerCase();
						if(sections.contains(" or ") || sections.contains("-")){
							continue;
						}
					}
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[14] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[14]);
						if(excludeZones == null && data[15] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[15]);
						}
					}
					if(excludeZones != null) {
						if(data[16] != null && data[17] != null) {//tmatZone(16),productId(17)
							String key = (data[17].toString())+"_"+(data[16].toString()).toUpperCase();
							if(excludeZones.contains(key)) {
								continue;								
							}
						}
					}
					
					sb.append(data[0]+",");
					
					if(data[1] != null)
						sb.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[2] != null)
						sb.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[3] != null)
						sb.append(data[3]+",");
					else
						sb.append(",");
					
					if(data[4] != null)
						sb.append(data[4]+",");
					else
						sb.append(",");
					
					if(data[5] != null)
						sb.append(data[5]+",");
					else
						sb.append(",");
					
					if(data[6] != null)
						sb.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					else
						sb.append(",");
					
					if(data[7] != null){
						sb.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sb.append(",");
					}
					
					sb.append(data[8]+",");
					sb.append(data[9]+",");
					
					if(data[10] != null)
						sb.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[11] != null)
						sb.append(data[11]+",");
					else
						sb.append(",");
					
					if(data[12] != null)
						sb.append(data[12]+",");
					else
						sb.append(",");
					
					sb.append("\n");
					
				}
			}
			
			File file = new File(folderPath+project+"\\"+fileNamePath+".csv");
				
			if(file.exists()) {
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\"+fileNamePath+"-"+df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigcats-"+df.format(new Date())+".csv"));
			}
			
			if (!file.exists())
				file.createNewFile();
			
			FileWriter writer = new FileWriter(file);
			writer.append(sb.toString());
			writer.flush();
		    writer.close();

		    message = fileNamePath+".csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
			message = fileNamePath+".csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String larryLastAndAllLastRowMiniCatsTicketCityCSV(AutopricingSettings larrySettings,AutopricingSettings lastrowSettings,
			AutopricingSettings zoneLastrowSettings){
		String message = "";
		String project = "";
		
		try {
			if(larrySettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(larrySettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}
			
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			String productIdStr = larrySettings.getProductId()+","+lastrowSettings.getProductId()+","+zoneLastrowSettings.getProductId();

			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByBrokerIdandProductIds(larrySettings.getBrokerId(),productIdStr);
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getProductId()+"_"+excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByBrokerIdandProductIds(larrySettings.getBrokerId(),productIdStr);
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getProductId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getLarryAndAllLastRowTicketCityCsv(larrySettings,lastrowSettings,zoneLastrowSettings);
			StringBuilder sb = new StringBuilder();
			sb.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketID,Edelivery_ind,InHandDate,Instant,SPEC");
			sb.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					String sections = (String) data[6];
					
					if(sections != null && !sections.isEmpty()){
						sections = sections.toLowerCase();
						if(sections.contains(" or ") || sections.contains("-")){
							continue;
						}
					}
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[15] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[15]);
						if(excludeZones == null && data[16] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[16]);
						}
					}
					if(excludeZones != null) {
						if(data[17] != null && data[18] != null) {//tmatZone(17),productId(18)
							String key = (data[18].toString())+"_"+(data[17].toString()).toUpperCase();
							if(excludeZones.contains(key)) {
								continue;								
							}
						}
					}
					
					sb.append(data[0]+",");
					
					if(data[1] != null)
						sb.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[2] != null)
						sb.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[3] != null)
						sb.append(data[3]+",");
					else
						sb.append(",");
					
					if(data[4] != null)
						sb.append(data[4]+",");
					else
						sb.append(",");
					
					if(data[5] != null)
						sb.append(data[5]+",");
					else
						sb.append(",");
					
					if(data[6] != null)
						sb.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					else
						sb.append(",");
					
					if(data[7] != null){
						sb.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sb.append(",");
					}
					
					sb.append(data[8]+",");
					sb.append(data[9]+",");
					
					if(data[10] != null)
						sb.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[11] != null)
						sb.append(data[11]+",");
					else
						sb.append(",");
					
					if(data[12] != null)
						sb.append(data[12]+",");
					else
						sb.append(",");
					
					if(data[13] != null){
						Integer ticketType = (Integer) data[13];
						if(ticketType == 1 || ticketType == 9)
							sb.append("Y,");
						else
							sb.append("N,");
					}else{
						sb.append("N,");
					}
					if(data[14] != null){//InhandDate
						sb.append(data[14]+",");
					}else{
						sb.append(",");
					}
					sb.append(",");
					sb.append(",");
					
					sb.append("\n");
					
				}
			}
			
			File file = new File(folderPath+project+"\\ticketcity\\"+project+"ticketcitylastrowminicats.csv");
				
			if(file.exists()) {
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\ticketcity\\"+project+"ticketcitylastrowminicats-"+df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigcats-"+df.format(new Date())+".csv"));
			}
			
			if (!file.exists())
				file.createNewFile();
			
			FileWriter writer = new FileWriter(file);
			writer.append(sb.toString());
			writer.flush();
		    writer.close();

		    message = "\\ticketcity\\"+project+"ticketcitylastrowminicats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\ticketcity\\"+project+"ticketcitylastrowminicats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	public static String larryLastAndAllLastRowMiniCatsFanXchangeCSV(AutopricingSettings larrySettings,AutopricingSettings lastrowSettings,
			AutopricingSettings zoneLastrowSettings){
		String message = "";
		String project = "";
		
		try {
			if(larrySettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
			} else if(larrySettings.getBroker().getName().equals("Manhattan")) {
				project = "Manhattan";
			}
			Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			String productIdStr = larrySettings.getProductId()+","+lastrowSettings.getProductId()+","+zoneLastrowSettings.getProductId();

			List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByBrokerIdandProductIds(larrySettings.getBrokerId(),productIdStr);
			for (ExcludeEventZones excludeEventZone : excludeEventZones) {
				Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
				if(eventZones == null) {
					eventZones = new HashSet<String>();
				}
				eventZones.add(excludeEventZone.getProductId()+"_"+excludeEventZone.getZone().toUpperCase());
				excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
			}
			List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByBrokerIdandProductIds(larrySettings.getBrokerId(),productIdStr);
			for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
				Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
				if(venueCategoryZones == null) {
					venueCategoryZones = new HashSet<String>();
				}
				venueCategoryZones.add(excludeVenueCategoryZone.getProductId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
				excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
			}
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getLarryAndAllLastRowFanXchangeCsv(larrySettings,lastrowSettings,zoneLastrowSettings);
			StringBuilder sb = new StringBuilder();
			sb.append("Edit,EventName,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId");
			sb.append("\n");
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					String sections = (String) data[6];
					
					if(sections != null && !sections.isEmpty()){
						sections = sections.toLowerCase();
						if(sections.contains(" or ") || sections.contains("-")){
							continue;
						}
					}
					
					//Tamil : skip excluded zone tickets
					Set<String> excludeZones = null;
					if(data[14] != null ) {//eventId
						excludeZones = excludeEventZoneMap.get((Integer)data[14]);
						if(excludeZones == null && data[15] != null) {//venuecategoryid
							excludeZones = excludeVenueCategoryZoneMap.get((Integer)data[15]);
						}
					}
					if(excludeZones != null) {
						if(data[16] != null && data[17] != null) {//tmatZone(16),productId(17)
							String key = (data[17].toString())+"_"+(data[16].toString()).toUpperCase();
							if(excludeZones.contains(key)) {
								continue;								
							}
						}
					}
					
					sb.append(data[0]+",");
					
					if(data[1] != null)
						sb.append(((String) data[1]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[2] != null)
						sb.append(((String) data[2]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[3] != null)
						sb.append(data[3]+",");
					else
						sb.append(",");
					
					if(data[4] != null)
						sb.append(data[4]+",");
					else
						sb.append(",");
					
					if(data[5] != null)
						sb.append(data[5]+",");
					else
						sb.append(",");
					
					if(data[6] != null)
						sb.append(((String) data[6]).trim().replaceAll("[,.]", " ")+",");
					else
						sb.append(",");
					
					if(data[7] != null){
						sb.append(((String) data[7]).trim().replaceAll("[,.]", " ")+",");
					}else{
						sb.append(",");
					}
					
					sb.append(data[8]+",");
					sb.append(data[9]+",");
					
					if(data[10] != null)
						sb.append(((String) data[10]).trim().replaceAll(",", " ")+",");
					else
						sb.append(",");
					
					if(data[11] != null)
						sb.append(data[11]+",");
					else
						sb.append(",");
					
					if(data[12] != null)
						sb.append(data[12]+",");
					else
						sb.append(",");
					
					sb.append("\n");
					
				}
			}
			
			File file = new File(folderPath+project+"\\fanxchange\\"+project+"fanxchangelastrowminicats.csv");
				
			if(file.exists()) {
				SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				generateBackupFile(file, folderPath+project+"\\backup\\fanxchange\\"+project+"fanxchangelastrowminicats-"+df.format(new Date())+".csv");
				
				//file.renameTo(new File(folderPath+project+"\\backup\\"+project+"scorebigcats-"+df.format(new Date())+".csv"));
			}
			
			if (!file.exists())
				file.createNewFile();
			
			FileWriter writer = new FileWriter(file);
			writer.append(sb.toString());
			writer.flush();
		    writer.close();

		    message = "\\fanxchange\\"+project+"fanxchangelastrowminicats.csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
			message = "\\fanxchange\\"+project+"fanxchangelastrowminicats.csv" + " - Not generated due to exception";
			return message;
		}
	}
	
	private static void generateBackupFile(File sourceFile, String backupFilePath) throws Exception {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(sourceFile);
	        os = new FileOutputStream(new File(backupFilePath));
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    }
	    finally {
	        is.close();
	        os.close();
	    }
	}
	
	public void generateAutoPricingFiles(){

		String rotMessage = "Below ROT autopricing files located at S:\\Autopricing\\ROT are generated." + "\n";
		String manhattanMessage = "Below Manhattan autopricing files located at S:\\Autopricing\\Manhattan are generated." + "\n";
		String rtwMessage = "Below RTW autopricing files located at S:\\Autopricing\\RTW are generated." + "\t\n";

		try {
		Broker rotBroker = DAORegistry.getBrokerDAO().getBrokerByName("Reserve One Tickets");
		Broker manhattanBroker = DAORegistry.getBrokerDAO().getBrokerByName("Manhattan");
		Broker rtwBroker = DAORegistry.getBrokerDAO().getBrokerByName("Right This Way");
		
		try {
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SGLastFiveRow");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("SeatGeek");
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rtwBroker.getId(), product.getId(), exchange.getId());
			
			//rtwMessage += "\t" + seatgeekLastFiveRowCSVFromUploadCats(autopricingSettings) + "\n";
			System.out.println("SeatGeek file generated....."+new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Stoped all other file generation process
		boolean flag = false;
		if(flag) {
	
			try {
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SGLastFiveRow");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("SeatGeek");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rtwBroker.getId(), product.getId(), exchange.getId());
			//rtwMessage += "\t" + seatgeekLastFiveRowCSV(autopricingSettings) + "\n";
			System.out.println("SeatGeek file generated....."+new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("ManualCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), product.getId(), exchange.getId());
			rotMessage += "\t" + manualCatsVividCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MiniCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), product.getId(), exchange.getId());
			rotMessage += "\t" + miniCatsVividCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try{
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MiniCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Scorebig");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), product.getId(), exchange.getId());
			rotMessage += "\t" + miniCatscoreBigCSV(autopricingSettings) + "\n";
			
			//autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), product.getId(), exchange.getId());
			//manhattanMessage += "\t" + miniCatscoreBigCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*try {
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MiniCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("FanXchange");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), product.getId(), exchange.getId());
			manhattanMessage += "\t" + miniCatFanXchangeCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		try{
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MiniCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), product.getId(), exchange.getId());
			manhattanMessage += "\t" + miniCatsTicketCityCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try{
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MiniCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), product.getId(), exchange.getId());
			manhattanMessage += "\t" + miniCatsSportsTicketCityCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*try {
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("VIP MiniCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Scorebig");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), product.getId(), exchange.getId());
			manhattanMessage += "\t" + vipMiniCatscoreBigCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		/*try{
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("VIP MiniCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("FanXchange");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), product.getId(), exchange.getId());
			manhattanMessage += "\t" + vipMiniCatFanXchangeCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		try{
			
			AutopricingProduct product = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("VIP MiniCats");
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity");
			
			AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), product.getId(), exchange.getId());
			manhattanMessage += "\t" + vipMiniCatsTicketCityCSV(autopricingSettings) + "\n";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Scorebig");
			
			AutopricingProduct larryProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SS Account");
			AutopricingProduct lastRowProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("LastRow MiniCats");
			AutopricingProduct zoneLastRowProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("Zoned LastRow MiniCats");
			
			
			AutopricingSettings larryAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), larryProduct.getId(), exchange.getId());
			AutopricingSettings lastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), lastRowProduct.getId(), exchange.getId());
			AutopricingSettings zoneLastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), zoneLastRowProduct.getId(), exchange.getId());
			
			//To get larryLast and all LastrowMinicats with csv named as scorebigcats.csv
			rotMessage += "\t" + larryLastAndAllLastRowMiniCatsScoreBigCSV(larryAPSettings,lastRowAPSettings,zoneLastRowAPSettings) + "\n";
			
			larryAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), larryProduct.getId(), exchange.getId());
			lastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), lastRowProduct.getId(), exchange.getId());
			zoneLastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), zoneLastRowProduct.getId(), exchange.getId());
			
			//To get larryLast and all LastrowMinicats with csv named as scorebigcats.csv
			manhattanMessage += "\t" + larryLastAndAllLastRowMiniCatsScoreBigCSV(larryAPSettings,lastRowAPSettings,zoneLastRowAPSettings) + "\n";
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("FanXchange");
			
			AutopricingProduct larryProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SS Account");
			AutopricingProduct lastRowProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("LastRow MiniCats");
			AutopricingProduct zoneLastRowProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("Zoned LastRow MiniCats");
			
			AutopricingSettings larryAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), larryProduct.getId(), exchange.getId());
			AutopricingSettings lastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), lastRowProduct.getId(), exchange.getId());
			AutopricingSettings zoneLastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), zoneLastRowProduct.getId(), exchange.getId());
			
			//To get larryLast and all LastrowMinicats with csv named as fanxchangecats.csv
			manhattanMessage += "\t" + larryLastAndAllLastRowMiniCatsFanXchangeCSV(larryAPSettings,lastRowAPSettings,zoneLastRowAPSettings) + "\n";
			
			
			/*larryAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), larryProduct.getId(), exchange.getId());
			lastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), lastRowProduct.getId(), exchange.getId());
			zoneLastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), zoneLastRowProduct.getId(), exchange.getId());
			
			//To get larryLast and all LastrowMinicats with csv named as scorebigcats.csv
			rotMessage += "\t" + larryLastAndAllLastRowMiniCatsScoreBigCSV(larryAPSettings,lastRowAPSettings,zoneLastRowAPSettings) + "\n";*/
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		try {
			
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity");
			
			AutopricingProduct larryProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SS Account");
			AutopricingProduct lastRowProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("LastRow MiniCats");
			AutopricingProduct zoneLastRowProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("Zoned LastRow MiniCats");
			
			AutopricingSettings larryAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), larryProduct.getId(), exchange.getId());
			AutopricingSettings lastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), lastRowProduct.getId(), exchange.getId());
			AutopricingSettings zoneLastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(manhattanBroker.getId(), zoneLastRowProduct.getId(), exchange.getId());
			
			//To get larryLast and all LastrowMinicats with csv named as scorebigcats.csv
			manhattanMessage += "\t" + larryLastAndAllLastRowMiniCatsTicketCityCSV(larryAPSettings,lastRowAPSettings,zoneLastRowAPSettings) + "\n";
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		try{
			
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats");
			
			AutopricingProduct larryProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SS Account");
			AutopricingProduct lastRowProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("LastRow MiniCats");
			AutopricingProduct zoneLastRowProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("Zoned LastRow MiniCats");
			
			AutopricingSettings larryAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), larryProduct.getId(), exchange.getId());
			AutopricingSettings lastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), lastRowProduct.getId(), exchange.getId());
			AutopricingSettings zoneLastRowAPSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(rotBroker.getId(), zoneLastRowProduct.getId(), exchange.getId());
			
			//To get larryLast and all LastrowMinicats with csv named as vividseatslarrytheater.csv			
			rotMessage += "\t" + larryAndAlLastRowMiniCatsVividSeatCSV(larryAPSettings,lastRowAPSettings,zoneLastRowAPSettings) + "\n";
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		}
		
		String messageText = rtwMessage+ "\n \n \n" +rotMessage+ "\n \n \n" + manhattanMessage;
		String subject = "Autopricing file generation job completed successfully.";
		
		if(messageText.contains("Not generated due to exception")) {
			subject = "Autopricing file generation job failed.";
		}

		 try {
			//sendMailNow(subject, messageText);
			
			//mailManager.sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth, username, password, emailFrom, emailFrom, toAddress, subject, null, null, mimeType);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		} catch(Exception e) {
			System.err.println("Error in Autopricing File Generation Process");
			e.printStackTrace();
		}
	}
	
	
	public static String manualCatsVividCSV(AutopricingSettings autopricingSettings) {
		String message = "";
		String project = "";
		String fileName = "";
		try {
			
		
			if(autopricingSettings.getBroker().getName().equals("Reserve One Tickets")) {
				project = "ROT";
				fileName="vividseatsmanualcats";
			}
		
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getManualCatsVividCsv(autopricingSettings);
			
			StringBuilder sbVividSeats = new StringBuilder();
			sbVividSeats.append("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,InHandDate,EDelivery");
			sbVividSeats.append("\n");
			
			if(list != null && !list.isEmpty()){
				for(Object[] data : list){
					
					sbVividSeats.append(data[0]+",");
					
					if(data[1] != null){
						sbVividSeats.append(((String) data[1]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[2] != null){
						sbVividSeats.append(((String) data[2]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[3] != null){
						sbVividSeats.append(data[3]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[4] != null){
						sbVividSeats.append(data[4]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[5] != null){
						sbVividSeats.append(data[5]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[6] != null){
						sbVividSeats.append(((String) data[6]).replaceAll("[,.]", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[7] != null){
						sbVividSeats.append(((String) data[7]).replaceAll("[,.]", " ")+",");
					}else{
						sbVividSeats.append(",");
					}					
					
					sbVividSeats.append(data[8]+",");
					sbVividSeats.append(data[9]+",");
					
					if(data[10] != null){
						sbVividSeats.append(((String) data[10]).replaceAll(",", " ")+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[11] != null){
						sbVividSeats.append(data[11]+",");
					}else{
						sbVividSeats.append(",");
					}
					
					if(data[12] != null){
						sbVividSeats.append(data[12]+",");
					}else{
						sbVividSeats.append(",");
					}
					if(data[13] != null){
						sbVividSeats.append(data[13]+",");
					}else{
						sbVividSeats.append(",");
					}
					if(data[14] != null){
						Integer ticketType = (Integer) data[14];
						if(ticketType == 1 || ticketType == 9)
							sbVividSeats.append(1);
						else
							sbVividSeats.append(0);
					}else{
						sbVividSeats.append(0);
					}				
					
					sbVividSeats.append("\n");
					
				}
			}
				
			File file = new File(folderPath+project+"\\"+project+fileName+".csv");
			
			
			 if(file.exists()){
				 SimpleDateFormat df = new SimpleDateFormat("MM-dd-hh-mmaa");
				 generateBackupFile(file, folderPath+project+"\\backup\\"+project+fileName+"-" + df.format(new Date())+".csv");
			 }
			 
			if (!file.exists())
				file.createNewFile();
			
			FileWriter vividSeatWriter = new FileWriter(file);				
			vividSeatWriter.append(sbVividSeats.toString());
			vividSeatWriter.flush();
			vividSeatWriter.close();
			
			message = project+fileName+".csv" + " - Generated fine";
			return message;
			
		} catch (Exception e) {
			e.printStackTrace();
			message = project+fileName+".csv" + " - Not generated due to exception";
			return message;
		}
			
	}
	
	
	public void sendMailNow(String subject , String content) throws Exception	{
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailAODev");
		
		
		String smtpHost = DAORegistry.getPropertyDAO().getPropertyByName("smtp.host").getValue().trim();
		int smtpPort = Integer.parseInt(DAORegistry.getPropertyDAO().getPropertyByName("smtp.port").getValue().trim());
		String smtpSecureConnection = DAORegistry.getPropertyDAO().getPropertyByName("smtp.secure.connection").getValue();
		Boolean smtpAuth = Boolean.parseBoolean(DAORegistry.getPropertyDAO().getPropertyByName("smtp.auth").getValue());
		
		String username = null;
		Property smtpUserNameProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.username");
		if (smtpUserNameProperty != null) {
			username = smtpUserNameProperty.getValue();
		}
		
		String fromName =null;
		Property fromNameProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.from");
		if (fromNameProperty != null) {
			fromName = fromNameProperty.getValue();
		}
		String password = null;
		Property smtpUserPasswordProperty = DAORegistry.getPropertyDAO().getPropertyByName("smtp.password");
		if (smtpUserPasswordProperty != null) {
			password = smtpUserPasswordProperty.getValue();
			if (password.isEmpty()) {
				password = null;
			}
		}
		String emailFrom = DAORegistry.getPropertyDAO().getPropertyByName("smtp.from").getValue();
	
		EmailUtil.sendMailNow(smtpHost, smtpPort, "smtp", smtpSecureConnection, smtpAuth, username, password, fromName, emailFrom, toAddress, null, null, subject , content, "text/plain");
	}
	
}
