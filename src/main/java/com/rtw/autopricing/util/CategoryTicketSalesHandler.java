package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.client.RestTemplate;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.LarryLastCategoryTicket;
import com.rtw.autopricing.ftp.data.LastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.ManhattanZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.data.RTFLastRowCategoryTicket;
import com.rtw.autopricing.ftp.data.SoldCategoryTicket;
import com.rtw.autopricing.ftp.data.TNDRealCategoryTicket;
import com.rtw.autopricing.ftp.data.TNDZonesCategoryTicket;
import com.rtw.autopricing.ftp.data.TixCityZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.data.VipCatsCategoryTicket;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;
import com.rtw.autopricing.ftp.data.ZonedLastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.ZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.Broker;
import com.rtw.autopricing.ftp.zonetickets.dao.implementaion.ZoneTicketDAORegistry;


public class CategoryTicketSalesHandler extends QuartzJobBean implements StatefulJob {
	static boolean running=false;
	static Logger logger = LoggerFactory.getLogger(CategoryTicketSalesHandler.class);
	
	
   public static List<SoldCategoryTicket> getAutoCats96SoldCategoryTickets(String fromDateStr,String toDateStr,List<SoldCategoryTicket> soldCatTicketList) throws Exception {
	   SoldCategoryTicket soldCategoryTicket = null;
	   List<Integer> brokerIds = AutoCats96Utils.getAutoCatsActiveBrokerIds();
	   List<String> brokersInternalNotes = AutoCats96Utils.getAutoCats96InternalNotes();;
	   String internalNotesStr ="";
	   for (String string : brokersInternalNotes) {
		   internalNotesStr = internalNotesStr + ",'" + string +"'";
	   }
	   if(internalNotesStr.length() > 0) {
			internalNotesStr = internalNotesStr.substring(1);
		}
	   
	   AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
	   
	   for (Integer brokerId : brokerIds) {
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			//Tamil : to check broker is enabled for TN
			Boolean isEnabled = DAORegistry.getAutopricingSettingsDAO().getIsBrokerEnabledForExchange(broker.getId(),exchange.getId());
			if(!isEnabled) {
				continue;
			}
			
			try{
				List<Integer> soldCatTixIdList = InduxDAORegistry.getPosCategoryTicketGroupDAO().getSoldCategoryTicketGroupIds(fromDateStr, toDateStr, internalNotesStr, broker.getPosBrokerId());
				
				if(soldCatTixIdList != null && !soldCatTixIdList.isEmpty()) {
					AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("AutoCats96");
					List<AutoCats96CategoryTicket> miniCategoryticketList = DAORegistry.getAutoCats96CategoryTicketDAO().getAllAutoCats96CategoryTicketsByPosCategoryTicketGroupIds(soldCatTixIdList,broker.getId());
					for (AutoCats96CategoryTicket categoryTicket : miniCategoryticketList) {
						soldCategoryTicket = new SoldCategoryTicket();
						soldCategoryTicket.setProductId(autopricingProduct.getId());
						soldCategoryTicket.setEventId(categoryTicket.getEventId());
						soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
						
						soldCatTicketList.add(soldCategoryTicket);
						
						categoryTicket.setStatus("SOLD");
						categoryTicket.setLastUpdated(new Date());
						DAORegistry.getAutoCats96CategoryTicketDAO().update(categoryTicket);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
	   }
	 return soldCatTicketList;
	}
	
	public static void getTodaySalesFromPOS() {
		
		if(isRunning()){
			return ;
		}
		setRunning(true);
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress =resourceBundle.getString("emailNotificationTo");
		String ccAddress=resourceBundle.getString("emailNotificationCCTo");
		String zonesWSUrl = resourceBundle.getString("zoneswebservice.url");
		String zonesWSConfigId = resourceBundle.getString("zoneswebservice.configid");
		
		logger.info("Sales Collection Job Started @ " + new Date());
		System.out.println("Sales Collection Job Started @ " + new Date());
			
			try {
				
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date today = new Date();
				
				String fromDateStr = df.format(today)+" 00:00:00";
				String toDateStr = df.format(today)+" 23:59:59";
				
				System.out.println("sales fromDateStr.."+fromDateStr+"..toDateStr."+toDateStr);
				logger.info("sales fromDateStr.."+fromDateStr+"..toDateStr."+toDateStr);

				String internalNotesStr = "";
				List<String> internalNotesList = DAORegistry.getAutopricingProductDAO().getAutopricingProductInternalNotes();
				List<String> ssAccountInternalNotes = LarryLastScheduler.getSsAccountInternalNotes();
				if(ssAccountInternalNotes != null) {
					if(internalNotesList == null) {
						internalNotesList = new ArrayList<String>();
					}
					internalNotesList.addAll(ssAccountInternalNotes);
				}
				for (String string : internalNotesList) {
					internalNotesStr = internalNotesStr + ",'" + string +"'";
				}
				if(internalNotesStr.length() > 0) {
					internalNotesStr = internalNotesStr.substring(1);
				}
				
				List<SoldCategoryTicket> soldCatTicketList = new ArrayList<SoldCategoryTicket>();
				SoldCategoryTicket soldCategoryTicket = null;
				
				AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
				List<Integer> brokerIds = BrokerUtils.getAllBrokersId();
				for (Integer brokerId : brokerIds) {
					Broker broker = BrokerUtils.getBrokerById(brokerId);
					try {
						//Tamil : to check broker is enabled for TN
						Boolean isEnabled = DAORegistry.getAutopricingSettingsDAO().getIsBrokerEnabledForExchange(broker.getId(),exchange.getId());
						if(!isEnabled) {
							continue;
						}
						
						List<Integer> soldCatTixIdList = InduxDAORegistry.getPosCategoryTicketGroupDAO().getSoldCategoryTicketGroupIds(fromDateStr, toDateStr, internalNotesStr, broker.getPosBrokerId());
						
						if(soldCatTixIdList != null && !soldCatTixIdList.isEmpty()) {
							AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MiniCats");
							List<MiniCategoryTicket> miniCategoryticketList = DAORegistry.getMiniCategoryTicketDAO().getAllMiniCatsCategoryTicketsByPosCategoryTicketGroupIds(soldCatTixIdList,broker.getId());
							for (MiniCategoryTicket categoryTicket : miniCategoryticketList) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getMiniCategoryTicketDAO().update(categoryTicket);
								//}
							}
							
							autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("VIP MiniCats");
							List<VipCatsCategoryTicket> vipMiniCategoryticketList = DAORegistry.getVipCatsCategoryTicketDAO().getAllVipMiniCatsCategoryTicketsByPosCategoryTicektGroupIds(soldCatTixIdList,broker.getId());
							for (VipCatsCategoryTicket categoryTicket : vipMiniCategoryticketList) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getVipCatsCategoryTicketDAO().update(categoryTicket);
								//}
							}
							autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("LastRow MiniCats");
							List<LastRowMiniCategoryTicket> lastrowMiniCategoryticketList = DAORegistry.getLastRowMiniCategoryTicketDAO().getAllLastRowMiniCatsCategoryTicketsByPosCategoryTicektGroupIds(soldCatTixIdList,broker.getId());
							for (LastRowMiniCategoryTicket categoryTicket : lastrowMiniCategoryticketList) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getLastRowMiniCategoryTicketDAO().update(categoryTicket);
								//}
							}
							autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("ZonesPricing");
							List<ZonesPricingCategoryTicket> zonPricingCategoryticketList = DAORegistry.getZonesPricingCategoryTicketDAO().getAllZonePricingCategoryTicketsByPosCategoryTicketGroupIds(soldCatTixIdList,broker.getId());
							for (ZonesPricingCategoryTicket categoryTicket : zonPricingCategoryticketList) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getZonesPricingCategoryTicketDAO().update(categoryTicket);
								//}
							}
							
							autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RTWZonesPricing");
							List<TixCityZonesPricingCategoryTicket> tixcityZonPricingCategoryticketList = DAORegistry.getTixCityZonesPricingCategoryTicketDAO().getAllTixCityZonePricingCategoryTicketsByPosCategoryTicketGroupIds(soldCatTixIdList,broker.getId());
							for (TixCityZonesPricingCategoryTicket categoryTicket : tixcityZonPricingCategoryticketList) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getTixCityZonesPricingCategoryTicketDAO().update(categoryTicket);
								//}
							}
							
							autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MHZonePricing");
							List<ManhattanZonesPricingCategoryTicket> manhattanZonPricingCategoryticketList = DAORegistry.getManhattanZonesPricingCategoryTicketDAO().getAllManhattanZonePricingCategoryTicketsByPosCategoryTicketGroupIds(soldCatTixIdList,broker.getId());
							for (ManhattanZonesPricingCategoryTicket categoryTicket : manhattanZonPricingCategoryticketList) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getManhattanZonesPricingCategoryTicketDAO().update(categoryTicket);
								//}
							}
							
							autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNDRealTix");
							List<TNDRealCategoryTicket> tndRealCategoryTicket = DAORegistry.getTndRealCategoryTicketDAO().getAllMiniCatsCategoryTicketsByPosCategoryTicketGroupIds(soldCatTixIdList,broker.getId());
							for (TNDRealCategoryTicket categoryTicket : tndRealCategoryTicket) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getTndRealCategoryTicketDAO().update(categoryTicket);
								//}
							}
							autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNDZones");
							List<TNDZonesCategoryTicket> tndZonesCategoryTicket = DAORegistry.getTndZonesCategoryTicketDAO().getAllMiniCatsCategoryTicketsByPosCategoryTicketGroupIds(soldCatTixIdList,broker.getId());
							for (TNDZonesCategoryTicket categoryTicket : tndZonesCategoryTicket) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								//soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getTndZonesCategoryTicketDAO().update(categoryTicket);
								//}
							}
						}
						
						List<Integer> posSoldTicketGroupIdList = InduxDAORegistry.getPosTicketGroupDAO().getSoldTicketGroupIds(fromDateStr, toDateStr, internalNotesStr, broker.getPosBrokerId());
						
						if(posSoldTicketGroupIdList != null && !posSoldTicketGroupIdList.isEmpty()) {
							AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SS Account");
							List<LarryLastCategoryTicket> larryLastCategoryticketList = DAORegistry.getLarryLastCategoryTicketDAO().getAllLarryLastCategoryTicketsByPosTicketGroupIds(posSoldTicketGroupIdList,broker.getId());
							for (LarryLastCategoryTicket categoryTicket : larryLastCategoryticketList) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getLarryLastCategoryTicketDAO().update(categoryTicket);
								//}
							}
							
							autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("Zoned LastRow MiniCats");
							List<ZonedLastRowMiniCategoryTicket> zoneLastRowCategoryTicket = DAORegistry.getZonedLastRowMiniCategoryTicketDAO().getAllZonedLastRowMiniCategoryTicketsByPosTicketGroupIds(posSoldTicketGroupIdList,broker.getId());
							for (ZonedLastRowMiniCategoryTicket categoryTicket : zoneLastRowCategoryTicket) {
								soldCategoryTicket = new SoldCategoryTicket();
								soldCategoryTicket.setProductId(autopricingProduct.getId());
								soldCategoryTicket.setEventId(categoryTicket.getEventId());
								soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
								
								soldCatTicketList.add(soldCategoryTicket);
								
								//if(categoryTicket.getStatus().equals("ACTIVE")) {
									categoryTicket.setStatus("SOLD");
									categoryTicket.setLastUpdated(new Date());
									DAORegistry.getZonedLastRowMiniCategoryTicketDAO().update(categoryTicket);
								//}
							}
							
						}
						
						getAutoCats96SoldCategoryTickets(fromDateStr, toDateStr,soldCatTicketList);
						
					} catch (Exception e) {
						e.printStackTrace();
						isErrorOccured = true;
						error = new AutoPricingError();
						error.setMessage("Error While getting Todays Sales for Broker "+broker);
						error.setExample("Error While getting Todays Sales for Broker "+broker);
						error.setProcess("Get Todays Sales");
						error.setEventId(0);
						error.setTimeStamp(new Date());
						errorList.add(error);
						logger.error("Error While getting Todays Sales for Broker "+broker);
						System.out.println("Error While getting Todays Sales for Broker "+broker);
					}
				}
				
				
				
				DAORegistry.getSoldCategoryTicketDAO().saveAll(soldCatTicketList);
				
				try {

					String rtfInternalNotesStr = BrokerUtils.getRtfAutopricingProductInternalNotesStrForSqlQuery();
					List<Integer> zoneTicketsSoldTicketsIds = ZoneTicketDAORegistry.getZoneTicketsTicketGroupDAO().getRTFSoldCategoryTicketGroupIds(fromDateStr, toDateStr, rtfInternalNotesStr);
					if(zoneTicketsSoldTicketsIds != null && !zoneTicketsSoldTicketsIds.isEmpty()) {
						AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RewardTheFan Listings");
						List<ZoneTicketsProcessorCategoryTicket> miniCategoryticketList = DAORegistry.getZoneTicketsProcessorCategoryTicketDAO().getAllZoneTicektsProcessorCategoryTicketsByIds(zoneTicketsSoldTicketsIds);
						for (ZoneTicketsProcessorCategoryTicket categoryTicket : miniCategoryticketList) {
							soldCategoryTicket = new SoldCategoryTicket();
							soldCategoryTicket.setProductId(autopricingProduct.getId());
							soldCategoryTicket.setEventId(categoryTicket.getEventId());
							soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
							
							soldCatTicketList.add(soldCategoryTicket);
							
							//if(categoryTicket.getStatus().equals("ACTIVE")) {
								categoryTicket.setStatus("SOLD");
								categoryTicket.setLastUpdated(new Date());
								DAORegistry.getZoneTicketsProcessorCategoryTicketDAO().update(categoryTicket);
							//}
						}
						autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RTFLastRowCats");
						List<RTFLastRowCategoryTicket> rtfLastRowCatTixList = DAORegistry.getRtfLastRowCategoryTicketDAO().getAllRTFLastRowCategoryTicketsByIds(zoneTicketsSoldTicketsIds);
						for (RTFLastRowCategoryTicket categoryTicket : rtfLastRowCatTixList) {
							soldCategoryTicket = new SoldCategoryTicket();
							soldCategoryTicket.setProductId(autopricingProduct.getId());
							soldCategoryTicket.setEventId(categoryTicket.getEventId());
							soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
							
							soldCatTicketList.add(soldCategoryTicket);
							
							//if(categoryTicket.getStatus().equals("ACTIVE")) {
								categoryTicket.setStatus("SOLD");
								categoryTicket.setLastUpdated(new Date());
								DAORegistry.getRtfLastRowCategoryTicketDAO().update(categoryTicket);
							//}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					isErrorOccured = true;
					error = new AutoPricingError();
					error.setMessage("Error While getting ZoneTickets sold tickets");
					error.setExample("Error While getting ZoneTickets sold tickets");
					error.setProcess("Get Todays Sales");
					error.setEventId(0);
					error.setTimeStamp(new Date());
					errorList.add(error);
					logger.error("Error While getting ZoneTickets sold tickets");
					System.out.println("Error While getting ZoneTickets sold tickets");
				}
					
				logger.error("Last Hour Sales count:"+ soldCatTicketList.size() +":@:"+new Date());
				System.out.println("Last Hour Sales count:"+ soldCatTicketList.size() +":@:"+new Date());
				running=false;
			} catch (Exception e) {
				running=false;
				e.printStackTrace();
				isErrorOccured = true;
				error = new AutoPricingError();
				error.setMessage("Error in CategoryTicket SalesHandler");
				error.setExample("Error in CategoryTicket SalesHandler");
				error.setProcess("Get Todays Sales");
				error.setEventId(0);
				error.setTimeStamp(new Date());
				errorList.add(error);
				logger.error("Error in CategoryTicket SalesHandler");
				System.out.println("Error in CategoryTicket SalesHandler");
			}
			String subject,fileName;
			Map<String, Object> map;
			
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {

					DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
					
					subject = "Autopricing Sales Handler Job failed.";
					fileName = "templates/autopricing-job-failure-message.txt";
					
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("Sales Collection Job finished @ " + new Date());
			logger.info("Sales Collection Job finished @ " + new Date());
	
	}
	
	/*public static List<Integer> getZoneTicektsSoldTickets(String zonesWSUrl,String zonesWSConfigId,String fromDateStr,String toDateStr) throws Exception {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		fromDateStr = df.format(today);
		toDateStr = df.format(today);
		
		String url = zonesWSUrl+"GetSoldTicketIds.json?configId="+zonesWSConfigId+"&fromDate="+fromDateStr+"&toDate="+toDateStr;
		RestTemplate restTemplate = new RestTemplate();
		String jsonData = restTemplate.getForObject(url, String.class);
		JSONObject jsonObject = new JSONObject(jsonData);
		jsonData = jsonObject.getString("soldTicketIdList");
		ObjectMapper mapper = new ObjectMapper();
		SoldTicketIdList soldTicketIdList = mapper.readValue(jsonData, SoldTicketIdList.class);
		
		return soldTicketIdList.getTicketGroupIds();
	}*/
	
	/*public static List<SoldCategoryTicket> getSoldCategoryTickets(List<CategoryTicket> categoryTicektList,AutopricingProduct autopricingProduct) throws Exception {
		
		List<SoldCategoryTicket> soldCatTicketList = new ArrayList<SoldCategoryTicket>();
		for (CategoryTicket categoryTicket : categoryTicektList) {
			
			SoldCategoryTicket soldCategoryTicket = new SoldCategoryTicket();
			soldCategoryTicket.setProductId(autopricingProduct.getId());
			soldCategoryTicket.setEventId(categoryTicket.getEventId());
			soldCategoryTicket.setTicketId(categoryTicket.getTicketId());
			
			soldCatTicketList.add(soldCategoryTicket);
		}
		
		return null;
	}*/
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		System.out.println("SALES Collection Job Called.." + new Date() + ": " + running);
		logger.info("SALES Collection Job Called.." + new Date() + ": " + running);
		try{
			
			getTodaySalesFromPOS();
			System.out.println("Sales Scheduler Job finished @ " + new Date());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		CategoryTicketSalesHandler.running = running;
	}
	
}