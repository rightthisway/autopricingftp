package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.UnbroadcastedEvents;

public class UnbroadCastedEventDetailsMail extends QuartzJobBean implements StatefulJob {

	 private static Logger log = LoggerFactory.getLogger(UnbroadCastedEventDetailsMail.class);
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	 public static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	 public void sendEventDetails(){
		
		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.DATE, -1);
		 Date yesterdayDate = cal.getTime();
		 try{
		 UnbroadcastedEvents unbroadcastedEvents = DAORegistry.getUnbroadcastedEventsDAO().
					getUnbrodacastedEventsByDate(dbDateFormat.format(yesterdayDate));
		 Map<String, Object> map = new HashMap<String, Object>();
		 if(unbroadcastedEvents != null && unbroadcastedEvents.getEventIds() != null && 
				 !unbroadcastedEvents.getEventIds().isEmpty()){
			 Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByeventIds(unbroadcastedEvents.getEventIds());
			 map.put("events", events);
		 }
		 map.put("dateMsg", dateFormat.format(yesterdayDate));
		 String subject = "Unbroadcasted Events due to multiple sales On - "+ dateFormat.format(yesterdayDate);
		 String fileName = "templates/unbroadcasted-events-details.txt";
		 String toAddress ="leor@rewardthefan.com;ezippo@rightthisway.com;jreardon@rightthisway.com;bmanusama@rightthisway.com";
		 String bccAddress = "AODev@rightthisway.com";
		 EmailManager.sendEmail(toAddress, null, bccAddress, subject, fileName, map);
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	 }
	 
	 public void sendEventDetailsForLstSevenDays(){

		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.DATE, -7);
		 Date fromDate = new Date(cal.getTimeInMillis());
		 String fromDateStr = dbDateFormat.format(fromDate);
		
		 cal = Calendar.getInstance();
		 cal.add(Calendar.DATE, -1);
		 Date toDate = new Date(cal.getTimeInMillis());
		 String toDateStr = dbDateFormat.format(toDate);
		 
		 try{
		 List<UnbroadcastedEvents> unbroadcastedEventsList = DAORegistry.getUnbroadcastedEventsDAO().
					getUnbrodacastedEventsByDateRange(fromDateStr, toDateStr);
		 Map<String, Object> map = new HashMap<String, Object>();
		 String eventIDs = "";
		 for (UnbroadcastedEvents unbroadcastedEvent : unbroadcastedEventsList) {
			 if(unbroadcastedEvent != null && unbroadcastedEvent.getEventIds() != null && 
					 !unbroadcastedEvent.getEventIds().isEmpty()){
				 eventIDs = eventIDs + ","+unbroadcastedEvent.getEventIds();
				 /*Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByeventIds(unbroadcastedEvents.getEventIds());
				 map.put("events", events);*/
			 }
		 }
		 if(!eventIDs.equals("")) {
			 eventIDs = eventIDs.substring(1);
			 List<Integer> eventIdList = DAORegistry.getQueryManagerDAO().getAllEventIdssInUnbroadcatedStatusByIdStr(eventIDs);
			 String unbroadcastIds = "";
			 for (Integer eventId : eventIdList) {
				 unbroadcastIds = unbroadcastIds + "," + eventId;
			 }
			 if(!unbroadcastIds.equals("")) {
				 unbroadcastIds = unbroadcastIds.substring(1);
				 Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByeventIds(unbroadcastIds);
				 map.put("events", events);	 
			 }
			 
		 }
		 map.put("dateMsg", " From "+dateFormat.format(fromDate)+" To "+dateFormat.format(toDate));
		 String subject = "Events in Unbroadcasted Status due to multiple sales From - "+ dateFormat.format(fromDate)+" To "+dateFormat.format(toDate);
		 String fileName = "templates/unbroadcasted-events-details.txt";
		 String toAddress ="leor@rewardthefan.com;ezippo@rightthisway.com;jreardon@rightthisway.com;bmanusama@rightthisway.com";
		 String bccAddress = "AODev@rightthisway.com";
		 //String toAddress ="tselvan@rightthisway.com";
		 //String bccAddress = null;
		 
		 EmailManager.sendEmail(toAddress, null, bccAddress, subject, fileName, map);
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	 }
	 
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		System.out.println("Unbroadcasted event details mail job started.." + new Date() + ": " + running);
		try{
			 if(isStopped() || isRunning()){
					return ;
			 }
			setRunning(true);
			
			sendEventDetails();
			sendEventDetailsForLstSevenDays();
			
			setRunning(false);
			
			System.out.println("Unbroadcasted event details mail job finished @ " + new Date());
		}catch(Exception e){
			setRunning(false);
			
			e.printStackTrace();
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			String toAddress = resourceBundle.getString("emailNotificationTo");
			String ccAddress= resourceBundle.getString("emailNotificationCCTo");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("bodyContent", "Following error occured while processing Unbroadcast Events Email Trigger Job : ");
			map.put("error", ""+e.fillInStackTrace());
			try {
				String subject = "Unbroadcast Events Email Trigger Job Failed : ";
				String fileName = "templates/common-mail-error.html.txt";
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
			} catch (Exception e1) {
				log.error("POSLOCALEVENTIMPORTER 17 : Error while Importing Local Event By Exchange Event Id.");
				System.out.println("POSLOCALEVENTIMPORTER 17 : Error while Importing Local Event By Exchange Event Id.");
				e1.printStackTrace();
			}
		}
}
 
 public static Boolean isStopped() {
	if(stopped==null){
		stopped =  false;
	}
	return stopped;
}


public static void setStopped(Boolean stopped) {
	UnbroadCastedEventDetailsMail.stopped = stopped;
}


public static Boolean isRunning() {
	return running;
}


public static void setRunning(Boolean running) {
	UnbroadCastedEventDetailsMail.running = running;
}
}
