package com.rtw.autopricing.util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleAutoCatCategoryTicket;


public class AutoCats96CategoryTicketUtil {
	static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	public static Map<Integer, List<AutoCats96CategoryTicket>> prioritizingCategoryTicketsByBroker(Map<String, AutoCats96CategoryTicket> catTixFromDB,
			Map<String,AutoCats96CategoryTicket> catTixMap,Map<Integer, Integer> brokerTixCountMap,Integer eventMaxTixCount,List<Integer> tnBrokerIds,Set<String> excludeZones)throws Exception {
			Map<Integer,List<AutoCats96CategoryTicket>> brokersCategoryTixsMap = new HashMap<Integer, List<AutoCats96CategoryTicket>>();
			List<AutoCats96CategoryTicket> categoryTixList = new ArrayList<AutoCats96CategoryTicket>();
			try{
				
				String priceHistory="";
				String ticketIdHistory= "";
				String baseTicketOneHistory= "";
				String baseTicketTwoHistory="";
				String baseTicketThreeHistory="";
				String ticketHDateStr = dateTimeFormat.format(new Date());
				String priceHDateStr = dateTimeFormat.format(new Date());
				
				if(null != catTixFromDB && !catTixFromDB.isEmpty()){
					Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
					List<String> keys = new ArrayList<String>(catTixFromDB.keySet());
							
					for (String key : keys) {
						AutoCats96CategoryTicket dbTix =catTixFromDB.remove(key);
						AutoCats96CategoryTicket curTicket = catTixMap.remove(key);
						if(null != curTicket ){
							if(!curTicket.getActualPrice().equals(dbTix.getActualPrice())){
								priceHistory = priceHDateStr+"-"+curTicket.getActualPrice();
								if(dbTix.getPriceHistory() != null) {
									priceHistory = dbTix.getPriceHistory() +","+priceHistory;
									if(priceHistory.length()>=500) {
										priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
									}
								}
								dbTix.setPriceHistory(priceHistory);
								dbTix.setIsEdited(true);
								dbTix.setIsTnUpdate(true);
								dbTix.setActualPrice(curTicket.getActualPrice());
								dbTix.setTnPrice(curTicket.getTnPrice());
							}else if (!dbTix.getCatRow().equals(curTicket.getCatRow())){
								dbTix.setCatRow(curTicket.getCatRow());
								dbTix.setIsEdited(true);
							}else if(!curTicket.getBaseTicketOne().equals(dbTix.getBaseTicketOne()) || 
									!curTicket.getBaseTicketTwo().equals(dbTix.getBaseTicketTwo()) || 
									!curTicket.getBaseTicketThree().equals(dbTix.getBaseTicketThree())) {
								
								dbTix.setIsEdited(true);
								
							}
							
							if(!dbTix.getShippingMethodSpecialId().equals(curTicket.getShippingMethodSpecialId()) ||
									!dbTix.getNearTermOptionId().equals(curTicket.getNearTermOptionId()) ||
									dbTix.getExpectedArrivalDate().compareTo(curTicket.getExpectedArrivalDate()) != 0) {
								dbTix.setNearTermOptionId(curTicket.getNearTermOptionId());
								dbTix.setShippingMethodSpecialId(curTicket.getShippingMethodSpecialId());
								dbTix.setExpectedArrivalDate(curTicket.getExpectedArrivalDate());
								dbTix.setIsEdited(true);
								dbTix.setIsTnUpdate(true);
							}
							
							if(isPriceChangeFlag || !dbTix.getTicketId().equals(curTicket.getTicketId())) {
								ticketIdHistory = ticketHDateStr +"/"+ curTicket.getTicketId() +"/"+ curTicket.getPurPrice();
								if(dbTix.getTicketIdHistory() != null) {
									ticketIdHistory = dbTix.getTicketIdHistory()+","+ ticketIdHistory;
									if(ticketIdHistory.length()>= 500) {
										ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
									}
								}
								dbTix.setTicketIdHistory(ticketIdHistory);
								dbTix.setIsEdited(true);
							}
							
							
							//Base Ticket One
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketOne() != null && dbTix.getBaseTicketOne() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketOne().equals(curTicket.getBaseTicketOne())) {
									tempTicketIdFlag = true;
								}
							} else if(curTicket.getBaseTicketOne() != null && curTicket.getBaseTicketOne() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketOneHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketOne() +"/"+ curTicket.getBaseTicketOnePurPrice();
								if(dbTix.getBaseTicketOneHistory() != null) {
									baseTicketOneHistory = dbTix.getBaseTicketOneHistory()+","+ baseTicketOneHistory;
															
									if(baseTicketOneHistory.length()>= 500) {
										baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketOneHistory(baseTicketOneHistory);
								dbTix.setIsEdited(true);
							}
							
							// For Base Ticket 2
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketTwo() != null && dbTix.getBaseTicketTwo() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketTwo().equals(curTicket.getBaseTicketTwo())){
									tempTicketIdFlag = true;
								}
							} else if(curTicket.getBaseTicketTwo() != null && curTicket.getBaseTicketTwo() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketTwoHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketTwo() +"/"+ curTicket.getBaseTicketTwoPurPrice();
								if(dbTix.getBaseTicketTwoHistory() != null) {
									baseTicketTwoHistory = dbTix.getBaseTicketTwoHistory()+","+ baseTicketTwoHistory;
															
									if(baseTicketTwoHistory.length()>= 500) {
										baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
								dbTix.setIsEdited(true);
							}
							
							
							// For Base Ticket 3
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketThree() != null && dbTix.getBaseTicketThree() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketThree().equals(curTicket.getBaseTicketThree()))  {
									tempTicketIdFlag = true;
								}
							} else if(curTicket.getBaseTicketThree() != null && curTicket.getBaseTicketThree() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketThreeHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketThree() +"/"+ curTicket.getBaseTicketThreePurPrice();
								if(dbTix.getBaseTicketThreeHistory() != null) {
									baseTicketThreeHistory = dbTix.getBaseTicketThreeHistory()+","+ baseTicketThreeHistory;
															
									if(baseTicketThreeHistory.length()>= 500) {
										baseTicketThreeHistory = baseTicketThreeHistory.substring(baseTicketThreeHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
								dbTix.setIsEdited(true);
							}
							
							dbTix.setBaseTicketOne(curTicket.getBaseTicketOne());
							dbTix.setBaseTicketTwo(curTicket.getBaseTicketTwo());
							dbTix.setBaseTicketThree(curTicket.getBaseTicketThree());
							dbTix.setCategoryId(curTicket.getCategoryId());
							dbTix.setTicketId(curTicket.getTicketId());
							dbTix.setItemId(curTicket.getItemId());
							dbTix.setCatRow(curTicket.getCatRow());
							dbTix.setLastRow(curTicket.getLastRow());
							//dbTix.setAlternateRow(curTicket.getAlternateRow());
							dbTix.setRowRange(curTicket.getRowRange());
							dbTix.setActualPrice(curTicket.getActualPrice());
							dbTix.setTnPrice(curTicket.getTnPrice());
							dbTix.setTmatCategory(curTicket.getTmatCategory());
							dbTix.setShippingMethodSpecialId(curTicket.getShippingMethodSpecialId());
							dbTix.setNearTermOptionId(curTicket.getNearTermOptionId());
							dbTix.setExpectedArrivalDate(curTicket.getExpectedArrivalDate());
							dbTix.setProductPriority(curTicket.getProductPriority());
							dbTix.setTmatZone(curTicket.getTmatZone());
							dbTix.setTnExchangeEventId(curTicket.getTnExchangeEventId());
							
							categoryTixList.add(dbTix);
						}else{
							dbTix.setStatus("DELETED");
							categoryTixList.add(dbTix);
						}
					}
				}
					
				if(null != catTixMap && !catTixMap.isEmpty()){
					
					for (AutoCats96CategoryTicket tix : catTixMap.values()) {
						
						priceHistory = priceHDateStr+"-"+tix.getActualPrice();
						tix.setPriceHistory(priceHistory);
						
						ticketIdHistory = ticketHDateStr +"/"+ tix.getTicketId() +"/"+ tix.getPurPrice();
						tix.setTicketIdHistory(ticketIdHistory);
						
						if(null != tix.getBaseTicketOne() && tix.getBaseTicketOne() > 0){
							baseTicketOneHistory = ticketHDateStr +"/"+ tix.getBaseTicketOne() +"/"+ tix.getBaseTicketOnePurPrice();
							tix.setBaseTicketOneHistory(baseTicketOneHistory);
						}
						
						if(null != tix.getBaseTicketTwo() && tix.getBaseTicketTwo() > 0){
							baseTicketTwoHistory = ticketHDateStr +"/"+ tix.getBaseTicketTwo() +"/"+ tix.getBaseTicketTwoPurPrice();
							tix.setBaseTicketTwoHistory(baseTicketTwoHistory);
						}
						
						if(null != tix.getBaseTicketThree() && tix.getBaseTicketThree() > 0){
							baseTicketThreeHistory = ticketHDateStr +"/"+ tix.getBaseTicketThree() +"/"+ tix.getBaseTicketThreePurPrice();
							tix.setBaseTicketThreeHistory(baseTicketThreeHistory);
						}
						tix.setIsEdited(true);
						categoryTixList.add(tix);
					}
				}
				
				List<AutoCats96CategoryTicket> tobeDeletedTickets = new ArrayList<AutoCats96CategoryTicket>();
				List<AutoCats96CategoryTicket> tobeUpdatedTickets = new ArrayList<AutoCats96CategoryTicket>();
				List<AutoCats96CategoryTicket> toBeSaveTickets = new ArrayList<AutoCats96CategoryTicket>();
				
				for (AutoCats96CategoryTicket catTix : categoryTixList) {
					
					if(null != catTix.getId() && catTix.getTnCategoryTicketGroupId() != null){
						if(catTix.getStatus().equals("ACTIVE")){
							tobeUpdatedTickets.add(catTix);
							continue;
						}
						tobeDeletedTickets.add(catTix);
						continue;
					}
					toBeSaveTickets.add(catTix);
				}
				
				Map<Integer, List<AutoCats96CategoryTicket>> brokersExistingTicketMap = new HashMap<Integer, List<AutoCats96CategoryTicket>>();
				
				for (AutoCats96CategoryTicket tix : tobeUpdatedTickets) {
					
					List<AutoCats96CategoryTicket> catTickets = brokersExistingTicketMap.get(tix.getTnBrokerId());
					
					Integer brokerMaxTixCount = brokerTixCountMap.get(tix.getTnBrokerId());
					
					if(null != catTickets && !catTickets.isEmpty()){
						
						if(catTickets.size() > brokerMaxTixCount){
							tobeDeletedTickets.add(tix);
							continue;
						}
						brokersExistingTicketMap.get(tix.getTnBrokerId()).add(tix);
					}else{
						catTickets = new ArrayList<AutoCats96CategoryTicket>();
						catTickets.add(tix);
						brokersExistingTicketMap.put(tix.getTnBrokerId(),catTickets);
					}
				}
				
				
				Map<String, List<AutoCats96CategoryTicket>> zonesCatTicketsMap =  new HashMap<String, List<AutoCats96CategoryTicket>>();
				
				Map<String, List<AutoCats96CategoryTicket>> duplicateSectionTicketsMap =  new HashMap<String, List<AutoCats96CategoryTicket>>();
				Set<String> duplicateSections = new HashSet<String>();
		    	
				Collections.sort(toBeSaveTickets, AutoCats96CategoryTicketUtil.sortingCategoryTicketbyPrice);
				
		    	for (AutoCats96CategoryTicket newTix : toBeSaveTickets) {
		    		List<AutoCats96CategoryTicket> zoneCatTickets = zonesCatTicketsMap.get(newTix.getTmatCategory());
					if(null != zoneCatTickets && !zoneCatTickets.isEmpty()){
						
						if(!duplicateSections.add(newTix.getSection())){
							
							List<AutoCats96CategoryTicket> duplicateTickets = duplicateSectionTicketsMap.get(newTix.getSection());
							
							if(null != duplicateTickets && !duplicateTickets.isEmpty()){
								duplicateSectionTicketsMap.get(newTix.getSection()).add(newTix);
							}else{
								duplicateTickets = new ArrayList<AutoCats96CategoryTicket>();
								duplicateTickets.add(newTix);
								duplicateSectionTicketsMap.put(newTix.getSection(), duplicateTickets);
							}
							continue;
						}
						zonesCatTicketsMap.get(newTix.getTmatCategory()).add(newTix);
					}else{
						zoneCatTickets = new ArrayList<AutoCats96CategoryTicket>();
						zoneCatTickets.add(newTix);
						zonesCatTicketsMap.put(newTix.getTmatCategory(), zoneCatTickets);
					}
					duplicateSections.add(newTix.getSection());
				}
		    	
		    	getCheapestCategoryTicketsByZone(zonesCatTicketsMap, duplicateSectionTicketsMap, brokersExistingTicketMap,
		    			tnBrokerIds,brokersCategoryTixsMap,brokerTixCountMap,excludeZones);
		    	
		    	//To Be Deleted Tickets because no ticket found in tmat.
				brokersCategoryTixsMap.put(0, tobeDeletedTickets);
				
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			return brokersCategoryTixsMap;
		}
	
	/*public static Map<Integer, List<AutoCats96CategoryTicket>> prioritizingCategoryTicketsByBroker(Map<Integer, Map<Integer, List<AutoCats96CategoryTicket>>> qtyBrokerDBCatTixMap,
			Map<Integer, List<AutoCats96CategoryTicket>> qtyCatTixMap,Integer brokerMaxTixCount,Integer eventMaxTixCount,List<Integer> tnBrokerIds) {
		Map<Integer,List<AutoCats96CategoryTicket>> brokersCategoryTixsMap = new HashMap<Integer, List<AutoCats96CategoryTicket>>();
		List<AutoCats96CategoryTicket> categoryTixList = new ArrayList<AutoCats96CategoryTicket>();
		try{
			
			for (Integer qty : qtyCatTixMap.keySet()) {
				
				List<AutoCats96CategoryTicket> curCatTickets =  qtyCatTixMap.get(qty);
				Map<Integer, List<AutoCats96CategoryTicket>> brokerDBTixMap = qtyBrokerDBCatTixMap.remove(qty);
				Map<String, AutoCats96CategoryTicket> curTixMap = new HashMap<String, AutoCats96CategoryTicket>();
				
				for (AutoCats96CategoryTicket curTix : curCatTickets) {
					curTixMap.put(qty+":"+ curTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+curTix.getCatRow().replaceAll("\\s+", " ").trim().toLowerCase(), curTix);
				}
				
				if(null != brokerDBTixMap && !brokerDBTixMap.isEmpty()){
					
					String priceHistory="";
					String ticketIdHistory= "";
					String baseTicketOneHistory= "";
					String baseTicketTwoHistory="";
					String baseTicketThreeHistory="";
					Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
					
					for (Integer brokerId : brokerDBTixMap.keySet()) {
						List<AutoCats96CategoryTicket> dbCatTickets =  brokerDBTixMap.get(brokerId);
						for (AutoCats96CategoryTicket dbTix : dbCatTickets) {
							String key = qty+":"+ dbTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+dbTix.getCatRow().replaceAll("\\s+", " ").trim().toLowerCase();
							AutoCats96CategoryTicket curTicket = curTixMap.remove(key);
							if(null != curTicket ){
								if(!curTicket.getActualPrice().equals(dbTix.getActualPrice())){
									if(dbTix.getPriceHistory() != null){
										priceHistory = dbTix.getPriceHistory() +","+curTicket.getPriceHistory();
										if(priceHistory.length()>= 500){
											priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
										}
									}
									dbTix.setPriceHistory(priceHistory);
									dbTix.setIsEdited(true);
									dbTix.setIsTnUpdate(true);
									dbTix.setActualPrice(curTicket.getActualPrice());
									dbTix.setTnPrice(curTicket.getTnPrice());
								}else if (!dbTix.getCatRow().equals(curTicket.getCatRow())){
									dbTix.setCatRow(curTicket.getCatRow());
									dbTix.setIsEdited(true);
								}else if(!curTicket.getBaseTicketOne().equals(dbTix.getBaseTicketOne()) || 
										!curTicket.getBaseTicketTwo().equals(dbTix.getBaseTicketTwo()) || 
										!curTicket.getBaseTicketThree().equals(dbTix.getBaseTicketThree())) {
									
									dbTix.setIsEdited(true);
									
								} else if (!dbTix.getTicketId().equals(curTicket.getTicketId())){
									
									if(dbTix.getTicketIdHistory() != null) {
										ticketIdHistory = dbTix.getTicketIdHistory()+","+ curTicket.getTicketIdHistory();
										if(ticketIdHistory.length()>= 500) {
											ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
										}
									}
									dbTix.setTicketIdHistory(ticketIdHistory);
									dbTix.setIsEdited(true);
								}
								
								if(!dbTix.getShippingMethodSpecialId().equals(curTicket.getShippingMethodSpecialId()) ||
										!dbTix.getNearTermOptionId().equals(curTicket.getNearTermOptionId())){
									dbTix.setNearTermOptionId(curTicket.getNearTermOptionId());
									dbTix.setShippingMethodSpecialId(curTicket.getShippingMethodSpecialId());
									dbTix.setIsEdited(true);
									dbTix.setIsTnUpdate(true);
								}
								
								if(isPriceChangeFlag || !dbTix.getTicketId().equals(curTicket.getTicketId())) {
									if(dbTix.getTicketIdHistory() != null) {
										ticketIdHistory = dbTix.getTicketIdHistory()+","+ curTicket.getTicketIdHistory();
										if(ticketIdHistory.length()>= 500) {
											ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
										}
									}
									dbTix.setTicketIdHistory(ticketIdHistory);
									dbTix.setIsEdited(true);
								}
								
								
								//Base Ticket One
								tempTicketIdFlag = false;
								if (dbTix.getBaseTicketOne() != null && dbTix.getBaseTicketOne() != 0) {
									if(isPriceChangeFlag || !dbTix.getBaseTicketOne().equals(curTicket.getBaseTicketOne())) {
										tempTicketIdFlag = true;
									}
								} else if(curTicket.getBaseTicketOne() != null && curTicket.getBaseTicketOne() != 0) {
									tempTicketIdFlag = true;
								}
								
								if(tempTicketIdFlag) {
									if(dbTix.getBaseTicketOneHistory() != null) {
										
										baseTicketOneHistory = dbTix.getBaseTicketOneHistory()+","+ curTicket.getBaseTicketOneHistory();
																
										if(baseTicketOneHistory.length()>= 500) {
											baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
										}
									}
									dbTix.setBaseTicketOneHistory(baseTicketOneHistory);
									dbTix.setIsEdited(true);
								}
								
								// For Base Ticket 2
								tempTicketIdFlag = false;
								if (dbTix.getBaseTicketTwo() != null && dbTix.getBaseTicketTwo() != 0) {
									if(isPriceChangeFlag || !dbTix.getBaseTicketTwo().equals(curTicket.getBaseTicketTwo())){
										tempTicketIdFlag = true;
									}
								} else if(curTicket.getBaseTicketTwo() != null && curTicket.getBaseTicketTwo() != 0) {
									tempTicketIdFlag = true;
								}
								
								if(tempTicketIdFlag) {
									if(dbTix.getBaseTicketTwoHistory() != null) {
										baseTicketTwoHistory = dbTix.getBaseTicketTwoHistory()+","+ curTicket.getBaseTicketTwoHistory() ;
																
										if(baseTicketTwoHistory.length()>= 500) {
											baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
										}
									}
									dbTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
									dbTix.setIsEdited(true);
								}
								
								
								// For Base Ticket 3
								tempTicketIdFlag = false;
								if (dbTix.getBaseTicketThree() != null && dbTix.getBaseTicketThree() != 0) {
									if(isPriceChangeFlag || !dbTix.getBaseTicketThree().equals(curTicket.getBaseTicketThree()))  {
										tempTicketIdFlag = true;
									}
								} else if(curTicket.getBaseTicketThree() != null && curTicket.getBaseTicketThree() != 0) {
									tempTicketIdFlag = true;
								}
								
								if(tempTicketIdFlag) {
									
									if(dbTix.getBaseTicketThreeHistory() != null) {
										baseTicketThreeHistory = dbTix.getBaseTicketThreeHistory()+","+ curTicket.getBaseTicketThreeHistory();
																
										if(baseTicketThreeHistory.length()>= 500) {
											baseTicketThreeHistory = baseTicketThreeHistory.substring(baseTicketThreeHistory.indexOf(",")+1);	
										}
									}
									dbTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
									dbTix.setIsEdited(true);
								}
								
								dbTix.setBaseTicketOne(curTicket.getBaseTicketOne());
								dbTix.setBaseTicketTwo(curTicket.getBaseTicketTwo());
								dbTix.setBaseTicketThree(curTicket.getBaseTicketThree());
								dbTix.setCategoryId(curTicket.getCategoryId());
								dbTix.setTicketId(curTicket.getTicketId());
								dbTix.setItemId(curTicket.getItemId());
								dbTix.setCatRow(curTicket.getCatRow());
								dbTix.setLastRow(curTicket.getLastRow());
								dbTix.setAlternateRow(curTicket.getAlternateRow());
								dbTix.setRowRange(curTicket.getRowRange());
								dbTix.setActualPrice(curTicket.getActualPrice());
								dbTix.setTnPrice(curTicket.getTnPrice());
								dbTix.setTmatCategory(curTicket.getTmatCategory());
								dbTix.setShippingMethodSpecialId(curTicket.getShippingMethodSpecialId());
								dbTix.setNearTermOptionId(curTicket.getNearTermOptionId());
								categoryTixList.add(dbTix);
							}else{
								dbTix.setStatus("DELETED");
								categoryTixList.add(dbTix);
							}
						}
					}
				}
				
				if(null != curTixMap && !curTixMap.isEmpty()){
					categoryTixList.addAll(curTixMap.values());
				}
			}
			
			if(null != qtyBrokerDBCatTixMap && !qtyBrokerDBCatTixMap.isEmpty()){
				
				for (Integer qty : qtyBrokerDBCatTixMap.keySet()) {
					Map<Integer, List<AutoCats96CategoryTicket>> brokerTixMap = qtyBrokerDBCatTixMap.get(qty);
					for (Integer brokerId : brokerTixMap.keySet()){
						List<AutoCats96CategoryTicket> deleteTixs = brokerTixMap.get(brokerId);
						for (AutoCats96CategoryTicket tix : deleteTixs) {
							tix.setStatus("DELETED");
							categoryTixList.add(tix);
						}
					}
				}
			}
			
			List<AutoCats96CategoryTicket> tobeDeletedTickets = new ArrayList<AutoCats96CategoryTicket>();
			List<AutoCats96CategoryTicket> tobeUpdatedTickets = new ArrayList<AutoCats96CategoryTicket>();
			List<AutoCats96CategoryTicket> toBeSaveTickets = new ArrayList<AutoCats96CategoryTicket>();
			
			for (AutoCats96CategoryTicket catTix : categoryTixList) {
				
				if(null != catTix.getId()){
					if(catTix.getStatus().equals("ACTIVE")){
						tobeUpdatedTickets.add(catTix);
						continue;
					}
					tobeDeletedTickets.add(catTix);
					continue;
				}
				toBeSaveTickets.add(catTix);
			}
			
			Map<Integer, List<AutoCats96CategoryTicket>> brokersExistingTicketMap = new HashMap<Integer, List<AutoCats96CategoryTicket>>();
			
			for (AutoCats96CategoryTicket tix : tobeUpdatedTickets) {
				
				List<AutoCats96CategoryTicket> catTickets = brokersExistingTicketMap.get(tix.getTnBrokerId());
				if(null != catTickets && !catTickets.isEmpty()){
					
					if(catTickets.size() > brokerMaxTixCount){
						tobeDeletedTickets.add(tix);
						continue;
					}
					brokersExistingTicketMap.get(tix.getTnBrokerId()).add(tix);
				}else{
					catTickets = new ArrayList<AutoCats96CategoryTicket>();
					catTickets.add(tix);
					brokersExistingTicketMap.put(tix.getTnBrokerId(),catTickets);
				}
			}
			
			
			Map<String, List<AutoCats96CategoryTicket>> zonesCatTicketsMap =  new HashMap<String, List<AutoCats96CategoryTicket>>();
			
			Map<String, List<AutoCats96CategoryTicket>> duplicateSectionTicketsMap =  new HashMap<String, List<AutoCats96CategoryTicket>>();
			Set<String> duplicateSections = new HashSet<String>();
	    	
			Collections.sort(toBeSaveTickets, AutoCats96CategoryTicketUtil.sortingCategoryTicketbyPrice);
			
	    	for (AutoCats96CategoryTicket newTix : toBeSaveTickets) {
	    		List<AutoCats96CategoryTicket> zoneCatTickets = zonesCatTicketsMap.get(newTix.getTmatCategory());
				if(null != zoneCatTickets && !zoneCatTickets.isEmpty()){
					
					if(!duplicateSections.add(newTix.getSection())){
						
						List<AutoCats96CategoryTicket> duplicateTickets = duplicateSectionTicketsMap.get(newTix.getSection());
						
						if(null != duplicateTickets && !duplicateTickets.isEmpty()){
							duplicateSectionTicketsMap.get(newTix.getSection()).add(newTix);
						}else{
							duplicateTickets = new ArrayList<AutoCats96CategoryTicket>();
							duplicateTickets.add(newTix);
							duplicateSectionTicketsMap.put(newTix.getSection(), duplicateTickets);
						}
						continue;
					}
					zonesCatTicketsMap.get(newTix.getTmatCategory()).add(newTix);
				}else{
					zoneCatTickets = new ArrayList<AutoCats96CategoryTicket>();
					zoneCatTickets.add(newTix);
					zonesCatTicketsMap.put(newTix.getTmatCategory(), zoneCatTickets);
				}
				duplicateSections.add(newTix.getSection());
			}

	    	for (String key : zonesCatTicketsMap.keySet()) {
	    		Collections.sort(zonesCatTicketsMap.get(key), AutoCats96CategoryTicketUtil.sortingCategoryTicketbyPrice);
	    		
	    		for (AutoCats96CategoryTicket tix : zonesCatTicketsMap.get(key)) {
					
	    			System.out.println(key+", Section :"+tix.getSection()+", ROW :"+tix.getCatRow()+", Price :"+tix.getTnPrice());
	    			
				}
			}
	    	for (String key : duplicateSectionTicketsMap.keySet()) {
	    		Collections.sort(duplicateSectionTicketsMap.get(key), AutoCats96CategoryTicketUtil.sortingCategoryTicketbyPrice);
	    		
	    		for (AutoCats96CategoryTicket tix : duplicateSectionTicketsMap.get(key)) {
					
	    			System.out.println(key+", Section :"+tix.getSection()+", ROW :"+tix.getCatRow()+", Price :"+tix.getTnPrice());
	    			
				}
			}
	    	
	    	getCheapestCategoryTicketsByZone(zonesCatTicketsMap, duplicateSectionTicketsMap, brokersExistingTicketMap, tnBrokerIds,brokersCategoryTixsMap);
	    	
	    	//To Be Deleted Tickets because no ticket found in tmat.
			brokersCategoryTixsMap.put(0, tobeDeletedTickets);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return brokersCategoryTixsMap;
	}*/
	
	public static Map<Integer, List<CategoryTicket>> prioritizingCategoryTicketsByBrokerNew(Map<String,CategoryTicket> catTixFromDB,
			Map<String, CategoryTicket> catTixMap,Integer brokerMaxTixCount,Integer eventMaxTixCount,List<Integer> tnBrokerIds)throws Exception {
		Map<Integer,List<CategoryTicket>> brokersCategoryTixsMap = new HashMap<Integer, List<CategoryTicket>>();
		List<CategoryTicket> categoryTixList = new ArrayList<CategoryTicket>();
		try{
			
			String priceHistory="";
			String ticketIdHistory= "";
			String baseTicketOneHistory= "";
			String baseTicketTwoHistory="";
			String baseTicketThreeHistory="";
			String ticketHDateStr = dateTimeFormat.format(new Date());
			String priceHDateStr = dateTimeFormat.format(new Date());
			
			if(null != catTixFromDB && !catTixFromDB.isEmpty()){
				Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
				List<String> keys = new ArrayList<String>(catTixFromDB.keySet());
						
				for (String key : keys) {
					CategoryTicket dbTix =catTixFromDB.remove(key);
					CategoryTicket curTicket = catTixMap.remove(key);
					if(null != curTicket ){
						if(!curTicket.getActualPrice().equals(dbTix.getActualPrice())){
							priceHistory = priceHDateStr+"-"+curTicket.getActualPrice();
							if(dbTix.getPriceHistory() != null) {
								priceHistory = dbTix.getPriceHistory() +","+priceHistory;
								if(priceHistory.length()>=500) {
									priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
								}
							}
							dbTix.setPriceHistory(priceHistory);
							dbTix.setIsEdited(true);
							dbTix.setIsTnUpdate(true);
							dbTix.setActualPrice(curTicket.getActualPrice());
							dbTix.setTnPrice(curTicket.getTnPrice());
						}else if (!dbTix.getRowRange().equals(curTicket.getRowRange())){
							dbTix.setRowRange(curTicket.getRowRange());
							dbTix.setIsEdited(true);
						}else if(!curTicket.getBaseTicketOne().equals(dbTix.getBaseTicketOne()) || 
								!curTicket.getBaseTicketTwo().equals(dbTix.getBaseTicketTwo()) || 
								!curTicket.getBaseTicketThree().equals(dbTix.getBaseTicketThree())) {
							
							dbTix.setIsEdited(true);
							
						} /*else if (!dbTix.getTicketId().equals(curTicket.getTicketId())){
							ticketIdHistory = ticketHDateStr +"/"+ curTicket.getTicketId() +"/"+ curTicket.getPurPrice();
							if(dbTix.getTicketIdHistory() != null) {
								ticketIdHistory = dbTix.getTicketIdHistory()+","+ ticketIdHistory;
								if(ticketIdHistory.length()>= 500) {
									ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
								}
							}
							dbTix.setTicketIdHistory(ticketIdHistory);
							dbTix.setIsEdited(true);
						}*/
						
						if(!dbTix.getShippingMethodSpecialId().equals(curTicket.getShippingMethodSpecialId()) ||
								!dbTix.getNearTermOptionId().equals(curTicket.getNearTermOptionId()) ||
								dbTix.getExpectedArrivalDate().compareTo(curTicket.getExpectedArrivalDate()) != 0) {
							dbTix.setNearTermOptionId(curTicket.getNearTermOptionId());
							dbTix.setShippingMethodSpecialId(curTicket.getShippingMethodSpecialId());
							dbTix.setExpectedArrivalDate(curTicket.getExpectedArrivalDate());
							dbTix.setIsEdited(true);
							dbTix.setIsTnUpdate(true);
						}
						
						if(isPriceChangeFlag || !dbTix.getTicketId().equals(curTicket.getTicketId())) {
							ticketIdHistory = ticketHDateStr +"/"+ curTicket.getTicketId() +"/"+ curTicket.getPurPrice();
							if(dbTix.getTicketIdHistory() != null) {
								ticketIdHistory = dbTix.getTicketIdHistory()+","+ ticketIdHistory;
								if(ticketIdHistory.length()>= 500) {
									ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
								}
							}
							dbTix.setTicketIdHistory(ticketIdHistory);
							dbTix.setIsEdited(true);
						}
						
						
						//Base Ticket One
						tempTicketIdFlag = false;
						if (dbTix.getBaseTicketOne() != null && dbTix.getBaseTicketOne() != 0) {
							if(isPriceChangeFlag || !dbTix.getBaseTicketOne().equals(curTicket.getBaseTicketOne())) {
								tempTicketIdFlag = true;
							}
						} else if(curTicket.getBaseTicketOne() != null && curTicket.getBaseTicketOne() != 0) {
							tempTicketIdFlag = true;
						}
						
						if(tempTicketIdFlag) {
							baseTicketOneHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketOne() +"/"+ curTicket.getBaseTicketOnePurPrice();
							if(dbTix.getBaseTicketOneHistory() != null) {
								baseTicketOneHistory = dbTix.getBaseTicketOneHistory()+","+ baseTicketOneHistory;
														
								if(baseTicketOneHistory.length()>= 500) {
									baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
								}
							}
							dbTix.setBaseTicketOneHistory(baseTicketOneHistory);
							dbTix.setIsEdited(true);
						}
						
						// For Base Ticket 2
						tempTicketIdFlag = false;
						if (dbTix.getBaseTicketTwo() != null && dbTix.getBaseTicketTwo() != 0) {
							if(isPriceChangeFlag || !dbTix.getBaseTicketTwo().equals(curTicket.getBaseTicketTwo())){
								tempTicketIdFlag = true;
							}
						} else if(curTicket.getBaseTicketTwo() != null && curTicket.getBaseTicketTwo() != 0) {
							tempTicketIdFlag = true;
						}
						
						if(tempTicketIdFlag) {
							baseTicketTwoHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketTwo() +"/"+ curTicket.getBaseTicketTwoPurPrice();
							if(dbTix.getBaseTicketTwoHistory() != null) {
								baseTicketTwoHistory = dbTix.getBaseTicketTwoHistory()+","+ baseTicketTwoHistory;
														
								if(baseTicketTwoHistory.length()>= 500) {
									baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
								}
							}
							dbTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
							dbTix.setIsEdited(true);
						}
						
						
						// For Base Ticket 3
						tempTicketIdFlag = false;
						if (dbTix.getBaseTicketThree() != null && dbTix.getBaseTicketThree() != 0) {
							if(isPriceChangeFlag || !dbTix.getBaseTicketThree().equals(curTicket.getBaseTicketThree()))  {
								tempTicketIdFlag = true;
							}
						} else if(curTicket.getBaseTicketThree() != null && curTicket.getBaseTicketThree() != 0) {
							tempTicketIdFlag = true;
						}
						
						if(tempTicketIdFlag) {
							baseTicketThreeHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketThree() +"/"+ curTicket.getBaseTicketThreePurPrice();
							if(dbTix.getBaseTicketThreeHistory() != null) {
								baseTicketThreeHistory = dbTix.getBaseTicketThreeHistory()+","+ baseTicketThreeHistory;
														
								if(baseTicketThreeHistory.length()>= 500) {
									baseTicketThreeHistory = baseTicketThreeHistory.substring(baseTicketThreeHistory.indexOf(",")+1);	
								}
							}
							dbTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
							dbTix.setIsEdited(true);
						}
						
						dbTix.setBaseTicketOne(curTicket.getBaseTicketOne());
						dbTix.setBaseTicketTwo(curTicket.getBaseTicketTwo());
						dbTix.setBaseTicketThree(curTicket.getBaseTicketThree());
						dbTix.setCategoryId(curTicket.getCategoryId());
						dbTix.setTicketId(curTicket.getTicketId());
						dbTix.setItemId(curTicket.getItemId());
						dbTix.setCatRow(curTicket.getCatRow());
						dbTix.setLastRow(curTicket.getLastRow());
						//dbTix.setAlternateRow(curTicket.getAlternateRow());
						dbTix.setRowRange(curTicket.getRowRange());
						dbTix.setActualPrice(curTicket.getActualPrice());
						dbTix.setTnPrice(curTicket.getTnPrice());
						dbTix.setTmatCategory(curTicket.getTmatCategory());
						dbTix.setShippingMethodSpecialId(curTicket.getShippingMethodSpecialId());
						dbTix.setNearTermOptionId(curTicket.getNearTermOptionId());
						dbTix.setExpectedArrivalDate(curTicket.getExpectedArrivalDate());
						dbTix.setProductPriority(curTicket.getProductPriority());
						categoryTixList.add(dbTix);
					}else{
						dbTix.setStatus("DELETED");
						categoryTixList.add(dbTix);
					}
				}
			}
				
			if(null != catTixMap && !catTixMap.isEmpty()){
				
				for (CategoryTicket tix : catTixMap.values()) {
					
					priceHistory = priceHDateStr+"-"+tix.getActualPrice();
					tix.setPriceHistory(priceHistory);
					
					ticketIdHistory = ticketHDateStr +"/"+ tix.getTicketId() +"/"+ tix.getPurPrice();
					tix.setTicketIdHistory(ticketIdHistory);
					
					if(null != tix.getBaseTicketOne() && tix.getBaseTicketOne() > 0){
						baseTicketOneHistory = ticketHDateStr +"/"+ tix.getBaseTicketOne() +"/"+ tix.getBaseTicketOnePurPrice();
						tix.setBaseTicketOneHistory(baseTicketOneHistory);
					}
					
					if(null != tix.getBaseTicketTwo() && tix.getBaseTicketTwo() > 0){
						baseTicketTwoHistory = ticketHDateStr +"/"+ tix.getBaseTicketTwo() +"/"+ tix.getBaseTicketTwoPurPrice();
						tix.setBaseTicketTwoHistory(baseTicketTwoHistory);
					}
					
					if(null != tix.getBaseTicketThree() && tix.getBaseTicketThree() > 0){
						baseTicketThreeHistory = ticketHDateStr +"/"+ tix.getBaseTicketThree() +"/"+ tix.getBaseTicketThreePurPrice();
						tix.setBaseTicketThreeHistory(baseTicketThreeHistory);
					}
					
					categoryTixList.add(tix);
				}
			}
			
			List<CategoryTicket> tobeDeletedTickets = new ArrayList<CategoryTicket>();
			List<CategoryTicket> tobeUpdatedTickets = new ArrayList<CategoryTicket>();
			List<CategoryTicket> toBeSaveTickets = new ArrayList<CategoryTicket>();
			
			for (CategoryTicket catTix : categoryTixList) {
				
				if(null != catTix.getId() && catTix.getTnCategoryTicketGroupId() != null){
					if(catTix.getStatus().equals("ACTIVE")){
						tobeUpdatedTickets.add(catTix);
						continue;
					}
					tobeDeletedTickets.add(catTix);
					continue;
				}
				toBeSaveTickets.add(catTix);
			}
			
			Map<Integer, List<CategoryTicket>> brokersExistingTicketMap = new HashMap<Integer, List<CategoryTicket>>();
			
			for (CategoryTicket tix : tobeUpdatedTickets) {
				
				List<CategoryTicket> catTickets = brokersExistingTicketMap.get(tix.getTnBrokerId());
				if(null != catTickets && !catTickets.isEmpty()){
					
					if(catTickets.size() > brokerMaxTixCount){
						tobeDeletedTickets.add(tix);
						continue;
					}
					brokersExistingTicketMap.get(tix.getTnBrokerId()).add(tix);
				}else{
					catTickets = new ArrayList<CategoryTicket>();
					catTickets.add(tix);
					brokersExistingTicketMap.put(tix.getTnBrokerId(),catTickets);
				}
			}
			
			
			Map<String, List<CategoryTicket>> zonesCatTicketsMap =  new HashMap<String, List<CategoryTicket>>();
			
			Map<String, List<CategoryTicket>> duplicateSectionTicketsMap =  new HashMap<String, List<CategoryTicket>>();
			Set<String> duplicateSections = new HashSet<String>();
	    	
			Collections.sort(toBeSaveTickets, AutoCats96CategoryTicketUtil.sortingCategoryTicketbyPriceNew);
			
	    	for (CategoryTicket newTix : toBeSaveTickets) {
	    		List<CategoryTicket> zoneCatTickets = zonesCatTicketsMap.get(newTix.getTmatCategory());
				if(null != zoneCatTickets && !zoneCatTickets.isEmpty()){
					
					if(!duplicateSections.add(newTix.getSection())){
						
						List<CategoryTicket> duplicateTickets = duplicateSectionTicketsMap.get(newTix.getSection());
						
						if(null != duplicateTickets && !duplicateTickets.isEmpty()){
							duplicateSectionTicketsMap.get(newTix.getSection()).add(newTix);
						}else{
							duplicateTickets = new ArrayList<CategoryTicket>();
							duplicateTickets.add(newTix);
							duplicateSectionTicketsMap.put(newTix.getSection(), duplicateTickets);
						}
						continue;
					}
					zonesCatTicketsMap.get(newTix.getTmatCategory()).add(newTix);
				}else{
					zoneCatTickets = new ArrayList<CategoryTicket>();
					zoneCatTickets.add(newTix);
					zonesCatTicketsMap.put(newTix.getTmatCategory(), zoneCatTickets);
				}
				duplicateSections.add(newTix.getSection());
			}

	    	/*for (String key : zonesCatTicketsMap.keySet()) {
	    		Collections.sort(zonesCatTicketsMap.get(key), CategoryTicketUtil.sortingCategoryTicketbyPrice);
	    		
	    		for (CategoryTicket tix : zonesCatTicketsMap.get(key)) {
					
	    			System.out.println(key+", Section :"+tix.getSection()+", ROW :"+tix.getRowRange()+", Price :"+tix.getTnPrice());
	    			
				}
			}
	    	for (String key : duplicateSectionTicketsMap.keySet()) {
	    		Collections.sort(duplicateSectionTicketsMap.get(key), CategoryTicketUtil.sortingCategoryTicketbyPrice);
	    		
	    		for (CategoryTicket tix : duplicateSectionTicketsMap.get(key)) {
					
	    			System.out.println(key+", Section :"+tix.getSection()+", ROW :"+tix.getRowRange()+", Price :"+tix.getTnPrice());
	    			
				}
			}*/
	    	
	    	getCheapestCategoryTicketsByZoneNew(zonesCatTicketsMap, duplicateSectionTicketsMap, brokersExistingTicketMap, tnBrokerIds,brokersCategoryTixsMap);
	    	
	    	//To Be Deleted Tickets because no ticket found in tmat.
			brokersCategoryTixsMap.put(0, tobeDeletedTickets);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return brokersCategoryTixsMap;
	}
	
	
	
	public static Map<Integer,List<AutoCats96CategoryTicket>> getCheapestCategoryTicketsByZone(Map<String, List<AutoCats96CategoryTicket>> zonesCatTicketsMap,
			Map<String, List<AutoCats96CategoryTicket>> duplicateSectionTicketsMap,
			Map<Integer, List<AutoCats96CategoryTicket>> brokersExistingTicketMap,List<Integer> tnBrokerIds,
			Map<Integer,List<AutoCats96CategoryTicket>> brokersCategoryTixsMap,Map<Integer, Integer> brokerTixCountMap,Set<String> excludeZones)throws Exception{
		
		List<Integer> activeTnBrokerIds = new ArrayList<Integer>();
		activeTnBrokerIds.addAll(tnBrokerIds);
		//Collections.sort(activeTnBrokerIds, AutoCats96CategoryTicketUtil.sortingTnBrokerIds);
		Map<Integer,List<String>> brokersAllowedSectionsMap = new HashMap<Integer,List<String>>();
		Set<String> prohibitedSections = new HashSet<String>();
		
		for (Integer brokerId : activeTnBrokerIds) {
			
			List<String> allowedBrokerSections = brokersAllowedSectionsMap.get(brokerId);
			if(null ==allowedBrokerSections || allowedBrokerSections.isEmpty()){
				allowedBrokerSections = new ArrayList<String>();
			}
			List<AutoCats96CategoryTicket> existingCatTixs = brokersExistingTicketMap.get(brokerId);
			
			if(null != existingCatTixs && !existingCatTixs.isEmpty()){
				
				for (AutoCats96CategoryTicket dbCatTix : existingCatTixs) {
					allowedBrokerSections.add(dbCatTix.getSection());
					prohibitedSections.add(dbCatTix.getSection());
				}
				
				brokersCategoryTixsMap.put(brokerId, existingCatTixs);
			}else{
				brokersCategoryTixsMap.put(brokerId, new ArrayList<AutoCats96CategoryTicket>());
			}
			brokersAllowedSectionsMap.put(brokerId, allowedBrokerSections);
		}
		
		Map<String,AutoCats96CategoryTicket> zoneSectionMap = new HashMap<String,AutoCats96CategoryTicket>();
		for (String zone : zonesCatTicketsMap.keySet()) {
			
			List<AutoCats96CategoryTicket> zoneCatTickets = zonesCatTicketsMap.get(zone);
			
			if(null != zoneCatTickets && !zoneCatTickets.isEmpty()){
				Collections.sort(zoneCatTickets, AutoCats96CategoryTicketUtil.sortingCategoryTicketbyPrice);
				int i=1;
				for (AutoCats96CategoryTicket catTix : zoneCatTickets) {
					zoneSectionMap.put(i+":"+zone, catTix);
					i++;
				}
			}
		}
		
		Boolean maxLimitReached = false;
		Integer lastProcessedBrokerId = null,brokerMaxCount = 0;
		List<AutoCats96CategoryTicket> skippedCatTixs = new ArrayList<AutoCats96CategoryTicket>();
		
		List<String> brokerLevelZoneKeys = new ArrayList<String>();
		if(null != zoneSectionMap && zoneSectionMap.size() > 0){
			brokerLevelZoneKeys.addAll(zoneSectionMap.keySet());
			Collections.sort(brokerLevelZoneKeys);
		}
		
		for (String brokerLevelZone : brokerLevelZoneKeys) {
			
			AutoCats96CategoryTicket tix = zoneSectionMap.get(brokerLevelZone);
			
			if(prohibitedSections.contains(tix.getSection())){
				skippedCatTixs.add(tix);
				continue;
			}
			
			lastProcessedBrokerId = AutoCats96Utils.getNextAvailableBrokerId(lastProcessedBrokerId, activeTnBrokerIds);
			
			if(null == lastProcessedBrokerId){
				maxLimitReached= true;
				break;
			}
			
			//Tamil : Logic for skipping excluded zone tickets - begins
			if(excludeZones != null ) {
				Integer tempBrokerId = lastProcessedBrokerId;
				boolean isAllBrokerExcludedZone = false;
				while(true) {
					String exZoneKey = lastProcessedBrokerId+"_"+tix.getTmatZone().toUpperCase(); 
					if(excludeZones.contains(exZoneKey)) {
						lastProcessedBrokerId = AutoCats96Utils.getNextAvailableBrokerId(lastProcessedBrokerId, activeTnBrokerIds);
						if(tempBrokerId.equals(lastProcessedBrokerId)) {
							isAllBrokerExcludedZone = true;
							break;
						}
					} else {
						break;
					}
				}
				if(isAllBrokerExcludedZone) {
					continue;
				}
			}
			//Tamil : Logic for skipping excluded zone tickets - ends
			
			brokerMaxCount = brokerTixCountMap.get(lastProcessedBrokerId);
			
			if(brokersCategoryTixsMap.get(lastProcessedBrokerId).size() >= brokerMaxCount){
				break;
			}
			
			brokersAllowedSectionsMap.get(lastProcessedBrokerId).add(tix.getSection());
			
			//Tamil : don't process 6qty listings to RTW-2
			if(!lastProcessedBrokerId.equals(10) || !tix.getQuantity().equals(6)) {
				brokersCategoryTixsMap.get(lastProcessedBrokerId).add(tix);
			}
			
			if(brokersCategoryTixsMap.get(lastProcessedBrokerId).size() == brokerMaxCount){
				activeTnBrokerIds.remove(lastProcessedBrokerId);
			}
			prohibitedSections.add(tix.getSection());
		}
		
		if(!maxLimitReached){
			
			for (AutoCats96CategoryTicket tix : skippedCatTixs) {
				List<AutoCats96CategoryTicket> dupCatTixs = duplicateSectionTicketsMap.get(tix.getSection());
				if(null != dupCatTixs && !dupCatTixs.isEmpty()){
					duplicateSectionTicketsMap.get(tix.getSection()).add(tix);
					continue;
				}
				dupCatTixs = new ArrayList<AutoCats96CategoryTicket>();
				dupCatTixs.add(tix);
				duplicateSectionTicketsMap.put(tix.getSection(), dupCatTixs);
			}
			
			for (String section : duplicateSectionTicketsMap.keySet()) {
				Collections.sort(duplicateSectionTicketsMap.get(section), AutoCats96CategoryTicketUtil.sortingCategoryTicketbyPrice);
			}
			
			for (Integer brokerId : brokersCategoryTixsMap.keySet()) {
				
				List<AutoCats96CategoryTicket> brokerCatTickets = brokersCategoryTixsMap.get(brokerId);
				
				brokerMaxCount = brokerTixCountMap.get(brokerId);
				
				if(brokerCatTickets.size() >= brokerMaxCount){
					continue;
				}
				
				List<String> allowedBrokerSection = brokersAllowedSectionsMap.get(brokerId);
				
				List<String> duplicateSections = new ArrayList<String>(duplicateSectionTicketsMap.keySet());
				
				for (String section : duplicateSections) {
					
					if(allowedBrokerSection.contains(section)){
						
						List<AutoCats96CategoryTicket> duplicateTickets = duplicateSectionTicketsMap.get(section);
						List<AutoCats96CategoryTicket> toBeDeletedTickets = new ArrayList<AutoCats96CategoryTicket>();
						
						if(null == duplicateTickets || duplicateTickets.isEmpty()){
							continue;
						}
						for (AutoCats96CategoryTicket dubTix : duplicateTickets) {
							if(brokerCatTickets.size() < brokerMaxCount){
								
								//Tamil : Logic for skipping excluded zone tickets - begins 
								if(excludeZones != null) {
									String exZoneKey = brokerId+"_"+dubTix.getTmatZone().toUpperCase(); 
									if(excludeZones.contains(exZoneKey)) {
										continue;	
									}
								}
								//Tamil : Logic for skipping excluded zone tickets - ends
								
								//Tamil : don't process 6qty listings to RTW-2
								if(!brokerId.equals(10) || !dubTix.getQuantity().equals(6)) {
									brokerCatTickets.add(dubTix);
								}
							}
							toBeDeletedTickets.add(dubTix);
						}
						duplicateSectionTicketsMap.get(section).removeAll(toBeDeletedTickets);
					}
				}
				brokersCategoryTixsMap.put(brokerId,brokerCatTickets);
			}
		}
		return brokersCategoryTixsMap;
	}
	
	public static Comparator<Integer> sortingTnBrokerIds = new Comparator<Integer>() {

		public int compare(Integer ticket1, Integer ticket2) {
			int cmp= ticket1.compareTo(
					ticket2);
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			return ticket1.compareTo(ticket2);
	    }
		
	};
	
	public static Map<Integer,List<CategoryTicket>> getCheapestCategoryTicketsByZoneNew(Map<String, List<CategoryTicket>> zonesCatTicketsMap,
			Map<String, List<CategoryTicket>> duplicateSectionTicketsMap,
			Map<Integer, List<CategoryTicket>> brokersExistingTicketMap,List<Integer> tnBrokerIds,
			Map<Integer,List<CategoryTicket>> brokersCategoryTixsMap)throws Exception{
		
		List<Integer> activeTnBrokerIds = new ArrayList<Integer>();
		activeTnBrokerIds.addAll(tnBrokerIds);
		//Collections.sort(activeTnBrokerIds, AutoCats96CategoryTicketUtil.sortingTnBrokerIds);
		Map<Integer,List<String>> brokersAllowedSectionsMap = new HashMap<Integer,List<String>>();
		Set<String> prohibitedSections = new HashSet<String>();
		
		for (Integer brokerId : activeTnBrokerIds) {
			
			List<String> allowedBrokerSections = brokersAllowedSectionsMap.get(brokerId);
			if(null ==allowedBrokerSections || allowedBrokerSections.isEmpty()){
				allowedBrokerSections = new ArrayList<String>();
			}
			List<CategoryTicket> existingCatTixs = brokersExistingTicketMap.get(brokerId);
			
			if(null != existingCatTixs && !existingCatTixs.isEmpty()){
				
				for (CategoryTicket dbCatTix : existingCatTixs) {
					allowedBrokerSections.add(dbCatTix.getSection());
					prohibitedSections.add(dbCatTix.getSection());
				}
				
				brokersCategoryTixsMap.put(brokerId, existingCatTixs);
			}else{
				brokersCategoryTixsMap.put(brokerId, new ArrayList<CategoryTicket>());
			}
			brokersAllowedSectionsMap.put(brokerId, allowedBrokerSections);
		}
		
		Map<String,CategoryTicket> zoneSectionMap = new HashMap<String,CategoryTicket>();
		for (String zone : zonesCatTicketsMap.keySet()) {
			List<CategoryTicket> zoneCatTickets = zonesCatTicketsMap.get(zone);
			int i=1;
			for (CategoryTicket catTix : zoneCatTickets) {
				zoneSectionMap.put(i+":"+zone, catTix);
				i++;
			}
		}
		
		Boolean maxLimitReached = false;
		Integer lastProcessedBrokerId = null;
		
		for (String brokerLevelZone : zoneSectionMap.keySet()) {
			
			CategoryTicket tix = zoneSectionMap.get(brokerLevelZone);
			
			if(prohibitedSections.contains(tix.getSection())){
				continue;
			}
			
			lastProcessedBrokerId = AutoCats96Utils.getNextAvailableBrokerId(lastProcessedBrokerId, activeTnBrokerIds);
			
			if(null == lastProcessedBrokerId){
				maxLimitReached= true;
				break;
			}
			
			if(brokersCategoryTixsMap.get(lastProcessedBrokerId).size() >= 32){
				break;
			}
			
			brokersAllowedSectionsMap.get(lastProcessedBrokerId).add(tix.getSection());
			brokersCategoryTixsMap.get(lastProcessedBrokerId).add(tix);
			if(brokersCategoryTixsMap.get(lastProcessedBrokerId).size() == 32){
				activeTnBrokerIds.remove(lastProcessedBrokerId);
			}
		}
		
		if(!maxLimitReached){
			
			for (String section : duplicateSectionTicketsMap.keySet()) {
				Collections.sort(duplicateSectionTicketsMap.get(section), AutoCats96CategoryTicketUtil.sortingCategoryTicketbyPriceNew);
			}
			
			for (Integer brokerId : brokersCategoryTixsMap.keySet()) {
				
				List<CategoryTicket> brokerCatTickets = brokersCategoryTixsMap.get(brokerId);
				
				if(brokerCatTickets.size() >= 32){
					continue;
				}
				
				List<String> allowedBrokerSection = brokersAllowedSectionsMap.get(brokerId);
				
				List<String> duplicateSections = new ArrayList<String>(duplicateSectionTicketsMap.keySet());
				
				for (String section : duplicateSections) {
					
					if(allowedBrokerSection.contains(section)){
						
						List<CategoryTicket> duplicateTickets = duplicateSectionTicketsMap.get(section);
						List<CategoryTicket> toBeDeletedTickets = new ArrayList<CategoryTicket>();
						
						if(null == duplicateTickets || duplicateTickets.isEmpty()){
							continue;
						}
						for (CategoryTicket dubTix : duplicateTickets) {
							if(brokerCatTickets.size() < 32){
								brokerCatTickets.add(dubTix);
							}
							toBeDeletedTickets.add(dubTix);
						}
						duplicateSectionTicketsMap.get(section).removeAll(toBeDeletedTickets);
					}
				}
				brokersCategoryTixsMap.put(brokerId,brokerCatTickets);
			}
		}
		return brokersCategoryTixsMap;
	}
	
	
	
 
 public static Comparator<AutoCats96CategoryTicket> sortingCategoryTicketbyPrice = new Comparator<AutoCats96CategoryTicket>() {

		public int compare(AutoCats96CategoryTicket ticket1, AutoCats96CategoryTicket ticket2) {
			int cmp= ticket1.getTnPrice().compareTo(
					ticket2.getTnPrice());
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			if(null != ticket1.getProductPriority() && null != ticket2.getProductPriority()){
				return ticket1.getProductPriority().compareTo(
						ticket2.getProductPriority());
			}
			return cmp;
			
	    }
		
	};
	
	public static Comparator<CategoryTicket> sortingCategoryTicketbyPriceNew = new Comparator<CategoryTicket>() {

		public int compare(CategoryTicket ticket1, CategoryTicket ticket2) {
			int cmp= ticket1.getTnPrice().compareTo(
					ticket2.getTnPrice());
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			return ticket1.getProductPriority().compareTo(
					ticket2.getProductPriority());
	    }
		
	};
	
	/*public static void main(String[] args) {
		
		Map<String,String> zoneSectionMap = new HashMap<String,String>();
		zoneSectionMap.put("1:A", "1:A");
		zoneSectionMap.put("2:A", "2:A");
		zoneSectionMap.put("1:B", "1:B");
		zoneSectionMap.put("2:B", "2:B");
		zoneSectionMap.put("3:A", "3:A");
		zoneSectionMap.put("1:C", "1:C");
		zoneSectionMap.put("1:D", "1:D");
		
		
		for (String string : new TreeSet<String>(zoneSectionMap.keySet())) {
			System.out.println(string);
		}
		
		
	}*/
}
