package com.rtw.autopricing.util;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.autopricing.util.common.SharedProperty;
import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEvent;
import com.rtw.autopricing.ftp.data.AutoCats96ExchangeEventAudit;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.AutopricingVenueSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;
import com.rtw.autopricing.ftp.data.EventDTO;
import com.rtw.autopricing.ftp.data.ExcludeVenues;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEventAudit;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.LastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ManhattanZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.MiniExchangeEvent;
import com.rtw.autopricing.ftp.data.MiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ParentCategory;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEventAudit;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEvent;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsExchangeEventAudit;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEvent;
import com.rtw.autopricing.ftp.data.SGLastFiveRowExchangeEventAudit;
import com.rtw.autopricing.ftp.data.TicketListingCrawl;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEvent;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEventAudit;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.TixCityZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.VipMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneLastRowMiniExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEvent;
import com.rtw.autopricing.ftp.data.ZonePricingExchangeEventAudit;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEvent;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorExchangeEventAudit;



public class AutoExchangeEventLoader extends QuartzJobBean implements StatefulJob {
	public static Integer MINICATS_PRODUCT_ID=3;
	public static Integer VIP_MINICATS_PRODUCT_ID=4;
	public static Integer LASTROW_MINICATS_PRODUCT_ID=5;
	public static Integer LARRYLAST_PRODUCT_ID=6;
	public static Integer ZONE_LASTROW_MINICATS_PRODUCT_ID=7;
	public static Integer ZONES_PRICNG_PRODUCT_ID=8;
	public static Integer ZONE_TICKETS_PROCESSOR_PRODUCT_ID=9;
	public static Integer AUTOCATS96_PRODUCT_ID=10;
	public static Integer PRESALE_AUTOCAT_PRODUCT_ID=11;
	public static Integer TN_SPECIAL_PRODUCT_ID=13;
	public static Integer TIXCITY_ZONES_PRICNG_PRODUCT_ID=14;
	public static Integer MANHATTAN_ZONES_PRICNG_PRODUCT_ID=15;
	public static Integer VIPLASTROW_MINICATS_PRODUCT_ID=16;
	public static Integer PRESALE_ZONE_TICKETS_PRODUCT_ID=17;
	public static Integer SG_LAST_FIVE_ROW_PRODUCT_ID=19;
	
	//public static Integer TIXCITY_BROKER_ID=7;
	//public static Integer POS_TIXCITY_BROKER_ID=166;
	
	public static Integer TIXCITY_BROKER_ID=11;
	public static Integer POS_TIXCITY_BROKER_ID=11499;
	
	public static String LARRYLAST_INTERNAL_NOTES="LarryLast NOSTUB NOTNOW NOVIVID NOTEVO";
	public static String ZONED_LASTROW_INTERNAL_NOTES="ZLR NOSTUB NOTNOW NOVIVID NOTEVO";
	
	private static Logger log = LoggerFactory.getLogger(AutoExchangeEventLoader.class);
	public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	private static SharedProperty sharedProperty;
	private boolean running;
	
	public static SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	public final void setSharedProperty(SharedProperty sharedProperty) {
		AutoExchangeEventLoader.sharedProperty = sharedProperty;
	}
	@Override 
	public void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		
		try {
			if(!running){
				running = true;
				
				System.out.println("Autopricing Event Loader Job Started....."+new Date());
				Calendar cal = Calendar.getInstance();
				
				Integer count = DAORegistry.getQueryManagerDAO().insertDataintoTmatInduxEventDetailsTable();
				System.out.println("Autopricing TAMT Indux Event insert count : "+count +" : "+new Date());
				
				autoExchangeEventLoadProcess();
				updateDeletedTMATEventsExchangeEventStatus();
				
				//Tamil : exculte autoexchange event updater process for every 4 hours once
				//if(cal.get(Calendar.HOUR_OF_DAY) % 4 ==0 && 
					//	cal.get(Calendar.MINUTE) < 15) {
					System.out.println("Autopricing Event Updater Job started....."+new Date());
					
					AutoExchangeEventUpdater.autoExchangeEventUpdateProcess();
					
					System.out.println("Autopricing Event Updater Job Completed....."+new Date());
				//}
				
					//update if event is with 0% markup then change crawl frequency
					updatePresaleSetingsEventsCrawlFrequencyinTMAT();
					
				running = false;
				System.out.println("Autopricing Event Loader Job Completed....."+new Date());
			}
		} catch(Exception e) {
			running = false;
			e.printStackTrace();
			
			try {
				ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
				String toAddress = resourceBundle.getString("emailNotificationTo");
				String ccAddress= resourceBundle.getString("emailNotificationCCTo");
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("example", ""+e.fillInStackTrace());
				
				String subject = "Auto Exchange event loader job failed :";
				String fileName = "templates/auto-exchange-event-loader-job-failure-message.txt";
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
			} catch (Exception e1) {
				log.error("Error while sending email in auto exchagne event loader job.");
				e1.printStackTrace();
			}
			
		}
	}
	
	public static void autoExchangeEventLoadProcess() throws Exception {
		
		//System.out.println("running...in ExchangeEventLoader");
			
			try{
				List<EventDTO> eventDTOs = DAORegistry.getQueryManagerDAO().getAutopricingAutoEvents();
				Map<String, Integer> parentCategoryMap = new HashMap<String, Integer>();
				try{
					List<ParentCategory> tourCategories = DAORegistry.getParentCategoryDAO().getAllTourCategoryOrderByName();
					
					for (ParentCategory tourCategory : tourCategories) {
						if(tourCategory.getName().equals("Theater")){ //THEATRE 
							tourCategory.setName("Theatre");
						}
						parentCategoryMap.put(tourCategory.getName().toUpperCase(), tourCategory.getId());
					}
				}catch (Exception e) {
					e.printStackTrace();
					throw e;
					
				}
				
				
				if(eventDTOs != null && !eventDTOs.isEmpty()){
					//System.out.println("event size........."+eventDTOs.size());
					
					/*List<Broker> brokerList = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
					Map<Integer,Broker> brokerMap = new HashMap<Integer, Broker>();
					for (Broker broker : brokerList) {
						brokerMap.put(broker.getId(),broker);
					}*/
					
					Map<Integer,Boolean> activeProductsMap = new HashMap<Integer, Boolean>();
					List<AutopricingProduct> autopricingProducts = DAORegistry.getAutopricingProductDAO().getAllActiveProducts();
					for (AutopricingProduct autopricingProduct : autopricingProducts) {
						activeProductsMap.put(autopricingProduct.getId(), Boolean.TRUE);
					}
					
					Collection<DefaultAutoPricingProperties> defaultAutopricingPropertiesFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getAll();
					Map<String,DefaultAutoPricingProperties> defautlApPropertiesMap = new HashMap<String, DefaultAutoPricingProperties>();
					//Map<String,DefaultAutoPricingProperties> posDefautlApPropertiesMap = new HashMap<String, DefaultAutoPricingProperties>();
					for (DefaultAutoPricingProperties defaultAutopricing : defaultAutopricingPropertiesFromDB) {
						String key = defaultAutopricing.getProductId()+"_"+defaultAutopricing.getParentCategoryId();
						defautlApPropertiesMap.put(key,defaultAutopricing);
						
						//Getting Default auto pricing properties based on TN parent category
						//posDefautlApPropertiesMap.put(defaultAutopricing.getProductId()+"_"+defaultAutopricing.getParentCategory().getTnParentCategoryId(), defaultAutopricing);
					}
					
					Map<Integer,PresaleAutoCatExchangeEvent> existingPresaleAutoExEventMap = new HashMap<Integer, PresaleAutoCatExchangeEvent>();
					if(activeProductsMap.get(PRESALE_AUTOCAT_PRODUCT_ID) != null) {
						Collection<PresaleAutoCatExchangeEvent> existingPresaleAutoExEvents = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAll();
						if(existingPresaleAutoExEvents != null) {
							for (PresaleAutoCatExchangeEvent presaleAutoExEvent : existingPresaleAutoExEvents) {
								existingPresaleAutoExEventMap.put(presaleAutoExEvent.getEventId(), presaleAutoExEvent);
							}	
						}
					}
					
					Map<Integer,MiniExchangeEvent> existingMiniExEventMap = new HashMap<Integer, MiniExchangeEvent>();
					if(activeProductsMap.get(MINICATS_PRODUCT_ID) != null) {
						Collection<MiniExchangeEvent> existingMiniExEvents = DAORegistry.getMiniExchangeEventDAO().getAll();
						if(existingMiniExEvents != null) {
							for (MiniExchangeEvent miniExEvent : existingMiniExEvents) {
								existingMiniExEventMap.put(miniExEvent.getEventId(), miniExEvent);
							}	
						}
					}
					
					Map<Integer,SGLastFiveRowExchangeEvent> existingSgLastFiveRowExEventMap = new HashMap<Integer, SGLastFiveRowExchangeEvent>();
					if(activeProductsMap.get(SG_LAST_FIVE_ROW_PRODUCT_ID) != null) {
						Collection<SGLastFiveRowExchangeEvent> existingSgLastFiveRowExEvents = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAll();
						if(existingSgLastFiveRowExEvents != null) {
							for (SGLastFiveRowExchangeEvent miniExEvent : existingSgLastFiveRowExEvents) {
								existingSgLastFiveRowExEventMap.put(miniExEvent.getEventId(), miniExEvent);
							}	
						}
					}
					
					Map<Integer,VipMiniExchangeEvent> existingVipMiniExEventMap = new HashMap<Integer, VipMiniExchangeEvent>();
					if(activeProductsMap.get(VIP_MINICATS_PRODUCT_ID) != null) {
						Collection<VipMiniExchangeEvent> existingVipMiniExEvents = DAORegistry.getVipMiniExchangeEventDAO().getAll();
						if(existingVipMiniExEvents != null) {
							for (VipMiniExchangeEvent vipMiniExEvent : existingVipMiniExEvents) {
								existingVipMiniExEventMap.put(vipMiniExEvent.getEventId(), vipMiniExEvent);
							}	
						}
					}
					Map<Integer,LastRowMiniExchangeEvent> existingLastRowMiniExEventMap = new HashMap<Integer, LastRowMiniExchangeEvent>();
					if(activeProductsMap.get(LASTROW_MINICATS_PRODUCT_ID) != null) {
						Collection<LastRowMiniExchangeEvent> existingLastRowMiniExEvents = DAORegistry.getLastRowMiniExchangeEventDAO().getAll();
						if(existingLastRowMiniExEvents != null) {
							for (LastRowMiniExchangeEvent lastRowMiniExEvent : existingLastRowMiniExEvents) {
								existingLastRowMiniExEventMap.put(lastRowMiniExEvent.getEventId(), lastRowMiniExEvent);
							}	
						}
					}
					Map<Integer,ZonePricingExchangeEvent> existingZonePricingExEventMap = new HashMap<Integer, ZonePricingExchangeEvent>();
					if(activeProductsMap.get(ZONES_PRICNG_PRODUCT_ID) != null) {
						Collection<ZonePricingExchangeEvent> existingZonePricingExEvents = DAORegistry.getZonePricingExchangeEventDAO().getAll();
						if(existingZonePricingExEvents != null) {
							for (ZonePricingExchangeEvent zonePricingMiniExEvent : existingZonePricingExEvents) {
								existingZonePricingExEventMap.put(zonePricingMiniExEvent.getEventId(), zonePricingMiniExEvent);
							}	
						}
					}
					Map<Integer,TixCityZonePricingExchangeEvent> existingTixCityZonePricingExEventMap = new HashMap<Integer, TixCityZonePricingExchangeEvent>();
					if(activeProductsMap.get(TIXCITY_ZONES_PRICNG_PRODUCT_ID) != null) {
						Collection<TixCityZonePricingExchangeEvent> existingTixCityZonePricingExEvents = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAll();
						if(existingTixCityZonePricingExEvents != null) {
							for (TixCityZonePricingExchangeEvent tixCityZonePricingMiniExEvent : existingTixCityZonePricingExEvents) {
								existingTixCityZonePricingExEventMap.put(tixCityZonePricingMiniExEvent.getEventId(), tixCityZonePricingMiniExEvent);
							}	
						}
					}
					Map<Integer,ManhattanZonePricingExchangeEvent> existingManhattanZonePricingExEventMap = new HashMap<Integer, ManhattanZonePricingExchangeEvent>();
					if(activeProductsMap.get(MANHATTAN_ZONES_PRICNG_PRODUCT_ID) != null) {
						Collection<ManhattanZonePricingExchangeEvent> existingManhattanZonePricingExEvents = DAORegistry.getManhattanZonePricingExchangeEventDAO().getAll();
						if(existingManhattanZonePricingExEvents != null) {
							for (ManhattanZonePricingExchangeEvent manhattanZonePricingMiniExEvent : existingManhattanZonePricingExEvents) {
								existingManhattanZonePricingExEventMap.put(manhattanZonePricingMiniExEvent.getEventId(), manhattanZonePricingMiniExEvent);
							}	
						}
					}
					
					Map<Integer,LarryLastExchangeEvent> existingLarryLastExEventMap = new HashMap<Integer, LarryLastExchangeEvent>();
					if(activeProductsMap.get(LARRYLAST_PRODUCT_ID) != null) {
						Collection<LarryLastExchangeEvent> existingLarryLastExEvents = DAORegistry.getLarryLastExchangeEventDAO().getAll();
						if(existingLarryLastExEvents != null) {
							for (LarryLastExchangeEvent larryLastExEvent : existingLarryLastExEvents) {
								existingLarryLastExEventMap.put(larryLastExEvent.getEventId(), larryLastExEvent);
							}	
						}
					}
					Map<Integer,ZoneLastRowMiniExchangeEvent> existingZoneLastRowMiniExEventMap = new HashMap<Integer, ZoneLastRowMiniExchangeEvent>();
					if(activeProductsMap.get(ZONE_LASTROW_MINICATS_PRODUCT_ID) != null) {
						Collection<ZoneLastRowMiniExchangeEvent> existingZoneLastRowMiniExEvents = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAll();
						if(existingZoneLastRowMiniExEvents != null) {
							for (ZoneLastRowMiniExchangeEvent larryLastExEvent : existingZoneLastRowMiniExEvents) {
								existingZoneLastRowMiniExEventMap.put(larryLastExEvent.getEventId(), larryLastExEvent);
							}	
						}
					}
					Map<Integer,ZoneTicketsProcessorExchangeEvent> existingZoneTicketsProcessorExEventMap = new HashMap<Integer, ZoneTicketsProcessorExchangeEvent>();
					if(activeProductsMap.get(ZONE_TICKETS_PROCESSOR_PRODUCT_ID) != null) {
						Collection<ZoneTicketsProcessorExchangeEvent> existingZoneTicketsProcessorExEvents = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAll();
						if(existingZoneTicketsProcessorExEvents != null) {
							for (ZoneTicketsProcessorExchangeEvent larryLastExEvent : existingZoneTicketsProcessorExEvents) {
								existingZoneTicketsProcessorExEventMap.put(larryLastExEvent.getEventId(), larryLastExEvent);
							}	
						}
					}
					
					// Ulaganathan : Added for Presale Zone Tickets
					Map<Integer,PresaleZoneTicketsExchangeEvent> existingPresaleZoneTicketsExEventMap = new HashMap<Integer, PresaleZoneTicketsExchangeEvent>();
					if(activeProductsMap.get(PRESALE_ZONE_TICKETS_PRODUCT_ID) != null) {
						Collection<PresaleZoneTicketsExchangeEvent> existingPresaleZoneTicketsExEvents = DAORegistry.getPresaleZoneTicketsExchangeEventDAO().getAll();
						if(existingPresaleZoneTicketsExEvents != null) {
							for (PresaleZoneTicketsExchangeEvent larryLastExEvent : existingPresaleZoneTicketsExEvents) {
								existingPresaleZoneTicketsExEventMap.put(larryLastExEvent.getEventId(), larryLastExEvent);
							}	
						}
					}
					
					Map<Integer,AutoCats96ExchangeEvent> existingAutoCats96ProcessorExEventMap = new HashMap<Integer, AutoCats96ExchangeEvent>();
					if(activeProductsMap.get(AUTOCATS96_PRODUCT_ID) != null) {
						Collection<AutoCats96ExchangeEvent> existingAutoCats96ProcessorExEvents = DAORegistry.getAutoCats96ExchangeEventDAO().getAll();
						if(existingAutoCats96ProcessorExEvents != null) {
							for (AutoCats96ExchangeEvent larryLastExEvent : existingAutoCats96ProcessorExEvents) {
								existingAutoCats96ProcessorExEventMap.put(larryLastExEvent.getEventId(), larryLastExEvent);
							}	
						}
					}
					
					Map<Integer,TicketNetworkSSAccountExchangeEvent> existingTNSpecialExEventMap = new HashMap<Integer, TicketNetworkSSAccountExchangeEvent>();
					if(activeProductsMap.get(TN_SPECIAL_PRODUCT_ID) != null) {
						Collection<TicketNetworkSSAccountExchangeEvent> existingTNSpecialExEvents = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAll();
						if(existingTNSpecialExEvents != null) {
							for (TicketNetworkSSAccountExchangeEvent ssAccountExchangeEvent : existingTNSpecialExEvents) {
								existingTNSpecialExEventMap.put(ssAccountExchangeEvent.getEventId(), ssAccountExchangeEvent);
							}	
						}
					}
					
					// Mehul : Added for vip last row
					Map<Integer,VipLastRowMiniExchangeEvent> existingVipLastRowMiniExEventMap = new HashMap<Integer, VipLastRowMiniExchangeEvent>();
					if(activeProductsMap.get(VIPLASTROW_MINICATS_PRODUCT_ID) != null) {
						Collection<VipLastRowMiniExchangeEvent> existingVipLastRowMiniExEvents = DAORegistry.getVipLastRowMiniExchangeEventDAO().getAll();
						if(existingVipLastRowMiniExEvents != null) {
							for (VipLastRowMiniExchangeEvent vipLastRowMiniExEvent : existingVipLastRowMiniExEvents) {
								existingVipLastRowMiniExEventMap.put(vipLastRowMiniExEvent.getEventId(), vipLastRowMiniExEvent);
							}	
						}
					}
					
					Map<String,ExcludeVenues> excludeEventVenueMap = new HashMap<String,ExcludeVenues>();
					Collection<ExcludeVenues> excludeVenueList =DAORegistry.getExcludeVenuesDAO().getAll();
					if(excludeVenueList!=null){
						for (ExcludeVenues excludeVenues : excludeVenueList) {
							String key=excludeVenues.getProductId()+"_"+excludeVenues.getBrokerId()+"_"+excludeVenues.getVenueId();
							excludeEventVenueMap.put(key, excludeVenues);
						}
					}
					//Tamil : get shipping method and nearterm display option from venue level settings
					Map<String,AutopricingVenueSettings> autopricingVenuesettingsMap = new HashMap<String,AutopricingVenueSettings>();
					Collection<AutopricingVenueSettings> autopricingVenueSettings = DAORegistry.getAutopricingVenueSettingsDAO().getAllActiveAutopricingVenueSettings();
					if(excludeVenueList!=null){
						for (AutopricingVenueSettings apVenueSetting : autopricingVenueSettings) {
							String key=apVenueSetting.getProductId()+"_"+apVenueSetting.getVenueId();
							autopricingVenuesettingsMap.put(key, apVenueSetting);
						}
					}
					
					
					List<PresaleAutoCatExchangeEvent> presaleAutoCatExEvents = new ArrayList<PresaleAutoCatExchangeEvent>();
					List<PresaleAutoCatExchangeEventAudit> presaleAutoCatExEventAudits = new ArrayList<PresaleAutoCatExchangeEventAudit>();
					
					List<MiniExchangeEvent> miniExEvents = new ArrayList<MiniExchangeEvent>();
					List<MiniExchangeEventAudit> miniExEventAudits = new ArrayList<MiniExchangeEventAudit>();
					
					List<SGLastFiveRowExchangeEvent> sgLastFiveRowExEvents = new ArrayList<SGLastFiveRowExchangeEvent>();
					List<SGLastFiveRowExchangeEventAudit> sgLastFiveRowExEventAudits = new ArrayList<SGLastFiveRowExchangeEventAudit>();

					List<VipMiniExchangeEvent> vipMiniExEvents = new ArrayList<VipMiniExchangeEvent>();
					List<VipMiniExchangeEventAudit> vipMiniExEventAudits = new ArrayList<VipMiniExchangeEventAudit>();
					
					List<LastRowMiniExchangeEvent> lastRowMiniExEvents = new ArrayList<LastRowMiniExchangeEvent>();
					List<LastRowMiniExchangeEventAudit> lastRowMiniExEventAudits = new ArrayList<LastRowMiniExchangeEventAudit>();
					
					List<LarryLastExchangeEvent> larryLastExEvents = new ArrayList<LarryLastExchangeEvent>();
					List<LarryLastExchangeEventAudit> larryLastExEventAudits = new ArrayList<LarryLastExchangeEventAudit>();
					
					List<ZoneLastRowMiniExchangeEvent> zoneLastRowMiniExEvents = new ArrayList<ZoneLastRowMiniExchangeEvent>();
					List<ZoneLastRowMiniExchangeEventAudit> zoneLastRowMiniExEventAudits = new ArrayList<ZoneLastRowMiniExchangeEventAudit>();
					
					List<ZonePricingExchangeEvent> zonePricingExEvents = new ArrayList<ZonePricingExchangeEvent>();
					List<ZonePricingExchangeEventAudit> zonePricingExEventAudits = new ArrayList<ZonePricingExchangeEventAudit>();
					
					List<TixCityZonePricingExchangeEvent> tixCityZonePricingExEvents = new ArrayList<TixCityZonePricingExchangeEvent>();
					List<TixCityZonePricingExchangeEventAudit> tixCityZonePricingExEventAudits = new ArrayList<TixCityZonePricingExchangeEventAudit>();
					
					List<ManhattanZonePricingExchangeEvent> manhattanZonePricingExEvents = new ArrayList<ManhattanZonePricingExchangeEvent>();
					List<ManhattanZonePricingExchangeEventAudit> manhattanZonePricingExEventAudits = new ArrayList<ManhattanZonePricingExchangeEventAudit>();
					
					List<ZoneTicketsProcessorExchangeEvent> zoneTicketsProcessorExEvents = new ArrayList<ZoneTicketsProcessorExchangeEvent>();
					List<ZoneTicketsProcessorExchangeEventAudit> zoneTicketsProcessorExEventAudits = new ArrayList<ZoneTicketsProcessorExchangeEventAudit>();
					
					List<PresaleZoneTicketsExchangeEvent> presaleZoneTicketsExEvents = new ArrayList<PresaleZoneTicketsExchangeEvent>();
					List<PresaleZoneTicketsExchangeEventAudit> presaleZoneTicketsExEventAudits = new ArrayList<PresaleZoneTicketsExchangeEventAudit>();
					
					List<AutoCats96ExchangeEvent> autoCats96ExEvents = new ArrayList<AutoCats96ExchangeEvent>();
					List<AutoCats96ExchangeEventAudit> autoCats96ExEventAudits = new ArrayList<AutoCats96ExchangeEventAudit>();
					
					List<TicketNetworkSSAccountExchangeEvent> tnSpecialExEvents = new ArrayList<TicketNetworkSSAccountExchangeEvent>();
					List<TicketNetworkSSAccountExchangeEventAudit> tnSpecialExEventAudits = new ArrayList<TicketNetworkSSAccountExchangeEventAudit>();
					
					List<VipLastRowMiniExchangeEvent> vipLastRowMiniExEvents = new ArrayList<VipLastRowMiniExchangeEvent>();
					List<VipLastRowMiniExchangeEventAudit> vipLastRowMiniExEventAudits = new ArrayList<VipLastRowMiniExchangeEventAudit>();
					
					PresaleAutoCatExchangeEvent presaleAutoCatExchangeEvent = null; 
					MiniExchangeEvent miniExEvent = null;
					SGLastFiveRowExchangeEvent sgLastFiveRowExEvent = null;
					VipMiniExchangeEvent vipMiniExEvent = null;
					LastRowMiniExchangeEvent lastRowMiniExEvent = null;
					LarryLastExchangeEvent larryLastExEvent = null;
					ZoneLastRowMiniExchangeEvent zoneLastRowMiniExEvent = null;
					ZonePricingExchangeEvent zonePricingExEvent = null;
					TixCityZonePricingExchangeEvent tixCityZonePricingExEvent = null;
					ManhattanZonePricingExchangeEvent manhattanZonePricingExEvent = null;
					ZoneTicketsProcessorExchangeEvent zoneTicketsProcessorExEvent = null;
					PresaleZoneTicketsExchangeEvent presaleZoneTicketsExchangeEvent = null;
					AutoCats96ExchangeEvent autoCats96ExchangeEvent= null;
					TicketNetworkSSAccountExchangeEvent tnSpecialExEvent=null;
					VipLastRowMiniExchangeEvent vipLastRowMiniExEvent = null;
					
//					AutopricingProduct zoneTixProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RewardTheFan Listings");
//					AutopricingExchange zoneTicketExchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("RewardTheFan");
//					Broker zoneTixBroker = DAORegistry.getBrokerDAO().getBrokerByName("MZTix");
//					AutopricingSettings autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(zoneTixBroker.getId(),zoneTixProduct.getId(), zoneTicketExchange.getId());
					Integer zoneTixCommonExcludeHoursBeforeEvent = Integer.parseInt(DAORegistry.getPropertyDAO().get("zonetickets.exclude.hours.before.event").getValue());
//					if(autopricingSettings != null && autopricingSettings.getExcludeEventDays() != null) {
//						zoneTixCommonExcludeHoursBeforeEvent = autopricingSettings.getExcludeEventDays() * 24;				 
//					 } 
					
					DefaultAutoPricingProperties defaultApProerties = null;
					String key = null;
					Integer zoonesPricingParentCatId = 0;
					String username = "AUTO";//SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
					//File mapFile = null;
					Property property = DAORegistry.getPropertyDAO().get("zone_tickets_discount_zone_disc_perc");
					
					Double zoneDiscountPerc = Double.valueOf(property.getValue());
					int presaleEventCount = 0;
				//	List<Integer> autopricingUncheckedVenues =DAORegistry.getQueryManagerDAO().getAllVenuesTobeRemovedFromAutopricingProducts();

					//Tamil : 09/17/2018 changed presale settings from 7 days to 0(event created day only to be a presale day) days as per amit request for RTW
					//Tamil : 08/27/2018 changed presale settings from 5 days to 7 days as per amit request for RTW
					//Tamil : 06/20/2018 changed presale settings from 10 days to 5 days as per amit request
					//Tamil : 05/23/2018 changed presale settings from 7 days to 10 days as per amit request
					//Tamil : 05/08/2018 changed presale settings from 4 days to 7 days as per amit request
					Integer presaleSettingDays = 0;
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DATE, -presaleSettingDays);
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					Date presaleSettingDate = new Date(cal.getTimeInMillis());
					
					
					
					for(EventDTO eventDTO:eventDTOs ){
						/*if(eventDTO.getGrandChildCategoryId()==null){
							continue;
						}*/
						
					//	Collection<ExcludeVenues> excludeVenueList =DAORegistry.getExcludeVenuesDAO().getExcludeVenuesByVenueId(eventDTO.getVenueId());
						
						if(eventDTO.getVenueCategoryId()==null || eventDTO.getNoOfCrawls()==0){
							continue;
						}
						/*if(eventDTO.getAdmitoneId() == null) {
							continue;
						}*/
						
						boolean isEventExistinPOS = false,isZoneEvent = false;
						
						if(eventDTO.getPosExchangeEventId() != null ) {
							isEventExistinPOS = true;
						}
						if(eventDTO.getIsZoneEvent() != null && eventDTO.getIsZoneEvent()) {
							isZoneEvent = true;
						}
						
						/*Broker broker = brokerMap.get(2);
						if(eventDTO.getParentCategoryId().equals(3) || eventDTO.getParentCategoryId().equals(4)) {
							broker = brokerMap.get(3);
						}*/
						
						Integer posBasedParentCategoryId = null;
						if(null != eventDTO.getPosParentCategoryName() && !eventDTO.getPosParentCategoryName().isEmpty()){
							posBasedParentCategoryId = parentCategoryMap.get(eventDTO.getPosParentCategoryName().toUpperCase());
						}else{
							posBasedParentCategoryId = eventDTO.getParentCategoryId();
						}
						
						boolean isPresaleEvent = false,isEventEligigbleForTN=true,isEventEligibleForRTF=true;;
						
						/*if(eventDTO.getPresaleEvent() != null && eventDTO.getPresaleEvent()) {//eventDTO.getVenueCategoryName().equalsIgnoreCase("PRESALE")
							isPresaleEvent = true;
						}*/

						//Tamil : 06/26/2018 if event from NY state then do not process that event to Tixcity
						if(eventDTO.getState() != null && (eventDTO.getState().equalsIgnoreCase("NY") || 
								eventDTO.getState().equalsIgnoreCase("New York"))) {
							isEventEligigbleForTN = false;
						}
									
						//Tamil 05/23/2017 start process presale setup events of NY state to Tixcity
						//Tamil 10/16/2017 for all NY state events process to TN except presale events.
						//If event is in NY state then don't add events to TN
						//if(eventDTO.getState() != null && (eventDTO.getState().equalsIgnoreCase("NY") || 
						//		eventDTO.getState().equalsIgnoreCase("New York"))) {//isPresaleEvent &&
							
							//Tamil : if event created date is with in 4 days then do not consider in Tixcity
							/*if(eventDTO.getEventCreatedDate().before(presaleSettingDate)) {
								isEventEligigbleForTN = true;
							} else {
								isEventEligigbleForTN = false;
							}*/
							/*
							//if event is NY state and child category is broadway and artist except these 2 then process events for TN
							if(!isPresaleEvent && eventDTO.getPosChildCategoryName() != null && eventDTO.getPosChildCategoryName().equalsIgnoreCase("BROADWAY") &&
									!eventDTO.getName().equalsIgnoreCase("Hamilton") && !eventDTO.getArtistName().equalsIgnoreCase("Hamilton") &&
									!eventDTO.getName().equalsIgnoreCase("Dear Evan Hansen") && !eventDTO.getArtistName().equalsIgnoreCase("Dear Evan Hansen") ) {
								isEventEligigbleForTN = true;
								
							//Tamil : process NY state Sports evnets to TN
							} else if(!isPresaleEvent && eventDTO.getPosParentCategoryName() != null &&
									eventDTO.getPosParentCategoryName().equalsIgnoreCase("SPORTS")) {
								isEventEligigbleForTN = true;
							} else {
								isEventEligigbleForTN = false;
							}*/
						//}

						//Tamil 11/20/2017 If event is from CANADA then do not process to TN products
						//Tamil 11/27/2017 as leor wants to consider CA events for Tn
						//Tamil 12/14/2017 as leor wants to take down CANADA events TN
						//Tamil 01/29/2018 as leor wants to take down canada events from RTF
						//Tamil 02/14/2018 as amit wants to consdier all canada events except Ontario state events for Tixcity and RTF
						//tamil 06/12/2018 as amit wants to remove Canada events from tixcity
						if(eventDTO.getCountry() != null && eventDTO.getCountry().equalsIgnoreCase("CA")) {
							if(eventDTO.getState() != null && eventDTO.getState().equalsIgnoreCase("ON")) {
								isEventEligibleForRTF = false;
							}
							isEventEligigbleForTN = false;
						}
						
						//Tamil 11/29/2017 consider FIFA world cup evnets to TN only. these events are taking place in Russia
						//if evnet childcategory is SOCCER and grandchildcategory is Word Cup then those events are FIFA world cup events
						if(eventDTO.getCountry() != null && eventDTO.getCountry().equalsIgnoreCase("RU")) {
						
							isEventEligibleForRTF = false;
							
							if(eventDTO.getPosChildCategoryName() != null && eventDTO.getPosChildCategoryName().equalsIgnoreCase("SOCCER") &&
									eventDTO.getPosGrandChildCategoryName() != null && eventDTO.getPosGrandChildCategoryName().equalsIgnoreCase("World Cup")) {
							isEventEligigbleForTN = true;	
							} else {
								 isEventEligigbleForTN = false;
							}
							
						}
						
						if(activeProductsMap.get(ZONE_TICKETS_PROCESSOR_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = ZONE_TICKETS_PROCESSOR_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							zoneTicketsProcessorExEvent = existingZoneTicketsProcessorExEventMap.remove(eventDTO.getId());
							
							if(isEventEligibleForRTF) {
							//if(!isPresaleEvent) {
								if(zoneTicketsProcessorExEvent == null) {
									try {
										//mapFile = new File("C:\\TMATIMAGESFINAL\\SvgMaps\\" + eventDTO.getVenueId() + "_" +eventDTO.getVenueCategoryName()+ ".gif");	
										
										//if (mapFile.exists()) {
										
											zoneTicketsProcessorExEvent = new ZoneTicketsProcessorExchangeEvent();
											String exVenueKey="";
											zoneTicketsProcessorExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
											zoneTicketsProcessorExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
											zoneTicketsProcessorExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
											zoneTicketsProcessorExEvent.setZoneTicketBrokerId(defaultApProerties.getZoneTicketBrokerId());
											zoneTicketsProcessorExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
											zoneTicketsProcessorExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
											
											if(defaultApProerties.getExcludeHoursBeforeEvent() != null) {
												zoneTicketsProcessorExEvent.setExcludeHoursBeforeEvent(defaultApProerties.getExcludeHoursBeforeEvent());
											} else {
												zoneTicketsProcessorExEvent.setExcludeHoursBeforeEvent(zoneTixCommonExcludeHoursBeforeEvent);	
											}
											
												
										    exVenueKey=ZONE_TICKETS_PROCESSOR_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && zoneTicketsProcessorExEvent.getTicketNetworkBrokerId() != null){
													    zoneTicketsProcessorExEvent.setTicketNetworkBrokerId(null);
											   }
											exVenueKey=ZONE_TICKETS_PROCESSOR_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
										    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
											   if(excludeVenues!=null && zoneTicketsProcessorExEvent.getVividBrokerId() != null){
													    zoneTicketsProcessorExEvent.setVividBrokerId(null);
												    }
											exVenueKey=ZONE_TICKETS_PROCESSOR_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
											excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
												  if(excludeVenues!=null && zoneTicketsProcessorExEvent.getScoreBigBrokerId() != null){
													    zoneTicketsProcessorExEvent.setScoreBigBrokerId(null);
												  } 
											exVenueKey=ZONE_TICKETS_PROCESSOR_PRODUCT_ID+"_"+defaultApProerties.getZoneTicketBrokerId()+"_"+eventDTO.getVenueId();   
											excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
											if(excludeVenues!=null && zoneTicketsProcessorExEvent.getZoneTicketBrokerId() != null){
												zoneTicketsProcessorExEvent.setZoneTicketBrokerId(null);
											} 	 
											exVenueKey=ZONE_TICKETS_PROCESSOR_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
											excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
											if(excludeVenues!=null && zoneTicketsProcessorExEvent.getFanxchangeBrokerId() != null){
												zoneTicketsProcessorExEvent.setFanxchangeBrokerId(null);
											} 
											exVenueKey=ZONE_TICKETS_PROCESSOR_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
											excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
											if(excludeVenues!=null && zoneTicketsProcessorExEvent.getTicketcityBrokerId() != null){
												zoneTicketsProcessorExEvent.setTicketcityBrokerId(null);
											} 
											   
												  
											
											zoneTicketsProcessorExEvent.setEventId(eventDTO.getId());
											zoneTicketsProcessorExEvent.setAllowSectionRange(false);
											zoneTicketsProcessorExEvent.setExposure(defaultApProerties.getExposure());
											//zoneTicketsProcessorExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
											zoneTicketsProcessorExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
											//zoneTicketsProcessorExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
											zoneTicketsProcessorExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
											//zoneTicketsProcessorExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
											zoneTicketsProcessorExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
											zoneTicketsProcessorExEvent.setRptFactor(defaultApProerties.getRptFactor());
											//zoneTicketsProcessorExEvent.setShippingDays(defaultApProerties.getShippingDays());
											//zoneTicketsProcessorExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
											zoneTicketsProcessorExEvent.setStatus("ACTIVE");
											zoneTicketsProcessorExEvent.setLastUpdatedBy(username);
											zoneTicketsProcessorExEvent.setLastUpdatedDate(now);
											zoneTicketsProcessorExEvent.setZone(isZoneEvent);
											zoneTicketsProcessorExEvent.setDiscountZone(false);
											zoneTicketsProcessorExEvent.setZoneDiscountPerc(zoneDiscountPerc);
											zoneTicketsProcessorExEvent.setTaxPercentage(defaultApProerties.getTaxPercentage());
											
											//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
											AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(ZONE_TICKETS_PROCESSOR_PRODUCT_ID+"_"+eventDTO.getVenueId());
											if(apVenueSettings != null) {
												zoneTicketsProcessorExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
												zoneTicketsProcessorExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
												zoneTicketsProcessorExEvent.setLowerMarkup(apVenueSettings.getMarkup());
												zoneTicketsProcessorExEvent.setUpperMarkup(apVenueSettings.getMarkup());
												zoneTicketsProcessorExEvent.setShippingDays(apVenueSettings.getShippingDays());
											} else {
												zoneTicketsProcessorExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
												zoneTicketsProcessorExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
												zoneTicketsProcessorExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
												zoneTicketsProcessorExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
												zoneTicketsProcessorExEvent.setShippingDays(defaultApProerties.getShippingDays());
											}
											
											zoneTicketsProcessorExEvents.add(zoneTicketsProcessorExEvent);
										//}	
									} catch (Exception e) {
										e.printStackTrace();
									}
									
								} else if(!zoneTicketsProcessorExEvent.getStatus().equals("ACTIVE") && zoneTicketsProcessorExEvent.getLastUpdatedBy().equals(username)) {
									ZoneTicketsProcessorExchangeEventAudit zoneTicketsProcessorExEventAudit = new ZoneTicketsProcessorExchangeEventAudit(zoneTicketsProcessorExEvent);
									zoneTicketsProcessorExEventAudits.add(zoneTicketsProcessorExEventAudit);
									
									zoneTicketsProcessorExEvent.setStatus("ACTIVE");
									zoneTicketsProcessorExEvent.setLastUpdatedBy(username);
									zoneTicketsProcessorExEvent.setLastUpdatedDate(now);
									zoneTicketsProcessorExEvent.setZone(isZoneEvent);
									
									zoneTicketsProcessorExEvents.add(zoneTicketsProcessorExEvent);
									
								} else if(isEventExistinPOS) {
									/*if(!isZoneEvent && zoneTicketsProcessorExEvent.getZoneTicketBrokerId() != null) {
										ZoneTicketsProcessorExchangeEventAudit zoneTicketsProcessorExEventAudit = new ZoneTicketsProcessorExchangeEventAudit(zoneTicketsProcessorExEvent);
										zoneTicketsProcessorExEventAudits.add(zoneTicketsProcessorExEventAudit);
										
										zoneTicketsProcessorExEvent.setZoneTicketBrokerId(null);
										zoneTicketsProcessorExEvent.setLastUpdatedBy(username);
										zoneTicketsProcessorExEvent.setLastUpdatedDate(now);
										zoneTicketsProcessorExEvent.setZone(isZoneEvent);
										zoneTicketsProcessorExEvents.add(zoneTicketsProcessorExEvent);
										
									} else if(isZoneEvent && zoneTicketsProcessorExEvent.getZoneTicketBrokerId() == null && zoneTicketsProcessorExEvent.getLastUpdatedBy().equals(username) 
												&& defaultApProerties.getZoneTicketBrokerId() != null) {
										ZoneTicketsProcessorExchangeEventAudit zoneTicketsProcessorExEventAudit = new ZoneTicketsProcessorExchangeEventAudit(zoneTicketsProcessorExEvent);
										zoneTicketsProcessorExEventAudits.add(zoneTicketsProcessorExEventAudit);
										
										zoneTicketsProcessorExEvent.setZoneTicketBrokerId(defaultApProerties.getZoneTicketBrokerId());
										zoneTicketsProcessorExEvent.setLastUpdatedBy(username);
										zoneTicketsProcessorExEvent.setLastUpdatedDate(now);
										zoneTicketsProcessorExEvent.setZone(isZoneEvent);
										zoneTicketsProcessorExEvents.add(zoneTicketsProcessorExEvent);
										
									} else*/ if(zoneTicketsProcessorExEvent.getZone() == null || !zoneTicketsProcessorExEvent.getZone().equals(isZoneEvent)) {
										ZoneTicketsProcessorExchangeEventAudit zoneTicketsProcessorExEventAudit = new ZoneTicketsProcessorExchangeEventAudit(zoneTicketsProcessorExEvent);
										zoneTicketsProcessorExEventAudits.add(zoneTicketsProcessorExEventAudit);
										
										zoneTicketsProcessorExEvent.setZone(isZoneEvent);
										zoneTicketsProcessorExEvents.add(zoneTicketsProcessorExEvent);
									}
								}
							} else {
								if(zoneTicketsProcessorExEvent != null && zoneTicketsProcessorExEvent.getStatus().equals("ACTIVE")) {
									ZoneTicketsProcessorExchangeEventAudit zoneTicketsProcessorExEventAudit = new ZoneTicketsProcessorExchangeEventAudit(zoneTicketsProcessorExEvent);
									zoneTicketsProcessorExEventAudits.add(zoneTicketsProcessorExEventAudit);
									
									zoneTicketsProcessorExEvent.setStatus("DELETED");
									zoneTicketsProcessorExEvent.setLastUpdatedBy(username);
									zoneTicketsProcessorExEvent.setLastUpdatedDate(now);
									zoneTicketsProcessorExEvent.setZone(isZoneEvent);
									
									zoneTicketsProcessorExEvents.add(zoneTicketsProcessorExEvent);
									
								}
							}
						}
					
						if(activeProductsMap.get(PRESALE_ZONE_TICKETS_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = PRESALE_ZONE_TICKETS_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							presaleZoneTicketsExchangeEvent = existingPresaleZoneTicketsExEventMap.remove(eventDTO.getId());
							if(isPresaleEvent) {
								
								if(presaleZoneTicketsExchangeEvent == null) {
									try {
										//mapFile = new File("C:\\TMATIMAGESFINAL\\SvgMaps\\" + eventDTO.getVenueId() + "_" +eventDTO.getVenueCategoryName()+ ".gif");	
										
										//if (mapFile.exists()) {
										
											presaleZoneTicketsExchangeEvent = new PresaleZoneTicketsExchangeEvent();
											String exVenueKey="";
											presaleZoneTicketsExchangeEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
											presaleZoneTicketsExchangeEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
											presaleZoneTicketsExchangeEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
											presaleZoneTicketsExchangeEvent.setZoneTicketBrokerId(defaultApProerties.getZoneTicketBrokerId());
											presaleZoneTicketsExchangeEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
											presaleZoneTicketsExchangeEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
												
										    exVenueKey=PRESALE_ZONE_TICKETS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && presaleZoneTicketsExchangeEvent.getTicketNetworkBrokerId() != null){
												   presaleZoneTicketsExchangeEvent.setTicketNetworkBrokerId(null);
											   }
											exVenueKey=PRESALE_ZONE_TICKETS_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
										    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
											   if(excludeVenues!=null && presaleZoneTicketsExchangeEvent.getVividBrokerId() != null){
												   presaleZoneTicketsExchangeEvent.setVividBrokerId(null);
												}
											exVenueKey=PRESALE_ZONE_TICKETS_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
											excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
												  if(excludeVenues!=null && presaleZoneTicketsExchangeEvent.getScoreBigBrokerId() != null){
													  presaleZoneTicketsExchangeEvent.setScoreBigBrokerId(null);
												  } 
											exVenueKey=PRESALE_ZONE_TICKETS_PRODUCT_ID+"_"+defaultApProerties.getZoneTicketBrokerId()+"_"+eventDTO.getVenueId();   
											excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
											if(excludeVenues!=null && presaleZoneTicketsExchangeEvent.getZoneTicketBrokerId() != null){
												presaleZoneTicketsExchangeEvent.setZoneTicketBrokerId(null);
											} 	 
											exVenueKey=PRESALE_ZONE_TICKETS_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
											excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
											if(excludeVenues!=null && presaleZoneTicketsExchangeEvent.getFanxchangeBrokerId() != null){
												presaleZoneTicketsExchangeEvent.setFanxchangeBrokerId(null);
											} 
											exVenueKey=PRESALE_ZONE_TICKETS_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
											excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
											if(excludeVenues!=null && presaleZoneTicketsExchangeEvent.getTicketcityBrokerId() != null){
												presaleZoneTicketsExchangeEvent.setTicketcityBrokerId(null);
											} 
											   
												  
											
											presaleZoneTicketsExchangeEvent.setEventId(eventDTO.getId());
											presaleZoneTicketsExchangeEvent.setAllowSectionRange(false);
											presaleZoneTicketsExchangeEvent.setExposure(defaultApProerties.getExposure());
											presaleZoneTicketsExchangeEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
											presaleZoneTicketsExchangeEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
											presaleZoneTicketsExchangeEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
											presaleZoneTicketsExchangeEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
											presaleZoneTicketsExchangeEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
											presaleZoneTicketsExchangeEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
											presaleZoneTicketsExchangeEvent.setRptFactor(defaultApProerties.getRptFactor());
											presaleZoneTicketsExchangeEvent.setShippingDays(defaultApProerties.getShippingDays());
											presaleZoneTicketsExchangeEvent.setShippingMethod(defaultApProerties.getShippingMethod());
											presaleZoneTicketsExchangeEvent.setStatus("ACTIVE");
											presaleZoneTicketsExchangeEvent.setLastUpdatedBy(username);
											presaleZoneTicketsExchangeEvent.setLastUpdatedDate(now);
											presaleZoneTicketsExchangeEvent.setZone(isZoneEvent);
											presaleZoneTicketsExchangeEvent.setDiscountZone(false);
											presaleZoneTicketsExchangeEvent.setZoneDiscountPerc(zoneDiscountPerc);
											presaleZoneTicketsExEvents.add(presaleZoneTicketsExchangeEvent);
											
										//}
									} catch (Exception e) {
										e.printStackTrace();
									}
									
								} else if(isEventExistinPOS) {
									if(presaleZoneTicketsExchangeEvent.getZone() == null || !presaleZoneTicketsExchangeEvent.getZone().equals(isZoneEvent)) {
										PresaleZoneTicketsExchangeEventAudit presaleZoneTicketsExEventAudit = new PresaleZoneTicketsExchangeEventAudit(presaleZoneTicketsExchangeEvent);
										presaleZoneTicketsExEventAudits.add(presaleZoneTicketsExEventAudit);
										
										presaleZoneTicketsExchangeEvent.setZone(isZoneEvent);
										presaleZoneTicketsExEvents.add(presaleZoneTicketsExchangeEvent);
									}
								} 
							}else{
								
								if(presaleZoneTicketsExchangeEvent != null && presaleZoneTicketsExchangeEvent.getStatus().equals("ACTIVE")) {
									PresaleZoneTicketsExchangeEventAudit presaleAutocatExEventAudit = new PresaleZoneTicketsExchangeEventAudit(presaleZoneTicketsExchangeEvent);
									presaleZoneTicketsExEventAudits.add(presaleAutocatExEventAudit);
									
									presaleZoneTicketsExchangeEvent.setStatus("DELETED");
									presaleZoneTicketsExchangeEvent.setLastUpdatedBy(username);
									presaleZoneTicketsExchangeEvent.setLastUpdatedDate(now);
									presaleZoneTicketsExchangeEvent.setZone(isZoneEvent);
									presaleZoneTicketsExEvents.add(presaleZoneTicketsExchangeEvent);
								}
							}
						}
						//Skip specified venue events from all autopricing products except Zoneticketsprocessor
						/*if(autopricingUncheckedVenues != null && autopricingUncheckedVenues.contains(eventDTO.getVenueId())) {
							continue;
						}*/
						
						if(activeProductsMap.get(AUTOCATS96_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = AUTOCATS96_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							autoCats96ExchangeEvent = existingAutoCats96ProcessorExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {//!isPresaleEvent
							if(autoCats96ExchangeEvent == null) {
								if(isEventExistinPOS && !isZoneEvent) {
								autoCats96ExchangeEvent = new AutoCats96ExchangeEvent();
								/*if(isEventExistinPOS && !isZoneEvent) {
									autoCats96ExchangeEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
								} else {
									autoCats96ExchangeEvent.setTicketNetworkBrokerId(null);
								}
								autoCats96ExchangeEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
								autoCats96ExchangeEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());*/
								autoCats96ExchangeEvent.setVividEnabled(defaultApProerties.getVividSeatEnabled());
								autoCats96ExchangeEvent.setRotEnabled(defaultApProerties.getRotEnabled());
								autoCats96ExchangeEvent.setRtwEnabled(defaultApProerties.getRtwEnabled());
								autoCats96ExchangeEvent.setTixcityEnabled(defaultApProerties.getTixcityEnabled());
								
								  
								   String exVenueKey=null;
								   if(defaultApProerties.getRotEnabled().equals(true)){
									    exVenueKey=AUTOCATS96_PRODUCT_ID+"_"+10+"_"+eventDTO.getVenueId();
									    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
										   if(excludeVenues!=null && autoCats96ExchangeEvent.getRotEnabled() != null && autoCats96ExchangeEvent.getRotEnabled()){
											   autoCats96ExchangeEvent.setRotEnabled(false);
										   }
									   }
								   if(defaultApProerties.getTixcityEnabled().equals(true)){
									    exVenueKey=AUTOCATS96_PRODUCT_ID+"_"+TIXCITY_BROKER_ID+"_"+eventDTO.getVenueId();
									    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
										   if(excludeVenues!=null && autoCats96ExchangeEvent.getTixcityEnabled() != null && autoCats96ExchangeEvent.getTixcityEnabled()){
											   autoCats96ExchangeEvent.setTixcityEnabled(false);
										   }
									   }
								   if(defaultApProerties.getRtwEnabled().equals(true)){
									    exVenueKey=AUTOCATS96_PRODUCT_ID+"_"+5+"_"+eventDTO.getVenueId();
									    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
										   if(excludeVenues!=null && autoCats96ExchangeEvent.getRtwEnabled() != null && autoCats96ExchangeEvent.getRtwEnabled()){
											   autoCats96ExchangeEvent.setRtwEnabled(false);
										   }
									   }
								
								autoCats96ExchangeEvent.setMinicatsEnabled(defaultApProerties.getMinicatsEnabled());
								autoCats96ExchangeEvent.setLastrowMinicatsEnabled(defaultApProerties.getLastrowMinicatsEnabled());
								autoCats96ExchangeEvent.setVipMinicatsEnabled(defaultApProerties.getVipMinicatsEnabled());
								autoCats96ExchangeEvent.setVipLastrowMinicatsEnabled(defaultApProerties.getVipLastRowMinicatsEnabled());
								autoCats96ExchangeEvent.setPresaleAutocatsEnabled(false);
								
								autoCats96ExchangeEvent.setEventId(eventDTO.getId());
								autoCats96ExchangeEvent.setAllowSectionRange(false);
								//autoCats96ExchangeEvent.setExposure(defaultApProerties.getExposure());
								//autoCats96ExchangeEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
								autoCats96ExchangeEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
								//autoCats96ExchangeEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
								autoCats96ExchangeEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
								autoCats96ExchangeEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
								autoCats96ExchangeEvent.setRptFactor(defaultApProerties.getRptFactor());
								//autoCats96ExchangeEvent.setShippingDays(defaultApProerties.getShippingDays());
								autoCats96ExchangeEvent.setStatus("ACTIVE");
								autoCats96ExchangeEvent.setLastUpdatedBy(username);
								autoCats96ExchangeEvent.setLastUpdatedDate(now);
								autoCats96ExchangeEvent.setZone(isZoneEvent);
								autoCats96ExchangeEvent.setDiscountZone(false);
								
								
								//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
								AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(AUTOCATS96_PRODUCT_ID+"_"+eventDTO.getVenueId());
								if(apVenueSettings != null) {
									autoCats96ExchangeEvent.setShippingMethod(apVenueSettings.getShippingMethod());
									autoCats96ExchangeEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
									//autoCats96ExchangeEvent.setLowerMarkup(apVenueSettings.getMarkup());
									//autoCats96ExchangeEvent.setUpperMarkup(apVenueSettings.getMarkup());
									autoCats96ExchangeEvent.setShippingDays(apVenueSettings.getShippingDays());
								} else {
									autoCats96ExchangeEvent.setShippingMethod(defaultApProerties.getShippingMethod());
									autoCats96ExchangeEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
									//autoCats96ExchangeEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									//autoCats96ExchangeEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									autoCats96ExchangeEvent.setShippingDays(defaultApProerties.getShippingDays());
								}

								//tamil 08/16/2018 : change markups to 25% for presale setting events
								//Tamil 04/27/2018 : for first 4 days from evnet created date use markups as 0 and thne update proper markup settings so moved below logic to nextelse if
								//If evnet is created before 5 days then use normal settings otherwise use 0 markup settings
								if(!eventDTO.getEventCreatedDate().before(presaleSettingDate)) {
									autoCats96ExchangeEvent.setLowerMarkup(0.0);
									autoCats96ExchangeEvent.setUpperMarkup(0.0);
									autoCats96ExchangeEvent.setExposure("1-OXP");
									autoCats96ExchangeEvent.setLowerShippingFees(0.0);
									autoCats96ExchangeEvent.setUpperShippingFees(0.0);
									autoCats96ExchangeEvent.setPreSaleSettingsUpdated(Boolean.FALSE);
								} else {
									if(apVenueSettings != null) {
										autoCats96ExchangeEvent.setLowerMarkup(apVenueSettings.getMarkup());
										autoCats96ExchangeEvent.setUpperMarkup(apVenueSettings.getMarkup());
									} else {
										autoCats96ExchangeEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
										autoCats96ExchangeEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									}
									autoCats96ExchangeEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
									autoCats96ExchangeEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									autoCats96ExchangeEvent.setExposure(defaultApProerties.getExposure());
									autoCats96ExchangeEvent.setPreSaleSettingsUpdated(Boolean.TRUE);
								}
								autoCats96ExEvents.add(autoCats96ExchangeEvent);
								}
								
							} else if(!autoCats96ExchangeEvent.getPreSaleSettingsUpdated() && eventDTO.getEventCreatedDate().before(presaleSettingDate) &&
									(autoCats96ExchangeEvent.getLastUpdatedBy().equalsIgnoreCase(username) || autoCats96ExchangeEvent.getLastUpdatedBy().equalsIgnoreCase("Manual"))) {

								AutoCats96ExchangeEventAudit autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(autoCats96ExchangeEvent);
								autoCats96ExEventAudits.add(autoCats96ExchangeEventAudit);
								
								AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(AUTOCATS96_PRODUCT_ID+"_"+eventDTO.getVenueId());
								if(apVenueSettings != null) {
									autoCats96ExchangeEvent.setLowerMarkup(apVenueSettings.getMarkup());
									autoCats96ExchangeEvent.setUpperMarkup(apVenueSettings.getMarkup());
								} else {
									autoCats96ExchangeEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									autoCats96ExchangeEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
								}
								
								autoCats96ExchangeEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
								autoCats96ExchangeEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
								autoCats96ExchangeEvent.setExposure(defaultApProerties.getExposure());
								autoCats96ExchangeEvent.setPreSaleSettingsUpdated(Boolean.TRUE);
								autoCats96ExchangeEvent.setLastUpdatedBy(username);
								autoCats96ExchangeEvent.setLastUpdatedDate(now);
								autoCats96ExchangeEvent.setZone(isZoneEvent);
								
								autoCats96ExEvents.add(autoCats96ExchangeEvent);
								
								
							} else if(isEventExistinPOS) {
								
								if(isZoneEvent && !autoCats96ExchangeEvent.getZone().equals(isZoneEvent)) {
									AutoCats96ExchangeEventAudit autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(autoCats96ExchangeEvent);
									autoCats96ExEventAudits.add(autoCats96ExchangeEventAudit);
									//autoCats96ExchangeEvent.setTicketNetworkBrokerId(null);
									autoCats96ExchangeEvent.setLastUpdatedBy(username);
									autoCats96ExchangeEvent.setLastUpdatedDate(now);
									autoCats96ExchangeEvent.setZone(isZoneEvent);
									autoCats96ExEvents.add(autoCats96ExchangeEvent);
									
								} else if(!isZoneEvent && autoCats96ExchangeEvent.getStatus().equals("ACTIVE") && autoCats96ExchangeEvent.getLastUpdatedBy().equals(username) && 
										
										((autoCats96ExchangeEvent.getRotEnabled() == null && defaultApProerties.getRotEnabled() != null) 
											||	(autoCats96ExchangeEvent.getTixcityEnabled() == null && defaultApProerties.getTixcityEnabled() != null)
											|| (autoCats96ExchangeEvent.getRtwEnabled() == null && defaultApProerties.getRtwEnabled() != null)
										)) {
									
									
									autoCats96ExchangeEvent.setRotEnabled(defaultApProerties.getRotEnabled());
									autoCats96ExchangeEvent.setTixcityEnabled(defaultApProerties.getTixcityEnabled());
									autoCats96ExchangeEvent.setRtwEnabled(defaultApProerties.getRtwEnabled());
									String exVenueKey=null;
									Boolean two=true,three=true,four=true;
									
									   if(defaultApProerties.getRotEnabled().equals(true)){
										    exVenueKey=AUTOCATS96_PRODUCT_ID+"_"+10+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && autoCats96ExchangeEvent.getRotEnabled() != null && autoCats96ExchangeEvent.getRotEnabled()){
												   autoCats96ExchangeEvent.setRotEnabled(false);
												   two=false;
											   }
										   }
									   if(defaultApProerties.getTixcityEnabled().equals(true)){
										    exVenueKey=AUTOCATS96_PRODUCT_ID+"_"+TIXCITY_BROKER_ID+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && autoCats96ExchangeEvent.getTixcityEnabled() != null && autoCats96ExchangeEvent.getTixcityEnabled()){
												   autoCats96ExchangeEvent.setTixcityEnabled(false);
												   four=false;
											   }
										   }
									   if(defaultApProerties.getRtwEnabled().equals(true)){
										    exVenueKey=AUTOCATS96_PRODUCT_ID+"_"+5+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && autoCats96ExchangeEvent.getRtwEnabled() != null && autoCats96ExchangeEvent.getRtwEnabled()){
												   autoCats96ExchangeEvent.setRtwEnabled(false);
												   three=false;
											   }
										   }
									   if(two || three || four ){
										 AutoCats96ExchangeEventAudit autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(autoCats96ExchangeEvent);
										 autoCats96ExEventAudits.add(autoCats96ExchangeEventAudit);
										 autoCats96ExchangeEvent.setLastUpdatedBy(username);
										 autoCats96ExchangeEvent.setLastUpdatedDate(now);
										 autoCats96ExchangeEvent.setZone(isZoneEvent);
										 autoCats96ExEvents.add(autoCats96ExchangeEvent);
									 }
									
									
									
									
								} else if(autoCats96ExchangeEvent.getZone() == null || !autoCats96ExchangeEvent.getZone().equals(isZoneEvent)) {
									AutoCats96ExchangeEventAudit autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(autoCats96ExchangeEvent);
									autoCats96ExEventAudits.add(autoCats96ExchangeEventAudit);
									
									autoCats96ExchangeEvent.setZone(isZoneEvent);
									autoCats96ExEvents.add(autoCats96ExchangeEvent);
								}
							}
							} else {
								if(autoCats96ExchangeEvent != null && autoCats96ExchangeEvent.getStatus().equals("ACTIVE")) {
									AutoCats96ExchangeEventAudit autoCats96ExchangeEventAudit = new AutoCats96ExchangeEventAudit(autoCats96ExchangeEvent);
									autoCats96ExEventAudits.add(autoCats96ExchangeEventAudit);
									
									autoCats96ExchangeEvent.setStatus("DELETED");
									autoCats96ExchangeEvent.setLastUpdatedBy(username);
									autoCats96ExchangeEvent.setLastUpdatedDate(now);
									autoCats96ExchangeEvent.setZone(isZoneEvent);
									autoCats96ExEvents.add(autoCats96ExchangeEvent);
								}
							}
						}
						

						if(activeProductsMap.get(PRESALE_AUTOCAT_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = PRESALE_AUTOCAT_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							presaleAutoCatExchangeEvent = existingPresaleAutoExEventMap.remove(eventDTO.getId());
							
							//if(eventDTO.getEvent().getVenueCategory().getCategoryGroup().equals("PRESALE") && eventDTO.getVenueCategoryId()!=null){
							
							if(isPresaleEvent && isEventEligigbleForTN) {
												
							if(presaleAutoCatExchangeEvent == null) {
								if(isEventExistinPOS && !isZoneEvent) {
								presaleAutoCatExchangeEvent = new PresaleAutoCatExchangeEvent();
								/*if(isEventExistinPOS && !isZoneEvent) {
								    presaleAutoCatExchangeEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									
								} else {
									presaleAutoCatExchangeEvent.setTicketNetworkBrokerId(null);
								}
								presaleAutoCatExchangeEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
								presaleAutoCatExchangeEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
								presaleAutoCatExchangeEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());*/
								
								presaleAutoCatExchangeEvent.setVividEnabled(defaultApProerties.getVividSeatEnabled());
								presaleAutoCatExchangeEvent.setRotEnabled(defaultApProerties.getRotEnabled());
								presaleAutoCatExchangeEvent.setTixcityEnabled(defaultApProerties.getTixcityEnabled());
								presaleAutoCatExchangeEvent.setRtwEnabled(defaultApProerties.getRtwEnabled());
								
								  
								String exVenueKey=null;
								if(defaultApProerties.getRotEnabled().equals(true)){
									exVenueKey=PRESALE_AUTOCAT_PRODUCT_ID+"_"+10+"_"+eventDTO.getVenueId();
									ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									if(excludeVenues!=null && presaleAutoCatExchangeEvent.getRotEnabled() != null && presaleAutoCatExchangeEvent.getRotEnabled()){
										presaleAutoCatExchangeEvent.setRotEnabled(false);
									}
								}
								if(defaultApProerties.getTixcityEnabled().equals(true)){
									exVenueKey=PRESALE_AUTOCAT_PRODUCT_ID+"_"+TIXCITY_BROKER_ID+"_"+eventDTO.getVenueId();
									ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									if(excludeVenues!=null && presaleAutoCatExchangeEvent.getTixcityEnabled() != null && presaleAutoCatExchangeEvent.getTixcityEnabled()){
										presaleAutoCatExchangeEvent.setTixcityEnabled(false);
									}
								}
								if(defaultApProerties.getRtwEnabled().equals(true)){
									exVenueKey=PRESALE_AUTOCAT_PRODUCT_ID+"_"+5+"_"+eventDTO.getVenueId();
									ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									if(excludeVenues!=null && presaleAutoCatExchangeEvent.getRtwEnabled() != null && presaleAutoCatExchangeEvent.getRtwEnabled()){
										presaleAutoCatExchangeEvent.setRtwEnabled(false);
									}
								}
								
								presaleAutoCatExchangeEvent.setMinicatsEnabled(defaultApProerties.getMinicatsEnabled());
								presaleAutoCatExchangeEvent.setLastrowMinicatsEnabled(false);
								presaleAutoCatExchangeEvent.setVipMinicatsEnabled(false);
								//Tamil : Do not consider section range tickets for ticketnetwork
								presaleAutoCatExchangeEvent.setPresaleAutocatsEnabled(false);//defaultApProerties.getPresaleAutocatsEnabled()
								   
								presaleAutoCatExchangeEvent.setEventId(eventDTO.getId());
								presaleAutoCatExchangeEvent.setAllowSectionRange(true);
								presaleAutoCatExchangeEvent.setExposure(defaultApProerties.getExposure());
								//presaleAutoCatExchangeEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
								presaleAutoCatExchangeEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
								//presaleAutoCatExchangeEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
								presaleAutoCatExchangeEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
								presaleAutoCatExchangeEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
								presaleAutoCatExchangeEvent.setRptFactor(defaultApProerties.getRptFactor());
								//presaleAutoCatExchangeEvent.setShippingDays(defaultApProerties.getShippingDays());
								presaleAutoCatExchangeEvent.setStatus("ACTIVE");
								presaleAutoCatExchangeEvent.setLastUpdatedBy(username);
								presaleAutoCatExchangeEvent.setLastUpdatedDate(now);
								presaleAutoCatExchangeEvent.setZone(isZoneEvent);
								presaleAutoCatExchangeEvent.setDiscountZone(false);
								
								
								
								//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
								AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(PRESALE_AUTOCAT_PRODUCT_ID+"_"+eventDTO.getVenueId());
								if(apVenueSettings != null) {
									presaleAutoCatExchangeEvent.setShippingMethod(apVenueSettings.getShippingMethod());
									presaleAutoCatExchangeEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
									presaleAutoCatExchangeEvent.setLowerMarkup(apVenueSettings.getMarkup());
									presaleAutoCatExchangeEvent.setUpperMarkup(apVenueSettings.getMarkup());
									presaleAutoCatExchangeEvent.setShippingDays(apVenueSettings.getShippingDays());
								} else {
									presaleAutoCatExchangeEvent.setShippingMethod(defaultApProerties.getShippingMethod());
									presaleAutoCatExchangeEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
									presaleAutoCatExchangeEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									presaleAutoCatExchangeEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									presaleAutoCatExchangeEvent.setShippingDays(defaultApProerties.getShippingDays());
								}
								
								presaleAutoCatExEvents.add(presaleAutoCatExchangeEvent);
								}
								
							} else if(isEventExistinPOS) {
								if(isZoneEvent && !presaleAutoCatExchangeEvent.getZone().equals(isZoneEvent)) {
									PresaleAutoCatExchangeEventAudit presaleAutocatExEventAudit = new PresaleAutoCatExchangeEventAudit(presaleAutoCatExchangeEvent);
									presaleAutoCatExEventAudits.add(presaleAutocatExEventAudit);
									
									presaleAutoCatExchangeEvent.setTicketNetworkBrokerId(null);
									presaleAutoCatExchangeEvent.setLastUpdatedBy(username);
									presaleAutoCatExchangeEvent.setLastUpdatedDate(now);
									presaleAutoCatExchangeEvent.setZone(isZoneEvent);
									presaleAutoCatExEvents.add(presaleAutoCatExchangeEvent);
									
								} else if(!isZoneEvent && presaleAutoCatExchangeEvent.getStatus().equals("ACTIVE") && presaleAutoCatExchangeEvent.getLastUpdatedBy().equals(username) && 
										
										((presaleAutoCatExchangeEvent.getRotEnabled() == null && defaultApProerties.getRotEnabled() != null)
											|| (presaleAutoCatExchangeEvent.getTixcityEnabled() == null && defaultApProerties.getTixcityEnabled() != null)
											|| (presaleAutoCatExchangeEvent.getRtwEnabled() == null && defaultApProerties.getRtwEnabled() != null)
										)) { 
									
									presaleAutoCatExchangeEvent.setRotEnabled(defaultApProerties.getRotEnabled());
									presaleAutoCatExchangeEvent.setTixcityEnabled(defaultApProerties.getTixcityEnabled());
									presaleAutoCatExchangeEvent.setRtwEnabled(defaultApProerties.getRtwEnabled());
									String exVenueKey=null;
									Boolean two=true,three=true,four=true;
									
									   if(defaultApProerties.getRotEnabled().equals(true)){
										    exVenueKey=PRESALE_AUTOCAT_PRODUCT_ID+"_"+10+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && presaleAutoCatExchangeEvent.getRotEnabled() != null && presaleAutoCatExchangeEvent.getRotEnabled()){
												   presaleAutoCatExchangeEvent.setRotEnabled(false);
												   two=false;
											   }
										   }
									   if(defaultApProerties.getTixcityEnabled().equals(true)){
										    exVenueKey=PRESALE_AUTOCAT_PRODUCT_ID+"_"+TIXCITY_BROKER_ID+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && presaleAutoCatExchangeEvent.getTixcityEnabled() != null && presaleAutoCatExchangeEvent.getTixcityEnabled()){
												   presaleAutoCatExchangeEvent.setTixcityEnabled(false);
												   four=false;
											   }
										   }
									   if(defaultApProerties.getRtwEnabled().equals(true)){
										    exVenueKey=PRESALE_AUTOCAT_PRODUCT_ID+"_"+5+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && presaleAutoCatExchangeEvent.getRtwEnabled() != null && presaleAutoCatExchangeEvent.getRtwEnabled()){
												   presaleAutoCatExchangeEvent.setRtwEnabled(false);
												   three=false;
											   }
										   }
									   if(two || three || four){
										   PresaleAutoCatExchangeEventAudit presaleAutocatExEventAudit = new PresaleAutoCatExchangeEventAudit(presaleAutoCatExchangeEvent);
										   presaleAutoCatExEventAudits.add(presaleAutocatExEventAudit);
										   presaleAutoCatExchangeEvent.setLastUpdatedBy(username);
										   presaleAutoCatExchangeEvent.setLastUpdatedDate(now);
										   presaleAutoCatExchangeEvent.setZone(isZoneEvent);
										   presaleAutoCatExEvents.add(presaleAutoCatExchangeEvent);
									   }
									   
								} else if(presaleAutoCatExchangeEvent.getZone() == null || !presaleAutoCatExchangeEvent.getZone().equals(isZoneEvent)) {
									PresaleAutoCatExchangeEventAudit presaleAutocatExEventAudit = new PresaleAutoCatExchangeEventAudit(presaleAutoCatExchangeEvent);
									presaleAutoCatExEventAudits.add(presaleAutocatExEventAudit);
									
									presaleAutoCatExchangeEvent.setZone(isZoneEvent);
									presaleAutoCatExEvents.add(presaleAutoCatExchangeEvent);
								}
							}
						} else {
							if(presaleAutoCatExchangeEvent != null && presaleAutoCatExchangeEvent.getStatus().equals("ACTIVE")) {
								PresaleAutoCatExchangeEventAudit presaleAutocatExEventAudit = new PresaleAutoCatExchangeEventAudit(presaleAutoCatExchangeEvent);
								presaleAutoCatExEventAudits.add(presaleAutocatExEventAudit);
								
								presaleAutoCatExchangeEvent.setStatus("DELETED");
								presaleAutoCatExchangeEvent.setLastUpdatedBy(username);
								presaleAutoCatExchangeEvent.setLastUpdatedDate(now);
								presaleAutoCatExchangeEvent.setZone(isZoneEvent);
								presaleAutoCatExEvents.add(presaleAutoCatExchangeEvent);
							}
						}
						}
						
						
						if(activeProductsMap.get(MINICATS_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = MINICATS_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							miniExEvent = existingMiniExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(miniExEvent == null) {
								miniExEvent = new MiniExchangeEvent();
								if(isEventExistinPOS && !isZoneEvent) {
									miniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
								} else {
									miniExEvent.setTicketNetworkBrokerId(null);
								}
								miniExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
								miniExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
								miniExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
								miniExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
								
								 /*for (ExcludeVenues excludeVenues : excludeVenueList) {
									  if(excludeVenues.getProductId().equals(MINICATS_PRODUCT_ID)){
										  if(excludeVenues.getBrokerId().equals(defaultApProerties.getTicketNetworkBrokerId())){
											    miniExEvent.setTicketNetworkBrokerId(null);
										    }
										  if(excludeVenues.getBrokerId().equals(defaultApProerties.getVividBrokerId())){
										    	miniExEvent.setVividBrokerId(null);
										    }
										  if(excludeVenues.getBrokerId().equals(defaultApProerties.getScoreBigBrokerId())){
										    	miniExEvent.setScoreBigBrokerId(null);
										  }  
										  
									  }
								}*/
								    String exVenueKey=null;
								    exVenueKey=MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && miniExEvent.getTicketNetworkBrokerId() != null ){
										   miniExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=MINICATS_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && miniExEvent.getVividBrokerId() != null ){
										   miniExEvent.setVividBrokerId(null);
										    }
									exVenueKey=MINICATS_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && miniExEvent.getScoreBigBrokerId() != null ){
										miniExEvent.setScoreBigBrokerId(null);
									}
									exVenueKey=MINICATS_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && miniExEvent.getFanxchangeBrokerId() != null ){
										miniExEvent.setFanxchangeBrokerId(null);
									}
									exVenueKey=MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && miniExEvent.getTicketcityBrokerId() != null ){
										miniExEvent.setTicketcityBrokerId(null);
									} 
									   
								miniExEvent.setEventId(eventDTO.getId());
								miniExEvent.setAllowSectionRange(false);
								miniExEvent.setExposure(defaultApProerties.getExposure());
								//miniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
								miniExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
								//miniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
								miniExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
								miniExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
								miniExEvent.setRptFactor(defaultApProerties.getRptFactor());
								//miniExEvent.setShippingDays(defaultApProerties.getShippingDays());
								miniExEvent.setStatus("ACTIVE");
								miniExEvent.setLastUpdatedBy(username);
								miniExEvent.setLastUpdatedDate(now);
								miniExEvent.setZone(isZoneEvent);
								miniExEvent.setDiscountZone(false);
								
								//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
								AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(MINICATS_PRODUCT_ID+"_"+eventDTO.getVenueId());
								if(apVenueSettings != null) {
									miniExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
									miniExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
									miniExEvent.setLowerMarkup(apVenueSettings.getMarkup());
									miniExEvent.setUpperMarkup(apVenueSettings.getMarkup());
									miniExEvent.setShippingDays(apVenueSettings.getShippingDays());
								} else {
									miniExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
									miniExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
									miniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									miniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									miniExEvent.setShippingDays(defaultApProerties.getShippingDays());
									
								}
								
								miniExEvents.add(miniExEvent);
								
							} else if(isEventExistinPOS) {
								if(isZoneEvent && miniExEvent.getTicketNetworkBrokerId() != null) {
									MiniExchangeEventAudit miniExEventAudit = new MiniExchangeEventAudit(miniExEvent);
									miniExEventAudits.add(miniExEventAudit);
									
									miniExEvent.setTicketNetworkBrokerId(null);
									miniExEvent.setLastUpdatedBy(username);
									miniExEvent.setLastUpdatedDate(now);
									miniExEvent.setZone(isZoneEvent);
									miniExEvents.add(miniExEvent);
									
								} else if(!isZoneEvent && miniExEvent.getStatus().equals("ACTIVE") && miniExEvent.getTicketNetworkBrokerId() == null && miniExEvent.getLastUpdatedBy().equals(username) 
											&& defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									miniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									Boolean one=true;
									 String exVenueKey=null;
									    exVenueKey=MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
									    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
										   if(excludeVenues!=null && miniExEvent.getTicketNetworkBrokerId() != null ){
											   miniExEvent.setTicketNetworkBrokerId(null);
											   one=false;
										   }
									if(one){
										MiniExchangeEventAudit miniExEventAudit = new MiniExchangeEventAudit(miniExEvent);
										miniExEventAudits.add(miniExEventAudit);
										miniExEvent.setLastUpdatedBy(username);
										miniExEvent.setLastUpdatedDate(now);
										miniExEvent.setZone(isZoneEvent);
										miniExEvents.add(miniExEvent);
									}
									
									
								} else if(miniExEvent.getZone() == null || !miniExEvent.getZone().equals(isZoneEvent)) {
									MiniExchangeEventAudit miniExEventAudit = new MiniExchangeEventAudit(miniExEvent);
									miniExEventAudits.add(miniExEventAudit);
									
									miniExEvent.setZone(isZoneEvent);
									miniExEvents.add(miniExEvent);
								}
							}
							} else {
								if(miniExEvent != null && miniExEvent.getStatus().equals("ACTIVE")) {
									MiniExchangeEventAudit miniExEventAudit = new MiniExchangeEventAudit(miniExEvent);
									miniExEventAudits.add(miniExEventAudit);
									
									miniExEvent.setStatus("DELETED");
									miniExEvent.setLastUpdatedBy(username);
									miniExEvent.setLastUpdatedDate(now);
									miniExEvent.setZone(isZoneEvent);
									miniExEvents.add(miniExEvent);
								}
							}
						}
						
						if(activeProductsMap.get(SG_LAST_FIVE_ROW_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							sgLastFiveRowExEvent = existingSgLastFiveRowExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(sgLastFiveRowExEvent == null) {
								sgLastFiveRowExEvent = new SGLastFiveRowExchangeEvent();
								if(isEventExistinPOS ) {//&& !isZoneEvent
									sgLastFiveRowExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
								} /*else {
									sgLastFiveRowExEvent.setTicketNetworkBrokerId(null);
								}*/
								sgLastFiveRowExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
								sgLastFiveRowExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
								sgLastFiveRowExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
								sgLastFiveRowExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
								sgLastFiveRowExEvent.setSeatGeekBrokerId(defaultApProerties.getSeatGeekBrokerId());
								
								 /*for (ExcludeVenues excludeVenues : excludeVenueList) {
									  if(excludeVenues.getProductId().equals(MINICATS_PRODUCT_ID)){
										  if(excludeVenues.getBrokerId().equals(defaultApProerties.getTicketNetworkBrokerId())){
											    sgLastFiveRowExEvent.setTicketNetworkBrokerId(null);
										    }
										  if(excludeVenues.getBrokerId().equals(defaultApProerties.getVividBrokerId())){
										    	sgLastFiveRowExEvent.setVividBrokerId(null);
										    }
										  if(excludeVenues.getBrokerId().equals(defaultApProerties.getScoreBigBrokerId())){
										    	sgLastFiveRowExEvent.setScoreBigBrokerId(null);
										  }  
										  
									  }
								}*/
								    String exVenueKey=null;
								    exVenueKey=SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && sgLastFiveRowExEvent.getTicketNetworkBrokerId() != null ){
										   sgLastFiveRowExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && sgLastFiveRowExEvent.getVividBrokerId() != null ){
										   sgLastFiveRowExEvent.setVividBrokerId(null);
										    }
									exVenueKey=SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && sgLastFiveRowExEvent.getScoreBigBrokerId() != null ){
										sgLastFiveRowExEvent.setScoreBigBrokerId(null);
									}
									exVenueKey=SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && sgLastFiveRowExEvent.getFanxchangeBrokerId() != null ){
										sgLastFiveRowExEvent.setFanxchangeBrokerId(null);
									}
									exVenueKey=SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && sgLastFiveRowExEvent.getTicketcityBrokerId() != null ){
										sgLastFiveRowExEvent.setTicketcityBrokerId(null);
									} 
									exVenueKey=SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+defaultApProerties.getSeatGeekBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && sgLastFiveRowExEvent.getSeatGeekBrokerId() != null ){
										sgLastFiveRowExEvent.setSeatGeekBrokerId(null);
									} 
									   
								sgLastFiveRowExEvent.setEventId(eventDTO.getId());
								sgLastFiveRowExEvent.setAllowSectionRange(false);
								sgLastFiveRowExEvent.setExposure(defaultApProerties.getExposure());
								//sgLastFiveRowExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
								sgLastFiveRowExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
								//sgLastFiveRowExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
								sgLastFiveRowExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
								sgLastFiveRowExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
								sgLastFiveRowExEvent.setRptFactor(defaultApProerties.getRptFactor());
								//sgLastFiveRowExEvent.setShippingDays(defaultApProerties.getShippingDays());
								sgLastFiveRowExEvent.setStatus("ACTIVE");
								sgLastFiveRowExEvent.setLastUpdatedBy(username);
								sgLastFiveRowExEvent.setLastUpdatedDate(now);
								sgLastFiveRowExEvent.setZone(isZoneEvent);
								sgLastFiveRowExEvent.setDiscountZone(false);
								
								//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
								AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+eventDTO.getVenueId());
								if(apVenueSettings != null) {
									sgLastFiveRowExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
									sgLastFiveRowExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
									sgLastFiveRowExEvent.setLowerMarkup(apVenueSettings.getMarkup());
									sgLastFiveRowExEvent.setUpperMarkup(apVenueSettings.getMarkup());
									sgLastFiveRowExEvent.setShippingDays(apVenueSettings.getShippingDays());
								} else {
									sgLastFiveRowExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
									sgLastFiveRowExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
									sgLastFiveRowExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									sgLastFiveRowExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									sgLastFiveRowExEvent.setShippingDays(defaultApProerties.getShippingDays());
									
								}
								
								sgLastFiveRowExEvents.add(sgLastFiveRowExEvent);
								
							} else if(isEventExistinPOS) {
								/*if(isZoneEvent && sgLastFiveRowExEvent.getTicketNetworkBrokerId() != null) {
									SGLastFiveRowExchangeEventAudit sgLastFiveRowExEventAudit = new SGLastFiveRowExchangeEventAudit(sgLastFiveRowExEvent);
									sgLastFiveRowExEventAudits.add(sgLastFiveRowExEventAudit);
									
									sgLastFiveRowExEvent.setTicketNetworkBrokerId(null);
									sgLastFiveRowExEvent.setLastUpdatedBy(username);
									sgLastFiveRowExEvent.setLastUpdatedDate(now);
									sgLastFiveRowExEvent.setZone(isZoneEvent);
									sgLastFiveRowExEvents.add(sgLastFiveRowExEvent);
									
								} else*/ if(sgLastFiveRowExEvent.getStatus().equals("ACTIVE") && sgLastFiveRowExEvent.getTicketNetworkBrokerId() == null && sgLastFiveRowExEvent.getLastUpdatedBy().equals(username) 
											&& defaultApProerties.getTicketNetworkBrokerId() != null) {//!isZoneEvent &&
									
									
									sgLastFiveRowExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									Boolean one=true;
									 String exVenueKey=null;
									    exVenueKey=SG_LAST_FIVE_ROW_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
									    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
										   if(excludeVenues!=null && sgLastFiveRowExEvent.getTicketNetworkBrokerId() != null ){
											   sgLastFiveRowExEvent.setTicketNetworkBrokerId(null);
											   one=false;
										   }
									if(one){
										SGLastFiveRowExchangeEventAudit sgLastFiveRowExEventAudit = new SGLastFiveRowExchangeEventAudit(sgLastFiveRowExEvent);
										sgLastFiveRowExEventAudits.add(sgLastFiveRowExEventAudit);
										sgLastFiveRowExEvent.setLastUpdatedBy(username);
										sgLastFiveRowExEvent.setLastUpdatedDate(now);
										sgLastFiveRowExEvent.setZone(isZoneEvent);
										sgLastFiveRowExEvents.add(sgLastFiveRowExEvent);
									}
									
									
								} else if(sgLastFiveRowExEvent.getZone() == null || !sgLastFiveRowExEvent.getZone().equals(isZoneEvent)) {
									SGLastFiveRowExchangeEventAudit sgLastFiveRowExEventAudit = new SGLastFiveRowExchangeEventAudit(sgLastFiveRowExEvent);
									sgLastFiveRowExEventAudits.add(sgLastFiveRowExEventAudit);
									
									sgLastFiveRowExEvent.setZone(isZoneEvent);
									sgLastFiveRowExEvents.add(sgLastFiveRowExEvent);
								}
							}
							} else {
								if(sgLastFiveRowExEvent != null && sgLastFiveRowExEvent.getStatus().equals("ACTIVE")) {
									SGLastFiveRowExchangeEventAudit sgLastFiveRowExEventAudit = new SGLastFiveRowExchangeEventAudit(sgLastFiveRowExEvent);
									sgLastFiveRowExEventAudits.add(sgLastFiveRowExEventAudit);
									
									sgLastFiveRowExEvent.setStatus("DELETED");
									sgLastFiveRowExEvent.setLastUpdatedBy(username);
									sgLastFiveRowExEvent.setLastUpdatedDate(now);
									sgLastFiveRowExEvent.setZone(isZoneEvent);
									sgLastFiveRowExEvents.add(sgLastFiveRowExEvent);
								}
							}
						}
						
						if(activeProductsMap.get(VIP_MINICATS_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = VIP_MINICATS_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							vipMiniExEvent = existingVipMiniExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(vipMiniExEvent == null) {
								vipMiniExEvent = new VipMiniExchangeEvent();
								if(isEventExistinPOS && !isZoneEvent) {
									vipMiniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
								} else {
									vipMiniExEvent.setTicketNetworkBrokerId(null);
								}
								vipMiniExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
								vipMiniExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
								vipMiniExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
								vipMiniExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
								
								String exVenueKey=null;
								    exVenueKey=VIP_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && vipMiniExEvent.getTicketNetworkBrokerId() != null){
										   vipMiniExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=VIP_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && vipMiniExEvent.getVividBrokerId() != null){
										   vipMiniExEvent.setVividBrokerId(null);
										    }
									exVenueKey=VIP_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && vipMiniExEvent.getScoreBigBrokerId() != null){
										vipMiniExEvent.setScoreBigBrokerId(null);
									}  
									exVenueKey=VIP_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && vipMiniExEvent.getFanxchangeBrokerId() != null){
										vipMiniExEvent.setFanxchangeBrokerId(null);
									} 
									exVenueKey=VIP_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && vipMiniExEvent.getTicketcityBrokerId() != null){
										vipMiniExEvent.setTicketcityBrokerId(null);
									} 
									
								vipMiniExEvent.setEventId(eventDTO.getId());
								vipMiniExEvent.setAllowSectionRange(false);
								vipMiniExEvent.setExposure(defaultApProerties.getExposure());
								//vipMiniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
								vipMiniExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
								//vipMiniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
								vipMiniExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
								vipMiniExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
								vipMiniExEvent.setRptFactor(defaultApProerties.getRptFactor());
								//vipMiniExEvent.setShippingDays(defaultApProerties.getShippingDays());
								vipMiniExEvent.setStatus("ACTIVE");
								vipMiniExEvent.setLastUpdatedBy(username);
								vipMiniExEvent.setLastUpdatedDate(now);
								vipMiniExEvent.setZone(isZoneEvent);
								vipMiniExEvent.setDiscountZone(false);
								
								//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
								AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(VIP_MINICATS_PRODUCT_ID+"_"+eventDTO.getVenueId());
								if(apVenueSettings != null) {
									vipMiniExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
									vipMiniExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
									vipMiniExEvent.setLowerMarkup(apVenueSettings.getMarkup());
									vipMiniExEvent.setUpperMarkup(apVenueSettings.getMarkup());
									vipMiniExEvent.setShippingDays(apVenueSettings.getShippingDays());
								} else {
									vipMiniExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
									vipMiniExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
									vipMiniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									vipMiniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									vipMiniExEvent.setShippingDays(defaultApProerties.getShippingDays());
									
								}
								
								vipMiniExEvents.add(vipMiniExEvent);
								
							} else if(isEventExistinPOS) {
								if(isZoneEvent && vipMiniExEvent.getTicketNetworkBrokerId() != null) {
									VipMiniExchangeEventAudit vipMiniExEventAudit = new VipMiniExchangeEventAudit(vipMiniExEvent);
									vipMiniExEventAudits.add(vipMiniExEventAudit);
									
									vipMiniExEvent.setTicketNetworkBrokerId(null);
									vipMiniExEvent.setLastUpdatedBy(username);
									vipMiniExEvent.setLastUpdatedDate(now);
									vipMiniExEvent.setZone(isZoneEvent);
									vipMiniExEvents.add(vipMiniExEvent);
									
								} else if(!isZoneEvent  && vipMiniExEvent.getStatus().equals("ACTIVE") && vipMiniExEvent.getTicketNetworkBrokerId() == null && vipMiniExEvent.getLastUpdatedBy().equals(username) 
											&& defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									
									vipMiniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									
									String exVenueKey=null;
									Boolean one=true;
								    exVenueKey=VIP_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && vipMiniExEvent.getTicketNetworkBrokerId() != null){
										   vipMiniExEvent.setTicketNetworkBrokerId(null);
										   one=false;
									   }
									   if(one){
										   VipMiniExchangeEventAudit vipMiniExEventAudit = new VipMiniExchangeEventAudit(vipMiniExEvent);
										   vipMiniExEventAudits.add(vipMiniExEventAudit);
										   vipMiniExEvent.setLastUpdatedBy(username);
										   vipMiniExEvent.setLastUpdatedDate(now);
										   vipMiniExEvent.setZone(isZoneEvent);
										   vipMiniExEvents.add(vipMiniExEvent);
									   }
								
									
								} else if(vipMiniExEvent.getZone() == null || !vipMiniExEvent.getZone().equals(isZoneEvent)) {
									VipMiniExchangeEventAudit vipMiniExEventAudit = new VipMiniExchangeEventAudit(vipMiniExEvent);
									vipMiniExEventAudits.add(vipMiniExEventAudit);
									
									vipMiniExEvent.setZone(isZoneEvent);
									vipMiniExEvents.add(vipMiniExEvent);
								}
							}
							} else {
								if(vipMiniExEvent != null && vipMiniExEvent.getStatus().equals("ACTIVE")) {
									VipMiniExchangeEventAudit vipMiniExEventAudit = new VipMiniExchangeEventAudit(vipMiniExEvent);
									vipMiniExEventAudits.add(vipMiniExEventAudit);
									
									vipMiniExEvent.setStatus("DELETED");
									vipMiniExEvent.setLastUpdatedBy(username);
									vipMiniExEvent.setLastUpdatedDate(now);
									vipMiniExEvent.setZone(isZoneEvent);
									vipMiniExEvents.add(vipMiniExEvent);
								}
							}
						}
						
						if(activeProductsMap.get(LASTROW_MINICATS_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = LASTROW_MINICATS_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							lastRowMiniExEvent = existingLastRowMiniExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(lastRowMiniExEvent == null) {
								if(isEventExistinPOS && !isZoneEvent) {
									lastRowMiniExEvent = new LastRowMiniExchangeEvent();
									lastRowMiniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									lastRowMiniExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									lastRowMiniExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									lastRowMiniExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									lastRowMiniExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									String exVenueKey=null;
								    exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && lastRowMiniExEvent.getTicketNetworkBrokerId() != null){
										   lastRowMiniExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && lastRowMiniExEvent.getVividBrokerId() != null){
										   lastRowMiniExEvent.setVividBrokerId(null);
										    }
									exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && lastRowMiniExEvent.getScoreBigBrokerId() != null){
										lastRowMiniExEvent.setScoreBigBrokerId(null);
									}
									exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && lastRowMiniExEvent.getFanxchangeBrokerId() != null){
										lastRowMiniExEvent.setFanxchangeBrokerId(null);
									}
									exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && lastRowMiniExEvent.getTicketcityBrokerId() != null){
										lastRowMiniExEvent.setTicketcityBrokerId(null);
									}
									   
									   
									lastRowMiniExEvent.setEventId(eventDTO.getId());
									lastRowMiniExEvent.setAllowSectionRange(false);
									lastRowMiniExEvent.setExposure(defaultApProerties.getExposure());
									//lastRowMiniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									lastRowMiniExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
									//lastRowMiniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									lastRowMiniExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									lastRowMiniExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
									lastRowMiniExEvent.setRptFactor(defaultApProerties.getRptFactor());
									//lastRowMiniExEvent.setShippingDays(defaultApProerties.getShippingDays());
									lastRowMiniExEvent.setStatus("ACTIVE");
									lastRowMiniExEvent.setLastUpdatedBy(username);
									lastRowMiniExEvent.setLastUpdatedDate(now);
									lastRowMiniExEvent.setZone(isZoneEvent);
									lastRowMiniExEvent.setDiscountZone(false);
									
									//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
									AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(LASTROW_MINICATS_PRODUCT_ID+"_"+eventDTO.getVenueId());
									if(apVenueSettings != null) {
										lastRowMiniExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
										lastRowMiniExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
										lastRowMiniExEvent.setLowerMarkup(apVenueSettings.getMarkup());
										lastRowMiniExEvent.setUpperMarkup(apVenueSettings.getMarkup());
										lastRowMiniExEvent.setShippingDays(apVenueSettings.getShippingDays());
									} else {
										lastRowMiniExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
										lastRowMiniExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
										lastRowMiniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
										lastRowMiniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
										lastRowMiniExEvent.setShippingDays(defaultApProerties.getShippingDays());
										
									}
									
									lastRowMiniExEvents.add(lastRowMiniExEvent);
								} 
							} else if(isEventExistinPOS) {
								if(isZoneEvent && lastRowMiniExEvent.getStatus().equals("ACTIVE")) {
									LastRowMiniExchangeEventAudit lastRowMiniExEventAudit = new LastRowMiniExchangeEventAudit(lastRowMiniExEvent);
									lastRowMiniExEventAudits.add(lastRowMiniExEventAudit);
									
									lastRowMiniExEvent.setStatus("DELETED");
									lastRowMiniExEvent.setTicketNetworkBrokerId(null);
									lastRowMiniExEvent.setVividBrokerId(null);
									lastRowMiniExEvent.setScoreBigBrokerId(null);
									lastRowMiniExEvent.setFanxchangeBrokerId(null);
									lastRowMiniExEvent.setTicketcityBrokerId(null);
									lastRowMiniExEvent.setLastUpdatedBy(username);
									lastRowMiniExEvent.setLastUpdatedDate(now);
									lastRowMiniExEvent.setZone(isZoneEvent);
									lastRowMiniExEvents.add(lastRowMiniExEvent);
									
								} else if(!isZoneEvent && lastRowMiniExEvent.getStatus().equals("ACTIVE") && lastRowMiniExEvent.getTicketNetworkBrokerId() == null  && lastRowMiniExEvent.getLastUpdatedBy().equals(username) 
											 && defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									lastRowMiniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									lastRowMiniExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									lastRowMiniExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									lastRowMiniExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									lastRowMiniExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									String exVenueKey=null;
									Boolean one=true,two=true,three=true;
								    exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && lastRowMiniExEvent.getTicketNetworkBrokerId() != null){
										   lastRowMiniExEvent.setTicketNetworkBrokerId(null);
										   one=false;
									   }
									exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && lastRowMiniExEvent.getVividBrokerId() != null){
										   lastRowMiniExEvent.setVividBrokerId(null);
										   two=false;
										   }
									exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && lastRowMiniExEvent.getScoreBigBrokerId() != null){
										   lastRowMiniExEvent.setScoreBigBrokerId(null);
										   three=false;
										 }
									   exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
									   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && lastRowMiniExEvent.getFanxchangeBrokerId() != null){
										   lastRowMiniExEvent.setFanxchangeBrokerId(null);
										   three=false;
									   }
									   
									   exVenueKey=LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
									   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && lastRowMiniExEvent.getTicketcityBrokerId() != null){
										   lastRowMiniExEvent.setTicketcityBrokerId(null);
										   three=false;
									   }
	//
									   if(one || two || three ){
										   LastRowMiniExchangeEventAudit lastRowMiniExEventAudit = new LastRowMiniExchangeEventAudit(lastRowMiniExEvent);
										   lastRowMiniExEventAudits.add(lastRowMiniExEventAudit);
										   lastRowMiniExEvent.setLastUpdatedBy(username);
										   lastRowMiniExEvent.setLastUpdatedDate(now);
									       lastRowMiniExEvent.setZone(isZoneEvent);
										   lastRowMiniExEvents.add(lastRowMiniExEvent);
										 }   
									
									
								} else if(lastRowMiniExEvent.getZone() == null || !lastRowMiniExEvent.getZone().equals(isZoneEvent)) {
									LastRowMiniExchangeEventAudit lastRowMiniExEventAudit = new LastRowMiniExchangeEventAudit(lastRowMiniExEvent);
									lastRowMiniExEventAudits.add(lastRowMiniExEventAudit);
									
									lastRowMiniExEvent.setZone(isZoneEvent);
									lastRowMiniExEvents.add(lastRowMiniExEvent);
								}
							}
							} else {
								if(lastRowMiniExEvent != null && lastRowMiniExEvent.getStatus().equals("ACTIVE")) {
									LastRowMiniExchangeEventAudit lastRowMiniExEventAudit = new LastRowMiniExchangeEventAudit(lastRowMiniExEvent);
									lastRowMiniExEventAudits.add(lastRowMiniExEventAudit);
									
									lastRowMiniExEvent.setStatus("DELETED");
									lastRowMiniExEvent.setLastUpdatedBy(username);
									lastRowMiniExEvent.setLastUpdatedDate(now);
									lastRowMiniExEvent.setZone(isZoneEvent);
									lastRowMiniExEvents.add(lastRowMiniExEvent);
								}
							}
						}
						
						
						/*if(null != eventDTO.getPosParentCategoryName() && !eventDTO.getPosParentCategoryName().isEmpty()){
							zoonesPricingParentCatId = parentCategoryMap.get(eventDTO.getPosParentCategoryName().toUpperCase());
						}else{
							zoonesPricingParentCatId = eventDTO.getParentCategoryId();
						}*/
						
						if(activeProductsMap.get(ZONES_PRICNG_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = ZONES_PRICNG_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							zonePricingExEvent = existingZonePricingExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(zonePricingExEvent == null) {
								if(isEventExistinPOS && isZoneEvent) {
									zonePricingExEvent = new ZonePricingExchangeEvent();
									zonePricingExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									zonePricingExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									zonePricingExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									zonePricingExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									zonePricingExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									
									String exVenueKey=null;
								    exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && zonePricingExEvent.getTicketNetworkBrokerId() != null){
										   zonePricingExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && zonePricingExEvent.getVividBrokerId() != null){
										   zonePricingExEvent.setVividBrokerId(null);
										    }
									exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && zonePricingExEvent.getScoreBigBrokerId() != null){
										   zonePricingExEvent.setScoreBigBrokerId(null);
										 }  
									
								   exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && zonePricingExEvent.getFanxchangeBrokerId() != null){
									   zonePricingExEvent.setFanxchangeBrokerId(null);
								   } 
								   exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && zonePricingExEvent.getTicketcityBrokerId() != null){
									   zonePricingExEvent.setTicketcityBrokerId(null);
								   } 
									
									zonePricingExEvent.setEventId(eventDTO.getId());
									zonePricingExEvent.setAllowSectionRange(false);
									zonePricingExEvent.setExposure(defaultApProerties.getExposure());
									//zonePricingExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									zonePricingExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
									//zonePricingExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									zonePricingExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									zonePricingExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
									zonePricingExEvent.setRptFactor(defaultApProerties.getRptFactor());
									//zonePricingExEvent.setShippingDays(defaultApProerties.getShippingDays());
									zonePricingExEvent.setStatus("ACTIVE");
									zonePricingExEvent.setLastUpdatedBy(username);
									zonePricingExEvent.setLastUpdatedDate(now);
									zonePricingExEvent.setZone(isZoneEvent);
									zonePricingExEvent.setDiscountZone(false);
									
									//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
									AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(ZONES_PRICNG_PRODUCT_ID+"_"+eventDTO.getVenueId());
									if(apVenueSettings != null) {
										zonePricingExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
										zonePricingExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
										zonePricingExEvent.setLowerMarkup(apVenueSettings.getMarkup());
										zonePricingExEvent.setUpperMarkup(apVenueSettings.getMarkup());
										zonePricingExEvent.setShippingDays(apVenueSettings.getShippingDays());
									} else {
										zonePricingExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
										zonePricingExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
										zonePricingExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
										zonePricingExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
										zonePricingExEvent.setShippingDays(defaultApProerties.getShippingDays());
										
									}
									
									zonePricingExEvents.add(zonePricingExEvent);
								}
								
							} else if(isEventExistinPOS) {
								if(!isZoneEvent && zonePricingExEvent.getStatus().equals("ACTIVE")) {
									ZonePricingExchangeEventAudit zonePricingExEventAudit = new ZonePricingExchangeEventAudit(zonePricingExEvent);
									zonePricingExEventAudits.add(zonePricingExEventAudit);
									
									zonePricingExEvent.setStatus("DELETED");
									zonePricingExEvent.setTicketNetworkBrokerId(null);
									zonePricingExEvent.setVividBrokerId(null);
									zonePricingExEvent.setScoreBigBrokerId(null);
									zonePricingExEvent.setFanxchangeBrokerId(null);
									zonePricingExEvent.setTicketcityBrokerId(null);
									zonePricingExEvent.setLastUpdatedBy(username);
									zonePricingExEvent.setLastUpdatedDate(now);
									zonePricingExEvent.setZone(isZoneEvent);
									zonePricingExEvents.add(zonePricingExEvent);
									
								} else if(isZoneEvent && zonePricingExEvent.getStatus().equals("ACTIVE") && zonePricingExEvent.getTicketNetworkBrokerId() == null && zonePricingExEvent.getLastUpdatedBy().equals(username) 
											&& defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									zonePricingExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									zonePricingExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									zonePricingExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									zonePricingExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									zonePricingExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									Boolean one=true,two=true,three=true;
									String exVenueKey=null;
								    exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && zonePricingExEvent.getTicketNetworkBrokerId() != null){
										   zonePricingExEvent.setTicketNetworkBrokerId(null);
										   one=false;
									   }
									exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && zonePricingExEvent.getVividBrokerId() != null){
										   zonePricingExEvent.setVividBrokerId(null);
										   two=false;
										    }
									exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && zonePricingExEvent.getScoreBigBrokerId() != null){
										   zonePricingExEvent.setScoreBigBrokerId(null);
										   three=false;
										 }
								   exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && zonePricingExEvent.getFanxchangeBrokerId() != null){
									   zonePricingExEvent.setFanxchangeBrokerId(null);
									   three=false;
								   } 
								   
								   exVenueKey=ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && zonePricingExEvent.getTicketcityBrokerId() != null){
									   zonePricingExEvent.setTicketcityBrokerId(null);
									   three=false;
								   } 
									   if(one || two || three ){
										   ZonePricingExchangeEventAudit zonePricingExEventAudit = new ZonePricingExchangeEventAudit(zonePricingExEvent);
										   zonePricingExEventAudits.add(zonePricingExEventAudit);
										   zonePricingExEvent.setLastUpdatedBy(username);
										   zonePricingExEvent.setLastUpdatedDate(now);
										   zonePricingExEvent.setZone(isZoneEvent);
										   zonePricingExEvents.add(zonePricingExEvent);
									   }
									   
									
									
								} else if(zonePricingExEvent.getZone() == null || !zonePricingExEvent.getZone().equals(isZoneEvent)) {
									ZonePricingExchangeEventAudit zonePricingExEventAudit = new ZonePricingExchangeEventAudit(zonePricingExEvent);
									zonePricingExEventAudits.add(zonePricingExEventAudit);
									
									zonePricingExEvent.setZone(isZoneEvent);
									zonePricingExEvents.add(zonePricingExEvent);
								}
							}
							} else {
								if(zonePricingExEvent != null && zonePricingExEvent.getStatus().equals("ACTIVE")) {
									ZonePricingExchangeEventAudit zonePricingExEventAudit = new ZonePricingExchangeEventAudit(zonePricingExEvent);
									zonePricingExEventAudits.add(zonePricingExEventAudit);
									
									zonePricingExEvent.setStatus("DELETED");
									zonePricingExEvent.setLastUpdatedBy(username);
									zonePricingExEvent.setLastUpdatedDate(now);
									zonePricingExEvent.setZone(isZoneEvent);
									zonePricingExEvents.add(zonePricingExEvent);
								}
							}
						}
						
						if(activeProductsMap.get(TIXCITY_ZONES_PRICNG_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							tixCityZonePricingExEvent = existingTixCityZonePricingExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(tixCityZonePricingExEvent == null) {
								
								//Tamil : For RTW process Hamilton Events of Richard Rodgers Theatre,Bank of America Theatre, Pantages Theatre - CA venue only
								if(isEventExistinPOS && isZoneEvent) {
										//&& eventDTO.getName().equalsIgnoreCase("Hamilton") && 
										//(eventDTO.getVenueId().equals(8327) || eventDTO.getVenueId().equals(8533) || eventDTO.getVenueId().equals(8932))) {
									tixCityZonePricingExEvent = new TixCityZonePricingExchangeEvent();
									tixCityZonePricingExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									tixCityZonePricingExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									tixCityZonePricingExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									tixCityZonePricingExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									tixCityZonePricingExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									
									String exVenueKey=null;
								    exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && tixCityZonePricingExEvent.getTicketNetworkBrokerId() != null){
										   tixCityZonePricingExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && tixCityZonePricingExEvent.getVividBrokerId() != null){
										   tixCityZonePricingExEvent.setVividBrokerId(null);
										    }
									exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && tixCityZonePricingExEvent.getScoreBigBrokerId() != null){
										   tixCityZonePricingExEvent.setScoreBigBrokerId(null);
										 }  
									
								   exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && tixCityZonePricingExEvent.getFanxchangeBrokerId() != null){
									   tixCityZonePricingExEvent.setFanxchangeBrokerId(null);
								   }  
								   exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && tixCityZonePricingExEvent.getTicketcityBrokerId() != null){
									   tixCityZonePricingExEvent.setTicketcityBrokerId(null);
								   }  
									
									tixCityZonePricingExEvent.setEventId(eventDTO.getId());
									tixCityZonePricingExEvent.setAllowSectionRange(false);
									tixCityZonePricingExEvent.setExposure(defaultApProerties.getExposure());
									//tixCityZonePricingExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									tixCityZonePricingExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
									//tixCityZonePricingExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									tixCityZonePricingExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									tixCityZonePricingExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
									tixCityZonePricingExEvent.setRptFactor(defaultApProerties.getRptFactor());
									//tixCityZonePricingExEvent.setShippingDays(defaultApProerties.getShippingDays());
									tixCityZonePricingExEvent.setStatus("ACTIVE");
									tixCityZonePricingExEvent.setLastUpdatedBy(username);
									tixCityZonePricingExEvent.setLastUpdatedDate(now);
									tixCityZonePricingExEvent.setZone(isZoneEvent);
									tixCityZonePricingExEvent.setDiscountZone(false);
									
									//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
									AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+eventDTO.getVenueId());
									if(apVenueSettings != null) {
										tixCityZonePricingExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
										tixCityZonePricingExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
										tixCityZonePricingExEvent.setLowerMarkup(apVenueSettings.getMarkup());
										tixCityZonePricingExEvent.setUpperMarkup(apVenueSettings.getMarkup());
										tixCityZonePricingExEvent.setShippingDays(apVenueSettings.getShippingDays());
									} else {
										tixCityZonePricingExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
										tixCityZonePricingExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
										tixCityZonePricingExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
										tixCityZonePricingExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
										tixCityZonePricingExEvent.setShippingDays(defaultApProerties.getShippingDays());
										
									}
									
									tixCityZonePricingExEvents.add(tixCityZonePricingExEvent);
								}
								
							} else if(isEventExistinPOS) {
								//Tamil : For RTW process Hamilton Events of Richard Rodgers Theatre,Bank of America Theatre, Pantages Theatre - CA venue only
								if((!isZoneEvent && tixCityZonePricingExEvent.getStatus().equals("ACTIVE"))
										//|| ((!eventDTO.getName().equalsIgnoreCase("Hamilton") || 
												//(!eventDTO.getVenueId().equals(8327) && !eventDTO.getVenueId().equals(8533) && !eventDTO.getVenueId().equals(8932))) &&
												//tixCityZonePricingExEvent.getStatus().equals("ACTIVE"))
												) {
									TixCityZonePricingExchangeEventAudit tixCityZonePricingExEventAudit = new TixCityZonePricingExchangeEventAudit(tixCityZonePricingExEvent);
									tixCityZonePricingExEventAudits.add(tixCityZonePricingExEventAudit);
									
									tixCityZonePricingExEvent.setStatus("DELETED");
									//tixCityZonePricingExEvent.setTicketNetworkBrokerId(null);
									//tixCityZonePricingExEvent.setVividBrokerId(null);
									//tixCityZonePricingExEvent.setScoreBigBrokerId(null);
									//tixCityZonePricingExEvent.setFanxchangeBrokerId(null);
									//tixCityZonePricingExEvent.setTicketcityBrokerId(null);
									tixCityZonePricingExEvent.setLastUpdatedBy(username);
									tixCityZonePricingExEvent.setLastUpdatedDate(now);
									tixCityZonePricingExEvent.setZone(isZoneEvent);
									tixCityZonePricingExEvents.add(tixCityZonePricingExEvent);
									
								} else if(isZoneEvent && tixCityZonePricingExEvent.getStatus().equals("ACTIVE") && tixCityZonePricingExEvent.getTicketNetworkBrokerId() == null && tixCityZonePricingExEvent.getLastUpdatedBy().equals(username) 
											&& defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									tixCityZonePricingExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									tixCityZonePricingExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									tixCityZonePricingExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									tixCityZonePricingExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									tixCityZonePricingExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									Boolean one=true,two=true,three=true;
									String exVenueKey=null;
								    exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && tixCityZonePricingExEvent.getTicketNetworkBrokerId() != null){
										   tixCityZonePricingExEvent.setTicketNetworkBrokerId(null);
										   one=false;
									   }
									exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && tixCityZonePricingExEvent.getVividBrokerId() != null){
										   tixCityZonePricingExEvent.setVividBrokerId(null);
										   two=false;
										    }
									exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && tixCityZonePricingExEvent.getScoreBigBrokerId() != null){
										   tixCityZonePricingExEvent.setScoreBigBrokerId(null);
										   three=false;
										 }
								   exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && tixCityZonePricingExEvent.getFanxchangeBrokerId() != null){
									   tixCityZonePricingExEvent.setFanxchangeBrokerId(null);
									   three=false;
								   } 
								   exVenueKey=TIXCITY_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && tixCityZonePricingExEvent.getTicketcityBrokerId() != null){
									   tixCityZonePricingExEvent.setTicketcityBrokerId(null);
									   three=false;
								   } 
									   if(one || two || three ){
										   TixCityZonePricingExchangeEventAudit tixCityZonePricingExEventAudit = new TixCityZonePricingExchangeEventAudit(tixCityZonePricingExEvent);
										   tixCityZonePricingExEventAudits.add(tixCityZonePricingExEventAudit);
										   tixCityZonePricingExEvent.setLastUpdatedBy(username);
										   tixCityZonePricingExEvent.setLastUpdatedDate(now);
										   tixCityZonePricingExEvent.setZone(isZoneEvent);
										   tixCityZonePricingExEvents.add(tixCityZonePricingExEvent);
									   }
									   
									
									
								} else if(tixCityZonePricingExEvent.getZone() == null || !tixCityZonePricingExEvent.getZone().equals(isZoneEvent)) {
									TixCityZonePricingExchangeEventAudit tixCityZonePricingExEventAudit = new TixCityZonePricingExchangeEventAudit(tixCityZonePricingExEvent);
									tixCityZonePricingExEventAudits.add(tixCityZonePricingExEventAudit);
									
									tixCityZonePricingExEvent.setZone(isZoneEvent);
									tixCityZonePricingExEvents.add(tixCityZonePricingExEvent);
								}
							}
							} else {
								if(tixCityZonePricingExEvent != null && tixCityZonePricingExEvent.getStatus().equals("ACTIVE")) {
									TixCityZonePricingExchangeEventAudit tixCityZonePricingExEventAudit = new TixCityZonePricingExchangeEventAudit(tixCityZonePricingExEvent);
									tixCityZonePricingExEventAudits.add(tixCityZonePricingExEventAudit);
									
									tixCityZonePricingExEvent.setStatus("DELETED");
									tixCityZonePricingExEvent.setLastUpdatedBy(username);
									tixCityZonePricingExEvent.setLastUpdatedDate(now);
									tixCityZonePricingExEvent.setZone(isZoneEvent);
									tixCityZonePricingExEvents.add(tixCityZonePricingExEvent);
								}
							}
						}
						
						// manhattan zone pricing
						//System.out.println("Child cat. name "+ eventDTO.getPosChildCategoryName());
						
						if(activeProductsMap.get(MANHATTAN_ZONES_PRICNG_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							manhattanZonePricingExEvent = existingManhattanZonePricingExEventMap.remove(eventDTO.getId());
							
							Date date = null;
							if(eventDTO.getEventDate()!= null){
								date = df.parse(df.format(eventDTO.getEventDate()));	
							}
							if(eventDTO.getPosChildCategoryName() != null && eventDTO.getPosChildCategoryName().equalsIgnoreCase("broadway")
									&& date!=null && ((date.getTime()-now.getTime()) <= 7 * 24 * 60 * 60 * 1000) && isEventEligigbleForTN) {
								
							if(manhattanZonePricingExEvent == null) {
								if(isEventExistinPOS && isZoneEvent) {
									manhattanZonePricingExEvent = new ManhattanZonePricingExchangeEvent();
									manhattanZonePricingExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									manhattanZonePricingExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									manhattanZonePricingExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									manhattanZonePricingExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									manhattanZonePricingExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									
									String exVenueKey=null;
								    exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && manhattanZonePricingExEvent.getTicketNetworkBrokerId() != null){
										   manhattanZonePricingExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && manhattanZonePricingExEvent.getVividBrokerId() != null){
										   manhattanZonePricingExEvent.setVividBrokerId(null);
										    }
									exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && manhattanZonePricingExEvent.getScoreBigBrokerId() != null){
										   manhattanZonePricingExEvent.setScoreBigBrokerId(null);
										 }  
									
								   exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && manhattanZonePricingExEvent.getFanxchangeBrokerId() != null){
									   manhattanZonePricingExEvent.setFanxchangeBrokerId(null);
								   }  
								   exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && manhattanZonePricingExEvent.getTicketcityBrokerId() != null){
									   manhattanZonePricingExEvent.setTicketcityBrokerId(null);
								   } 
									
								   manhattanZonePricingExEvent.setEventId(eventDTO.getId());
								   manhattanZonePricingExEvent.setAllowSectionRange(false);
								   manhattanZonePricingExEvent.setExposure(defaultApProerties.getExposure());
								  // manhattanZonePricingExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
								   manhattanZonePricingExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
								  // manhattanZonePricingExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
								   manhattanZonePricingExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
								   manhattanZonePricingExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
								   manhattanZonePricingExEvent.setRptFactor(defaultApProerties.getRptFactor());
								  // manhattanZonePricingExEvent.setShippingDays(defaultApProerties.getShippingDays());
								   manhattanZonePricingExEvent.setStatus("ACTIVE");
								   manhattanZonePricingExEvent.setLastUpdatedBy(username);
								   manhattanZonePricingExEvent.setLastUpdatedDate(now);
								   manhattanZonePricingExEvent.setZone(isZoneEvent);
								   manhattanZonePricingExEvent.setDiscountZone(false);
								   
								 //Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
									AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+eventDTO.getVenueId());
									if(apVenueSettings != null) {
										manhattanZonePricingExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
										manhattanZonePricingExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
										manhattanZonePricingExEvent.setLowerMarkup(apVenueSettings.getMarkup());
										manhattanZonePricingExEvent.setUpperMarkup(apVenueSettings.getMarkup());
										manhattanZonePricingExEvent.setShippingDays(apVenueSettings.getShippingDays());
									} else {
										manhattanZonePricingExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
										manhattanZonePricingExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
										manhattanZonePricingExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
										manhattanZonePricingExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
										manhattanZonePricingExEvent.setShippingDays(defaultApProerties.getShippingDays());
										
									}
									
								   manhattanZonePricingExEvents.add(manhattanZonePricingExEvent);
								}
								
							} else if(isEventExistinPOS) {
								if(!isZoneEvent && manhattanZonePricingExEvent.getStatus().equals("ACTIVE")) {
									ManhattanZonePricingExchangeEventAudit manhattanZonePricingExEventAudit = new ManhattanZonePricingExchangeEventAudit(manhattanZonePricingExEvent);
									manhattanZonePricingExEventAudits.add(manhattanZonePricingExEventAudit);
									
									manhattanZonePricingExEvent.setStatus("DELETED");
									manhattanZonePricingExEvent.setTicketNetworkBrokerId(null);
									manhattanZonePricingExEvent.setVividBrokerId(null);
									manhattanZonePricingExEvent.setScoreBigBrokerId(null);
									manhattanZonePricingExEvent.setFanxchangeBrokerId(null);
									manhattanZonePricingExEvent.setTicketcityBrokerId(null);
									manhattanZonePricingExEvent.setLastUpdatedBy(username);
									manhattanZonePricingExEvent.setLastUpdatedDate(now);
									manhattanZonePricingExEvent.setZone(isZoneEvent);
									manhattanZonePricingExEvents.add(manhattanZonePricingExEvent);
									
								} else if(isZoneEvent && manhattanZonePricingExEvent.getStatus().equals("ACTIVE") && manhattanZonePricingExEvent.getTicketNetworkBrokerId() == null && manhattanZonePricingExEvent.getLastUpdatedBy().equals(username) 
											&& defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									manhattanZonePricingExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									manhattanZonePricingExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									manhattanZonePricingExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									manhattanZonePricingExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									manhattanZonePricingExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									Boolean one=true,two=true,three=true;
									String exVenueKey=null;
								    exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && manhattanZonePricingExEvent.getTicketNetworkBrokerId() != null){
										   manhattanZonePricingExEvent.setTicketNetworkBrokerId(null);
										   one=false;
									   }
									exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && manhattanZonePricingExEvent.getVividBrokerId() != null){
										   manhattanZonePricingExEvent.setVividBrokerId(null);
										   two=false;
										    }
									exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && manhattanZonePricingExEvent.getScoreBigBrokerId() != null){
										   manhattanZonePricingExEvent.setScoreBigBrokerId(null);
										   three=false;
										 }
								   exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && manhattanZonePricingExEvent.getFanxchangeBrokerId() != null){
									   manhattanZonePricingExEvent.setFanxchangeBrokerId(null);
									   three=false;
								   } 
								   exVenueKey=MANHATTAN_ZONES_PRICNG_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && manhattanZonePricingExEvent.getTicketcityBrokerId() != null){
									   manhattanZonePricingExEvent.setTicketcityBrokerId(null);
									   three=false;
								   } 
									   if(one || two || three ){
										   ManhattanZonePricingExchangeEventAudit manhattanZonePricingExEventAudit = new ManhattanZonePricingExchangeEventAudit(manhattanZonePricingExEvent);
										   manhattanZonePricingExEventAudits.add(manhattanZonePricingExEventAudit);
										   manhattanZonePricingExEvent.setLastUpdatedBy(username);
										   manhattanZonePricingExEvent.setLastUpdatedDate(now);
										   manhattanZonePricingExEvent.setZone(isZoneEvent);
										   manhattanZonePricingExEvents.add(manhattanZonePricingExEvent);
									   }
									   
									
									
								} else if(manhattanZonePricingExEvent.getZone() == null || !manhattanZonePricingExEvent.getZone().equals(isZoneEvent)) {
									ManhattanZonePricingExchangeEventAudit manhattanZonePricingExEventAudit = new ManhattanZonePricingExchangeEventAudit(manhattanZonePricingExEvent);
									manhattanZonePricingExEventAudits.add(manhattanZonePricingExEventAudit);
									
									manhattanZonePricingExEvent.setZone(isZoneEvent);
									manhattanZonePricingExEvents.add(manhattanZonePricingExEvent);
								}
							}
						}else if(isEventExistinPOS) {
							// checking if event's child category changed from broadway to anything else and that event is present in pos and exchange event or not
							if(manhattanZonePricingExEvent != null && manhattanZonePricingExEvent.getStatus().equals("ACTIVE")){
								ManhattanZonePricingExchangeEventAudit manhattanZonePricingExEventAudit = new ManhattanZonePricingExchangeEventAudit(manhattanZonePricingExEvent);
								manhattanZonePricingExEventAudits.add(manhattanZonePricingExEventAudit);
								
								manhattanZonePricingExEvent.setStatus("DELETED");
								manhattanZonePricingExEvent.setTicketNetworkBrokerId(null);
								manhattanZonePricingExEvent.setVividBrokerId(null);
								manhattanZonePricingExEvent.setScoreBigBrokerId(null);
								manhattanZonePricingExEvent.setFanxchangeBrokerId(null);
								manhattanZonePricingExEvent.setTicketcityBrokerId(null);
								manhattanZonePricingExEvent.setLastUpdatedBy(username);
								manhattanZonePricingExEvent.setLastUpdatedDate(now);
								manhattanZonePricingExEvent.setZone(isZoneEvent);
								manhattanZonePricingExEvents.add(manhattanZonePricingExEvent);
							}
						}
						}
							
						
							//Mehul : added for vip last row cat product
						
						if(activeProductsMap.get(VIPLASTROW_MINICATS_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = VIPLASTROW_MINICATS_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							vipLastRowMiniExEvent = existingVipLastRowMiniExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(vipLastRowMiniExEvent == null) {
								if(isEventExistinPOS && !isZoneEvent) {
									vipLastRowMiniExEvent = new VipLastRowMiniExchangeEvent();
									vipLastRowMiniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									vipLastRowMiniExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									vipLastRowMiniExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									vipLastRowMiniExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									vipLastRowMiniExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									String exVenueKey=null;
								    exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null){
										   vipLastRowMiniExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null){
										   vipLastRowMiniExEvent.setVividBrokerId(null);
										    }
									exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null){
										vipLastRowMiniExEvent.setScoreBigBrokerId(null);
									}
									exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null){
										vipLastRowMiniExEvent.setFanxchangeBrokerId(null);
									}
									exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null){
										vipLastRowMiniExEvent.setTicketcityBrokerId(null);
									}
									   
									   
									vipLastRowMiniExEvent.setEventId(eventDTO.getId());
									vipLastRowMiniExEvent.setAllowSectionRange(false);
									vipLastRowMiniExEvent.setExposure(defaultApProerties.getExposure());
									vipLastRowMiniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									vipLastRowMiniExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
									vipLastRowMiniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									vipLastRowMiniExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									vipLastRowMiniExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
									vipLastRowMiniExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
									vipLastRowMiniExEvent.setRptFactor(defaultApProerties.getRptFactor());
									vipLastRowMiniExEvent.setShippingDays(defaultApProerties.getShippingDays());
									vipLastRowMiniExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
									vipLastRowMiniExEvent.setStatus("ACTIVE");
									vipLastRowMiniExEvent.setLastUpdatedBy(username);
									vipLastRowMiniExEvent.setLastUpdatedDate(now);
									vipLastRowMiniExEvent.setZone(isZoneEvent);
									vipLastRowMiniExEvent.setDiscountZone(false);
									
									vipLastRowMiniExEvents.add(vipLastRowMiniExEvent);
								} 
							} else if(isEventExistinPOS) {
								if(isZoneEvent && vipLastRowMiniExEvent.getStatus().equals("ACTIVE")) {
									VipLastRowMiniExchangeEventAudit vipLastRowMiniExEventAudit = new VipLastRowMiniExchangeEventAudit(vipLastRowMiniExEvent);
									vipLastRowMiniExEventAudits.add(vipLastRowMiniExEventAudit);
									
									vipLastRowMiniExEvent.setStatus("DELETED");
									vipLastRowMiniExEvent.setTicketNetworkBrokerId(null);
									vipLastRowMiniExEvent.setVividBrokerId(null);
									vipLastRowMiniExEvent.setScoreBigBrokerId(null);
									vipLastRowMiniExEvent.setFanxchangeBrokerId(null);
									vipLastRowMiniExEvent.setTicketcityBrokerId(null);
									vipLastRowMiniExEvent.setLastUpdatedBy(username);
									vipLastRowMiniExEvent.setLastUpdatedDate(now);
									vipLastRowMiniExEvent.setZone(isZoneEvent);
									vipLastRowMiniExEvents.add(vipLastRowMiniExEvent);
									
								} else if(!isZoneEvent && vipLastRowMiniExEvent.getStatus().equals("ACTIVE") && vipLastRowMiniExEvent.getTicketNetworkBrokerId() == null  && vipLastRowMiniExEvent.getLastUpdatedBy().equals(username) 
											 && defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									vipLastRowMiniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									vipLastRowMiniExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									vipLastRowMiniExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									vipLastRowMiniExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									vipLastRowMiniExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									String exVenueKey=null;
									Boolean one=true,two=true,three=true;
								    exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null){
										   vipLastRowMiniExEvent.setTicketNetworkBrokerId(null);
										   one=false;
									   }
									exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null){
										   vipLastRowMiniExEvent.setVividBrokerId(null);
										   two=false;
										   }
									exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null){
										   vipLastRowMiniExEvent.setScoreBigBrokerId(null);
										   three=false;
										 }
									   exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
									   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null){
										   vipLastRowMiniExEvent.setFanxchangeBrokerId(null);
										   three=false;
									   }
									   
									   exVenueKey=VIPLASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
									   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null){
										   vipLastRowMiniExEvent.setTicketcityBrokerId(null);
										   three=false;
									   }
	//
									   if(one || two || three ){
										   VipLastRowMiniExchangeEventAudit vipLastRowMiniExEventAudit = new VipLastRowMiniExchangeEventAudit(vipLastRowMiniExEvent);
										   vipLastRowMiniExEventAudits.add(vipLastRowMiniExEventAudit);
										   vipLastRowMiniExEvent.setLastUpdatedBy(username);
										   vipLastRowMiniExEvent.setLastUpdatedDate(now);
										   vipLastRowMiniExEvent.setZone(isZoneEvent);
										   vipLastRowMiniExEvents.add(vipLastRowMiniExEvent);
										 }   
									
									
								} else if(vipLastRowMiniExEvent.getZone() == null || !vipLastRowMiniExEvent.getZone().equals(isZoneEvent)) {
									VipLastRowMiniExchangeEventAudit vipLastRowMiniExEventAudit = new VipLastRowMiniExchangeEventAudit(vipLastRowMiniExEvent);
									vipLastRowMiniExEventAudits.add(vipLastRowMiniExEventAudit);
									
									vipLastRowMiniExEvent.setZone(isZoneEvent);
									vipLastRowMiniExEvents.add(vipLastRowMiniExEvent);
								}
							}
							} else {
								if(vipLastRowMiniExEvent != null && vipLastRowMiniExEvent.getStatus().equals("ACTIVE")) {
									VipLastRowMiniExchangeEventAudit vipLastRowMiniExEventAudit = new VipLastRowMiniExchangeEventAudit(vipLastRowMiniExEvent);
									vipLastRowMiniExEventAudits.add(vipLastRowMiniExEventAudit);
									
									vipLastRowMiniExEvent.setStatus("DELETED");
									vipLastRowMiniExEvent.setLastUpdatedBy(username);
									vipLastRowMiniExEvent.setLastUpdatedDate(now);
									vipLastRowMiniExEvent.setZone(isZoneEvent);
									vipLastRowMiniExEvents.add(vipLastRowMiniExEvent);
								}
							}
						}
							// Mehul : vip last row mini cats ends here
						
						if(activeProductsMap.get(LARRYLAST_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = LARRYLAST_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							larryLastExEvent = existingLarryLastExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(larryLastExEvent == null) {
								larryLastExEvent = new LarryLastExchangeEvent();
								if(isEventExistinPOS && isZoneEvent) {
									/*larryLastExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									larryLastExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									larryLastExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									larryLastExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									larryLastExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
	
									String exVenueKey=null;
								    exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && larryLastExEvent.getTicketNetworkBrokerId() != null){
										   larryLastExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && larryLastExEvent.getVividBrokerId() != null){
										   larryLastExEvent.setVividBrokerId(null);
										    }
									exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && larryLastExEvent.getScoreBigBrokerId() != null){
										   larryLastExEvent.setScoreBigBrokerId(null);
										 }
								   exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && larryLastExEvent.getFanxchangeBrokerId() != null){
									   larryLastExEvent.setFanxchangeBrokerId(null);
								   }
								   exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && larryLastExEvent.getTicketcityBrokerId() != null){
									   larryLastExEvent.setTicketcityBrokerId(null);
								   }*/
									
									larryLastExEvent.setVividEnabled(defaultApProerties.getVividSeatEnabled());
									larryLastExEvent.setRotEnabled(defaultApProerties.getRotEnabled());
									larryLastExEvent.setTixcityEnabled(defaultApProerties.getTixcityEnabled());
									larryLastExEvent.setRtwEnabled(defaultApProerties.getRtwEnabled());
									
									  
									   String exVenueKey=null;
									   if(defaultApProerties.getRotEnabled().equals(true)){
										    exVenueKey=LARRYLAST_PRODUCT_ID+"_"+10+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && larryLastExEvent.getRotEnabled() != null && larryLastExEvent.getRotEnabled()){
												   larryLastExEvent.setRotEnabled(false);
											   }
										   }
									   if(defaultApProerties.getTixcityEnabled().equals(true)){
										    exVenueKey=LARRYLAST_PRODUCT_ID+"_"+TIXCITY_BROKER_ID+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && larryLastExEvent.getTixcityEnabled() != null && larryLastExEvent.getTixcityEnabled()){
												   larryLastExEvent.setTixcityEnabled(false);
											   }
										   }
									   if(defaultApProerties.getRtwEnabled().equals(true)){
										    exVenueKey=LARRYLAST_PRODUCT_ID+"_"+5+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && larryLastExEvent.getRtwEnabled() != null && larryLastExEvent.getRtwEnabled()){
												   larryLastExEvent.setRtwEnabled(false);
											   }
										   }
									
									larryLastExEvent.setEventId(eventDTO.getId());
									larryLastExEvent.setAllowSectionRange(false);
									//larryLastExEvent.setExposure(defaultApProerties.getExposure());
									//larryLastExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									//larryLastExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
									//larryLastExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									//larryLastExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									larryLastExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
									larryLastExEvent.setRptFactor(defaultApProerties.getRptFactor());
									//larryLastExEvent.setShippingDays(defaultApProerties.getShippingDays());
									larryLastExEvent.setStatus("ACTIVE");
									larryLastExEvent.setLastUpdatedBy(username);
									larryLastExEvent.setLastUpdatedDate(now);
									larryLastExEvent.setZone(isZoneEvent);
									larryLastExEvent.setDiscountZone(false);
									larryLastExEvent.setZonedLastrowMinicatsEnabled(defaultApProerties.getZonedLastrowMinicatsEnabled());
									larryLastExEvent.setLarryLastEnabled(defaultApProerties.getLarryLastEnabled());
									
									
									//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
									AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(LARRYLAST_PRODUCT_ID+"_"+eventDTO.getVenueId());
									if(apVenueSettings != null) {
										larryLastExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
										larryLastExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
										//larryLastExEvent.setLowerMarkup(apVenueSettings.getMarkup());
										//larryLastExEvent.setUpperMarkup(apVenueSettings.getMarkup());
										larryLastExEvent.setShippingDays(apVenueSettings.getShippingDays());
									} else {
										larryLastExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
										larryLastExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
										//larryLastExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
										//larryLastExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
										larryLastExEvent.setShippingDays(defaultApProerties.getShippingDays());
										
									}
									
									//Tamil 04/27/2018 : for first 4 days from evnet created date use markups as 0 and thne update proper markup settings so moved below logic to nextelse if
									//If evnet is created before 5 days then use normal settings otherwise use 0 markup settings
									if(!eventDTO.getEventCreatedDate().before(presaleSettingDate)) {
										larryLastExEvent.setLowerMarkup(0.0);
										larryLastExEvent.setUpperMarkup(0.0);
										larryLastExEvent.setExposure("1-OXP");
										larryLastExEvent.setLowerShippingFees(0.0);
										larryLastExEvent.setUpperShippingFees(0.0);
										larryLastExEvent.setPreSaleSettingsUpdated(Boolean.FALSE);
									} else {
										if(apVenueSettings != null) {
											larryLastExEvent.setLowerMarkup(apVenueSettings.getMarkup());
											larryLastExEvent.setUpperMarkup(apVenueSettings.getMarkup());
										} else {
											larryLastExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
											larryLastExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
										}
										larryLastExEvent.setExposure(defaultApProerties.getExposure());
										larryLastExEvent.setPreSaleSettingsUpdated(Boolean.TRUE);
										larryLastExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
										larryLastExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									}
									
									larryLastExEvents.add(larryLastExEvent);
								}
								
								
							} else if(!larryLastExEvent.getPreSaleSettingsUpdated() && eventDTO.getEventCreatedDate().before(presaleSettingDate) &&
									(larryLastExEvent.getLastUpdatedBy().equalsIgnoreCase(username) || larryLastExEvent.getLastUpdatedBy().equalsIgnoreCase("Manual"))) {
								
								LarryLastExchangeEventAudit larryLastExEventAudit = new LarryLastExchangeEventAudit(larryLastExEvent);
								larryLastExEventAudits.add(larryLastExEventAudit);
								
								//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
								AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(LARRYLAST_PRODUCT_ID+"_"+eventDTO.getVenueId());
								if(apVenueSettings != null) {
									larryLastExEvent.setLowerMarkup(apVenueSettings.getMarkup());
									larryLastExEvent.setUpperMarkup(apVenueSettings.getMarkup());
								} else {
									larryLastExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									larryLastExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
								}
								larryLastExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
								larryLastExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
								larryLastExEvent.setExposure(defaultApProerties.getExposure());
								larryLastExEvent.setPreSaleSettingsUpdated(Boolean.TRUE);
								larryLastExEvent.setLastUpdatedBy(username);
								larryLastExEvent.setLastUpdatedDate(now);
								larryLastExEvent.setZone(isZoneEvent);
								larryLastExEvents.add(larryLastExEvent);
								
								larryLastExEvents.add(larryLastExEvent);
								
							} else if(isEventExistinPOS) {
								if(!isZoneEvent && larryLastExEvent.getStatus().equals("ACTIVE")) {
									LarryLastExchangeEventAudit larryLastExEventAudit = new LarryLastExchangeEventAudit(larryLastExEvent);
									larryLastExEventAudits.add(larryLastExEventAudit);
									
									larryLastExEvent.setStatus("DELETED");
									/*larryLastExEvent.setTicketNetworkBrokerId(null);
									larryLastExEvent.setVividBrokerId(null);
									larryLastExEvent.setScoreBigBrokerId(null);
									larryLastExEvent.setFanxchangeBrokerId(null);
									larryLastExEvent.setTicketcityBrokerId(null);*/
									larryLastExEvent.setLastUpdatedBy(username);
									larryLastExEvent.setLastUpdatedDate(now);
									larryLastExEvent.setZone(isZoneEvent);
									larryLastExEvents.add(larryLastExEvent);
									
								} else if(isZoneEvent && larryLastExEvent.getStatus().equals("ACTIVE") && larryLastExEvent.getLastUpdatedBy().equals(username) && 
										((larryLastExEvent.getRotEnabled() == null && defaultApProerties.getRotEnabled() != null)
											|| (larryLastExEvent.getTixcityEnabled() == null && defaultApProerties.getTixcityEnabled() != null)
											|| (larryLastExEvent.getRtwEnabled() == null && defaultApProerties.getRtwEnabled() != null)
										)) {
									
									
									larryLastExEvent.setRotEnabled(defaultApProerties.getRotEnabled());
									larryLastExEvent.setTixcityEnabled(defaultApProerties.getTixcityEnabled());
									larryLastExEvent.setRtwEnabled(defaultApProerties.getRtwEnabled());
									String exVenueKey=null;
									Boolean two=true,three=true,four=true;
									
									   if(defaultApProerties.getRotEnabled().equals(true)){
										    exVenueKey=LARRYLAST_PRODUCT_ID+"_"+10+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && larryLastExEvent.getRotEnabled() != null && larryLastExEvent.getRotEnabled()){
												   larryLastExEvent.setRotEnabled(false);
												   two=false;
											   }
										   }
									   if(defaultApProerties.getTixcityEnabled().equals(true)){
										    exVenueKey=LARRYLAST_PRODUCT_ID+"_"+TIXCITY_BROKER_ID+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && larryLastExEvent.getTixcityEnabled() != null && larryLastExEvent.getTixcityEnabled()){
												   larryLastExEvent.setTixcityEnabled(false);
												   four=false;
											   }
										   }
									   if(defaultApProerties.getRtwEnabled().equals(true)){
										    exVenueKey=LARRYLAST_PRODUCT_ID+"_"+5+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && larryLastExEvent.getRtwEnabled() != null && larryLastExEvent.getRtwEnabled()){
												   larryLastExEvent.setRtwEnabled(false);
												   three=false;
											   }
										   }
									   if(two || three || four){
										   LarryLastExchangeEventAudit larryLastExEventAudit = new LarryLastExchangeEventAudit(larryLastExEvent);
										   larryLastExEventAudits.add(larryLastExEventAudit);
										   larryLastExEvent.setLastUpdatedBy(username);
										   larryLastExEvent.setLastUpdatedDate(now);
										   larryLastExEvent.setZone(isZoneEvent);
										   larryLastExEvents.add(larryLastExEvent);
									 }
								} 
								
								/*else if(isZoneEvent && larryLastExEvent.getStatus().equals("ACTIVE") && larryLastExEvent.getTicketNetworkBrokerId() == null && larryLastExEvent.getLastUpdatedBy().equals(username) 
											&& defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									larryLastExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									larryLastExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									larryLastExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									larryLastExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									larryLastExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
	
									String exVenueKey=null;
									Boolean one=true,two=true,three=true;
								    exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && larryLastExEvent.getTicketNetworkBrokerId() != null){
										   larryLastExEvent.setTicketNetworkBrokerId(null);
										   one=false;
									   }
									exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && larryLastExEvent.getVividBrokerId() != null){
										   larryLastExEvent.setVividBrokerId(null);
										   two=false;
										    }
									exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && larryLastExEvent.getScoreBigBrokerId() != null){
										   larryLastExEvent.setScoreBigBrokerId(null);
										   three=false;
										 }
								   exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && larryLastExEvent.getFanxchangeBrokerId() != null){
									   larryLastExEvent.setFanxchangeBrokerId(null);
									   three=false;
								   }
								   exVenueKey=LARRYLAST_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && larryLastExEvent.getTicketcityBrokerId() != null){
									   larryLastExEvent.setTicketcityBrokerId(null);
									   three=false;
								   }
									   if(one || two || three ){
										   LarryLastExchangeEventAudit larryLastExEventAudit = new LarryLastExchangeEventAudit(larryLastExEvent);
										   larryLastExEventAudits.add(larryLastExEventAudit);
										   larryLastExEvent.setLastUpdatedBy(username);
										   larryLastExEvent.setLastUpdatedDate(now);
										   larryLastExEvent.setZone(isZoneEvent);
										   larryLastExEvents.add(larryLastExEvent);
									   }
									
									
								}*/ else if(larryLastExEvent.getZone() == null || !larryLastExEvent.getZone().equals(isZoneEvent)) {
									LarryLastExchangeEventAudit larryLastExEventAudit = new LarryLastExchangeEventAudit(larryLastExEvent);
									larryLastExEventAudits.add(larryLastExEventAudit);
									
									larryLastExEvent.setZone(isZoneEvent);
									larryLastExEvents.add(larryLastExEvent);
								}
							}
							} else {
								if(larryLastExEvent != null && larryLastExEvent.getStatus().equals("ACTIVE")) {
									LarryLastExchangeEventAudit larryLastExEventAudit = new LarryLastExchangeEventAudit(larryLastExEvent);
									larryLastExEventAudits.add(larryLastExEventAudit);
									
									larryLastExEvent.setStatus("DELETED");
									larryLastExEvent.setLastUpdatedBy(username);
									larryLastExEvent.setLastUpdatedDate(now);
									larryLastExEvent.setZone(isZoneEvent);
									larryLastExEvents.add(larryLastExEvent);
								}
							}
						}
						
						if(activeProductsMap.get(ZONE_LASTROW_MINICATS_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							zoneLastRowMiniExEvent = existingZoneLastRowMiniExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(zoneLastRowMiniExEvent == null) {
								zoneLastRowMiniExEvent = new ZoneLastRowMiniExchangeEvent();
								if(isEventExistinPOS && isZoneEvent) {
									/*zoneLastRowMiniExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									zoneLastRowMiniExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									zoneLastRowMiniExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									zoneLastRowMiniExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									zoneLastRowMiniExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
									String exVenueKey=null;
								    exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && zoneLastRowMiniExEvent.getTicketNetworkBrokerId() != null){
										   zoneLastRowMiniExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && zoneLastRowMiniExEvent.getVividBrokerId() != null){
										   zoneLastRowMiniExEvent.setVividBrokerId(null);
										    }
									exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && zoneLastRowMiniExEvent.getScoreBigBrokerId() != null){
										   zoneLastRowMiniExEvent.setScoreBigBrokerId(null);
										 }
								   exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && zoneLastRowMiniExEvent.getFanxchangeBrokerId() != null){
									   zoneLastRowMiniExEvent.setFanxchangeBrokerId(null);
								   } 
								   exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && zoneLastRowMiniExEvent.getTicketcityBrokerId() != null){
									   zoneLastRowMiniExEvent.setTicketcityBrokerId(null);
								   } */
									
									zoneLastRowMiniExEvent.setVividEnabled(defaultApProerties.getVividSeatEnabled());
									zoneLastRowMiniExEvent.setRotEnabled(defaultApProerties.getRotEnabled());
									zoneLastRowMiniExEvent.setTixcityEnabled(defaultApProerties.getTixcityEnabled());
									zoneLastRowMiniExEvent.setRtwEnabled(defaultApProerties.getRtwEnabled());
									
									  
									   String exVenueKey=null;
									   if(defaultApProerties.getRotEnabled().equals(true)){
										    exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+10+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && zoneLastRowMiniExEvent.getRotEnabled() != null && zoneLastRowMiniExEvent.getRotEnabled()){
												   zoneLastRowMiniExEvent.setRotEnabled(false);
											   }
										   }
									   if(defaultApProerties.getTixcityEnabled().equals(true)){
										    exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+TIXCITY_BROKER_ID+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && zoneLastRowMiniExEvent.getTixcityEnabled() != null && zoneLastRowMiniExEvent.getTixcityEnabled()){
												   zoneLastRowMiniExEvent.setTixcityEnabled(false);
											   }
										   }
									   if(defaultApProerties.getRtwEnabled().equals(true)){
										    exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+5+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && zoneLastRowMiniExEvent.getRtwEnabled() != null && zoneLastRowMiniExEvent.getRtwEnabled()){
												   zoneLastRowMiniExEvent.setRtwEnabled(false);
											   }
										   }
									
									zoneLastRowMiniExEvent.setEventId(eventDTO.getId());
									zoneLastRowMiniExEvent.setAllowSectionRange(false);
									zoneLastRowMiniExEvent.setExposure(defaultApProerties.getExposure());
									//zoneLastRowMiniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									zoneLastRowMiniExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
									//zoneLastRowMiniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									zoneLastRowMiniExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									zoneLastRowMiniExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
									zoneLastRowMiniExEvent.setRptFactor(defaultApProerties.getRptFactor());
									//zoneLastRowMiniExEvent.setShippingDays(defaultApProerties.getShippingDays());
									zoneLastRowMiniExEvent.setStatus("ACTIVE");
									zoneLastRowMiniExEvent.setLastUpdatedBy(username);
									zoneLastRowMiniExEvent.setLastUpdatedDate(now);
									zoneLastRowMiniExEvent.setZone(isZoneEvent);
									zoneLastRowMiniExEvent.setDiscountZone(false);
									
									//Tamil : get shipping method and near term display option values from venue setings if not ther get it from default settings
									AutopricingVenueSettings apVenueSettings = autopricingVenuesettingsMap.get(ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+eventDTO.getVenueId());
									if(apVenueSettings != null) {
										zoneLastRowMiniExEvent.setShippingMethod(apVenueSettings.getShippingMethod());
										zoneLastRowMiniExEvent.setNearTermDisplayOption(apVenueSettings.getNearTermDisplayOption());
										zoneLastRowMiniExEvent.setLowerMarkup(apVenueSettings.getMarkup());
										zoneLastRowMiniExEvent.setUpperMarkup(apVenueSettings.getMarkup());
										zoneLastRowMiniExEvent.setShippingDays(apVenueSettings.getShippingDays());
									} else {
										zoneLastRowMiniExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
										zoneLastRowMiniExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
										zoneLastRowMiniExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
										zoneLastRowMiniExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
										zoneLastRowMiniExEvent.setShippingDays(defaultApProerties.getShippingDays());
										
									}
									
									zoneLastRowMiniExEvents.add(zoneLastRowMiniExEvent);
								}
								
								
							} else if(isEventExistinPOS) {
								if(!isZoneEvent && zoneLastRowMiniExEvent.getStatus().equals("ACTIVE")) {
									ZoneLastRowMiniExchangeEventAudit zoneLastRowMiniExEventAudit = new ZoneLastRowMiniExchangeEventAudit(zoneLastRowMiniExEvent);
									zoneLastRowMiniExEventAudits.add(zoneLastRowMiniExEventAudit);
									
									zoneLastRowMiniExEvent.setStatus("DELETED");
									/*zoneLastRowMiniExEvent.setTicketNetworkBrokerId(null);
									zoneLastRowMiniExEvent.setVividBrokerId(null);
									zoneLastRowMiniExEvent.setScoreBigBrokerId(null);
									zoneLastRowMiniExEvent.setFanxchangeBrokerId(null);
									zoneLastRowMiniExEvent.setTicketcityBrokerId(null);*/
									zoneLastRowMiniExEvent.setLastUpdatedBy(username);
									zoneLastRowMiniExEvent.setLastUpdatedDate(now);
									zoneLastRowMiniExEvent.setZone(isZoneEvent);
									zoneLastRowMiniExEvents.add(zoneLastRowMiniExEvent);
									
								} else if(isZoneEvent && zoneLastRowMiniExEvent.getStatus().equals("ACTIVE") && zoneLastRowMiniExEvent.getLastUpdatedBy().equals(username) && 
										((zoneLastRowMiniExEvent.getRotEnabled() == null && defaultApProerties.getRotEnabled() != null)
												|| (zoneLastRowMiniExEvent.getTixcityEnabled() == null && defaultApProerties.getTixcityEnabled() != null)
												|| (zoneLastRowMiniExEvent.getRtwEnabled() == null && defaultApProerties.getRtwEnabled() != null)
										)) {
									
									zoneLastRowMiniExEvent.setRotEnabled(defaultApProerties.getRotEnabled());
									zoneLastRowMiniExEvent.setTixcityEnabled(defaultApProerties.getTixcityEnabled());
									zoneLastRowMiniExEvent.setRtwEnabled(defaultApProerties.getRtwEnabled());
									String exVenueKey=null;
									Boolean two=true,three=true,four=true;
									
									   if(defaultApProerties.getRotEnabled().equals(true)){
										    exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+10+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && zoneLastRowMiniExEvent.getRotEnabled() != null && zoneLastRowMiniExEvent.getRotEnabled()){
												   zoneLastRowMiniExEvent.setRotEnabled(false);
												   two=false;
											   }
										   }
									   if(defaultApProerties.getTixcityEnabled().equals(true)){
										    exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+TIXCITY_BROKER_ID+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && zoneLastRowMiniExEvent.getTixcityEnabled() != null && zoneLastRowMiniExEvent.getTixcityEnabled()){
												   zoneLastRowMiniExEvent.setTixcityEnabled(false);
												   four=false;
											   }
										   }
									   if(defaultApProerties.getRtwEnabled().equals(true)){
										    exVenueKey=ZONE_LASTROW_MINICATS_PRODUCT_ID+"_"+5+"_"+eventDTO.getVenueId();
										    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
											   if(excludeVenues!=null && zoneLastRowMiniExEvent.getRtwEnabled() != null && zoneLastRowMiniExEvent.getRtwEnabled()){
												   zoneLastRowMiniExEvent.setRtwEnabled(false);
												   three=false;
											   }
										   }
									   if(two || three || four){
										   ZoneLastRowMiniExchangeEventAudit larryLastExEventAudit = new ZoneLastRowMiniExchangeEventAudit(zoneLastRowMiniExEvent);
										   zoneLastRowMiniExEventAudits.add(larryLastExEventAudit);
										   zoneLastRowMiniExEvent.setLastUpdatedBy(username);
										   zoneLastRowMiniExEvent.setLastUpdatedDate(now);
										   zoneLastRowMiniExEvent.setZone(isZoneEvent);
										   zoneLastRowMiniExEvents.add(zoneLastRowMiniExEvent);
									 }
									
									
								} else if(zoneLastRowMiniExEvent.getZone() == null || !zoneLastRowMiniExEvent.getZone().equals(isZoneEvent)) {
									ZoneLastRowMiniExchangeEventAudit zoneLastRowMiniExEventAudit = new ZoneLastRowMiniExchangeEventAudit(zoneLastRowMiniExEvent);
									zoneLastRowMiniExEventAudits.add(zoneLastRowMiniExEventAudit);
									
									zoneLastRowMiniExEvent.setZone(isZoneEvent);
									zoneLastRowMiniExEvents.add(zoneLastRowMiniExEvent);
								}
							}
							} else {
								if(zoneLastRowMiniExEvent != null && zoneLastRowMiniExEvent.getStatus().equals("ACTIVE")) {
									ZoneLastRowMiniExchangeEventAudit zoneLastRowMiniExEventAudit = new ZoneLastRowMiniExchangeEventAudit(zoneLastRowMiniExEvent);
									zoneLastRowMiniExEventAudits.add(zoneLastRowMiniExEventAudit);
									
									zoneLastRowMiniExEvent.setStatus("DELETED");
									zoneLastRowMiniExEvent.setLastUpdatedBy(username);
									zoneLastRowMiniExEvent.setLastUpdatedDate(now);
									zoneLastRowMiniExEvent.setZone(isZoneEvent);
									zoneLastRowMiniExEvents.add(zoneLastRowMiniExEvent);
								}
							}
						}
						
						if(activeProductsMap.get(TN_SPECIAL_PRODUCT_ID) != null) {
							//Tamil : Add all settings based on events POS parent category
							key = TN_SPECIAL_PRODUCT_ID+"_"+posBasedParentCategoryId;
							defaultApProerties = defautlApPropertiesMap.get(key);
							tnSpecialExEvent = existingTNSpecialExEventMap.remove(eventDTO.getId());
							if(isEventEligigbleForTN) {
							if(tnSpecialExEvent == null) {
								if(isEventExistinPOS) {
									tnSpecialExEvent = new TicketNetworkSSAccountExchangeEvent();
									tnSpecialExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									tnSpecialExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									tnSpecialExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									tnSpecialExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									tnSpecialExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
	
									String exVenueKey=null;
								    exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && tnSpecialExEvent.getTicketNetworkBrokerId() != null){
										   tnSpecialExEvent.setTicketNetworkBrokerId(null);
									   }
									exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && tnSpecialExEvent.getVividBrokerId() != null){
										   tnSpecialExEvent.setVividBrokerId(null);
										    }
									exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && tnSpecialExEvent.getScoreBigBrokerId() != null){
										   tnSpecialExEvent.setScoreBigBrokerId(null);
										 }
								   exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									if(excludeVenues!=null && tnSpecialExEvent.getFanxchangeBrokerId() != null){
									   tnSpecialExEvent.setFanxchangeBrokerId(null);
									} 
									exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
									   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
										if(excludeVenues!=null && tnSpecialExEvent.getTicketcityBrokerId() != null){
										   tnSpecialExEvent.setTicketcityBrokerId(null);
										} 
									
									
									tnSpecialExEvent.setEventId(eventDTO.getId());
									tnSpecialExEvent.setAllowSectionRange(false);
									tnSpecialExEvent.setExposure(defaultApProerties.getExposure());
									tnSpecialExEvent.setLowerMarkup(defaultApProerties.getLowerMarkup());
									tnSpecialExEvent.setLowerShippingFees(defaultApProerties.getLowerShippingFees());
									tnSpecialExEvent.setUpperMarkup(defaultApProerties.getUpperMarkup());
									tnSpecialExEvent.setUpperShippingFees(defaultApProerties.getUpperShippingFees());
									tnSpecialExEvent.setNearTermDisplayOption(defaultApProerties.getNearTermDisplayOption());
									tnSpecialExEvent.setPriceBreakup(defaultApProerties.getPriceBreakup());
									tnSpecialExEvent.setRptFactor(defaultApProerties.getRptFactor());
									tnSpecialExEvent.setShippingDays(defaultApProerties.getShippingDays());
									tnSpecialExEvent.setShippingMethod(defaultApProerties.getShippingMethod());
									tnSpecialExEvent.setStatus("ACTIVE");
									tnSpecialExEvent.setLastUpdatedBy(username);
									tnSpecialExEvent.setLastUpdatedDate(now);
									tnSpecialExEvent.setZone(isZoneEvent);
									tnSpecialExEvent.setDiscountZone(false);
									
									tnSpecialExEvents.add(tnSpecialExEvent);
								}
								
								
							} else if(isEventExistinPOS) {
								// Event exist in TMAT as well as in POS..
								if(tnSpecialExEvent.getTicketNetworkBrokerId() == null && tnSpecialExEvent.getLastUpdatedBy().equals(username) 
											&& defaultApProerties.getTicketNetworkBrokerId() != null) {
									
									
									tnSpecialExEvent.setTicketNetworkBrokerId(defaultApProerties.getTicketNetworkBrokerId());
									tnSpecialExEvent.setVividBrokerId(defaultApProerties.getVividBrokerId());
									tnSpecialExEvent.setScoreBigBrokerId(defaultApProerties.getScoreBigBrokerId());
									tnSpecialExEvent.setFanxchangeBrokerId(defaultApProerties.getFanxchangeBrokerId());
									tnSpecialExEvent.setTicketcityBrokerId(defaultApProerties.getTicketcityBrokerId());
									
	
									String exVenueKey=null;
									Boolean one=true,two=true,three=true;
								    exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getTicketNetworkBrokerId()+"_"+eventDTO.getVenueId();
								    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
									   if(excludeVenues!=null && tnSpecialExEvent.getTicketNetworkBrokerId() != null){
										   tnSpecialExEvent.setTicketNetworkBrokerId(null);
										   one=false;
									   }
									exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
								    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
									   if(excludeVenues!=null && tnSpecialExEvent.getVividBrokerId() != null){
										   tnSpecialExEvent.setVividBrokerId(null);
										   two=false;
										    }
									exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
									excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
									   if(excludeVenues!=null && tnSpecialExEvent.getScoreBigBrokerId() != null){
										   tnSpecialExEvent.setScoreBigBrokerId(null);
										   three=false;
										 }  
								   exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && tnSpecialExEvent.getFanxchangeBrokerId() != null){
									   tnSpecialExEvent.setFanxchangeBrokerId(null);
									   three=false;
								   }
								   exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								   excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								   if(excludeVenues!=null && tnSpecialExEvent.getTicketcityBrokerId() != null){
									   tnSpecialExEvent.setTicketcityBrokerId(null);
									   three=false;
								   }
									   if(one || two || three ){
										   TicketNetworkSSAccountExchangeEventAudit tnSpecialExEventAudit = new TicketNetworkSSAccountExchangeEventAudit(tnSpecialExEvent);
										   tnSpecialExEventAudits.add(tnSpecialExEventAudit);
										   tnSpecialExEvent.setLastUpdatedBy(username);
										   tnSpecialExEvent.setLastUpdatedDate(now);
										   tnSpecialExEvent.setZone(isZoneEvent);
										   tnSpecialExEvents.add(tnSpecialExEvent);
									   }
								} 
							}else{ // If event not exist in TMAT but not in POS make tn broker id null.
								tnSpecialExEvent.setTicketNetworkBrokerId(null);
								String exVenueKey=null;
							    ExcludeVenues excludeVenues=excludeEventVenueMap.get(exVenueKey);
								exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getVividBrokerId()+"_"+eventDTO.getVenueId();   
							    excludeVenues=excludeEventVenueMap.get(exVenueKey);	 							    
							   if(excludeVenues!=null && tnSpecialExEvent.getVividBrokerId() != null){
								   tnSpecialExEvent.setVividBrokerId(null);
							    }
								exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getScoreBigBrokerId()+"_"+eventDTO.getVenueId();   
								excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								if(excludeVenues!=null && tnSpecialExEvent.getScoreBigBrokerId() != null){
									tnSpecialExEvent.setScoreBigBrokerId(null);
								}
								exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getFanxchangeBrokerId()+"_"+eventDTO.getVenueId();   
								excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								if(excludeVenues!=null && tnSpecialExEvent.getFanxchangeBrokerId() != null){
									tnSpecialExEvent.setFanxchangeBrokerId(null);
								}
								
								exVenueKey=TN_SPECIAL_PRODUCT_ID+"_"+defaultApProerties.getTicketcityBrokerId()+"_"+eventDTO.getVenueId();   
								excludeVenues=excludeEventVenueMap.get(exVenueKey);	 	   
								if(excludeVenues!=null && tnSpecialExEvent.getTicketcityBrokerId() != null){
									tnSpecialExEvent.setTicketcityBrokerId(null);
								}
								
								tnSpecialExEvents.add(tnSpecialExEvent);
								
							}
							} else {
								if(tnSpecialExEvent != null && tnSpecialExEvent.getStatus().equals("ACTIVE")) {
									TicketNetworkSSAccountExchangeEventAudit tnSpecialExEventAudit = new TicketNetworkSSAccountExchangeEventAudit(tnSpecialExEvent);
									   tnSpecialExEventAudits.add(tnSpecialExEventAudit);
									
									tnSpecialExEvent.setStatus("DELETED");
									tnSpecialExEvent.setLastUpdatedBy(username);
									tnSpecialExEvent.setLastUpdatedDate(now);
									tnSpecialExEvent.setZone(isZoneEvent);
									tnSpecialExEvents.add(tnSpecialExEvent);
								}
							}
						}
					}
					
					DAORegistry.getMiniExchangeEventDAO().saveOrUpdateAll(miniExEvents);
					DAORegistry.getMiniExchangeEventAuditDAO().saveAll(miniExEventAudits);
					
					DAORegistry.getSgLastFiveRowExchangeEventDAO().saveOrUpdateAll(sgLastFiveRowExEvents);
					DAORegistry.getSgLastFiveRowExchangeEventAuditDAO().saveAll(sgLastFiveRowExEventAudits);
					
					DAORegistry.getVipMiniExchangeEventDAO().saveOrUpdateAll(vipMiniExEvents);
					DAORegistry.getVipMiniExchangeEventAuditDAO().saveAll(vipMiniExEventAudits);
					
					DAORegistry.getLastRowMiniExchangeEventDAO().saveOrUpdateAll(lastRowMiniExEvents);
					DAORegistry.getLastRowMiniExchangeEventAuditDAO().saveAll(lastRowMiniExEventAudits);
					
					DAORegistry.getZonePricingExchangeEventDAO().saveOrUpdateAll(zonePricingExEvents);
					DAORegistry.getZonePricingExchangeEventAuditDAO().saveAll(zonePricingExEventAudits);
					
					DAORegistry.getTixCityZonePricingExchangeEventDAO().saveOrUpdateAll(tixCityZonePricingExEvents);
					DAORegistry.getTixCityZonePricingExchangeEventAuditDAO().saveAll(tixCityZonePricingExEventAudits);
					
					DAORegistry.getManhattanZonePricingExchangeEventDAO().saveOrUpdateAll(manhattanZonePricingExEvents);
					DAORegistry.getManhattanZonePricingExchangeEventAuditDAO().saveAll(manhattanZonePricingExEventAudits);
					
					DAORegistry.getLarryLastExchangeEventDAO().saveOrUpdateAll(larryLastExEvents);
					DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(larryLastExEventAudits);
					
					DAORegistry.getZoneLastRowMiniExchangeEventDAO().saveOrUpdateAll(zoneLastRowMiniExEvents);
					DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().saveAll(zoneLastRowMiniExEventAudits);
					
					DAORegistry.getZoneTicketsProcessorExchangeEventDAO().saveOrUpdateAll(zoneTicketsProcessorExEvents);
					DAORegistry.getZoneTicketsProcessorExchangeEventAuditDAO().saveAll(zoneTicketsProcessorExEventAudits);
					
					DAORegistry.getPresaleZoneTicketsExchangeEventDAO().saveOrUpdateAll(presaleZoneTicketsExEvents);
					DAORegistry.getPresaleZoneTicketsExchangeEventAuditDAO().saveAll(presaleZoneTicketsExEventAudits);
					
					DAORegistry.getPresaleAutoCatExchangeEventDAO().saveOrUpdateAll(presaleAutoCatExEvents);
					DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().saveAll(presaleAutoCatExEventAudits);
					
					DAORegistry.getAutoCats96ExchangeEventDAO().saveOrUpdateAll(autoCats96ExEvents);
					DAORegistry.getAutoCats96ExchangeEventAuditDAO().saveAll(autoCats96ExEventAudits);
					
					DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().saveOrUpdateAll(tnSpecialExEvents);
					DAORegistry.getTicketNetworkSSAccountExchangeEventAuditDAO().saveAll(tnSpecialExEventAudits);
					
					DAORegistry.getVipLastRowMiniExchangeEventDAO().saveOrUpdateAll(vipLastRowMiniExEvents);
					DAORegistry.getVipLastRowMiniExchangeEventAuditDAO().saveAll(vipLastRowMiniExEventAudits);
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Error in AutoExchange Event Loader process.");
				throw e;
			}
	}
	
	
	public static void updateDeletedTMATEventsExchangeEventStatus() throws Exception {
	
		try {
			String username = "AUTO";//SecurityContextHolder.getContext().getAuthentication().getName();
			Date now = new Date();
			
			List<PresaleAutoCatExchangeEventAudit> presaleAutoCatExEventAudits = new ArrayList<PresaleAutoCatExchangeEventAudit>();
			List<MiniExchangeEventAudit> miniExEventAudits = new ArrayList<MiniExchangeEventAudit>();
			List<SGLastFiveRowExchangeEventAudit> sgLastFiveRowExEventAudits = new ArrayList<SGLastFiveRowExchangeEventAudit>();
			List<VipMiniExchangeEventAudit> vipMiniExEventAudits = new ArrayList<VipMiniExchangeEventAudit>();
			List<LastRowMiniExchangeEventAudit> lastRowMiniExEventAudits = new ArrayList<LastRowMiniExchangeEventAudit>();
			List<LarryLastExchangeEventAudit> larryLastExEventAudits = new ArrayList<LarryLastExchangeEventAudit>();
			List<ZoneLastRowMiniExchangeEventAudit> zoneLastRowMiniExEventAudits = new ArrayList<ZoneLastRowMiniExchangeEventAudit>();
			List<ZonePricingExchangeEventAudit> zonePricingExEventAudits = new ArrayList<ZonePricingExchangeEventAudit>();
			List<ZoneTicketsProcessorExchangeEventAudit> zoneTicketsProcessorExEventAudits = new ArrayList<ZoneTicketsProcessorExchangeEventAudit>();
			List<PresaleZoneTicketsExchangeEventAudit> presaleZoneTicketsExchangeEventAudits = new ArrayList<PresaleZoneTicketsExchangeEventAudit>();
			List<TixCityZonePricingExchangeEventAudit> tixCityZonePricingExEventAudits = new ArrayList<TixCityZonePricingExchangeEventAudit>();
			List<AutoCats96ExchangeEventAudit> autoCats96ExEventAudits = new ArrayList<AutoCats96ExchangeEventAudit>();
			List<TicketNetworkSSAccountExchangeEventAudit> tnSpecialExEventAudits = new ArrayList<TicketNetworkSSAccountExchangeEventAudit>();
			
			List<MiniExchangeEvent> miniExEvents = DAORegistry.getMiniExchangeEventDAO().getAllMiniExchangeEventsofDeletedTmatEvents();
			for (MiniExchangeEvent miniExEvent : miniExEvents) {
				MiniExchangeEventAudit miniExEventAudit = new MiniExchangeEventAudit(miniExEvent);
				miniExEventAudits.add(miniExEventAudit);
				
				miniExEvent.setStatus("DELETED");
				miniExEvent.setLastUpdatedBy(username);
				miniExEvent.setLastUpdatedDate(now);
			}
			
			List<SGLastFiveRowExchangeEvent> sgLastFiveRowExEvents = DAORegistry.getSgLastFiveRowExchangeEventDAO().getAllSGLastFiveRowExchangeEventsofDeletedTmatEvents();
			for (SGLastFiveRowExchangeEvent sgLastFiveRowExEvent : sgLastFiveRowExEvents) {
				SGLastFiveRowExchangeEventAudit sgLastFiveRowExEventAudit = new SGLastFiveRowExchangeEventAudit(sgLastFiveRowExEvent);
				sgLastFiveRowExEventAudits.add(sgLastFiveRowExEventAudit);
				
				sgLastFiveRowExEvent.setStatus("DELETED");
				sgLastFiveRowExEvent.setLastUpdatedBy(username);
				sgLastFiveRowExEvent.setLastUpdatedDate(now);
			}
			
			List<PresaleAutoCatExchangeEvent> presaleAutoCatExEvents = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsofDeletedTmatEvents();
			for (PresaleAutoCatExchangeEvent exEvent : presaleAutoCatExEvents) {
				PresaleAutoCatExchangeEventAudit miniExEventAudit = new PresaleAutoCatExchangeEventAudit(exEvent);
				presaleAutoCatExEventAudits.add(miniExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			List<VipMiniExchangeEvent> vipMiniCatExEvents = DAORegistry.getVipMiniExchangeEventDAO().getAllVipMiniExchangeEventsofDeletedTmatEvents();
			for (VipMiniExchangeEvent exEvent : vipMiniCatExEvents) {
				VipMiniExchangeEventAudit miniExEventAudit = new VipMiniExchangeEventAudit(exEvent);
				vipMiniExEventAudits.add(miniExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			List<LastRowMiniExchangeEvent> lastrowMiniExEvents = DAORegistry.getLastRowMiniExchangeEventDAO().getAllLastRowMiniExchangeEventsofDeletedTmatEvents();
			for (LastRowMiniExchangeEvent exEvent : lastrowMiniExEvents) {
				LastRowMiniExchangeEventAudit lastrowMiniExEventAudit = new LastRowMiniExchangeEventAudit(exEvent);
				lastRowMiniExEventAudits.add(lastrowMiniExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			
			List<LarryLastExchangeEvent> larryLastExEvents = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsofDeletedTmatEvents();
			for (LarryLastExchangeEvent exEvent : larryLastExEvents) {
				LarryLastExchangeEventAudit larryLastExEventAudit = new LarryLastExchangeEventAudit(exEvent);
				larryLastExEventAudits.add(larryLastExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			
			List<ZoneLastRowMiniExchangeEvent> zoneLastrowExEvents = DAORegistry.getZoneLastRowMiniExchangeEventDAO().getAllZoneLastRowMiniExchangeEventsofDeletedTmatEvents();
			for (ZoneLastRowMiniExchangeEvent exEvent : zoneLastrowExEvents) {
				ZoneLastRowMiniExchangeEventAudit zoneLastrowExEventAudit = new ZoneLastRowMiniExchangeEventAudit(exEvent);
				zoneLastRowMiniExEventAudits.add(zoneLastrowExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			
			List<ZonePricingExchangeEvent> zonePricingExEvents = DAORegistry.getZonePricingExchangeEventDAO().getAllZonePricingExchangeEventsofDeletedTmatEvents();
			for (ZonePricingExchangeEvent exEvent : zonePricingExEvents) {
				ZonePricingExchangeEventAudit zonePricingExEventAudit = new ZonePricingExchangeEventAudit(exEvent);
				zonePricingExEventAudits.add(zonePricingExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}

			List<TixCityZonePricingExchangeEvent> tixCityZonePricingExEvents = DAORegistry.getTixCityZonePricingExchangeEventDAO().getAllTixCityZonePricingExchangeEventsofDeletedTmatEvents();
			for (TixCityZonePricingExchangeEvent exEvent : tixCityZonePricingExEvents) {
				TixCityZonePricingExchangeEventAudit tixCityZonePricingExEventAudit = new TixCityZonePricingExchangeEventAudit(exEvent);
				tixCityZonePricingExEventAudits.add(tixCityZonePricingExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			
			List<ZoneTicketsProcessorExchangeEvent> zoneTicketsProcessorExEvents = DAORegistry.getZoneTicketsProcessorExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsofDeletedTmatEvents();
			for (ZoneTicketsProcessorExchangeEvent exEvent : zoneTicketsProcessorExEvents) {
				ZoneTicketsProcessorExchangeEventAudit zoneTicketsProcessorExEventAudit = new ZoneTicketsProcessorExchangeEventAudit(exEvent);
				zoneTicketsProcessorExEventAudits.add(zoneTicketsProcessorExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			
			List<PresaleZoneTicketsExchangeEvent> presaleZoneTicketsExchangeEvents = DAORegistry.getPresaleZoneTicketsExchangeEventDAO().getAllZoneTicketsProcessorExchangeEventsofDeletedTmatEvents();
			for (PresaleZoneTicketsExchangeEvent exEvent : presaleZoneTicketsExchangeEvents) {
				PresaleZoneTicketsExchangeEventAudit zoneTicketsProcessorExEventAudit = new PresaleZoneTicketsExchangeEventAudit(exEvent);
				presaleZoneTicketsExchangeEventAudits.add(zoneTicketsProcessorExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			
			List<AutoCats96ExchangeEvent> autoCats96ExEvents = DAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsofDeletedTmatEvents();
			for (AutoCats96ExchangeEvent exEvent : autoCats96ExEvents) {
				AutoCats96ExchangeEventAudit autoCats96ExEventAudit = new AutoCats96ExchangeEventAudit(exEvent);
				autoCats96ExEventAudits.add(autoCats96ExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			
			List<TicketNetworkSSAccountExchangeEvent> tnSpecialExEvents = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAllTNSpecialExchangeEventsofDeletedTmatEvents();
			for (TicketNetworkSSAccountExchangeEvent exEvent : tnSpecialExEvents) {
				TicketNetworkSSAccountExchangeEventAudit tnSpecialExEventAudit = new TicketNetworkSSAccountExchangeEventAudit(exEvent);
				tnSpecialExEventAudits.add(tnSpecialExEventAudit);
				
				exEvent.setStatus("DELETED");
				exEvent.setLastUpdatedBy(username);
				exEvent.setLastUpdatedDate(now);
			}
			
			DAORegistry.getMiniExchangeEventDAO().saveOrUpdateAll(miniExEvents);
			DAORegistry.getMiniExchangeEventAuditDAO().saveAll(miniExEventAudits);
			
			DAORegistry.getSgLastFiveRowExchangeEventDAO().saveOrUpdateAll(sgLastFiveRowExEvents);
			DAORegistry.getSgLastFiveRowExchangeEventAuditDAO().saveAll(sgLastFiveRowExEventAudits);
			
			DAORegistry.getVipMiniExchangeEventDAO().saveOrUpdateAll(vipMiniCatExEvents);
			DAORegistry.getVipMiniExchangeEventAuditDAO().saveAll(vipMiniExEventAudits);
			
			DAORegistry.getLastRowMiniExchangeEventDAO().saveOrUpdateAll(lastrowMiniExEvents);
			DAORegistry.getLastRowMiniExchangeEventAuditDAO().saveAll(lastRowMiniExEventAudits);
			
			DAORegistry.getZonePricingExchangeEventDAO().saveOrUpdateAll(zonePricingExEvents);
			DAORegistry.getZonePricingExchangeEventAuditDAO().saveAll(zonePricingExEventAudits);
			
			DAORegistry.getTixCityZonePricingExchangeEventDAO().saveOrUpdateAll(tixCityZonePricingExEvents);
			DAORegistry.getTixCityZonePricingExchangeEventAuditDAO().saveAll(tixCityZonePricingExEventAudits);
			
			DAORegistry.getLarryLastExchangeEventDAO().saveOrUpdateAll(larryLastExEvents);
			DAORegistry.getLarryLastExchangeEventAuditDAO().saveAll(larryLastExEventAudits);
			
			DAORegistry.getZoneLastRowMiniExchangeEventDAO().saveOrUpdateAll(zoneLastrowExEvents);
			DAORegistry.getZoneLastRowMiniExchangeEventAuditDAO().saveAll(zoneLastRowMiniExEventAudits);
			
			DAORegistry.getZoneTicketsProcessorExchangeEventDAO().saveOrUpdateAll(zoneTicketsProcessorExEvents);
			DAORegistry.getZoneTicketsProcessorExchangeEventAuditDAO().saveAll(zoneTicketsProcessorExEventAudits);
			
			DAORegistry.getPresaleZoneTicketsExchangeEventDAO().saveOrUpdateAll(presaleZoneTicketsExchangeEvents);
			DAORegistry.getPresaleZoneTicketsExchangeEventAuditDAO().saveAll(presaleZoneTicketsExchangeEventAudits);
			
			DAORegistry.getPresaleAutoCatExchangeEventDAO().saveOrUpdateAll(presaleAutoCatExEvents);
			DAORegistry.getPresaleAutoCatExchangeEventAuditDAO().saveAll(presaleAutoCatExEventAudits);
			
			DAORegistry.getAutoCats96ExchangeEventDAO().saveOrUpdateAll(autoCats96ExEvents);
			DAORegistry.getAutoCats96ExchangeEventAuditDAO().saveAll(autoCats96ExEventAudits);
			
			DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().saveOrUpdateAll(tnSpecialExEvents);
			DAORegistry.getTicketNetworkSSAccountExchangeEventAuditDAO().saveAll(tnSpecialExEventAudits);
			
			System.out.println("AutoExchange Event Loader Job Completed Successfully.."+new Date());
						
		} catch(Exception e){
			e.printStackTrace();
			System.out.println("Error in AutoExchange Event Loader -updateDeletedTMATEventsExchangeEventStatus process.");
			throw e;
		}
	}
public static void updatePresaleSetingsEventsCrawlFrequencyinTMAT() throws Exception {
		
		//System.out.println("running...in ExchangeEventLoader");
			
		//update crawl frequency to once in a ay for presale setting events 
			try{
				List<TicketListingCrawl> crawlsList = DAORegistry.getQueryManagerDAO().getAllTicketListingCrawlForPresaleSetingsCrawlFrequencyUpdate();
				if(crawlsList != null && !crawlsList.isEmpty()){
					int count = 0;
					int processed = 0;
					String crawlIdStr="";
					for (TicketListingCrawl crawl : crawlsList) {
						crawlIdStr = crawlIdStr +crawl.getId()+",";
						count++;
						processed++;
						
						if(count == 100) {
							crawlIdStr=crawlIdStr.substring(0,crawlIdStr.length()-1);
							int upCount = DAORegistry.getQueryManagerDAO().updateTicketListingCrawlPresaleSetingsCrawlFrequency(crawlIdStr);
							forceUpdateCrawlsInTMAT(crawlIdStr);
							System.out.println("CRALW UPCONT : "+upCount+" : PROC : "+processed+" : "+ new Date());
							crawlIdStr ="";
							count = 0;
						}
					}
					if(!crawlIdStr.equals("")) {
						crawlIdStr=crawlIdStr.substring(0,crawlIdStr.length()-1);
						int upCount = DAORegistry.getQueryManagerDAO().updateTicketListingCrawlPresaleSetingsCrawlFrequency(crawlIdStr);
						forceUpdateCrawlsInTMAT(crawlIdStr);
						System.out.println("CRALW UPCONT 1 : "+upCount+" : PROC : "+processed+" : "+ new Date());
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Error in Presale Setings Events Crawl Frequency Update.");
				throw e;
			}
			
			//update crawl frequency to normal mode for non presale setings events
			try{
				List<TicketListingCrawl> crawlsList = DAORegistry.getQueryManagerDAO().getAllTicketListingCrawlOfNonPresaleSetingsEvenstForUpdate();
				if(crawlsList != null && !crawlsList.isEmpty()){
					int count = 0;
					int processed = 0;
					String crawlIdStr="";
					for (TicketListingCrawl crawl : crawlsList) {
						crawlIdStr = crawlIdStr +crawl.getId()+",";
						count++;
						processed++;
						
						if(count == 100) {
							crawlIdStr=crawlIdStr.substring(0,crawlIdStr.length()-1);
							int upCount = DAORegistry.getQueryManagerDAO().updateTicketListingCrawlFrequencytoNormalForNonPresaleEvents(crawlIdStr);
							forceUpdateCrawlsInTMAT(crawlIdStr);
							System.out.println("CRALW UPCONT REVERT : "+upCount+" : PROC : "+processed+" : "+ new Date());
							crawlIdStr ="";
							count = 0;
						}
					}
					if(!crawlIdStr.equals("")) {
						crawlIdStr=crawlIdStr.substring(0,crawlIdStr.length()-1);
						int upCount = DAORegistry.getQueryManagerDAO().updateTicketListingCrawlFrequencytoNormalForNonPresaleEvents(crawlIdStr);
						forceUpdateCrawlsInTMAT(crawlIdStr);
						System.out.println("CRALW UPCONT REVERT 1 : "+upCount+" : PROC : "+processed+" : "+ new Date());
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Error in Presale Setings Events Crawl Frequency Update REVERT.");
				throw e;
			}
	}
	public static void forceUpdateCrawlsInTMAT(String crawlIdStr) throws Exception {
		
		try {
			Date now = new Date();
			String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers";//?action=updateCrawl&crawls=";
			HttpClient hc = new DefaultHttpClient();
			HttpPost hp = new HttpPost(url);
			NameValuePair nameValuePair = new BasicNameValuePair("action", "updateCrawl");
			//NameValuePair nameValuePair1 = new BasicNameValuePair("action", sharedProperty.getAutoPricingUrl()+"ForceCrawledEventResult");
			NameValuePair nameValuePair1 = new BasicNameValuePair("crawls", crawlIdStr);
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			parameters.add(nameValuePair);
			parameters.add(nameValuePair1);
			//parameters.add(nameValuePair2);
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
			hp.setEntity(entity);
			HttpResponse res = hc.execute(hp);
			String content = EntityUtils.toString(res.getEntity());
			System.out.println("CRAWL Content : "+content);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}