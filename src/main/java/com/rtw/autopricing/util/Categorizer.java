package com.rtw.autopricing.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.Category;
import com.rtw.autopricing.ftp.data.CategoryMapping;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.Ticket;


public class Categorizer {
	private static Pattern numberOrLetterBlock = Pattern.compile("([^\\d]+)|(\\d+)");
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(Categorizer.class);
//	private static final int REINIT_TIMER_INTERVAL = 30 * 60 * 1000;
	
	private static Map<Integer, Collection<CategoryMapping>> realTimeMappingMap = new HashMap<Integer, Collection<CategoryMapping>>();
	private static Map<Integer, Long> mappingTimeMap = new HashMap<Integer, Long>();
//	private static Map<String, Collection<CategoryMapping>> mappingMap = new HashMap<String, Collection<CategoryMapping>>(); 
	private static Map<Integer,Map<Long, Map<String, List<CategoryMapping>>>> mapTime = new HashMap<Integer, Map<Long,Map<String,List<CategoryMapping>>>>();
	private static Map<String, String> validSection = new HashMap<String, String>();
	
	
	public static Collection<CategoryMapping> getAllCategoryMappingsByVenueCategoryId(Integer venueCategoryId) throws Exception {
		Collection<CategoryMapping> list = null;
		if(mappingTimeMap.get(venueCategoryId)==null || (new Date().getTime()-mappingTimeMap.get(venueCategoryId)>=1000*60*5)){
			list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategoryId);//Records are in descending order.
			realTimeMappingMap.put(venueCategoryId, list);
			mappingTimeMap.put(venueCategoryId, new Date().getTime());
		}else{
			list = realTimeMappingMap.get(venueCategoryId);
		}
		return list;
	}
	
	public static Map<String, List<CategoryMapping>> getCategoryMappingByVenueCategoryId(Event event) throws Exception {
		
		Map<String, List<CategoryMapping>> finalMap = new HashMap<String, List<CategoryMapping>>();
		Map<String, List<CategoryMapping>> map = null;
		Map<Long, Map<String, List<CategoryMapping>>> catMap = mapTime.get(event.getVenueCategoryId());
		if(catMap!=null && !catMap.isEmpty()){
			Long time = catMap.keySet().iterator().next();
			if(new Date().getTime() - time <= 1000*60*5){
				map = catMap.get(time);
				//catMappingList = realTimeMappingMap.get(event.getVenueCategoryId());
			}
		}else{
			catMap = new HashMap<Long, Map<String,List<CategoryMapping>>>();
		}
		if(map==null){
//			Collection<CategoryMapping> list = null;
//			if(mappingTimeMap.get(event.getVenueCategoryId())==null || (new Date().getTime()-mappingTimeMap.get(event.getVenueCategoryId())>=1000*60*5)){
//				list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());//Records are in descending order.
//				realTimeMappingMap.put(event.getVenueCategoryId(), list);
//				mappingTimeMap.put(event.getVenueCategoryId(), new Date().getTime());
//			}else{
//				list = realTimeMappingMap.get(event.getVenueCategoryId());
//			}
			Collection<CategoryMapping> list = getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
			
			
			//catMappingList = list;
			map = new HashMap<String, List<CategoryMapping>>();
			for(CategoryMapping mapping:list){
				List<CategoryMapping> mappingList = map.get(mapping.getStartSection().replaceAll("\\s+", " ").trim().toLowerCase());
				if(mappingList==null){
					mappingList = new ArrayList<CategoryMapping>();
				}
				mappingList.add(mapping);
				map.put(mapping.getStartSection().replaceAll("\\s+", " ").trim().toLowerCase(), mappingList);
				
				/*if(mapping.getStartSection().equals(mapping.getEndSection())){
					continue;
				}*/
				if(mapping.getStartSection().equals(mapping.getEndSection())){
				     if(mapping.getStartSection().contains("-")){
				      mappingList = map.get("-");
				      if(mappingList==null){
				       mappingList = new ArrayList<CategoryMapping>();
				      }
				      mappingList.add(mapping);
				      map.put("-", mappingList);
				     }
				     continue;
				}
				
				boolean isInt=false;
				Integer intStartSection =0;
				String startSection ="";
				String endSection  ="";
				Integer intEndSection =0; 
				String prefix="";  
				// Consider only integer section range..
				try{
					startSection = mapping.getStartSection();
					endSection = mapping.getEndSection();
					intStartSection= Integer.parseInt(startSection);
					intEndSection= Integer.parseInt(endSection);
					isInt=true;
				}catch (Exception ex) {
					
				}
				
				if(isInt){
					for(int start=intStartSection;start<=intEndSection;start++){
						String sec = prefix + start;
						if(sec.equals(startSection)){
							continue;
						}
						mappingList = map.get(sec.toLowerCase());
						if(mappingList==null){
							mappingList = new ArrayList<CategoryMapping>();
						}
						mappingList.add(mapping);
						map.put(sec.toLowerCase(), mappingList);
					}
					
				}
			}
			catMap.clear();
			catMap.put(new Date().getTime(), map);
			mapTime.put(event.getVenueCategoryId(),catMap);
		}
		if(map != null) {
			finalMap = new HashMap<String, List<CategoryMapping>>(map);	
		}
		return finalMap;
	}
	
	public static Collection<Ticket> computeCategoryForTickets(Collection<Ticket> tickets,Map<Integer, Category> categoryMap,Event event) throws Exception {
//		return computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getNormalizedRow(), ticket.getNormalizedSeat(),groupName);
		Collection<Ticket> result = new ArrayList<Ticket>();
		
		if(tickets==null || tickets.isEmpty()){
			return tickets;
		}
		
		Map<String, List<CategoryMapping>> map = getCategoryMappingByVenueCategoryId(event);
		
		for(Ticket ticket:tickets){
			//List<CategoryMapping> mappingList = map.get(ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase());
			List<CategoryMapping> mappingList = null;
			if(ticket.getNormalizedSection().contains("-")){
				mappingList = map.get("-");
			}else{
				mappingList = map.get(ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase());
			}
			if(mappingList==null) {
				mappingList = map.get("*");
				if(mappingList!=null && !mappingList.isEmpty()){
					CategoryMapping mapping = mappingList.get((mappingList.size()-1));
					ticket.setCategory(categoryMap.get(mapping.getCategoryId()));
					ticket.setCategoryMapping(mapping);
					//ticket.setUncategorized(false);
					result.add(ticket);
				}
			} else if(mappingList!=null){
				for(CategoryMapping mapping:mappingList){
					Integer catId = computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getRow(),ticket.getSeat(),mapping);
					if(catId!=null){
						ticket.setCategory(categoryMap.get(catId));
						ticket.setCategoryMapping(mapping);
						result.add(ticket);
						break;
					}
				}	
			} 
			
		}
		return result;
	}
	
	public static Map<String,List<String>> getLarrySectionBasedLastRows(Event event) throws Exception {
		
		
		Map<String,List<String>> sectionMap = new HashMap<String, List<String>>();
		Map<String,List<CategoryMapping>> catmappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(catmappingMap.keySet());
		
		for (String key : keyList) {
			
			if(key.equals("-")) {
				continue;
			}
			
			List<CategoryMapping> catmappingList = catmappingMap.get(key);
			List<String> lastRowList = new ArrayList<String>();
		
			for (CategoryMapping catmapping : catmappingList) {
				if(catmapping.getLastRow() != null && !catmapping.getLastRow().equals("")) {
					String lastRow = catmapping.getLastRow().toLowerCase();
					
					if(!lastRowList.contains(lastRowList)) {
						lastRowList.add(lastRow);
					}
				}
			}
			if(!lastRowList.isEmpty()) {
				sectionMap.put(key, lastRowList);
			}
		}
		
		return sectionMap;
	
	}
	public static Map<String,CategoryMapping> getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(Event event) throws Exception {
		
		String intRegex = "[0-9]+";
		String strAlphabetRegex = "[a-z]+";
		
		Map<String,CategoryMapping> sectionMap = new HashMap<String, CategoryMapping>();
		Map<String,List<CategoryMapping>> catmappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(catmappingMap.keySet());
		
		//Tamil : for Bank of america theater events do not chop off rows
		boolean isAllRowEligibleRows = false;
		if(event.getVenueId().equals(8533)) {//Bank of America Theatre	Chicago
			isAllRowEligibleRows = true;
		}
		
		for (String key : keyList) {
			
			if(key.equals("-")) {
				continue;
			}
			
			List<CategoryMapping> catmappingList = catmappingMap.get(key);
		
			for (CategoryMapping catmapping : catmappingList) {
				if(catmapping.getLastRow() != null && !catmapping.getLastRow().equals("")) {
					String lastRow = catmapping.getLastRow().toLowerCase();
					
					try {
						Set<String> eligibleRowList = new HashSet<String>();
						String miniLastRow = null;
			
						if(lastRow.matches(intRegex)) {
							Integer miniLastRowInt = Integer.valueOf(lastRow);
							int startRowInt=1;
							
							if(!isAllRowEligibleRows) {
								//Tamil : if lastrow is between 1-5 do nothing, 6-19 do a -1 and  if between 20 to 29 do a-2 and if above 29 do a -3 from both sides.
								if(miniLastRowInt>= 1 && miniLastRowInt <= 5) {
									startRowInt=1;
									
								} else if(miniLastRowInt >= 6 && miniLastRowInt <= 19) {
									startRowInt = 2;
									miniLastRowInt = miniLastRowInt - 1;
								} else if(miniLastRowInt >= 20 && miniLastRowInt <= 29) {
									startRowInt = 3;
									miniLastRowInt = miniLastRowInt - 2;
								} else {//Row above 29
									startRowInt = 4;
									miniLastRowInt = miniLastRowInt - 3;
								}
							}

							if(miniLastRowInt >= startRowInt) {
								for (int i = miniLastRowInt; i >= startRowInt; i--) {
									eligibleRowList.add("" + i);
								}
								miniLastRow = "" + miniLastRowInt;
								//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
							}
							
						} else if(lastRow.matches(strAlphabetRegex)) {
							String charRepeatregex = "^" + lastRow.charAt(0) + "+$";
							if(lastRow.matches(charRepeatregex)) {
								
								int stringLength = lastRow.length();
								int miniLastRowInt = (int) lastRow.toLowerCase().charAt(0);
								int startRowInt=97;
								
								if(!isAllRowEligibleRows) {
									//Tamil : if lastrow is between 1-5 do nothing, 6-19 do a -1 and above 20 do a -2 from both sides.
									if(miniLastRowInt>= 97 && miniLastRowInt <= 101) {//Row between a to e
										startRowInt=97;//Row Starts from a
										
									} else if(miniLastRowInt >= 102 && miniLastRowInt <= 115) {//Row between f to s
										startRowInt = 98;//Row Starts from b
										miniLastRowInt = miniLastRowInt - 1;
									} else {//Row above s
										startRowInt = 99;//Row Starts from c
										miniLastRowInt = miniLastRowInt - 2;
									}
									if(miniLastRowInt == 105) {//If new category row is I then consider H instead of I
										//continue;
										miniLastRowInt = miniLastRowInt - 1;
									}
								}
								if(miniLastRowInt >= startRowInt) {
									for (int i = miniLastRowInt; i >= startRowInt; i--) {
										
										//Tamil : don't consider row i
										if(i == 105) {//Checking i value
											continue;
										}
										char tempChar = (char)i;
										String str="";
										
										for(int j=0; j < stringLength; j++) {
											str = str+tempChar;
										}
										eligibleRowList.add("" + str);
										
										if(i == miniLastRowInt) {
											miniLastRow = str;
											//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
										}
									}
								}
							} else if(lastRow != null && !lastRow.contains("-") && !lastRow.contains(" or ")) {
								eligibleRowList.add(lastRow);
								miniLastRow = lastRow;
							}
						//if lastrow is like GA or BOX then consider tickets from that row only and consider that row as cats row 	
						} else if(lastRow != null && !lastRow.contains("-") && !lastRow.contains(" or ")) {
							eligibleRowList.add(lastRow);
							miniLastRow = lastRow;
						}
						
						if(!eligibleRowList.isEmpty()) {
							CategoryMapping tempCatMapping = new CategoryMapping(catmapping);
							tempCatMapping.setMiniLastRow(miniLastRow.toUpperCase());
							tempCatMapping.setMiniLastEligibleRows(eligibleRowList);
							sectionMap.put(key, tempCatMapping);
							//break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						throw e;
					}
				}
			}
		}
		
		return sectionMap;
	
	}
public static Map<Integer,String> getMiniLastRow(String lastRow,Integer subtractCount) throws Exception {
		
		String intRegex = "[0-9]+";
		String strAlphabetRegex = "[a-z]+";
		
		Map<Integer,String> sectionMap = new HashMap<Integer,String>();
		lastRow = lastRow.toLowerCase();
					
		try {
			if(lastRow.matches(intRegex)) {
				Integer lastRowInt = Integer.valueOf(lastRow);
				int count = 0;
				for (int i=0;i<subtractCount;i++) {
					Integer tempRowInt = lastRowInt - count;
					if(tempRowInt >= 3) {
						sectionMap.put(i, ""+tempRowInt);	
					}
					count++;
				}
								
			} else if(lastRow.matches(strAlphabetRegex)) {
				String charRepeatregex = "^" + lastRow.charAt(0) + "+$";
				if(lastRow.matches(charRepeatregex)) {
				
					int stringLength = lastRow.length();
					int lastRowInt = (int) lastRow.toLowerCase().charAt(0);
					
					int count =0;
					for (int i=0;i<subtractCount;i++) {
						int tempRowInt = lastRowInt - count;
						if(tempRowInt == 105) {//If new category row is I then consider H instead of I
							tempRowInt = tempRowInt - 1;
							count++;
						}
						if(tempRowInt >= 99) {//97=a
							//Tamil : don't consider row i
							if(tempRowInt == 105) {//Checking i value
								continue;
							}
							char tempChar = (char)tempRowInt;
							String str="";
							for(int j=0; j < stringLength; j++) {
								str = str+tempChar;
							}
							sectionMap.put(i, ""+str);
						}
						count++;
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return sectionMap;
	}
public static String getTNDZonesLastRow(String lastRow) throws Exception {
	
	String intRegex = "[0-9]+";
	String strAlphabetRegex = "[a-z]+";
	
	lastRow = lastRow.toLowerCase();
	String tndLastRow = null;//lastRow;
				
	try {
		if(lastRow.matches(intRegex)) {
			Integer lastRowInt = Integer.valueOf(lastRow);
			lastRowInt = lastRowInt + 5;
			tndLastRow = ""+lastRowInt;
							
		} else if(lastRow.matches(strAlphabetRegex)) {
			String charRepeatregex = "^" + lastRow.charAt(0) + "+$";
			if(lastRow.matches(charRepeatregex)) {
			
				int stringLength = lastRow.length();
				int lastRowInt = (int) lastRow.toLowerCase().charAt(0);
				lastRowInt = lastRowInt + 5;
				
				if(lastRowInt == 105) {//If new category row is I then consider J instead of I
					lastRowInt = lastRowInt + 1;
				}
				if(lastRowInt > 122) {//122=z
					lastRowInt = 122;
				}
				//if(lastRowInt <= 122) {//122=z
					char tempChar = (char)lastRowInt;
					String str="";
					for(int j=0; j < stringLength; j++) {
						str = str+tempChar;
					}
					tndLastRow = ""+str;
				//}
			} else if(lastRow.trim().equals("ga")) {
				tndLastRow = lastRow;
			}
		}
		
	} catch (Exception e) {
		e.printStackTrace();
		throw e;
	}
	
	return tndLastRow;
}
	
public static Map<String,CategoryMapping> getSGLastFiveRowsSectionBasedEligibleTMATTicketRowsFromLastRow(Event event) throws Exception {
		
		String intRegex = "[0-9]+";
		String strAlphabetRegex = "[a-z]+";
		
		Map<String,CategoryMapping> sectionMap = new HashMap<String, CategoryMapping>();
		Map<String,List<CategoryMapping>> catmappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(catmappingMap.keySet());
		
		//Tamil : for Bank of america theater events do not chop off rows
		boolean isAllRowEligibleRows = false;
		if(event.getVenueId().equals(8533)) {//Bank of America Theatre	Chicago
			isAllRowEligibleRows = true;
		}
		
		for (String key : keyList) {
			
			if(key.equals("-")) {
				continue;
			}
			
			List<CategoryMapping> catmappingList = catmappingMap.get(key);
		
			for (CategoryMapping catmapping : catmappingList) {
				if(catmapping.getLastRow() != null && !catmapping.getLastRow().equals("")) {
					String lastRow = catmapping.getLastRow().toLowerCase();
					
					try {
						Set<String> eligibleRowList = new HashSet<String>();
						String miniLastRow = null;
			
						if(lastRow.matches(intRegex)) {
							Integer miniLastRowInt = Integer.valueOf(lastRow);
							int startRowInt=1;
							
							if(!isAllRowEligibleRows) {
								//Tamil : if lastrow is between 1-5 do nothing, 6-19 do a -1 and  if between 20 to 29 do a-2 and if above 29 do a -3 from both sides.
								if(miniLastRowInt>= 1 && miniLastRowInt <= 5) {
									startRowInt = miniLastRowInt-2;
								} else if(miniLastRowInt >= 6 && miniLastRowInt <= 10) {
									startRowInt = miniLastRowInt-3;
								} else if(miniLastRowInt > 10) {
									startRowInt = miniLastRowInt-7;
								}
							}
							if(miniLastRowInt >= startRowInt) {
								for (int i = miniLastRowInt; i >= startRowInt; i--) {
									if(i<1) {
										continue;
									}
									eligibleRowList.add("" + i);
								}
								miniLastRow = "" + miniLastRowInt;
								//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
							}
							
						} else if(lastRow.matches(strAlphabetRegex)) {
							String charRepeatregex = "^" + lastRow.charAt(0) + "+$";
							if(lastRow.matches(charRepeatregex)) {
								
								int stringLength = lastRow.length();
								int miniLastRowInt = (int) lastRow.toLowerCase().charAt(0);
								Integer startRowInt = null;
								
								if(!isAllRowEligibleRows) {
									//Tamil : if lastrow is between 1-5 do nothing, 6-19 do a -1 and above 20 do a -2 from both sides.
									if(miniLastRowInt>= 97 && miniLastRowInt <= 101) {//Row between a to e
										startRowInt = miniLastRowInt-2;
									} else if(miniLastRowInt >= 102 && miniLastRowInt <= 106) {//Row between f to s
										startRowInt = miniLastRowInt-3;
									} else if(miniLastRowInt > 106) {//Row above s
										startRowInt = miniLastRowInt - 7;
									}
								}
								if(miniLastRowInt == 105) {//If new category row is I then consider H instead of I
									//continue;
									miniLastRowInt = miniLastRowInt - 1;
								}
								if(startRowInt != null && miniLastRowInt >= startRowInt) {
									for (int i = miniLastRowInt; i >= startRowInt; i--) {
										
										//Tamil : don't consider row i
										if(i < 97 || i == 105) {//Checking i value
											continue;
										}
										char tempChar = (char)i;
										String str="";
										
										for(int j=0; j < stringLength; j++) {
											str = str+tempChar;
										}
										eligibleRowList.add("" + str);
										
										if(i == miniLastRowInt) {
											miniLastRow = str;
											//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
										}
									}
								}
							}
						}
						
						if(!eligibleRowList.isEmpty()) {
							CategoryMapping tempCatMapping = new CategoryMapping(catmapping);
							tempCatMapping.setMiniLastRow(miniLastRow.toUpperCase());
							tempCatMapping.setMiniLastEligibleRows(eligibleRowList);
							sectionMap.put(key, tempCatMapping);
							//break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						throw e;
					}
				}
			}
		}
		
		return sectionMap;
	
	}
	
	public static CategoryMapping computeLarryLAstCategoryMappingEligibleRowsFromLastRow(CategoryMapping catmapping,boolean isAllRowEligibleRows) throws Exception {

		
		String intRegex = "[0-9]+";
		String strAlphabetRegex = "[a-z]+";
		Set<String> eligibleRowList = null;
		
		if(catmapping.getLastRow() != null && !catmapping.getLastRow().equals("")) {
			String lastRow = catmapping.getLastRow().toLowerCase();
					
			try {
				eligibleRowList = new HashSet<String>();
				String miniLastRow = null;
	
				if(lastRow.matches(intRegex)) {
					Integer miniLastRowInt = Integer.valueOf(lastRow);
					int startRowInt=1;
					
					if(!isAllRowEligibleRows) {
						//Tamil : if lastrow is between 1-5 do nothing, 6-19 do a -1 and  if between 20 to 29 do a-2 and if above 29 do a -3 from both sides.
						if(miniLastRowInt>= 1 && miniLastRowInt <= 5) {
							startRowInt=1;
							
						} else if(miniLastRowInt >= 6 && miniLastRowInt <= 19) {
							startRowInt = 2;
							miniLastRowInt = miniLastRowInt - 1;
						} else if(miniLastRowInt >= 20 && miniLastRowInt <= 29) {
							startRowInt = 3;
							miniLastRowInt = miniLastRowInt - 2;
						} else {//Row above 29
							startRowInt = 4;
							miniLastRowInt = miniLastRowInt - 3;
						}
					}

					if(miniLastRowInt >= startRowInt) {
						for (int i = miniLastRowInt; i >= startRowInt; i--) {
							eligibleRowList.add("" + i);
						}
						miniLastRow = "" + miniLastRowInt;
						//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
					}
					
				} else if(lastRow.matches(strAlphabetRegex)) {
					String charRepeatregex = "^" + lastRow.charAt(0) + "+$";
					if(lastRow.matches(charRepeatregex)) {
						
						int stringLength = lastRow.length();
						int miniLastRowInt = (int) lastRow.toLowerCase().charAt(0);
						int startRowInt=97;
						
						if(!isAllRowEligibleRows) {
							//Tamil : if lastrow is between 1-5 do nothing, 6-19 do a -1 and above 20 do a -2 from both sides.
							if(miniLastRowInt>= 97 && miniLastRowInt <= 101) {//Row between a to e
								startRowInt=97;//Row Starts from a
								
							} else if(miniLastRowInt >= 102 && miniLastRowInt <= 115) {//Row between f to s
								startRowInt = 98;//Row Starts from b
								miniLastRowInt = miniLastRowInt - 1;
							} else {//Row above s
								startRowInt = 99;//Row Starts from c
								miniLastRowInt = miniLastRowInt - 2;
							}
							if(miniLastRowInt == 105) {//If new category row is I then consider H instead of I
								//continue;
								miniLastRowInt = miniLastRowInt - 1;
							}
						}
						
						if(miniLastRowInt >= startRowInt) {
							for (int i = miniLastRowInt; i >= startRowInt; i--) {
								
								//Tamil : don't consider row i
								if(i == 105) {//Checking i value
									continue;
								}
								char tempChar = (char)i;
								String str="";
								
								for(int j=0; j < stringLength; j++) {
									str = str+tempChar;
								}
								eligibleRowList.add("" + str);
								
								if(i == miniLastRowInt) {
									miniLastRow = str;
									//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
								}
							}
						}
					}
				}
				
				if(!eligibleRowList.isEmpty()) {
					CategoryMapping tempCatMapping = new CategoryMapping(catmapping);
					tempCatMapping.setMiniLastRow(miniLastRow.toUpperCase());
					tempCatMapping.setMiniLastEligibleRows(eligibleRowList);
					//sectionMap.put(key, tempCatMapping);
					//break;
					return tempCatMapping;
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		
		return null;
	}
	
public static Map<String,CategoryMapping> getLarryLastProductsLarrySectionBasedEligibleTMATTicketRowsFromLastRow(Event event) throws Exception {
		
		
		Map<String,CategoryMapping> larrySectionMap = new HashMap<String, CategoryMapping>();
		Map<String,List<CategoryMapping>> catmappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(catmappingMap.keySet());
		
		//Tamil : if event is hamilton and venue in Newyork city then consider all rows and do not chop any rows from lastrow
		boolean isAllRowEligibleRows = false;
		if(event.getName().equalsIgnoreCase("Hamilton") && event.getVenue().getCity().equalsIgnoreCase("New York")) {
			isAllRowEligibleRows = true;
		}
		
		//Tamil : for Bank of america theater events do not chop off rows
		if(event.getVenueId().equals(8533)) {//Bank of America Theatre	Chicago
			isAllRowEligibleRows = true;
		}

		for (String key : keyList) {
			
			if(key.equals("-")) {
				continue;
			}
			
			List<CategoryMapping> catmappingList = catmappingMap.get(key);
		
			for (CategoryMapping catmapping : catmappingList) {
				try {
					if(catmapping.getLarrySection() != null && catmapping.getLarrySection().length() > 0 &&
							 catmapping.getLastRow() != null && catmapping.getLastRow().length() > 0) {

						String larryKey = catmapping.getLarrySection().replaceAll("\\s+", " ").trim().toLowerCase();
						/*if(larrySectionMap.get(larryKey) != null) {
							continue;
						}*/
						
						CategoryMapping tempCatMapping = computeLarryLAstCategoryMappingEligibleRowsFromLastRow(catmapping,isAllRowEligibleRows);
						if(tempCatMapping != null) {
							larrySectionMap.put(larryKey, tempCatMapping);
							//break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
			}
		}
		
		return larrySectionMap;
	
	}
	
public static Map<Integer,String> getVipMiniLastRow(String vipLastRow,Integer subtractCount) throws Exception {
	
	String intRegex = "[0-9]+";
	String strAlphabetRegex = "[a-z]+";
	
	Map<Integer,String> sectionMap = new HashMap<Integer,String>();
	vipLastRow = vipLastRow.toLowerCase();
				
	try {
		if(vipLastRow.matches(intRegex)) {
			Integer vipLastRowInt = Integer.valueOf(vipLastRow);
			int count = 0;
			for (int i=0;i<subtractCount;i++) {
				Integer tempRowInt = vipLastRowInt - count;
				if(tempRowInt >= 3) {
					sectionMap.put(i, ""+tempRowInt);	
				}
				count++;
			}
							
		} else if(vipLastRow.matches(strAlphabetRegex)) {
			String charRepeatregex = "^" + vipLastRow.charAt(0) + "+$";
			if(vipLastRow.matches(charRepeatregex)) {
			
				int stringLength = vipLastRow.length();
				int vipLastRowInt = (int) vipLastRow.toLowerCase().charAt(0);
				
				int count =0;
				for (int i=0;i<subtractCount;i++) {
					int tempRowInt = vipLastRowInt - count;
					if(tempRowInt == 105) {//If new category row is I then consider H instead of I
						tempRowInt = tempRowInt - 1;
						count++;
					}
					if(tempRowInt >= 99) {//97=a
						//Tamil : don't consider row i
						if(tempRowInt == 105) {//Checking i value
							continue;
						}
						char tempChar = (char)tempRowInt;
						String str="";
						for(int j=0; j < stringLength; j++) {
							str = str+tempChar;
						}
						sectionMap.put(i, ""+str);
					}
					count++;
				}
			}
		}
		
	} catch (Exception e) {
		e.printStackTrace();
		throw e;
	}
	
	return sectionMap;
}
	// Mehul : added below method for VipLastRowCats
	
public static Map<String,CategoryMapping> getVipLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromVipLastRow(Event event) throws Exception {
		
		String intRegex = "[0-9]+";
		String strAlphabetRegex = "[a-z]+";
		
		Map<String,CategoryMapping> sectionMap = new HashMap<String, CategoryMapping>();
		Map<String,List<CategoryMapping>> catmappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(catmappingMap.keySet());
		
		for (String key : keyList) {
			
			if(key.equals("-")) {
				continue;
			}
			
			List<CategoryMapping> catmappingList = catmappingMap.get(key);
		
			for (CategoryMapping catmapping : catmappingList) {
				if(catmapping.getVipLastRow() != null && !catmapping.getVipLastRow().equals("")) {
					String vipLastRow = catmapping.getVipLastRow().toLowerCase();
					
					try {
						Set<String> eligibleRowList = new HashSet<String>();
						String miniVipLastRow = null;
						
						//Mehul: We are not skipping any rows
						
						if(vipLastRow.matches(intRegex)) {
							Integer miniVipLastRowInt = Integer.valueOf(vipLastRow);
							
							int startRowInt = 1; 

							for (int i = miniVipLastRowInt; i >= startRowInt; i--) {
								eligibleRowList.add("" + i);
							}
							miniVipLastRow = "" + miniVipLastRowInt;
							//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
							
							
						} else if(vipLastRow.matches(strAlphabetRegex)) {
							String charRepeatregex = "^" + vipLastRow.charAt(0) + "+$";
							if(vipLastRow.matches(charRepeatregex)) {
								
								int stringLength = vipLastRow.length();
								int miniVipLastRowInt = (int) vipLastRow.toLowerCase().charAt(0);
								
								if(miniVipLastRowInt == 105) {//If new category row is I then consider H instead of I
									//continue;
									miniVipLastRowInt = miniVipLastRowInt - 1;
								}
								//Mehul : we should consider rowsstarts from A
								int startRowInt = 97;
								
								//Mehul: We are not skipping any rows
									for (int i = miniVipLastRowInt; i >= startRowInt; i--) {
										char tempChar = (char)i;
										String str="";
										
										//Tamil : don't consider row i
										if(i == 105) {//Checking i value
											continue;
										}
										
										for(int j=0; j < stringLength; j++) {
											str = str+tempChar;
										}
										eligibleRowList.add("" + str);
										
										if(i == miniVipLastRowInt) {
											miniVipLastRow = str;
											//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
										}
									}
								
							}
						}
						
						if(!eligibleRowList.isEmpty()) {
							CategoryMapping tempCatMapping = new CategoryMapping(catmapping);
							tempCatMapping.setMiniVipLastRow(miniVipLastRow.toUpperCase());
							tempCatMapping.setMiniVipLastEligibleRows(eligibleRowList);
							sectionMap.put(key, tempCatMapping);
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						throw e;
					}
				}
			}
		}
		
		return sectionMap;
	
	}
	
	public static Map<String, CategoryMapping> computeLastRowForSectionsByVenueCategoryId(Integer venueCategoryId) throws Exception {
		
		List<CategoryMapping> list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategoryId);
		Map<String, CategoryMapping> map = new HashMap<String, CategoryMapping>();
		
		for(CategoryMapping mapping:list){
			String lastRow = null;
			if(mapping.getLastRow() != null && mapping.getLastRow().trim().length() > 0 ) {
				lastRow = mapping.getLastRow();
				map.put(mapping.getStartSection().replaceAll("\\s+", " ").trim().toLowerCase(),mapping);
			}
			
			if(mapping.getStartSection().equals(mapping.getEndSection())){
				continue;
			}
			boolean isInt=false;
			Integer intStartSection =0;
			String startSection ="";
			String endSection  ="";
			Integer intEndSection =0; 
			String prefix="";  
			// Consider only integer section range..
			try{
				startSection = mapping.getStartSection();
				endSection = mapping.getEndSection();
				intStartSection= Integer.parseInt(startSection);
				intEndSection= Integer.parseInt(endSection);
				isInt=true;
			}catch (Exception ex) {
				
			}
			
			if(isInt){
				for(int start=intStartSection;start<=intEndSection;start++){
					String sec = prefix + start;
					if(sec.equals(startSection)){
						continue;
					}
					if(null != lastRow) {
						map.put(sec.toLowerCase(), mapping);
					}
				}
			}
		}
		return map;
	}
	
	public static Map<String, CategoryMapping> computeLastRowForCategoryandSectionByVenueCategoryId(Integer venueCategoryId) throws Exception {
		
		Collection<CategoryMapping> list = getAllCategoryMappingsByVenueCategoryId(venueCategoryId);
		//List<CategoryMapping> list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategoryId);
		Map<String, CategoryMapping> map = new HashMap<String, CategoryMapping>();
		
		for(CategoryMapping mapping:list){
			String lastRow = null;
			if(mapping.getLastRow() != null && mapping.getLastRow().trim().length() > 0 ) {
				lastRow = mapping.getLastRow();
				String key = mapping.getCategoryId()+":"+mapping.getStartSection().replaceAll("\\s+", " ").trim().toLowerCase(); 
				map.put(key,mapping);
			}
			
			if(mapping.getStartSection().equals(mapping.getEndSection())){
				continue;
			}
			boolean isInt=false;
			Integer intStartSection =0;
			String startSection ="";
			String endSection  ="";
			Integer intEndSection =0; 
			String prefix="";  
			// Consider only integer section range..
			try{
				startSection = mapping.getStartSection();
				endSection = mapping.getEndSection();
				intStartSection= Integer.parseInt(startSection);
				intEndSection= Integer.parseInt(endSection);
				isInt=true;
			}catch (Exception ex) {
				
			}
			
			if(isInt){
				for(int start=intStartSection;start<=intEndSection;start++){
					String sec = prefix + start;
					if(sec.equals(startSection)){
						continue;
					}
					if(null != lastRow) {
						String key = mapping.getCategoryId()+":"+sec.toLowerCase();
						map.put(key, mapping);
					}
				}
			}
		}
		return map;
	}
	
	public static Collection<Ticket> computeSectionForTickets(Collection<Ticket> tickets,List<Category> categoryList ,Event event) throws Exception {

		/*Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		for (Category category: categoryList) {
			categoryMap.put(category.getId(), category);
		}*/
		
//		return computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getNormalizedRow(), ticket.getNormalizedSeat(),groupName);
		Collection<Ticket> result = new ArrayList<Ticket>();
		if(tickets==null || tickets.isEmpty()){
			return tickets;
		}
//		Integer eventId= tickets.iterator().next().getEventId();
		Map<String, List<CategoryMapping>> map = null;
		Map<Long, Map<String, List<CategoryMapping>>> catMap = mapTime.get(event.getVenueCategoryId());
		if(catMap!=null && !catMap.isEmpty()){
			Long time = catMap.keySet().iterator().next();
			if(new Date().getTime() - time <= 1000*60*5){
				map = catMap.get(time);
			}
		}else{
			catMap = new HashMap<Long, Map<String,List<CategoryMapping>>>();
		}
		if(map==null){
			Collection<CategoryMapping> list = null;
			if(mappingTimeMap.get(event.getVenueCategoryId())==null || (new Date().getTime()-mappingTimeMap.get(event.getVenueCategoryId())>=1000*60*5)){
				list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
				realTimeMappingMap.put(event.getVenueCategoryId(), list);
				mappingTimeMap.put(event.getVenueCategoryId(), new Date().getTime());
			}else{
				list = realTimeMappingMap.get(event.getVenueCategoryId());
			}
			map = new HashMap<String, List<CategoryMapping>>();
			List<CategoryMapping> mappingList = null;
			for(CategoryMapping mapping:list){
				
				mappingList = map.get(mapping.getStartSection().replaceAll("\\s+", " ").trim().toLowerCase());
				
				if(mappingList==null || (null != mapping.getRowRange() && mapping.getRowRange().trim().length() > 0)){
					mappingList = new ArrayList<CategoryMapping>();
					mappingList.add(mapping);
				}
				map.put(mapping.getStartSection().replaceAll("\\s+", " ").trim().toLowerCase(), mappingList);
				
				if(mapping.getStartSection().equals(mapping.getEndSection())){
					continue;
				}
				
				mappingList = map.get(mapping.getEndSection().replaceAll("\\s+", " ").trim().toLowerCase());
				if(mappingList==null || (null != mapping.getRowRange() && mapping.getRowRange().trim().length() > 0)){
					mappingList = new ArrayList<CategoryMapping>();
					mappingList.add(mapping);
				}
				
				map.put(mapping.getEndSection().replaceAll("\\s+", " ").trim().toLowerCase(), mappingList);
				
				
				
				//For skiping intermediate sections.
				/*boolean isInt=false;
				Integer intStartSection =0;
				String startSection ="";
				String endSection  ="";
				Integer intEndSection =0; 
				String prefix="";  
				// Consider only integer section range..
				try{
					startSection = mapping.getStartSection();
					endSection = mapping.getEndSection();
					intStartSection= Integer.parseInt(startSection);
					intEndSection= Integer.parseInt(endSection);
					isInt=true;
				}catch (Exception ex) {
					
				}
				
				if(isInt){
					for(int start=intStartSection;start<=intEndSection;start++){
						String sec = prefix + start;
						if(sec.equals(startSection)){
							continue;
						}
						mappingList = map.get(mapping.getStartSection().toLowerCase());
						if(mappingList==null){
							mappingList = new ArrayList<CategoryMapping>();
						}
						mappingList.add(mapping);
						map.put(sec.toLowerCase(), mappingList);
					}
					
				}*/
			}
			catMap.clear();
			catMap.put(new Date().getTime(), map);
			mapTime.put(event.getVenueCategoryId(),catMap);
		}
		for(Ticket ticket:tickets){
			List<CategoryMapping> mappingList = map.get(ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase());
			if(mappingList!=null && !mappingList.isEmpty()){
				CategoryMapping mapping = mappingList.get(0);				
				//ticket.setCategory(categoryMap.get(mapping.getCategoryId()));
				//ticket.setCategoryMapping(mapping);
				result.add(ticket);
				
				/*for(CategoryMapping mapping:mappingList){
					Integer catId = computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getRow(),ticket.getSeat(),mapping);
					if(catId!=null){
						ticket.setCategory(categoryMap.get(catId));
						ticket.setCategoryMapping(mapping);
						result.add(ticket);
						break;
					}
//					Collection<CategoryMapping> list = Categorizer.getMappingMap(ticket.getEventId()+ groupName);
//					if(list!=null && mapping.getCategoryId() != null){
//						for(CategoryMapping categoryMapping:list){
//							if(categoryMapping.getCategoryId().equals(mapping.getCategoryId())){
//								ticket.setCategoryMappingId(categoryMapping.getId());
//								break;
//							}
//						}
//					}
				}	*/
			}
			
		}
		return result;
	}
	
//	public static Integer computeCategory(BaseTicket ticket, String groupName) {
//		return computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getNormalizedRow(),ticket.getNormalizedSeat(), groupName);
//	}
	
	/**
	 * computeCategory
	 * 
	 * Will return a category id if there exists a category mapping for this
	 * section, row, and seat. If not it will check for a category id for
	 * this section and row.
	 * @param eventId
	 * @param section
	 * @param row
	 * @param startSeat
	 * @return
	 * @throws SQLException 
	 */
	public static CategoryMapping computeCategory(Event event, String section, String row, String seatRange, String groupName) throws Exception{
		
		String startSeat = null;
		String endSeat = null;
		logger.info("in computeCategory method, eventID :"+event.getId()+" section :"+ section+" row :"+row+" seatRange :"+seatRange+" groupname :"+groupName);
		if(seatRange != null && !seatRange.isEmpty() ) {
			 String[] tokens = seatRange.split("-", 2);
			 if(tokens.length == 2) {
				 startSeat = tokens[0];
				 endSeat = tokens[1];
			 } else {
				 startSeat = seatRange;
				 endSeat = seatRange;
			 }
		}
		Collection<CategoryMapping> list=null;
		if(mappingTimeMap.get(event.getId()+"-"+ groupName)==null || (new Date().getTime()-mappingTimeMap.get(event.getId() +"-"+ groupName)>=1000*60*5)){
			list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
			realTimeMappingMap.put(event.getVenueCategoryId(), list);
			mappingTimeMap.put(event.getVenueCategoryId(), new Date().getTime());
		}else{
			list = realTimeMappingMap.get(event.getId()+"-"+ groupName);
		}
		
//		Collection<CategoryMapping> list = mappingMap.get(eventId + groupName);
//		Collection<CategoryMapping> list = TMATDAORegistry.getCategoryMappingDAO().getAllCategoryMappingsFromEventAndScheme(eventId,groupName);
		
		
		
		if(list == null) {
//			logger.info("The mappingMap is null for: " + event.getId() + groupName);
		
			return computeCategory(event, section, row);
		}
//		logger.info("categoryMapping list size :"+list.size());
		// francois - FIXME: looks like inefficient code (should sort before putting in map)
		// -- begin --
		
		
		List<CategoryMapping> catList = new ArrayList<CategoryMapping>();
		catList.addAll(list);
		
		//order list for "Worst First"
		orderCatMaps(catList);
		
		// -- end --
//		for(CategoryMapping  cm :  catList){
////			logger.info("Category mapping, key is : " + cm.getKey() + " category is: " + cm.getCategory().getSymbol());
//		}
		logger.info("Category List Size :"+catList.size());
		for (CategoryMapping mapping: catList) {
			Integer cmp1 = compare(mapping.getStartSection(), section);
			if (cmp1 == null) {
				continue;
			}
			
			Integer cmp2 = compare(section, mapping.getEndSection());
			if (cmp2 == null) {
				continue;
			}

			Integer cmp3 = compare(mapping.getStartRow(), row);
			if (cmp3 == null) {
				continue;
			}

			Integer cmp4 = compare(row, mapping.getEndRow());
			if (cmp4 == null) {
				continue;
			}

			
			if(startSeat == null || endSeat == null) {
				boolean flag = false;
				if((mapping.getStartSeat()!= null && (mapping.getStartSeat().equals("^") || mapping.getStartSeat().equals("*"))) ||( mapping.getEndSeat()!=null && (mapping.getEndSeat().equals("^") ||mapping.getEndSeat().equals("*"))))
				{
					flag= true;
				}
				if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && (flag))  {
					return mapping;
				}
			}else if(!(mapping.getStartSeat().equals("^"))){
				Integer cmp5 = compareSeat(mapping.getStartSeat(), startSeat);
				if (cmp5 == null) {
					continue;
				}
				
				Integer cmp6 = compareSeat(startSeat, mapping.getEndSeat());
				if (cmp6 == null) {
					continue;
				}
	
				
				Integer cmp7 = compareSeat(mapping.getStartSeat(), endSeat);
				if (cmp7 == null) {
					continue;
				}
				
				Integer cmp8 = compareSeat(endSeat, mapping.getEndSeat());
				if (cmp8 == null) {
					continue;
				}
	
				// if one of the seats matches, it should be the lowest category
				//Changes for including seat number in categorization
				if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && ((cmp5 <= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)||(cmp5 >= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)))  {
					return mapping;
				}
			}
		}
		return null;
	}
	
	
	/*public static Integer computeCategory(Integer eventId, String section, String row, String seatRange,CategoryMapping mapping){
		
		String startSeat = null;
		String endSeat = null;
		
		if(seatRange != null && !seatRange.isEmpty() ) {
			 String[] tokens = seatRange.split("-", 2);
			 if(tokens.length == 2) {
				 startSeat = tokens[0];
				 endSeat = tokens[1];
			 } else {
				 startSeat = seatRange;
				 endSeat = seatRange;
			 }
		}	
		
			Integer cmp1 = compare(mapping.getStartSection(), section);
			if (cmp1 == null) {
				return null;
			}
			
			Integer cmp2 = compare(section, mapping.getEndSection());
			if (cmp2 == null) {
				return null;
			}
			
			if(row == null || row.isEmpty()) {
				if((null !=mapping.getStartRow() && mapping.getStartRow().equals("^")) || 
						(null !=mapping.getEndRow() && mapping.getEndRow().equals("^"))){
					
					if (cmp1 <= 0 && cmp2 <= 0 )  {
						return mapping.getCategoryId();
					}
				}
			}
			

			Integer cmp3 = compare(mapping.getStartRow(), row);
			if (cmp3 == null) {
				return null;
			}

			Integer cmp4 = compare(row, mapping.getEndRow());
			if (cmp4 == null) {
				return null;
			}

			
			if(startSeat == null || endSeat == null) {
				boolean flag = false;
				if((mapping.getStartSeat()!= null && (mapping.getStartSeat().equals("^") || mapping.getStartSeat().equals("*"))) ||( mapping.getEndSeat()!=null && (mapping.getEndSeat().equals("^") ||mapping.getEndSeat().equals("*"))))
				{
					flag= true;
				}
				if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && (flag))  {
					return mapping.getCategoryId();
				}
			}else if(!(mapping.getStartSeat().equals("^"))){
				Integer cmp5 = compareSeat(mapping.getStartSeat(), startSeat);
				if (cmp5 == null) {
					return null;
				}
				
				Integer cmp6 = compareSeat(startSeat, mapping.getEndSeat());
				if (cmp6 == null) {
					return null;
				}

				
				Integer cmp7 = compareSeat(mapping.getStartSeat(), endSeat);
				if (cmp7 == null) {
					return null;
				}
				
				Integer cmp8 = compareSeat(endSeat, mapping.getEndSeat());
				if (cmp8 == null) {
					return null;
				}

				// if one of the seats matches, it should be the lowest category
				//Changes for including seat number in categorization
				if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && ((cmp5 <= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)||(cmp5 >= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)))  {
					return mapping.getCategoryId();
				}
			}
		return null;
	}*/
	
	
	/*public static Integer computeCategory1(Integer eventId, String section, String row, String seatRange, CategoryMapping mapping){
	      
	      String startSeat = null;
	      String endSeat = null;
	      
	      if(seatRange != null && !seatRange.isEmpty() ) {
	            String[] tokens = seatRange.split("-", 2);
	            if(tokens.length == 2) {
	                  startSeat = tokens[0];
	                  endSeat = tokens[1];
	            } else {
	                  startSeat = seatRange;
	                  endSeat = seatRange;
	            }
	      }     
	      Integer cmp1 = null;
	      Integer cmp2 = null;
	      boolean isSectionRange = false;
	      boolean isRange = false;
	      if(section.contains("-")){
	            try{
	                  String sections[] = section.split("-");
	                  String startSection = sections[0];
	                  String endSection = sections[1];
	                  try{
	                        Integer.parseInt(startSection);
	                        Integer.parseInt(endSection);
	                        isRange= true;
	                  }catch (Exception e) {
	                        startSection = section;
	                        endSection = section;
	                  }
	                  String mappingStartSection = mapping.getStartSection();
	                  String mappingEndSection = mapping.getEndSection();
	                  if(mappingStartSection.equalsIgnoreCase(mappingEndSection)){
	                        if(mappingStartSection.contains("-")){
	                              String tempStartSection = "";
	                              String tempEndSection = "";
	                              if(isRange){
	                                    tempStartSection = mappingStartSection.split("-")[0];
	                                    tempEndSection = mappingStartSection.split("-")[1];
	                              }else{
	                                    tempStartSection = mappingStartSection;
	                                    tempEndSection = mappingStartSection;
	                              }
	                              
	                              Integer tempStartSecton1 = compare(tempStartSection, startSection +"");
	                              Integer tempStartSecton2 = compare(tempEndSection, startSection +"");
	                              Integer tempEndSecton1 = compare(tempStartSection, endSection +"");
	                              Integer tempEndSecton2 = compare(tempEndSection, endSection +"");
	                              if(tempStartSecton1==null || tempStartSecton2 ==null || tempEndSecton1==null || tempEndSecton2 ==null  
	                                          || tempStartSecton1>0 || tempStartSecton2<0 || tempEndSecton1>0 || tempEndSecton2<0){
	                                    return null;
	                              }else{
	                                    cmp1 = -1;
	                                    cmp2 = -1;
	                                    isSectionRange = true;
	                              }
	                        }
	                        
	                  }
	                  
	            }catch (Exception e) {
	                  isSectionRange = false;
	            }
	      }
	      if(!isSectionRange){
	            cmp1 = compare(mapping.getStartSection(), section);
	            if (cmp1 == null) {
	                  return null;
	            }
	      
	            cmp2 = compare(section, mapping.getEndSection());
	            if (cmp2 == null) {
	                  return null;
	            }
	      }
	            if(row == null || row.isEmpty()) {
	                  if((null !=mapping.getStartRow() && mapping.getStartRow().equals("^")) || 
	                              (null !=mapping.getEndRow() && mapping.getEndRow().equals("^"))){
	                        
	                        if (cmp1 <= 0 && cmp2 <= 0 )  {
	                              return mapping.getCategoryId();
	                        }
	                  }
	            }

	            Integer cmp3 = compare(mapping.getStartRow(), row);
	            if (cmp3 == null) {
	                  return null;
	            }

	            Integer cmp4 = compare(row, mapping.getEndRow());
	            if (cmp4 == null) {
	                  return null;
	            }

	            
	            if(startSeat == null || endSeat == null) {
	                  boolean flag = false;
	                  if((mapping.getStartSeat()!= null && (mapping.getStartSeat().equals("^") || mapping.getStartSeat().equals("*"))) ||( mapping.getEndSeat()!=null && (mapping.getEndSeat().equals("^") ||mapping.getEndSeat().equals("*")))){
	                        flag= true;
	                  }
	                  if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && (flag))  {
	                        return mapping.getCategoryId();
	                  }
	            }else if(!(mapping.getStartSeat().equals("^"))){
	                  Integer cmp5 = compareSeat(mapping.getStartSeat(), startSeat);
	                  if (cmp5 == null) {
	                        return null;
	                  }
	                  
	                  Integer cmp6 = compareSeat(startSeat, mapping.getEndSeat());
	                  if (cmp6 == null) {
	                        return null;
	                  }

	                  
	                  Integer cmp7 = compareSeat(mapping.getStartSeat(), endSeat);
	                  if (cmp7 == null) {
	                        return null;
	                  }
	                  
	                  Integer cmp8 = compareSeat(endSeat, mapping.getEndSeat());
	                  if (cmp8 == null) {
	                        return null;
	                  }

	                  // if one of the seats matches, it should be the lowest category
	                  //Changes for including seat number in categorization
	                  if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && ((cmp5 <= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)||(cmp5 >= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)))  {
	                        return mapping.getCategoryId();
	                  }
	            }
//	    }
	      return null;
	}*/

	
	public static Integer computeCategory(Integer eventId, String section, String row, String seatRange,CategoryMapping mapping) throws Exception{
		
		String startSeat = null;
		String endSeat = null;
		
		if(seatRange != null && !seatRange.isEmpty() ) {
			 String[] tokens = seatRange.split("-", 2);
			 if(tokens.length == 2) {
				 startSeat = tokens[0];
				 endSeat = tokens[1];
			 } else {
				 startSeat = seatRange;
				 endSeat = seatRange;
			 }
		}	
		Integer cmp1 = null,cmp2 = null;
		boolean isSectionRange = false;
		if(section.contains("-")){
			try{
				String sections[] = section.split("-");
			
				//int startSection = Integer.parseInt(sections[0]);
				//int endSection = Integer.parseInt(sections[1]);
				String startSection = sections[0];
				String endSection = sections[1];
				String mappingStartSection = mapping.getStartSection();
				String mappingEndSection = mapping.getEndSection();
				if(mappingStartSection.equalsIgnoreCase(mappingEndSection)){
					if(mappingStartSection.contains("-")){
						String tempStartSection = mappingStartSection.split("-")[0];
						String tempEndSection = mappingStartSection.split("-")[1];
						Integer tempStartSecton1 = compare(tempStartSection, startSection);
						Integer tempStartSecton2 = compare(tempEndSection, startSection);
						Integer tempEndSecton1 = compare(tempStartSection, endSection);
						Integer tempEndSecton2 = compare(tempEndSection, endSection);

						if(tempStartSecton1==null || tempStartSecton2 ==null || tempEndSecton1==null || tempEndSecton2 ==null  
								|| tempStartSecton1>0 || tempStartSecton2<0 || tempEndSecton1>0 || tempEndSecton2<0){
							return null;
						}else{
							cmp1 = -1;
							cmp2 = -1;
							isSectionRange = true;
						}
					}
					
				}
				
//				Integer temp1 = compare(mapping.getStartSection(), startSection +"");
//				Integer temp2 = compare(mapping.getStartSection(), endSection +"");
				
//				isSectionRange = true;
			}catch (Exception e) {
				isSectionRange = false;
			}
		}
		if(!isSectionRange){
			cmp1 = compare(mapping.getStartSection(), section);
			if (cmp1 == null) {
				return null;
			}
			cmp2 = compare(section, mapping.getEndSection());
			if (cmp2 == null) {
				return null;
			}
		}
		 
		if(row==null || row.isEmpty()){
			if((null !=mapping.getStartRow() && mapping.getStartRow().equals("^")) || 
					(null !=mapping.getEndRow() && mapping.getEndRow().equals("^"))){
				
				if (cmp1 <= 0 && cmp2 <= 0 )  {
					return mapping.getCategoryId();
				}
			}
		}
		
		Integer cmp3 = compare(mapping.getStartRow(), row);
		if (cmp3 == null) {
			return null;
		}

		Integer cmp4 = compare(row, mapping.getEndRow());
		if (cmp4 == null) {
			return null;
		}

		
		if(startSeat == null || endSeat == null) {
			boolean flag = false;
			if((mapping.getStartSeat()!= null && (mapping.getStartSeat().equals("^") || mapping.getStartSeat().equals("*"))) ||( mapping.getEndSeat()!=null && (mapping.getEndSeat().equals("^") ||mapping.getEndSeat().equals("*"))))
			{
				flag= true;
			}
			if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && (flag))  {
				return mapping.getCategoryId();
			}
		}else if(!(mapping.getStartSeat().equals("^"))){
			Integer cmp5 = compareSeat(mapping.getStartSeat(), startSeat);
			if (cmp5 == null) {
				return null;
			}
			
			Integer cmp6 = compareSeat(startSeat, mapping.getEndSeat());
			if (cmp6 == null) {
				return null;
			}

			
			Integer cmp7 = compareSeat(mapping.getStartSeat(), endSeat);
			if (cmp7 == null) {
				return null;
			}
			
			Integer cmp8 = compareSeat(endSeat, mapping.getEndSeat());
			if (cmp8 == null) {
				return null;
			}

			// if one of the seats matches, it should be the lowest category
			//Changes for including seat number in categorization
			if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && ((cmp5 <= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)||(cmp5 >= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)))  {
				return mapping.getCategoryId();
			}
		}
		return null;
	}
	
	public static CategoryMapping computeCategory(Event event, String section, String row) throws Exception {
//		Collection<CategoryMapping> list = mappingMap.get(eventId + groupName);
		Collection<CategoryMapping> list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
		List<CategoryMapping> catList = new ArrayList<CategoryMapping>();
		
		if (list != null) {
			catList.addAll(list);
		}
		
		if (section == null) {
			section = "";
		}
		
		if (row == null) {
			row = "";
		}
		
		if (list == null) {
			return null;
		}
		
		orderCatMaps(catList);
		for (CategoryMapping mapping: catList) {
			Integer cmp1 = compare(mapping.getStartSection(), section);
			if (cmp1 == null) {
				continue;
			}

			Integer cmp2 = compare(section, mapping.getEndSection());
			if (cmp2 == null) {
				continue;
			}

			Integer cmp3 = compare(mapping.getStartRow(), row);
			if (cmp3 == null) {
				continue;
			}
			
			Integer cmp4 = compare(row, mapping.getEndRow());
			if (cmp4 == null) {
				continue;
			}
			
			if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0) {
				return mapping;
			}
			
		}
		//logger.info("returning null w/ Mappings");
		return null;
	}
		
	public static Integer computeCategory(List<CategoryMapping> catList, String section, String row) {

		if (section == null) {
			section = "";
		}
		
		if (row == null) {
			row = "";
		}
		
		if (catList == null) {
			return null;
		}
				
		if (catList.isEmpty()) {
			return null;
		}
		
		orderCatMaps(catList);
			for (CategoryMapping mapping: catList) {
				Integer cmp1 = compare(mapping.getStartSection(), section);
				if (cmp1 == null) {
					continue;
				}
				Integer cmp2 = compare(section, mapping.getEndSection());
				if (cmp2 == null) {
					continue;
				}
				Integer cmp3 = compare(mapping.getStartRow(), row);
				if (cmp3 == null) {
					continue;
				}
				Integer cmp4 = compare(row, mapping.getEndRow());
				if (cmp4 == null) {
					continue;
				}
				
				if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0) {
					return mapping.getCategoryId();
				}
				
			}
		//logger.info("returning null w/ Mappings");
		return null;
	}
	/*
	 * Compare by letter blocks and number blocks
	 * (e.g., ab56 > ab8 even though alphabetically it's not the case)
	 * we compare block per block so in this case first "ab" and "ab" then 56 and 8
	 * However if we have to compare things like: 20C and 19, then these are not comparable
	 * 
	 * So the idea is when we compare 20C and 19
	 * is first to compare that potentially 19 is less than 20. Then keep going and see that these 2 are not
	 * comparable
	 * 
	 * Same thing for 20C and 21BT, these 2 are not comparable
	 * However 20C and 21B are comparable
	 * 
	 * so we parse block per block (of letters of numbers) and whenever we
	 * find which is one is greater than the others, we keep going to check
	 * that the 2 symbols are comparable.
	 * 
	 * New addition: now, we have one more restriction: 12C and 14D are not comparable
	 * However 12C and 14C are.
	 * 
	 * They are comparable if:
	 * - they have the same structure: same alternance of number and letter blocks
	 * - letters block which are compared are of the same size (cannot compare C and CD for instance)
	 */
	
	public static Integer compare(String symbol1, String symbol2) {
		if (symbol1 == null || symbol2 == null) {
			return null;
		}
		
		symbol1 = symbol1.replaceAll(" ", "").toLowerCase();
		symbol2 = symbol2.replaceAll(" ", "").toLowerCase();

		if (symbol1.equals("*")) {
			return 0;
		}

		if (symbol2.equals("*")) {
			return 0;
		}
		
		if (symbol1.length() == 0 || symbol2.length() == 0) {
			return null;
		}
		
		if (symbol1.charAt(0) == '>') {
			for (String token: symbol1.split(">")) {
				
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				
				if (symbol2.indexOf(token) >= 0) {
					return 0;
				}
			}
			
			return null;
		}

		if (symbol2.charAt(0) == '>') {
			for (String token: symbol2.split(">")) {
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				if (symbol1.indexOf(token) >= 0) {
					return 0;
				}
			}
			
			return null;
		}

		Matcher matcher1 = numberOrLetterBlock.matcher(symbol1);
		Matcher matcher2 = numberOrLetterBlock.matcher(symbol2);
		Integer offset1 = 0;
		Integer offset2 = 0;
		
		Integer result = 0;
		while (true) {
			Boolean match1 = matcher1.find(offset1);
			Boolean match2 = matcher2.find(offset2);
			
//			System.out.println("MATCH " + match1 + " " + match2);
			if (match1 == false && match2 == false) {
				return result;
			}

			// if one has a next block and not the other => not comparable
			if (match1 == false || match2 == false) {
				return null;
			}

			String letters1 = matcher1.group(1);
			String letters2 = matcher2.group(1);

			// block of numbers to compare
			if (letters1 == null && letters2 == null) {
				String numberStr1 = matcher1.group(2);
				String numberStr2 = matcher2.group(2);

				try {
					/*
					 * getting numberFormatException for numbers greater then 10 digits so used Long instead of Integer
					 * In future we may change long to anyother datatype or some other logic if number 'll be greater then 19 digits					 * 
					 * Integer number1 = Integer.parseInt(numberStr1);
					 * Integer number2 = Integer.parseInt(numberStr2);
					*/
					Long number1 = Long.parseLong(numberStr1);
					Long number2 = Long.parseLong(numberStr2);
					if (number1 < number2) {
						// result already found
						if (result == 0) {
							result = -1;
						} else { // not comparable
							return null;
						}
					} else if (number1 > number2) {
						// result already found
						if (result == 0) {
							result = 1;
						} else { // not comparable
							return null;
						}
					}
					offset1 += numberStr1.length();
					offset2 += numberStr2.length();
				} catch (NumberFormatException nfe) {
					logger.info("Number format exception while parsing" +nfe.getMessage());
					return null;
					}
				catch (Exception e)
				{
					logger.info("Exception while parsing" +e.getMessage());
					return null;
				}
								
			} else if (letters1 != null & letters2 != null) {
				// block of letters to compare
				// must have the same length othewise not comparable
				if (letters1.length() != letters2.length()) {
					return null;
				}
				
				Integer cmp = letters1.compareToIgnoreCase(letters2);
				if (cmp < 0 || cmp > 0) {
					// result already found
					if (result == 0) {
						result = cmp;
					} else { // not comparable
						return null;						
					}
				}
				
				offset1 += letters1.length();
				offset2 += letters2.length();
			} else {
				// letter vs. number
				return null;				
			}
		}
		
	}

	public static Integer compareSeat(String symbol1, String symbol2) {
		if (symbol1 == null || symbol2 == null) {
			return null;
		}
		
		symbol1 = symbol1.replaceAll(" ", "").toLowerCase();
		symbol2 = symbol2.replaceAll(" ", "").toLowerCase();

		if (symbol1.equals("*")|| symbol1.equals("^")) {
			return 0;
		}
	
		if (symbol2.equals("*") || symbol2.equals("^")) {
			return 0;
		}
		
		if (symbol1.length() == 0 || symbol2.length() == 0) {
			return null;
		}
		
		if (symbol1.charAt(0) == '>') {
			for (String token: symbol1.split(">")) {
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				if(org.apache.commons.lang.StringUtils.isNumeric(token) && org.apache.commons.lang.StringUtils.isNumeric(symbol2))
				{
					if (new Integer(symbol2) > new Integer(token)) {
						return 0;
					}
				}
			}
			
			return null;
		}

		if (symbol2.charAt(0) == '>') {
			for (String token: symbol2.split(">")) {
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				if(org.apache.commons.lang.StringUtils.isNumeric(token) && org.apache.commons.lang.StringUtils.isNumeric(symbol1))
				{
					if (new Integer(symbol1) > new Integer(token)) {
						return 0;
					}
				}
			}
			
			return null;
		}

		Matcher matcher1 = numberOrLetterBlock.matcher(symbol1);
		Matcher matcher2 = numberOrLetterBlock.matcher(symbol2);
		Integer offset1 = 0;
		Integer offset2 = 0;
		
		Integer result = 0;
		while (true) {
			Boolean match1 = matcher1.find(offset1);
			Boolean match2 = matcher2.find(offset2);
			
//			System.out.println("MATCH " + match1 + " " + match2);
			if (match1 == false && match2 == false) {
				return result;
			}

			// if one has a next block and not the other => not comparable
			if (match1 == false || match2 == false) {
				return null;
			}

			String letters1 = matcher1.group(1);
			String letters2 = matcher2.group(1);

			// block of numbers to compare
			if (letters1 == null && letters2 == null) {
				String numberStr1 = matcher1.group(2);
				String numberStr2 = matcher2.group(2);

				try {
					Integer number1 = Integer.parseInt(numberStr1);
					Integer number2 = Integer.parseInt(numberStr2);
					if (number1 < number2) {
						// result already found
						if (result == 0) {
							result = -1;
						} else { // not comparable
							return null;
						}
					} else if (number1 > number2) {
						// result already found
						if (result == 0) {
							result = 1;
						} else { // not comparable
							return null;
						}
					}
					offset1 += numberStr1.length();
					offset2 += numberStr2.length();
				} catch (NumberFormatException nfe) {
					logger.info("Number format exception while parsing" +nfe.getMessage());
					return null;
					}
				catch (Exception e)
				{
					logger.info("Exception while parsing" +e.getMessage());
					return null;
				}
								
			} else if (letters1 != null & letters2 != null) {
				// block of letters to compare
				// must have the same length otherwise not comparable
				if (letters1.length() != letters2.length()) {
					return null;
				}
				
				Integer cmp = letters1.compareToIgnoreCase(letters2);
				if (cmp < 0 || cmp > 0) {
					// result already found
					if (result == 0) {
						result = cmp;
					} else { // not comparable
						return null;						
					}
				}
				
				offset1 += letters1.length();
				offset2 += letters2.length();
			} else {
				// letter vs. number
				return null;				
			}
		}
		
	}

	/*public static List<Integer> getCategoriesByEvent(Integer eventId, String groupName) {
		
		Collection<CategoryMapping> catMaps = mappingMap.get(eventId + groupName);
		
		List<Integer> categoryIds = new ArrayList<Integer>();
		if(catMaps != null) {
			if(!catMaps.isEmpty()) {
				for(CategoryMapping map : catMaps){
					if(!categoryIds.contains(map.getCategoryId())){
						categoryIds.add(map.getCategoryId());
					}
				}
			}
		}
		
		return categoryIds;
	}*/
	
/*	public static List<String> getAllCategoryGroups() {
		Set<String> groupNames = new HashSet<String>();
		for(String key : mappingMap.keySet()) {
			String scheme = mappingMap.get(key).iterator().next().getCategoryGroupName();
			if (scheme == null || scheme.trim().isEmpty()) {
				continue;
			}
			groupNames.add(scheme);
		}
		if(groupNames.isEmpty()){
			groupNames.add("");
		}
		
		// FIXME: not sure why we do this
		return new ArrayList<String>(groupNames);
	}
*/
	/*public static List<String> getCategoryGroupsByEvent(Integer eventId) {
		List<String> groupNames = new ArrayList<String>();
		if(eventId != null){
			for(String key : mappingMap.keySet()) {
				if(key.contains(eventId.toString())) {
					groupNames.add(mappingMap.get(key).iterator().next().getCategoryGroupName());
				}
			}
		}
		// FIXME: not sure why we do this
		if(groupNames.isEmpty()){
			groupNames.add("");
		}
		return groupNames;
	}
*/
	/*public static List<String> getCategoryGroupsByTour(Integer tourId) {
		
		List<String> groupNames = new ArrayList<String>();
		Collection<Event> events = TMATDAORegistry.getEventDAO().getAllEventsByTourAndStatus(tourId, EventStatus.ACTIVE);
		if (events != null) { 
			for(Event event : events) {
				for(String key : mappingMap.keySet()) {
					if(key.contains(event.getId().toString())) {
						String groupName = mappingMap.get(key).iterator().next().getCategoryGroupName();
						if(!groupNames.contains(groupName)) {
							groupNames.add(groupName);
						}
					}
				}
			}
		}
		// FIXME: not sure why we do this
		if(groupNames.isEmpty()){
			groupNames.add("");
		}
		return groupNames;
	}
*/
	/*public static Category getCategoryByCatSection(String section, String row, Integer tourId, Integer eventId, String groupName) {	
		

		Category category = null;
		
		try{
	
			Collection<CategorySynonym> synonyms = TMATDAORegistry.getCategorySynonymDAO().getTourCategorySynonyms(tourId);
	
			if(synonyms == null) {
				System.out.println("Invalid category. "
						+ "The category for the symbol '"
						+ section + "' cannot be found. There are no "
						+ "synonyms for this category");
				return null;
			}
			
			List<Integer> categoryIds = getCategoriesByEvent(eventId, groupName);
			
			Iterator<CategorySynonym> syns = synonyms.iterator();
			
			Collection<CategoryMapping> list = mappingMap.get(eventId + groupName);
			if (list == null) {
				return null;
			}
			
			while(syns.hasNext() && category == null) {
				
				CategorySynonym syn = syns.next();
				category = syn.getCategory();
				
				//System.out.println("Checking Category: " + category.getDescription());	
				//System.out.println("Section: " + section);
				//System.out.println("range: " + syn.getStartSynonym() + "-" + syn.getEndSynonym());
	
				if(syn.getStartSynonym().equals(section)){
					for(CategoryMapping catMap : list){
						if(catMap.getCategoryId().equals(category.getId())) {
							Integer cmp1 = compare(catMap.getStartRow(), row);
							if (cmp1 == null) {
								continue;
							}
							Integer cmp2 = compare(row, catMap.getEndRow());
							if (cmp2 == null) {
								continue;
							}
							if(cmp1 <= 0 && cmp2 <= 0) {
								return category;
							}
						}
					}
				} else if(syn.getEndSynonym() != null && !syn.getEndSynonym().equals("")){
					Integer cmp1 = Categorizer.compare(section, syn.getStartSynonym());
					if (cmp1 == null) {
						continue;
					}
					Integer cmp2 = Categorizer.compare(syn.getEndSynonym(), section);
					if (cmp2 == null) {
						continue;
					}
					// System.out.println("cmp1: " + cmp1 + " cmp2: " + cmp2);
					if ((cmp1 > 0 && cmp2 >= 0) || (cmp1 < 0 && cmp2 <= 0)) {
						//if category is in this event's category mapping
						//if it is not that means it is for  different venue in this tour
						if(categoryIds.contains(category.getId())) {
							//System.out.println("Returning Category: " + category.getDescription());
							for(CategoryMapping catMap : list){
								if(catMap.getCategoryId().equals(category.getId())) {
									Integer cmp3 = compare(catMap.getStartRow(), row);
									Integer cmp4 = compare(row, catMap.getEndRow());
									if(cmp3 <= 0 && cmp4 <= 0) {
										return category;
									}
								}
							}
						}
					}
				}
				category = null;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
*/
	/**
	 * orderCatMaps - THIS METHOD MUTATES THE PARAMETER catMaps
	 * 
	 * This method will mutate the parameter List statuses and 
	 * order it by:
	 * a.) Worse(newer/lower numbered) category ids before better category ids
	 *
	 * @param catMaps - List of CatMaps to be ordered
	 * @return void 
	 */
	public static void orderCatMaps(List<CategoryMapping> catMaps){
		Collections.sort(catMaps, new Comparator<CategoryMapping>() {
			
			public int compare(CategoryMapping catMap1, CategoryMapping catMap2) {
				if(catMap1.getCategoryId() < catMap2.getCategoryId()) {
					return 1;
				} else if(catMap1.getCategoryId() > catMap2.getCategoryId()) {
					return -1;
				}
				return 0;
			}
		});
		// sort = "external";
	}
	
	public  static void fillValidSection(){
		String[] sectionGroup = "A:B,B:C,C:D,D:E,E:F,F:G,G:H,H:I,I:J,J:K,K:L,L:M,M:N,N:O,O:P,P:Q,Q:R,R:S,S:T,T:U,U:V,V:W,W:X,X:Y,Y:Z,Z:--,AA:BB,BB:CC,CC:DD,DD:EE,EE:FF,FF:GG,GG:HH,HH:II,II:JJ,JJ:KK,KK:LL,LL:MM,MM:NN,NN:OO,OO:PP,PP:QQ,QQ:RR,RR:SS,SS:TT,TT:UU,UU:VV,VV:WW,WW:XX,XX:YY,YY:ZZ,ZZ,--".split(",");
		for(String section:sectionGroup){
			if(section!=null && !section.isEmpty()){
				String[] keyValue = section.split(",");
				if(keyValue.length==2){
					validSection.put(keyValue[0], keyValue[1]);
				}
			}
		}
	}
	
	
public static Map<String, CategoryMapping> computeRowRangeForSectionsByVenueCategoryIdOne(Event event) throws Exception {
		
		Map<String, CategoryMapping> map = new HashMap<String, CategoryMapping>();
		Map<String,List<CategoryMapping>> csvCatMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(csvCatMap.keySet());
		
		for (String key : keyList) {
			
			if(key.equals("-")) {
				continue;
			}
			List<CategoryMapping> catmappingList = csvCatMap.get(key);
		
			for (CategoryMapping mapping : catmappingList) {
				if(mapping.getRowRange() != null && mapping.getRowRange().trim().length() > 0 ) {
					map.put(key,mapping);
				}
			}
		}
		return map;
	}

	public static void main(String[] args) throws Exception {
		String lastRow = "5";
		
		Scanner scr = new Scanner(System.in);
		while(lastRow!="01") {
			System.out.println("Enter Row : ");
			lastRow = scr.nextLine();
			
			System.out.println("lastRow : "+lastRow+" : "+getTNDZonesLastRow(lastRow));
			/*Map<Integer, String> map = getMiniLastRow(lastRow, 3);
			if(map != null && !map.isEmpty()) {
				for (int i=0;i<3;i++) {
					System.out.println("lastRow : "+lastRow+" : subtractCount : "+i+" : "+map.get(i));
				}
				
			}*/
		}
		
		
	}

	public static void main1(String[] args) throws Exception {
		
		
		
		  String section = "100-110";
		  String startSection ="100-150";
		  String endSection ="100-150";
		  CategoryMapping mapping = new CategoryMapping(1, startSection, endSection, "*", "*","*","*");
		  Integer i = computeCategory(1, section, "", "", mapping);
		  System.out.println(i);
		  mapping = new CategoryMapping(11, "100-110", "100-110", "*", "*","*","*");
		  i = computeCategory(1, section, "", "", mapping);
		  System.out.println(i);
		  mapping = new CategoryMapping(31, "A-F", "A-F", "*", "*","*","*");
		  i = computeCategory(1, "A-F", "", "", mapping);
		  System.out.println("3.."+i);
		  mapping = new CategoryMapping(31, "A-F", "A-F", "*", "*","*","*");
		  //i = computeCategory1(1, "B-C", "", "", mapping);
		  System.out.println("4.."+i);
		  
		 }
}
