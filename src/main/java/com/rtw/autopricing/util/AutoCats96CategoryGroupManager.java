package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Category;
import com.rtw.autopricing.ftp.data.CategoryMapping;
import com.rtw.autopricing.ftp.data.DefaultPurchasePrice;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.ManagePurchasePrice;
import com.rtw.autopricing.ftp.data.PresaleAutoCatCategoryTicket;
import com.rtw.autopricing.ftp.data.Ticket;


public class AutoCats96CategoryGroupManager {
	public static Logger logger = LoggerFactory.getLogger(AutoCats96CategoryGroupManager.class);
	static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	static DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	public static int PURCHASE_PRICE_THRESHHOLD = 10;
	public static int MAX_PUR_PRICE_THRESHOLD_PERCENTAGE = 200;
	
	
	public static Collection<Ticket> getTMATActiveTickets(Event event,AutopricingProduct product,Boolean isAllowSectionRange) throws Exception {
		
		List<Ticket> newFinalTicketList = new ArrayList<Ticket>();
		Collection<Ticket> finalTicketList = new ArrayList<Ticket>();
		Collection<Category> categoryList = null;
		
		Collection<Ticket> ticketList = CategoryGroupManager.getTMATActiveTicketsFromDB(event, product);
		
		try {
			
			categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
			ticketList = TicketUtil.removeDuplicateTickets(ticketList);
			ticketList = TicketUtil.removeAdmitoneTickets(ticketList, event, event.getAdmitoneId(),isAllowSectionRange);
			ticketList = TicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
			
			//Purchase tour = DAORegistry.getPurchaseDAO().getPurchaseByEventId(event.getId());
			Collection<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAll();
			Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
			for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
				defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
			}
			Collection<ManagePurchasePrice> managePurchasePricelist = DAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());//tour.getTmatPurchaseId());
			Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
			for(ManagePurchasePrice tourPrice:managePurchasePricelist){
				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
			}
			
			for (Ticket ticket : ticketList) {
				TicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);
				if(ticket.getPurchasePrice() < PURCHASE_PRICE_THRESHHOLD) {
					continue;
				}
				//Tamil : Added to skip tickets with quantity 2 and purchase price below 100 in auto pricing
				if(ticket.getQuantity().equals(2) && ticket.getPurchasePrice()< 100) {
					continue;
				}
				finalTicketList.add(ticket);
			}
			//Removing Fake Tickets by Maximum Purchase Price Threshold
			finalTicketList = TicketUtil.removeFakeTicketsByCategoryandSection(finalTicketList, MAX_PUR_PRICE_THRESHOLD_PERCENTAGE);
			
			
			for (Ticket ticket : finalTicketList) {
				//Tamil : for Broadway events process premium zone tickets only in all products
				/*if(event.getChildCategoryName().equalsIgnoreCase("Broadway") && 
					(ticket.getCategory() != null && ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().equalsIgnoreCase("premium"))) {
					continue;
				}*/
				newFinalTicketList.add(ticket);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + e.fillInStackTrace());
			System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
			throw e;
		}
		return newFinalTicketList;
	}
	
	public static Integer getExchangeEventMaxTixCount(Collection<Ticket> finalTicketList) throws Exception {
		Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
		Set<Ticket> sectionTickets = null;
		
		for (Ticket ticket : finalTicketList) {
			String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
			sectionTickets = sectionTicketMap.get(section);
			if(null == sectionTickets) {
				sectionTickets = new HashSet<Ticket>();
				sectionTicketMap.put(section, sectionTickets);
			}
			sectionTickets.add(ticket);
		}
		return sectionTicketMap.size();
	}
	
	
	public static List<AutoCats96CategoryTicket> computeCategoryTickets(Event event,ExchangeEvent exEvent,Collection<Ticket> finalTicketList,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,AutopricingProduct product) throws Exception {
		List<AutoCats96CategoryTicket> catTixList = new ArrayList<AutoCats96CategoryTicket>();
		//Map<Integer, List<AutoCats96CategoryTicket>> qtyCatTixMap = new HashMap<Integer, List<AutoCats96CategoryTicket>>();
		try{
			
			Integer minEligibleSections = Integer.parseInt(exEvent.getExposure().split("-")[0]);
			if(minEligibleSections > 2){
				minEligibleSections = 2;
			}
			sectionEligibleMinEntries = minEligibleSections;
			
			//Process Lastrow lisitngs only. TN not allowing sections or row with ranges
			/*if(null != exEvent.getMinicatsEnabled() && exEvent.getMinicatsEnabled()){
				getSectionBasedCheapestMiniCatsForBelow96Cats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList);
			}*/
			
			if(exEvent.getIsPresale()) {
				if(null != exEvent.getPresaleAutocatsEnabled() && exEvent.getPresaleAutocatsEnabled()){
					//Tamil : Do not consider section range tickets for ticketnetwork
					//getPresaleAutoCatCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						//	vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList);
				}
			} else {
				if(null != exEvent.getLastrowMinicatsEnabled() && exEvent.getLastrowMinicatsEnabled()){
					getLastRowMiniCatCategoryTicketsForBelow96Cats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
							vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList,product);
				}
				
				//Tamil : stop VIP Lastrow for TN as per Amit on 09/19/2017
				/*if(null != exEvent.getVipLastrowMinicatsEnabled() && exEvent.getVipLastrowMinicatsEnabled()){
					getVipLastRowMiniCatCategoryTicketsForBelow96Cats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
							vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList,product);
				}*/
				/*
				if(null != exEvent.getVipMinicatsEnabled() && exEvent.getVipMinicatsEnabled()){
					getSectionBasedCheapestVipCategoryTicketsForBelow96Cats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList,product);
				
				}*/	
			}
			
			//System.out.println("TOTAL CATEGORY TICKETS : "+catTixList.size());
			 
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + e.fillInStackTrace());
			System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
			throw e;
		}
		return catTixList;
	}
	
	public static Map<Integer, List<AutoCats96CategoryTicket>> computeCategoryTicketsNew(Event event,ExchangeEvent exEvent,Collection<Ticket> finalTicketList,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,AutopricingProduct product,boolean isAbove96Cats) throws Exception {
		
		Map<Integer, List<AutoCats96CategoryTicket>> qtyCatTixMap = new HashMap<Integer, List<AutoCats96CategoryTicket>>();
		try{
			List<AutoCats96CategoryTicket> catTixList = new ArrayList<AutoCats96CategoryTicket>();
			
			
			if(null != exEvent.getMinicatsEnabled() && exEvent.getMinicatsEnabled()){
				getSectionBasedCheapestMiniCatsForBelow96Cats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList);
			}
			
			if(null != exEvent.getLastrowMinicatsEnabled() && exEvent.getLastrowMinicatsEnabled()){
				getLastRowMiniCatCategoryTicketsForBelow96Cats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList,product);
			}
			
			if(null != exEvent.getVipMinicatsEnabled() && exEvent.getVipMinicatsEnabled()){
				getSectionBasedCheapestVipCategoryTicketsForBelow96Cats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,finalTicketList,catTixList,product);
				
			}
			
			
			System.out.println("TOTAL CATEGORY TICKETS : "+catTixList.size());
			 
			String priceHistory="";
			String ticketIdHistory= "";
			String baseTicketOneHistory= "";
			String baseTicketTwoHistory="";
			String baseTicketThreeHistory="";
			String ticketHDateStr = dateTimeFormat.format(new Date());
			String priceHDateStr = dateTimeFormat.format(new Date());
			
			for (AutoCats96CategoryTicket tix : catTixList) {
				priceHistory = priceHDateStr+"-"+tix.getActualPrice();
				tix.setPriceHistory(priceHistory);
				
				ticketIdHistory = ticketHDateStr +"/"+ tix.getTicketId() +"/"+ tix.getPurPrice();
				tix.setTicketIdHistory(ticketIdHistory);
				
				if(null != tix.getBaseTicketOne() && tix.getBaseTicketOne() > 0){
					baseTicketOneHistory = ticketHDateStr +"/"+ tix.getBaseTicketOne() +"/"+ tix.getBaseTicketOnePurPrice();
					tix.setBaseTicketOneHistory(baseTicketOneHistory);
				}
				
				if(null != tix.getBaseTicketTwo() && tix.getBaseTicketTwo() > 0){
					baseTicketTwoHistory = ticketHDateStr +"/"+ tix.getBaseTicketTwo() +"/"+ tix.getBaseTicketTwoPurPrice();
					tix.setBaseTicketTwoHistory(baseTicketTwoHistory);
				}
				
				if(null != tix.getBaseTicketThree() && tix.getBaseTicketThree() > 0){
					baseTicketThreeHistory = ticketHDateStr +"/"+ tix.getBaseTicketThree() +"/"+ tix.getBaseTicketThreePurPrice();
					tix.setBaseTicketThreeHistory(baseTicketThreeHistory);
				}
				
				List<AutoCats96CategoryTicket> categoryTickets = qtyCatTixMap.get(tix.getQuantity());
				
				if(null != categoryTickets && !categoryTickets.isEmpty()){
					
					qtyCatTixMap.get(tix.getQuantity()).add(tix);
				}else{
					categoryTickets = new ArrayList<AutoCats96CategoryTicket>();
					categoryTickets.add(tix);
					qtyCatTixMap.put(tix.getQuantity(), categoryTickets);
				}
			}
			
			/*System.out.println("BEGINS");
			for (Integer key : qtyCatTixMap.keySet()) {
				
				List<AutoCats96CategoryTicket> categoryTickets = qtyCatTixMap.get(key);
				
				for (AutoCats96CategoryTicket tix : categoryTickets) {
					System.out.println("QTY :"+tix.getQuantity()+"===> Section :"+tix.getSection()+",row :"+tix.getCatRow()+", category :"+tix.getTmatCategory()+", " +
							"Price :"+tix.getTnPrice()+", InternalNotes :"+tix.getInternalNotes());
				}
				
				
				System.out.println("============================================================================================================");
			}
			System.out.println("DONE");*/
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + e.fillInStackTrace());
			System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
			throw e;
		}
		return qtyCatTixMap;
	}
	
	public static CategoryTicket computecategoryTicket(Set<Ticket> sectionTickets,Event event,ExchangeEvent exEvent,int quantity,
			int sectionEligibleMinEntries,Date expectedArrivalDate,double tnExAddMarkup,double vividExAddMarkup,double tickPickExAddMarkup,
			double scoreBigExAddMarkup,double fanxchangeAddMarkup,double ticketcityAddMarkup,CategoryTicket catTicket) throws Exception {
		
		
		TicketUtil.getAuotCats96CheapestTicketByQty(sectionTickets, exEvent, quantity, sectionEligibleMinEntries,catTicket);
		
		/*if(isabove96Cats){
			TicketUtil.getAuotCats96CheapestTicketByQty(sectionTickets, exEvent, quantity, sectionEligibleMinEntries,catTicket,ignoreSections);
		}else{
			TicketUtil.getCheapestTicketByQty(sectionTickets, exEvent, quantity, sectionEligibleMinEntries,catTicket);
		}*/
		
		if(catTicket.getPurPrice()!=null) {
			catTicket.setQuantity(quantity);
			catTicket.setPosExchangeEventId(event.getAdmitoneId());
			catTicket.setExpectedArrivalDate(expectedArrivalDate);
			catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
			//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
			computecategoryTicketPrices(catTicket, exEvent, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup);
		} else {
			catTicket = null;
		}
		return catTicket;
	}
	
	public static void computecategoryTicketPrices(CategoryTicket catTicket,ExchangeEvent exEvent,double tnExAddMarkup,
			double vividExAddMarkup,double tickPickExAddMarkup,double scoreBigExAddMarkup,double fanxchangeAddMarkup,double ticketcityAddMarkup) throws Exception {
		
		
			double roundedPrice;
			double eventMarkup,eventShippingFees;
			
			if(catTicket.getPurPrice() >= exEvent.getPriceBreakup()) {
				eventMarkup = exEvent.getUpperMarkup();
				eventShippingFees = exEvent.getUpperShippingFees();
			} else {
				eventMarkup = exEvent.getLowerMarkup();
				eventShippingFees = exEvent.getLowerShippingFees();
			}
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setActualPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setZoneTicketPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+tnExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setTnPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+vividExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setVividPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+tickPickExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setTickpickPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+scoreBigExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setScoreBigPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+fanxchangeAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setFanxchangePrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+ticketcityAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setTicketcityPrice(roundedPrice);
			
	}
	

	
	public static List<AutoCats96CategoryTicket> getSectionBasedCheapestMiniCatsForAbove96Cats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap,
			List<AutoCats96CategoryTicket> catTixList,List<String> ignoreSections,boolean isAbove96Cats) throws Exception {
		
		if(exEvent.getAllowSectionRange()) {
			sectionTicketMap = TicketUtil.getSectionRangeTicketsFromUniqueSections(sectionTicketMap);
		}
		Map<String,CategoryMapping> rowRangeMap = Categorizer.computeRowRangeForSectionsByVenueCategoryIdOne(event);
		Set<Ticket> sectionTickets = null;
		Date expectedArrivalDate = null;
		if(event.getLocalDate()!=null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(event.getLocalDate());
			cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
			expectedArrivalDate = cal.getTime();
		}
		
		for (String  key : sectionTicketMap.keySet()) {
			
			sectionTickets = sectionTicketMap.get(key);
				
			for (int i = 1; i <= 10; i++) {
				
				if(i ==2 || i== 4 || i== 6 || i== 8 || i== 10 || i== 3 || i== 5 || i== 7 ) {
					
					int quantity = i;
					AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
					
					catTicket = (AutoCats96CategoryTicket) computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries,
							expectedArrivalDate,tnExAddMarkup,vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
					
					if(catTicket != null) {
						
						if(quantity == 2 && catTicket.getTnPrice() < 50){
							continue;
						}
						catTicket.setProductPriority(1);
						catTicket.setQtyPriority(getQtyPriority(quantity));
						catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
						catTicket.setQuantity(quantity);
						catTicket.setTnExchangeEventId(event.getAdmitoneId());
						catTicket = computeRowRange(catTicket, rowRangeMap);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
						catTicket.setInternalNotes("MINICATS");
						if(catTicket.getRowRange().isEmpty() || catTicket.getRowRange().equals("")){
							continue;
						}
						catTicket.setCatRow(catTicket.getRowRange());
						catTixList.add(catTicket);
						ignoreSections.add(catTicket.getSection());
					}
				}
			}
		}
		

		return catTixList;
	}
	
	public static List<AutoCats96CategoryTicket> getLastRowMiniCatCategoryTicketsForAbove96Cats(Event event,ExchangeEvent exEvent,
			Collection<Ticket> finalTicketList,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,
			List<AutoCats96CategoryTicket> catTixList,AutopricingProduct product,List<String> toSkipSections,boolean isAbove96Cats) throws Exception {

		try{
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			
			Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
			
			for (Ticket ticket : finalTicketList) {
				
				if(null == ticket.getCategory() ){
					continue;
				}
				String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
				Set<Ticket> sectionTickets = sectionTicketMap.get(section);
				if(null == sectionTickets) {
					sectionTickets = new HashSet<Ticket>(); 
					sectionTicketMap.put(section, sectionTickets);
				}
				sectionTickets.add(ticket);
			}
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			Set<Ticket> eligibleFinalTickets = new HashSet<Ticket>();
			
			for (String  key : sectionTicketMap.keySet()) {
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						eligibleFinalTickets.add(ticket);
					}
				}
			}
			
			Map<String, Set<Ticket>> zoneTicketMap = new HashMap<String, Set<Ticket>>();
			
			for (Ticket ticket : eligibleFinalTickets) {
				
				String section = ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().toLowerCase();
				Set<Ticket> sectionTickets = zoneTicketMap.get(section);
				
				if(null == sectionTickets) {
					sectionTickets = new HashSet<Ticket>(); 
					zoneTicketMap.put(section, sectionTickets);
				}
				sectionTickets.add(ticket);
			}
			
			for (String  key : zoneTicketMap.keySet()) {
				
				Set<Ticket> sectionTickets = zoneTicketMap.get(key);
				
				for (int i = 1; i <= 10; i++) {
					if(i ==2 || i== 4 || i== 6 || i== 8 || i== 10 || i== 3 || i== 5 || i== 7 ) {
						int quantity = i;
						AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
						
						catTicket = (AutoCats96CategoryTicket)computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries,expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
						
						if(catTicket != null) {
							catTicket = computeLastRow(eligibleRowListMap, catTicket);
							if(catTicket.getLastRow().isEmpty() || catTicket.getLastRow().equals("")){
								continue;
							}
							if(catTicket.getTnPrice() < 50 && quantity == 2){
								continue;
							}
							catTicket.setProductPriority(2);
							catTicket.setQtyPriority(getQtyPriority(quantity));
							catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
							catTicket.setQuantity(quantity);
							catTicket.setTnExchangeEventId(event.getAdmitoneId());
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							catTicket.setInternalNotes("LASTROW MINICATS");
							catTicket.setCatRow(catTicket.getLastRow());
							catTixList.add(catTicket);	
							toSkipSections.add(catTicket.getSection());
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
	
	public static List<AutoCats96CategoryTicket> getSectionBasedCheapestVipCategoryTicketsForAbove96Cats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<AutoCats96CategoryTicket> catTixList,AutopricingProduct product,List<String> toSkipSections,boolean isAbove96Cats) throws Exception {
		
		
		Date expectedArrivalDate = null;
		if(event.getLocalDate()!=null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(event.getLocalDate());
			cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
			expectedArrivalDate = cal.getTime();
		}
		Map<String ,Map<String,CategoryMapping>> alternateRowMap = TicketUtil.getSectionBasedAlternateRows(event);
		
		Map<String,Set<String>> zoneUniqueSectionMap = new HashMap<String, Set<String>>();
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<String> uniqueZoneSection = new HashSet<String>();
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			for (Ticket ticket : sectionTickets) {
				uniqueZoneSection.add(ticket.getNormalizedSection().toUpperCase());
			}
			zoneUniqueSectionMap.put(key, uniqueZoneSection);
		}
		
		for (String  key : sectionTicketMap.keySet()) {
			
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			Set<Ticket> rowTicketList =  new HashSet<Ticket>();
			Set<String> uniqueZoneSection = zoneUniqueSectionMap.get(key);
			Map<String ,Map<String,CategoryMapping>> alternateRowMapBySection = new HashMap<String, Map<String,CategoryMapping>>();
			
			for (String section : uniqueZoneSection) {
				
				
				Map<String,CategoryMapping> rowMap = alternateRowMap.get(section);
				
				
				if(rowMap == null ||  rowMap.isEmpty()) {
					continue;	
				}
				for (String  alRow : rowMap.keySet()) {
					
					CategoryMapping categoryMapping = rowMap.get(alRow);
					if(categoryMapping == null) {
						continue;
					}
					List<String>  rowList = categoryMapping.getRowList();
					if(rowList == null ||  rowList.isEmpty()) {
						continue;
					}
					for (Ticket ticket : sectionTickets) {
						if(ticket.getRow()!= null && rowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
							rowTicketList.add(ticket);
						}
					}
				}
				alternateRowMapBySection.put(section.toUpperCase(), rowMap);
			}
			
			for (int i = 1; i <= 10; i++) {
				
				if( i ==2 || i== 4 || i== 6 || i== 8 || i== 10 || i== 3 || i== 5 || i== 7 ) {
					
					int quantity = i;
					
					AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
					catTicket = (AutoCats96CategoryTicket) computecategoryTicket(rowTicketList, event, exEvent, quantity, sectionEligibleMinEntries,expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
					
					if(catTicket != null) {
						
						if(catTicket.getTnPrice() < 50 && quantity ==2 ){
							continue;
						}
						
						String alternateRow =getVipMiniCatsAlternateRow(alternateRowMapBySection, catTicket);
						if(null == alternateRow || alternateRow.isEmpty()){
							continue;
						}
						catTicket.setProductPriority(3);
						catTicket.setQtyPriority(getQtyPriority(quantity));
						catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
						catTicket.setQuantity(quantity);
						catTicket.setTnExchangeEventId(event.getAdmitoneId());
						catTicket.setAlternateRow(alternateRow);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
						catTicket.setInternalNotes("VIPMINICATS");
						catTicket.setCatRow(catTicket.getAlternateRow());
						catTixList.add(catTicket);
						toSkipSections.add(catTicket.getSection());
					}
				}
			}
		}
		return catTixList;
	}
	
	public static String getVipMiniCatsAlternateRow(Map<String ,Map<String,CategoryMapping>> alternateRowMapBySection,CategoryTicket catTicket) throws Exception{
		
		Map<String,CategoryMapping> rowMap = alternateRowMapBySection.get(catTicket.getSection());
		
		if(null ==rowMap || rowMap.isEmpty()){
			return "";
		}
		
		int size = rowMap.values().size(),i=1;
		String alternateRow = "";
		
		for(CategoryMapping catMapping : rowMap.values()){
			
			if(size == i){
				alternateRow = catMapping.getAlternateRow();
			}
			i++;
		}
		return alternateRow;
	}
	
	
	
	public static Integer getQtyPriority(Integer qty){
		Integer priority = 1;
		
		switch (qty) {
			
			case 2:
				priority = 2;
				break;
			case 3:
				priority = 6;
				break;
			case 4:
				priority = 1;
				break;
			case 5:
				priority = 7;
				break;
			case 6:
				priority = 3;
				break;
			case 7:
				priority = 8;
				break;
			case 8:
				priority = 4;
				break;
			case 9:
				priority = 9;
				break;
			case 10:
				priority = 5;
				break;
			default:
				break;
		}
		return priority;
	}
	
	
	public static Integer getAutoCatsPriority(int qty,Integer productPriority){
		Integer autoCatsPriority = 1;
		
		switch (qty) {
			
			case 4:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 1;
						break;
					case 2:
						autoCatsPriority = 3;
						break;
					case 3:
						autoCatsPriority = 5;
						break;
	
					default:
						break;
					}
				break;
				
			case 2:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 2;
						break;
					case 2:
						autoCatsPriority = 4;
						break;
					case 3:
						autoCatsPriority = 6;
						break;
					default:
						break;
					}
				break;
				
			case 6:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 7;
						break;
					case 2:
						autoCatsPriority = 8;
						break;
					case 3:
						autoCatsPriority = 9;
						break;
					default:
						break;
					}
				break;
				
			case 8:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 10;
						break;
					case 2:
						autoCatsPriority = 11;
						break;
					case 3:
						autoCatsPriority = 12;
						break;
					default:
						break;
					}
				break;
				
			case 10:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 13;
						break;
					case 2:
						autoCatsPriority = 14;
						break;
					case 3:
						autoCatsPriority = 15;
						break;
					default:
						break;
					}
				break;
				
			case 3:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 16;
						break;
					case 2:
						autoCatsPriority = 17;
						break;
					case 3:
						autoCatsPriority = 18;
						break;
					default:
						break;
					}
				break;
				
				
			case 5:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 19;
						break;
					case 2:
						autoCatsPriority = 20;
						break;
					case 3:
						autoCatsPriority = 21;
						break;
					default:
						break;
					}
				break;
				
			case 7:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 22;
						break;
					case 2:
						autoCatsPriority = 23;
						break;
					case 3:
						autoCatsPriority = 24;
						break;
					default:
						break;
					}
				break;
				
			case 9:
				switch (productPriority) {
					case 1:
						autoCatsPriority = 25;
						break;
					case 2:
						autoCatsPriority = 26;
						break;
					case 3:
						autoCatsPriority = 27;
						break;
					default:
						break;
					}
				break;
			
			default:
				break;
		}
		
		return autoCatsPriority;
	}
	
	public static AutoCats96CategoryTicket computeRowRange(AutoCats96CategoryTicket catTicket,Map<String,CategoryMapping> rowRangeMap)throws Exception{
		
		
		CategoryMapping categoryMapping = null;
		categoryMapping = rowRangeMap.get(catTicket.getSection());
		String rowRange = "",lastRow = ""; 
		if(categoryMapping != null) {
			rowRange = categoryMapping.getRowRange();
			lastRow = categoryMapping.getLastRow();
			if(lastRow == null) {
				lastRow = "";
			}
		}
		catTicket.setRowRange(rowRange);
		catTicket.setLastRow(lastRow);
		return catTicket;
	}
	
	public static AutoCats96CategoryTicket computeLastRow(Map<String,CategoryMapping> eligibleRowListMap,AutoCats96CategoryTicket categoryTicket)throws Exception{
		CategoryMapping categoryMapping = null;
		categoryMapping = eligibleRowListMap.get(categoryTicket.getNormalizedSection());
		String rowRange ="",lastRow="",miniLastRow="";
		
		if(categoryMapping != null) {
			 rowRange = categoryMapping.getRowRange();
			 lastRow = categoryMapping.getLastRow();
			 miniLastRow = categoryMapping.getMiniLastRow();
		}
		if(rowRange == null) {
			rowRange = "";
		}
		categoryTicket.setLastRow(miniLastRow);
		categoryTicket.setRowRange(rowRange);
		return categoryTicket;
	}
	
	
	public static List<AutoCats96CategoryTicket> getSectionBasedCheapestMiniCatsForBelow96Cats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,
			Collection<Ticket> finalTicketList, List<AutoCats96CategoryTicket> catTixList) throws Exception {

		int eventLevelsectionEligibleCount = sectionEligibleMinEntries;
		
		Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
		for (Ticket ticket : finalTicketList) {
			
			String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
			Set<Ticket> sectionTickets = sectionTicketMap.get(section);
			
			if(null == sectionTickets) {
				sectionTickets = new HashSet<Ticket>();
				sectionTicketMap.put(section, sectionTickets);
			}
			sectionTickets.add(ticket);
		}
		
//		Date now = new Date();
//		now = df.parse(df.format(now));
		
		if(exEvent.getAllowSectionRange()) {
			sectionTicketMap = TicketUtil.getSectionRangeTicketsFromUniqueSections(sectionTicketMap);
		}
		
		Map<String,CategoryMapping> rowRangeMap = Categorizer.computeRowRangeForSectionsByVenueCategoryIdOne(event);
		CategoryMapping categoryMapping = null;
		
		Date expectedArrivalDate = null;
		if(event.getLocalDate()!=null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(event.getLocalDate());
			cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
			expectedArrivalDate = cal.getTime();
		}
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			
			categoryMapping = rowRangeMap.get(key);
			String rowRange = "",lastRow = ""; 
			
			if(categoryMapping != null) {
				rowRange = categoryMapping.getRowRange();
				lastRow = categoryMapping.getLastRow();
				
				if(lastRow == null) {
					lastRow = "";
				}
			}
			
			if(exEvent.getAllowSectionRange() && key.contains("-")) {
				sectionEligibleMinEntries = 1;
//				isAllowRangeSection = true;
			} else {
				sectionEligibleMinEntries = eventLevelsectionEligibleCount;
//				isAllowRangeSection = false;
			}
			
			if(rowRange.equals("")) {
				if(exEvent.getAllowSectionRange() && key.contains("-")) {
					Iterator<Ticket> iterator = sectionTickets.iterator();
					Ticket tix = null;
						
					while(iterator.hasNext()) {
						tix = iterator.next();
						if(key.equals(tix.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase())) {
							break;
						}
					}
						
					if(null != tix && tix.getCategoryMapping() != null) {
						categoryMapping = rowRangeMap.get(tix.getCategoryMapping().getStartSection().replaceAll("\\s+", " ").trim().toLowerCase());	
					}
					if(categoryMapping != null) {
						rowRange = categoryMapping.getRowRange();
						lastRow = categoryMapping.getLastRow();
							
						if(lastRow == null) {
							lastRow = "";
						}
					}
					if(rowRange.equals("")) {
						continue;
					}
				} else {
					continue;
				}
			}
				
			for (int i = 1; i <= 10; i++) {
				//Tamil Process 2 and 4 quantities only to TN
				if(i ==2 || i== 4 ) {//|| i== 8 || i== 3 || i== 6  
					int quantity = i;
					AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
					catTicket= (AutoCats96CategoryTicket) computecategoryTicket(sectionTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
					
					if(catTicket != null) {
						catTicket.setQuantity(quantity);
						//catTicket.setQtyPriority(getQtyPriority(quantity));
						catTicket.setProductPriority(3);
						//catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
						catTicket.setInternalNotes("MINICATS");
						catTicket.setCatRow(rowRange);
						catTicket.setPosExchangeEventId(event.getAdmitoneId());
						//catTicket.setRowRange(rowRange);
						//catTicket.setLastRow(lastRow);
						catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
						catTixList.add(catTicket);
					}
				}
			}
		}
		return catTixList;
	}

	public static List<AutoCats96CategoryTicket> getLastRowMiniCatCategoryTicketsForBelow96Cats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Collection<Ticket> finalTicketList, 
			List<AutoCats96CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		try{
			
			Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
			for (Ticket ticket : finalTicketList) {
				
				String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
				Set<Ticket> sectionTickets = sectionTicketMap.get(section);
				
				if(null == sectionTickets) {
					sectionTickets = new HashSet<Ticket>();
					sectionTicketMap.put(section, sectionTickets);
				}
				sectionTickets.add(ticket);
			}
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			String eventExposure = exEvent.getExposure();
			Integer eventSectionEligibleMinEntries = sectionEligibleMinEntries;
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				//String rowRange = categoryMapping.getRowRange();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				//if(rowRange == null) {
				//	rowRange = "";
				//}
				
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				Set<Ticket> sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				
				Map<Integer,AutoCats96CategoryTicket> catsMap = new HashMap<Integer, AutoCats96CategoryTicket>();
				for (int i = 1; i <= 10; i++) {
					//Tamil Process 2 and 4 quantities only to TN
					if(i ==2 || i== 4 || i== 6) {//|| i== 8 || i== 3   
						int quantity = i;
						
						AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
						
						exEvent.setExposure(eventExposure);
						sectionEligibleMinEntries=eventSectionEligibleMinEntries;
						
						catTicket = (AutoCats96CategoryTicket)computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
						
						if(catTicket != null) {
							catTicket.setQuantity(quantity);
							//catTicket.setQtyPriority(getQtyPriority(quantity));
							catTicket.setProductPriority(4);
							//catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
							catTicket.setInternalNotes("LASTROW MINICATS");
							catTicket.setCatRow(miniLastRow);
							//catTicket.setLastRow(miniLastRow);
							//catTicket.setRowRange(rowRange);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							
							//catTixList.add(catTicket);
							catsMap.put(quantity, catTicket);
						}
					}
				}
				
				/*
				 * Compute tickets for lastfive rows from lastrow of each sections
				 * 6qty for last row
				 * 4 qty for last 2nd row
				 * 2 quantity for last 3rd,4th and 5th rows
				 * if ther is no 4 or qty then consider 2 qty listing for all five rows
				 * for last 2nd row computation remove last row listings and check min price from last2nd row if no tix then use last row tix
				 * for lease row comutations use exposure as 1-OXP and mintixCount to 1
				 * 
				 * if lastrow is like GA or BOX ,etc then consider TMAT tickets with row as lastrow only.
				 * consider cats listings row as csv lastorw value.
				 * create listings for qty 2 and 4 with same lastrow value.
				 * if minlastorw list is null then consider that row is like GA or BOX type row
				*/
				
				if (!catsMap.isEmpty()) {
					int size = 5;
					Map<Integer,String> minLastRowMap = Categorizer.getMiniLastRow(miniLastRow,size);
					
					if(!minLastRowMap.isEmpty()) {
						int subtractRowCount = 0;
						String tempRow = null;
						int quantity = 6;
						Set<Ticket> qtyTempSectionTickets = new HashSet<Ticket>(sectionTickets);
						for(int i=0;i<5;i++) {
							AutoCats96CategoryTicket catTicket = null;
							
								if(tempRow != null) {
									eligibleRowList.remove(tempRow.toLowerCase());
									
									
									Set<Ticket> qtySectionTickets = new HashSet<Ticket>();
									for (Ticket ticket : qtyTempSectionTickets) {
										if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
											qtySectionTickets.add(ticket);
										} 
									}
									exEvent.setExposure("1-OXP");
									sectionEligibleMinEntries=1;
									
									qtyTempSectionTickets = new HashSet<Ticket>(qtySectionTickets);
									catTicket = getLastRowMinicatCategoryTicket(event, exEvent, expectedArrivalDate, sectionEligibleMinEntries,
											tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup, fanxchangeAddMarkup, ticketcityAddMarkup,
											qtySectionTickets, product, quantity, miniLastRow);
									
									if(catTicket != null) {
										catsMap.put(quantity, catTicket);
									}
								} 
								
								
								if(catTicket == null) {
									while(true) {
										catTicket = catsMap.get(quantity);
										if(quantity==6) {
											quantity=4;
										} else if(quantity == 4) {
											quantity =2;
										} else if(quantity == 2) {
											break;
										}
										if(catTicket != null) {
											catTicket = new AutoCats96CategoryTicket(catTicket);
											break;
										}
									}
								} else {
									if(quantity==6) {
										quantity=4;
									} else if(quantity == 4) {
										quantity =2;
									}
								}
								
								tempRow = minLastRowMap.get(subtractRowCount);
								if(catTicket != null && tempRow != null) {
									catTicket.setCatRow(tempRow.toUpperCase());
									catTixList.add(catTicket);
									subtractRowCount++;
								} else {
									tempRow = null;
								}
						}
					} else {
						AutoCats96CategoryTicket catTicket = catsMap.get(4);
						if(catTicket != null) {
							catTixList.add(catTicket);
						}
						catTicket = catsMap.get(2);
						if(catTicket != null) {
							catTixList.add(catTicket);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return catTixList;
	}
	public static AutoCats96CategoryTicket getLastRowMinicatCategoryTicket(Event event,ExchangeEvent exEvent,
			Date expectedArrivalDate,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,
			Set<Ticket> sectionTickets,AutopricingProduct product,int quantity,String miniLastRow) throws Exception {
		
		AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
		
		catTicket = (AutoCats96CategoryTicket)computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
				tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
		
		if(catTicket != null) {
			catTicket.setQuantity(quantity);
			//catTicket.setQtyPriority(getQtyPriority(quantity));
			catTicket.setProductPriority(4);
			//catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
			catTicket.setInternalNotes("LASTROW MINICATS");
			catTicket.setCatRow(miniLastRow);
			//catTicket.setLastRow(miniLastRow);
			//catTicket.setRowRange(rowRange);
			catTicket.setPosExchangeEventId(event.getAdmitoneId());
			catTicket.setExpectedArrivalDate(expectedArrivalDate);
			catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
			//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
			
			//catTixList.add(catTicket);
			//catsMap.put(quantity, catTicket);
			return catTicket;
		}
		return null;
	}
	
	public static List<AutoCats96CategoryTicket> getVipLastRowMiniCatCategoryTicketsForBelow96Cats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Collection<Ticket> finalTicketList, 
			List<AutoCats96CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		try{
			
			Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
			for (Ticket ticket : finalTicketList) {
				
				String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
				Set<Ticket> sectionTickets = sectionTicketMap.get(section);
				
				if(null == sectionTickets) {
					sectionTickets = new HashSet<Ticket>();
					sectionTicketMap.put(section, sectionTickets);
				}
				sectionTickets.add(ticket);
			}
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			String eventExposure = exEvent.getExposure();
			Integer eventSectionEligibleMinEntries = sectionEligibleMinEntries;
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getVipLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromVipLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniVipLastEligibleRows();
				String rowRange = categoryMapping.getRowRange();
				String miniVipLastRow = categoryMapping.getMiniVipLastRow();
					
				if(rowRange == null) {
					rowRange = "";
				}
				
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				Set<Ticket> sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				
				Map<Integer,AutoCats96CategoryTicket> catsMap = new HashMap<Integer, AutoCats96CategoryTicket>();
				for (int i = 1; i <= 10; i++) {
					//Tamil Process 2 and 4 quantities only to TN
					if(i ==2 || i== 4) {//|| i== 8 || i== 3  || i== 6  
						int quantity = i;
						
						exEvent.setExposure(eventExposure);
						sectionEligibleMinEntries=eventSectionEligibleMinEntries;
						
						AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
						
						
						catTicket = (AutoCats96CategoryTicket)computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
						
						if(catTicket != null) {
							catTicket.setQuantity(quantity);
							//catTicket.setQtyPriority(getQtyPriority(quantity));
							catTicket.setProductPriority(2);
							//catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
							catTicket.setInternalNotes("VIPLR");
							catTicket.setCatRow(miniVipLastRow);
							//catTicket.setLastRow(miniLastRow);
							//catTicket.setRowRange(rowRange);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							//catTixList.add(catTicket);
							catsMap.put(quantity, catTicket);
						}
					}
				}
				if (!catsMap.isEmpty()) {
					int size = 2;
					Map<Integer,String> vipMinLastRowMap = Categorizer.getVipMiniLastRow(miniVipLastRow,size);
					
					int subtractRowCount = 0;
					String tempRow = null;
					int quantity = 4;
					Set<Ticket> qtyTempSectionTickets = new HashSet<Ticket>(sectionTickets);
					for(int i=0;i<2;i++) {
						AutoCats96CategoryTicket catTicket = null;
						
							if(tempRow != null) {
								eligibleRowList.remove(tempRow.toLowerCase());
								
								
								Set<Ticket> qtySectionTickets = new HashSet<Ticket>();
								for (Ticket ticket : qtyTempSectionTickets) {
									if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
										qtySectionTickets.add(ticket);
									} 
								}
								exEvent.setExposure("1-OXP");
								sectionEligibleMinEntries=1;
								
								qtyTempSectionTickets = new HashSet<Ticket>(qtySectionTickets);
								catTicket = new AutoCats96CategoryTicket();
								
								catTicket = (AutoCats96CategoryTicket)computecategoryTicket(qtyTempSectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
										tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
								
								if(catTicket != null) {
									catTicket.setQuantity(quantity);
									//catTicket.setQtyPriority(getQtyPriority(quantity));
									catTicket.setProductPriority(2);
									//catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
									catTicket.setInternalNotes("VIPLR");
									catTicket.setCatRow(miniVipLastRow);
									//catTicket.setLastRow(miniLastRow);
									//catTicket.setRowRange(rowRange);
									catTicket.setPosExchangeEventId(event.getAdmitoneId());
									catTicket.setExpectedArrivalDate(expectedArrivalDate);
									catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
									//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
									catTixList.add(catTicket);
								}
							} 
							
							
							if(catTicket == null) {
								while(true) {
									catTicket = catsMap.get(quantity);
									if(quantity==6) {
										quantity=4;
									} else if(quantity == 4) {
										quantity =2;
									} else if(quantity == 2) {
										break;
									}
									if(catTicket != null) {
										catTicket = new AutoCats96CategoryTicket(catTicket);
										break;
									}
								}
							} else {
								if(quantity==6) {
									quantity=4;
								} else if(quantity == 4) {
									quantity =2;
								}
							}
							
							tempRow = vipMinLastRowMap.get(subtractRowCount);
							if(catTicket != null && tempRow != null) {
								catTicket.setCatRow(tempRow.toUpperCase());
								catTixList.add(catTicket);
								subtractRowCount++;
							} else {
								tempRow = null;
							}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
	
	public static List<AutoCats96CategoryTicket> getSectionBasedCheapestVipCategoryTicketsForBelow96Cats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,
			Collection<Ticket> finalTicketList, List<AutoCats96CategoryTicket> catTixList,AutopricingProduct product) throws Exception {
		
		
		Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
		for (Ticket ticket : finalTicketList) {
			
			String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
			Set<Ticket> sectionTickets = sectionTicketMap.get(section);
			
			if(null == sectionTickets) {
				sectionTickets = new HashSet<Ticket>();
				sectionTicketMap.put(section, sectionTickets);
			}
			sectionTickets.add(ticket);
		}
		
		Date expectedArrivalDate = null;
		if(event.getLocalDate()!=null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(event.getLocalDate());
			cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
			expectedArrivalDate = cal.getTime();
		}
		Map<String ,Map<String,CategoryMapping>> alternateRowMap = TicketUtil.getSectionBasedAlternateRows(event);
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);

			Map<String,CategoryMapping> rowMap = alternateRowMap.get(key);
			if(rowMap == null ||  rowMap.isEmpty()) {
				continue;	
			}
				
			for (String  alRow : rowMap.keySet()) {
				CategoryMapping categoryMapping = rowMap.get(alRow);
				if(categoryMapping == null) {
					continue;
				}
				List<String>  rowList = categoryMapping.getRowList();
				if(rowList == null ||  rowList.isEmpty()) {
					continue;
				}
				Set<Ticket> rowTicketList =  new HashSet<Ticket>();
				
				for (Ticket ticket : sectionTickets) {
					if(ticket.getRow()!= null && rowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						rowTicketList.add(ticket);
					}
				}
					
				for (int i = 1; i <= 10; i++) {
					//Tamil Process 2 and 4 quantities only to TN
					if( i ==2 || i== 4) {// || i== 6 || i== 8 || i== 3  || i== 6 
						int quantity = i;
						AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
						catTicket = (AutoCats96CategoryTicket) computecategoryTicket(rowTicketList, event, exEvent, quantity, sectionEligibleMinEntries,expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
						
						if(catTicket != null) {
							catTicket.setQuantity(quantity);
							//catTicket.setQtyPriority(getQtyPriority(quantity));
							catTicket.setProductPriority(1);
							//catTicket.setCatsPriority(getAutoCatsPriority(quantity, catTicket.getProductPriority()));
							catTicket.setInternalNotes("VIPMINICATS");
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							catTicket.setCatRow(categoryMapping.getAlternateRow().toUpperCase());
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							//toSkipSections.add(catTicket.getSection());
							catTixList.add(catTicket);
						}
					}
				}
			}
		}
		return catTixList;
	}
	
	public static List<AutoCats96CategoryTicket> getPresaleAutoCatCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,
			Collection<Ticket> finalTicketList, List<AutoCats96CategoryTicket> catTixList) throws Exception {

		Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
		for (Ticket ticket : finalTicketList) {
			
			if(ticket.getCategory().getNormalizedZone() != null && ticket.getCategory().getNormalizedZone().length() > 0) {
				String section = ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().toLowerCase();
				Set<Ticket> sectionTickets = sectionTicketMap.get(section);
				
				if(null == sectionTickets) {
					sectionTickets = new HashSet<Ticket>();
					sectionTicketMap.put(section, sectionTickets);
				}
				sectionTickets.add(ticket);
			}
		}

//		Date now = new Date();
//		now = df.parse(df.format(now));
		
		//sectionEligibleMinEntries=1;
			//logger.info("NF eventId...."+event.getId()+"..section.."+key+"..rowRange.."+rowRange);
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);				
			for (int i = 1; i <= 9; i++) {
				//Tamil Process 2 and 4 quantities only to TN
				if( i ==2 || i== 4) {// || i== 3 || i== 6  
					int quantity = i;
					AutoCats96CategoryTicket catTicket = new AutoCats96CategoryTicket();
					catTicket.setInternalNotes("AUTOCAT");
					catTicket = (AutoCats96CategoryTicket) computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries,expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket);
					
					if(catTicket != null) {
						catTicket.setQuantity(quantity);
						catTicket.setPosExchangeEventId(event.getAdmitoneId());
						catTicket.setProductPriority(1);
					//	catTicket.setRowRange(rowRange);
					//	catTicket.setLastRow(lastRow);
						catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
						//catTicket.setSection(key);
						
						catTixList.add(catTicket);
					}
				}
			}
		}		
		return catTixList;
	}	
}
