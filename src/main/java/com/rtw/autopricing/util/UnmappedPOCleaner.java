package com.rtw.autopricing.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.Broker;


public class UnmappedPOCleaner extends QuartzJobBean implements StatefulJob {
	static Logger logger = LoggerFactory.getLogger(UnmappedPOCleaner.class);
	
	private static Integer MINUTE_PO_DELETE_LIMIT = 200;
	private static Integer MINUTE_PO_DELETE_COUNTER = 0;
	private static Date PO_DELETE_TIMESTAMP = new Date();
	private static Boolean running=false;
	
	
	public static void getTodaySalesFromPOS() {
		
		if(isRunning()){
			return ;
		}
		setRunning(true);
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress =resourceBundle.getString("emailNotificationTo");
		String ccAddress=resourceBundle.getString("emailNotificationCCTo");
//		logger.info("Unmapped POS Cleaner Job Started @ " + new Date());
		System.out.println("Unmapped POS Cleaner Job Started @ " + new Date());
		try {
			
					
			int tTixRemovedToAPI = 0;
			MINUTE_PO_DELETE_COUNTER = 0;
			PO_DELETE_TIMESTAMP = new Date();
			
			AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
			List<Integer> brokerIds = BrokerUtils.getAllBrokersId();
			for (Integer brokerId : brokerIds) {
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				try {
					//Tamil : to check broker is enabled for TN
					Boolean isEnabled = DAORegistry.getAutopricingSettingsDAO().getIsBrokerEnabledForExchange(broker.getId(),exchange.getId());
					if(!isEnabled) {
						continue;
					}
					
					if(broker.getId() != 2 && broker.getId() != 10) {
						continue;
					}
					List<Integer> purchaseOrderIds = InduxDAORegistry.getPurchaseOrderDAO().getAllUnMappedPurchaseOrderIds(broker);
					System.out.println("Total Unmapped PO's to be removed :"+purchaseOrderIds.size()+" : for broker :"+broker.getId());
					
					for (Integer poId : purchaseOrderIds) {
						try {
							if(MINUTE_PO_DELETE_COUNTER >= MINUTE_PO_DELETE_LIMIT){
								Long nowTime = new Date().getTime();
								Long minuteTime = PO_DELETE_TIMESTAMP.getTime();
								if((nowTime - minuteTime )<60*1000){
									Thread.sleep((60*1000)-(nowTime - minuteTime));
								}
								PO_DELETE_TIMESTAMP = new Date();
								MINUTE_PO_DELETE_COUNTER =0;
							}
							
							Long nowTime = new Date().getTime();
							Long minuteTime = PO_DELETE_TIMESTAMP.getTime();
							if((nowTime - minuteTime )>60*1000){
								PO_DELETE_TIMESTAMP = new Date();
								MINUTE_PO_DELETE_COUNTER =0;
							}
							
							//System.out.println("PO CL Delete starts....."+new Date()+"...."+broker.getId()+"...count.."+tTixRemovedToAPI);
							
							InduxDAORegistry.getPurchaseOrderDAO().deleteById(broker.getPosBrokerId(), poId);
							MINUTE_PO_DELETE_COUNTER++;
							tTixRemovedToAPI++;
							
						}catch (Exception e) {
							e.printStackTrace();
							isErrorOccured = true;
							error = new AutoPricingError();
							error.setMessage("" + e.fillInStackTrace());
							error.setProcess("Error While Remove PO : "+poId);
							error.setEventId(0);
							error.setTimeStamp(new Date());
							errorList.add(error);
							logger.error("Remove PO: " + poId+" : "+broker.getId());
							System.err.println("Remove PO: " + poId+" : "+broker.getId());
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					isErrorOccured = true;
					error = new AutoPricingError();
					error.setMessage("Error While Deleting unmapped PO's for broker :  "+broker);
					error.setExample("Error While Deleting unmapped PO's for broker :  "+broker);
					error.setProcess("Unmapped PO Removal.");
					error.setEventId(0);
					error.setTimeStamp(new Date());
					errorList.add(error);
					logger.error("Error While Deleting unmapped PO's for broker :  "+broker);
					System.err.println("Error While Deleting unmapped PO's for broker :  "+broker);
				}
			}
				
					
	//		logger.error("Totla Removed PO's count:"+ tTixRemovedToAPI +":@:"+new Date());
			System.out.println("Totla Removed PO's count:"+ tTixRemovedToAPI +":@:"+new Date());
		
			running=false;
		}catch (Exception e) {
			running=false;
			
			e.printStackTrace();
			isErrorOccured = true;
			error = new AutoPricingError();
			error.setMessage("Error in Unmapped POs Removal");
			error.setExample("Error in Unmapped POs Removal");
			error.setProcess("Unmapped PO removal process.");
			error.setEventId(0);
			error.setTimeStamp(new Date());
			errorList.add(error);
			logger.error("Error in Unmapped POs Removal");
			System.err.println("Error in Unmapped POs Removal");
		}
			String subject,fileName;
			Map<String, Object> map;
			
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {

					DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
					
					subject = "Unmapped POs Removal Job failed.";
					fileName = "templates/autopricing-job-failure-message.txt";
					
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		//}
		System.out.println("Unmapped PO's Job Completed @ " + new Date());
		//logger.info("Unmapped PO's Job Completed @ " + new Date());
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
			
		try{
			System.out.println("Unmapped PO's Job Called.." + new Date() + ": " + running);
			//logger.info("Unmapped PO's Job Called.." + new Date() + ": " + running);
			
			getTodaySalesFromPOS();
			
			//System.out.println("Unmapped PO's Job finished @ " + new Date());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		UnmappedPOCleaner.running = running;
	}
}