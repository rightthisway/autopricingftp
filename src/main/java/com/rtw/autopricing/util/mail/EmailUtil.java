package com.rtw.autopricing.util.mail;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;



public class EmailUtil {
	private static Logger logger= LoggerFactory.getLogger(EmailUtil.class) ;
	private static final int DEFAULT_TIMEOUT = 30000;
	
	public static void sendOld(String generator,Exception ex,String host,String from,String userName,String password,String to,String ccTo) {
		
		Properties props=System.getProperties();
		Session session=Session.getDefaultInstance(props);
		props.put("mail.smtp.auth", "true");
		Authenticator auth = new MailAuthenticator(userName, password);
		session = Session.getInstance(props, auth);			
		props.put("mail.smtp.host", host);
		Message msg= new MimeMessage(session);

		

		try {
			msg.setFrom(new InternetAddress(from));
			msg.setRecipients(Message.RecipientType.TO , InternetAddress.parse(to,false));
			String[] ccList=ccTo.split(";");
			for(String cc:ccList){
				msg.addRecipients(Message.RecipientType.CC,InternetAddress.parse(cc,false));
			}
			msg.setSubject("Job For Exchange Files is Failed.");
		
			String text="Job For Exchange Files is Failed due to :" + ex.getMessage() + " in " + generator +" Class \n";
			text+="Detail Cause:" + ex.getCause();
			text+=ex.toString();
			if(ex.getMessage()!=null){
				String[]temp=ex.getMessage().split("\\(",2);
				String fileName=temp[0].trim();
				if(fileName.contains(".csv") || fileName.contains(".txt")){
					File file= new File(fileName);
					Date dt=new Date(file.lastModified());
					text+="Last Modified Date:" + dt.toString();
				}
			}
			
						
			
			
			msg.setText(text);
			msg.setHeader("X-Mailer", "LOTONtechEmail");
			msg.setSentDate(new Date());
	
		    Transport.send(msg);
			
	
		} catch (AddressException e) {
			e.printStackTrace();
			logger.error("Address Exception:"+ e.fillInStackTrace());
		} catch (MessagingException e) {
			e.printStackTrace();
			logger.error("Messaging Exception"+ e.fillInStackTrace());
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception"+ e.fillInStackTrace());
		}

	}
	
	public static void sendEmailOld(String smtpHost, int smtpPort, String transportProtocol,
		String smtpSecureConnection,
		Boolean smtpAuth, String username, String password,
		String fromName,
		String emailFrom,
		final String toAddress,
		final String ccAddress,
		final String bccAddress,Exception ex, String generator,String subject,String text){
//		final String subject, final String body) {
		
	
	/**/
	try {
	Properties props = new Properties();
	props.put("mail.smtp.host", smtpHost);
	props.put("mail.smtp.port", smtpPort);
	props.put("mail.debug", "false");
	props.put("mail.transport.protocol", "smtp");
	
	if (smtpSecureConnection.equalsIgnoreCase("tls")) {
		props.put("mail.smtp.starttls.enable", "true");
	}
	
	props.put("mail.smtp.connectiontimeout", 30000L);
	props.put("mail.smtp.timeout", 30000L);
	
	props.put("mail.smtp.socketFactory.port", smtpPort);
	if (!smtpSecureConnection.equalsIgnoreCase("no")) {
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	}
	props.put("mail.smtp.socketFactory.fallback", "false");
	props.put("mail.smtp.quitwait", "false");			
			
	Session session;
	if (smtpAuth) {
		props.put("mail.smtp.auth", "true");
		Authenticator auth = new MailAuthenticator(username, password);
		session = Session.getInstance(props, auth);
	} else {
		props.put("mail.smtp.auth", "false");
		Authenticator auth = new MailAuthenticator(username, null);
		session = Session.getInstance(props, auth);
	}

	MimeMessage message = new MimeMessage(session);

	String fullFromAddress = null;
	if (fromName == null) {
		fullFromAddress = emailFrom;			
	} else {
		fullFromAddress = "\"" + fromName + "\" <" + emailFrom + ">";			
	}			

	Address fromAddr = new InternetAddress(fullFromAddress);

//	message.setSubject(subject);
	message.setFrom(fromAddr);
	message.setReplyTo(new Address[] {new InternetAddress(fullFromAddress)});

	if (toAddress != null) {
		message.setRecipients(Message.RecipientType.TO, getAddresses(toAddress));	
	}
	if (ccAddress != null) {
		message.addRecipients(Message.RecipientType.CC, getAddresses(ccAddress));	
	}
	if (bccAddress != null) {
		message.addRecipients(Message.RecipientType.BCC, getAddresses(bccAddress));
	}

	message.setSubject(subject);
	if(ex!=null){
		message.setSubject("Job For Exchange Files is Failed");
		text="Job For Exchange Files is Failed due to :" + ex.fillInStackTrace().toString() + " in " + generator +" Class \n";
		text+="Detail Cause:" + ex.getCause();
		text+=ex.toString();
		if(ex.getMessage()!=null){
			String[]temp=ex.getMessage().split("\\(",2);
			String fileName=temp[0].trim();
			if(fileName.contains(".csv") || fileName.contains(".txt")){
				File file= new File(fileName);
				Date dt=new Date(file.lastModified());
				text+="Last Modified Date:" + dt.toString();
			}
		}
	}
	
	
				
	
	
	message.setText(text);

	message.setSentDate(new Date());
	
	Transport transport = session.getTransport("smtp");
	transport.connect(smtpHost, smtpPort, username, password);
	transport.send(message);
	logger.info("Message Sent successfully");
	System.out.println("Message Sent successfully");
	
		} catch (AddressException e) {
			e.printStackTrace();
			logger.error("Address Exception:"+ e.fillInStackTrace());
		} catch (MessagingException e) {
			e.printStackTrace();
			logger.error("Messaging Exception"+ e.fillInStackTrace());
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception"+ e.fillInStackTrace());
		}

	}
	
	public static void sendMailNow(String smtpHost, int smtpPort, String transportProtocol,
			String smtpSecureConnection,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress,
			final String ccAddress,
			final String bccAddress,
			final String subject, String content, String mimeType) throws Exception {
		
//		System.out.println("TRYING TO SEND MAIL " + resource);
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		props.put("mail.transport.protocol", "smtp");
		
		if (smtpSecureConnection.equalsIgnoreCase("tls")) {
			props.put("mail.smtp.starttls.enable", "true");
		}
		
		props.put("mail.smtp.connectiontimeout", DEFAULT_TIMEOUT);
		props.put("mail.smtp.timeout", DEFAULT_TIMEOUT);
		
		props.put("mail.smtp.socketFactory.port", DAORegistry.getPropertyDAO().getPropertyByName("smtp.port").getValue());
		if (!smtpSecureConnection.equalsIgnoreCase("no")) {
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		}
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.quitwait", "false");			
				
		Session session;
		if (smtpAuth) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new MailAuthenticator(username, password);
			session = Session.getInstance(props, auth);
		} else {
			props.put("mail.smtp.auth", "false");
			Authenticator auth = new MailAuthenticator(username, null);
			session = Session.getInstance(props, auth);
		}

		MimeMessage message = new MimeMessage(session);

		String fullFromAddress = null;
		if (fromName == null) {
			fullFromAddress = emailFrom;			
		} else {
			fullFromAddress = "\"" + fromName + "\" <" + emailFrom + ">";			
		}			

		Address fromAddr = new InternetAddress(fullFromAddress);

		message.setSubject(subject);
		message.setFrom(fromAddr);
		message.setReplyTo(new Address[] {new InternetAddress(fullFromAddress)});

		if (toAddress != null) {
			message.setRecipients(Message.RecipientType.TO, getAddresses(toAddress));
		}
		if (ccAddress != null) {
			message.addRecipients(Message.RecipientType.CC, getAddresses(ccAddress));	
		}
		if (bccAddress != null) {
			message.addRecipients(Message.RecipientType.BCC, getAddresses(bccAddress));
		}

		
		message.setSubject(subject);
		
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(content, mimeType);
		
		//MimeMultipart mimeMultiPart = new MimeMultipart("related");
		MimeMultipart mimeMultiPart = new MimeMultipart("alternative");
		mimeMultiPart.addBodyPart(mimeBodyPart);
		
				message.setContent(mimeMultiPart);

		message.setSentDate(new Date());
		
		Transport transport = session.getTransport("smtp");
		transport.connect(smtpHost, smtpPort, username, password);
		Transport.send(message);
	}
	
	private static Address[] getAddresses(String addr) throws Exception {
		addr = addr.trim();
		if (addr == null || addr.isEmpty()) {
			return null;
		}
		String[] recipients = addr.split(";");
		Address[] addrs = new Address[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addrs[i] = new InternetAddress(recipients[i].trim());
		}
		return addrs;
	}
}
