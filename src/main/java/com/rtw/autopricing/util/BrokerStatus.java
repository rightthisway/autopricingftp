package com.rtw.autopricing.util;

import java.util.Date;

import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;


public class BrokerStatus {
	
	Broker broker;
	AutopricingProduct product;
	
	Integer id;
	Integer productId;
	Integer brokerId;
	String processType;
	Date lastRunTime;
	Integer count;
	boolean isDBConnection;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public Date getLastRunTime() {
		return lastRunTime;
	}
	public void setLastRunTime(Date lastRunTime) {
		this.lastRunTime = lastRunTime;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
	public Boolean getIsDBConnection() {
		return isDBConnection;
	}

	public void setIsDBConnection(Boolean isDBConnection) {
		this.isDBConnection = isDBConnection;
	}
	
	public Broker getBroker() {
		return broker;
	}
	public void setBroker(Broker broker) {
		this.broker = broker;
	}
	public AutopricingProduct getProduct() {
		return product;
	}
	public void setProduct(AutopricingProduct product) {
		this.product = product;
	}
	public void setDBConnection(boolean isDBConnection) {
		this.isDBConnection = isDBConnection;
	}

}
