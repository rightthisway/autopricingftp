package com.rtw.autopricing.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;


public class VipMiniTicketsCleaner extends QuartzJobBean implements StatefulJob {
	private static Boolean running = false;
	
	private static Logger log = LoggerFactory.getLogger(VipMiniTicketsCleaner.class);
	public void cleanAllPOS(){
		
		if(isRunning()){
			return ;
		}
		setRunning(true);
		
		 Date now = new Date(); 
		 int tUpdate=0,tremovet=0;
		 int update=0,removet=0;
		 ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		 String toAddress = resourceBundle.getString("emailNotificationTo");
		 String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		 AutoPricingError error= null;
		 List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		 boolean isErrorOccured = false;
		 
		 Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
		 
		 AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("VIP MiniCats");
		 AutopricingExchange tnExchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
		 
		 if(autopricingProduct.getStopped()){
			 setRunning(false);
			 //setStopped(true);
			 System.out.println("VIP Mini Ticket Cleaner job skiped.");
			 log.info("VIP Mini Ticket Cleaner job skiped.");
				
			 return;
		 }
		 
		 Map<Integer,AutopricingSettings> autopricingSettingsMap = new HashMap<Integer, AutopricingSettings>();
		 List<AutopricingSettings> autopricingSettingsList = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettingsByProductIdandExchangeId(autopricingProduct.getId(), tnExchange.getId());
		 for (AutopricingSettings autopricingSettings : autopricingSettingsList) {
			 autopricingSettingsMap.put(autopricingSettings.getBrokerId(), autopricingSettings);
		 }
		 
		 Integer minimumExcludeEventDays = 0;
		 try {
			 minimumExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
			 Integer result = DAORegistry.getVipCatsCategoryTicketDAO().deleteAllVipMiniCategoryTicketswithinMinimumExcludeEventDays(minimumExcludeEventDays);
				 
			 System.out.println("VIPMC Total minicats Listings within miniumum exclude event days ("+minimumExcludeEventDays+") to be removed : "+result);
//			 log.info("Total minicats Listings within miniumum exclude event days ("+minimumExcludeEventDays+") to be removed : "+result);
			 
		 } catch (Exception e) {
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+")");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Deleting Records in TMAT.");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("VIPMC 1 : Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+") in TMAT.");
			 System.err.println("VIPMC 1 : Error while Deleting Records within miniumum exclude event days ("+minimumExcludeEventDays+") in TMAT.");
			 e.printStackTrace();
		 }
		 
		 try {
			 Integer result = DAORegistry.getVipCatsCategoryTicketDAO().deleteAllVipMiniCategoryTicketsnotExistinExchangeEventforAnyExchange();
				 
			 System.out.println("VIPMC Total Listings Not Exist in Exchange Event for any Exchange to be removed : "+result);
//			 log.info("Total Listings Not Exist in Exchange Event for any Exchange to be removed : "+result);
			 
		 } catch (Exception e) {
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Deleting Records in TMAT.");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("VIPMC 2 : Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
			 System.err.println("VIPMC 2 : Error while Deleting Records not existin exchange Event for any exchange in TMAT.");
			 e.printStackTrace();
		 }
		 
//commented for not updating tn
		 //List<Integer> brokerIdList = BrokerUtils.getAllBrokersId();
		 List<Integer> brokerIdList = new ArrayList<Integer>();
			 
		for(Integer brokerId : brokerIdList){
			update=0;removet=0;
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			
			AutopricingSettings tnautopricingSettings = autopricingSettingsMap.get(broker.getId());
			if(tnautopricingSettings == null || !tnautopricingSettings.getIsEnabled()) {
				System.out.println("vip Mini Ticket Cleaner job skiped for BROKER " + brokerId);
				log.info("vip Mini Ticket Cleaner job skiped for BROKER " + brokerId);
				
				continue;
			}
			
			try {
				 if(tnautopricingSettings.getExcludeEventDays()> minimumExcludeEventDays) {
					 
					 Integer result = DAORegistry.getVipCatsCategoryTicketDAO().deleteAllTnVipMiniCategoryTicketswithinExcludeEventDaysByBroker(brokerId,tnautopricingSettings.getExcludeEventDays());
					 
					 System.out.println("VIPMC : Total TN Listings Not Exist in Exchange Event for BROKER " + brokerId + " to be removed : "+result);
//					 log.info("Total TN Listings Not Exist in Exchange Event for BROKER " + brokerId + " to be removed : "+result);
				 }
				 
			 } catch (Exception e) {
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Deleting Records in TMAT.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("VIPMC 3 : Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);;
				 System.err.println("VIPMC 3 : Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);
				 e.printStackTrace();
			 }
			 
			 try {
				 Integer result = DAORegistry.getVipCatsCategoryTicketDAO().deleteAllTnVipMiniCategoryTicketsnotExistinExchangeEventForTNByBroker(brokerId);
				 
				 System.out.println("VIPMC : Total TN Listings Not Exist in Exchange Event for BROKER " + brokerId + " to be removed : "+result);
//				 log.info("Total TN Listings Not Exist in Exchange Event for BROKER " + brokerId + " to be removed : "+result);
				 
			 } catch (Exception e) {
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Deleting Records in TMAT.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("VIPMC 4 : Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);
				 System.err.println("VIPMC 4 : Error while Deleting TN Listings Not Exist in Exchange Event for broker : "+brokerId);
				 e.printStackTrace();
			 }
			 
			 try {
				//Deleting unmapped MiniCats listings with POS
				 Integer result = DAORegistry.getVipCatsCategoryTicketDAO().deleteAllVipMinicatsTnCategoryTicketGroupIdsNotExistInPOSByBrokerId(broker,autopricingProduct);
					System.out.println("VIPMC : Unmapped Larry tn tickets with POS for BROKER " + brokerId + " to be removed :"+result);
//					log.info("Unmapped Larry tn tickets with POS for BROKER " + brokerId + " to be removed :"+result);
					
				}catch (Exception e) {
					e.printStackTrace();
					error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					error.setMessage("Error while removeing unmapped larry listings with POS in TMAT for broker : "+brokerId);
					error.setExample(""+e.fillInStackTrace());
					error.setProcess("Remove Category.");
					error.setEventId(0);
					error.setTimeStamp(new Date());
					errorList.add(error);
					log.error("VIPMC 5 : Error while removeing unmapped larry listings with POS in TMAT for broker : "+brokerId);
					System.err.println("VIPMC 5 : Error while removeing unmapped larry listings with POS in TMAT for broker : "+brokerId);
				}
				
				//Deleting unmapped MiniCats listings in POS with larryLast category
			 try {
				 List<Integer> nonExistTciketGroupIds = DAORegistry.getVipCatsCategoryTicketDAO().getAllTicketNetworkGroupIdsNotExistInVipMiniCategoryTicketByBrokerId(broker,autopricingProduct);
				 System.out.println("VIPMC : TN ticket groups Not Exist in Mini for BROKER " + brokerId + " to be removed :"+nonExistTciketGroupIds.size());
//				 log.info("TN ticket groups Not Exist in Mini for BROKER " + brokerId + " to be removed :"+nonExistTciketGroupIds.size());
				 
				 for(Integer categoryTicketGroupId : nonExistTciketGroupIds){
						
					try {
						if(Counter.isMinuteDeleteAllow(broker)) {
//							System.out.println("FE API delete begins....."+new Date()+"...count.."+removet);
							
							InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),categoryTicketGroupId);
							broker.addMinuteUpdateCounter();
							removet++;
						}
					} catch (Exception e) {
						e.printStackTrace();
							isErrorOccured = true;
							error = new AutoPricingError();
							 error.setProductId(autopricingProduct.getId());
							error.setMessage("ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
							error.setProcess("POS Remove Category.");
							error.setExample(""+e.fillInStackTrace());
							error.setEventId(0);
							error.setTimeStamp(new Date());
							errorList.add(error);
							log.error("VIPMC 6 : Error While Remove TN ticket groups Not Exist in larrylast ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
							System.err.println("VIPMC 6 : Error While Remove TN ticket groups Not Exist in larrylast ctgid " +categoryTicketGroupId+" : broler id : "+broker.getId());
					}
				 }

			 } catch (Exception e) {
				 
//					 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Deleting mismatching Records in POS for broker : "+brokerId);
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Deleting Records in POS.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("VIPMC 7 : Error while Deleting mismatching Records in POS for broker : "+brokerId);
				 System.err.println("VIPMC 7 : Error while Deleting mismatching Records in POS. for broker : "+brokerId);
				 e.printStackTrace();
			 }
			 tUpdate = tUpdate + update;
			 tremovet= tremovet + removet;
			 
			 Integer deleteCount = brokerDeleteCountMap.get(broker.getId());
			 if(deleteCount==null){
				 deleteCount = 0;
			 }
			 deleteCount= deleteCount + removet;
			 brokerDeleteCountMap.put(broker.getId(),deleteCount);
		}
		running = false;
		
		List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
		
		for(Integer brokerId:brokerDeleteCountMap.keySet()){
			int count = brokerDeleteCountMap.get(brokerId);
			if(count==0){
				continue;
			}
			AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			addAudit.setProduct(autopricingProduct);
			addAudit.setBroker(broker);
			addAudit.setCount(count);
			addAudit.setProcessType("Delete");
			addAudit.setLastRunTime(now);
			autoCatsProjectAuditList.add(addAudit);
			
		}
		DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);
		
		 String subject,fileName;
		 Map<String, Object> map;
		
		if(isErrorOccured){
			map = new HashMap<String, Object>();
			map.put("error", error);
			map.put("errors", errorList);
			try {
				log.info("CT error size......"+errorList.size());
				
				DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
				subject = "VIP MiniCats Ticket Cleaner job failed :";
				fileName = "templates/autopricing-job-failure-message.txt";
				
				//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
				
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
				
			} catch (Exception e) {
				log.error("VIPMC 8 : Error while Inserting Error Listings in TMAT.");
				 System.out.println("VIPMC 8 : Error while Inserting Error Listings in TMAT.");
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		cleanAllPOS();
	}
	
	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		VipMiniTicketsCleaner.running = running;
	}
}
