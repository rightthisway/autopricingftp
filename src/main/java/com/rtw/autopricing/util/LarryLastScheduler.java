package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.ExcludeEventZones;
import com.rtw.autopricing.ftp.data.ExcludeVenueCategoryZones;
import com.rtw.autopricing.ftp.data.LarryLastCategoryTicket;
import com.rtw.autopricing.ftp.data.LarryLastExchangeEvent;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.indux.data.PosEvent;

public class LarryLastScheduler extends QuartzJobBean implements StatefulJob {
	 private static Logger log = LoggerFactory.getLogger(LarryLastScheduler.class);
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 
	 
	 public static void processLarryMinicatsTickets() throws Exception{
		Calendar cal =  Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.MINUTE,15);
		setNextRunTime(cal.getTime());
		if(isStopped() || isRunning()){
			return ;
		}
		setRunning(true);
		
		
		Date lastRunTime = getLastUpdateTime();
		Long minute = 0l;
		if(lastRunTime!=null){
			minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
		}else{
			minute = 2880l; // 2 Days in minute
		}
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SS Account");
		if(autopricingProduct==null){
			autopricingProduct = new AutopricingProduct();
			autopricingProduct.setName("SS Account");
			autopricingProduct.setStatus("ACTIVE");
			autopricingProduct.setStopped(false);
		}
		if(autopricingProduct.getStopped()){
			setStopped(true);
			setRunning(false);
			return;
		}
		
		setLastUpdateTime(now);
		autopricingProduct.setLastRunTime(now);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
//		Date jobStartTime = new Date();
		int tInsert=0,tUpdate=0,tremovet=0,tposCount=0;
		int eInsert=0,eUpdate=0,eremovet=0;
		
		 try{
			 Map<Integer, Integer> brokerAddCountMap = new HashMap<Integer, Integer>();
			 Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
			 Map<Integer, Integer> brokerUpdateCountMap = new HashMap<Integer, Integer>();
			 
			 List<DefaultAutoPricingProperties> defaultAutoPricingsFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getDefaultAutoPricingPropertiesByProductId(autopricingProduct.getId());
			 Map<Integer,DefaultAutoPricingProperties> defaultAutoPricingsMap = new HashMap<Integer, DefaultAutoPricingProperties>();
			 for (DefaultAutoPricingProperties defaultAutopriicngSettings : defaultAutoPricingsFromDB) {
				 defaultAutoPricingsMap.put(defaultAutopriicngSettings.getParentCategoryId(),defaultAutopriicngSettings);
			 }
			 
			 Double tnExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork").getAdditionalMarkup();
			 Double vividExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats").getAdditionalMarkup();
			 Double scorebigMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Scorebig").getAdditionalMarkup();
			 Double fanxchangeMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("FanXchange").getAdditionalMarkup();
			 Double ticketcityMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity").getAdditionalMarkup();
			 Double tickPickExAddMarkup = 0.0;
			 Double scoreBigExAddMarkup = 0.0;
				
			 Boolean isTnEvent = false;
			 
			 Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
			 List<PosEvent> posEvents = new ArrayList<PosEvent>();
			 Map<Integer, List<PosEvent>> posEventsAndBrokerMap = new HashMap<Integer, List<PosEvent>> ();
			 Collection<Event> eventList = null;
			 Collection<LarryLastExchangeEvent> larryLastExchangeEvents = null;
			 //Map<Integer, Boolean> tnCrawlOnlyEventMap = new HashMap<Integer, Boolean>();	
			 
			 try {
				 //Property eventDeleteFlagProperty = DAORegistry.getPropertyDAO().getPropertyByName("larrylast.event.delete.flag");
				 //Boolean eventDeleteFlagOn = Boolean.valueOf(eventDeleteFlagProperty.getValue());
				 
				 //if(eventDeleteFlagOn) {
					 //Get all events that is updated in tmat since last run time. 
					// larryLastExchangeEvents = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsEligibleForUpdateTemp(minute,autopricingProduct);					 
				 //} else {
					 //Get all events that is updated in tmat since last run time. 
					 larryLastExchangeEvents = DAORegistry.getLarryLastExchangeEventDAO().getAllLarryLastExchangeEventsEligibleForUpdate(minute,autopricingProduct);
				 //}
				 System.out.println("Larry exchange Event Size : "+larryLastExchangeEvents.size());
				 log.info("Larry exchange Event Size : "+larryLastExchangeEvents.size());
				 
				 eventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentId();
				 for(Event event:eventList) {
					eventMap.put(event.getId(), event);
					eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
				 }
				 System.out.println("Larry : TMAT event size : "+eventList.size());
				 log.info("Larry : TMAT event size : "+eventList.size());
				 
				 /*List<Integer> eventIdswithTnCrawlsOnly = DAORegistry.getQueryManagerDAO().getAllEventsIdswithTnCrawlsOnly();
				 for (Integer eventId : eventIdswithTnCrawlsOnly) {
					 tnCrawlOnlyEventMap.put(eventId, Boolean.TRUE);
				 }
				 System.out.println("Larry : TN Crawl only event size : "+eventIdswithTnCrawlsOnly.size());*/
				 
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("Larry 1 : Error while Loading Events.");
				 System.err.println("Larry 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			 Map<Integer,Set<String>> excludeEventZoneMap = new HashMap<Integer,Set<String>>();
			 Map<Integer,Set<String>> excludeVenueCategoryZoneMap = new HashMap<Integer,Set<String>>();
			 
			 try {
				 List<ExcludeEventZones> excludeEventZones = DAORegistry.getExcludeEventZonesDAO().getExcludeEventZonesByProductId(autopricingProduct.getId());
				 for (ExcludeEventZones excludeEventZone : excludeEventZones) {
					 Set<String> eventZones = excludeEventZoneMap.get(excludeEventZone.getEventId());
					 if(eventZones == null) {
						 eventZones = new HashSet<String>();
					 }
					 eventZones.add(excludeEventZone.getBrokerId()+"_"+excludeEventZone.getZone().toUpperCase());
					 excludeEventZoneMap.put(excludeEventZone.getEventId(),eventZones);
				}
				 List<ExcludeVenueCategoryZones> excludeVenueCategoryZones = DAORegistry.getExcludeVenueCategoryZonesDAO().getExcludeVenueCategoryZonesByProductId(autopricingProduct.getId());
				 for (ExcludeVenueCategoryZones excludeVenueCategoryZone : excludeVenueCategoryZones) {
					 Set<String> venueCategoryZones = excludeVenueCategoryZoneMap.get(excludeVenueCategoryZone.getVenueCategoryId());
					 if(venueCategoryZones == null) {
						 venueCategoryZones = new HashSet<String>();
					 }
					 venueCategoryZones.add(excludeVenueCategoryZone.getBrokerId()+"_"+excludeVenueCategoryZone.getZone().toUpperCase());
					 excludeVenueCategoryZoneMap.put(excludeVenueCategoryZone.getVenueCategoryId(),venueCategoryZones);
				}
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Exclude Zones.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Exclude Zones Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("MINI 1 : Error while Loading Exclude Zones..");
				 System.err.println("MINI 1 : Error while Loading Exclude Zones..");
				 e.printStackTrace();
			 }
			 Integer minimamExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
			 
			 Map<Integer, Map<Integer, PosEvent>> brokerPosEventMap = new HashMap<Integer, Map<Integer,PosEvent>>();
			 Map<Integer, Broker> brokerMap = getAllAutoPricingBrokersForLarryLast();
			 for (Integer brokerId : brokerMap.keySet()) {
				 
				 Map<Integer,PosEvent> posEventMap = brokerPosEventMap.get(brokerId);
				 
				 if(null == posEventMap || posEventMap.isEmpty()) {
					 posEvents = posEventsAndBrokerMap.get(brokerId);
					if(posEvents == null){
						posEvents = BrokerUtils.getPOSEventByBrokerId(brokerId);
						posEventsAndBrokerMap.put(brokerId, posEvents);
					}
					posEventMap = new HashMap<Integer, PosEvent>();
					if(posEvents!=null){
						 for(PosEvent posEvent:posEvents){
							 posEventMap.put(posEvent.getExchangeEventId(), posEvent);
						 }
					 }
				 }
				 brokerPosEventMap.put(brokerId, posEventMap);
			 }
			 
			 /* If you want to delete existing listings and to recreate it. Begins- Tamil  */
			 Property eventDeleteFlagProperty = DAORegistry.getPropertyDAO().getPropertyByName("larrylast.event.delete.flag");
			 Boolean eventDeleteFlagOn = Boolean.valueOf(eventDeleteFlagProperty.getValue());
			 /* If you want to delete existing listings and to recreate it. Ends- Tamil  */
			
			 
			 int eventSize = larryLastExchangeEvents.size();
			 Integer i=0;
			 for (ExchangeEvent exEvent : larryLastExchangeEvents) {
				 if(isStopped()){
					 break;
				 }
				 boolean isZoneEvent =true;
				 isTnEvent = false;
				 List<Integer> tnBrokerIds = new ArrayList<Integer>();
				 List<Integer> activeBrokerIds = new ArrayList<Integer>();
				 List<Integer> tobeDeletedBrokerIds = new ArrayList<Integer>();
				 
				 List<String> activeProducts = new ArrayList<String>();
				 List<String> tobeDeletedProducts = new ArrayList<String>();
				 
				 i++;
				 Integer eventId = exEvent.getEventId();
//				 eventId = 1000071847; 
				 Event event = eventMap.get(eventId);
				 if(event==null ){
					 System.out.println(i+".TMAT Event Not Exisit :" + eventId);
//					 log.info(i+".TMAT Event Not Exisit :" + eventId);
					 continue;
				 }
				 eInsert=0;eUpdate=0;eremovet=0;
				 
				 Date startDate = new Date();	
				 long preProcessTime = 0,eventProcessTime=0,postProcessTime=0,posUpdateTime=0,tmatUpdateTime=0,totalTime=0;
					
				 /* If you want to delete existing listings and to recreate it. Begins -By Tamil  */
				 if(eventDeleteFlagOn){
					 
					 for (Integer brokerId : brokerMap.keySet()) {
						 Date currentTime = new Date();
						 Broker broker = null;
						 try{
							 broker = BrokerUtils.getBrokerById(brokerId); 
						 }catch(Exception e){
							 e.printStackTrace();
							 System.out.println("EXCEPTION ON BROKER ID=====>"+brokerId);
						 }
						 
						 List<LarryLastCategoryTicket> miniTickets = DAORegistry.getLarryLastCategoryTicketDAO().getAllTNLarryLastCategoryTicketsByEventIdByBrokerID(eventId,brokerId);
							
						 if(null == miniTickets || miniTickets.isEmpty()){
							 continue;
						 }
						 System.out.println("FORCE EVENT DELETE: Broker Name: "+broker.getName()+" " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
						 
						 for(LarryLastCategoryTicket cat:miniTickets){
							 	cat.setReason("Forced to Delete Existing Tickets");
								if(cat.getTnTicketGroupId() == null || cat.getTnTicketGroupId() <= 0 ) { 
									cat.setStatus("DELETED");
									cat.setLastUpdated(currentTime);
									continue;
								}
								try {
									if(Counter.isMinuteDeleteAllow(broker)) {
										int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,cat.getTnTicketGroupId());
										eremovet++;
										if(result == -1) {
											cat.setReason("Broker is Disabled for this Event and Ticket hold by market maker in TN");
										} else {
											cat.setReason("Broker is Disabled for this Event and Deleted from TN");
										}
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										cat.setReason("Forced to Delete Existing Tickets and Deleted from TN");
									}
								} catch (Exception e) {
									cat.setReason("Forced to Delete Existing Tickets and Exception from TN");
									isErrorOccured = true;
									error = new AutoPricingError();
									error.setProductId(autopricingProduct.getId());
									error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnCategoryTicketGroupId());
									error.setProcess("POS Remove Category.");
									error.setExample(""+e.fillInStackTrace());
									error.setEventId(eventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("SS Account 2 FORCE EVENT DELETE: Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
									System.err.println("SS Account 2 FORCE EVENT DELETE : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
								}
							}
							DAORegistry.getLarryLastCategoryTicketDAO().updateAll(miniTickets);
					 }
				 }
				 /* If you want to delete existing listings and to recreate it. Ends - By Tamil  */
				 
				 tnBrokerIds = LarryLastScheduler.getBrokerListbyParentCategory(event.getParentCategoryId());
				 //Tixcity
				 /*if(null != exEvent.getTixcityEnabled() && exEvent.getTixcityEnabled()){
					 activeBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID);
				 }else{
					 tobeDeletedBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID);
				 }*/
				 //RTW
				 if(null != exEvent.getRtwEnabled() && exEvent.getRtwEnabled()){
					 activeBrokerIds.add(5);
				 }else{
					 tobeDeletedBrokerIds.add(5);
				 }
				 //RTW-2
				/* if(null != exEvent.getRotEnabled() && exEvent.getRotEnabled()){
					 activeBrokerIds.add(10);
				 }else{
					 tobeDeletedBrokerIds.add(10);
				 }*/
				 
				 if(null != exEvent.getZonedLastrowMinicatsEnabled() && exEvent.getZonedLastrowMinicatsEnabled()){
					 activeProducts.add("ZONED LASTROW MINICATS");
				 }else{
					 tobeDeletedProducts.add("ZONED LASTROW MINICATS");
				 }
				 if(null != exEvent.getLarryLastEnabled() && exEvent.getLarryLastEnabled()){
					 activeProducts.add("LARRYLAST");
				 }else{
					 tobeDeletedProducts.add("LARRYLAST");
				 }
				 
				 System.out.println("SS Account Even:" + i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());
				 
				 List<LarryLastCategoryTicket> larrylastTickets = DAORegistry.getLarryLastCategoryTicketDAO().getAllLarryLastCategoryTicketsByEventId(eventId);
					 
					 //If There is an existing tickets for this event then only we will try to delete based on some criteria. - Done By Ulaganathan
					 
					 Map<Integer, List<LarryLastCategoryTicket>> brokerTicketsMap = new HashMap<Integer, List<LarryLastCategoryTicket>>();
					 Map<String, List<LarryLastCategoryTicket>> brokerProductTicketsMap = new HashMap<String, List<LarryLastCategoryTicket>>();
					 for (LarryLastCategoryTicket catTicket : larrylastTickets) {
						 
						 List<LarryLastCategoryTicket> tempTickets = brokerTicketsMap.get(catTicket.getTnBrokerId());
						 if(null != tempTickets && !tempTickets.isEmpty()){
							 brokerTicketsMap.get(catTicket.getTnBrokerId()).add(catTicket);
						 }else{
							 tempTickets= new ArrayList<LarryLastCategoryTicket>();
							 tempTickets.add(catTicket);
							 brokerTicketsMap.put(catTicket.getTnBrokerId(),tempTickets);
						 }
						 List<LarryLastCategoryTicket> tempTickets1 = brokerProductTicketsMap.get(catTicket.getTnBrokerId()+":"+catTicket.getInternalNotes());
						 if(null != tempTickets1 && !tempTickets1.isEmpty()){
							 brokerProductTicketsMap.get(catTicket.getTnBrokerId()+":"+catTicket.getInternalNotes()).add(catTicket);
						 }else{
							 tempTickets1= new ArrayList<LarryLastCategoryTicket>();
							 tempTickets1.add(catTicket);
							 brokerProductTicketsMap.put(catTicket.getTnBrokerId()+":"+catTicket.getInternalNotes(),tempTickets1);
						 }
					 }
					 
					 Map<Integer, Broker> enabledBrokersMap = new HashMap<Integer, Broker>();
					 Map<Integer, AutopricingSettings> enabledBrokersAutoSettingsMap = new HashMap<Integer, AutopricingSettings>();
					 for (Integer brokerId : tnBrokerIds) {
						 Broker broker = null;
						 AutopricingSettings autopricingSettings = null;
						 AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
						 isTnEvent = true;
						 if(isTnEvent){
							 broker = BrokerUtils.getBrokerById(brokerId);
							 if(broker!=null){
								 autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
							 }
						 }
						 if(autopricingSettings != null && autopricingSettings.getIsEnabled()) {
							 enabledBrokersMap.put(brokerId, broker);
						 }/*else{
							 enabledBrokersMap.put(brokerId, broker);
						 }*/
						 enabledBrokersAutoSettingsMap.put(brokerId, autopricingSettings);
					}
					 
					 
					 
					 for (Integer deleteBrokerId : tobeDeletedBrokerIds) {
						 Date currentTime = new Date();
						 Broker broker =enabledBrokersMap.get(deleteBrokerId);
						 
						 if(broker != null && null != broker.getId()) {
							 
							 List<LarryLastCategoryTicket> miniTickets = brokerTicketsMap.remove(deleteBrokerId);
							
							 if(null == miniTickets || miniTickets.isEmpty()){
								 continue;
							 }
							 System.out.println("DELETING: BROKER IS DISABLED: Broker Name: "+broker.getName()+" " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
							 for(LarryLastCategoryTicket cat:miniTickets){
									cat.setStatus("DELETED");
									cat.setLastUpdated(currentTime);
									cat.setReason("Broker is Disabled for this Event");
									
									if(cat.getTnTicketGroupId() == null || cat.getTnTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										continue;
									}
									try {
										if(Counter.isMinuteDeleteAllow(broker)) {
//											System.out.println("SS Account Event(1) :delete begins....."+new Date()+"...count.."+eremovet);
											int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,cat.getTnTicketGroupId());
											
											if(result == -1) {
												cat.setReason("Broker is Disabled for this Event and Ticket hold by market maker in TN");
											} else {
												cat.setReason("Broker is Disabled for this Event and Deleted from TN");
											}
											broker.addMinuteUpdateCounter();
											eremovet++;
											cat.setStatus("DELETED");
											cat.setLastUpdated(currentTime);
											
										}
									} catch (Exception e) {
										cat.setReason("Broker is Disabled for this Event and Exception in TN");
											e.printStackTrace();
											isErrorOccured = true;
											error = new AutoPricingError();
											error.setProductId(autopricingProduct.getId());
											error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnTicketGroupId());
											error.setProcess("POS Remove Category.");
											error.setExample(""+e.fillInStackTrace());
											error.setEventId(eventId);
											error.setTimeStamp(new Date());
											errorList.add(error);
											log.error("Larry 2 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
											System.err.println("Larry 2 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
									}
								}
								DAORegistry.getLarryLastCategoryTicketDAO().updateAll(miniTickets);
						 }
					}
					 
					 for (Integer activeBrokerId : activeBrokerIds) {
						
						 isTnEvent = true;
						 Date currentTime = new Date();
						 Broker broker = enabledBrokersMap.get(activeBrokerId);
						 boolean isUpdateTN = true;
						 
						 if(broker == null ) {
							 isUpdateTN = false;
						 }
						 
						 if(exEvent.getZone() == null || !exEvent.getZone()) { // || !event.getBrokerStatus().equalsIgnoreCase("ACTIVE")
							 isZoneEvent = false;
							 try { // Remove all tickets of event if event is not zone , event is not active or there is restriction to update POS.
							
								 if(broker != null && isUpdateTN && isTnEvent) {
									 
									 List<LarryLastCategoryTicket> miniTickets = brokerTicketsMap.remove(activeBrokerId);
									 if(null == miniTickets || miniTickets.isEmpty()){
										continue;
									 }
									 System.out.println("DELETING: EX EVENT IS ZONE EVENT " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
									
									 for(LarryLastCategoryTicket cat:miniTickets){
											
										 cat.setReason("Event is a non Zone Event");
										if(cat.getTnTicketGroupId() == null || cat.getTnTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
											cat.setStatus("DELETED");
											cat.setLastUpdated(currentTime);
											continue;
										}
										try {
											if(Counter.isMinuteDeleteAllow(broker)) {
//												System.out.println("SS Account Event(1) :delete begins....."+new Date()+"...count.."+eremovet);
												int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,cat.getTnTicketGroupId());
												
												if(result == -1) {
													cat.setReason("Event is a non Zone Event and Ticket hold by market maker in TN");
												} else {
													cat.setReason("Event is a non Zone Event and Deleted from TN");
												}
												broker.addMinuteUpdateCounter();
												eremovet++;
												cat.setStatus("DELETED");
												cat.setLastUpdated(currentTime);
												
											}
										} catch (Exception e) {
											cat.setReason("Event is a non Zone Event and Exception in TN");
												e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnTicketGroupId());
												error.setProcess("POS Remove Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("Larry 2 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
												System.err.println("Larry 2 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
										}
									 }
									 DAORegistry.getLarryLastCategoryTicketDAO().updateAll(miniTickets);
								 }
								 
								
							} catch (Exception e) {
								isErrorOccured = true;
								error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("Deleting Event Listings.");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("Larry 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
								 System.err.println("Larry  3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
								 e.printStackTrace();
							}
							//continue;
							isTnEvent = false;
						 }
						 
						 Map<Integer,PosEvent> posEventMap = null;
						 
						 if(broker != null && isUpdateTN && isTnEvent) {
							 posEventMap = brokerPosEventMap.get(activeBrokerId); 
						 }
						 
						 PosEvent posEvent = null;
						 
						 if(null ==posEventMap || posEventMap.isEmpty()) {
							isTnEvent = false;
						 }else{
							 posEvent = posEventMap.get(event.getAdmitoneId());
						 }
						 
						 if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || 
								 posEvent.getExchangeEventId() == 0){
							 
							 // Remove all tickets of the event if event is not in POS or not an exchange event in POS.
							if(broker != null && isUpdateTN && isTnEvent) {
								try {
									List<LarryLastCategoryTicket> miniTickets = brokerTicketsMap.remove(activeBrokerId);
									if(null == miniTickets || miniTickets.isEmpty()){
										continue;
									}
									
									System.out.println("DELETING: NO POS EVENT " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());

									for(LarryLastCategoryTicket cat:miniTickets){
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										cat.setReason("Event not Exist in TN");
										if(cat.getTnTicketGroupId() == null || cat.getTnTicketGroupId() <= 0 || (cat.getTnBrokerId() != null && !cat.getTnBrokerId().equals(broker.getId())) ) {//|| cat.getTnPrice() <= 0.0
											continue;
										}
										try {
											if(Counter.isMinuteDeleteAllow(broker)) {
//												System.out.println("SS Account Event(2)delete begins....."+new Date()+"...count.."+eremovet);
												int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,cat.getTnTicketGroupId());
												
												if(result == -1) {
													cat.setReason("Event not Exist in TN and Ticket hold by market maker in TN");
												} else {
													cat.setReason("Event not Exist in TN and Deleted from TN");
												}
												broker.addMinuteUpdateCounter();
												eremovet++;
												cat.setStatus("DELETED");
												cat.setLastUpdated(currentTime);
											}
										} catch (Exception e) {
											cat.setReason("Event not Exist in TN and Exception in TN");
											e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnTicketGroupId());
												error.setProcess("POS Remove Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("Larry 4 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
												System.err.println("Larry 4 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
										}
									}
									DAORegistry.getLarryLastCategoryTicketDAO().updateAll(miniTickets);

								} catch (Exception e) {
									isErrorOccured = true;
									error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									 error.setMessage("Error while Deleting Event Listings for Event not Found in POS."+eventId);
									 error.setExample(""+e.fillInStackTrace());
									 error.setProcess("Deleting Event Listings.");
									 error.setEventId(eventId);
									 error.setTimeStamp(new Date());
									 errorList.add(error);
									 log.error("Larry 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
									 System.err.println("Larry 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
									 e.printStackTrace();
								}
							}
							isTnEvent = false;
						 }
							
							
						Date date = null;
						if(event.getLocalDate()!=null){
							date = df.parse(df.format(event.getLocalDate()));	
						}
						now = df.parse(df.format(now));
						Integer excludingEventDays = null;
						AutopricingSettings autopricingSettings =enabledBrokersAutoSettingsMap.get(activeBrokerId);
						
						if(autopricingSettings!=null){
							excludingEventDays = autopricingSettings.getExcludeEventDays();
						}
						if(excludingEventDays==null){
							excludingEventDays = minimamExcludeEventDays ;
						}
						
						if(date!=null && ((date.getTime()-now.getTime()) <= excludingEventDays* 24 * 60 * 60 * 1000)) {//2 days events
							 // Remove all tickets of event if event is within 2 days..
							
							tobeDeletedBrokerIds.add(activeBrokerId);
							
							try {
								
								if(broker != null && isUpdateTN && isTnEvent) {
									List<LarryLastCategoryTicket> miniTickets = brokerTicketsMap.remove(activeBrokerId);
									if(null == miniTickets || miniTickets.isEmpty()){
										 continue;
									 }
									System.out.println("DELETING: EVENT WITHIN TWO DAYS " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
										
									for(LarryLastCategoryTicket cat:miniTickets){
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
										cat.setReason("Event within exclude eventdays");
										
										if(cat.getTnTicketGroupId() == null || cat.getTnTicketGroupId() <= 0 || (cat.getTnBrokerId() != null && !cat.getTnBrokerId().equals(broker.getId())) ) {//|| cat.getTnPrice() <= 0.0
											continue;
										}
										try {
											if(Counter.isMinuteDeleteAllow(broker)) {
//													System.out.println("SS Account Event(3)delete begins....."+new Date()+"...count.."+eremovet);
												int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,cat.getTnTicketGroupId());
												if(result == -1) {
													cat.setReason("Event within exclude eventdays and Ticket hold by market maker in TN");
												} else {
													cat.setReason("Event within exclude eventdays and Deleted from TN");
												}
												broker.addMinuteUpdateCounter();
												eremovet++;
											}
										} catch (Exception e) {
											cat.setReason("Event within exclude eventdays and Exception in TN");
											e.printStackTrace();
												isErrorOccured = true;
												error = new AutoPricingError();
												error.setProductId(autopricingProduct.getId());
												error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+cat.getTnTicketGroupId());
												error.setProcess("POS Remove Category.");
												error.setExample(""+e.fillInStackTrace());
												error.setEventId(eventId);
												error.setTimeStamp(new Date());
												errorList.add(error);
												log.error("Larry 6 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
												System.err.println("Larry 6 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getLastRow() + "):Msg:" + e.fillInStackTrace());
										}
									}
									 DAORegistry.getLarryLastCategoryTicketDAO().updateAll(miniTickets);
								 }
							} catch (Exception e) {
								 isErrorOccured = true;
								 error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Deleting Event Listings for Event within Few Days."+eventId);
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("Deleting Event Listings.");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("Larry 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
								 System.err.println("Larry 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
								 e.printStackTrace();
							}
							continue;
						}
						
						for (String productName : tobeDeletedProducts) {
							
							try {
								List<LarryLastCategoryTicket> tempTickets = brokerTicketsMap.remove(activeBrokerId);
								if(null == tempTickets || tempTickets.isEmpty()){
									 continue;
								}
								List<LarryLastCategoryTicket> miniTickets = brokerProductTicketsMap.remove(activeBrokerId+":"+productName);
								
								if(null == miniTickets || miniTickets.isEmpty()){
									 continue;
								}
								System.out.println("DELETING:DISABLED PRODUCT TIXS " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
								boolean tnFlag = false;
								if(broker != null && isUpdateTN && isTnEvent) {
									tnFlag = true;
								}
								for(LarryLastCategoryTicket cat:miniTickets){
									cat.setStatus("DELETED");
									cat.setLastUpdated(currentTime);
									cat.setReason("Product is Disabled for this Event");
									if(tnFlag){
										
										if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {
											continue;
										}
										try {
											if(Counter.isMinuteDeleteAllow(broker)) {
//													System.out.println("SS Account Event(3)delete begins....."+new Date()+"...count.."+eremovet);
												int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,cat.getTnTicketGroupId());
												if(result == -1) {
													cat.setReason("Product is Disabled for this Event and Ticket hold by market maker in TN");
												} else {
													cat.setReason("Product is Disabled for this Event and Deleted from TN");
												}
												broker.addMinuteUpdateCounter();
												eremovet++;
											}
										} catch (Exception e) {
											cat.setReason("Product is Disabled for this Event and Exception from TN");
											e.printStackTrace();
												isErrorOccured = true;
												 error = new AutoPricingError();
												 error.setProductId(autopricingProduct.getId());
												 error.setMessage("Error while Deleting Event Listings for Product is Disabled for this Event."+eventId);
												 error.setExample(""+e.fillInStackTrace());
												 error.setProcess("Deleting Event Listings.");
												 error.setEventId(eventId);
												 error.setTimeStamp(new Date());
												 errorList.add(error);
												 log.error("Larry 7 : Error while Deleting Event Listings for Product is Disabled for this Event."+eventId);
												 System.err.println("Larry 7 : Error while Deleting Event Listings for Product is Disabled for this Event."+eventId);
												 e.printStackTrace();
										}
									}
								}
								DAORegistry.getLarryLastCategoryTicketDAO().updateAll(miniTickets);
								
							} catch (Exception e) {
								 isErrorOccured = true;
								 error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Deleting Event Listings for Event within Few Days."+eventId);
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("Deleting Event Listings.");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("AUTOCATS96 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
								 System.err.println("AUTOCATS96 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
								 e.printStackTrace();
							}
						}
					 }
				 
					 if(exEvent.getZone() == null || !exEvent.getZone()) {
						 isZoneEvent = false;
					 }
				
				 /*It will skip the ticket computation part for non zone events . Please remove this code if you want to enable 
				  * ticket computation for vivid broker - Done by Tamil */
				 
				 if(!isZoneEvent){
					 //Skip the ticket computation process if the event is non zone event.
					 continue;
				 }
				
				Set<String> excludeZones = excludeEventZoneMap.get(eventId);
				if(excludeZones == null) {
					excludeZones = excludeVenueCategoryZoneMap.get(event.getVenueCategoryId());
				}
				int sectionMinEtry = 1;
				DefaultAutoPricingProperties defaultAutopricing = defaultAutoPricingsMap.get(event.getParentCategoryId());
				if(defaultAutopricing != null) {
					sectionMinEtry = defaultAutopricing.getSectionCountTicket();
				}
				
				tnBrokerIds.removeAll(tobeDeletedBrokerIds);
				if(null == tnBrokerIds || tnBrokerIds.size() <=0 ){
					continue;
				}
				
				AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
				Map<Integer, AutopricingSettings> brokerAutoPricingSettingMap = new HashMap<Integer, AutopricingSettings>(); 
				Broker broker = null;
				AutopricingSettings autopricingSettings = null;
				
				for (Integer finalBrokerId  : tnBrokerIds) {
					broker = BrokerUtils.getBrokerById(finalBrokerId);
					if(broker!=null){
						autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
					}
					brokerAutoPricingSettingMap.put(finalBrokerId, autopricingSettings);
				}
				
				preProcessTime = new Date().getTime()-startDate.getTime();
				
				// If event is valid event add , update or delete event tickets based on latest tmat tickets.
				Map<String,LarryLastCategoryTicket> catTixMap = new HashMap<String, LarryLastCategoryTicket>();
				Map<String,LarryLastCategoryTicket> catTixFromDB = new HashMap<String, LarryLastCategoryTicket>();
				List<LarryLastCategoryTicket> updatedBrokerTicketList = new ArrayList<LarryLastCategoryTicket>();
				try {
					//Tamil 04/27/2018 Remove Presale Logic
					//Tamil 10/11/2017 : if event has TN crawl only or if event is presale event then consider markup shipping as 0 and exposure as 1-OXP
					//Tamil 10/18/2017 Removed TN crawl only rule 
					//tnCrawlOnlyEventMap.get(event.getId()) != null ||
					/*if(event.getPresaleEvent() != null && event.getPresaleEvent()) {//(event.getZoneCategoryGroupName() != null &&  event.getZoneCategoryGroupName().equalsIgnoreCase("PRESALE")
						exEvent.setExposure("1-OXP");
						exEvent.setLowerMarkup(0.0);
						exEvent.setUpperMarkup(0.0);
						exEvent.setLowerShippingFees(0.0);
						exEvent.setUpperShippingFees(0.0);
						sectionMinEtry = 1;
					}*/
					
					Collection<Ticket> finalTicketList = AutoCats96CategoryGroupManager.getTMATActiveTickets(event,autopricingProduct,exEvent.getAllowSectionRange());
					
					List<CategoryTicket> catTixList = LarryLastCategoryGroupManager.computeCategoryTickets(event, exEvent,finalTicketList,exEvent.getShippingDays(),
							sectionMinEtry, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, 
							 scoreBigExAddMarkup,fanxchangeMarkup,ticketcityMarkup,autopricingProduct);
					 //List<CategoryTicket> catTixList = CategoryGroupManager.computeCategoryTickets(event, exEvent, exEvent.getShippingDays(),sectionMinEtry, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeMarkup,ticketcityMarkup,autopricingProduct,null);
					
					 for (CategoryTicket catTixObj : catTixList) { // Add generated tickets in map
						 LarryLastCategoryTicket catTix = (LarryLastCategoryTicket)catTixObj;

						//Tamil : changed tickets minimam price from $50 to $70
						// if((catTix.getQuantity() <= 3 && catTix.getTnPrice() < 75 ) ||
							//	 (catTix.getQuantity() > 3 && catTix.getTnPrice() < 50)){
						
						if(catTix.getTnPrice() < 70 ) {
							 continue;
						 }
						 if(catTix.getTnPrice()>=5000){
							 continue;
						 }
						 if(catTix.getLastRow() == null || catTix.getLastRow().contains("-") || catTix.getLastRow().toLowerCase().contains(" or ") ||
								 catTix.getSection() == null || catTix.getSection().contains("-") || catTix.getSection().toLowerCase().contains(" or ")) {
									 continue;
						 }
						 //update user assigned shipping method
						 if(exEvent.getShippingMethod() != null) {
							 catTix.setShippingMethodSpecialId(exEvent.getShippingMethod());
						 }
							
						String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+catTix.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase();
						
						LarryLastCategoryTicket existingCatTix =catTixMap.get(key);  
						 if(existingCatTix != null) {
							 if(existingCatTix.getActualPrice() > catTix.getActualPrice()) {
								 catTixMap.put(key, catTix);
								 continue;
							 } else if(existingCatTix.getActualPrice().equals(catTix.getActualPrice())) {
								 if(existingCatTix.getProductPriority() > catTix.getProductPriority()) {
									 catTixMap.put(key, catTix);
									 continue;
								 }
							 }
						 }
						catTixMap.put(key, catTix);
					 }
					 
					 Map<Integer,Integer> brokerTixCountMap = new HashMap<Integer, Integer>();
					 Set<String> distinctSections = new HashSet<String>();
					 
					 List<LarryLastCategoryTicket> larryTickets = DAORegistry.getLarryLastCategoryTicketDAO().getAllLarryLastCategoryTicketsByEventId(event.getId());
					 for (LarryLastCategoryTicket dbCatTix : larryTickets) {// Add Existing tickets in map.
						 if(isTnEvent) {
							 
							 if(dbCatTix.getTnExchangeEventId() != null && !dbCatTix.getTnExchangeEventId().equals(event.getAdmitoneId())) {
								 dbCatTix.setStatus("DELETED");
								 dbCatTix.setLastUpdated(new Date());
								 dbCatTix.setReason("POS Event Id Mismatching");
								 updatedBrokerTicketList.add(dbCatTix);
								 continue;
							 }
							 
							 if(dbCatTix.getTnTicketGroupId() != null) {
								 if(excludeZones != null && dbCatTix.getTnBrokerId() != null && dbCatTix.getTmatZone() != null) {
									 String exZoneKey = dbCatTix.getTnBrokerId()+"_"+dbCatTix.getTmatZone().toUpperCase(); 
									 if(excludeZones.contains(exZoneKey)) {
										 dbCatTix.setStatus("DELETED");
										 dbCatTix.setLastUpdated(new Date());
										 dbCatTix.setReason("Excluded Zone ticket");
										 updatedBrokerTicketList.add(dbCatTix);
										 continue;
									 }
								 }
								 /*if((dbCatTix.getQuantity() <= 3 && dbCatTix.getTnPrice() < 75 ) ||
										 (dbCatTix.getQuantity() > 3 && dbCatTix.getTnPrice() < 50)){
									 dbCatTix.setStatus("DELETED");
									 dbCatTix.setReason("Price below $50 ");
									 dbCatTix.setLastUpdated(new Date());
									 updatedBrokerTicketList.add(dbCatTix);
									 continue;
							 	}*/
								/* if(dbCatTix.getTnBrokerId()!= null) {
									 if(!dbCatTix.getTnBrokerId().equals(exEvent.getTicketNetworkBrokerId())){
										 dbCatTix.setStatus("DELETED");
										 dbCatTix.setReason("Ticket TN Broker mismatching with event TN broker");
										 updatedBrokerTicketList.add(dbCatTix);
										 continue;
									 }
								 }else{
									 dbCatTix.setTnBrokerId(exEvent.getTicketNetworkBrokerId());
								 }*/
								 
								 if(dbCatTix.getTnBrokerId() != null) {
									 distinctSections.add(dbCatTix.getSection().trim().toLowerCase());
									 Integer tixCount = brokerTixCountMap.get(dbCatTix.getTnBrokerId());
									 if(tixCount == null) {
										 tixCount = 0;
									 }
									 tixCount = tixCount + 1;
									 brokerTixCountMap.put(dbCatTix.getTnBrokerId(),tixCount);
									
								 }
								 
							 }
						 }
						 String key = dbCatTix.getQuantity()+":"+ dbCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+dbCatTix.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase();
						 catTixFromDB.put(key, dbCatTix);
					 }
					 eventProcessTime = new Date().getTime()-(startDate.getTime()+preProcessTime);
					 
					 System.out.println("SS Account Event Id :"+eventId+"..catsize.."+catTixList.size()+"..dbcatsize.."+larryTickets.size());
					 Boolean deleteAndCreateFlag = false;
					 for (Integer brokerId : tnBrokerIds) {
						 Integer tixCount = brokerTixCountMap.get(brokerId);
						 if(tixCount == null || tixCount <= 0) {
							 broker = BrokerUtils.getBrokerById(brokerId);
							 autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
							 if(autopricingSettings != null && autopricingSettings.getIsEnabled()) {
								 
								 Map<Integer,PosEvent> posEventMap = brokerPosEventMap.get(brokerId);
									PosEvent posEvent = null;
									if(null != posEventMap && posEventMap.get(event.getAdmitoneId()) != null){
										posEvent = posEventMap.get(event.getAdmitoneId());
										if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || 
												 posEvent.getExchangeEventId() == 0){
											continue;
										}
									} else {
										continue;
									}
									
								 if(distinctSections.size() >= tnBrokerIds.size()) {
									 deleteAndCreateFlag = true;
								 }
							 }
						 }
					}
					 
					 Map<Integer,Integer> brokersPOIDMap = new HashMap<Integer, Integer>();
					 if(deleteAndCreateFlag) {
						 System.out.println("SS Account : All Listings Delted due to Brokers Tix Counts Mismatching. EventId : "+eventId+" count :"+larryTickets.size()+" : "+new Date());
						 try {
							 for (Integer brokerId : tnBrokerIds) {
								 Broker tempBroker = BrokerUtils.getBrokerById(brokerId);
								 Integer purchaseOrderId = InduxDAORegistry.getPosTicketGroupDAO().getPurchaseOrderIdByExchangeEventIdForSSListing(tempBroker,event.getAdmitoneId());
								 
								 if(purchaseOrderId != null && purchaseOrderId !=0) {
									 brokersPOIDMap.put(tempBroker.getId(), purchaseOrderId);
								 }
							 }
						 } catch(Exception e) {
							 isErrorOccured = true;
							 error = new AutoPricingError();
							 error.setProductId(autopricingProduct.getId());
							 error.setMessage("Error while getting purchase orderid from pos "+eventId);
							 error.setExample(""+e.fillInStackTrace());
							 error.setProcess("getting purchase orderid from pos");
							 error.setEventId(eventId);
							 error.setTimeStamp(new Date());
							 errorList.add(error);
							 log.error("Larry 7 : Error while getting purchase orderid from pos ."+eventId);
							 System.err.println("Larry 7 : Error while getting purchase orderid from pos ."+eventId);
							 e.printStackTrace();
						 
						 }
						
						 
						 for (LarryLastCategoryTicket catTix : larryTickets) {

								try {
									catTix.setReason("Brokers Tix Count Mismatching");
									catTix.setStatus("DELETED");
									catTix.setLastUpdated(new Date());
									
									broker = BrokerUtils.getBrokerById(catTix.getTnBrokerId());
									autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
									boolean isUpdateTN=true;
									if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
										isUpdateTN = false;
									}
									if(isUpdateTN){
										 if(catTix.getTnTicketGroupId()!=null){	
											 if(Counter.isMinuteDeleteAllow(broker)) {
		//										 System.out.println("SS Account Event(4)delete begins....."+new Date()+"...count.."+eremovet);
											
												 int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,catTix.getTnTicketGroupId());
												 if(result == -1) {
													 catTix.setReason(catTix.getReason()+" and Ticket hold by market maker in TN");
												 } else {
													 catTix.setReason(catTix.getReason()+" and Deleted from TN");
												 }
												 broker.addMinuteUpdateCounter();
												 eremovet++;
											}
										 }
									}
								 } catch (Exception e) {
									 catTix.setReason(catTix.getReason()+" and Exception in TN");
									 e.printStackTrace();
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+catTix.getTnTicketGroupId());
										error.setProcess("POS Remove Category.");
										error.setExample(""+e.fillInStackTrace());
										error.setEventId(eventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("Larry 13 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + catTix.getQuantity() +":" + catTix.getSection() +":" + catTix.getLastRow() + "):Msg:" + e.fillInStackTrace());
										System.err.println("Larry 13 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + catTix.getQuantity() +":" + catTix.getSection() +":" + catTix.getLastRow() + "):Msg:" + e.fillInStackTrace());
								 }
						 }
						 DAORegistry.getLarryLastCategoryTicketDAO().updateAll(larryTickets);
						 catTixFromDB = new HashMap<String, LarryLastCategoryTicket>();
						 updatedBrokerTicketList = new ArrayList<LarryLastCategoryTicket>();
					 }
					 
					 brokerTixCountMap = new HashMap<Integer, Integer>();
					 int eventMaxTixCount = 0;
					 Map<Integer, List<LarryLastCategoryTicket>> brokersTicketMap = LarryLastScheduler.prioritizingCategoryTicketsByBroker(catTixFromDB, 
								catTixMap, brokerTixCountMap,eventMaxTixCount,tnBrokerIds,excludeZones);
						
					 postProcessTime = new Date().getTime()-(startDate.getTime()+preProcessTime+eventProcessTime);	
						
						List<LarryLastCategoryTicket> toBeSavedOrUpdatedTixs = new ArrayList<LarryLastCategoryTicket>();
						
						for (LarryLastCategoryTicket tix : updatedBrokerTicketList) {
							toBeSavedOrUpdatedTixs.add(tix);
							
							try {
								broker = BrokerUtils.getBrokerById(tix.getTnBrokerId());
								autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
								boolean isUpdateTN=true;
								if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
									isUpdateTN = false;
								}
								if(isUpdateTN){
									 if(tix.getTnTicketGroupId()!=null){	
										 if(Counter.isMinuteDeleteAllow(broker)) {
	//										 System.out.println("SS Account Event(4)delete begins....."+new Date()+"...count.."+eremovet);
										
											 int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,tix.getTnTicketGroupId());
											 if(result == -1) {
												 tix.setReason(tix.getReason()+" and Ticket hold by market maker in TN");
											 } else {
												 tix.setReason(tix.getReason()+" and Deleted from TN");
											 }
											 broker.addMinuteUpdateCounter();
											 eremovet++;
										}
									 }
								}
							 } catch (Exception e) {
								 tix.setReason(tix.getReason()+" and Exception in TN");
								 e.printStackTrace();
									isErrorOccured = true;
									error = new AutoPricingError();
									error.setProductId(autopricingProduct.getId());
									error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+tix.getTnTicketGroupId());
									error.setProcess("POS Remove Category.");
									error.setExample(""+e.fillInStackTrace());
									error.setEventId(eventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("Larry 13 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() + "):Msg:" + e.fillInStackTrace());
									System.err.println("Larry 13 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() + "):Msg:" + e.fillInStackTrace());
							 }
						}
						
						for (Integer finalBrokerId : brokersTicketMap.keySet()) {
							List<LarryLastCategoryTicket> tickets =brokersTicketMap.get(finalBrokerId);
							if(null ==tickets || tickets.isEmpty()){
								continue;
							}
							System.out.println("SS Account Event Id :"+eventId+"..brid.."+finalBrokerId+"..dbcatsize.."+tickets.size());
							broker = null;
							autopricingSettings = null;
							if(finalBrokerId == 0 ){
								
								for (LarryLastCategoryTicket tix : tickets) {
									tix.setStatus("DELETED");
									tix.setLastUpdated(new Date());
									eremovet++;
									tix.setReason("Ticket not exist in TMAT");
									
									toBeSavedOrUpdatedTixs.add(tix);
									
									try {
										broker = BrokerUtils.getBrokerById(tix.getTnBrokerId());
										autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
										boolean isUpdateTN=true;
										if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
											isUpdateTN = false;
										}
										if(isUpdateTN){
											 if(tix.getTnTicketGroupId()!=null){	
												 if(Counter.isMinuteDeleteAllow(broker)) {
			//										 System.out.println("SS Account Event(4)delete begins....."+new Date()+"...count.."+eremovet);
												
													 int result = InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,tix.getTnTicketGroupId());
													 if(result == -1) {
														 tix.setReason(tix.getReason()+" and Ticket hold by market maker in TN");
													 } else {
														 tix.setReason(tix.getReason()+" and Deleted from TN");
													 }
													 broker.addMinuteUpdateCounter();
													 eremovet++;
												}
											 }
										}
									 } catch (Exception e) {
										 tix.setReason(tix.getReason()+" and Exception in TN");
										 e.printStackTrace();
											isErrorOccured = true;
											error = new AutoPricingError();
											error.setProductId(autopricingProduct.getId());
											error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+tix.getTnTicketGroupId());
											error.setProcess("POS Remove Category.");
											error.setExample(""+e.fillInStackTrace());
											error.setEventId(eventId);
											error.setTimeStamp(new Date());
											errorList.add(error);
											log.error("Larry 13 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() + "):Msg:" + e.fillInStackTrace());
											System.err.println("Larry 13 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() + "):Msg:" + e.fillInStackTrace());
									 }
								}
								//DAORegistry.getAutoCats96CategoryTicketDAO().updateAll(tickets);
								continue;
							}
							
							isTnEvent = true;
							broker = null;
							autopricingSettings = null;
							broker = BrokerUtils.getBrokerById(finalBrokerId);
							if(null != broker){
								autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
							}
							
							boolean isUpdateTN=true,isBrokerEnabled=false;
							if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
								isUpdateTN = false;
							}
							
							if(activeBrokerIds.contains(finalBrokerId)){
								isBrokerEnabled=true;
							}else{
								//continue;
							}
							
							Map<Integer,PosEvent> posEventMap = brokerPosEventMap.get(finalBrokerId);
							PosEvent posEvent = null;
							if(null == posEventMap || posEventMap.isEmpty()){
								isTnEvent = false;
							}else{
								posEvent = posEventMap.get(event.getAdmitoneId());
							}
							
							if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || 
									 posEvent.getExchangeEventId() == 0){
								isTnEvent = false;
							}
							
							int addCount=0;
							boolean isPOCreated = false;
							Integer  purchaseOrderId = null;
							 
							for (LarryLastCategoryTicket tix : tickets) {
								
								tix.setTnBrokerId(finalBrokerId);
								tix.setLastUpdated(new Date());
								if(null != tix.getId() && tix.getTnTicketGroupId() != null){
									if(null != tix.getIsTnUpdate() && tix.getIsTnUpdate()){
										if(isTnEvent && isUpdateTN && isBrokerEnabled && isZoneEvent){ 
											 try {
												 if(Counter.isMinuteDeleteAllow(broker)) {
//													 System.out.println("CT API Update begins....."+new Date()+"...count.."+tUpdate);
													 InduxDAORegistry.getPosTicketGroupDAO().updateTicketGroup(broker.getPosBrokerId(), tix);
													 broker.addMinuteUpdateCounter();
													 tUpdate++;
													}
											 } catch (Exception e) {
												 e.printStackTrace();
													isErrorOccured = true;
													error = new AutoPricingError();
													error.setProductId(autopricingProduct.getId());
													error.setMessage("While Removing Category Ticket :"+broker.getName()+":"+tix.getTnTicketGroupId());
													error.setProcess("POS Remove Category.");
													error.setExample(""+e.fillInStackTrace());
													error.setEventId(eventId);
													error.setTimeStamp(new Date());
													errorList.add(error);
													log.error("Larry 13 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() + "):Msg:" + e.fillInStackTrace());
													System.err.println("Larry 13 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() + "):Msg:" + e.fillInStackTrace());
											 }
										 }
									}
									if(null != tix.getIsEdited() && tix.getIsEdited()){
										toBeSavedOrUpdatedTixs.add(tix);
									}
								}else {
									//boolean isCreateListingEnabled = Counter.isCreateListingEnabled(broker);
									if(isTnEvent  && isUpdateTN && isBrokerEnabled && isZoneEvent){// && isCreateListingEnabled
										
										/*if(tix.getQuantity() == 2 && tix.getTnPrice()<50){
											 continue;
										 }*/
										 if(excludeZones != null && broker != null && tix.getTmatZone() != null) {
											 String exZoneKey = broker.getId()+"_"+tix.getTmatZone().toUpperCase(); 
											 if(excludeZones.contains(exZoneKey)) {
												 continue;
											 }
										 }
										// tix.setTnBrokerId(exEvent.getTicketNetworkBrokerId());
										 if(Counter.isMinuteAddAllow(broker)){
//											 System.out.println("SS Account API ADD begins....."+new Date()+"...count.."+addCount);
											 
											 if(purchaseOrderId == null || purchaseOrderId == 0) {
												 purchaseOrderId = brokersPOIDMap.get(broker.getId());
											 }
											 //autopricingProduct.setInternalNotes(tix.getInternalNotes());
											 
											 
											 if(purchaseOrderId == null || purchaseOrderId == 0) {
												 try {
													 purchaseOrderId = InduxDAORegistry.getPosTicketGroupDAO().getPurchaseOrderIdByExchangeEventIdForSSListing(broker,event.getAdmitoneId());
													 
													 if(purchaseOrderId == null || purchaseOrderId==0) {
														 purchaseOrderId = InduxDAORegistry.getPurchaseOrderDAO().creatrePurchaseOrder(broker.getPosBrokerId());
														 System.out.println("Purchase Order Id:" + purchaseOrderId);
														 if(purchaseOrderId == -1){
															 break;
														 }
														 tposCount++;
														 isPOCreated = true;
													 }
														 
												 } catch (Exception e) {
													 isErrorOccured = true;
													 error = new AutoPricingError();
													 error.setProductId(autopricingProduct.getId());
													 error.setMessage("Error while Creating Purchase Order. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
													 error.setExample(""+e.fillInStackTrace());
													 error.setProcess("Purchase Order Creation.");
													 error.setEventId(eventId);
													 error.setTimeStamp(new Date());
													 errorList.add(error);
													 log.error("Larry 9 : Error while Creating Purchase Order. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
													 System.err.println("Larry 9 : Error while Creating Purchase Order. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
													 e.printStackTrace();
												}
											 }
										 
											 try {
												 if(InduxDAORegistry.getPosTicketGroupDAO().addTicketGroupInPOS(broker.getPosBrokerId(),tix,autopricingProduct,posEvent,purchaseOrderId)){
													 tix.setIsEdited(true);
													 broker.addMinuteUpdateCounter();
													 addCount++;
													 eInsert++;
												 }
											 } catch(Exception e) {
												 isErrorOccured = true;
												 error = new AutoPricingError();
												 error.setProductId(autopricingProduct.getId());
												 error.setMessage("Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
												 error.setExample(""+e.fillInStackTrace());
												 error.setProcess("Ticket Group Creation..");
												 error.setEventId(eventId);
												 error.setTimeStamp(new Date());
												 errorList.add(error);
												 log.error("Larry 10 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
												 System.err.println("Larry 10 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getLastRow() +")");
												 e.printStackTrace();
											 }
										 }
									 }
									if(tix.getId() == null) {
										tix.setCreatedDate(new Date());
									}
									if(null != tix.getIsEdited() && tix.getIsEdited()){
										toBeSavedOrUpdatedTixs.add(tix);
									}
								}
								
								if(broker!=null){
									 Integer createCount = brokerAddCountMap.get(broker.getId());
									 if(createCount==null){
										 createCount = 0;
									 }
									 createCount= createCount + eInsert;
									 brokerAddCountMap.put(broker.getId(),createCount);
									 
									 Integer deleteCount = brokerDeleteCountMap.get(broker.getId());
									 if(deleteCount==null){
										 deleteCount = 0;
									 }
									 deleteCount= deleteCount + eremovet;
									 brokerDeleteCountMap.put(broker.getId(),deleteCount);
									 
									 Integer updateCount = brokerUpdateCountMap.get(broker.getId());	 
									 if(updateCount==null){
										 updateCount = 0;
									 }
									 updateCount= updateCount + eUpdate;
									 brokerUpdateCountMap.put(broker.getId(),updateCount);
								}
							}
							try {
								 if(addCount==0 && isPOCreated) {
									 log.info("Purchase ORDER DELETE - " + purchaseOrderId);
									 InduxDAORegistry.getPurchaseOrderDAO().deleteById(broker.getPosBrokerId(),purchaseOrderId);
									 tposCount--;
								 }
							 } catch (Exception e) {
								 isErrorOccured = true;
								 error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Deleting PO."+purchaseOrderId+": boker "+broker.getName());
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("PO Deletion..");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("Larry 11 : Error while Deleting PO."+eventId+" : PO :"+purchaseOrderId);
								 System.err.println("Larry 11 : Error while Deleting PO."+eventId+" : PO :"+purchaseOrderId);
								 e.printStackTrace();
							 }
						}
						posUpdateTime = new Date().getTime()-(startDate.getTime()+preProcessTime+eventProcessTime+postProcessTime);
						
						DAORegistry.getLarryLastCategoryTicketDAO().saveOrUpdateAll(toBeSavedOrUpdatedTixs);
						
						tmatUpdateTime = new Date().getTime()-(startDate.getTime()+preProcessTime+eventProcessTime+postProcessTime+posUpdateTime);
					 
				} catch (Exception e) {
					isErrorOccured = true;
					error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Computing Category Tickets."+eventId);
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Category Ticket Computation.");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("Larry 8 : Error while Computing Category Tickets."+eventId);
					 System.err.println("Larry 8 : Error while Computing Category Tickets."+eventId);
					 e.printStackTrace();
					 continue;
				}
				tInsert += eInsert;
				tUpdate += eUpdate;
				tremovet += eremovet;
				 
				totalTime = new Date().getTime()-startDate.getTime();
				
				System.out.println("Larry  : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet);
				System.out.println("Larry : totalTime : "+totalTime+"prePros :"+preProcessTime+" EvenPos : "+eventProcessTime+" : postPros :"+postProcessTime+" : posUpd : "+posUpdateTime+" : tmatUpd : "+tmatUpdateTime);
			 }
			
			 List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
			
			for(Integer brokerId:brokerAddCountMap.keySet()){
				int count = brokerAddCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Insert");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerUpdateCountMap.keySet()){
				int count = brokerUpdateCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Update");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerDeleteCountMap.keySet()){
				int count = brokerDeleteCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Delete");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);
			DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
			System.out.println("Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
//			log.info("Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
			
			eventDeleteFlagProperty = DAORegistry.getPropertyDAO().getPropertyByName("larrylast.event.delete.flag");
			Boolean isFlagOn = Boolean.valueOf(eventDeleteFlagProperty.getValue());
			if(isFlagOn){
				eventDeleteFlagProperty.setValue("false");
				DAORegistry.getPropertyDAO().update(eventDeleteFlagProperty);
			}
			
			running = false; 
		 }catch(Exception e){
			 running = false;
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Loading Common Properties.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Loading common Properties..");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("Larry 15 : Error while Loading Common Properties.");
			 System.err.println("Larry 15 :  Error while Loading Common Properties.");
			 e.printStackTrace();
		 }
		 
		 String subject,fileName;
		 Map<String, Object> map;
		
		if(isErrorOccured){
			map = new HashMap<String, Object>();
			map.put("error", error);
			map.put("errors", errorList);
			try {
//				log.info("CT error size......"+errorList.size());
				
				DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
				subject = "SS Account Scheduler job failed :";
				fileName = "templates/autopricing-job-failure-message.txt";
				
				//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
				
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
				
			} catch (Exception e) {
				log.error("Larry 16 : Error while Inserting Error Listings in TMAT.");
				 System.err.println("Larry 16 : Error while Inserting Error Listings in TMAT.");
				e.printStackTrace();
			}
		}
		 
	 }
	 
	 public static Map<Integer, List<LarryLastCategoryTicket>> prioritizingCategoryTicketsByBroker(Map<String, LarryLastCategoryTicket> catTixFromDB,
				Map<String,LarryLastCategoryTicket> catTixMap,Map<Integer, Integer> brokerTixCountMap,Integer eventMaxTixCount,List<Integer> tnBrokerIds,Set<String> excludeZones)throws Exception {
				Map<Integer,List<LarryLastCategoryTicket>> brokersCategoryTixsMap = new HashMap<Integer, List<LarryLastCategoryTicket>>();
				List<LarryLastCategoryTicket> categoryTixList = new ArrayList<LarryLastCategoryTicket>();
				try{
					
					DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					String priceHistory="";
					String ticketIdHistory= "";
					String baseTicketOneHistory= "";
					String baseTicketTwoHistory="";
					String baseTicketThreeHistory="";
					String ticketHDateStr = dateTimeFormat.format(new Date());
					String priceHDateStr = dateTimeFormat.format(new Date());
					
					if(null != catTixFromDB && !catTixFromDB.isEmpty()){
						Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
						List<String> keys = new ArrayList<String>(catTixFromDB.keySet());
								
						for (String key : keys) {
							LarryLastCategoryTicket dbTix =catTixFromDB.remove(key);
							LarryLastCategoryTicket curTicket = catTixMap.remove(key);
							if(null != curTicket ){
								if(!curTicket.getActualPrice().equals(dbTix.getActualPrice()) ||
										!curTicket.getTnPrice().equals(dbTix.getTnPrice())){
									priceHistory = priceHDateStr+"-"+curTicket.getActualPrice();
									if(dbTix.getPriceHistory() != null) {
										priceHistory = dbTix.getPriceHistory() +","+priceHistory;
										if(priceHistory.length()>=500) {
											priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
										}
									}
									dbTix.setPriceHistory(priceHistory);
									dbTix.setIsEdited(true);
									dbTix.setIsTnUpdate(true);
									dbTix.setActualPrice(curTicket.getActualPrice());
									dbTix.setTnPrice(curTicket.getTnPrice());
								}else if(!dbTix.getVividPrice().equals(curTicket.getVividPrice()) ||
										 !dbTix.getTickpickPrice().equals(curTicket.getTickpickPrice()) ||
											!dbTix.getScoreBigPrice().equals(curTicket.getScoreBigPrice()) || 
											!dbTix.getFanxchangePrice().equals(curTicket.getFanxchangePrice()) ||
											!dbTix.getTicketcityPrice().equals(curTicket.getTicketcityPrice()) || 
											!dbTix.getTicketId().equals(curTicket.getTicketId())) { 
										
										dbTix.setIsEdited(true);
										
								} else if (!dbTix.getLastRow().equals(curTicket.getLastRow())){
									dbTix.setLastRow(curTicket.getLastRow());
									dbTix.setIsEdited(true);
								}else if(!curTicket.getBaseTicketOne().equals(dbTix.getBaseTicketOne()) || 
										!curTicket.getBaseTicketTwo().equals(dbTix.getBaseTicketTwo()) || 
										!curTicket.getBaseTicketThree().equals(dbTix.getBaseTicketThree())) {
									
									dbTix.setIsEdited(true);
									
								}
								
								if(!dbTix.getShippingMethodSpecialId().equals(curTicket.getShippingMethodSpecialId()) ||
										!dbTix.getNearTermOptionId().equals(curTicket.getNearTermOptionId()) ||
										dbTix.getExpectedArrivalDate().compareTo(curTicket.getExpectedArrivalDate()) != 0) {
									dbTix.setNearTermOptionId(curTicket.getNearTermOptionId());
									dbTix.setShippingMethodSpecialId(curTicket.getShippingMethodSpecialId());
									dbTix.setExpectedArrivalDate(curTicket.getExpectedArrivalDate());
									dbTix.setIsEdited(true);
									dbTix.setIsTnUpdate(true);
								}
								
								if(isPriceChangeFlag || !dbTix.getTicketId().equals(curTicket.getTicketId())) {
									ticketIdHistory = ticketHDateStr +"/"+ curTicket.getTicketId() +"/"+ curTicket.getPurPrice();
									if(dbTix.getTicketIdHistory() != null) {
										ticketIdHistory = dbTix.getTicketIdHistory()+","+ ticketIdHistory;
										if(ticketIdHistory.length()>= 500) {
											ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
										}
									}
									dbTix.setTicketIdHistory(ticketIdHistory);
									dbTix.setIsEdited(true);
								}
								
								
								//Base Ticket One
								tempTicketIdFlag = false;
								if (dbTix.getBaseTicketOne() != null && dbTix.getBaseTicketOne() != 0) {
									if(isPriceChangeFlag || !dbTix.getBaseTicketOne().equals(curTicket.getBaseTicketOne())) {
										tempTicketIdFlag = true;
									}
								} else if(curTicket.getBaseTicketOne() != null && curTicket.getBaseTicketOne() != 0) {
									tempTicketIdFlag = true;
								}
								
								if(tempTicketIdFlag) {
									baseTicketOneHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketOne() +"/"+ curTicket.getBaseTicketOnePurPrice();
									if(dbTix.getBaseTicketOneHistory() != null) {
										baseTicketOneHistory = dbTix.getBaseTicketOneHistory()+","+ baseTicketOneHistory;
																
										if(baseTicketOneHistory.length()>= 500) {
											baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
										}
									}
									dbTix.setBaseTicketOneHistory(baseTicketOneHistory);
									dbTix.setIsEdited(true);
								}
								
								// For Base Ticket 2
								tempTicketIdFlag = false;
								if (dbTix.getBaseTicketTwo() != null && dbTix.getBaseTicketTwo() != 0) {
									if(isPriceChangeFlag || !dbTix.getBaseTicketTwo().equals(curTicket.getBaseTicketTwo())){
										tempTicketIdFlag = true;
									}
								} else if(curTicket.getBaseTicketTwo() != null && curTicket.getBaseTicketTwo() != 0) {
									tempTicketIdFlag = true;
								}
								
								if(tempTicketIdFlag) {
									baseTicketTwoHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketTwo() +"/"+ curTicket.getBaseTicketTwoPurPrice();
									if(dbTix.getBaseTicketTwoHistory() != null) {
										baseTicketTwoHistory = dbTix.getBaseTicketTwoHistory()+","+ baseTicketTwoHistory;
																
										if(baseTicketTwoHistory.length()>= 500) {
											baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
										}
									}
									dbTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
									dbTix.setIsEdited(true);
								}
								
								
								// For Base Ticket 3
								tempTicketIdFlag = false;
								if (dbTix.getBaseTicketThree() != null && dbTix.getBaseTicketThree() != 0) {
									if(isPriceChangeFlag || !dbTix.getBaseTicketThree().equals(curTicket.getBaseTicketThree()))  {
										tempTicketIdFlag = true;
									}
								} else if(curTicket.getBaseTicketThree() != null && curTicket.getBaseTicketThree() != 0) {
									tempTicketIdFlag = true;
								}
								
								if(tempTicketIdFlag) {
									baseTicketThreeHistory = ticketHDateStr +"/"+ curTicket.getBaseTicketThree() +"/"+ curTicket.getBaseTicketThreePurPrice();
									if(dbTix.getBaseTicketThreeHistory() != null) {
										baseTicketThreeHistory = dbTix.getBaseTicketThreeHistory()+","+ baseTicketThreeHistory;
																
										if(baseTicketThreeHistory.length()>= 500) {
											baseTicketThreeHistory = baseTicketThreeHistory.substring(baseTicketThreeHistory.indexOf(",")+1);	
										}
									}
									dbTix.setBaseTicketThreeHistory(baseTicketThreeHistory);
									dbTix.setIsEdited(true);
								}
								
								dbTix.setBaseTicketOne(curTicket.getBaseTicketOne());
								dbTix.setBaseTicketTwo(curTicket.getBaseTicketTwo());
								dbTix.setBaseTicketThree(curTicket.getBaseTicketThree());
								dbTix.setCategoryId(curTicket.getCategoryId());
								dbTix.setTicketId(curTicket.getTicketId());
								dbTix.setItemId(curTicket.getItemId());
								
								dbTix.setActualPrice(curTicket.getActualPrice());
								dbTix.setVividPrice(curTicket.getVividPrice());
								dbTix.setTickpickPrice(curTicket.getTickpickPrice());
								dbTix.setScoreBigPrice(curTicket.getScoreBigPrice());
								dbTix.setFanxchangePrice(curTicket.getFanxchangePrice());
								dbTix.setTicketcityPrice(curTicket.getTicketcityPrice());
								
								//dbTix.setLastRow(curTicket.getLastRow());
								//dbTix.setLastRow(curTicket.getLastRow());
								//dbTix.setAlternateRow(curTicket.getAlternateRow());
								dbTix.setRowRange(curTicket.getRowRange());
								dbTix.setActualPrice(curTicket.getActualPrice());
								dbTix.setTnPrice(curTicket.getTnPrice());
								//dbTix.setTmatCategory(curTicket.getTmatCategory());
								dbTix.setShippingMethodSpecialId(curTicket.getShippingMethodSpecialId());
								dbTix.setNearTermOptionId(curTicket.getNearTermOptionId());
								dbTix.setExpectedArrivalDate(curTicket.getExpectedArrivalDate());
								dbTix.setProductPriority(curTicket.getProductPriority());
								dbTix.setTmatZone(curTicket.getTmatZone());
								dbTix.setTnExchangeEventId(curTicket.getTnExchangeEventId());
								dbTix.setInternalNotes(curTicket.getInternalNotes());
								
								categoryTixList.add(dbTix);
							}else{
								dbTix.setStatus("DELETED");
								dbTix.setLastUpdated(new Date());
								dbTix.setReason("Ticket not exist in TMAT");
								categoryTixList.add(dbTix);
							}
						}
					}
						
					if(null != catTixMap && !catTixMap.isEmpty()){
						
						for (LarryLastCategoryTicket tix : catTixMap.values()) {
							
							priceHistory = priceHDateStr+"-"+tix.getActualPrice();
							tix.setPriceHistory(priceHistory);
							tix.setCreatedDate(new Date());
							tix.setLastUpdated(new Date());
							
							ticketIdHistory = ticketHDateStr +"/"+ tix.getTicketId() +"/"+ tix.getPurPrice();
							tix.setTicketIdHistory(ticketIdHistory);
							
							if(null != tix.getBaseTicketOne() && tix.getBaseTicketOne() > 0){
								baseTicketOneHistory = ticketHDateStr +"/"+ tix.getBaseTicketOne() +"/"+ tix.getBaseTicketOnePurPrice();
								tix.setBaseTicketOneHistory(baseTicketOneHistory);
							}
							
							if(null != tix.getBaseTicketTwo() && tix.getBaseTicketTwo() > 0){
								baseTicketTwoHistory = ticketHDateStr +"/"+ tix.getBaseTicketTwo() +"/"+ tix.getBaseTicketTwoPurPrice();
								tix.setBaseTicketTwoHistory(baseTicketTwoHistory);
							}
							
							if(null != tix.getBaseTicketThree() && tix.getBaseTicketThree() > 0){
								baseTicketThreeHistory = ticketHDateStr +"/"+ tix.getBaseTicketThree() +"/"+ tix.getBaseTicketThreePurPrice();
								tix.setBaseTicketThreeHistory(baseTicketThreeHistory);
							}
							tix.setIsEdited(true);
							categoryTixList.add(tix);
						}
					}
					
					List<LarryLastCategoryTicket> tobeDeletedTickets = new ArrayList<LarryLastCategoryTicket>();
					List<LarryLastCategoryTicket> tobeUpdatedTickets = new ArrayList<LarryLastCategoryTicket>();
					List<LarryLastCategoryTicket> toBeSaveTickets = new ArrayList<LarryLastCategoryTicket>();
					
					for (LarryLastCategoryTicket catTix : categoryTixList) {
						
						if(null != catTix.getId() && catTix.getTnTicketGroupId() != null){
							if(catTix.getStatus().equals("ACTIVE")){
								tobeUpdatedTickets.add(catTix);
								continue;
							}
							tobeDeletedTickets.add(catTix);
							continue;
						}
						toBeSaveTickets.add(catTix);
					}
					
					Map<Integer, List<LarryLastCategoryTicket>> brokersExistingTicketMap = new HashMap<Integer, List<LarryLastCategoryTicket>>();
					for (LarryLastCategoryTicket tix : tobeUpdatedTickets) {
						
						List<LarryLastCategoryTicket> catTickets = brokersExistingTicketMap.get(tix.getTnBrokerId());
						if(null != catTickets && !catTickets.isEmpty()){
							brokersExistingTicketMap.get(tix.getTnBrokerId()).add(tix);
						}else{
							catTickets = new ArrayList<LarryLastCategoryTicket>();
							catTickets.add(tix);
							brokersExistingTicketMap.put(tix.getTnBrokerId(),catTickets);
						}
					}
					
					
					Map<String, List<LarryLastCategoryTicket>> zonesCatTicketsMap =  new HashMap<String, List<LarryLastCategoryTicket>>();
					Map<String, List<LarryLastCategoryTicket>> duplicateSectionTicketsMap =  new HashMap<String, List<LarryLastCategoryTicket>>();
					Set<String> duplicateSections = new HashSet<String>();
			    	
					Collections.sort(toBeSaveTickets, LarryLastScheduler.sortingCategoryTicketbyPrice);
					
			    	for (LarryLastCategoryTicket newTix : toBeSaveTickets) {
			    		List<LarryLastCategoryTicket> zoneCatTickets = zonesCatTicketsMap.get(newTix.getTmatZone());
						if(null != zoneCatTickets && !zoneCatTickets.isEmpty()){
							
							if(!duplicateSections.add(newTix.getSection())){
								List<LarryLastCategoryTicket> duplicateTickets = duplicateSectionTicketsMap.get(newTix.getSection());
								if(null != duplicateTickets && !duplicateTickets.isEmpty()){
									duplicateSectionTicketsMap.get(newTix.getSection()).add(newTix);
								}else{
									duplicateTickets = new ArrayList<LarryLastCategoryTicket>();
									duplicateTickets.add(newTix);
									duplicateSectionTicketsMap.put(newTix.getSection(), duplicateTickets);
								}
								continue;
							}
							zonesCatTicketsMap.get(newTix.getTmatZone()).add(newTix);
						}else{
							zoneCatTickets = new ArrayList<LarryLastCategoryTicket>();
							zoneCatTickets.add(newTix);
							zonesCatTicketsMap.put(newTix.getTmatZone(), zoneCatTickets);
						}
						duplicateSections.add(newTix.getSection());
					}
			    	
			    	getCheapestCategoryTicketsByZone(zonesCatTicketsMap, duplicateSectionTicketsMap, brokersExistingTicketMap,
			    			tnBrokerIds,brokersCategoryTixsMap,brokerTixCountMap,excludeZones);
			    	
			    	//To Be Deleted Tickets because no ticket found in tmat.
					brokersCategoryTixsMap.put(0, tobeDeletedTickets);
					
				}catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
				return brokersCategoryTixsMap;
			}
	 
	 public static Map<Integer,List<LarryLastCategoryTicket>> getCheapestCategoryTicketsByZone(Map<String, List<LarryLastCategoryTicket>> zonesCatTicketsMap,
				Map<String, List<LarryLastCategoryTicket>> duplicateSectionTicketsMap,
				Map<Integer, List<LarryLastCategoryTicket>> brokersExistingTicketMap,List<Integer> tnBrokerIds,
				Map<Integer,List<LarryLastCategoryTicket>> brokersCategoryTixsMap,Map<Integer, Integer> brokerTixCountMap,Set<String> excludeZones)throws Exception{
			
			List<Integer> activeTnBrokerIds = new ArrayList<Integer>();
			activeTnBrokerIds.addAll(tnBrokerIds);
			//Collections.sort(activeTnBrokerIds, LarryLastScheduler.sortingTnBrokerIds);
			Map<Integer,List<String>> brokersAllowedSectionsMap = new HashMap<Integer,List<String>>();
			Set<String> prohibitedSections = new HashSet<String>();
			
			for (Integer brokerId : activeTnBrokerIds) {
				
				List<String> allowedBrokerSections = brokersAllowedSectionsMap.get(brokerId);
				if(null ==allowedBrokerSections || allowedBrokerSections.isEmpty()){
					allowedBrokerSections = new ArrayList<String>();
				}
				List<LarryLastCategoryTicket> existingCatTixs = brokersExistingTicketMap.get(brokerId);
				
				if(null != existingCatTixs && !existingCatTixs.isEmpty()){
					
					for (LarryLastCategoryTicket dbCatTix : existingCatTixs) {
						allowedBrokerSections.add(dbCatTix.getSection());
						prohibitedSections.add(dbCatTix.getSection());
					}
					brokersCategoryTixsMap.put(brokerId, existingCatTixs);
				}else{
					brokersCategoryTixsMap.put(brokerId, new ArrayList<LarryLastCategoryTicket>());
				}
				brokersAllowedSectionsMap.put(brokerId, allowedBrokerSections);
			}
			
			Map<String,LarryLastCategoryTicket> zoneSectionMap = new HashMap<String,LarryLastCategoryTicket>();
			for (String zone : zonesCatTicketsMap.keySet()) {
				
				List<LarryLastCategoryTicket> zoneCatTickets = zonesCatTicketsMap.get(zone);
				if(null != zoneCatTickets && !zoneCatTickets.isEmpty()){
					Collections.sort(zoneCatTickets, LarryLastScheduler.sortingCategoryTicketbyPrice);
					int i=1;
					for (LarryLastCategoryTicket catTix : zoneCatTickets) {
						zoneSectionMap.put(i+":"+zone, catTix);
						i++;
					}
				}
			}
			
			Boolean maxLimitReached = false;
			Integer lastProcessedBrokerId = null,brokerMaxCount = 0;
			List<LarryLastCategoryTicket> skippedCatTixs = new ArrayList<LarryLastCategoryTicket>();
			
			List<String> brokerLevelZoneKeys = new ArrayList<String>();
			if(null != zoneSectionMap && zoneSectionMap.size() > 0){
				brokerLevelZoneKeys.addAll(zoneSectionMap.keySet());
				Collections.sort(brokerLevelZoneKeys);
			}
			
			for (String brokerLevelZone : brokerLevelZoneKeys) {
				
				LarryLastCategoryTicket tix = zoneSectionMap.get(brokerLevelZone);
				if(prohibitedSections.contains(tix.getSection())){
					skippedCatTixs.add(tix);
					continue;
				}
				
				lastProcessedBrokerId = LarryLastScheduler.getNextAvailableBrokerId(lastProcessedBrokerId, activeTnBrokerIds);
				
				if(null == lastProcessedBrokerId){
					maxLimitReached= true;
					break;
				}
				
				//Tamil : Logic for skipping excluded zone tickets - begins
				if(excludeZones != null ) {
					Integer tempBrokerId = lastProcessedBrokerId;
					boolean isAllBrokerExcludedZone = false;
					while(true) {
						String exZoneKey = lastProcessedBrokerId+"_"+tix.getTmatZone().toUpperCase(); 
						if(excludeZones.contains(exZoneKey)) {
							lastProcessedBrokerId = LarryLastScheduler.getNextAvailableBrokerId(lastProcessedBrokerId, activeTnBrokerIds);
							if(tempBrokerId.equals(lastProcessedBrokerId)) {
								isAllBrokerExcludedZone = true;
								break;
							}
						} else {
							break;
						}
					}
					if(isAllBrokerExcludedZone) {
						continue;
					}
				}
				//Tamil : Logic for skipping excluded zone tickets - ends
				
				/*brokerMaxCount = brokerTixCountMap.get(lastProcessedBrokerId);
				
				if(brokersCategoryTixsMap.get(lastProcessedBrokerId).size() >= brokerMaxCount){
					break;
				}*/
				
				brokersAllowedSectionsMap.get(lastProcessedBrokerId).add(tix.getSection());
				brokersCategoryTixsMap.get(lastProcessedBrokerId).add(tix);
				/*if(brokersCategoryTixsMap.get(lastProcessedBrokerId).size() == brokerMaxCount){
					activeTnBrokerIds.remove(lastProcessedBrokerId);
				}*/
				prohibitedSections.add(tix.getSection());
			}
			
			if(!maxLimitReached){
				
				for (LarryLastCategoryTicket tix : skippedCatTixs) {
					List<LarryLastCategoryTicket> dupCatTixs = duplicateSectionTicketsMap.get(tix.getSection());
					if(null != dupCatTixs && !dupCatTixs.isEmpty()){
						duplicateSectionTicketsMap.get(tix.getSection()).add(tix);
						continue;
					}
					dupCatTixs = new ArrayList<LarryLastCategoryTicket>();
					dupCatTixs.add(tix);
					duplicateSectionTicketsMap.put(tix.getSection(), dupCatTixs);
				}
				
				for (String section : duplicateSectionTicketsMap.keySet()) {
					Collections.sort(duplicateSectionTicketsMap.get(section), LarryLastScheduler.sortingCategoryTicketbyPrice);
				}
				
				for (Integer brokerId : brokersCategoryTixsMap.keySet()) {
					
					List<LarryLastCategoryTicket> brokerCatTickets = brokersCategoryTixsMap.get(brokerId);
					
					/*brokerMaxCount = brokerTixCountMap.get(brokerId);
					if(brokerCatTickets.size() >= brokerMaxCount){
						continue;
					}*/
					
					List<String> allowedBrokerSection = brokersAllowedSectionsMap.get(brokerId);
					List<String> duplicateSections = new ArrayList<String>(duplicateSectionTicketsMap.keySet());
					
					for (String section : duplicateSections) {
						
						if(allowedBrokerSection.contains(section)){
							
							List<LarryLastCategoryTicket> duplicateTickets = duplicateSectionTicketsMap.get(section);
							List<LarryLastCategoryTicket> toBeDeletedTickets = new ArrayList<LarryLastCategoryTicket>();
							
							if(null == duplicateTickets || duplicateTickets.isEmpty()){
								continue;
							}
							for (LarryLastCategoryTicket dubTix : duplicateTickets) {
						//		if(brokerCatTickets.size() < brokerMaxCount){
									
									//Tamil : Logic for skipping excluded zone tickets - begins 
									if(excludeZones != null) {
										String exZoneKey = brokerId+"_"+dubTix.getTmatZone().toUpperCase(); 
										if(excludeZones.contains(exZoneKey)) {
											continue;	
										}
									}
									//Tamil : Logic for skipping excluded zone tickets - ends
									
									brokerCatTickets.add(dubTix);
						//		}
								toBeDeletedTickets.add(dubTix);
							}
							duplicateSectionTicketsMap.get(section).removeAll(toBeDeletedTickets);
						}
					}
					brokersCategoryTixsMap.put(brokerId,brokerCatTickets);
				}
			}
			return brokersCategoryTixsMap;
		}
	 
	 public static Integer getNextAvailableBrokerId(Integer lastProcessedBrokerId,List<Integer> availableTnBrokerIds){
			
			if(null == availableTnBrokerIds || availableTnBrokerIds.isEmpty()){
				return null;
			}
			
			if(null == lastProcessedBrokerId){
				return availableTnBrokerIds.get(0);
			}
			
			int totalBrokerSize = availableTnBrokerIds.size();
			int indexPosition = availableTnBrokerIds.indexOf(lastProcessedBrokerId);
			if(indexPosition == totalBrokerSize-1){
				indexPosition = 0;
			}else{
				indexPosition = indexPosition +1;
			}
			Integer nextBrokerId = availableTnBrokerIds.get(indexPosition);
			return nextBrokerId;
		}
	 
	 public static Comparator<Integer> sortingTnBrokerIds = new Comparator<Integer>() {

			public int compare(Integer ticket1, Integer ticket2) {
				int cmp= ticket1.compareTo(
						ticket2);
				if (cmp < 0) {
					return 1;
				}
				if (cmp > 0) {
					return -1;
				}
				return ticket1.compareTo(ticket2);
		    }
			
		};
	 
	 public static Comparator<LarryLastCategoryTicket> sortingCategoryTicketbyPrice = new Comparator<LarryLastCategoryTicket>() {

			public int compare(LarryLastCategoryTicket ticket1, LarryLastCategoryTicket ticket2) {
				int cmp= ticket1.getTnPrice().compareTo(
						ticket2.getTnPrice());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				return cmp;
				
		    }
		};
		
		public static List<Integer> getLarryLastActiveBrokerIds(){
			List<Integer> larryLastBrokerIds = new ArrayList<Integer>();
			//larryLastBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID); /*Tixcity*/
			larryLastBrokerIds.add(5); /*RTW*/
			//larryLastBrokerIds.add(10);//RTW-2 /*Reserve One Tickets*/
			return larryLastBrokerIds;
		}
		
	 //Tamil : getting all Larrylast enabled brokers
	 public static Map<Integer, Broker> getAllAutoPricingBrokersForLarryLast() throws Exception{
			
		 Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
		 
		 //Broker tixcityBroker = DAORegistry.getBrokerDAO().get(AutoExchangeEventLoader.TIXCITY_BROKER_ID);//Tixcity
		 //brokerMap.put(tixcityBroker.getId(), tixcityBroker);
		 
		 Broker rtwBroker = DAORegistry.getBrokerDAO().get(5);//RTW
		 brokerMap.put(rtwBroker.getId(), rtwBroker);
		 
		// Broker rotBroker = DAORegistry.getBrokerDAO().get(10);//RTW-2 //ROT
		// brokerMap.put(rotBroker.getId(), rotBroker);
		 
		 return brokerMap;
	 }
	 
	 public static List<Integer> getBrokerListbyParentCategory(Integer parentCategoryId){
		 if(parentCategoryId == 1 || parentCategoryId == 2){
			 List<Integer> larryLastSportsConcertsBrokerIds = new ArrayList<Integer>();
			 //larryLastSportsConcertsBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID); /*Tixcity*/
			 larryLastSportsConcertsBrokerIds.add(5); /*RTW*/
			 //larryLastSportsConcertsBrokerIds.add(10); /*Reserve One Tickets*/
			 return larryLastSportsConcertsBrokerIds;
		 }else {
			 List<Integer> larryLastTheaterOtherBrokerIds = new ArrayList<Integer>();
			 //larryLastTheaterOtherBrokerIds.add(AutoExchangeEventLoader.TIXCITY_BROKER_ID); /*Tixcity*/
			 larryLastTheaterOtherBrokerIds.add(5); /*RTW*/
			 //larryLastTheaterOtherBrokerIds.add(10); /*Reserve One Tickets*/
			 return larryLastTheaterOtherBrokerIds;
		 }
	 }
	 
	 public static List<String> getSsAccountInternalNotes(){
			List<String> ssAccountInternalNotes = new ArrayList<String>();
			ssAccountInternalNotes.add(AutoExchangeEventLoader.ZONED_LASTROW_INTERNAL_NOTES); /*MINICATS*/
			ssAccountInternalNotes.add(AutoExchangeEventLoader.LARRYLAST_INTERNAL_NOTES); /*LASTROW MINICATS*/
			return ssAccountInternalNotes;
		 }
	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
			System.out.println("Larry-Lastrow Job Called.." + new Date() + ": " + running);
			log.info("Larry-Lastrow Job Called.." + new Date() + ": " + running);
			try{
				
				processLarryMinicatsTickets();
				System.out.println("SS Account Job finished @ " + new Date());
			}catch(Exception e){
				e.printStackTrace();
			}
	}


	public static Boolean isStopped() {
		if(stopped==null){
			return false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		LarryLastScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		LarryLastScheduler.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("SS Account").getLastRunTime();
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		LarryLastScheduler.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}


	public static void setNextRunTime(Date nextRunTime) {
		LarryLastScheduler.nextRunTime = nextRunTime;
	}


	
}