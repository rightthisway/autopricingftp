package com.rtw.autopricing.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
//import com.rtw.autopricing.ftp.indux.data.JoeKnowsTicketBroker;
//import com.rtw.autopricing.ftp.indux.data.MZTIXTicketBroker;
//import com.rtw.autopricing.ftp.indux.data.ManhattanBroker;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
//import com.rtw.autopricing.ftp.indux.data.ReservOneTicketBroker;
import com.rtw.autopricing.ftp.indux.data.PosVenueConfigurationZone;


public class BrokerUtils {
	
	private static Logger log = LoggerFactory.getLogger(BrokerUtils.class);
//	public static  Integer JOE_KNOWS_TICKET = 7784;
//	public static  Integer RESERVE_ONE_TICKET = 7790;
//	public static  Integer MANHATAN_TICKET = 7791;
//	public static  Integer MZTIX_TICKET = 3320;
	
	public static Integer RTF_EXCHANGE_BROKER_ID=1001;
	
	private static Map<Integer, List<PosEvent>> map = new HashMap<Integer, List<PosEvent>>();
	private static Map<Integer, Broker> brokerMap = new HashMap<Integer, Broker>();
	private static Map<Integer, Broker> brokerMapByPOSBrokerId = new HashMap<Integer, Broker>();
	private static Date lastFetchTime = null;
	private static  Integer FREQUENCY_HOURS = 15* 60 * 1000;
	private static List<Integer> posBrokerIds = new ArrayList<Integer>();
	private static List<Integer> brokerIds = new ArrayList<Integer>();
	
	private static Map<Integer,List<PosEvent>> getPOSEventAndBrokersMap(){
		Date now = new Date();
		if(map.isEmpty() || lastFetchTime==null || (now.getTime()-lastFetchTime.getTime()>= FREQUENCY_HOURS) ){
			try {
				for(Broker broker:DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers()){
					try {
						if(broker.getPosBrokerId()!=null){
							map.put(broker.getId(), InduxDAORegistry.getPosEventDAO().getAllActivePOSEventsByPOSBrokerId(broker.getPosBrokerId()));
						}
					} catch (Exception e) {
						System.out.println("Error while fetching events from POS.."+broker.getId());
						e.printStackTrace();
					}
				}
				
//				List<PosEvent> mtEvents = new ArrayList<PosEvent>();
//				broker = BrokerUtils.getBrokerById(MANHATAN_TICKET);
//				if(broker!=null && broker.getIsTNUpdate()) {
//					mtEvents  = InduxDAORegistry.getRegistryInstance().getPosEventDAO().getAllActivePOSEvents(MANHATAN_TICKET);
//				}
//				map.put(MANHATAN_TICKET, mtEvents);
				
				lastFetchTime = now;
			} catch (Exception e) {
				System.out.println("Error while fetching events from POS..");
				e.printStackTrace();
			}
		}
		return map;
	}
	
	public static Map<String, Integer> getPOSVenueConfigurationMap(Broker broker) throws Exception {
				
			List<PosVenueConfigurationZone> venuConfigZones = InduxDAORegistry.getPosVenueConfigurationZoneDAO().getAllVenueConfigZone(broker.getPosBrokerId());
			Map<String, Integer> venueConfigZoneMap = new HashMap<String, Integer>();
			for (PosVenueConfigurationZone configZoneObj : venuConfigZones) {
				String key = configZoneObj.getVenueConfigurationId()+":"+configZoneObj.getZone().replaceAll("\\s+", " ").trim().toLowerCase();
				venueConfigZoneMap.put(key, configZoneObj.getVenueConfigurationZoneId());
			}
			
			return venueConfigZoneMap; 
	 }
	
	public static List<PosEvent> getPOSEventByBrokerId(Integer brokerId){
		getPOSEventAndBrokersMap();
		return map.get(brokerId);
	}
	
	public static Broker getBrokerById(int brokerId){
		Broker broker = brokerMap.get(brokerId);
		if(broker==null){
			broker = DAORegistry.getBrokerDAO().get(brokerId);
			brokerMap.put(brokerId,broker);
			if(broker.getPosBrokerId()!=null){
				brokerMapByPOSBrokerId.put(broker.getPosBrokerId(), broker);
			}
		}
//		System.out.println(brokerMap.get(brokerId).toString());
		return broker;
		
	}
	
	public static void removeBrokerById(int brokerId){
		Broker broker = brokerMap.remove(brokerId);
		if(broker != null) {
			brokerMapByPOSBrokerId.remove(broker.getPosBrokerId());	
		}
		
	}
	
	public static Broker getBrokerByPOSBrokerId(int posBrokerId){
		Broker broker = brokerMapByPOSBrokerId.get(posBrokerId);
		if(broker==null){
			broker = DAORegistry.getBrokerDAO().getBrokerByPOSBrokerId(posBrokerId);
			if(broker!=null){
				brokerMap.put(broker.getId(),broker);
				brokerMapByPOSBrokerId.put(broker.getPosBrokerId(), broker);
			}
		}
//		System.out.println(brokerMap.get(brokerId).toString());
		return brokerMapByPOSBrokerId.get(posBrokerId);
		
	}
	/*public static List<Integer> getAllPosBrokersId(){
		if(posBrokerIds.isEmpty()){
			posBrokerIds.add(JOE_KNOWS_TICKET);
			posBrokerIds.add(RESERVE_ONE_TICKET);
//			brokerIds.add(MANHATAN_TICKET);
		}
		return brokerIds;
	}*/
	public static List<Integer> getAllBrokersId(){
		if(brokerIds.isEmpty()){
			List<Integer> broekrIds = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokerIds();
			brokerIds = new ArrayList<Integer>(broekrIds);
		}
		return brokerIds;
	}
	
	public static Map<Integer, Broker> getAllAutoPricingBrokers(){
		Collection<Broker> activeBrokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		for (Broker broker : activeBrokers) {
			brokerMap.put(broker.getId(), broker);
		}
		return new HashMap<Integer, Broker>(brokerMap);
	}
	
	/*public static Integer getAllActiveTicketsCountinPOS() throws SQLException {
		Integer totTixCount = 0;
		try {
			 for (Integer brokerId : getAllBrokersId()) {
				 Broker broker = BrokerUtils.getBrokerById(brokerId);
				
				 if(broker != null ){//&& broker.getIsTNUpdate()) {
					 totTixCount = totTixCount + InduxDAORegistry.getPosTicketGroupDAO().getAllActiveTicketsCountinPOS(brokerId);
				 }
			 }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return totTixCount;
	}*/
	public static Integer getLarryLastActiveTicketsCountinPOSByeventType(String posEventTypeIdStr) throws SQLException {
		Integer totTixCount = 0;
		try {
			 for (Integer brokerId : getAllBrokersId()) {
				 Broker broker = BrokerUtils.getBrokerById(brokerId);
				 if(broker != null){// && broker.getIsTNUpdate()) {
					// totTixCount = totTixCount + InduxDAORegistry.getPosTicketGroupDAO().getLarryLastActiveTicketsCountinPOS(brokerId,posEventTypeIdStr);
				 }
			 }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return totTixCount;
	}
	
	/*public static int deleteUnMappedPOSListingswithLarryLastCategoryTicket(int brokerId) throws Exception {
		 int startIndex = 0,size=0,totalCount=0;
		 List<Integer> nonExistTciketGroupIds = TMATDAORegistry.getRegistryInstance().getLastRowMiniCatsCategoryTicketDAO().getAllTicketNetworkGroupIdsNotExistInLarryLastCategoryTicket(brokerId);
		 
		 int count = 0;
		 if(nonExistTciketGroupIds.size() > 0) {
			 size = nonExistTciketGroupIds.size();
			 count = size;
			 System.out.println("total listings to be deleted in pos....."+size);
			 log.info("total listings to be deleted in pos....."+size);
			 
			 int minuteUpdate = Counter.MINUTE_UPDATE_LIMIT + 1;
			 
			 while(size > 0) {
				 List<Integer> subList;
				 if(size > minuteUpdate) {
					 subList = nonExistTciketGroupIds.subList(startIndex, (startIndex+minuteUpdate));
					 startIndex = startIndex + minuteUpdate;
					 size = size - minuteUpdate;
				 } else {
					 subList = nonExistTciketGroupIds.subList(startIndex, nonExistTciketGroupIds.size());	
					 size = 0;
				 }
				 
				 if(Counter.isMinuteUpdateAllow()) {
					 System.out.println("to be delete..."+subList.size());
					 InduxDAORegistry.getRegistryInstance().getTicketGroupDAO().updateTicketGroupsToZeroByPosTicketGroupId(brokerId,subList);
					 Counter.MINUTE_UPDATE_COUNTER += subList.size();
					 
					 totalCount = totalCount + subList.size();
					 System.out.println("total deleted listings.from pos.1..."+totalCount+"..remaining.."+size);
					 log.info("total deleted listings.from pos.1..."+totalCount+"..remaining.."+size);
				 }
			 }
		 }
		 return count;
	 }
	 
	 public static Integer updatePriceMismatchListingsinPOS(Integer brokerId) throws Exception {
		 int startIndex = 0,size=0,totalCount=0;
		 List<LastRowMiniCatsCategoryTicket> priceMismatchListings = TMATDAORegistry.getRegistryInstance().getLastRowMiniCatsCategoryTicketDAO().getAllTnPriceMismatchLarryLastCategoryTickets(brokerId);
		 
		 int count = 0;
		 if(priceMismatchListings.size() > 0) {
			 size = priceMismatchListings.size();
			 count = size;
			 System.out.println("Total Price Mismatch listings to be Updated in pos....."+size);
			 log.info("total Price Mismatch listings to be Updated in pos....."+size);
			 
			 int minuteUpdate = Counter.MINUTE_UPDATE_LIMIT + 1;
			 
			 while(size > 0) {
				 List<LastRowMiniCatsCategoryTicket> subList;
				 if(size > minuteUpdate) {
					 subList = priceMismatchListings.subList(startIndex, (startIndex+minuteUpdate));
					 startIndex = startIndex + minuteUpdate;
					 size = size - minuteUpdate;
				 } else {
					 subList = priceMismatchListings.subList(startIndex, priceMismatchListings.size());	
					 size = 0;
				 }
				 
				 if(Counter.isMinuteUpdateAllow()) {
					 System.out.println("to be update..."+subList.size());
					 InduxDAORegistry.getRegistryInstance().getTicketGroupDAO().updateAllTicketGroupsPrice(brokerId,subList);
					 Counter.MINUTE_UPDATE_COUNTER += subList.size();
					 
					 totalCount = totalCount + subList.size();
					 System.out.println("total update listings.from pos.1..."+totalCount+"..remaining.."+size);
					 log.info("total update listings.from pos.1..."+totalCount+"..remaining.."+size);
				 }
			 }
		 }
		 return count;
	 }*/
	
	public static String getActiveAndUniqueCategoryInternalNotes(){
		Collection<AutopricingProduct> products = DAORegistry.getAutopricingProductDAO().getAllActiveAutopricingProducts();
		String interalNotesCombined = "";
		for(AutopricingProduct product : products){
			// Mehul : we are skipping zone tickets processor and autocats96 products
			// also we are skipping office id 2 products
			if(product.getId() == 22 || product.getId() == 9 || (product.getOfficeId() != null && product.getOfficeId() == 2)){//product.getId() == 10 ||
				continue;
			}
			String arr[] = product.getInternalNotes().split(",");
			if(arr.length > 0) {
				for (String intNotes : arr) {
					interalNotesCombined += "'" + intNotes + "'" + ",";					
				}
			} else {
				interalNotesCombined += "'" + product.getInternalNotes() + "'" + ",";
			}
			
		}
		//System.out.println("Internal notes combined - "+ interalNotesCombined.substring(0, interalNotesCombined.length() - 1));
		return interalNotesCombined.substring(0, interalNotesCombined.length() - 1);
	}
	
	public static String getActiveAndUniqueSSAccountInternalNotesOld(){
		Collection<AutopricingProduct> products = DAORegistry.getAutopricingProductDAO().getAllActiveAutopricingProducts();
		String interalNotesCombined = "";
		for(AutopricingProduct product : products){
			// Mehul : we are skipping category autopricing products
			if(product.getOfficeId() == null){
				continue;
			}
			interalNotesCombined += "'" + product.getInternalNotes() + "'" + ",";
		}
		//System.out.println("Internal notes combined - "+ interalNotesCombined.substring(0, interalNotesCombined.length() - 1));
		return interalNotesCombined.substring(0, interalNotesCombined.length() - 1);
	}
	public static String getActiveAndUniqueSSAccountInternalNotes(){
		List<String> ssAccountInternalNotes = LarryLastScheduler.getSsAccountInternalNotes();
		String interalNotesCombined = "";
		for(String  internalNotes : ssAccountInternalNotes){
			interalNotesCombined += "'" + internalNotes + "'" + ",";
		}
		//System.out.println("Internal notes combined - "+ interalNotesCombined.substring(0, interalNotesCombined.length() - 1));
		return interalNotesCombined.substring(0, interalNotesCombined.length() - 1);
	}
	public static String getRtfAutopricingProductInternalNotesStrForSqlQuery() {
		String internalNotesStr ="";
		List<String> intNotesList = DAORegistry.getAutopricingProductDAO().getRTFAutopricingProductInternalNotes();
		for (String string : intNotesList) {
			 internalNotesStr = internalNotesStr + ",'" + string +"'";
		}
		if(internalNotesStr.length() > 0) {
			internalNotesStr = internalNotesStr.substring(1);
		}
		return internalNotesStr;
	}
}
