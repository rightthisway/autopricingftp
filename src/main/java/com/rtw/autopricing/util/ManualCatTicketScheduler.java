package com.rtw.autopricing.util;

//import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.ManualCategoryTicket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;


public class ManualCatTicketScheduler  extends QuartzJobBean implements StatefulJob {
	
	private static Logger log = LoggerFactory.getLogger(ManualCatTicketScheduler.class);
	
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
	 
	 public static void processManualCatsTickets() throws Exception{ 
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("MiniCats");
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		 
		Collection<Broker> brokers =  DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		Map<Integer, Broker> brokerMap  = new HashMap<Integer, Broker>();
		 
		for (Broker broker : brokers) {
		  if(broker.getId() == 5 || broker.getId() == 1 || broker.getId() == 4 ){
			 continue;
		  }
		  brokerMap.put(broker.getId(), broker);
		}
		 
		for (Integer brokerId : brokerMap.keySet()) {
			Broker broker = brokerMap.get(brokerId);
			
			Map<Integer, PosEvent> eventMap = new HashMap<Integer, PosEvent>();
			List<PosEvent> posEvents = InduxDAORegistry.getPosEventDAO().getAllActivePOSLocalEventsByPOSBrokerId(broker.getPosBrokerId());
			
			for (PosEvent posEvent : posEvents) {
				eventMap.put(posEvent.getId(), posEvent);
			}
			
			Map<String, List<ManualCategoryTicket>> currentEventCategoryTicketsMap = new HashMap<String, List<ManualCategoryTicket>>();
			Map<String, List<ManualCategoryTicket>> dbEventCategoryTicketsMap = new HashMap<String, List<ManualCategoryTicket>>();
			
			List<ManualCategoryTicket> activeCategoryTickets = InduxDAORegistry.getPosCategoryTicketGroupDAO().getAllBroadCastedManualCategoryTickets(broker.getPosBrokerId());
			System.out.println("ManualCats: POS MANUAL Tickets--->"+activeCategoryTickets.size());
			
			for (ManualCategoryTicket manualCategoryTicket : activeCategoryTickets) {
				
				List<ManualCategoryTicket> catTickets =  currentEventCategoryTicketsMap.get(manualCategoryTicket.getTnLocalEventId()+":"+brokerId);
				
				if(null != catTickets && !catTickets.isEmpty()){
					currentEventCategoryTicketsMap.get(manualCategoryTicket.getTnLocalEventId()+":"+brokerId).add(manualCategoryTicket);
					continue;
				}
				catTickets = new ArrayList<ManualCategoryTicket>();
				catTickets.add(manualCategoryTicket);
				currentEventCategoryTicketsMap.put(manualCategoryTicket.getTnLocalEventId()+":"+brokerId, catTickets);
			}
			
			List<ManualCategoryTicket> dbCategoryTickets = DAORegistry.getManualCategoryTicketDAO().getAllManualCategoryTicketsByBrokerId(brokerId);
			
			if(null != dbCategoryTickets && !dbCategoryTickets.isEmpty()){
				
				System.out.println("ManualCats: BROKER MANUAL Tickets--->"+dbCategoryTickets.size());
				for (ManualCategoryTicket dbCategoryTicket : dbCategoryTickets) {
					
					List<ManualCategoryTicket> catTickets =  dbEventCategoryTicketsMap.get(dbCategoryTicket.getTnLocalEventId()+":"+brokerId);
					
					if(null != catTickets && !catTickets.isEmpty()){
						dbEventCategoryTicketsMap.get(dbCategoryTicket.getTnLocalEventId()+":"+brokerId).add(dbCategoryTicket);
						continue;
					}
					catTickets = new ArrayList<ManualCategoryTicket>();
					catTickets.add(dbCategoryTicket);
					dbEventCategoryTicketsMap.put(dbCategoryTicket.getTnLocalEventId()+":"+brokerId, catTickets);
				}
			}
			
			for (String key : currentEventCategoryTicketsMap.keySet()) {
				System.out.println("ManualCats: <----BEGINS-------------------"+key+"--------------------------BEGINS--->"+new Date());
				Integer tnLocalEventId = Integer.parseInt(key.split(":")[0]);
			 try{	
				PosEvent posEvent =eventMap.get(tnLocalEventId);
				Date eventDate  = dbDateTimeFormat.parse(posEvent.getEventDateStr()),inHandDate=null;
				
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(eventDate);
				calendar.add(Calendar.DAY_OF_MONTH, -1);
				inHandDate = calendar.getTime();
				
				List<ManualCategoryTicket> currentCatTickets =  currentEventCategoryTicketsMap.get(key);
				List<ManualCategoryTicket> dbCatTickets =  dbEventCategoryTicketsMap.remove(key);
				
				Map<Integer, ManualCategoryTicket> dbTixMap =  new HashMap<Integer, ManualCategoryTicket>();
				
				if(null != dbCatTickets && !dbCatTickets.isEmpty()){
					for(ManualCategoryTicket dbTix : dbCatTickets) {
						dbTixMap.put(dbTix.getTnCategoryGroupId(), dbTix);
					}
				}
				System.out.println("ManualCats: POS currentCatTickets :"+currentCatTickets.size());
				
				List<ManualCategoryTicket> toBeSavedOrUpdatedTickets =  new ArrayList<ManualCategoryTicket>();
				DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String priceHistory = "";
				String priceHDateStr = dateTimeFormat.format(new Date());
				
				for (ManualCategoryTicket curCatTix : currentCatTickets) {
					
					ManualCategoryTicket dbTix = dbTixMap.remove(curCatTix.getTnCategoryGroupId());
					
					if(null != dbTix ){
						
						if(!dbTix.getCost().equals(curCatTix.getCost())){
							dbTix.setCost(curCatTix.getCost());
							priceHistory = priceHDateStr+"-"+curCatTix.getCost();
							if(dbTix.getPriceHistory() != null) {
								priceHistory = dbTix.getPriceHistory() +","+priceHistory;
								if(priceHistory.length()>=500) {
									priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
								}
							}
							dbTix.setPriceHistory(priceHistory);
							dbTix.setIsEdit(true);
						}
						
						if(!dbTix.getQuantity().equals(curCatTix.getQuantity()) || dbTix.getQuantity() != curCatTix.getQuantity()){
							dbTix.setIsEdit(true);
						}
						
						if(!dbTix.getSection().equals(curCatTix.getSection()) || !dbTix.getRow().equals(curCatTix.getRow())){
							dbTix.setSection(curCatTix.getSection());
							dbTix.setRow(curCatTix.getRow());
							dbTix.setIsEdit(true);
						}
						if(null != dbTix.getIsEdit() && dbTix.getIsEdit()){
							dbTix.setEventDate(curCatTix.getEventDate());
							dbTix.setEventName(curCatTix.getEventName());
							dbTix.setVenueName(curCatTix.getVenueName());
							dbTix.setInHandDate(inHandDate);
							dbTix.setLastUpdated(new Date());
							dbTix.setQuantity(curCatTix.getQuantity());
							toBeSavedOrUpdatedTickets.add(dbTix);
						}
					}else{
						curCatTix.setInHandDate(inHandDate);
						curCatTix.setCreatedDate(new Date());
						curCatTix.setLastUpdated(new Date());
						curCatTix.setBrokerId(brokerId);
						curCatTix.setStatus("ACTIVE");
						toBeSavedOrUpdatedTickets.add(curCatTix);
					}
				}
				if(null != dbTixMap && !dbTixMap.isEmpty() ){
					Collection<ManualCategoryTicket> toBeDeletedTickets =  dbTixMap.values();

					for (ManualCategoryTicket dbCatTix : toBeDeletedTickets) {
						dbCatTix.setStatus("DELETE");
						dbCatTix.setLastUpdated(new Date());
					}
					System.out.println("ManualCats: POS toBeDeletedTickets :"+toBeDeletedTickets.size());
					DAORegistry.getManualCategoryTicketDAO().updateAll(toBeDeletedTickets);
				}
				if(null != toBeSavedOrUpdatedTickets && toBeSavedOrUpdatedTickets.size() > 0){
					System.out.println("ManualCats: POS toBeSavedOrUpdatedTickets :"+toBeSavedOrUpdatedTickets.size());
					DAORegistry.getManualCategoryTicketDAO().saveOrUpdateAll(toBeSavedOrUpdatedTickets);
				}
				System.out.println("MANUALCATS: <----ENDS-------------------"+tnLocalEventId+"--------------------------ENDS--->"+new Date());
			}catch(Exception e){
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Processing this Event.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Processing Event.");
				 error.setEventId(tnLocalEventId);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("MANUALCATS 1 : Error while Processing this Event.");
				 System.err.println("MANUALCATS 1 : Error while Processing this Event.");
				 e.printStackTrace();
				 continue;
			}
		}
			
			List<String> keys = new ArrayList<String>(dbEventCategoryTicketsMap.keySet());
			if(null != keys && !keys.isEmpty()){
				for (String key : keys) {
					
					List<ManualCategoryTicket> toBeDeletedTickets =  dbEventCategoryTicketsMap.get(key);
					
					if(null != toBeDeletedTickets && !toBeDeletedTickets.isEmpty()){
						for (ManualCategoryTicket dbCatTix : toBeDeletedTickets) {
							dbCatTix.setStatus("DELETE");
							dbCatTix.setLastUpdated(new Date());
						}
						DAORegistry.getManualCategoryTicketDAO().updateAll(toBeDeletedTickets);
					}
				}
			}
		}
		String subject,fileName;
		Map<String, Object> map;
		if(isErrorOccured){
			map = new HashMap<String, Object>();
			map.put("error", error);
			map.put("errors", errorList);
			try {
				log.info("CT error size......"+errorList.size());
				
				DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
				subject = "MANUALCATS Scheduler job failed :";
				fileName = "templates/autopricing-job-failure-message.txt";
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
				
			} catch (Exception e) {
				log.error("MANUALCATS 17 : Error while Inserting Error Listings in TMAT.");
				 System.out.println("MANUALCATS 17 :Error while Inserting Error Listings in TMAT.");
				e.printStackTrace();
			}
		}
	 }
	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
				System.out.println("ManualCats Job Called.." + new Date());
				log.info("ManualCats Job Called.." + new Date());
				try{
					
					processManualCatsTickets();
					System.out.println("ManualCats Scheduler Job finished @ " + new Date());
				}catch(Exception e){
					e.printStackTrace();
				}
	}


}