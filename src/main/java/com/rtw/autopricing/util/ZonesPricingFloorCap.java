package com.rtw.autopricing.util;


public class ZonesPricingFloorCap {

	private Integer id;
	private String section;
	private Double floorCap;
	private Integer tnExchangeEventId;
	private Integer brokerId;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Integer getTnExchangeEventId() {
		return tnExchangeEventId;
	}
	public void setTnExchangeEventId(Integer tnExchangeEventId) {
		this.tnExchangeEventId = tnExchangeEventId;
	}
	public Double getFloorCap() {
		return floorCap;
	}
	public void setFloorCap(Double floorCap) {
		this.floorCap = floorCap;
	}
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
}
