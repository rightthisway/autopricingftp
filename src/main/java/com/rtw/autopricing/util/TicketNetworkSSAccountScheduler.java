package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Category;
import com.rtw.autopricing.ftp.data.DefaultPurchasePrice;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ManagePurchasePrice;
import com.rtw.autopricing.ftp.data.Site;
import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountExchangeEvent;
import com.rtw.autopricing.ftp.data.TicketNetworkSSAccountTicket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;

public class TicketNetworkSSAccountScheduler  extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(TicketNetworkSSAccountScheduler.class);
	public static Date lastUpdateTime;
	public static Date nextRunTime;
	private static Boolean running=false;
	private static Boolean stopped;
	public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 
	public static void getTicketNetworkSSAccountTickets() {
		Calendar cal =  Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.MINUTE,15);
		setNextRunTime(cal.getTime());
		
		if(isStopped() || isRunning()){
			return ;
		}
		setRunning(true);
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNSpecial");
		if(autopricingProduct==null){
			autopricingProduct = new AutopricingProduct();
			autopricingProduct.setName("TNSpecial");
			autopricingProduct.setStatus("ACTIVE");
			autopricingProduct.setStopped(false);
		}
		if(autopricingProduct.getStopped()){
			setStopped(true);
			setRunning(false);
			return;
		}
		
		
		
		Date lastRunTime = getLastUpdateTime();
		Long minute = 0l;
		if(lastRunTime!=null){
			minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
		}else{
			minute = 2880l; // 2 Days in minute
		}
		
		
		Collection<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAll();
		Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
		for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
			defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
		}
		
//		Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
//		Collection<Ticket> tempTicketList = new ArrayList<Ticket>();
//		Set<Ticket> sectionTickets = null;
		
			
		setLastUpdateTime(now);
		autopricingProduct.setLastRunTime(now);
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		
		 Map<Integer, Integer> brokerAddCountMap = new HashMap<Integer, Integer>();
		 Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
		 Map<Integer, Integer> brokerUpdateCountMap = new HashMap<Integer, Integer>();
		 
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
		int tInsert=0,tUpdate=0,tremovet=0,tposCount=0;
		int eInsert=0,eUpdate=0,eremovet=0;
		
		Collection<TicketNetworkSSAccountExchangeEvent> exEvents = null;
		Collection<Event> events = null;
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		try {
			//Get all events that is updated in tmat since last run time. 
			exEvents = DAORegistry.getTicketNetworkSSAccountExchangeEventDAO().getAllTicketNetworkSSAccountExchangeEventsEligibleForUpdate(minute);
			System.out.println("TNSPECIAL exchange Event Size : "+exEvents.size());
			log.info("TNSPECIAL exchange Event Size : "+exEvents.size());
			 
			events = DAORegistry.getEventDAO().getAllActiveEvents();
			for(Event event:events){
				eventMap.put(event.getId(),event);
			}
			 System.out.println("TNSPECIAL : TMAT event size : "+events.size());
			 log.info("TNSPECIAL : TMAT event size : "+events.size());
			 
		 } catch (Exception e) {
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Loading Events.");
//			 ////error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Event Loading.");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("TNSPECIAL 1 : Error while Loading Events.");
			 System.err.println("TNSPECIAL 1 : Error while Loading Events.");
			 e.printStackTrace();
		 }
		
		Map<Integer, Collection<Category>> categoryMap = new HashMap<Integer, Collection<Category>>();
		System.out.println("TN special job started.." + now);
		int i =0;
		List<PosEvent> posEvents = new ArrayList<PosEvent>();
		Map<Integer, List<PosEvent>> posEventsAndBrokerMap = new HashMap<Integer, List<PosEvent>> ();
		 
		for(TicketNetworkSSAccountExchangeEvent exEvent:exEvents){
			i++;
			boolean isTnEvent = false;
			boolean isUpdateTN=true;
			Integer eventId = exEvent.getEventId();
//			Integer eventId = 1000184812;
//			if(eventId != 1000184812){
//				continue;
//			}
			Broker broker = exEvent.getTicketNetworkBroker();
			Event event = eventMap.get(eventId);
			log.info("TN SPECIAL :Event: " + (i) + "/" + exEvents.size() + "::" +event.getId()+ ":" + event.getName() + ":" + event.getDate());
			if(event.getDate()==null || event.getDate().getTime()- now.getTime()<= 3* 24 * 60 * 60 * 1000){
//				System.out.println("Event skipped");
				continue;
			}
			try {
			Map<Integer,PosEvent> posEventMap = new HashMap<Integer, PosEvent>();
			Integer exBrokerId = null;
			if(exEvent.getTicketNetworkBrokerId()!=null && exEvent.getTicketNetworkBrokerId()!=0){
				isTnEvent = true;
				exBrokerId = exEvent.getTicketNetworkBrokerId();
			}else if(exEvent.getVividBrokerId()!=null && exEvent.getVividBrokerId()!=0){
				exBrokerId = exEvent.getVividBrokerId();
			}else {
				exBrokerId = exEvent.getScoreBigBrokerId();
			}
			
			if(broker != null && isUpdateTN && isTnEvent) {
				 //if(isUpdatePOS) {
				posEvents = posEventsAndBrokerMap.get(exBrokerId);
				if(posEvents == null){
					posEvents = BrokerUtils.getPOSEventByBrokerId(exBrokerId);
					posEventsAndBrokerMap.put(exBrokerId, posEvents);
				}
				if(posEvents!=null){
					for(PosEvent posEvent:posEvents){
						posEventMap.put(posEvent.getExchangeEventId(), posEvent);
					}
				}
			}
			if(posEventMap.isEmpty()) {
				isTnEvent = false;
			}
			PosEvent posEvent = posEventMap.get(event.getAdmitoneId());
			if(posEvent==null){
				isTnEvent = false;
				continue;
			}
			if(event== null || event.getVenueCategoryId()==null){
				continue;
			}
			
			
//			Integer addCount=0;
			boolean isPOCreated = false;
			Integer  purchaseOrderId = null;
			 
			 
			Collection<ManagePurchasePrice> managePurchasePricelist = DAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());//tour.getTmatPurchaseId());
			Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
			for(ManagePurchasePrice tourPrice:managePurchasePricelist){
				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
			}
			
			Map<String, List<Ticket>> ticketEvolutonMap = new HashMap<String, List<Ticket>>();
			Map<String, List<Ticket>> ticketNetworkMap = new HashMap<String, List<Ticket>>();
			
			List<TicketNetworkSSAccountTicket> catTix = new ArrayList<TicketNetworkSSAccountTicket>();
//			Collection<Ticket> proTickets = DAORegistry.getTicketDAO().getAllTNSpecialTicketsByEventAndSiteId(exEvent.getEventId());//, Site.TICKET_EVOLUTION);
//			Collection<Ticket> proTickets = DAORegistry.getTicketDAO().getAllTicketsByEventAndSiteId(eventId, Site.TICKET_EVOLUTION);
//			Collection<Ticket> compareTickets = DAORegistry.getTicketDAO().getAllTicketsByEventAndSiteId(eventId, Site.TICKET_NETWORK_DIRECT);
			Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllEligibleTNSpecailActiveTicketByEventId(eventId);
			Collection<Category> categories = categoryMap.get(event.getVenueCategoryId());
			categories = categoryMap.get(event.getVenueCategoryId());
			if(categories==null){
				categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
				categoryMap.put(event.getVenueCategoryId(), categories);
			}
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar expectedCal = Calendar.getInstance();
				expectedCal.setTime(event.getLocalDate());
				expectedCal.add(Calendar.DAY_OF_MONTH, - exEvent.getShippingDays());
				expectedArrivalDate = cal.getTime();
			}
			try {
				tickets = TicketUtil.preAssignCategoriesToTickets(tickets, event, categories);
				for(Ticket ticket:tickets){
					if(ticket.getSiteId().equalsIgnoreCase(Site.TICKET_EVOLUTION)){
						if(ticket.getCurrentPrice()<=5000 && ticket.getNormalizedSection().length()<30){
							String key = ticket.getEventId() +"-" + ticket.getNormalizedSection() + "-" + ticket.getNormalizedRow() + "-" + ticket.getQuantity();
							List<Ticket> subTicket =  ticketEvolutonMap.get(key);
							if(subTicket==null){
								subTicket = new ArrayList<Ticket>();
							}
							subTicket.add(ticket);
							ticketEvolutonMap.put(key, subTicket);
						}
					}else if(ticket.getSiteId().equalsIgnoreCase(Site.TICKET_NETWORK_DIRECT)){
						String key = ticket.getEventId() +"-" + ticket.getNormalizedSection() + "-" + ticket.getNormalizedRow() + "-" + ticket.getQuantity();
						List<Ticket> subTicket =  ticketNetworkMap.get(key);
						if(subTicket==null){
							subTicket = new ArrayList<Ticket>();
						}
						subTicket.add(ticket);
						ticketNetworkMap.put(key, subTicket);
					}
					
					
				}
//				proTickets = TicketUtil.removeFakeTicketsByCategoryandSection(proTickets, 200);
				
				for(String key:ticketEvolutonMap.keySet()){
					Collection<Ticket> proTicketList = ticketEvolutonMap.get(key);
					proTicketList = TicketUtil.removeFakeTicketsByCategoryandSection(proTicketList, 200);
					List<Ticket> compareTicketList = ticketNetworkMap.get(key);
//					As per new requirement no need to compare price or seat.. if quantity ,section and row matches consider that ticket as exist..
					if(compareTicketList!=null && !compareTicketList.isEmpty()){
						continue;
					}
					for(Ticket pt: proTicketList){
						boolean flag = true;
						
						/*if(compareTicketList!=null){
							for(Ticket ct:compareTicketList){
								if((pt.getSeat()!=null && ct.getSeat()!=null) && (!pt.getSeat().isEmpty()) && pt.getSeat().equals(ct.getSeat())){
									flag = false;
									break;
								}else{
									if((ct.getCurrentPrice()-ct.getCurrentPrice()*0.1) <=pt.getCurrentPrice() && ct.getCurrentPrice()*1.1 >=pt.getCurrentPrice()){
										flag = false;
										break;
									}
								}
							}
						}*/
						if(flag){
//							pt.setPurchasePrice(pt.getCurrentPrice()*1.2);
							double markup =0d;
							double shipping =0d;
							if(pt.getPurchasePrice()>=exEvent.getPriceBreakup()){
								markup = exEvent.getUpperMarkup();
//								shipping = exEvent.getUpperShippingFees();
							}else{
								markup = exEvent.getLowerMarkup();
//								shipping = exEvent.getLowerShippingFees();
							}
							TicketUtil.getPurchasePrice(pt, defaultPurchasePriceMap, null, tourPriceMap);
							if(pt.getPurchasePrice()* markup > 5000){
								continue;
							}
							TicketNetworkSSAccountTicket ticketNetworkSSAccountTicket = new TicketNetworkSSAccountTicket(pt);
							
							
							double tnPrice = (Math.round(pt.getCurrentPrice()*(1+markup/100) * 100.0) / 100.0) + (shipping / pt.getQuantity());
							ticketNetworkSSAccountTicket.setTnPrice(tnPrice);
							ticketNetworkSSAccountTicket.setExpectedArrivalDate(expectedArrivalDate);
							
							//Tamil : hardcoding shipping method as Default Website Setting(0) and nearterm display option as Standard delivery until near-term(0)
							ticketNetworkSSAccountTicket.setNearTermOptionId(0);
							ticketNetworkSSAccountTicket.setShippingMethodSpecialId(0);
							//ticketNetworkSSAccountTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption());
							//ticketNetworkSSAccountTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							
							ticketNetworkSSAccountTicket.setTnBrokerId(broker.getId());
							catTix.add(ticketNetworkSSAccountTicket);
						}
					}
				}
				List<TicketNetworkSSAccountTicket> dbtickets = DAORegistry.getTicketNetworkSSAccountTicketDAO().getAllTNTicketNetworkSSAccountTicketsByEventId(event.getId());
				Map<Integer, TicketNetworkSSAccountTicket> dbTixMap = new HashMap<Integer, TicketNetworkSSAccountTicket>();
				
				for(TicketNetworkSSAccountTicket dbTix:dbtickets){
					dbTixMap.put(dbTix.getTicketId(), dbTix);
				}
				List<TicketNetworkSSAccountTicket> toBeUpdateicket = new ArrayList<TicketNetworkSSAccountTicket>();
				if(broker != null && isUpdateTN && isTnEvent) {
					 
				int addCount=0;
				for(TicketNetworkSSAccountTicket accountTicket:catTix){
					TicketNetworkSSAccountTicket dbTix = dbTixMap.remove(accountTicket.getTicketId());
					if(dbTix==null){
						if(isTnEvent && isUpdateTN){
							if(Counter.isMinuteAddAllow(broker)){
//								 System.out.println("LarryLast API ADD begins....."+new Date()+"...count.."+addCount);
								 
								 if(purchaseOrderId == null || purchaseOrderId == 0) {
									 try {
										 purchaseOrderId = InduxDAORegistry.getPosTicketGroupDAO().getPurchaseOrderIdByExchangeEventId(broker,event.getAdmitoneId(),autopricingProduct);
										 
										 if(purchaseOrderId == null || purchaseOrderId==0) {
											 purchaseOrderId = InduxDAORegistry.getPurchaseOrderDAO().creatrePurchaseOrder(broker.getPosBrokerId());
											 System.out.println("Purchase Order Id:" + purchaseOrderId);
											 if(purchaseOrderId == -1){
												 break;
											 }
											 tposCount++;
											 isPOCreated = true;
										 }
											 
									 } catch (Exception e) {
										 isErrorOccured = true;
										 error = new AutoPricingError();
										 error.setProductId(autopricingProduct.getId());
										 error.setMessage("Error while Creating Purchase Order. "+"Event:" + eventId+ ":" +"Id:(" + accountTicket.getQuantity() +":" + accountTicket.getSection() +":" + accountTicket.getLastRow() +")");
//										 //error.setExample(""+e.fillInStackTrace());
										 error.setProcess("Purchase Order Creation.");
										 error.setEventId(eventId);
										 error.setTimeStamp(new Date());
										 errorList.add(error);
										 log.error("Larry 9 : Error while Creating Purchase Order. "+"Event:" + eventId+ ":" +"Id:(" + accountTicket.getQuantity() +":" + accountTicket.getSection() +":" + accountTicket.getLastRow() +")");
										 System.err.println("Larry 9 : Error while Creating Purchase Order. "+"Event:" + eventId+ ":" +"Id:(" + accountTicket.getQuantity() +":" + accountTicket.getSection() +":" + accountTicket.getLastRow() +")");
										 e.printStackTrace();
									}
								 }
							 
								 try {
									 if(InduxDAORegistry.getPosTicketGroupDAO().addTicketGroupInPOS(broker.getPosBrokerId(),accountTicket,autopricingProduct,posEvent,purchaseOrderId)){
										 broker.addMinuteUpdateCounter();
										 eInsert++;
										 addCount++;
									 }
								 } catch(Exception e) {
									 e.printStackTrace();
									 isErrorOccured = true;
									 error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									 error.setMessage("Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + accountTicket.getQuantity() +":" + accountTicket.getSection() +":" + accountTicket.getLastRow() +")");
//									 //error.setExample(""+e.fillInStackTrace());
									 error.setProcess("Ticket Group Creation..");
									 error.setEventId(eventId);
									 error.setTimeStamp(new Date());
									 errorList.add(error);
									 log.error("Larry 10 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + accountTicket.getQuantity() +":" + accountTicket.getSection() +":" + accountTicket.getLastRow() +")");
									 System.err.println("Larry 10 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + accountTicket.getQuantity() +":" + accountTicket.getSection() +":" + accountTicket.getLastRow() +")");
									 
								 }
								 
								 try {
									 if(addCount==0 && isPOCreated) {
										 log.info("Purchase ORDER DELETE - " + purchaseOrderId);
										 InduxDAORegistry.getPurchaseOrderDAO().deleteById(broker.getPosBrokerId(),purchaseOrderId);
										 tposCount--;
									 }
								 } catch (Exception e) {
									 isErrorOccured = true;
									 error = new AutoPricingError();
									 error.setProductId(autopricingProduct.getId());
									 error.setMessage("Error while Deleting PO."+purchaseOrderId);
//									 //error.setExample(""+e.fillInStackTrace());
									 error.setProcess("PO Deletion..");
									 error.setEventId(eventId);
									 error.setTimeStamp(new Date());
									 errorList.add(error);
									 log.error("Larry 11 : Error while Deleting PO."+eventId+" : PO :"+purchaseOrderId);
									 System.err.println("Larry 11 : Error while Deleting PO."+eventId+" : PO :"+purchaseOrderId);
									 e.printStackTrace();
								 }
							 }
						}
						toBeUpdateicket.add(accountTicket);
							
					}else if(!dbTix.getActualPrice().equals(accountTicket.getActualPrice())){
						if(isTnEvent && isUpdateTN){
							if(!dbTix.getTnPrice().equals(accountTicket.getTnPrice())){
								if(Counter.isMinuteAddAllow(broker)){
									try {
										 if(dbTix.getTnBrokerId()!= null) {
											 if(!dbTix.getTnBrokerId().equals(exEvent.getTicketNetworkBrokerId())){
												 dbTix.setStatus("DELETED");
												 TicketNetworkSSAccountTicket ticketNetworkSSAccountTicket =  new TicketNetworkSSAccountTicket(dbTix);
												 ticketNetworkSSAccountTicket.setTnBrokerId(exEvent.getTicketNetworkBrokerId());
												 toBeUpdateicket.add(ticketNetworkSSAccountTicket);
//												 String key = larryTicket.getQuantity()+":"+ larryTicket.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+larryTicket.getLastRow().replaceAll("\\s+", " ").trim().toLowerCase();
//												 exFinalCatTixMap.put(key, larryTicket);
												 continue;
											 }
										 }else{
											 dbTix.setTnBrokerId(exEvent.getTicketNetworkBrokerId());
										 }
									 	if(Counter.isMinuteUpdateAllow(broker)) {
//											System.out.println("CT API Update begins....."+new Date()+"...count.."+tUpdate);
											InduxDAORegistry.getPosTicketGroupDAO().updateTicketGroup(broker.getPosBrokerId(), dbTix);
											broker.addMinuteUpdateCounter();
											tUpdate++;
										}
									} catch (Exception e) {
											isErrorOccured = true;
											error = new AutoPricingError();
											 error.setProductId(autopricingProduct.getId());
											error.setMessage("" + e.fillInStackTrace());
											error.setProcess("POS Update Category.");
											error.setEventId(eventId);
											error.setTimeStamp(new Date());
											errorList.add(error);
											log.error("Larry 12 : Error While POS Update:"+"Event:" + eventId+ ":" +"Id:(" + dbTix.getQuantity() +":" + dbTix.getSection() +":" + dbTix.getLastRow() + "):Msg:" + e.fillInStackTrace());
											System.err.println("Larry 12 : Error From POS Update :"+"Event:" + eventId+ ":" +"Id:(" + dbTix.getQuantity() +":" + dbTix.getSection() +":" + dbTix.getLastRow() + "):Msg:" + e.fillInStackTrace());
											e.printStackTrace();
									}	
								}
							}
						}
						dbTix.setActualPrice(accountTicket.getActualPrice());
						dbTix.setTnPrice(accountTicket.getTnPrice());
						toBeUpdateicket.add(dbTix);
					}
				}
				 }
				for(TicketNetworkSSAccountTicket dbTix:dbTixMap.values()){
					if(isTnEvent && isUpdateTN){
						if(Counter.isMinuteDeleteAllow(broker)) {
	//						System.out.println("LarryLast Event(1) :delete begins....."+new Date()+"...count.."+eremovet);
							InduxDAORegistry.getPosTicketGroupDAO().deleteTicketGroupsByPosTicketGroupId(broker,dbTix.getTnTicketGroupId());
							broker.addMinuteUpdateCounter();
							eremovet++;
						}
					}
					dbTix.setStatus("DELETED");
					toBeUpdateicket.add(dbTix);
				}
				if(broker!=null){
					Integer addCount = brokerAddCountMap.get(broker.getId());
					 if(addCount==null){
						 addCount = 0;
					 }
					 addCount= addCount + eInsert;
					 brokerAddCountMap.put(broker.getId(),addCount);
					 
					 Integer deleteCount = brokerDeleteCountMap.get(broker.getId());
					 if(deleteCount==null){
						 deleteCount = 0;
					 }
					 deleteCount= deleteCount + eremovet;
					 brokerDeleteCountMap.put(broker.getId(),deleteCount);
					 
					 Integer updateCount = brokerUpdateCountMap.get(broker.getId());	 
					 if(updateCount==null){
						 updateCount = 0;
					 }
					 updateCount= updateCount + eUpdate;
					 brokerUpdateCountMap.put(broker.getId(),updateCount);
				 }else{
					 System.out.println("==================== ERROR ======= BROKER IS NULL ==== TN SPECIAL PRODUCT..===");
				 }
				DAORegistry.getTicketNetworkSSAccountTicketDAO().saveOrUpdateAll(toBeUpdateicket);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}catch (Exception e) {
			isErrorOccured = true;
			error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			error.setMessage("" + e.fillInStackTrace());
			error.setProcess("Error While processing Event"+eventId);
			error.setEventId(eventId);
			error.setTimeStamp(new Date());
			errorList.add(error);
			log.error("Error While processing Event"+eventId + "):Msg:" + e.fillInStackTrace());
			System.err.println("Error While processing Event"+eventId + "):Msg:" + e.fillInStackTrace());
			e.printStackTrace();
		}
		}
		running = false;
		List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
		
		for(Integer brokerId:brokerAddCountMap.keySet()){
			int count = brokerAddCountMap.get(brokerId);
			if(count==0){
				continue;
			}
			AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			addAudit.setProduct(autopricingProduct);
			addAudit.setBroker(broker);
			addAudit.setCount(count);
			addAudit.setProcessType("Insert");
			addAudit.setLastRunTime(getLastUpdateTime());
			autoCatsProjectAuditList.add(addAudit);
			
		}
		
		for(Integer brokerId:brokerUpdateCountMap.keySet()){
			int count = brokerUpdateCountMap.get(brokerId);
			if(count==0){
				continue;
			}
			AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			addAudit.setProduct(autopricingProduct);
			addAudit.setBroker(broker);
			addAudit.setCount(count);
			addAudit.setProcessType("Update");
			addAudit.setLastRunTime(getLastUpdateTime());
			autoCatsProjectAuditList.add(addAudit);
			
		}
		
		for(Integer brokerId:brokerDeleteCountMap.keySet()){
			int count = brokerDeleteCountMap.get(brokerId);
			if(count==0){
				continue;
			}
			AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
			Broker broker = BrokerUtils.getBrokerById(brokerId);
			addAudit.setProduct(autopricingProduct);
			addAudit.setBroker(broker);
			addAudit.setCount(count);
			addAudit.setProcessType("Delete");
			addAudit.setLastRunTime(getLastUpdateTime());
			autoCatsProjectAuditList.add(addAudit);
			
		}
		DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);
		DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
		if(isErrorOccured){
			String subject,fileName;
			Map<String, Object> map;
			map = new HashMap<String, Object>();
			map.put("error", error);
			map.put("errors", errorList);
			try {
				log.info("CT error size......"+errorList.size());
				
				DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
				subject = "TNSpecial Ticket Cleaner job failed :";
				fileName = "templates/autopricing-job-failure-message.txt";
				
				//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
				
				EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
				
			} catch (Exception e) {
				log.error("TN Special C  8 : Error while Inserting Error Listings in TMAT.");
				 System.err.println("TN Special C  8 : Error while Inserting Error Listings in TMAT.");
				e.printStackTrace();
			}
		}
		System.out.println("TN special job finished.. " + new Date());
	}

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		getTicketNetworkSSAccountTickets();
		
	}
	
	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		TicketNetworkSSAccountScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		TicketNetworkSSAccountScheduler.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("TNSpecial").getLastRunTime();
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		TicketNetworkSSAccountScheduler.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}

	public static void setNextRunTime(Date nextRunTime) {
		TicketNetworkSSAccountScheduler.nextRunTime = nextRunTime;
	}

}
