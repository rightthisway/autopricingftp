package com.rtw.autopricing.util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleAutoCatCategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleAutoCatExchangeEvent;
import com.rtw.autopricing.ftp.data.AutoCatsProjectAudit;
import com.rtw.autopricing.ftp.data.AutoPricingError;
import com.rtw.autopricing.ftp.data.AutopricingExchange;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.AutopricingSettings;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.DefaultAutoPricingProperties;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.Property;
import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.indux.dao.implementation.InduxDAORegistry;
import com.rtw.autopricing.ftp.indux.data.PosEvent;
import com.rtw.autopricing.ftp.indux.data.PosVenueCategory;


public class PresaleAutocatScheduler  extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(PresaleAutocatScheduler.class);
	
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 
	 
	 
	 public static void processPresaleAutoCatTickets() throws Exception{
		Calendar cal =  Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.MINUTE,15);
		setNextRunTime(cal.getTime());
		if(isStopped() || isRunning()){
			return ;
		}
		setRunning(true);
		
		
		Date lastRunTime = getLastUpdateTime();
		Long minute = 0l;
		if(lastRunTime!=null){
			minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
		}else{
			minute = 2880l; // 2 Days in minute
		}
		AutopricingProduct autopricingProduct = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("PresaleAutoCat");
		if(autopricingProduct==null){
			autopricingProduct = new AutopricingProduct();
			autopricingProduct.setName("PresaleAutoCat");
			autopricingProduct.setStatus("ACTIVE");
			autopricingProduct.setStopped(false);
		}
		if(autopricingProduct.getStopped()){
			setStopped(true);
			setRunning(false);
			return;
		}
		
		setLastUpdateTime(now);
		autopricingProduct.setLastRunTime(now);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
//		Date jobStartTime = new Date();
		int tInsert=0,tUpdate=0,tremovet=0;
		int eInsert=0,eUpdate=0,eremovet=0;
		
		 try{
			 Map<Integer, Integer> brokerAddCountMap = new HashMap<Integer, Integer>();
			 Map<Integer, Integer> brokerDeleteCountMap = new HashMap<Integer, Integer>();
			 Map<Integer, Integer> brokerUpdateCountMap = new HashMap<Integer, Integer>();
			 
			 
			 
			 Double tnExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork").getAdditionalMarkup();
			 Double vividExAddMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Vividseats").getAdditionalMarkup();
			 Double scorebigMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("Scorebig").getAdditionalMarkup();
			 Double fanxchangeMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("FanXchange").getAdditionalMarkup();
			 Double ticketcityMarkup = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketCity").getAdditionalMarkup();
			 Double tickPickExAddMarkup = 0.0;
			 Double scoreBigExAddMarkup = 0.0;
			 
			 scoreBigExAddMarkup = scorebigMarkup; 
			 
			 Boolean isTnEvent = false;
			 
			 Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
			 List<PosEvent> posEvents = new ArrayList<PosEvent>();
			 Map<Integer, List<PosEvent>> posEventsAndBrokerMap = new HashMap<Integer, List<PosEvent>> ();
			 
			 Map<Integer, Map<Integer, PosEvent>> brokerPosEventMap = new HashMap<Integer, Map<Integer,PosEvent>>();
			 
			 Collection<Event> eventList = null;
			 Collection<PresaleAutoCatExchangeEvent> miniExchangeEvents = null;
			 
			 try {
				 //Get all events that is updated in tmat since last run time. 
				 miniExchangeEvents = DAORegistry.getPresaleAutoCatExchangeEventDAO().getAllPresaleAutoCatExchangeEventsEligibleForUpdate(minute,autopricingProduct,false);
				 System.out.println("PresaleAutocat Event Size : "+miniExchangeEvents.size());
				 
				 eventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentId();
				 for(Event event:eventList) {
					eventMap.put(event.getId(), event);
					eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
				 }
				 System.out.println("PresaleAutocat : TMAT event size : "+eventList.size());
				 
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("PresaleAutocat 1 : Error while Loading Events.");
				 System.err.println("PresaleAutocat 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			
			 int eventSize = miniExchangeEvents.size();
			 Integer i=0;
			 Map<Integer,DefaultAutoPricingProperties> defaultAutoPricingsMap = new HashMap<Integer, DefaultAutoPricingProperties>();
			 List<DefaultAutoPricingProperties> defaultAutoPricingsFromDB = DAORegistry.getDefaultAutoPricingPropertiesDAO().getDefaultAutoPricingPropertiesByProductId(autopricingProduct.getId());
			 for (DefaultAutoPricingProperties defaultAutopriicngSettings : defaultAutoPricingsFromDB) {
				 defaultAutoPricingsMap.put(defaultAutopriicngSettings.getParentCategoryId(),defaultAutopriicngSettings);
			 }
			 Integer minimamExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());
			 
			 
			 Map<Integer, Broker> brokerMap = BrokerUtils.getAllAutoPricingBrokers();
			 
			 for (Integer brokerId : brokerMap.keySet()) {
				 
				 Map<Integer,PosEvent> posEventMap = brokerPosEventMap.get(brokerId);
				 
				 if(null == posEventMap || posEventMap.isEmpty())
				 {
					 posEvents = posEventsAndBrokerMap.get(brokerId);
					if(posEvents == null){
						posEvents = BrokerUtils.getPOSEventByBrokerId(brokerId);
						posEventsAndBrokerMap.put(brokerId, posEvents);
					}
					
					posEventMap = new HashMap<Integer, PosEvent>();
					if(posEvents!=null){
						 for(PosEvent posEvent:posEvents){
							 posEventMap.put(posEvent.getExchangeEventId(), posEvent);
						 }
					 }
				 }
				 brokerPosEventMap.put(brokerId, posEventMap);
			 }
			 
			 for (ExchangeEvent exEvent : miniExchangeEvents) {
				 
				 boolean isZoneEvent =false;
				 List<Integer> tnBrokerIds = new ArrayList<Integer>();
				 List<Integer> activeBrokerIds = new ArrayList<Integer>();
				 List<Integer> tobeDeletedBrokerIds = new ArrayList<Integer>();
				 
				 int brokerMaxTixCount = 32,eventMaxTixCount=96;
				 if(isStopped()){
					 break;
				 }
				 isTnEvent = false;
				 
				 i++;
				 Integer eventId = exEvent.getEventId();
				 Event event = eventMap.get(eventId);
				 if(event==null ){
					 System.out.println(i+".TMAT Event Not Exisit :" + eventId);
					 continue;
				 }
				 eInsert=0;eUpdate=0;eremovet=0;
				 
				 
				 tnBrokerIds = AutoCats96Utils.getBrokerListbyParentCategory(event.getParentCategoryId());
				 
				 if(null != exEvent.getRotEnabled() && exEvent.getRotEnabled()){
					 activeBrokerIds.add(3);
				 }else{
					 tobeDeletedBrokerIds.add(3);
				 }
				 
				 if(null != exEvent.getRtwEnabled() && exEvent.getRtwEnabled()){
					 activeBrokerIds.add(5);
				 }else{
					 tobeDeletedBrokerIds.add(5);
				 }
				 
				 System.out.println("PresaleAutocat Even:" + i+"/"+eventSize+" .Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate());
				 
				 for (Integer deleteBrokerId : tobeDeletedBrokerIds) {
					 
					 isTnEvent = true;
					 Date currentTime = new Date();
					 Broker broker = null;
					 AutopricingSettings autopricingSettings = null;
					 AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
					 if(isTnEvent){
						 broker = BrokerUtils.getBrokerById(deleteBrokerId);
						 if(broker!=null){
							 autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
						 }
					 }
					 boolean isUpdateTN=true;
					 if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
						isUpdateTN = false;
					 }
					 
					 if(broker != null && isUpdateTN) {
						 
						 List<PresaleAutoCatCategoryTicket> miniTickets = DAORegistry.getPresaleAutoCatCategoryTicketDAO().getAllTNPresaleAutoCatCategoryTicketsByEventIdByBrokerID(eventId,deleteBrokerId);
						
						 if(null == miniTickets || miniTickets.isEmpty()){
							 continue;
						 }
						 System.out.println("DELETING: BROKER IS DISABLED: Broker Name: "+broker.getName()+" " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
						 for(PresaleAutoCatCategoryTicket cat:miniTickets){
							 	
								if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) { 
									continue;
								}
								try {
									if(Counter.isMinuteDeleteAllow(broker)) {
										InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
										broker.addMinuteUpdateCounter();
										eremovet++;
										cat.setStatus("DELETED");
										cat.setLastUpdated(currentTime);
									}
								} catch (Exception e) {
										isErrorOccured = true;
										error = new AutoPricingError();
										 error.setProductId(autopricingProduct.getId());
										error.setMessage("" + e.fillInStackTrace());
										error.setProcess("POS Remove Category.");
										error.setEventId(eventId);
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("PresaleAutocat 2 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
										System.err.println("PresaleAutocat 2 : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
								}
							}
							DAORegistry.getPresaleAutoCatCategoryTicketDAO().updateAll(miniTickets);
					 }
				}
				 
				 for (Integer activeBrokerId : activeBrokerIds) {
					
					 isTnEvent = true;
					 Date currentTime = new Date();
					 Broker broker = null;
					 AutopricingSettings autopricingSettings = null;
					 AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
					 if(isTnEvent){
						 broker = BrokerUtils.getBrokerById(activeBrokerId);
						 if(broker!=null){
							 autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
						 }
					 }
					 boolean isUpdateTN=true;
					 if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
						isUpdateTN = false;
					 }
					 
					 if(exEvent.getZone() == null || exEvent.getZone()) {
						 
						 isZoneEvent = true;
						 
						 try { // Remove all tickets of event if event is not zone , event is not active or there is restriction to update POS.
							  if(broker != null && isUpdateTN) {
								 List<PresaleAutoCatCategoryTicket> miniTickets = DAORegistry.getPresaleAutoCatCategoryTicketDAO().getAllTNPresaleAutoCatCategoryTicketsByEventIdByBrokerID(eventId,activeBrokerId);
								 if(null == miniTickets || miniTickets.isEmpty()){
									continue;
								 }
								 System.out.println("DELETING: EX EVENT IS ZONE EVENT " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
								 for(PresaleAutoCatCategoryTicket cat:miniTickets){
									 	
									if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) { 
										continue;
									}
									try {
										if(Counter.isMinuteDeleteAllow(broker)) {
											InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
											broker.addMinuteUpdateCounter();
											eremovet++;
											cat.setStatus("DELETED");
											cat.setLastUpdated(currentTime);
										}
									} catch (Exception e) {
											isErrorOccured = true;
											error = new AutoPricingError();
											error.setProductId(autopricingProduct.getId());
											error.setMessage("" + e.fillInStackTrace());
											error.setProcess("POS Remove Category.");
											error.setEventId(eventId);
											error.setTimeStamp(new Date());
											errorList.add(error);
											log.error("PresaleAutocat 2 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											System.err.println("PresaleAutocat 2 : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
									}
								}
								 DAORegistry.getPresaleAutoCatCategoryTicketDAO().updateAll(miniTickets);
							 }
						} catch (Exception e) {
							isErrorOccured = true;
							error = new AutoPricingError();
							error.setProductId(autopricingProduct.getId());
							error.setMessage("Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
							error.setExample(""+e.fillInStackTrace());
							error.setProcess("Deleting Event Listings.");
							error.setEventId(eventId);
							error.setTimeStamp(new Date());
							errorList.add(error);
							log.error("PresaleAutocat 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
							System.err.println("PresaleAutocat 3 : Error while Deleting Event Listings for Event not Found in TMAT."+eventId);
							e.printStackTrace();
						}
						isTnEvent = false;
					 }
					 
					 Map<Integer,PosEvent> posEventMap = null;
					 
					 if(broker != null && isUpdateTN && isTnEvent) {
						 posEventMap = brokerPosEventMap.get(activeBrokerId); 
					 }
					 
					 PosEvent posEvent = null;
					 
					 if(null ==posEventMap || posEventMap.isEmpty()) {
						isTnEvent = false;
					 }else{
						 posEvent = posEventMap.get(event.getAdmitoneId());
					 }
					 
					 if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || 
							 posEvent.getExchangeEventId() == 0){
						 
						 // Remove all tickets of the event if event is not in POS or not an exchange event in POS.
						if(broker != null && isUpdateTN && isTnEvent) {
							try {
								List<PresaleAutoCatCategoryTicket> miniTickets = DAORegistry.getPresaleAutoCatCategoryTicketDAO().getAllTNPresaleAutoCatCategoryTicketsByEventIdByBrokerID(event.getId(),activeBrokerId);
								if(null == miniTickets || miniTickets.isEmpty()){
									 continue;
								 }
								
								System.out.println("DELETING: NO POS EVENT " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());

								for(PresaleAutoCatCategoryTicket cat:miniTickets){
									
									if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {//|| cat.getTnPrice() <= 0.0
										continue;
									}
									try {
										if(Counter.isMinuteDeleteAllow(broker)) {
											InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
											broker.addMinuteUpdateCounter();
											eremovet++;
											cat.setStatus("DELETED");
											cat.setLastUpdated(currentTime);
										}
									} catch (Exception e) {
										e.printStackTrace();
											isErrorOccured = true;
											error = new AutoPricingError();
											 error.setProductId(autopricingProduct.getId());
											error.setMessage("" + e.fillInStackTrace());
											error.setProcess("POS Remove Category.");
											error.setEventId(eventId);
											error.setTimeStamp(new Date());
											errorList.add(error);
											log.error("PresaleAutocat 4 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											System.err.println("PresaleAutocat 4 : 104.CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
									}
								}
								DAORegistry.getPresaleAutoCatCategoryTicketDAO().updateAll(miniTickets);

							} catch (Exception e) {
								isErrorOccured = true;
								error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Deleting Event Listings for Event not Found in POS."+eventId);
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("Deleting Event Listings.");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("PresaleAutocat 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
								 System.err.println("PresaleAutocat 5 : Error while Deleting Event Listings for Event not Found in POS."+eventId);
								 e.printStackTrace();
							}
							}
							isTnEvent = false;
						}
						
						
						Date date = null;
						if(event.getLocalDate()!=null){
							date = df.parse(df.format(event.getLocalDate()));	
						}
						now = df.parse(df.format(now));
						Integer excludingEventDays = null;
						if(autopricingSettings!=null){
							excludingEventDays = autopricingSettings.getExcludeEventDays();
						}
						if(excludingEventDays==null){
							excludingEventDays = minimamExcludeEventDays ;
						}
						
						if(date!=null && ((date.getTime()-now.getTime()) <= excludingEventDays* 24 * 60 * 60 * 1000)) {//2 days events
							 // Remove all tickets of event if event is within 2 days..
							
							tobeDeletedBrokerIds.add(activeBrokerId);
							
							try {
								List<PresaleAutoCatCategoryTicket> miniTickets = DAORegistry.getPresaleAutoCatCategoryTicketDAO().getAllTNPresaleAutoCatCategoryTicketsByEventIdByBrokerID(event.getId(),activeBrokerId);
								if(null == miniTickets || miniTickets.isEmpty()){
									 continue;
								 }
								System.out.println("DELETING: EVENT WITHIN TWO DAYS " + i +".Event: " + event.getId() + "," + event.getName() + ":" + event.getFormatedDate() + ", Tix: " + miniTickets.size());
								
								if(broker != null && isUpdateTN && isTnEvent) {
									
									for(PresaleAutoCatCategoryTicket cat:miniTickets){
										if(cat.getTnCategoryTicketGroupId() == null || cat.getTnCategoryTicketGroupId() <= 0 ) {
											continue;
										}
										try {
											if(Counter.isMinuteDeleteAllow(broker)) {
												System.out.println("PresaleAutocat delete begins..3..."+new Date()+"...count.."+eremovet);
												InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),cat.getTnCategoryTicketGroupId());
												broker.addMinuteUpdateCounter();
												eremovet++;
											}
										} catch (Exception e) {
											e.printStackTrace();
											isErrorOccured = true;
											error = new AutoPricingError();
											error.setProductId(autopricingProduct.getId());
											error.setMessage("" + e.fillInStackTrace());
											error.setProcess("POS Remove Category.");
											error.setEventId(eventId);
											error.setTimeStamp(new Date());
											errorList.add(error);
											log.error("PresaleAutocat 6 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
											System.err.println("PresaleAutocat 6 : CT Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + cat.getQuantity() +":" + cat.getSection() +":" + cat.getRowRange() + "):Msg:" + e.fillInStackTrace());
										}
									}
								 }
								 DAORegistry.getPresaleAutoCatCategoryTicketDAO().updateAll(miniTickets);
								
							} catch (Exception e) {
								 isErrorOccured = true;
								 error = new AutoPricingError();
								 error.setProductId(autopricingProduct.getId());
								 error.setMessage("Error while Deleting Event Listings for Event within Few Days."+eventId);
								 error.setExample(""+e.fillInStackTrace());
								 error.setProcess("Deleting Event Listings.");
								 error.setEventId(eventId);
								 error.setTimeStamp(new Date());
								 errorList.add(error);
								 log.error("PresaleAutocat 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
								 System.err.println("PresaleAutocat 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
								 e.printStackTrace();
							}
							continue;
						}
				}
				 
				try {
					int defaultExpectedArrivialDatePriorDays = exEvent.getShippingDays();
					int sectionMinEtry = 1;
					DefaultAutoPricingProperties defaultAutopricing = defaultAutoPricingsMap.get(event.getParentCategoryId());
					if(defaultAutopricing != null) {
						sectionMinEtry = defaultAutopricing.getSectionCountTicket();
					}
					
					for (Integer deletedBrokerId : tobeDeletedBrokerIds) {
						tnBrokerIds.remove(deletedBrokerId);
					}
					
					if(null != tnBrokerIds && tnBrokerIds.size() >0 ){
						eventMaxTixCount = brokerMaxTixCount * tnBrokerIds.size();
					}else{
						continue;
					}
					
					Map<String,CategoryTicket> catTixMap = new HashMap<String, CategoryTicket>();	
					List<CategoryTicket> catTixList = CategoryGroupManager.computeCategoryTickets(event, exEvent, defaultExpectedArrivialDatePriorDays, sectionMinEtry, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeMarkup,ticketcityMarkup,autopricingProduct,null);
					 for (CategoryTicket catTix : catTixList) { // Add generated tickets in map

						 if((catTix.getQuantity() <= 3 && catTix.getTnPrice() < 75 ) ||
								 (catTix.getQuantity() > 3 && catTix.getTnPrice() < 50)){
							 continue;
						 }
						 //update user assigned shipping method
						 if(exEvent.getShippingMethod() != null) {
							 catTix.setShippingMethodSpecialId(exEvent.getShippingMethod());
						 }
						 String key = catTix.getQuantity()+":"+ catTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+catTix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase();
						 catTixMap.put(key, catTix);
					 }
					
					Collection<PresaleAutoCatCategoryTicket> catTixFromDB = DAORegistry.getPresaleAutoCatCategoryTicketDAO().getAllPresaleAutoCatCategoryTicketsByEventId(eventId);
					Map<String, CategoryTicket> catTixFromDBMap = new HashMap<String, CategoryTicket>();
					for (PresaleAutoCatCategoryTicket dbCatTix : catTixFromDB) {
						String key = dbCatTix.getQuantity()+":"+ dbCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+dbCatTix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase();
						catTixFromDBMap.put(key, dbCatTix);	
					}
					
					Map<Integer, List<CategoryTicket>> brokersTicketMap = AutoCats96CategoryTicketUtil.prioritizingCategoryTicketsByBrokerNew(catTixFromDBMap, 
							catTixMap, brokerMaxTixCount,eventMaxTixCount,tnBrokerIds);
					
					AutopricingExchange exchange = DAORegistry.getAutopricingExchangeDAO().getAutopricingExchangeByName("TicketNetwork");
					Map<Integer, AutopricingSettings> brokerAutoPricingSettingMap = new HashMap<Integer, AutopricingSettings>(); 
					Broker broker = null;
					AutopricingSettings autopricingSettings = null;
					
					for (Integer finalBrokerId  : tnBrokerIds) {
						broker = BrokerUtils.getBrokerById(finalBrokerId);
						if(broker!=null){
							autopricingSettings = DAORegistry.getAutopricingSettingsDAO().getAutopricingSettings(broker.getId(), autopricingProduct.getId(), exchange.getId());
						}
						brokerAutoPricingSettingMap.put(finalBrokerId, autopricingSettings);
					}
					
					
					for (Integer finalBrokerId : brokersTicketMap.keySet()) {
						List<CategoryTicket> tickets =brokersTicketMap.get(finalBrokerId);
						if(null ==tickets || tickets.isEmpty()){
							continue;
						}
						
						//System.out.println("PresaleAutocat : ----Broker ID : "+finalBrokerId+"-------TobeUpdatedOrDeletedTixs----"+tickets.size());
						
						broker = null;
						autopricingSettings = null;
						if(finalBrokerId == 0 ){
							List<PresaleAutoCatCategoryTicket> ticketsOne = new ArrayList<PresaleAutoCatCategoryTicket>();
							for (CategoryTicket tixObj : tickets) {
								PresaleAutoCatCategoryTicket tix = (PresaleAutoCatCategoryTicket) tixObj;
								tix.setStatus("DELETED");
								tix.setLastUpdated(new Date());
								ticketsOne.add(tix);
								eremovet++;
								try {
									broker = BrokerUtils.getBrokerById(tix.getTnBrokerId());
									autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
									boolean isUpdateTN=true;
									if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
										isUpdateTN = false;
									}
									if(isUpdateTN){
										if(tix.getTnCategoryTicketGroupId()!=null){	
											 if(Counter.isMinuteDeleteAllow(broker)) {
												 InduxDAORegistry.getPosCategoryTicketGroupDAO().deleteTicketGroupsByTNCategoryTicketGroupId(broker.getPosBrokerId(),tix.getTnCategoryTicketGroupId());
												 broker.addMinuteUpdateCounter();
												 eremovet++;
											}
										 }
									}
								 } catch (Exception e) {
									e.printStackTrace();
									isErrorOccured = true;
									error = new AutoPricingError();
									error.setProductId(autopricingProduct.getId());
									error.setMessage("" + e.fillInStackTrace());
									error.setProcess("POS Remove Category.");
									error.setEventId(eventId);
									error.setTimeStamp(new Date());
									errorList.add(error);
									log.error("PresaleAutocat 14 : Error While Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() + "):Msg:" + e.fillInStackTrace());
									System.out.println("PresaleAutocat 14 : Error From POS API : Remove Category:"+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() + "):Msg:" + e.fillInStackTrace());
								 }
							}
							DAORegistry.getPresaleAutoCatCategoryTicketDAO().updateAll(ticketsOne);
							continue;
						}
						
						isTnEvent = true;
						broker = null;
						autopricingSettings = null;
						broker = BrokerUtils.getBrokerById(finalBrokerId);
						if(null != broker){
							autopricingSettings = brokerAutoPricingSettingMap.get(broker.getId());
						}
						
						boolean isUpdateTN=true,isBrokerEnabled=false;
						if(autopricingSettings != null && !autopricingSettings.getIsEnabled()) {
							isUpdateTN = false;
						}
						
						if(activeBrokerIds.contains(finalBrokerId)){
							isBrokerEnabled=true;
						}else{
							//continue;
						}
						
						Map<Integer,PosEvent> posEventMap = brokerPosEventMap.get(finalBrokerId);
						PosEvent posEvent = null;
						if(null == posEventMap || posEventMap.isEmpty()){
							isTnEvent = false;
						}else{
							posEvent = posEventMap.get(event.getAdmitoneId());
						}
						
						if(posEvent == null || null == posEvent.getExchangeEventId() || posEvent.getExchangeEventId() == -1 || 
								 posEvent.getExchangeEventId() == 0){
							isTnEvent = false;
						}
						
						Map<String, Integer> posVenueCatIdMap = new HashMap<String, Integer>();
						if(isTnEvent && !isZoneEvent){
							try {
								List<PosVenueCategory> posVenueCatList = InduxDAORegistry.getPosVenueCategoryDAO().getAllVenueCategorysByVenueId(posEvent.getVenueId(),broker.getPosBrokerId());
								for (PosVenueCategory posVenueCategory : posVenueCatList) {
									String key = posVenueCategory.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+posVenueCategory.getRow().replaceAll("\\s+", " ").trim().toLowerCase();
									posVenueCatIdMap.put(key, posVenueCategory.getId());
								}
							} catch (Exception e) {
								e.printStackTrace();
								isErrorOccured = true;
								error = new AutoPricingError();
								error.setProductId(autopricingProduct.getId());
								error.setMessage(""+e.fillInStackTrace());
								error.setExample("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ": While loading POS VenueCategors :ErrorMsg:" + e.fillInStackTrace());
								error.setProcess("Loading POS VenueCategors.");
								error.setEventId(event.getId());
								error.setTimeStamp(new Date());
								errorList.add(error);
								System.err.println("PresaleAutocat 9 : NF Error From While Loading POS VenueCategors.."+eventId);
							}
						}
						
						List<PresaleAutoCatCategoryTicket> toBeSavedOrUpdatedTixs = new ArrayList<PresaleAutoCatCategoryTicket>();
						
						for (CategoryTicket tixObj : tickets) {
							PresaleAutoCatCategoryTicket tix = (PresaleAutoCatCategoryTicket)tixObj;
							
							tix.setTnBrokerId(finalBrokerId);
							//tix.setProductName(productName);
							tix.setLastUpdated(new Date());
							if(null != tix.getTnCategoryTicketGroupId()){
								
								if(null != tix.getIsTnUpdate() && tix.getIsTnUpdate()){
									
									if(isTnEvent && isUpdateTN && isBrokerEnabled && !isZoneEvent){ 
										
										InduxDAORegistry.getPosCategoryTicketGroupDAO().updateCategoryTicketGroup(tix,broker.getPosBrokerId());
										broker.addMinuteUpdateCounter();
										eUpdate++;
									}
									
								}
								if(null != tix.getIsEdited() && tix.getIsEdited()){
									toBeSavedOrUpdatedTixs.add(tix);
								}
							}else{
								if(isTnEvent  && isUpdateTN && isBrokerEnabled && !isZoneEvent){
									 tix.setPosVenueId(posEvent.getVenueId());
									 tix.setPosEventId(posEvent.getId());	
									 Integer posVenueCategoryId = posVenueCatIdMap.get(tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase());
									 if(null == posVenueCategoryId || posVenueCategoryId == -1) {
										 tix.setRowRange(tix.getRowRange());
										posVenueCategoryId = InduxDAORegistry.getPosVenueCategoryDAO().save(tix,broker.getPosBrokerId(),autopricingProduct);
										String posVenueCategorykey = tix.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+tix.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase();
										posVenueCatIdMap.put(posVenueCategorykey, posVenueCategoryId);
									}
									if(null == posVenueCategoryId || posVenueCategoryId == -1) {
										isErrorOccured = true;
										error = new AutoPricingError();
										error.setProductId(autopricingProduct.getId());
										error.setMessage("Error While Creating VenueCategory");
										error.setExample("Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
										error.setProcess("ADD Venuecategory.");
										error.setEventId(event.getId());
										error.setTimeStamp(new Date());
										errorList.add(error);
										log.error("PresaleAutocat 11 :Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
										System.err.println("PresaleAutocat 11 : Error From While Loading POS VenueCategors.."+eventId+ ":" +"ID:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
										continue;
									}
									tix.setPosVenueCategoryId(posVenueCategoryId);
									tix.setPosExchangeEventId(posEvent.getExchangeEventId());
									 
									if(Counter.isMinuteAddAllow(broker)){
										try {
											//autopricingProduct.setInternalNotes(tix.getInternalNotes());
											
											Integer tnCategoryTicketGroupId = InduxDAORegistry.getPosCategoryTicketGroupDAO().save(autopricingProduct, tix,broker.getPosBrokerId());
											if(tnCategoryTicketGroupId!=-1l){
												tix.setTnCategoryTicketGroupId(tnCategoryTicketGroupId);
												broker.addMinuteUpdateCounter();
												eInsert++;
											}
										 } catch(Exception e) {
											 isErrorOccured = true;
											 error = new AutoPricingError();
											 error.setProductId(autopricingProduct.getId());
											 error.setMessage("Error while creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
											 error.setProcess("Ticket Group Creation..");
											 error.setEventId(eventId);
											 error.setTimeStamp(new Date());
											 errorList.add(error);
											 log.error("PresaleAutocat 12 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
											 System.out.println("PresaleAutocat 12 : Error while Creating Ticket Group. "+"Event:" + eventId+ ":" +"Id:(" + tix.getQuantity() +":" + tix.getSection() +":" + tix.getRowRange() +")");
											 e.printStackTrace();
										 }
									}
								 }
								tix.setCreatedDate(new Date());
								toBeSavedOrUpdatedTixs.add(tix);
							}
							
							if(broker!=null){
								 Integer addCount = brokerAddCountMap.get(broker.getId());
								 if(addCount==null){
									 addCount = 0;
								 }
								 addCount= addCount + eInsert;
								 brokerAddCountMap.put(broker.getId(),addCount);
								 
								 Integer deleteCount = brokerDeleteCountMap.get(broker.getId());
								 if(deleteCount==null){
									 deleteCount = 0;
								 }
								 deleteCount= deleteCount + eremovet;
								 brokerDeleteCountMap.put(broker.getId(),deleteCount);
								 
								 Integer updateCount = brokerUpdateCountMap.get(broker.getId());	 
								 if(updateCount==null){
									 updateCount = 0;
								 }
								 updateCount= updateCount + eUpdate;
								 brokerUpdateCountMap.put(broker.getId(),updateCount);
							}
						}
						DAORegistry.getPresaleAutoCatCategoryTicketDAO().saveOrUpdateAll(toBeSavedOrUpdatedTixs);
					}
				} catch (Exception e) {
					 isErrorOccured = true;
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Computing Category Tickets."+eventId);
					 error.setProcess("Category Ticket Computation.");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("PresaleAutocat 8 :Error while Computing Category Tickets."+eventId);
					 System.err.println("PresaleAutocat 8 : Error while Computing Category Tickets."+eventId);
					 e.printStackTrace();
				}
				tInsert += eInsert;
				tUpdate += eUpdate;
				tremovet += eremovet;
				 
				System.out.println("PresaleAutocat  : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet);
			}
			
			List<AutoCatsProjectAudit> autoCatsProjectAuditList= new ArrayList<AutoCatsProjectAudit>();
			
			for(Integer brokerId:brokerAddCountMap.keySet()){
				int count = brokerAddCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Insert");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
			}
			
			for(Integer brokerId:brokerUpdateCountMap.keySet()){
				int count = brokerUpdateCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Update");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			
			for(Integer brokerId:brokerDeleteCountMap.keySet()){
				int count = brokerDeleteCountMap.get(brokerId);
				if(count==0){
					continue;
				}
				AutoCatsProjectAudit addAudit = new AutoCatsProjectAudit();
				Broker broker = BrokerUtils.getBrokerById(brokerId);
				addAudit.setProduct(autopricingProduct);
				addAudit.setBroker(broker);
				addAudit.setCount(count);
				addAudit.setProcessType("Delete");
				addAudit.setLastRunTime(getLastUpdateTime());
				autoCatsProjectAuditList.add(addAudit);
				
			}
			DAORegistry.getAutoCatsProjectAuditDAO().saveAll(autoCatsProjectAuditList);
			DAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
			System.out.println("PresaleAutocat Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet);
			
			running = false; 
		 }catch(Exception e){
			 running = false;
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Loading Common Properties.");
			 error.setProcess("Loading common Properties..");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("PresaleAutocat 16 :Error while Loading Common Properties.");
			 System.out.println("PresaleAutocat 16 : Error while Loading Common Properties.");
			 e.printStackTrace();
		 }
		 
		 String subject,fileName;
		 Map<String, Object> map;
			
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {
					log.info("CT error size......"+errorList.size());
					DAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
					subject = "PresaleAutoCat Scheduler job failed :";
					fileName = "templates/autopricing-job-failure-message.txt";
					EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
				} catch (Exception e) {
					log.error("PresaleAutocat 17 : Error while Inserting Error Listings in TMAT.");
					 System.out.println("PresaleAutocat 17 :Error while Inserting Error Listings in TMAT.");
					e.printStackTrace();
				}
			}
	 }
	 
	 
	 @Override
	protected void executeInternal(JobExecutionContext context)throws JobExecutionException {
		System.out.println("PresaleAutocat Job Called.." + new Date() + ": " + running);
		log.info("PresaleAutocat Job Called.." + new Date() + ": " + running);
		try{
			processPresaleAutoCatTickets();
			System.out.println("PresaleAutocat Scheduler Job finished @ " + new Date());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}

	public static void setStopped(Boolean stopped) {
		PresaleAutocatScheduler.stopped = stopped;
	}

	public static Boolean isRunning() {
		return running;
	}

	public static void setRunning(Boolean running) {
		PresaleAutocatScheduler.running = running;
	}

	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = DAORegistry.getAutopricingProductDAO().getAutopricingProductByName("PresaleAutoCat").getLastRunTime();
		}
		return lastUpdateTime;
	}

	public static void setLastUpdateTime(Date lastUpdateTime) {
		PresaleAutocatScheduler.lastUpdateTime = lastUpdateTime;
	}

	public static Date getNextRunTime() {
		return nextRunTime;
	}

	public static void setNextRunTime(Date nextRunTime) {
		PresaleAutocatScheduler.nextRunTime = nextRunTime;
	}

}