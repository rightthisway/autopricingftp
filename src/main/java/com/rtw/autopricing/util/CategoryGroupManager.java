package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.autopricing.ftp.dao.implementaion.DAORegistry;
import com.rtw.autopricing.ftp.data.AutoCats96CategoryTicket;
import com.rtw.autopricing.ftp.data.AutopricingProduct;
import com.rtw.autopricing.ftp.data.Broker;
import com.rtw.autopricing.ftp.data.Category;
import com.rtw.autopricing.ftp.data.CategoryMapping;
import com.rtw.autopricing.ftp.data.CrownJewelCategoryTicket;
import com.rtw.autopricing.ftp.data.DefaultPurchasePrice;
import com.rtw.autopricing.ftp.data.Event;
import com.rtw.autopricing.ftp.data.ExchangeEvent;
import com.rtw.autopricing.ftp.data.LarryLastCategoryTicket;
import com.rtw.autopricing.ftp.data.LastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.ManagePurchasePrice;
import com.rtw.autopricing.ftp.data.ManhattanZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.data.MiniCategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleAutoCatCategoryTicket;
import com.rtw.autopricing.ftp.data.PresaleZoneTicketsCategoryTicket;
import com.rtw.autopricing.ftp.data.RTFLastRowCategoryTicket;
import com.rtw.autopricing.ftp.data.SGLastFiveRowCategoryTicket;
import com.rtw.autopricing.ftp.data.SoldCategoryTicket;
import com.rtw.autopricing.ftp.data.TNDZonesCategoryTicket;
import com.rtw.autopricing.ftp.data.Ticket;
import com.rtw.autopricing.ftp.data.TixCityZonesPricingCategoryTicket;
import com.rtw.autopricing.ftp.data.VipCatsCategoryTicket;
import com.rtw.autopricing.ftp.data.VipLastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.ZoneTicketsProcessorCategoryTicket;
import com.rtw.autopricing.ftp.data.ZonedLastRowMiniCategoryTicket;
import com.rtw.autopricing.ftp.data.ZonesPricingCategoryTicket;


public class CategoryGroupManager {
	public static Logger logger = LoggerFactory.getLogger(CategoryGroupManager.class);
	static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	static DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	public static int PURCHASE_PRICE_THRESHHOLD = 10;
	public static int MAX_PUR_PRICE_THRESHOLD_PERCENTAGE = 200;
	
	public static Collection<Ticket> getTMATActiveTicketsFromDB(Event event,AutopricingProduct product) throws Exception {
		
		Date now = new Date();
		Collection<Ticket> ticketList = new ArrayList<Ticket>();
		try{
			boolean isTNCrawlOnly = false;
			String testEventName = event.getName().replaceAll("\\s+", "").toLowerCase();
			
			//Tamil : if event name contains any of following key word then  consider ticketnetworkdirect crawl tickets only for that event 
			if(testEventName.contains("2day") || testEventName.contains("twoday") //2day,2days,2 day,2 days,two day,two days
					|| testEventName.contains("3day") || testEventName.contains("threeday")//3day,3days,3 day,3 days,three day,three days
					|| testEventName.contains("4day") || testEventName.contains("fourday")//4day,4days,4 day,4 days,four day,four days
					|| testEventName.contains("multiday") //multi day,multidays 
					|| testEventName.contains("weekly") || testEventName.contains("weekend")//weekly,weekend
					|| testEventName.contains("allsession")
					|| testEventName.contains("allperformances")
					|| testEventName.contains("seasontickets")//Season Tickets
					|| testEventName.contains("fullstriptickets"))//Full Strip Tickets is equal to 4 days pass 
			{
				isTNCrawlOnly = true;
			}

			//Tamil : if events belongs to AT&T Stadiu or AT&T Stadium Parking Lots then skip vividseat crawl tickets
			boolean isSkipVividCrawls = false;
			if(event.getVenueId() != null && 
					( event.getVenueId().equals(8913) || //AT&T Stadium
							event.getVenueId().equals(13919))) {//AT&T Stadium Parking Lots
				isSkipVividCrawls = true;
			}
			
			Collection<Ticket> dbTicketList = new ArrayList<Ticket>();
			/*if(product.getName().equalsIgnoreCase("MHZonePricing")) {
				ticketList = DAORegistry.getTicketDAO().getAllActiveTicketForAutoCatsByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
			} else*/ if(product.getName().equalsIgnoreCase("SGLastFiveRow") || product.getName().equalsIgnoreCase("minicats")) {
				dbTicketList = DAORegistry.getTicketDAO().getAllActiveInstantTicketForAutoCatByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
			} else if(product.getName().equalsIgnoreCase("RewardTheFan Listings") || product.getName().equalsIgnoreCase("RTFLastRowCats")) {
				//if event date with in 3 days then load etickets and instant tickets
				//Tamil : consider all tickets even if event is within 3 days
				//if(event.getLocalDate()!=null && ((event.getLocalDate().getTime() - now.getTime())  <= 72 * 60 * 60 * 1000 )){
				//	dbTicketList = DAORegistry.getTicketDAO().getAllActiveInstantTicketForAutoCatByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
				//} else {
					dbTicketList = DAORegistry.getTicketDAO().getAllActiveTicketForAutoCatsByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
				//}
			} else {
				if(event.getLocalDate()!=null && ((event.getLocalDate().getTime() - now.getTime())  <= 120 * 60 * 60 * 1000 )){
					dbTicketList = DAORegistry.getTicketDAO().getAllActiveInstantTicketForAutoCatByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
				} else {
					dbTicketList = DAORegistry.getTicketDAO().getAllActiveTicketForAutoCatsByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
				}
			}
			//Tamil : we have to skip each category sold tickets associated TMAT ticket in computation
			Collection<SoldCategoryTicket> soldCatTicketList = DAORegistry.getSoldCategoryTicketDAO().getAllSoldCategoryTicketsByEventId(event.getId());
			Map<Integer, Boolean> soldCatTicketMap = new HashMap<Integer, Boolean>();
			if(soldCatTicketList != null) {
				for (SoldCategoryTicket soldCategoryTicket : soldCatTicketList) {
					soldCatTicketMap.put(soldCategoryTicket.getTicketId(), Boolean.TRUE);
				}
			}
			for (Ticket ticket : dbTicketList) {
				if(soldCatTicketMap.get(ticket.getId()) != null) {
					continue;	
				}
				ticketList.add(ticket);
			}
			
			//Tamil : Added to skip tickets with quantity 2 and current price below 100 in auto pricing
			/*for (Ticket ticket : dbTicketList) {
				if(ticket.getQuantity().equals(2) && ticket.getCurrentPrice()< 100) {
					continue;
				}
				ticketList.add(ticket);
			}*/
			
		}catch (Exception e) {
			logger.error("105.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() );
			System.out.println("105.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() );
			e.printStackTrace();
			throw e;
		}
		return ticketList;
	}
	
	public static List<CategoryTicket> computeCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,
			AutopricingProduct product,Map<String, Double> zonesPricingMap) throws Exception {
		
		
		List<CategoryTicket> catTixList = new ArrayList<CategoryTicket>();
		Collection<Ticket> finalTicketList = new ArrayList<Ticket>();
		Collection<Category> categoryList = null;
		List<Ticket> uncatTixList = null;
		Collection<Ticket> ticketList = getTMATActiveTicketsFromDB(event, product);
		try {
			
			categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
			ticketList = TicketUtil.removeDuplicateTickets(ticketList);
			ticketList = TicketUtil.removeAdmitoneTickets(ticketList, event, event.getAdmitoneId(),exEvent.getAllowSectionRange());
			
			//For ZoneTicket theater events if TMAT ticket section starts with Zone key word then we have to consider that ticket as categorized tickets
			if((product.getName().equalsIgnoreCase("PresaleZoneTicketsProcessor") || product.getName().equalsIgnoreCase("RewardTheFan Listings")
					|| product.getName().equalsIgnoreCase("CrownJewel")) && event.getParentCategoryId().equals(4)){
				uncatTixList = new ArrayList<Ticket>(ticketList);	
			}

			/*if(product.getName().equalsIgnoreCase("ZonesPricing")){
				if(exEvent.getTicketNetworkBrokerId().equals(5)) {//for TIX City we have to consider all tickets
					ticketList = TicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
				}
			} else {*/
				ticketList = TicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
			//}
			if(uncatTixList != null){
				Map<String,Category> categoryMap = new HashMap<String, Category>();
				
				for (Category category : categoryList) {
					String categoryWithZone = "zone "+category.getSymbol();
					categoryMap.put(categoryWithZone.replaceAll("\\s+", " ").trim().toLowerCase(), category);
				}
				uncatTixList.removeAll(ticketList);
				
				for (Ticket ticket : uncatTixList) {
					String key = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					if(key.contains("zone")) {
						Category category = categoryMap.get(key);
						if(category != null) {
							ticket.setCategory(category);
							ticket.setZoneCategoryticket(true);
							ticketList.add(ticket);
						}
					}
				}
			}
			//Purchase tour = DAORegistry.getPurchaseDAO().getPurchaseByEventId(event.getId());
			Collection<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAll();
			Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
			for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
				defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
			}
			Collection<ManagePurchasePrice> managePurchasePricelist = DAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());//tour.getTmatPurchaseId());
			Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
			for(ManagePurchasePrice tourPrice:managePurchasePricelist){
				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
			}
			
			for (Ticket ticket : ticketList) {
				TicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);
				
				//for rewardthefan and rtflastrow consider all tickets which has price above $10 for all quantity
				//Tamil : for rewardthefan and rtflastrow consider all price tickets as per amit request
				if(!product.getName().equalsIgnoreCase("RewardTheFan Listings") && !product.getName().equalsIgnoreCase("RTFLastRowCats")) {
					//Tamil : Added to skip tickets with quantity 2 and current price below 100 in auto pricing
					/*if(ticket.getQuantity().equals(2) && ticket.getPurchasePrice()< 100) {
						continue;
					}*/
					
					//Tamil : for rewardthefan consider all price tickets as per amit request
					if(ticket.getPurchasePrice() < PURCHASE_PRICE_THRESHHOLD) {
						continue;
					}
				}
				finalTicketList.add(ticket);
			}
			
			//Removing Fake Tickets by Maximum Purchase Price Threshold
			finalTicketList = TicketUtil.removeFakeTicketsByCategoryandSection(finalTicketList, MAX_PUR_PRICE_THRESHOLD_PERCENTAGE);
			
			Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
			Set<Ticket> sectionTickets = null;
			for (Ticket ticket : finalTicketList) {
				
				if(ticket.getId().equals(925082047)) {
					System.out.println(ticket.getPurchasePrice());
				}
				CategoryMapping catmapping = ticket.getCategoryMapping();
				
				if(product.getName().equalsIgnoreCase("RewardTheFan Listings") || product.getName().equalsIgnoreCase("CrownJewel")){
					try {
					if(ticket.getCategory().getNormalizedZone() != null && ticket.getCategory().getNormalizedZone().length() > 0) {
						String section = ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().toLowerCase();

						//for Crown Jewel concert and theater events compute tickets only for zone A
						if(product.getName().equalsIgnoreCase("CrownJewel") && !exEvent.getCjParentType().equals("SPORTS")
								&& !section.equals("a")) {
							continue;
						}
						
						sectionTickets = sectionTicketMap.get(section);
						if(null == sectionTickets) {
							sectionTickets = new HashSet<Ticket>();
							sectionTicketMap.put(section, sectionTickets);
						}
						sectionTickets.add(ticket);	
					}
					}
					catch (Exception e) {
						e.printStackTrace();
						throw e;
					}
					
					
				} else if(product.getName().equalsIgnoreCase("PresaleZoneTicketsProcessor")){
					try {
						if(ticket.getCategory().getNormalizedZone() != null && ticket.getCategory().getNormalizedZone().length() > 0) {
							String section = ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().toLowerCase();
							sectionTickets = sectionTicketMap.get(section);
							
							if(null == sectionTickets) {
								sectionTickets = new HashSet<Ticket>();
								sectionTicketMap.put(section, sectionTickets);
							}
							sectionTickets.add(ticket);	
						}
						}
						catch (Exception e) {
							e.printStackTrace();
							throw e;
						}
						
						
					} /*else if(product.getName().equalsIgnoreCase("larrylast")) {
				
					if(catmapping != null && catmapping.getLarrySection() != null && catmapping.getLarrySection().length() > 0 &&
							 catmapping.getLastRow() != null && catmapping.getLastRow().length() > 0) {
						String section = catmapping.getLarrySection().replaceAll("\\s+", " ").trim().toLowerCase();
						sectionTickets = sectionTicketMap.get(section);
						
						if(null == sectionTickets) {
							sectionTickets = new HashSet<Ticket>(); 
							sectionTicketMap.put(section, sectionTickets);
						}
						sectionTickets.add(ticket);
					}
					
				}*/ else if(product.getName().equalsIgnoreCase("minicats") || product.getName().equalsIgnoreCase("vip minicats")){
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
				} else if(product.getName().equalsIgnoreCase("LastRow MiniCats")) {// || product.getName().equalsIgnoreCase("Zoned LastRow MiniCats")){
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
					
				} else if(product.getName().equalsIgnoreCase("RTFLastRowCats")){
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
					
				} else if(product.getName().equalsIgnoreCase("SGLastFiveRow")){
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
					
				} else if(product.getName().equalsIgnoreCase("VipLastRowCats")){
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
				} else if(product.getName().equalsIgnoreCase("ZonesPricing") || product.getName().equalsIgnoreCase("RTWZonesPricing") ||
						product.getName().equalsIgnoreCase("MHZonePricing")){
					
					//String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					String section = ("ZONE " + ticket.getCategory().getSymbol()).replaceAll("\\s+", " ").trim().toLowerCase();
					
					/*if(!exEvent.getTicketNetworkBrokerId().equals(5)) {//for TIX City we have to consider all sections
						if(!section.contains("zone")){
							continue;
						}
					}*/
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
				}else if(product.getName().equalsIgnoreCase("PresaleAutoCat")){
					if(ticket.getCategory().getNormalizedZone() != null && ticket.getCategory().getNormalizedZone().length() > 0) {
						String section = ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().toLowerCase();
						sectionTickets = sectionTicketMap.get(section);
						
						if(null == sectionTickets) {
							sectionTickets = new HashSet<Ticket>();
							sectionTicketMap.put(section, sectionTickets);
						}
						sectionTickets.add(ticket);
					}
					
				//	System.out.println( ticket.getCategory().getSymbol()+" "+ticket.getSection()+" "+ticket.getQuantity()+" "+ticket.getPurchasePrice());
				} else if(product.getName().equalsIgnoreCase("TNDZones")){
					if(ticket.getCategory().getNormalizedZone() != null && ticket.getCategory().getNormalizedZone().length() > 0) {
						String section = ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().toLowerCase();
						sectionTickets = sectionTicketMap.get(section);
						
						if(null == sectionTickets) {
							sectionTickets = new HashSet<Ticket>();
							sectionTicketMap.put(section, sectionTickets);
						}
						sectionTickets.add(ticket);
					}
				}
			
//				tempTicketList.add(ticket);
			}
			Integer minEligibleSections = Integer.parseInt(exEvent.getExposure().split("-")[0]);
			if(minEligibleSections > 2){
				minEligibleSections = 2;
			}
			sectionEligibleMinEntries = minEligibleSections;
			
			if(product.getName().equalsIgnoreCase("RewardTheFan Listings") || product.getName().equalsIgnoreCase("CrownJewel")){
				getZoneTicketsCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
					vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			} else if(product.getName().equalsIgnoreCase("PresaleZoneTicketsProcessor")){
				getPresaleZoneTicketsCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
					
			}/*else if(product.getName().equalsIgnoreCase("larrylast")){
				getLarryLastCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
					vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);

			}*/else if(product.getName().equalsIgnoreCase("minicats")){
				getSectionBasedCheapestMiniCats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			} else if(product.getName().equalsIgnoreCase("LastRow MiniCats")){
				getLastRowMiniCatCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			} /*else if(product.getName().equalsIgnoreCase("Zoned LastRow MiniCats")){
				getZoneLastRowMiniCatCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			}*/ else if(product.getName().equalsIgnoreCase("RTFLastRowCats")){
				getRTFLastRowCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			} else if(product.getName().equalsIgnoreCase("SGLastFiveRow")){
				getSGLastFiveRowCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			} else if(product.getName().equalsIgnoreCase("Vip MiniCats")){
				getSectionBasedCheapestVipCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			} else if(product.getName().equalsIgnoreCase("ZonesPricing")|| product.getName().equalsIgnoreCase("RTWZonesPricing") ||
					product.getName().equalsIgnoreCase("MHZonePricing")){
				getZonesPricingCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product,zonesPricingMap);
			}else if(product.getName().equalsIgnoreCase("PresaleAutoCat")){
				getPresaleAutoCatCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			}else if(product.getName().equalsIgnoreCase("VipLastRowCats")){
				getVipLastRowMiniCatCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			} else if(product.getName().equalsIgnoreCase("TNDZones")){
				getVipLastRowMiniCatCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,sectionTicketMap,catTixList,product);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() );
			//System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
			throw e;
		}
		
		return catTixList;
	}
	public static List<TNDZonesCategoryTicket> computeCategoryTicketsForTNDZones(Event event,
			int defaultExpectedArrivialDatePriorDays,Double markup,AutopricingProduct product,List<TNDZonesCategoryTicket> tndOrders) throws Exception {
		
		
		List<TNDZonesCategoryTicket> catTixList = new ArrayList<TNDZonesCategoryTicket>();
		Collection<Ticket> finalTicketList = new ArrayList<Ticket>();
		Collection<Category> categoryList = null;
		Collection<Ticket> ticketList = getTMATActiveTicketsFromDB(event, product);
		try {
			
			categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
			ticketList = TicketUtil.removeDuplicateTickets(ticketList);
			ticketList = TicketUtil.removeAdmitoneTickets(ticketList, event, event.getAdmitoneId(),false);
			
			ticketList = TicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
			
			Collection<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAll();
			Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
			for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
				defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
			}
			Collection<ManagePurchasePrice> managePurchasePricelist = DAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());//tour.getTmatPurchaseId());
			Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
			for(ManagePurchasePrice tourPrice:managePurchasePricelist){
				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
			}
			
			for (Ticket ticket : ticketList) {
				TicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);
				
				//Tamil : for rewardthefan consider all price tickets as per amit request
				if(ticket.getPurchasePrice() < PURCHASE_PRICE_THRESHHOLD) {
					continue;
				}
				finalTicketList.add(ticket);
			}
			
			//Removing Fake Tickets by Maximum Purchase Price Threshold
			finalTicketList = TicketUtil.removeFakeTicketsByCategoryandSection(finalTicketList, MAX_PUR_PRICE_THRESHOLD_PERCENTAGE);
			
			Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
			Set<Ticket> sectionTickets = null;
			for (Ticket ticket : finalTicketList) {
				
				//String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
				String section = ("ZONE " + ticket.getCategory().getSymbol()).replaceAll("\\s+", " ").trim().toLowerCase();
				sectionTickets = sectionTicketMap.get(section);
				if(null == sectionTickets) {
					sectionTickets = new HashSet<Ticket>();
					sectionTicketMap.put(section, sectionTickets);
				}
				sectionTickets.add(ticket);
			}
			
			getTNDZonesCategoryTickets(event,defaultExpectedArrivialDatePriorDays,markup,sectionTicketMap,catTixList,product,tndOrders);
			
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() );
			//System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
			throw e;
		}
		
		return catTixList;
	}
	public static CategoryTicket computecategoryTicket(Set<Ticket> sectionTickets,Event event,ExchangeEvent exEvent,int quantity,
			int sectionEligibleMinEntries,Date expectedArrivalDate,double tnExAddMarkup,double vividExAddMarkup,double tickPickExAddMarkup,
			double scoreBigExAddMarkup,double fanxchangeAddMarkup,double ticketcityAddMarkup,CategoryTicket catTicket,AutopricingProduct product) throws Exception {
		
//		CategoryTicket catTicket =  new CategoryTicket();
		TicketUtil.getCheapestTicketByQty(sectionTickets, exEvent, quantity, sectionEligibleMinEntries,catTicket,product);
		
		if(catTicket.getPurPrice()!=null) {
			catTicket.setQuantity(quantity);
			catTicket.setPosExchangeEventId(event.getAdmitoneId());
//			catTicket.setRowRange(rowRange);
//			catTicket.setLastRow(lastRow);
			catTicket.setExpectedArrivalDate(expectedArrivalDate);
			catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
			//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
			//catTicket.setSection(larrySection);
			
			computecategoryTicketPrices(catTicket, exEvent, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,product);
			
			/*double roundedPrice;
			double eventMarkup,eventShippingFees;
			
			if(catTicket.getPurPrice() >= exEvent.getPriceBreakup()) {
				eventMarkup = exEvent.getUpperMarkup();
				eventShippingFees = exEvent.getUpperShippingFees();
			} else {
				eventMarkup = exEvent.getLowerMarkup();
				eventShippingFees = exEvent.getLowerShippingFees();
			}
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100)) + eventShippingFees/quantity) * 100 / 100);
			catTicket.setActualPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100)) + eventShippingFees/quantity) * 100 / 100);
			catTicket.setZoneTicketPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+tnExAddMarkup)/100)) + eventShippingFees/quantity) * 100 / 100);
			catTicket.setTnPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+vividExAddMarkup)/100)) + eventShippingFees/quantity) * 100 / 100);
			catTicket.setVividPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+tickPickExAddMarkup)/100)) + eventShippingFees/quantity) * 100 / 100);
			catTicket.setTickpickPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+scoreBigExAddMarkup)/100)) + eventShippingFees/quantity) * 100 / 100);
			catTicket.setScoreBigPrice(roundedPrice);*/
			
			
			//exCatTixMap.put(catTicket.getQuantity()+":"+ catTicket.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+catTicket.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase(), catTicket);
			//catTixMap.put(catTicket.getQuantity()+":"+ catTicket.getSection().replaceAll("\\s+", " ").trim().toLowerCase()+":"+catTicket.getRowRange().replaceAll("\\s+", " ").trim().toLowerCase(), catTicket);
			
			//logger.info("NF Prices2..."+eventId+" ..sec.."+catTicket.getSection()+"...row.."+catTicket.getRowRange()+"..price.."+catTicket.getPrice()+"...purprice..."+cheapestTicket.getPurchasePrice()+".ticid.."+cheapestTicket.getId());
		} else {
			catTicket = null;
//			return null;
		}
		return catTicket;
	}
	
	public static void computecategoryTicketPrices(CategoryTicket catTicket,ExchangeEvent exEvent,double tnExAddMarkup,
			double vividExAddMarkup,double tickPickExAddMarkup,double scoreBigExAddMarkup,double fanxchangeAddMarkup,double ticketcityAddMarkup,AutopricingProduct product) throws Exception {
		
		
			double roundedPrice;
			double eventMarkup,eventShippingFees;
			
			if(catTicket.getPurPrice() >= exEvent.getPriceBreakup()) {
				eventMarkup = exEvent.getUpperMarkup();
				eventShippingFees = exEvent.getUpperShippingFees();
			} else {
				eventMarkup = exEvent.getLowerMarkup();
				eventShippingFees = exEvent.getLowerShippingFees();
			}
			
			if(product.getName().equalsIgnoreCase("RewardTheFan Listings")) {
				if(catTicket.getTmatSection() != null && 
						catTicket.getTmatSection().toLowerCase().contains("zone")) {
					eventMarkup = 0.0;
					eventShippingFees=0.0;
				}
			}
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setActualPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setZoneTicketPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+tnExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setTnPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+vividExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setVividPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+tickPickExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setTickpickPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+scoreBigExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setScoreBigPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+fanxchangeAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setFanxchangePrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+ticketcityAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setTicketcityPrice(roundedPrice);
			
			//if(exEvent.getTaxPercentage() != null) {//product.getName().equalsIgnoreCase("RewardTheFan Listings")
			//	roundedPrice = (double) Math.ceil((catTicket.getPurPrice()*(exEvent.getTaxPercentage()/100)) * 100 / 100);
			//	catTicket.setTaxAmount(roundedPrice);	
			//} else {
			//	catTicket.setTaxAmount(0.0);
			//}
	}
	
	public static Map<String, Set<Ticket>> getZoneTicketsEligibleTmatTicketsBasedonLastRow(Event event,
			Map<String, Set<Ticket>> sectionTicketMap,Map<String,CategoryMapping> lastRowCategoryMap) throws Exception {
		
		String intRegex = "[0-9]+";

		Map<String,Integer> sectionsEligibleLastRowMap = new HashMap<String, Integer>();
		for (String key : lastRowCategoryMap.keySet()) {
			CategoryMapping categoryMapping = lastRowCategoryMap.get(key);
			if(categoryMapping == null || categoryMapping.getLastRow() == null) {
				continue;
			}
			if(categoryMapping.getLastRow().matches(intRegex)) {
				Integer intLastRow = Integer.parseInt(categoryMapping.getLastRow());
				
				if(intLastRow <= 5) {
					
				} else if(intLastRow > 5 && intLastRow <= 10) {
					intLastRow = intLastRow - 1;
				} else if(intLastRow > 10 && intLastRow <= 20) {
					intLastRow = intLastRow - 2;
				} else if(intLastRow > 20 && intLastRow <= 35) {
					intLastRow = intLastRow - 4;
				} else if(intLastRow > 35 && intLastRow <= 50) {
					intLastRow = intLastRow - 6;
				} else if(intLastRow > 50) {
					intLastRow = intLastRow - 8;
				}
				//System.out.println("key.."+key+"..intLast..."+intLastRow);
				
				sectionsEligibleLastRowMap.put(key, intLastRow);
			}
		}
		
		Map<String, Set<Ticket>> newSectionTicketMap = new HashMap<String, Set<Ticket>>();
		Set<Ticket> newSectionTickets = null;
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			
			for (Ticket ticket : sectionTickets) {
				try {
					String categorySectionKey =  ticket.getCategory().getId()+":"+ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					Integer eligibleLastRow = sectionsEligibleLastRowMap.get(categorySectionKey);
					
					if(eligibleLastRow != null) {
						String row = ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase();
						if(!row.matches(intRegex)) {
							continue;
						}
						Integer intRow = Integer.parseInt(row);
						if(intRow > eligibleLastRow) {
							continue;
						}
					}
					String category = ticket.getCategory().getSymbol().replaceAll("\\s+", " ").trim().toLowerCase();
					newSectionTickets = newSectionTicketMap.get(category);
					if(null == newSectionTickets) {
						newSectionTickets = new HashSet<Ticket>();
						newSectionTicketMap.put(category, newSectionTickets);
					}
					newSectionTickets.add(ticket);
					
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
			}
		}
		return newSectionTicketMap;
	}
	public static List<CategoryTicket> getZoneTicketsCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		/*
		 * skip this logic for rewardthefan only
		//If event is sports or concerts then we have to remove specified numeric row tickets(lease row tickets) of each sections
		if(event.getParentCategoryId().equals(1) || event.getParentCategoryId().equals(2)) {
			Map<String,CategoryMapping> lastRowCategoryMap = Categorizer.computeLastRowForCategoryandSectionByVenueCategoryId(event.getVenueCategoryId());
			sectionTicketMap = getZoneTicketsEligibleTmatTicketsBasedonLastRow(event, sectionTicketMap, lastRowCategoryMap);
		}*/
		
		
		
		//List<Ticket> finalSectionTickets = new ArrayList<Ticket>();
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			
			//finalSectionTickets.clear();
			//finalSectionTickets.addAll(sectionTickets);
			//Collections.sort(finalSectionTickets, TicketUtil.ticketSortingComparator);
			
			//logger.info("NF eventId...."+event.getId()+"..section.."+key+"..rowRange.."+rowRange);
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
				
			for (int i = 1; i <= 9; i++) {
				if(i != 9) {
					int quantity = i;
					
					if(product.getName().equalsIgnoreCase("CrownJewel")) {
						
						if(i != 2 && i != 4 && i != 6) {
							continue;
						}
						
						CrownJewelCategoryTicket catTicket = new CrownJewelCategoryTicket();
						catTicket= (CrownJewelCategoryTicket) computecategoryTicket(sectionTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						if(catTicket != null) {
							catTicket.setQuantity(quantity);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setRowRange(rowRange);
							//catTicket.setLastRow(lastRow);
							catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							catTicket.setPriority(1);
							//catTicket.setSection(key);
							
							catTixList.add(catTicket);
						}
					} else {
						ZoneTicketsProcessorCategoryTicket catTicket = new ZoneTicketsProcessorCategoryTicket();
						catTicket= (ZoneTicketsProcessorCategoryTicket) computecategoryTicket(sectionTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						if(catTicket != null) {
							catTicket.setQuantity(quantity);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setRowRange(rowRange);
							//catTicket.setLastRow(lastRow);
							catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							catTicket.setPriority(1);
							//catTicket.setSection(key);
							catTicket.setTaxPercentage(exEvent.getTaxPercentage());
							
							catTixList.add(catTicket);
						}
					}
					
					/*if(catTicket != null){
						
						List<Ticket> nextCheapestTickets = finalSectionTickets;
						Set<Ticket> cheapestTickets = null;
						
						int fromIndex = nextCheapestTickets.indexOf(catTicket.getTicket());
						int ticketSize = nextCheapestTickets.size();
						int priority=2,j=2;
						
						while(j<ticketSize){
							
							if(priority > 5){
								break;	
							}
							cheapestTickets = new HashSet<Ticket>(nextCheapestTickets.subList(fromIndex+1, ticketSize));
							
							catTicket = new ZoneTicketsProcessorCategoryTicket();
							catTicket= (ZoneTicketsProcessorCategoryTicket) computecategoryTicket(cheapestTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
									tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,catTicket,product);
							
							if(catTicket != null) {
								

								catTicket.setQuantity(quantity);
								catTicket.setPosExchangeEventId(event.getAdmitoneId());
								//catTicket.setRowRange(rowRange);
								//catTicket.setLastRow(lastRow);
								catTicket.setExpectedArrivalDate(expectedArrivalDate);
								catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
								//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
								catTicket.setPriority(priority);
								//catTicket.setSection(key);
								
								catTixList.add(catTicket);
							
								fromIndex = nextCheapestTickets.indexOf(catTicket.getTicket());
								ticketSize = nextCheapestTickets.size();
								priority++;
							}else{
								ticketSize = 1;
								break;
							}
						}
					}*/
				}
			}
		}
		return catTixList;
	}
	
	public static List<CategoryTicket> getPresaleZoneTicketsCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		
		Map<String,CategoryMapping> lastRowCategoryMap = Categorizer.computeLastRowForCategoryandSectionByVenueCategoryId(event.getVenueCategoryId());
		
		//If event is sports or concerts then we have to remove specified numeric row tickets(lease row tickets) of each sections
		if(event.getParentCategoryId().equals(1) || event.getParentCategoryId().equals(2)) {
			sectionTicketMap = getZoneTicketsEligibleTmatTicketsBasedonLastRow(event, sectionTicketMap, lastRowCategoryMap);
		}
		
		//List<Ticket> finalSectionTickets = new ArrayList<Ticket>();
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
				
			for (int i = 1; i <= 9; i++) {
				if(i != 9) {
					int quantity = i;
					PresaleZoneTicketsCategoryTicket catTicket = new PresaleZoneTicketsCategoryTicket();
					catTicket= (PresaleZoneTicketsCategoryTicket) computecategoryTicket(sectionTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
					
					if(catTicket != null) {
						catTicket.setQuantity(quantity);
						catTicket.setPosExchangeEventId(event.getAdmitoneId());
						catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						catTicket.setPriority(1);
						catTixList.add(catTicket);
					}
					
				}
			}
		}
		return catTixList;
	}
	
		
	public static List<CategoryTicket> getLarryLastCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception{
		try{
			//Map<String,Set<String>> larrySectionMap = DAORegistry.getQueryManagerDAO().getAllLarrySectionAndLastRowByVenueCategoryId(event.getVenueCategoryId());
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLarryLastProductsLarrySectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				int quantity;
				//Set<String> lastRows = larrySectionMap.get(key.toUpperCase());
				//if(lastRows == null || lastRows.isEmpty()) {
					//continue;
				//}
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				//String rowRange = categoryMapping.getRowRange();
				//String lastRow = categoryMapping.getLastRow();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				Set<Ticket> sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				for (int i = 1; i <= 9; i++) {
					if(i ==2 || i== 4 ) {//|| i == 6
						
						//Tamil : for Hamilton events Process only 2 and 4 qty listings for SS account as business requirement
						if(i == 6 && event.getName().equalsIgnoreCase("Hamilton")) {
							continue;
						}
						
						quantity = i;
						LarryLastCategoryTicket catTicket = new LarryLastCategoryTicket();		
						catTicket = (LarryLastCategoryTicket)computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							
							//If LarrySection ends with SPECIAL then we have to create duplicate ticket with Sections like LEFT and RIGHT
							String sectionPrefix = null,section1 = null,section2=null;
							if(catTicket.getSection() != null && catTicket.getSection().toUpperCase().contains("SPECIAL")) {
								sectionPrefix = catTicket.getSection().replaceAll("\\s+", " ").toUpperCase().replaceAll("SPECIAL", "");
								section1 = sectionPrefix.trim() + " LEFT";
								section2 = sectionPrefix.trim() + " RIGHT";
							} else {
								section1 = catTicket.getSection();
							}
							catTicket.setSection(section1);
							catTicket.setLastRow(miniLastRow);
							
							catTixList.add(catTicket);
							
							if(section2 != null) {
								LarryLastCategoryTicket newCatTicket = new LarryLastCategoryTicket(catTicket);
								newCatTicket.setSection(section2);
								newCatTicket.setLastRow(miniLastRow);
								catTixList.add(newCatTicket);
							}
							// Need to work for other products..
							//If more than one different last row exist in CSV for single larrySection(CSV - G Column) we have to create create duplicate listings with all last row values.
							/*for (String lastRow : lastRows) {
								if(!catTicket.getLastRow().equalsIgnoreCase(lastRow)) {
									LarryLastCategoryTicket newCatTicket = new LarryLastCategoryTicket(catTicket);
									newCatTicket.setLastRow(lastRow);
									newCatTicket.setSection(section1);
									catTixList.add(newCatTicket);
									
									if(section2 != null) {
										newCatTicket = new LarryLastCategoryTicket(newCatTicket);
										newCatTicket.setSection(section2);
										catTixList.add(newCatTicket);
									}	
								}
							}*/
						}
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			//System.out.println(e.fillInStackTrace());
			//return null;
			throw e;
		}
		
		return catTixList;
	}
	
	public static List<CategoryTicket> getSectionBasedCheapestVipCategoryTicketsForMiniCats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {
		
		
		Date expectedArrivalDate = null;
		if(event.getLocalDate()!=null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(event.getLocalDate());
			cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
			expectedArrivalDate = cal.getTime();
		}
		Map<String ,Map<String,CategoryMapping>> alternateRowMap = TicketUtil.getSectionBasedAlternateRows(event);
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);

			Map<String,CategoryMapping> rowMap = alternateRowMap.get(key);
			if(rowMap == null ||  rowMap.isEmpty()) {
				continue;	
			}
				
			for (String  alRow : rowMap.keySet()) {
				CategoryMapping categoryMapping = rowMap.get(alRow);
				if(categoryMapping == null) {
					continue;
				}
				List<String>  rowList = categoryMapping.getRowList();
				if(rowList == null ||  rowList.isEmpty()) {
					continue;
				}
				Set<Ticket> rowTicketList =  new HashSet<Ticket>();
				
				for (Ticket ticket : sectionTickets) {
					if(ticket.getRow()!= null && rowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						rowTicketList.add(ticket);
					}
				}
				for (int i = 1; i <= 9; i++) {
					if( i ==2 || i== 4 ) {//|| i == 6
						int quantity = i;
						CategoryTicket catTicket = new MiniCategoryTicket();
						catTicket = computecategoryTicket(rowTicketList, event, exEvent, quantity, sectionEligibleMinEntries,expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							catTicket.setQuantity(quantity);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							catTicket.setRowRange(categoryMapping.getAlternateRow().toUpperCase());
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							
							catTixList.add(catTicket);
						}
					}
				}
			}
		}
		return catTixList;
	}
	
	public static List<CategoryTicket> getSectionBasedCheapestVipCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {
		
		
		Date expectedArrivalDate = null;
		if(event.getLocalDate()!=null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(event.getLocalDate());
			cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
			expectedArrivalDate = cal.getTime();
		}
		Map<String ,Map<String,CategoryMapping>> alternateRowMap = TicketUtil.getSectionBasedAlternateRows(event);
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);

			Map<String,CategoryMapping> rowMap = alternateRowMap.get(key);
			if(rowMap == null ||  rowMap.isEmpty()) {
				continue;	
			}
				
			for (String  alRow : rowMap.keySet()) {
				CategoryMapping categoryMapping = rowMap.get(alRow);
				if(categoryMapping == null) {
					continue;
				}
				List<String>  rowList = categoryMapping.getRowList();
				if(rowList == null ||  rowList.isEmpty()) {
					continue;
				}
				Set<Ticket> rowTicketList =  new HashSet<Ticket>();
				
				for (Ticket ticket : sectionTickets) {
					if(ticket.getRow()!= null && rowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						rowTicketList.add(ticket);
					}
					
					for (int i = 1; i <= 9; i++) {
						if( i ==2 || i== 4 || i == 6) {
							int quantity = i;
							CategoryTicket catTicket = new VipCatsCategoryTicket();
							catTicket = computecategoryTicket(rowTicketList, event, exEvent, quantity, sectionEligibleMinEntries,expectedArrivalDate,
									tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
							
							if(catTicket != null) {
								catTicket.setQuantity(quantity);
								catTicket.setPosExchangeEventId(event.getAdmitoneId());
								catTicket.setRowRange(categoryMapping.getAlternateRow().toUpperCase());
								//catTicket.setExpectedArrivalDate(expectedArrivalDate);
								catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
								//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
								
								catTixList.add(catTicket);
							}
						}
					}
				}
			}
		}
		return catTixList;
	}
	
	public static List<CategoryTicket> getSectionBasedCheapestMiniCats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		int eventLevelsectionEligibleCount = sectionEligibleMinEntries;

//		Date now = new Date();
//		now = df.parse(df.format(now));
		
		if(exEvent.getAllowSectionRange()) {
			sectionTicketMap = TicketUtil.getSectionRangeTicketsFromUniqueSections(sectionTicketMap);
		}
		
		Map<String,CategoryMapping> rowRangeMap = Categorizer.computeRowRangeForSectionsByVenueCategoryIdOne(event);
		CategoryMapping categoryMapping = null;
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			
			categoryMapping = rowRangeMap.get(key);
			String rowRange = "",lastRow = ""; 
			
			if(categoryMapping != null) {
				rowRange = categoryMapping.getRowRange();
				lastRow = categoryMapping.getLastRow();
				
				if(lastRow == null) {
					lastRow = "";
				}
			}
			
			if(exEvent.getAllowSectionRange() && key.contains("-")) {
				sectionEligibleMinEntries = 1;
//				isAllowRangeSection = true;
			} else {
				sectionEligibleMinEntries = eventLevelsectionEligibleCount;
//				isAllowRangeSection = false;
			}
			
			if(rowRange.equals("")) {
				if(exEvent.getAllowSectionRange() && key.contains("-")) {
					Iterator<Ticket> iterator = sectionTickets.iterator();
					Ticket tix = null;
						
					while(iterator.hasNext()) {
						tix = iterator.next();
						if(key.equals(tix.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase())) {
							break;
						}
					}
						
					if(null != tix && tix.getCategoryMapping() != null) {
						categoryMapping = rowRangeMap.get(tix.getCategoryMapping().getStartSection().replaceAll("\\s+", " ").trim().toLowerCase());	
					}
					if(categoryMapping != null) {
						rowRange = categoryMapping.getRowRange();
						lastRow = categoryMapping.getLastRow();
							
						if(lastRow == null) {
							lastRow = "";
						}
					}
				} else {
					continue;
				}
			}
			//logger.info("NF eventId...."+event.getId()+"..section.."+key+"..rowRange.."+rowRange);
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
				
			for (int i = 1; i <= 9; i++) {
				if(i ==2 || i== 4 ) {//|| i == 6
					int quantity = i;
					MiniCategoryTicket catTicket = new MiniCategoryTicket();
					catTicket= (MiniCategoryTicket) computecategoryTicket(sectionTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
					
					if(catTicket != null) {
						catTicket.setQuantity(quantity);
						catTicket.setPosExchangeEventId(event.getAdmitoneId());
						catTicket.setRowRange(rowRange);
						catTicket.setLastRow(lastRow);
						catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
						//catTicket.setSection(key);
						
						catTixList.add(catTicket);
					}
				}
			}
		}
		getSectionBasedCheapestVipCategoryTicketsForMiniCats(event, exEvent, defaultExpectedArrivialDatePriorDays, sectionEligibleMinEntries, tnExAddMarkup,
				vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup, fanxchangeAddMarkup, ticketcityAddMarkup, sectionTicketMap, catTixList, product);
		return catTixList;
	}
	
	public static List<CategoryTicket> getLastRowMiniCatCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		try{
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				String rowRange = categoryMapping.getRowRange();
				//String lastRow = categoryMapping.getLastRow();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				if(rowRange == null) {
					rowRange = "";
				}
				
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				Set<Ticket> sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				//for (Ticket tix : sectionTickets) {
						//System.out.println("tix id..."+tix.getId()+"...section..."+tix.getNormalizedSection()+"...purprice.."+tix.getPurchasePrice()+"..qty.."+tix.getQuantity()+"...row ..."+tix.getNormalizedRow());
				//}
				//System.out.println("section..."+key+"..rowrange.."+rowRange+"..lastrow.."+lastRow+"..minilastrow.."+miniLastRow);
				
				for (int i = 1; i <= 9; i++) {
					if(i ==2 || i== 4 ) { //|| i == 6
						int quantity = i;
						
						CategoryTicket catTicket = null;
						if(product.getName().equalsIgnoreCase("LastRow MiniCats")) {
							catTicket = new LastRowMiniCategoryTicket();
							
						} else if(product.getName().equalsIgnoreCase("Zoned LastRow MiniCats")) {
							catTicket = new ZonedLastRowMiniCategoryTicket();
							
							//Tamil : for Hamilton events Process only 2 and 4 qty listings for SS account as business requirement
							if(i == 6 && event.getName().equalsIgnoreCase("Hamilton")) {
								continue;
							}
						}
						
						
						catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							catTicket.setLastRow(miniLastRow);
							catTicket.setRowRange(rowRange);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							
							catTixList.add(catTicket);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
	public static List<CategoryTicket> getZoneLastRowMiniCatCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		try{
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			String eventExposure = exEvent.getExposure();
			Integer eventSectionEligibleMinEntries = sectionEligibleMinEntries;
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				String rowRange = categoryMapping.getRowRange();
				//String lastRow = categoryMapping.getLastRow();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				if(rowRange == null) {
					rowRange = "";
				}
				
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				Set<Ticket> sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				//for (Ticket tix : sectionTickets) {
						//System.out.println("tix id..."+tix.getId()+"...section..."+tix.getNormalizedSection()+"...purprice.."+tix.getPurchasePrice()+"..qty.."+tix.getQuantity()+"...row ..."+tix.getNormalizedRow());
				//}
				//System.out.println("section..."+key+"..rowrange.."+rowRange+"..lastrow.."+lastRow+"..minilastrow.."+miniLastRow);
				
				Map<Integer,CategoryTicket> catsMap = new HashMap<Integer, CategoryTicket>();
				for (int i = 1; i <= 9; i++) {
					if(i ==2 || i== 4 ) { //|| i == 6
						int quantity = i;
						
						CategoryTicket catTicket = new ZonedLastRowMiniCategoryTicket();
							
						//Tamil : for Hamilton events Process only 2 and 4 qty listings for SS account as business requirement
						if(i == 6 && event.getName().equalsIgnoreCase("Hamilton")) {
							continue;
						}
						exEvent.setExposure(eventExposure);
						sectionEligibleMinEntries=eventSectionEligibleMinEntries;
						
						catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							catTicket.setLastRow(miniLastRow);
							catTicket.setRowRange(rowRange);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							
							//catTixList.add(catTicket);
							catsMap.put(quantity, catTicket);
						}
					}
				}
				/*
				 * Compute tickets for lastfive rows from lastrow of each sections
				 * 6qty for last row
				 * 4 qty for last 2nd row
				 * 2 quantity for last 3rd,4th and 5th rows
				 * if ther is no 4 or qty then consider 2 qty listing for all five rows
				 * for last 2nd row computation remove last row listings and check min price from last2nd row if no tix then use last row tix
				 * for lease row comutations use exposure as 1-OXP and mintixCount to 1
				 * 
				 * if lastrow is like GA or BOX ,etc then consider TMAT tickets with row as lastrow only.
				 * consider cats listings row as csv lastorw value.
				 * create listings for qty 2 and 4 with same lastrow value.
				 * if minlastorw list is null then consider that row is like GA or BOX type row
				*/
				if (!catsMap.isEmpty()) {

					int size = 2;
					Map<Integer,String> minLastRowMap = Categorizer.getMiniLastRow(miniLastRow,size);
					
					if(!minLastRowMap.isEmpty()) {
						
						int subtractRowCount = 0;
						String tempRow = null;
						int quantity = 4;
						Set<Ticket> qtyTempSectionTickets = new HashSet<Ticket>(sectionTickets);
						for(int i=0;i<2;i++) {
							CategoryTicket catTicket = null;
							
								if(tempRow != null) {
									eligibleRowList.remove(tempRow.toLowerCase());
									
									
									Set<Ticket> qtySectionTickets = new HashSet<Ticket>();
									for (Ticket ticket : qtyTempSectionTickets) {
										if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
											qtySectionTickets.add(ticket);
										} 
									}
									exEvent.setExposure("1-OXP");
									sectionEligibleMinEntries=1;
									
									qtyTempSectionTickets = new HashSet<Ticket>(qtySectionTickets);
									catTicket = new ZonedLastRowMiniCategoryTicket();
									
									catTicket = computecategoryTicket(qtyTempSectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
											tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
									
									if(catTicket != null) {
										catTicket.setLastRow(miniLastRow);
										catTicket.setRowRange(rowRange);
										catTicket.setPosExchangeEventId(event.getAdmitoneId());
										//catTicket.setExpectedArrivalDate(expectedArrivalDate);
										catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
										//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
										
										//catTixList.add(catTicket);
										catsMap.put(quantity, catTicket);
									}
								} 
								
								
								if(catTicket == null) {
									while(true) {
										catTicket = catsMap.get(quantity);
										if(quantity==6) {
											quantity=4;
										} else if(quantity == 4) {
											quantity =2;
										} else if(quantity == 2) {
											break;
										}
										if(catTicket != null) {
											catTicket = new ZonedLastRowMiniCategoryTicket(catTicket);
											break;
										}
									}
								} else {
									if(quantity==6) {
										quantity=4;
									} else if(quantity == 4) {
										quantity =2;
									}
								}
								
								tempRow = minLastRowMap.get(subtractRowCount);
								if(catTicket != null && tempRow != null) {
									catTicket.setLastRow(tempRow.toUpperCase());
									catTixList.add(catTicket);
									subtractRowCount++;
								} else {
									tempRow = null;
								}
						}
					} else {
						CategoryTicket catTicket = catsMap.get(4);
						if(catTicket != null) {
							catTixList.add(catTicket);
						}
						catTicket = catsMap.get(2);
						if(catTicket != null) {
							catTixList.add(catTicket);
						}
					}
				
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
	
	public static List<CategoryTicket> getRTFLastRowCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		try{
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			String eventExposure = exEvent.getExposure();
			Integer eventSectionEligibleMinEntries = sectionEligibleMinEntries;
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				String rowRange = categoryMapping.getRowRange();
				//String lastRow = categoryMapping.getLastRow();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				if(rowRange == null) {
					rowRange = "";
				}
				
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				Set<Ticket> sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				//for (Ticket tix : sectionTickets) {
						//System.out.println("tix id..."+tix.getId()+"...section..."+tix.getNormalizedSection()+"...purprice.."+tix.getPurchasePrice()+"..qty.."+tix.getQuantity()+"...row ..."+tix.getNormalizedRow());
				//}
				//System.out.println("section..."+key+"..rowrange.."+rowRange+"..lastrow.."+lastRow+"..minilastrow.."+miniLastRow);
				
				Map<Integer,CategoryTicket> catsMap = new HashMap<Integer, CategoryTicket>();
				for (int i = 1; i <= 8; i++) {
					//if(i ==2 || i== 4 ) { //|| i == 6
						int quantity = i;
						
						CategoryTicket catTicket = new RTFLastRowCategoryTicket();
							
						exEvent.setExposure(eventExposure);
						sectionEligibleMinEntries=eventSectionEligibleMinEntries;
						
						catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							catTicket.setLastRow(miniLastRow);
							//catTicket.setRowRange(rowRange);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							catTicket.setPriority(1);
							
							//catTixList.add(catTicket);
							catsMap.put(quantity, catTicket);
						}
					//}
				}
				/*
				 * Compute tickets for lastfive rows from lastrow of each sections
				 * 6qty for last row
				 * 4 qty for last 2nd row
				 * 2 quantity for last 3rd,4th and 5th rows
				 * if ther is no 4 or qty then consider 2 qty listing for all five rows
				 * for last 2nd row computation remove last row listings and check min price from last2nd row if no tix then use last row tix
				 * for lease row comutations use exposure as 1-OXP and mintixCount to 1
				 * 
				 * if lastrow is like GA or BOX ,etc then consider TMAT tickets with row as lastrow only.
				 * consider cats listings row as csv lastorw value.
				 * create listings for qty 2 and 4 with same lastrow value.
				 * if minlastorw list is null then consider that row is like GA or BOX type row
				*/
				if (!catsMap.isEmpty()) {

					int size = 2;
					Map<Integer,String> minLastRowMap = Categorizer.getMiniLastRow(miniLastRow,size);
					
					if(!minLastRowMap.isEmpty()) {
						
						int subtractRowCount = 0;
						String tempRow = null;
						int quantity = 4;
						Set<Ticket> qtyTempSectionTickets = new HashSet<Ticket>(sectionTickets);
						for(int i=0;i<2;i++) {
							CategoryTicket catTicket = null;
							
								if(tempRow != null) {
									eligibleRowList.remove(tempRow.toLowerCase());
									
									
									Set<Ticket> qtySectionTickets = new HashSet<Ticket>();
									for (Ticket ticket : qtyTempSectionTickets) {
										if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
											qtySectionTickets.add(ticket);
										} 
									}
									exEvent.setExposure("1-OXP");
									sectionEligibleMinEntries=1;
									
									qtyTempSectionTickets = new HashSet<Ticket>(qtySectionTickets);
									catTicket = new RTFLastRowCategoryTicket();
									
									catTicket = computecategoryTicket(qtyTempSectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
											tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
									
									if(catTicket != null) {
										catTicket.setLastRow(miniLastRow);
										catTicket.setRowRange(rowRange);
										catTicket.setPosExchangeEventId(event.getAdmitoneId());
										//catTicket.setExpectedArrivalDate(expectedArrivalDate);
										catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
										//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
										catTicket.setPriority(1);
										
										//catTixList.add(catTicket);
										catsMap.put(quantity, catTicket);
									}
								} 
								
								
								if(catTicket == null) {
									while(true) {
										catTicket = catsMap.get(quantity);
										if(quantity==6) {
											quantity=4;
										} else if(quantity == 4) {
											quantity =2;
										} else if(quantity == 2) {
											break;
										}
										if(catTicket != null) {
											catTicket = new RTFLastRowCategoryTicket(catTicket);
											break;
										}
									}
								} else {
									if(quantity==6) {
										quantity=4;
									} else if(quantity == 4) {
										quantity =2;
									}
								}
								
								tempRow = minLastRowMap.get(subtractRowCount);
								if(catTicket != null && tempRow != null) {
									catTicket.setLastRow(tempRow.toUpperCase());
									catTixList.add(catTicket);
									subtractRowCount++;
								} else {
									tempRow = null;
								}
						}
					} else {
						CategoryTicket catTicket = catsMap.get(4);
						if(catTicket != null) {
							catTixList.add(catTicket);
						}
						catTicket = catsMap.get(2);
						if(catTicket != null) {
							catTixList.add(catTicket);
						}
					}
					for (int i = 1; i <= 8; i++) {
						if(i != 2 && i != 4) {
							CategoryTicket catTicket = catsMap.get(i);
							if(catTicket != null) {
								catTixList.add(catTicket);
							}
						}
						
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
	public static List<CategoryTicket> getSGLastFiveRowCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		try{
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getSGLastFiveRowsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				String rowRange = categoryMapping.getRowRange();
				//String lastRow = categoryMapping.getLastRow();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				if(rowRange == null) {
					rowRange = "";
				}
				
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				Set<Ticket> sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				//for (Ticket tix : sectionTickets) {
						//System.out.println("tix id..."+tix.getId()+"...section..."+tix.getNormalizedSection()+"...purprice.."+tix.getPurchasePrice()+"..qty.."+tix.getQuantity()+"...row ..."+tix.getNormalizedRow());
				//}
				//System.out.println("section..."+key+"..rowrange.."+rowRange+"..lastrow.."+lastRow+"..minilastrow.."+miniLastRow);
				
				for (int i = 1; i <= 9; i++) {
					if(i ==2 || i== 4 ) { //|| i == 6 || i == 6
						int quantity = i;
						
						CategoryTicket catTicket = new SGLastFiveRowCategoryTicket();
						catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							catTicket.setLastRow(miniLastRow);
							//catTicket.setRowRange(rowRange);
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							
							catTixList.add(catTicket);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
	public static List<CategoryTicket> getZonesPricingCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product,Map<String, Double> zonesPricingMap) throws Exception {
		
		try{
		
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			String eventExposure = exEvent.getExposure();
			sectionEligibleMinEntries = 1;
			Broker broker = BrokerUtils.getBrokerById(exEvent.getTicketNetworkBrokerId());
			
			if(broker.getName().equals("Reserve One Tickets")) {
				for (String  key : sectionTicketMap.keySet()) {
					Set<Ticket> sectionTickets =  sectionTicketMap.get(key);
					
					int quantity =4;
					CategoryTicket catTicket = null;
					
					//Tamil : if floor cap is exist then we should use 1-OXP for that zone otherwise we should use auto pricing settings
					String zoneExposure = eventExposure;
					if(zonesPricingMap != null) {
						Double floorCap = zonesPricingMap.get(key);
						if(floorCap != null && floorCap > 0) {
							zoneExposure = "1-OXP";
						}
					}
					exEvent.setExposure(zoneExposure);
					//End
					
					//Tamil : if zone is premiusm we have to process only 2 qty tickets else 4 qty tickets
					if(key.contains("premium")) {
						quantity = 2;
					} else {
						quantity = 4;
					}
					
					if(product.getName().equalsIgnoreCase("ZonesPricing")) {
						catTicket = new ZonesPricingCategoryTicket();
					} else if(product.getName().equalsIgnoreCase("RTWZonesPricing")){
						catTicket = new TixCityZonesPricingCategoryTicket();
					}
					catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
					if(catTicket != null) {
						/*if(catTicket.getTnPrice() < 50){
							continue;
						}*/
						
						catTicket.setPosExchangeEventId(event.getAdmitoneId());
						//catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());

						catTixList.add(catTicket);
					}
				}	
			} else if(broker.getName().contains("Right This Way")) {
				
				//Tamil : For RTW process Hamilton Events of Richard Rodgers Theatre,Bank of America Theatre, Pantages Theatre - CA venue only
				/*if(!event.getName().equalsIgnoreCase("Hamilton") || 
						(!event.getVenueId().equals(8327) && !event.getVenueId().equals(8533) && !event.getVenueId().equals(8932))) {
					return catTixList;
				}*/
				
				Calendar calendar = new GregorianCalendar();
			    calendar.setTime(event.getLocalDateTime());
			    int hourOfDay  = calendar.get(Calendar.HOUR_OF_DAY); 
			    
			    SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
			    String eventDayStr = simpleDateformat.format(event.getLocalDateTime());
			    
			    boolean isWednesdayMatineeEvent = false;
			    //If Event day is Wednesday and time is before 5:00 PM then it is called Wednesday Matinee Event
			    if(eventDayStr.equalsIgnoreCase("Wednesday") && hourOfDay < 17) {
			    	isWednesdayMatineeEvent = true;
			    }
				
			    for (String  key : sectionTicketMap.keySet()) {
					Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
					
					List<Integer> qtyList = new ArrayList<Integer>();
					
					//Tamil : for RTW process zone premium,zone a and zone b tickets only in zone pricing for broadway events
					//if(event.getPosChildCategoryName() != null && event.getPosChildCategoryName().equalsIgnoreCase("Broadway")) {
					
					//Tamil : for Broadway events process premium zone tickets only
					/*if(event.getPosChildCategoryName() != null && event.getPosChildCategoryName().equalsIgnoreCase("Broadway") && 
						(!key.equalsIgnoreCase("zone premium"))) {
						continue;
					}*/
					
					//Tamil : for RTW process zone premium,zone a and zone b tickets only in zone pricing for New york evnets
					if(event.getVenue() != null && event.getVenue().getState().equalsIgnoreCase("NY")) {
						if(!key.equalsIgnoreCase("zone premium") && !key.equals("zone a") && !key.equals("zone b")) {
							continue;		
						}
						//Tamil : for RTW process zone premium tickets only in zone pricing
					} else if(!key.equalsIgnoreCase("zone premium")) {
						continue;
					} 
					qtyList.add(2);
					qtyList.add(4);
					
					
					//Tamil : for Hamilton Events do not consider zone keyword tickets.
					Set<Ticket> sectionTickets = new HashSet<Ticket>();
					if(event.getName().equalsIgnoreCase("Hamilton")) {
						for (Ticket ticket : tempSectionTickets) {
							if(ticket.getNormalizedSection().toUpperCase().contains("ZONE")) {
								continue;	
							}
							sectionTickets.add(ticket);
						}
					} else {
						sectionTickets.addAll(tempSectionTickets);
					}
					CategoryTicket catTicket = null;
					
					//Tamil : if floor cap is exist then we should use 1-OXP for that zone otherwise we should use auto pricing settings
					/*String zoneExposure = eventExposure;
					if(zonesPricingMap != null) {
						Double floorCap = zonesPricingMap.get(key);
						if(floorCap != null && floorCap > 0) {
							zoneExposure = "1-OXP";
						}
					}
					exEvent.setExposure(zoneExposure);*/
					//End
					
					
					//Zone A only QTY 2 and do not show Wednesday matinee
					/*if(key.equalsIgnoreCase("ZONE A")) {
						
						if(isWednesdayMatineeEvent) {
							continue;
						}
						qtyList.add(2);
						
					//Zone B QTY 2 and 4 show everything
					} else if(key.equalsIgnoreCase("ZONE B")) {
						qtyList.add(2);
						qtyList.add(4);
						
					//Zone C QTY 2 and 4 Do not show wednesday matinee
					} else if(key.equalsIgnoreCase("ZONE C")) {
						
						if(isWednesdayMatineeEvent) {
							continue;
						}
						qtyList.add(2);
						qtyList.add(4);
						
					//Zone D QTY 2 Do not show wed matinee
					} else if(key.equalsIgnoreCase("ZONE D")) {
						
						if(isWednesdayMatineeEvent) {
							continue;
						}
						qtyList.add(2);
						
					//Zone E QTY 2 show everything
					} else if(key.equalsIgnoreCase("ZONE E")) {
						qtyList.add(2);
					} else {
						continue;
					}*/
					
					for (Integer quantity : qtyList) {
						
						//if(product.getName().equalsIgnoreCase("ZonesPricing")) {
							//catTicket = new ZonesPricingCategoryTicket();
						//} else if(product.getName().equalsIgnoreCase("RTWZonesPricing")){
							catTicket = new TixCityZonesPricingCategoryTicket();
						//}
						catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
						
						if(catTicket != null) {
							
							if( catTicket.getTnPrice() < 50){
								continue;
							}	
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							catTicket.setSection("ZONE "+catTicket.getTicket().getCategory().getSymbol().toUpperCase());

							catTixList.add(catTicket);
							
						}
					}
				}
			    
				/*for (String  key : sectionTicketMap.keySet()) {
					Set<Ticket> sectionTickets = sectionTicketMap.get(key);
					
					int quantity = 4;
					CategoryTicket catTicket = null;
					
					//Tamil : if floor cap is exist then we should use 1-OXP for that zone otherwise we should use auto pricing settings
					String zoneExposure = eventExposure;
					if(zonesPricingMap != null) {
						Double floorCap = zonesPricingMap.get(key);
						if(floorCap != null && floorCap > 0) {
							zoneExposure = "1-OXP";
						}
					}
					exEvent.setExposure(zoneExposure);
					//End
					
					//Tamil : if zone is premiusm we have to process only 2 qty tickets else 4 qty tickets
					if(key.contains("premium")) {
						quantity = 2;
					} else {
						quantity = 4;
					}
					if(product.getName().equalsIgnoreCase("ZonesPricing")) {
						catTicket = new ZonesPricingCategoryTicket();
					} else if(product.getName().equalsIgnoreCase("RTWZonesPricing")){
						catTicket = new TixCityZonesPricingCategoryTicket();
					}
					catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
					
					if(catTicket != null) {
						
							if( catTicket.getTnPrice() < 50){
								continue;
							}	
							catTicket.setPosExchangeEventId(event.getAdmitoneId());
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							catTicket.setSection("ZONE "+catTicket.getTicket().getCategory().getSymbol().toUpperCase());
							
							//if(event.getChildCategoryName().equalsIgnoreCase("Broadway") && 
								//	(key.equalsIgnoreCase("zone premium") || key.equals("zone a"))) {
								//continue;
							//}
							
							//if(key.contains("premium")) {
								//catTicket.setQuantity(8);
							//} else {
								//catTicket.setQuantity(12);
							//}
							//computecategoryTicketPrices(catTicket, exEvent, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup);*/

							//catTixList.add(catTicket);
						
					//}
				//}
			} else if(broker.getName().equals("Manhattan")) {
				
				Double upperMarkup = exEvent.getUpperMarkup();
				Double lowerMarkup = exEvent.getLowerMarkup();
				for (String  key : sectionTicketMap.keySet()) {
					Set<Ticket> sectionTickets = sectionTicketMap.get(key);
					exEvent.setLowerMarkup(lowerMarkup);
					exEvent.setUpperMarkup(upperMarkup);
					int quantity = 4;
					CategoryTicket catTicket = null;
					
					//Tamil : if floor cap is exist then we should use 1-OXP for that zone otherwise we should use auto pricing settings
					String zoneExposure = eventExposure;
					if(zonesPricingMap != null) {
						Double floorCap = zonesPricingMap.get(key);
						if(floorCap != null && floorCap > 0) {
							zoneExposure = "1-OXP";
						}
					}
					exEvent.setExposure(zoneExposure);
					//End
					
					if(key.equalsIgnoreCase("zone premium") || key.equalsIgnoreCase("zone a")) {
						
						//Tamil : if zone is premiusm we have to process only 2 qty tickets else 4 qty tickets
						if(key.contains("premium")) {
							quantity = 2;
							
							// hard coded mark up
							//Tamil : as per Amit Zone premium Markup changed to 0.00
							exEvent.setLowerMarkup(0.00);
							exEvent.setUpperMarkup(0.00);
						} else {
							quantity = 4;
							
							// hard coded mark up
							exEvent.setLowerMarkup(25.00);
							exEvent.setUpperMarkup(25.00);
						}
					} else {
						continue;
					}
					
					if(product.getName().equalsIgnoreCase("ZonesPricing")) {
						catTicket = new ZonesPricingCategoryTicket();
					} else if(product.getName().equalsIgnoreCase("MHZonePricing")){
						catTicket = new ManhattanZonesPricingCategoryTicket();
					}
					catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
					
					if(catTicket != null) {
						/*if( catTicket.getTnPrice() < 50){
							continue;
						}*/
						
						catTicket.setPosExchangeEventId(event.getAdmitoneId());
						//catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
						catTicket.setSection("ZONE "+catTicket.getTicket().getCategory().getSymbol().toUpperCase());
							
						catTixList.add(catTicket);
						
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
	
	public static List<CategoryTicket> getPresaleAutoCatCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {


//		Date now = new Date();
//		now = df.parse(df.format(now));
		
		//sectionEligibleMinEntries=1;
			//logger.info("NF eventId...."+event.getId()+"..section.."+key+"..rowRange.."+rowRange);
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);{				
			for (int i = 1; i <= 9; i++) {
				if(i ==2 || i== 4 || i == 6  || i == 3) {
					int quantity = i;
					PresaleAutoCatCategoryTicket catTicket = new PresaleAutoCatCategoryTicket();
					catTicket= (PresaleAutoCatCategoryTicket) computecategoryTicket(sectionTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
					
					if(catTicket != null) {
						catTicket.setQuantity(quantity);
						catTicket.setPosExchangeEventId(event.getAdmitoneId());
						catTicket.setProductPriority(1);
					//	catTicket.setRowRange(rowRange);
					//	catTicket.setLastRow(lastRow);
						catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
						//catTicket.setSection(key);
						
						catTixList.add(catTicket);
					}
				}
			}
		}
	}		
		return catTixList;
	}	
	
	// Mehul : Added below method for VipLastRowCats product
	
			public static List<CategoryTicket> getVipLastRowMiniCatCategoryTickets(Event event,ExchangeEvent exEvent,
					int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
					Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Double fanxchangeAddMarkup,Double ticketcityAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
					List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

				try{
				
					Date expectedArrivalDate = null;
					if(event.getLocalDate()!=null) {
						Calendar cal = Calendar.getInstance();
						cal.setTime(event.getLocalDate());
						cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
						expectedArrivalDate = cal.getTime();
					}
					
					Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getVipLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromVipLastRow(event);
					CategoryMapping categoryMapping = null;
					
					for (String  key : sectionTicketMap.keySet()) {
						
						categoryMapping = eligibleRowListMap.get(key);
						if(categoryMapping == null) {
							continue;
						}
						Set<String> eligibleRowList = categoryMapping.getMiniVipLastEligibleRows();
						//String rowRange = categoryMapping.getRowRange();
						//String lastRow = categoryMapping.getLastRow();
						String miniVipLastRow = categoryMapping.getMiniVipLastRow();
							
						//if(rowRange == null) {
						//	rowRange = "";
						//}
						
						Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
						Set<Ticket> sectionTickets = new HashSet<Ticket>();
						
						for (Ticket ticket : tempSectionTickets) {
							if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").trim().toLowerCase())) {
								sectionTickets.add(ticket);
							}
						}
						//for (Ticket tix : sectionTickets) {
								//System.out.println("tix id..."+tix.getId()+"...section..."+tix.getNormalizedSection()+"...purprice.."+tix.getPurchasePrice()+"..qty.."+tix.getQuantity()+"...row ..."+tix.getNormalizedRow());
						//}
						//System.out.println("section..."+key+"..rowrange.."+rowRange+"..lastrow.."+lastRow+"..minilastrow.."+miniLastRow);
						
						for (int i = 1; i <= 9; i++) {
							if(i ==2 || i== 4 || i == 6) {
								int quantity = i;
								
								CategoryTicket catTicket = null;
								
								catTicket = new VipLastRowMiniCategoryTicket();
								
								catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
										tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,fanxchangeAddMarkup,ticketcityAddMarkup,catTicket,product);
								
								if(catTicket != null) {
									// Mehul : setting vipLastRow as category ticket last row
									catTicket.setLastRow(miniVipLastRow);
									//catTicket.setRowRange(rowRange);
									catTicket.setPosExchangeEventId(event.getAdmitoneId());
									//catTicket.setExpectedArrivalDate(expectedArrivalDate);
									catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
									//catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
									
									catTixList.add(catTicket);
								}
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
				
				return catTixList;
			}
	public static List<TNDZonesCategoryTicket> getTNDZonesCategoryTickets(Event event,
			int defaultExpectedArrivialDatePriorDays,Double markup,Map<String, Set<Ticket>> zoneTicketMap, 
			List<TNDZonesCategoryTicket> catTixList,AutopricingProduct product,List<TNDZonesCategoryTicket> tndOrders) throws Exception {
	
		try{
		
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			
			Map<String,Boolean> processedSectionMap = new HashMap<String, Boolean>();
			Map<String,Set<Ticket>> sectinTixMap = new HashMap<String, Set<Ticket>>();
			List<String> keyList = new ArrayList<String>(zoneTicketMap.keySet());
			for (String key : keyList) {
				Set<Ticket> sectionTickets = zoneTicketMap.get(key);
				for (Ticket ticket : sectionTickets) {
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
					sectionTickets = sectinTixMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectinTixMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
				}
				
			}
			
			//List<TNDZonesCategoryTicket> tndOrders = new ArrayList<TNDZonesCategoryTicket>();

			for (TNDZonesCategoryTicket tndZonesCatTix : tndOrders) {
				Set<Ticket> zoneTickets = zoneTicketMap.get(tndZonesCatTix.getSection().replaceAll("\\s+", " ").trim().toLowerCase());
				if(zoneTickets != null) {
					for (Ticket ticket : zoneTickets) {
						if(ticket.getQuantity() >= 2 &&
								tndZonesCatTix.getActualPrice().equals(ticket.getPurchasePrice())) {
							//eligSectionList.add(ticket.getNormalizedSection());
							String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").trim().toLowerCase();
							
							if(processedSectionMap.get(section) != null) {
								continue;
							}
							Set<Ticket> sectionTickets = sectinTixMap.get(section);
							if(sectionTickets != null) {
								List<Ticket> orderSectionList = new ArrayList<Ticket>(sectionTickets);
								Collections.sort(orderSectionList, TicketUtil.sortTicketByPurchasePrice);
								
								int size = orderSectionList.size();
								for (int i = 0; i < size; i++) {
									Ticket cheapestTicket = orderSectionList.get(i);
									
									String lastRow = Categorizer.getTNDZonesLastRow(cheapestTicket.getNormalizedRow());
									if(lastRow != null) {
										TNDZonesCategoryTicket tndZoneTix = new TNDZonesCategoryTicket();
										tndZoneTix.createCategoryTicket(cheapestTicket);
										tndZoneTix.setExpectedArrivalDate(expectedArrivalDate);
										tndZoneTix.setQuantity(2);
										tndZoneTix.setEventId(event.getId());
										tndZoneTix.setTnExchangeEventId(event.getAdmitoneId());
										tndZoneTix.setLastRow(lastRow.toUpperCase());
										
										tndZoneTix.setBaseTicketOne(0);
										tndZoneTix.setBaseTicketTwo(0);
										tndZoneTix.setBaseTicketThree(0);
										
										double roundedPrice = (double) Math.ceil(((tndZoneTix.getPurPrice()*(1+(markup)/100))) * 100 / 100);
										tndZoneTix.setActualPrice(roundedPrice);
										tndZoneTix.setTnPrice(roundedPrice);
										
										catTixList.add(tndZoneTix);
										//purPriceMaxThrshold = (double) Math.ceil(cheapestTickets.getPurchasePrice()*(1+(maxPriceThrsholdPercentage)/100));
										
										processedSectionMap.put(section, Boolean.TRUE);
										break;
									}
								}
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return catTixList;
	}
			
}
