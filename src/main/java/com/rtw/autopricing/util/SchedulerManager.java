package com.rtw.autopricing.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;


public class SchedulerManager {
	public static Date FREQUENT_EVENT_JOB_START_TIME = new Date();
	public static Date NON_FREQUENT_EVENT_JOB_START_TIME = new Date();
	
	public static void main(String[] args) {
		
		Timer theaterEventTimer = new Timer();
		Timer sportsEventTimer = new Timer();
		Timer concertsEventTimer = new Timer();
		Timer salesEventTimer = new Timer();
		Timer cleanupEventTimer = new Timer();
		
		
		DateFormat dateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date frequentTime = new Date();
		Date salesJobTime = null;
		Date cleanupJobTime = null;
		Date sportsSchedulerFrequentTime = null;
		Date concertSchedulerFrequentTime = null;
		Date theaterSchedulerFrequentTime = null;
		
		try {
			
			String frequentTimeString = dateTime.format(frequentTime);
			int hourInt = Integer.parseInt(frequentTimeString.split(" ")[1].split(":")[0]);
			hourInt = hourInt+1;
			
			String concertsSchedulerHour = ""+hourInt;
			String sportsSchedulerHour = ""+hourInt;
			String theatersSchedulerHour = ""+hourInt;
			if (hourInt < 3){
				theatersSchedulerHour = "03:30:00";
				sportsSchedulerHour = "04:00:00";
				concertsSchedulerHour = "04:30:00";
			} else if( hourInt >= 3 && hourInt < 9){
				theatersSchedulerHour = "09:30:00";
				sportsSchedulerHour = "10:00:00";
				concertsSchedulerHour = "10:30:00";
			} else if (hourInt >= 9 && hourInt < 15){
//				nonFrequentHour = "21";
				theatersSchedulerHour = "15:30:00";
				sportsSchedulerHour = "16:00:00";
				concertsSchedulerHour = "16:30:00";
			}else if (hourInt >= 15 && hourInt < 21){
//				nonFrequentHour = "21";
				theatersSchedulerHour = "21:30:00";
				sportsSchedulerHour = "22:00:00";
				concertsSchedulerHour = "22:30:00";
			}else {
				theatersSchedulerHour = "03:30:00";
				sportsSchedulerHour = "04:00:00";
				concertsSchedulerHour = "04:30:00";
			}
			

			String frequentHour = ""+hourInt;
			if(hourInt<10){
				frequentHour = "0"+hourInt;
			}
//			nonFrequentHour = nonFrequentHour +":00:00";
			frequentHour = frequentHour +":00:00";
			
			salesJobTime = dateTime.parse(df.format(frequentTime)+" " + frequentHour) ;
			cleanupJobTime = dateTime.parse(df.format(frequentTime)+" " + frequentHour) ;
			sportsSchedulerFrequentTime = dateTime.parse(df.format(frequentTime)+ " " + sportsSchedulerHour);
			concertSchedulerFrequentTime = dateTime.parse(df.format(frequentTime)+ " " + concertsSchedulerHour);
			theaterSchedulerFrequentTime = dateTime.parse(df.format(frequentTime)+ " " + theatersSchedulerHour);
			
			
			
//			FREQUENT_EVENT_JOB_START_TIME = frequentTime;//frequentTime;
//			NON_FREQUENT_EVENT_JOB_START_TIME = nonFrequentTime;
//			
//			
//			System.out.println("salesJobTime time..."+salesJobTime+"...static..."+FREQUENT_EVENT_JOB_START_TIME);
//			System.out.println("non freq time..."+nonFrequentTime+"...static..."+NON_FREQUENT_EVENT_JOB_START_TIME);
//			System.out.println("now......"+new Date());
			
		} catch (Exception e) {
			System.out.println(e.fillInStackTrace());
		}
		
		//This property is used to fix compare method violation exception in java 1.7 
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
//		cleanupEventTimer.scheduleAtFixedRate(new LarryLastTicketsCleaner(), cleanupJobTime ,1 * 5 *  60 * 1000);
//		salesEventTimer.scheduleAtFixedRate(new CategoryTicketSalesHandler(), salesJobTime ,30 * 60 * 1000); 
//		theaterEventTimer.scheduleAtFixedRate(new LarryTheatersScheduler(), theaterSchedulerFrequentTime ,6 * 60 *  60 * 1000); 
//		sportsEventTimer.scheduleAtFixedRate(new LarrySportsScheduler(), sportsSchedulerFrequentTime ,6 * 60 *  60 * 1000); 
//		concertsEventTimer.scheduleAtFixedRate(new LarryLastScheduler(), concertSchedulerFrequentTime ,6 * 60 *  60 * 1000); 
		
	}

}
